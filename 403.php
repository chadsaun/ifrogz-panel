<html>
	<head>
		<title>iFrogz is temporarily down</title>
	</head>
	
	<style type="text/css">
	html, body {height: 100%;}
	
	#message {font-family: Arial; font-size: 18px; color: white;}
	</style>
	
	<body bgcolor="#111111">
	
	<table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">
		<tr>
			<td valign="middle">
			
			<table cellpadding="10" cellspacing="0" width="600" align="center">
				<tr>
					<td align="center" height="200"><img src="/lib/images/ifrogz_logo_big.png" /></td>
				</tr>
				<tr>
					<td align="center"><span id="message">Sorry, iFrogz is temporarily down.  We are making some changes to get things running faster for everyone.  Thank you for your patience.</span></td>
				</tr>
			</table>
			
			</td>
		</tr>
	</table>
	
	</body>
</html>