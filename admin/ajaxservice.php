<?php
session_cache_limiter('none');
session_start();
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business 
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$_GET['gid']!=''){
    include('init.php');
    include(APPPATH.'views/partials/admin/dbconnection.php');
    include(APPPATH.'views/partials/admin/includes.php');
    include(APPPATH.'views/partials/admin/language.php');
    include(APPPATH.'views/partials/admin/languagefile.php');
    include(APPPATH.'views/pages/admin/email.php');
    include(APPPATH.'views/partials/admin/functions.php');
	if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
	if(@$_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE){
	    header('Location: /user/login/');
		exit();
	}
}

if(@$_GET['gid'] != '' || $goid !=''){
	if($goid !='') $ordID = str_replace("'",'',$goid);
	else $ordID = str_replace("'",'',@$_GET['gid']);
	
	$sSQL = "SELECT ordPayProvider,ordAuthNumber,payProvData1,payProvData2,payProvDemo FROM orders INNER JOIN payprovider ON orders.ordPayProvider=payprovider.payProvID WHERE ordID='" . mysql_real_escape_string($ordID) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs = mysql_fetch_assoc($result)){
		$authcode=$rs['ordAuthNumber'];
		$googledata1=$rs['payProvData1'];
		$googledata2=$rs['payProvData2'];
		$googledemomode=$rs['payProvDemo'];
		if($rs['ordPayProvider']==20)$isgoogleorder=TRUE;
		else $isgoogleorder=FALSE;
	}
	
	if(@$_GET['act']=='charge'){
		// First set the status to process-order
		sendmessagewithbasicauth('<process-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"/>');

		$acttext = '<charge-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"></charge-order>';
	}elseif(@$_GET['act']=='cancel')
		$acttext = '<cancel-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"><reason>Cancelled by store admin on ' . date('F d Y H:i:s') . '.</reason></cancel-order>';
	elseif(@$_GET['act']=='refund')
		$acttext = '<refund-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"><reason>Refunded by store admin on ' . date('F d Y H:i:s') . '.</reason></refund-order>';
	elseif(@$_GET['act']=='ship' || $gship=='ship' && $isgoogleorder){
		// First set the status to process-order
		
		sendmessagewithbasicauth('<process-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"/>');

		$acttext = '<deliver-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '">';
		if(@$_GET['carrier'] != '' && @$_GET['trackno'] != ''){
			$sSQL = "UPDATE orders SET ordTrackNum='" . mysql_real_escape_string($_GET['trackno']) . "',ordShipCarrier=" . mysql_real_escape_string(@$_GET['carrier']) . " WHERE ordID='" . mysql_real_escape_string($ordID) . "'";
			mysql_query($sSQL) or print(mysql_error());
			$acttext .= '<tracking-data><carrier>';
			switch($_GET['carrier']){
				case "3":
					$acttext .= "USPS";
				break;
				case "4":
					$acttext .= "UPS";
				break;
				case "7":
					$acttext .= "FedEx";
				break;
				case "8":
					$acttext .= "DHL";
				break;
				default:
					$acttext .= "Other";
			}
			$acttext .= '</carrier><tracking-number>' . trim($_GET['trackno']) . '</tracking-number></tracking-data>';
		}
		$acttext .= '</deliver-order>';
	}elseif(@$_GET['act']=='message'){
		// First set the status to process-order
		sendmessagewithbasicauth('<process-order xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"/>');
		
		$acttext = '<send-buyer-message xmlns="http://checkout.google.com/schema/2" google-order-number="' . $authcode . '"><message>' . @$_POST['googlemessage'] . '</message><send-email>true</send-email></send-buyer-message>';
	}
	
	$cfres = sendmessagewithbasicauth($acttext);
	
	if(@$_GET['gid']!=''){
		if(! $success){
			print '<font color="#FF0000">' . "Error, couldn't update order " . $ordID . '</font><br/>';
		}else{
			$xmlDoc = new vrXMLDoc($cfres);
			$nodeList = $xmlDoc->nodeList->childNodes[0];
			if(($errmsg = $nodeList->getValueByTagName('error-message')) != null)
				print '<font color="#FF0000">' . $errmsg . '</font><br/>';
			else
				print 'Finished updating order ' . $ordID;
		}
	}
}
?>