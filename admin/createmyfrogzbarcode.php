<?php

include_once("Image/Barcode/Code39.php");

if(isset($_GET['ordID'])) {
	
	header('Content-Type: image/png');
	
	$rotate = 180;
	
	if(isset($_GET['rotate'])) {
		$rotate = $_GET['rotate'];
	}
	
	/* Data that will be encoded in the bar code */
	$bar_code_data = $_GET['ordID'];
	
	$bc = new Image_Barcode_Code39('');
	
	//draw the barcode image with the passed data.
	$img_bar = $bc->draw($bar_code_data, 'png', FALSE, 70);
	
	//rotate $img_bar by 180 degress (upside down).
	$img_bar = imagerotate($img_bar, $rotate, 0);
	imagepng($img_bar);
} 

?>