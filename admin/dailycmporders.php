<?php
include('init.php');
include_once(DOCROOT.'includes/ofc/php-ofc-library/open-flash-chart.php');
include_once(APPPATH.'views/partials/admin/dbconnection.php');

// Create dates
$wk1Start = strtotime("-14 days");
$wk1End = strtotime("-8 days");

$wk2Start = strtotime("-7 days");
$wk2End = strtotime("-1 days");

$yearAgo = strtotime("-1 year");

// Get last week numbers by day
$sql = "SELECT DATE_FORMAT(o.ordDate, '%Y/%m/%d') AS dte, COUNT(o.ordID) AS totOrders
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $wk1Start)."' AND '".date("Y-m-d 23:59:59", $wk1End)."'
		GROUP BY dte";
$res = mysql_query($sql) or die('Could not get last weeks numbers.' . mysql_error());
$data1 = array();
$xLabels = array();
$arrDaysOfWeek = array();
while ($row = mysql_fetch_assoc($res)) {
	$data1[] = $row['totOrders'];
	$xLabels[] = date("D", strtotime($row['dte']));
	$arrDaysOfWeek[] = date("w", strtotime($row['dte'])) + 1; // Add 1 because MySQL starts Sunday with 1 and PHP starts Sunday with 0
}

/*echo "Labels<br>";
showArray($xLabels);

echo "Last week<br>";
showArray($data1);*/

// Get this week numbers by day
$sql = "SELECT DATE_FORMAT(o.ordDate, '%Y/%m/%d') AS dte, COUNT(o.ordID) AS totOrders
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $wk2Start)."' AND '".date("Y-m-d 23:59:59", $wk2End)."'
		GROUP BY dte";
$res = mysql_query($sql) or die('Could not get this weeks numbers.' . mysql_error());
$data2 = array();
while ($row = mysql_fetch_assoc($res)) {
	$data2[] = $row['totOrders'];
}

/*echo "This week: from ". date("D", $wk2Start) ." to ". date("D", $wk2End) ."<br>";
showArray($data2);*/

// Get the average of revenue by day
$sql = "SELECT DAYOFWEEK(o.ordDate) AS dd, ROUND(COUNT(o.ordID) / COUNT(DISTINCT WEEK(o.ordDate)), 0) AS AverageD
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $yearAgo)."' AND '" . date('Y-m-d H:i:s') . "'
		GROUP BY dd
		ORDER BY dd";
$res = mysql_query($sql) or die('Could not get the average daily revenue.' . mysql_error());
$arrAvg = array();
while ($row = mysql_fetch_assoc($res)) {
	$arrAvg[$row['dd']] = $row['AverageD'];
}

/*echo "Average week<br>";
showArray($arrAvg); exit();*/

$data3 = array();
for ($i = 0; $i < count($arrDaysOfWeek); $i++) {
	$data3[$i] = $arrAvg[$arrDaysOfWeek[$i]];
}

/*showArray($data3); 
exit();*/

$g = new graph();
//$g->title( 'Daily Orders', '{font-size: 25px; color: #FF8040}' );
$g->bg_colour = '#FFFFFF';
$g->x_axis_colour('#818D9D', '#F0F0F0');
$g->y_axis_colour('#818D9D', '#ADB5C7');

// Set Data
$g->set_data($data3);
$g->line_dot(2, 4, '0x878787', 'Average Daily Orders (within last year)', 10);

$g->set_data( $data1 );
$g->line_dot( 2, 4, '0x78B73A', date("D M j", $wk1Start) . " - " . date("D M j", $wk1End), 10 );

$g->set_data( $data2 );
//$g->line_dot( 3, 5, '0x0077CC', date("M j", $wk2Start) . " - " . date("M j", $wk2End), 10);    // <-- 3px thick + dots
$g->line_dot( 2, 4, '0x0077CC', date("D M j", $wk2Start) . " - " . date("D M j", $wk2End), 10);    // <-- 3px thick + dots

$g->set_x_labels($xLabels);
$g->set_x_label_style( 10, '0x000000', 0, 1 );

$g->set_tool_tip( '#x_label#<br>Orders: #val#' );

// Find max value
$yMax = max(max($data1), max($data2), max($data3));

// Find Steps
$stepIncrement = 100;
$steps = ceil($yMax / $stepIncrement);
$yMax = $stepIncrement * $steps;

$g->set_y_max(roundup($yMax, -1));
$g->y_label_steps($steps);
$g->set_y_legend( 'Number of Orders', 12, '#000000' );

echo $g->render();


function roundup ($value, $dp)
{
    return ceil($value*pow(10, $dp))/pow(10, $dp);
}

function showArray($arr) {
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}

?>

