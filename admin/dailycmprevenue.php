<?php
include('init.php');
include_once(DOCROOT.'includes/ofc/php-ofc-library/open-flash-chart.php');
include_once(APPPATH.'views/partials/admin/dbconnection.php');

// Create dates
$wk1Start = strtotime("-14 days");
$wk1End = strtotime("-8 days");

$wk2Start = strtotime("-7 days");
$wk2End = strtotime("-1 days");

$yearAgo = strtotime("-1 year");

$dayOfWeekPositions = array('Sun' => 0, 'Mon' => 1, 'Tue' => 2, 'Wed' => 3, 'Thu' => 4, 'Fri' => 5, 'Sat' => 6);

// Get last week numbers by day
$sql = "SELECT DATE_FORMAT(o.ordDate, '%Y/%m/%d') AS dte, SUM(o.ordTotal) + SUM(o.ordShipping) - SUM(o.ordDiscount) + SUM(o.ordStateTax) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $wk1Start)."' AND '".date("Y-m-d 23:59:59", $wk1End)."'
		GROUP BY dte";
$res = mysql_query($sql) or die('Could not get last weeks numbers.' . mysql_error());
$data1 = array();
$xLabels = array();
while ($row = mysql_fetch_assoc($res)) {
	$data1[] = $row['totRevenue'];
	$xLabels[] = date("D", strtotime($row['dte']));
}

/*echo "Labels<br>";
showArray($xLabels);

echo "Last week<br>";
showArray($data1);*/

// Find todays position
$todaysPos = $dayOfWeekPositions[$xLabels[0]];
$shift = (6 - $todaysPos) + 1;

// Get this week numbers by day
$sql = "SELECT DATE_FORMAT(o.ordDate, '%Y/%m/%d') AS dte, SUM(o.ordTotal) + SUM(o.ordShipping) - SUM(o.ordDiscount) + SUM(o.ordStateTax) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $wk2Start)."' AND '".date("Y-m-d 23:59:59", $wk2End)."'
		GROUP BY dte";
echo $sql;
$res = mysql_query($sql) or die('Could not get last weeks numbers.' . mysql_error());
$data2 = array();
while ($row = mysql_fetch_assoc($res)) {
	$data2[] = $row['totRevenue'];
}

/*echo "This week: from ". date("D", $wk2Start) ." to ". date("D", $wk2End) ."<br>";
showArray($data2);*/

// Get the average of revenue by day
$sql = "SELECT DAYOFWEEK(o.ordDate) AS dd, (SUM(o.ordTotal) + SUM(o.ordShipping) - SUM(o.ordDiscount) + SUM(o.ordStateTax)) / COUNT(DISTINCT WEEK(o.ordDate)) AS AverageD
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $yearAgo)."' AND '" . date('Y-m-d H:i:s') . "'
		GROUP BY dd
		ORDER BY dd";
$res = mysql_query($sql) or die('Could not get the average daily revenue.' . mysql_error());
$arrAvg = array();
while ($row = mysql_fetch_assoc($res)) {
	$arrAvg[$row['dd'] - 1] = $row['AverageD']; // Minus 1 so they are zero based like PHP
}

/*echo "Average week<br>";
showArray($arrAvg);*/

$data3 = array();
for ($i = 0; $i < count($arrAvg); $i++) {
	$newIndex = (($i + 1) + $shift) % 7; // Add 1 to $i so its not zero based
	if (($newIndex - 1) < 0) {
		$newIndex = 6;
	} else {
		$newIndex = $newIndex - 1;
	}
	$data3[$newIndex] = $arrAvg[$i];
}
ksort($data3);

/*showArray($data3);
exit();*/

$g = new graph();
//$g->title( 'Daily Orders', '{font-size: 25px; color: #FF8040}' );
$g->bg_colour = '#FFFFFF';
$g->x_axis_colour('#818D9D', '#F0F0F0');
$g->y_axis_colour('#818D9D', '#ADB5C7');

// Set Data
$g->set_data($data3);
$g->line_dot(2, 4, '0x878787', 'Average Daily Revenue (within last year)', 10);

$g->set_data( $data1 );
$g->line_dot( 2, 4, '0x78B73A', date("D M j", $wk1Start) . " - " . date("D M j", $wk1End), 10 );

$g->set_data( $data2 );
//$g->line_dot( 3, 5, '0x0077CC', date("M j", $wk2Start) . " - " . date("M j", $wk2End), 10);    // <-- 3px thick + dots
$g->line_dot( 2, 4, '0x0077CC', date("D M j", $wk2Start) . " - " . date("D M j", $wk2End), 10);    // <-- 3px thick + dots

$g->set_x_labels($xLabels);
$g->set_x_label_style( 10, '0x000000', 0, 1 );

$g->set_tool_tip( '#x_label#<br>Revenue: $#val#' );

// Find max value
$yMax = max(max($data1), max($data2), max($data3));

// Find Steps
$stepIncrement = 5000;
$steps = ceil($yMax / $stepIncrement);
$yMax = $stepIncrement * $steps;

//echo $steps; exit();

$g->set_y_max(roundup($yMax, -3));
$g->y_label_steps($steps);
$g->set_y_legend( 'Revenue', 12, '#000000' );

echo $g->render();


function roundup ($value, $dp)
{
    return ceil($value*pow(10, $dp))/pow(10, $dp);
}

function showArray($arr) {
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}

?>

