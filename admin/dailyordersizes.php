<?php
include('init.php');
include_once(DOCROOT.'includes/ofc/php-ofc-library/open-flash-chart.php');
include_once(APPPATH.'views/partials/admin/dbconnection.php');

// Create dates
$mn1Start = strtotime("-31 days");
$mn1End = strtotime("-1 day");

$arrRanges = array();
$arrRanges[0]['from'] = 1;
$arrRanges[0]['to'] = 1;
$arrRanges[1]['from'] = 2;
$arrRanges[1]['to'] = 2;
$arrRanges[2]['from'] = 3;
$arrRanges[2]['to'] = 3;
$arrRanges[3]['from'] = 4;
$arrRanges[3]['to'] = 4;
$arrRanges[4]['from'] = 5;
$arrRanges[4]['to'] = 'infinity';

$dataSet = array();
for ($i = 0; $i < count($arrRanges); $i++) {
	if (ereg("[0-9]+", $arrRanges[$i]['to'])) {	
		$rng = "BETWEEN " . $arrRanges[$i]['from'] . " AND " . $arrRanges[$i]['to'];
	} else {
		$rng = ">= " . $arrRanges[$i]['from'];
	}	
	// Get this week numbers by day
	$sql = "SELECT DATE_FORMAT(o.ordDate, '%Y-%m-%d') AS dte, SUM(c.cartQuantity) $rng AS totOrdQty
			FROM orders o, cart c
			WHERE o.ordID = c.cartOrderID
			AND o.ordStatus >= 3
			AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $mn1Start)."' AND '".date("Y-m-d 23:59:59", $mn1End)."'
			GROUP BY o.ordID";
	//echo $sql; exit();
	$res = mysql_query($sql) or die('Could not get last weeks numbers.' . mysql_error());
	
	$data = array();
	$xLabels = array();
	$date = '';
	$index = 0;
	$j = 0;
	while ($row = mysql_fetch_assoc($res)) {
		if ($date != $row['dte']) {
			if ($j != 0) {
				$index++;
			}
			$xLabels[] = date("M j", strtotime($row['dte']));
		}
		$data[$index] += $row['totOrdQty'];
		$date = $row['dte'];
		$j++;
	}
	$dataSet[] = $data;
}

/*showArray($dataSet);
showArray($xLabels);
exit();*/

$g = new graph();
//$g->title( 'Daily Orders', '{font-size: 25px; color: #FF8040}' );
$g->bg_colour = '#FFFFFF';
$g->x_axis_colour('#818D9D', '#F0F0F0');
$g->y_axis_colour('#818D9D', '#ADB5C7');

$colors = array('FF0F00', 'FF6600', '804040', '008000', 'FF0080', 'B0DE09', '04D215', '0D8ECF', '000000');

// Set Data
$yMax = 0;
for ($i = 0; $i < count($dataSet); $i++) {
	$g->set_data($dataSet[$i]);
	$g->line_dot(2, 4, '0x'.$colors[$i], $arrRanges[$i]['from'] . "-" . $arrRanges[$i]['to'], 10);
	if (max($dataSet[$i]) > $yMax) {
		$yMax = max($dataSet[$i]);
	}
}

$g->set_x_labels($xLabels);
$g->set_x_label_style(10, '0x000000', 0, 4);

$g->set_tool_tip('#x_label#<br>Orders: #val#');

// Find Steps
$stepIncrement = 50;
$steps = ceil($yMax / $stepIncrement);
$yMax = $stepIncrement * $steps;

//echo $steps; exit();

$g->set_y_max(roundup($yMax, 0));
$g->y_label_steps($steps);
$g->set_y_legend('Revenue', 12, '#000000');

echo $g->render();


function roundup ($value, $dp)
{
    return ceil($value*pow(10, $dp))/pow(10, $dp);
}

function showArray($arr) {
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}

?>

