<?php
include('init.php');
include_once(DOCROOT.'includes/ofc/php-ofc-library/open-flash-chart.php');
include_once(APPPATH.'views/partials/admin/dbconnection.php');

// Create dates
$mn1Start = strtotime("-31 days");
$mn1End = strtotime("-1 day");

// Get this week numbers by day
$sql = "SELECT DATE_FORMAT(o.ordDate, '%Y-%m-%d') AS dte, SUM(o.ordTotal) - SUM(o.ordDiscount) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $mn1Start)."' AND '".date("Y-m-d 23:59:59", $mn1End)."'
		GROUP BY dte";
$res = mysql_query($sql) or die('Could not get last weeks numbers.' . mysql_error());
$data1 = array();
$xLabels = array();
while ($row = mysql_fetch_assoc($res)) {
	$data1[] = $row['totRevenue'];
	$xLabels[] = date("M j", strtotime($row['dte']));
}

$g = new graph();
//$g->title( 'Daily Orders', '{font-size: 25px; color: #FF8040}' );
$g->bg_colour = '#FFFFFF';
$g->x_axis_colour('#818D9D', '#F0F0F0');
$g->y_axis_colour('#818D9D', '#ADB5C7');

// Set Data
$g->set_data($data1);
$g->line_dot(2, 4, '0x0077CC', date("D M j", $mn1Start) . " - " . date("D M j", $mn1End), 10);

$g->set_x_labels($xLabels);
$g->set_x_label_style(10, '0x000000', 0, 4);

$g->set_tool_tip('#x_label#<br>Revenue: $#val#');

// Find max value
$yMax = max($data1);

// Find Steps
$stepIncrement = 5000;
$steps = ceil($yMax / $stepIncrement);
$yMax = $stepIncrement * $steps;

//echo $steps; exit();

$g->set_y_max(roundup($yMax, -3));
$g->y_label_steps($steps);
$g->set_y_legend('Revenue', 12, '#000000');

echo $g->render();


function roundup ($value, $dp)
{
    return ceil($value*pow(10, $dp))/pow(10, $dp);
}

function showArray($arr) {
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
}

?>

