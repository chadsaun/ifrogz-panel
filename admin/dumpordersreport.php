<?php
ob_start();
session_cache_limiter('none');
session_start();
//=========================================
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business 
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/functions.php');
if (@$storesessionvalue=="") $storesessionvalue="virtualstore";
if (@$_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) {
	header('Location: /user/login/');
	exit();
}

$admindatestr="Y-m-d";
if (@$admindateformat=="") $admindateformat=0;
if ($admindateformat==1)
	$admindatestr="m/d/Y";
else if ($admindateformat==2)
	$admindatestr="d/m/Y";
if (@$_POST["sd"] != "")
	$sd = @$_POST["sd"];
else if (@$_GET["sd"] != "")
	$sd = @$_GET["sd"];
else
	$sd = date($admindatestr);
if (@$_POST["ed"] != "")
	$ed = @$_POST["ed"];
else if(@$_GET["ed"] != "")
	$ed = @$_GET["ed"];
else
	$ed = date($admindatestr);
$sd = parsedate($sd);
$ed = parsedate($ed);
$hasdetails = (@$_POST["details"]=="true");
$sslok=TRUE;
if(@$_SERVER["HTTPS"] != "on" && (@$_SERVER["SERVER_PORT"] != "443") && @$nochecksslserver != TRUE) $sslok = FALSE;

$sSQL = "SELECT sum(cartQuantity) qty_total,coOptGroup,coCartOption,coOptID FROM cart LEFT JOIN orders ON cart.cartOrderId=orders.ordID LEFT JOIN cartoptions ON cart.cartID=cartoptions.coCartID WHERE ordID IN (".$order_id_commas.") GROUP BY coOptGroup,coCartOption,coOptID";
//echo $sSQL;
//exit();
$result = mysql_query($sSQL);
if(!empty($result)) $num_rows=mysql_num_rows($result);
$str_csv='';
if($num_rows>0) {
	$str_csv.= '"Product","Quantity"';
	$str_csv.= "\r\n";
	//print "<br />";
	while($rs = mysql_fetch_assoc($result)){
		$sql2="SELECT optStyleID,poProdID FROM options o, prodoptions po WHERE o.optGroup=po.poOptionGroup AND optID='".$rs["coOptID"]."'";
		$result2 = mysql_query($sql2) or print(mysql_error());
		$rs2=mysql_fetch_assoc($result2);
		/* sql3="SELECT prodID FROM products WHERE optID='".$rs["coOptID"]."'";
		$result3 = mysql_query($sql3) or print(mysql_error());
		$rs3=mysql_fetch_assoc($result3);  */
		$str_csv.= '"'.str_replace('"','""',$rs2["poProdID"]).str_replace('"','""',$rs2["optStyleID"]).' - '. str_replace('"','""',$rs["coOptGroup"]) . " - " . str_replace('"','""',$rs["coCartOption"]) . '","' .$rs['qty_total'] . '"';
		$str_csv.= "\r\n";
		//print '<br />';
	}

$file_path='order_reports/';
$ourFileName = date('MjYg:ia')."_orders.csv";
$ourFileHandle = fopen($file_path.$ourFileName, 'w') or die("can't open file");
fwrite($ourFileHandle,$str_csv);
fclose($ourFileHandle);	
$order_id_commas='';
?>
<a href="/admin/download.php?path=<?=$file_path?>&file=<?=$ourFileName?>"><?=$ourFileName?></a>
<? } else echo 'There are no authorized orders!';?>