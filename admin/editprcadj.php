<?php
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Edit Price Adjustment</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/javascript">
<!--
function clrl(){opener.location.reload(true);window.close();}
-->
</script>
</head>
<body <?=(($close==1)?'onload="javascript:clrl();"':'')?>>
<?

$qry = "SELECT * FROM price_adj WHERE id = '$pa_id'";
$res = mysql_query($qry);
if(!$res)
	die('Problem. Reason: '.mysql_error().'<br />'.$qry);
	
$row = mysql_fetch_assoc($res);

if($action == 'edit')
{
?>
	<form id="form1" name="form1" method="post" action="/admin/editprcadjprocess.php">
	<table width="100%" align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; border:1px solid #999; border-collapse: collapse">
	<tr>
		<th>Type</th>
		<th>Amt Type</th>
		<th>Amount</th>
		<th>Note</th>
	</tr>
	<tr valign="top">
		<td style="text-align: center">
			<select id="type" name="type">
				<option value="credit"<?=($row['type']=='credit')?' selected="selected"':''?>>Credit</option>
				<option value="debit"<?=($row['type']=='debit')?' selected="selected"':''?>>Debit</option>
			</select>
		</td>
		<td style="text-align: center">
			<select id="amt_type" name="amt_type">
				<option value="percentage"<?=($row['amt_type']=='percentage')?' selected="selected"':''?>>%</option>
				<option value="fixed"<?=($row['amt_type']=='fixed')?' selected="selected"':''?>>$</option>
			</select>
		</td>
		<td style="text-align: center"><input type="text" id="amt" name="amt" style="text-align: right" value="<?=$row['amt']?>" /></td>
		<td style="text-align: center"><textarea id="pa_note" name="pa_note"><?=$row['note']?></textarea></td>
	</tr>
	<tr>
		<td colspan="4" style="text-align: center"><input type="submit" id="editPA" name="editPA" value="Edit" /></td>
	</tr>			
	</table>
	<input type="hidden" name="pa_id" id="pa_id" value="<?=$row['id']?>" />
	<input type="hidden" name="pa_ordID" id="pa_ordID" value="<?=$row['ordID']?>" />
	</form>
<?php
}
elseif($action == 'delete')
{
?>
	<div align="center" style="margin: 10px auto; font-size: 16px; font-weight: bold">Are you sure you want to <strong>delete</strong> this price adjustment?</div>
	<form id="form2" name="form2" method="post" action="/admin/editprcadjprocess.php">
	<table width="100%" align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; border:1px solid #999; border-collapse: collapse">
	<tr>
		<th>Type</th>
		<th>Amt Type</th>
		<th>Amount</th>
		<th>Note</th>
	</tr>
	<tr valign="top">
		<td style="text-align: center">
			<select id="type" name="type">
				<option value="credit"<?=($row['type']=='credit')?' selected="selected"':''?>>Credit</option>
				<option value="debit"<?=($row['type']=='debit')?' selected="selected"':''?>>Debit</option>
			</select>
		</td>
		<td style="text-align: center">
			<select id="amt_type" name="amt_type">
				<option value="percentage"<?=($row['amt_type']=='percentage')?' selected="selected"':''?>>%</option>
				<option value="fixed"<?=($row['amt_type']=='fixed')?' selected="selected"':''?>>$</option>
			</select>
		</td>
		<td style="text-align: center"><input type="text" id="amt" name="amt" style="text-align: right" value="<?=$row['amt']?>" /></td>
		<td style="text-align: center"><textarea id="pa_note" name="pa_note"><?=$row['note']?></textarea></td>
	</tr>
	<tr>
		<td colspan="4" style="text-align: center"><input type="submit" id="delPA" name="delPA" value="Delete" /></td>
	</tr>			
	</table>
	<input type="hidden" name="pa_id" id="pa_id" value="<?=$row['id']?>" />
	<input type="hidden" name="pa_ordID" id="pa_ordID" value="<?=$row['ordID']?>" />
	</form>
<?php
}
?>
</body>
</html>