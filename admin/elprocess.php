<?php
session_cache_limiter('none');
session_start();
session_register('msg');
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/functions.php');

if($_REQUEST['action'] == 'subscribe') {
	// VALIDATE
	if(!ereg("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*",$_REQUEST['email'])) {
		$_SESSION['msg']['err'] = ''.$_REQUEST['email'].' is not a valid e-mail address.';
		header("Location: /el_subscribe.php");
		exit();
	}
	
	
	foreach($_REQUEST['list'] as $el_list) {
		// CHECK FOR DUPLICATE EMAIL
		$sql = "SELECT * FROM email_lists 
				WHERE email = '".E($_REQUEST['email'])."' 
				AND subscribed = '".E($el_list)."'
				AND status = 'active'";
		$res = mysql_query($sql) or print(mysql_error());
		if(mysql_num_rows($res) > 0) {
			//mysql_free_result($res);
			$_SESSION['msg']['err'] = 'You are already subscribed to '.$el_list.'and have been entered into the drawing.';
			//header("Location:http://".$_SERVER['HTTP_HOST']."/el_subscribe.php");
			//exit();
		}
		mysql_free_result($res);
		
		// CHECK TO SEE IF THEY HAVE SUBSCRIBED PREVIOUSLY
		$sql = "SELECT * FROM email_lists 
				WHERE email = '".E($_REQUEST['email'])."' 
				AND subscribed = '".E($el_list)."'
				";
		$res = mysql_query($sql) or print(mysql_error());
		if(mysql_num_rows($res) > 0) {
			$sql = "UPDATE email_lists
					SET status = 'active', firstname = '".E($_REQUEST['fname'])."', lastname = '".E($_REQUEST['lname'])."', extra=CONCAT(extra, ' ".$_REQUEST['extra']."')
					WHERE email = '".E($_REQUEST['email'])."'
					AND subscribed = '".E($el_list)."'";
			//echo $sql;
			$res = mysql_query($sql) or print(mysql_error());
		}else{
			$sql = "INSERT INTO email_lists ( firstname , lastname , email , ordered , subscribed , status, extra )
					VALUES ( '".E($_REQUEST['fname'])."' , '".E($_REQUEST['lname'])."' , '".E($_REQUEST['email'])."' , '".date("Y-m-d H:i:s")."' , '".E($el_list)."' , 'active','".$_REQUEST['extra']."' )";
			$res = mysql_query($sql) or print(mysql_error());
		}
	}
	
	$_SESSION['msg']['suc'] = 'Thank you for subscribing and have been entered into the drawing!';
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/el_subscribe.php');
	exit();
}elseif($_REQUEST['action'] == 'unsubscribe') {
	//showarray($_REQUEST); exit();
	if(!is_array($_REQUEST['list'])) {
		$_SESSION['msg']['err'] = 'You did not select from a list to be unsubscribed.';
		header('Location: http://'.$_SERVER['HTTP_HOST'].'/el_subscribe.php');
		exit();
	}
	foreach($_REQUEST['list'] as $el_list) {
		if($el_list == 'itunes') {
			// CHECK TO SEE IF THEY ARE SUBSCRIBED
			$sql = "SELECT * FROM itunes WHERE email = '".E($_REQUEST['email'])."'";
			$res = mysql_query($sql) or print(mysql_error());
			if(mysql_num_rows($res) > 0) {
				mysql_free_result($res);
				// DELETE EMAIL FROM TABLE
				$sql = "DELETE FROM itunes
						WHERE email = '".E($_REQUEST['email'])."'";
				$res = mysql_query($sql) or print(mysql_error());
				
				$_SESSION['msg']['suc'] = 'You have been unsubscribed.';
				header('location:http://'.$_SERVER['HTTP_HOST'].'/el_subscribe.php');
				exit();
			}
			
			$_SESSION['msg']['suc'] = 'You are not subscribed for this list so you cannot be removed.';
			header('Location: http://'.$_SERVER['HTTP_HOST'].'/el_subscribe.php');
			exit();
		}else{
			// CHECK TO SEE IF THEY ARE SUBSCRIBED
			$sql = "SELECT * FROM email_lists WHERE email = '".E($_REQUEST['email'])."'";
			$res = mysql_query($sql) or print(mysql_error());
			if(mysql_num_rows($res) > 0) {
				mysql_free_result($res);
				// SET EMAIL TO INACTIVE
				$sql = "DELETE FROM email_lists
						WHERE email = '".E($_REQUEST['email'])."'";
				$res = mysql_query($sql) or print(mysql_error());
				
				$_SESSION['msg']['suc'] = 'You have been unsubscribed.';
				header('location:http://'.$_SERVER['HTTP_HOST'].'/el_subscribe.php');
				exit();
			}
			
			$_SESSION['msg']['suc'] = 'You are not subscribed for this list so you cannot be removed.';
			header('location:http://'.$_SERVER['HTTP_HOST'].'/el_subscribe.php');
			exit();
		}
	}
}

function E($val) {
	$newval = mysql_real_escape_string($val);
	return $newval;
}
?>