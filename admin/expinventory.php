<?php
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/functions.php');

if (!empty($_SESSION['arrInfo'])) {
	$arr = $_SESSION['arrInfo'];
	
	// Determine if we have options or product info
	$isOptions = false;
	if (isset($arr[0]['optExtend_shipping'])) {
		$isOptions = true;
	}
	
	$csv = '';
	if ($isOptions) {
		$csv .= "Product ID,Option Name,On Hand,On Order,Available,Min Threshold,Extend Shipping,Display Point,Reorder Point,Reorder Quantity,Bin,Alt Prod ID\n";
		for ($i=0; $i < count($arr); $i++) {
			$arrTmp = array();
			$arrTmp[] = $arr[$i]['pID'];
			$arrTmp[] = $arr[$i]['optName'];
			$arrTmp[] = $arr[$i]['onhand'];
			$arrTmp[] = $arr[$i]['onOrder'];
			$arrTmp[] = $arr[$i]['optStock'] - $arr[$i]['onOrder'];
			$arrTmp[] = $arr[$i]['optMin'];
			$arrTmp[] = $arr[$i]['optExtend_shipping'];
			$arrTmp[] = $arr[$i]['optDisplay_point'];
			$arrTmp[] = $arr[$i]['optReorder_point'];
			$arrTmp[] = $arr[$i]['optReorder_qty'];
			$arrTmp[] = $arr[$i]['optBin'];
			$arrTmp[] = $arr[$i]['optRegExp'];
			
			$csv .= implode(",", $arrTmp)."\n";
		}
	} else {
		$csv .= "Product ID,On Hand,On Order,Available,Bin\n";
		for ($i=0; $i < count($arr); $i++) {
			$arrTmp = array();
			$arrTmp[] = $arr[$i]['pID'];
			$arrTmp[] = $arr[$i]['onhand'];
			$arrTmp[] = $arr[$i]['onOrder'];
			$arrTmp[] = $arr[$i]['pInStock'];
			$arrTmp[] = $arr[$i]['optBin'];
			
			$csv .= implode(",", $arrTmp)."\n";
		}
	}
	
	header("Content-Type: application/octet-stream");
	header('Content-Disposition: attachment; filename=inventory.csv');
	header('Pragma: no-cache');
	
	echo $csv;
}
?>