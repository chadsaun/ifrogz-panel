<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<h3>Upload Your Image</h3>
<ul>
	<li>To upload an image, click browse</li>
 	<li>A window will appear</li>
  	<li>Locate the desired image on your computer</li>
	<li>Select the image, and then click the open button</li>
   	<li>Click the Upload button to begin uploading your image.</li>
</ul>
<p>
	<form action="/admin/upload.php" method="post" name="frmupload" enctype="multipart/form-data">
		<input name="file" type="file"><br />
		<input name="upload" type="submit" value="upload" onClick="">
	</form>
</p>
<h3>Upload Guide</h3>
<ul>
	<li>Picture files can be in either JPEG (*.jpg, .jpeg) or Bitmap (*.bmp) formats.</li>
    <li>The file size should be at least 50KB in size, not to exceed three megabytes (Mb) and a least 640X480 pixels. For best results, use larger images with higher resolution for greater clarity on your personalized card.</li>
    <li>The maximum file size is three megabytes (Mb)</li>
    <li>Small pictures will reproduce poorly.</li>
    <li>If you increase the scale of your picture excessively, it may also reproduce poorly.</li>
    <li>The bigger the picture you choose, the longer it will take to upload.</li>
    <li>The time your picture takes to upload depends on your internet connection speed.</li>
    <li>If your image is slow to upload, please talk to your own internet service provider for help.</li>
    <li>On a typical 56k modem, each megabyte will take about three minutes to upload. However, this speed depends on your connection speed.</li>
    <li>You image needs to be downloaded to your computer&apos;s hard drive, before it can be uploaded to the designer.</li>
</ul>
</body>
</html>