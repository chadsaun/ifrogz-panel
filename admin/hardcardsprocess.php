<?php
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');

if($_GET['action'] == 'deletecert') {
	$sql = "UPDATE certificates
			SET cert_amt = 0, cert_prod_id = 'n/a', pend_order_amt = 0, pend_order_id = 0, cert_amt = 0, 
			cert_order_id = 0, cert_email = '', cert_exp_dt = 0, cert_group = ''
			WHERE cert_id = " . $_GET['cert_id'];
	$res = mysql_query($sql);
	
	$_SESSION['viewed'] = '';
	header("location: /admin/hardcards.php");
	exit();
}

if($_GET['action'] == 'deactivateselected') {
	foreach($_GET as $key => $value) {
		if(strstr($key,'cert')) {
			$sql = "UPDATE certificates
					SET cert_amt = 0, cert_prod_id = 'n/a', pend_order_amt = 0, pend_order_id = 0, cert_amt = 0, 
					cert_order_id = 0, cert_email = '', cert_exp_dt = 0, cert_group = ''
					WHERE cert_id = " . $value;
			$res = mysql_query($sql);
		}
	}
	
	$_SESSION['viewed'] = '';
	header("location: /admin/hardcards.php");
	exit();
}

if($_POST['action'] == 'mod_all_certs') {
	foreach($_POST['certs'] as $certID) {
		$sql = "UPDATE certificates
				SET cert_group = '".$_POST['cert_group']."', cert_exp_dt = ".strtotime($_POST['cert_exp_dt']).", 
					cert_prod_id = '".$_POST['cert_prod_id']."', cert_email = '".$_POST['cert_email']."', 
					cert_amt = '".$_POST['cert_amt']."', cert_order_id = '".$_POST['cert_order_id']."', 
					pend_order_amt = '".$_POST['pend_order_amt']."', pend_order_id = '".$_POST['pend_order_id']."'
				WHERE cert_id = $certID";
		$res = mysql_query($sql) or print(mysql_error());
	}
	
	$_SESSION['viewed'] = '';
	header("location: /admin/hardcards.php");
	exit();
}

function showarray($array)
{
    if(is_array($array)) {   
        echo '<ul>';
        foreach($array as $k=>$v) {
            if(is_array($v)) {
                echo '<li>K:'.$k.'</li>';
                showarray($v);
            }else {
                echo '<li>'.$k.'='.$v.'</li>';
            }
        }
        echo '</ul>';
    }else {
        return 'Not an array';
    }
}
?>