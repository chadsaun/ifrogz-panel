<?php
session_cache_limiter('none');
session_start();
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business 
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$_SESSION["loggedon"] != "virtualstore" && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
?>
<html>
<head>
<title>Admin</title>
<link rel="stylesheet" type="text/css" href="/lib/css/styles.css">
<?php
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
?>
<meta http-equiv="Content-Type" content="text/html; charset=<?php print $adminencoding ?>">
<script language="JavaScript" type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#000033" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('/lib/images/homeov.gif','/lib/images/mainov.gif','/lib/images/passwordov.gif','/lib/images/logoutov.gif','/lib/images/catsov.gif','/lib/images/prodsov.gif','/lib/images/prodoptsov.gif','/lib/images/statesov.gif','/lib/images/countriesov.gif','/lib/images/postalov.gif','/lib/images/paymentov.gif','/lib/images/affiliatesov.gif','/lib/images/submitov.gif','/lib/images/feedbackov.gif','/lib/images/ordersov.gif','/lib/images/affiliateov.gif','/lib/images/discountsov.gif')">
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#000033">
  <tr> 
    <td width="280" rowspan="2"><img src="/lib/images/logo.gif" width="277" height="80"></td>
    <td class="dark" align="right" valign="bottom"><a class="dark" href="http://www.ecommercetemplates.com/help.asp" target="_blank">help</a> 
      - <a class="dark" href="http://www.ecommercetemplates.com/support/" target="_blank">forum</a> 
      - <a class="dark" href="http://www.ecommercetemplates.com/support/search.asp" target="_blank">search forum</a> - <a class="dark" href="http://www.ecommercetemplates.com/updaters.asp" target="_blank">updaters</a> - 
      <a class="dark" href="/admin/logout.php">log-out</a>&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td align="right"><img src="/lib/images/boxes.gif" width="450" height="27"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="123" align="center" valign="top" bgcolor="#E7EAEF"><img src="/lib/images/admin.gif" width="123" height="19"><br>
      <a href="/admin/index.php"><img src="/lib/images/homeov.gif" name="Image11" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/main.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image15','','/lib/images/mainov.gif',1)"><img src="/lib/images/main.gif" name="Image15" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/orders.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','/lib/images/ordersov.gif',1)"><img src="/lib/images/orders.gif" name="Image42" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/login.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image17','','/lib/images/passwordov.gif',1)"><img src="/lib/images/password.gif" name="Image17" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/payprov.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image34','','/lib/images/paymentov.gif',1)"><img src="/lib/images/payment.gif" name="Image34" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/menuline.gif" width="119" height="1"><br>
	  <a href="/admin/affil.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image45','','/lib/images/affiliateov.gif',1)"><img src="/lib/images/affiliate.gif" name="Image45" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/logout.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','/lib/images/logoutov.gif',1)"><img src="/lib/images/logout.gif" name="Image18" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/products.gif"><br>
      <a href="/admin/prods.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image24','','/lib/images/prodsov.gif',1)"><img src="/lib/images/prods.gif" name="Image24" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/prodopts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image26','','/lib/images/prodoptsov.gif',1)"><img src="/lib/images/prodopts.gif" name="Image26" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/cats.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image22','','/lib/images/catsov.gif',1)"><img src="/lib/images/cats.gif" name="Image22" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
	  <a href="/admin/discounts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','/lib/images/discountsov.gif',1)"><img src="/lib/images/discounts.gif" name="Image47" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/shipping.gif" width="123" height="19"><br>
      <a href="/admin/state.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image28','','/lib/images/statesov.gif',1)"><img src="/lib/images/states.gif" name="Image28" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/country.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image30','','/lib/images/countriesov.gif',1)"><img src="/lib/images/countries.gif" name="Image30" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="/admin/zones.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image32','','/lib/images/postalov.gif',1)"><img src="/lib/images/postal.gif" name="Image32" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/extras.gif" width="123" height="19"><br>
      <a href="http://www.ecommercetemplates.com/affiliateinfo.asp" target="_blank" onMouseOver="MM_swapImage('Image36','','/lib/images/affiliatesov.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="/lib/images/affiliates.gif" name="Image36" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="http://www.ecommercetemplates.com/addsite.asp" target="_blank" onMouseOver="MM_swapImage('Image38','','/lib/images/submitov.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="/lib/images/submit.gif" name="Image38" width="123" height="19" border="0"></a><br>
	  <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <a href="http://www.ecommercetemplates.com/support/" target="_blank" onMouseOver="MM_swapImage('Image40','','/lib/images/feedbackov.gif',1)" onMouseOut="MM_swapImgRestore()"><img src="/lib/images/feedback.gif" name="Image40" width="123" height="19" border="0"></a><br>
      <img src="/lib/images/menuline.gif" width="119" height="1"><br>
      <p>&nbsp;</p></td>
    <td width="1" bgcolor="#000033"><img src="/lib/images/misc/clearpixel.gif" width="1" height="1"></td>
    <td width="19" valign="top" bgcolor="#FFFFFF"><img src="/lib/images/tl.gif" width="19" height="19"></td>
    <td width="100%" valign="top" bgcolor="#FFFFFF"><br> 
      <table width="100%" border="0" cellpadding="2" cellspacing="2" bgcolor="#FFFFFF">
        <tr> 
          <td>
<?php include(APPPATH.'views/pages/admin/hits.php'); ?></td>
        </tr>
      </table></td>
    <td width="19" align="right" valign="top" bgcolor="#FFFFFF"><img src="/lib/images/tr.gif" width="19" height="19"></td>
    <td width="6" bgcolor="#000033"><img src="/lib/images/misc/clearpixel.gif" width="6" height="1"></td>
  </tr>
  <tr> 
    <td bgcolor="#000033">&nbsp;</td>
    <td width="1" bgcolor="#000033"><img src="/lib/images/misc/clearpixel.gif" width="1" height="1"></td>
    <td><img src="/lib/images/bl.gif" width="19" height="19"></td>
    <td class="smaller" align="center" bgcolor="#FFFFFF">&copy; Copyright 2002 <a href="http://www.ecommercetemplates.com/" target="_blank">Shopping 
      cart software</a> by ecommercetemplates.com</td>
    <td align="right"><img src="/lib/images/br.gif" width="19" height="19"></td>
  <td width="6" bgcolor="#000033"><img src="/lib/images/misc/clearpixel.gif" width="6" height="1"></td>
  </tr>
  <tr> 
    <td colspan="3" bgcolor="#000033">&nbsp;</td>
    <td bgcolor="#000033">&nbsp;</td>
    <td colspan="2" bgcolor="#000033">&nbsp;</td>
  </tr>
</table>
</body>
</html>