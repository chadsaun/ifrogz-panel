<?php
//////////////////////////////////////////////////////////////////////////////////////////
if ( ! defined('KOHANA_EXTERNAL_MODE')) {
    define('KOHANA_EXTERNAL_MODE', TRUE);
}
include('init.php');
include_once(DOCROOT.'index.php');
//////////////////////////////////////////////////////////////////////////////////////////

if (isset($_GET['forwarded'])) {
	if ($_GET['forwarded'] == 'yes') {
		session_regenerate_id();
	}
}

$session = Session::instance();
$loggedon = $session->get('loggedon');

//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
include_once(IFZROOT.'kohana.php');

if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(($loggedon != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])=="") || @$disallowlogin==TRUE){
	header('Location: /user/login/');
	exit();
}
if($loggedon != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])!=""){ // Checking their cookie to login

	$config_admin = RBI_Kohana::config('database.default_admin.connection');

	$db_admin = mysql_connect($config['hostname'], $config['username'], $config['password']);
	mysql_select_db($config['database']) or die ('DB Admin connection failed.</td></tr></table></body></html>');

	$rbiSQL = 'select * from employee where username="'.trim($_COOKIE["WRITECKL"]).'" and HPassword="'.md5(trim($_COOKIE["WRITECKP"])).'"';
	$rs_rbi = mysql_query($rbiSQL, $db_admin);
	$rbi_row = mysql_fetch_assoc($rs_rbi);
	if(!empty($rbi_row['username'])) 
	{
		$_SESSION["employee"]=$rbi_row;
		$rbi_login_allow = true;
		@$_SESSION["loggedon"] = $storesessionvalue;
	}else{
		$success=2;
	}

	include(APPPATH.'views/partials/admin/dbconnection.php');
	
	/*$sSQL="SELECT adminID FROM admin WHERE adminUser='" . mysql_real_escape_string(unstripslashes(trim(@$_COOKIE["WRITECKL"]))) . "' AND adminPassword='" . mysql_real_escape_string(unstripslashes(trim(@$_COOKIE["WRITECKP"]))) . "' AND adminID=1";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result)>0)
		@$_SESSION["loggedon"] = $storesessionvalue;
	else
		$success=2;
	mysql_free_result($result);*/
}
$isprinter=FALSE;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Admin Home</title>
<link rel="stylesheet" type="text/css" href="/lib/css/adminstyle.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $adminencoding ?>"/>
</head>
<body <?php if($isprinter) print 'class="printbody"'?>>
<?php if (! $isprinter) { ?>
    <?php include(APPPATH.'views/partials/admin/nav.php'); ?>
<?php } ?>
<div id="main">
    <?php include(APPPATH.'views/pages/admin/index.php'); ?>
</div>
</body>
</html>