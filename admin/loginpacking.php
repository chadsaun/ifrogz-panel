<?php
session_cache_limiter('none');
session_start();
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/languagefile.php');
include(APPPATH.'views/partials/admin/functions.php');
if (strstr($_SERVER['HTTP_HOST'], ':8888')) {
	include_once(DOCROOT.'kohanabase/kohana.php');
} else {
	include_once(IFZROOT.'kohana.php');
}

if (@$storesessionvalue=="") $storesessionvalue="virtualstore";
if ((@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])=="") || @$disallowlogin==TRUE) {
	header('Location: /user/login/');
	exit();
}
$isprinter = (@$_GET["printer"]=="true");
$usepowersearch = (@$_COOKIE["powersearch"]=="1");
if (@$_POST["powersearch"]=="1") {
	if (@$_POST["startwith"]=="1") {
		setcookie("powersearch","1",time()+(60*60*24*365), "/");
		$usepowersearch = TRUE;
	} else {
		setcookie("powersearch","0",time()+(60*60*24*365), "/");
		$usepowersearch = FALSE;
	}
}

if (@$_POST["posted"]=="1") {
	$errmsg='';

	$config_admin = RBI_Kohana::config('database.default_admin.connection');

	$db_admin = mysql_connect($config['hostname'], $config['username'], $config['password']);
	mysql_select_db($config['database']) or die ('DB Admin connection failed.</td></tr></table></body></html>');

	$_POST["user1"] = str_replace(array(';','insert','delete'),'',substr($_POST[user1],0,24));
	$_POST["pass1"] = str_replace(array(';','insert','delete'),'',substr($_POST[pass1],0,24));
	$rbiSQL = 'select * from employee where username="'.$_POST['user1'].'" and HPassword="'.md5($_POST['pass1']).'"';
	$rs_rbi = mysql_query($rbiSQL, $db_admin);
	$rbi_row = mysql_fetch_assoc($rs_rbi);
	if (!empty($rbi_row['username'])) {
		$_SESSION['packingsliplogin']=$rbi_row;
		//header("Location: /admin/loginpackingslip.php");
		//echo 'test';
		//exit;
		?>
        <script type="text/javascript">
			function reloadparent(){
				window.opener.location.reload();
				//alert('in');
				window.close();
			}		
			window.open('/admin/loginpackingslip.php','Packing Slip','target=_blank,menubar=yes,scrollbars=yes,resizable=yes');
			//window.close();
		</script>
		<?php
	} else {
		$errmsg = 'Wrong Username Or Password';
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Admin Orders</title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php print $adminencoding; ?>"/>
</head>
<body onload="document.forms.plogin.user1.focus();" style="background-color: #E2E2E2;">
<div style="margin: 6px;">                              
    <h3 style="margin: 2px;">Packing Slip Login</h3>
    <div style="margin: 2px; color: #FF0000; font-weight: bold;"><?php if ($errmsg != '') { echo $errmsg; } ?></div>
    <form method="post" action="/admin/loginpacking.php" name="plogin">
        <input type="hidden" name="posted" value="1" />
        <strong><?php print $yyUname?>:</strong><br />          
        <input type="text" name="user1" id="user1" /><br />
        <strong><?php print $yyPass?>:</strong><br />
        <input type="password" name="pass1" id="pass1"><br />
        <input type="submit" name="login" value="login" />
    </form>   
</div>
</body>
</html>