<?php
session_cache_limiter('none');
session_start();
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
$isprinter = FALSE;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Admin Login</title>
<link rel="stylesheet" type="text/css" href="/lib/css/adminstyle.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=<?php print $adminencoding; ?>"/>
</head>
<body>
<div id="header">
    <div style="float: left; width: 200px; height: 61px; padding: 14px 0px 0px 14px;"><a href="/user/index/"><img style="width: 180px; height: 48px;" src="/lib/images/template/header/logoadmin.png" /></a></div>
    <div style="float: left; width: 110px;"><img style="width: 108px; height: 75px;" src="/lib/images/template/header/ethernet.png" /></div>
	<div style="float: right; width: 125px; height: 75px; line-height: 75px; padding-right: 14px; font-size: 16px; font-weight: bold;"><a href="javascript:void(0);" onclick="window.opener.close();window.close();" style="color: #FFFFFF;">Close</a></div>
    <div style="clear: both;"></div>
</div>
<div style="clear: both;"></div>
<?php include(APPPATH.'views/pages/admin/loginpackingslip.php'); ?>
</body>
</html>
