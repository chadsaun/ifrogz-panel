<?php
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/languagefile.php');
include(APPPATH.'views/partials/admin/functions.php');

//echo 'id= '.$_POST["id"];
//exit();
$isInStock=TRUE;
$WSP = "";
$OWSP = "";
$theid = mysql_real_escape_string(trim(@$_POST["id"]));
if(@$dateadjust=="") $dateadjust=0;
$alreadygotadmin = getadminsettings();
if(@$_SESSION["clientUser"] != ""){
	if(($_SESSION["clientActions"] & 8) == 8){
		$WSP = "pWholesalePrice AS ";
		if(@$wholesaleoptionpricediff==TRUE) $OWSP = 'optWholesalePriceDiff AS ';
		if(@$nowholesalediscounts==TRUE) $nodiscounts=TRUE;
	}
	if(($_SESSION["clientActions"] & 16) == 16){
		$WSP = $_SESSION["clientPercentDiscount"] . "*pPrice AS ";
		if(@$wholesaleoptionpricediff==TRUE) $OWSP = $_SESSION["clientPercentDiscount"] . '*optPriceDiff AS ';
		if(@$nowholesalediscounts==TRUE) $nodiscounts=TRUE;
	}
}

/*if($stockManage != 0){
	$sSQL = "SELECT cartOrderID,cartID FROM cart WHERE (cartCompleted=0 AND cartOrderID=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()+(($dateadjust-$stockManage)*60*60)) . "')";
	if($delAfter != 0)
		$sSQL .= " OR (cartCompleted=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()-($delAfter*60*60*24)) . "')";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result)>0){
		$addcomma = "";
		$delstr="";
		$delcart="";
		while($rs = mysql_fetch_assoc($result)){
			$delcart .= $addcomma . $rs["cartOrderID"];
			$delstr .= $addcomma . $rs["cartID"];
			$addcomma = ",";
		}
		if($delAfter != 0) mysql_query("DELETE FROM orders WHERE ordID IN (" . $delcart . ")") or print(mysql_error());
		mysql_query("DELETE FROM cart WHERE cartID IN (" . $delstr . ")") or print(mysql_error());
		mysql_query("DELETE FROM cartoptions WHERE coCartID IN (" . $delstr . ")") or print(mysql_error());
	}
	mysql_free_result($result);
}*/

function checkpricebreaks($cpbpid,$origprice){
	global $WSP;
	$newprice="";
	$sSQL = "SELECT SUM(cartQuantity) AS totquant FROM cart WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartProdID='".mysql_real_escape_string($cpbpid)."'";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rs=mysql_fetch_assoc($result);
	if(is_null($rs["totquant"])) $thetotquant=0; else $thetotquant = $rs["totquant"];
	$sSQL="SELECT ".$WSP."pPrice FROM pricebreaks WHERE ".$thetotquant.">=pbQuantity AND pbProdID='".mysql_real_escape_string($cpbpid)."' ORDER BY " . ($WSP==""?"pPrice":str_replace(' AS ','',$WSP));
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs=mysql_fetch_assoc($result)) 
		$thepricebreak = $rs["pPrice"];
	else
		$thepricebreak = $origprice;
	$sSQL = "UPDATE cart SET cartProdPrice=".$thepricebreak." WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartProdID='".mysql_real_escape_string($cpbpid)."'";
	mysql_query($sSQL) or print(mysql_error());
}


if(@$_POST["mode"]=="delete")
{
	foreach(@$_POST as $objItem => $objValue)
	{
		if(substr($objItem,0,5)=="delet")
		{
			$sSQL="DELETE FROM cart WHERE cartID='" . (int)substr($objItem, 5) . "'";
			mysql_query($sSQL) or print(mysql_error());
			$sSQL="DELETE FROM cartoptions WHERE coCartID='" . (int)substr($objItem, 5) . "'";
			mysql_query($sSQL) or print(mysql_error());
		}
	}
}
if(@$_POST["mode"]=="add")
{
	if(@$estimateshipping==TRUE) $_SESSION["xsshipping"] = "";
	if(@isset($_SESSION["discounts"])) $_SESSION["discounts"] = "";
	mysql_query("UPDATE orders SET ordTotal=0,ordShipping=0,ordStateTax=0,ordCountryTax=0,ordHSTTax=0,ordHandling=0,ordDiscount=0,ordDiscountText='' WHERE ordSessionID='" . session_id() . "' AND ordAuthNumber=''") or print(mysql_error());
	$bExists = FALSE;
	if(trim(@$_POST["frompage"])!="") $_SESSION["frompage"]=$_POST["frompage"]; else $_SESSION["frompage"]="";
	if(@$_POST["quant"]=="" || ! is_numeric(@$_POST["quant"]))
		$quantity=1;
	else
		$quantity=abs((int)@$_POST["quant"]);
	foreach(@$_POST as $objItem => $objValue){ // Check if the product id is modified
		if(substr($objItem,0,4)=="optn"){
			$sSQL="SELECT optRegExp FROM options WHERE optID='" . mysql_real_escape_string($objValue) . "'";
//			echo $sSQL;
//			exit();
			$result2 = mysql_query($sSQL) or print(mysql_error());
			$rs=mysql_fetch_assoc($result2);
			$theexp = trim($rs["optRegExp"]);
			if($theexp != "" && substr($theexp, 0, 1) != "!"){
				$theexp = str_replace('%s', $theid, $theexp);
				if(strpos($theexp, " ") !== FALSE){ // Search and replace
					$exparr = split(" ", $theexp, 2);
					$theid = str_replace($exparr[0], $exparr[1], $theid);
				}else
					$theid = $theexp;
			}
			mysql_free_result($result2);
		}
		if(! $bExists) break;
	}
	$sSQL = "SELECT cartID FROM cart WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartProdID='" . $theid . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_assoc($result)){
		$bExists = TRUE;
		$cartID = $rs["cartID"];
		foreach(@$_POST as $objItem => $objValue){ // We have the product. Check we have all the same options
			if(substr($objItem,0,4)=="optn"){
				if(@$_POST["v" . $objItem] != ""){
					$sSQL="SELECT coID FROM cartoptions WHERE coCartID=" . $cartID . " AND coOptID='" . mysql_real_escape_string($objValue) . "' AND coCartOption='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["v" . $objItem]))) . "'";
					$result2 = mysql_query($sSQL) or print(mysql_error());
					if(mysql_num_rows($result2)==0) $bExists=FALSE;
					mysql_free_result($result2);
				}else{
					$sSQL="SELECT coID FROM cartoptions WHERE coCartID=" . $cartID . " AND coOptID='" . mysql_real_escape_string($objValue) . "'";
					$result2 = mysql_query($sSQL) or print(mysql_error());
					if(mysql_num_rows($result2)==0) $bExists=FALSE;
					mysql_free_result($result2);
				}
			}
			if(! $bExists) break;
		}
		if($bExists) break;
	}
	mysql_free_result($result);
	$sSQL = "SELECT ".getlangid("pName",1).",".$WSP."pPrice,pInStock,pWeight,pSell,pPricing_group FROM products WHERE pID='" . $theid . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(! ($rsStock = mysql_fetch_array($result))){
		$rsStock[getlangid("pName",1)]=$theid;
		$stockManage=0;
		$isInStock=FALSE;
		$outofstockreason=2;
	}
	mysql_free_result($result);
	if($stockManage != 0){
		if(($rsStock["pSell"] & 2)==2){
			$isInStock = true;
			foreach(@$_POST as $objItem => $objValue){
				if(substr($objItem,0,4)=="optn"){
					$sSQL="SELECT optStock FROM options INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND optID='" . mysql_real_escape_string($objValue) . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs = mysql_fetch_array($result)) 
						$isInStock = ($isInStock && ($rs["optStock"]+1000 >= $quantity));
					mysql_free_result($result);
				}
			}
			if($isInStock){ // Check cart
				$bestDate = time()+(60*60*24*62);
				foreach(@$_POST as $objItem => $objValue){
					$totQuant = 0;
					$stockQuant = 0;
					$actualstockQuant=0;
					if(substr($objItem,0,4)=="optn"){
						$sSQL = "SELECT cartQuantity,cartDateAdded,cartOrderID,optStock,optExtend_shipping,optMin,coID, optName FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND cartCompleted=0 AND coOptID='" . mysql_real_escape_string($objValue) . "'";
						//echo $sSQL;
						$result = mysql_query($sSQL) or print(mysql_error());
						$extend_shipping_out='';
						if(mysql_num_rows($result)>0){
							$rs = mysql_fetch_array($result);							
							$stockQuant = $rs["optStock"]+1000;//adds 1000 to make it never out of stock
							do{
								$totQuant += $rs["cartQuantity"];
								if((int)$rs["cartOrderID"]==0 && strtotime($rs["cartDateAdded"]) < $bestDate) $bestDate = strtotime($rs["cartDateAdded"]);
							}while($rs = mysql_fetch_array($result));
							if(($totQuant+$quantity) > $stockQuant){
								$isInStock=false;
								$outofstockreason=1;
							}
						}
						mysql_free_result($result);
					}
				}
			}
		}else{
			if($isInStock = (($rsStock["pInStock"]-$quantity) >= 0)){ // Check cart
				$totQuant = 0;
				$bestDate = time()+(60*60*24*62);
				$sSQL = "SELECT cartQuantity,cartDateAdded,cartOrderID FROM cart WHERE cartCompleted=0 AND cartProdID='" . $theid . "'";
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs = mysql_fetch_array($result)){
					$totQuant += $rs["cartQuantity"];
					if((int)$rs["cartOrderID"]==0 && strtotime($rs["cartDateAdded"]) < $bestDate) $bestDate = strtotime($rs["cartDateAdded"]);
				}
				mysql_free_result($result);
				if(($rsStock["pInStock"]-($totQuant+$quantity)) < 0){
					$isInStock = FALSE;
					$outofstockreason=1;
				}
			}
		}
	}
	//check to see if quanity is less than min level added by Blake April 3, 2006
	$extend_shipping_out='';
	foreach(@$_POST as $objItem => $objValue){
		if(substr($objItem,0,4)=="optn"){
			if(trim(@$_POST["v" . $objItem])==""){		
				$sSQL = "SELECT cartQuantity,cartDateAdded,cartOrderID,optStock,optExtend_shipping,optMin,coID, optName FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND cartCompleted=0 AND coOptID='" . mysql_real_escape_string($objValue) . "'";
				//echo $sSQL;
				$result = mysql_query($sSQL) or print(mysql_error());
				if(mysql_num_rows($result)>0){
					$rs = mysql_fetch_array($result);							
					$stockQuant = $rs["optStock"]+1000;//adds 100 to make it never out of stock
					$actualstockQuant = $rs["optStock"];//this is the actual stock available
					$extend_shipping = $rs["optExtend_shipping"];//extends shipping time, displayed in the cart
					$min = $rs["optMin"];//sets how many in stock above zero the extend_shipping is displayed in the cart 
					$coID = $rs["coID"];
					$optname = $rs["optName"];
					do{
						$totQuant += $rs["cartQuantity"];
						if((int)$rs["cartOrderID"]==0 && strtotime($rs["cartDateAdded"]) < $bestDate) $bestDate = strtotime($rs["cartDateAdded"]);
					}while($rs = mysql_fetch_array($result));
					// blake
					//echo 'total qty='.$totQuant.' new qty='.$quantity.' min='.$min.' ='.$actualstockQuant.' name='.$optname;
					if(($totQuant+$quantity+$min)> $actualstockQuant){
						$sql_co="UPDATE cartoptions SET coExtendShipping='$extend_shipping' WHERE coCartID=".$cartID." AND coOptID=".$objValue;
						$extend_shipping_out[$objValue]=$extend_shipping;
						//echo '<br />Update='.$sql_co;
						mysql_query($sql_co);
					}
				} else {
					$sSQL="SELECT optStock,optExtend_shipping,optMin,optExtend_shipping,optName FROM options INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND optID='" . mysql_real_escape_string($objValue) . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs3 = mysql_fetch_array($result)) {
						if($quantity+$rs3["optMin"] > $rs3["optStock"]) {
							//echo $rs3["optName"].' qty='.$quantity.' min='.$rs3["optMin"].'='.$rs3["optStock"];
							$extend_shipping_out[$objValue]=$rs3["optExtend_shipping"];
							//echo '<br />first option in cart. Shipping='.$extend_shipping_out[$objValue];
						}
					}
				}
				mysql_free_result($result);
			}
		}
	}
	//end added
	if($isInStock){
		//wholesale prices Added by Blake 6-6-06 
		$pPrice_adj=1;
		if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rsStock["pPricing_group"]);//(customer ID,tier,pricing group)
		//
		if($bExists){
			$sSQL = "UPDATE cart SET cartQuantity=cartQuantity+" . $quantity . " WHERE cartID=" . $cartID;
			mysql_query($sSQL) or print(mysql_error());
		}else{
			$sSQL = "INSERT INTO cart (cartSessionID,cartProdID,cartQuantity,cartCompleted,cartProdName,cartProdPrice,cartOrderID,cartDateAdded) VALUES (";
			$sSQL .= "'" . session_id() . "',";
			$sSQL .= "'" . $theid . "',";
			$sSQL .= $quantity . ",";
			$sSQL .= "0,";
			$sSQL .= "'" . mysql_real_escape_string($rsStock[getlangid("pName",1)]) . "',";
			$sSQL .= "'" . $rsStock["pPrice"]*$pPrice_adj . "',";
			$sSQL .= "0,";
			$sSQL .= "'" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "')";
			mysql_query($sSQL) or print(mysql_error());
			$cartID = mysql_insert_id();
			foreach(@$_POST as $objItem => $objValue){
				if(substr($objItem,0,4)=="optn"){
					if(trim(@$_POST["v" . $objItem])==""){						
						$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)."," . $OWSP . "optPriceDiff,optWeightDiff,optType,optFlags FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($objValue) . "'";
						$result = mysql_query($sSQL) or print(mysql_error());
						if($rs = mysql_fetch_array($result)){
							if(abs($rs["optType"]) != 3){
								$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coExtendShipping,coPriceDiff,coWeightDiff) VALUES (" . $cartID . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string($rs[getlangid("optName",32)]) ."','" . $extend_shipping_out[$objValue] . "',";
								if(($rs["optFlags"]&1)==0) $sSQL .= $rs["optPriceDiff"] . ","; else $sSQL .= round(($rs["optPriceDiff"] * $rsStock["pPrice"])/100.0, 2) . ",";
								if(($rs["optFlags"]&2)==0) $sSQL .= $rs["optWeightDiff"] . ")"; else $sSQL .= multShipWeight($rsStock["pWeight"],$rs["optWeightDiff"]) . ")";
								//echo $sSQL;
							}else
								$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartID . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','',0,0)";
							mysql_query($sSQL) or print(mysql_error());
						}
						mysql_free_result($result);
					}else{
						//echo 'in2';
						$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)." FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($objValue) . "'";
						$result = mysql_query($sSQL) or print(mysql_error());
						$rs = mysql_fetch_array($result);
						$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartID . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string(unstripslashes(trim(@$_POST["v" . $objItem]))) . "',0,0)";
						mysql_query($sSQL) or print(mysql_error());
						mysql_free_result($result);
					}
				}
			}
		}
		checkpricebreaks($theid,$rsStock["pPrice"]*$pPrice_adj);
	}else{
?>

	<!-- If it's not in stock -->
	
	<div>That item is not in stock</div>

<?php
	}
}
?>

	<!-- Show the cart -->
	
<style type="text/css">
<!--
-->
</style>
	
<?php
	$alldata="";
	$sSQL = "SELECT cartID,cartProdID,cartProdName,cartProdPrice,cartQuantity,pWeight,pShipping,pShipping2,pExemptions,pSection,pDims,isSet,topSection FROM cart LEFT JOIN products ON cart.cartProdID=products.pID LEFT OUTER JOIN sections ON products.pSection=sections.sectionID WHERE cartCompleted=0 AND cartSessionID='" .  session_id() . "' ORDER BY cartID";
	$result = mysql_query($sSQL) or print(mysql_error());
?>
	
<?php
	if(!empty($_SESSION['os'])){
		$sSQL1 = "SELECT * FROM coupons WHERE ";
		$sSQL1 .="cpnNumber='".$_SESSION['os']."'";
		$sSQL1 .= $addor . " AND (cpnSitewide=1 OR cpnSitewide=2) AND cpnNumAvail>0 AND cpnEndDate>='" . date("Y-m-d",time()) ."' AND cpnIsCoupon=1";
		if(!empty($WSP)) $sSQL1 .= " AND (cpnIsWholesale=1)"; 
		else $sSQL1 .= " AND (cpnIsWholesale=0)";
		$sSQL1 .= " ORDER BY cpnID";
		//echo $sSQL1;
		$result21 = mysql_query($sSQL1) or print(mysql_error());
		if(mysql_num_rows($result21) > 0){ ?>
		  <div style="padding:2px; font-size:10px;"><strong>Discounts:</strong><?php //print $xxDsProd?><br /><font color="#FF0000" size="1">
		  <?php	while($rs2=mysql_fetch_assoc($result21)){
					print $rs2['cpnName']." <br />";
				} ?></font></div>
	<?php
		}
		mysql_free_result($result21);
	}	
		$totaldiscounts = 0;
		$changechecker = "";
		$index = 0;		
		
		if(mysql_num_rows($result)<=0) {
?>
			<div id="emptyCart">Your cart is empty.</div>
<?
		}
		echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		
		while($alldata=mysql_fetch_assoc($result))
		{
			$index++;
			$changechecker .= 'if(document.checkoutform.quant' . $alldata["cartID"] . ".value!=" . $alldata["cartQuantity"] . ") dowarning=true;\n";
			$theoptions = "";
			$theoptionspricediff = 0;
			$sSQL = "SELECT coOptGroup,coCartOption,coPriceDiff,coWeightDiff FROM cartoptions WHERE coCartID=" . $alldata["cartID"] . " ORDER BY coID";
			$opts = mysql_query($sSQL) or print(mysql_error());
			$optPriceDiff=0;
			
			if($cntr > 3) {
				$cntr = 0;
			}
			
			if($alldata['isSet']=='yes') {
				$isSet = true;
				$cntr++;
			}else{
				$isSet = false;
			}
			if(mysql_num_rows($opts) > 0) {	
				while($rs=mysql_fetch_assoc($opts))
				{
					if($isSet) {
						if($cntr==1) {
		?>
						<tr>
							<td align="left" valign="middle" style="font-size: 9px; font-weight: bold; padding: 2px 3px"><?=$rs["coOptGroup"].' - '.$rs["coCartOption"]?></td>
							<td width="10" valign="middle" style="text-align: center"><?=$alldata['cartQuantity']?></td>
							<td valign="bottom" style="margin: 0; padding: 0; height: auto">
								<!--<div style="height: 100%">
									<div><img src="/lib/images/brace/top.gif" width="9" height="5" /></div>
									<div style="height: 100%; background: url(/lib/images/brace/top_middle.gif) repeat-y"></div>
								</div>-->
								<table border="0" cellspacing="0" cellpadding="0" height="100%" style="margin: 0; padding: 0">
									<tr valign="bottom">
										<td height="5px" style="height: 5px"><img src="/lib/images/brace/top.gif" width="9" height="5" alt="" /></td>
									</tr>
									<tr>
										<td style="background: url(/lib/images/brace/top_middle.gif) repeat-y; height: auto">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td width="10" valign="middle" style="text-align: right">&nbsp;</td>
						</tr>
		<?
						}elseif($cntr==2){
		?>
						<tr>
							<td align="left" valign="middle" style="font-size: 9px; font-weight: bold; padding: 2px 3px"><?=$rs["coOptGroup"].' - '.$rs["coCartOption"]?></td>
							<td width="10" valign="middle" style="text-align: center"><?=$alldata['cartQuantity']?></td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0" height="100%">
									<tr>
										<td style="background: url(/lib/images/brace/top_middle.gif) repeat-y; height: auto">&nbsp;</td>
									</tr>
									<tr>
										<td height="8"><img src="/lib/images/brace/middle.gif" width="14" height="8" alt="" /></td>
									</tr>
									<tr>
										<td style="background: url(/lib/images/brace/top_middle.gif) repeat-y; height: auto">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td width="10" valign="middle" style="text-align: right"><input type="image" src="/lib/images/trash_can.gif" name="delet<?=$alldata['cartID']?>" onclick="delItem(this.name);" /></td>
						</tr>
		<?
						}elseif($cntr==3){
		?>
						<tr>
							<td align="left" valign="middle" style="font-size: 9px; font-weight: bold; padding: 2px 3px"><?=$rs["coOptGroup"].' - '.$rs["coCartOption"]?></td>
							<td width="10" valign="middle" style="text-align: center"><?=$alldata['cartQuantity']?></td>
							<td valign="bottom">
								<table border="0" cellspacing="0" cellpadding="0" height="100%">
									<tr>
										<td style="background: url(/lib/images/brace/top_middle.gif) repeat-y; height: auto">&nbsp;</td>
									</tr>
									<tr height="6" valign="top">
										<td height="6" style="height: 6px"><img src="/lib/images/brace/bottom.gif" height="6" width="9" alt="" /></td>
									</tr>
								</table>
							</td>
							<td width="10" valign="middle" style="text-align: right">&nbsp;</td>
						</tr>
		<?php
						}
						$cntr++;
					}else{
	?>				
					<tr>
						<td align="left" valign="middle" style="font-size: 9px; font-weight: bold; padding: 2px 3px"><?=$rs["coOptGroup"].' - '.$rs["coCartOption"]?></td>
						<td width="10" valign="middle" style="text-align: center"><?=$alldata['cartQuantity']?></td>
						<td>&nbsp;</td>
						<td width="10" valign="middle" style="text-align: right">
							<input type="image" src="/lib/images/trash_can.gif" name="delet<?=$alldata['cartID']?>" onclick="delItem(this.name);" />
						</td>
					</tr>
	<?php
					}
				}
			} else { ?>
			<tr>
				<td align="left" valign="middle" style="font-size: 9px; font-weight: bold; padding: 2px 3px"><?=$alldata['cartProdName']?></td>
				<td width="10" valign="middle" style="text-align: center"><?=$alldata['cartQuantity']?></td>
				<td>&nbsp;</td>
				<td width="10" valign="middle" style="text-align: right">
					<input type="image" src="/lib/images/trash_can.gif" name="delet<?=$alldata['cartID']?>" onclick="delItem(this.name);" />
				</td>
			</tr>
			<? }
			mysql_free_result($opts);
		}
		echo '</table>';
?>