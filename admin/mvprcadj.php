<?php
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');

if (!empty($pa_id)) {
	// Receive 'move up/down' and it's position '2'
	if ( $action=='moveup' ) {
		if ($position==1) {
			die('Not possible to move first element up one.');
		}
		else {
			$newPosition = $position - 1;
		}
	}
	else if ($action=='movedown') {
		$newPosition = $position + 1;
	}
	
//	echo 'Position = '.$position.'<br />';
//	echo 'New Position = '.$newPosition.'<br />';
//	exit();
	
	// Perform swap
	$qry2 = "SELECT * FROM price_adj WHERE ordID = '$pa_ordID' AND ordering = '$newPosition'";
	$res2 = mysql_query($qry2);
	if(!$res2)
	{
		die('Problem selecting price adjustment with new position. Reason: '.mysql_error().'<br />'.$qry2);
	}
	$row2 = mysql_fetch_assoc($res2);
	
	$qry1 = "UPDATE price_adj 
			 SET ordering = '$newPosition'
			 WHERE id = '$pa_id'";
	$res1 = mysql_query($qry1);
	if(!$res1)
	{
		die('Problem updating price adjustment with new position. Reason: '.mysql_error().'<br />'.$qry1);
	}
	
	$qry3 = "UPDATE price_adj 
			 SET ordering = '$position'
			 WHERE id = '".$row2['id']."'";
	$res3 = mysql_query($qry3);
	if(!$res3)
	{
		die('Problem updating price adjustment. Reason: '.mysql_error().'<br />'.$qry3);
	}
	
	header('Location: /admin/orders.php?id='.$pa_ordID.'#prc_adj');
	exit();
}

?>