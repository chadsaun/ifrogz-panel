<?php
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/functions.php');
$WSP="";
$OWSP="";
if (@$_SESSION["clientUser"] != ""){
	if (($_SESSION["clientActions"] & 8) == 8){
		$WSP = "pWholesalePrice AS ";
		if (@$wholesaleoptionpricediff==TRUE) $OWSP = 'optWholesalePriceDiff AS ';
		if (@$nowholesalediscounts==TRUE) $nodiscounts=TRUE;
	}
	if (($_SESSION["clientActions"] & 16) == 16){
		$WSP = $_SESSION["clientPercentDiscount"] . "*pPrice AS ";
		if(@$wholesaleoptionpricediff==TRUE) $OWSP = $_SESSION["clientPercentDiscount"] . '*optPriceDiff AS ';
		if(@$nowholesalediscounts==TRUE) $nodiscounts=TRUE;
	}
}

$product_id=$_POST['product_id'];
$media_type=$_POST['media_type'];

// Get Section Working Name
$section = '';
$sql = "SELECT s.sectionWorkingName
		FROM sections s, products p
		WHERE p.pSection = s.sectionID
		AND p.pID = '$product_id'";
$res = mysql_query($sql);
if (mysql_num_rows($res) > 0) {
	$row = mysql_fetch_assoc($res);
	$section = $row['sectionWorkingName'];
}

//echo '$media_type='.$media_type;
//echo 'day='.$_POST['day'];
// TOP PRODUCTS
if(strstr($product_id,'-Top')) {
	$sql="SELECT pOrder,pID,".$WSP."pPrice,pListPrice, pPricing_group FROM products WHERE pID='".$_POST['product_id']."'";
	$result = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($result);
	$pPrice_adj=1;
	if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$row["pPricing_group"]);
	$top=explode('_',$row['pID']);
	$cols=$top[1];
	if($cols=='')$cols=10;
	if(substr($row['pOrder'],0,1)==1) $type='wrap';
	elseif(substr($row['pOrder'],0,1)==2) $type='band';
	elseif(substr($row['pOrder'],0,1)==3) {$type='screen ';$td_height='60px';}
	$days=7;
	if($_POST['day']!='') $days=$_POST['day'];
	//echo 'days='.$days;
	$pOrder=substr($row['pOrder'],0,1);
	$pPrice=number_format(($row['pPrice']*$pPrice_adj),2);
	$pListPrice = number_format(($row['pListPrice']*$pPrice_adj),2);
	
	$prod_type=split('-',$product_id);
	$top_prods=getTopProds($type,$days,0,$cols,$prod_type[0]); 
	$this_price=$pPrice;
	$this_list_price = $pListPrice;
	$this_type=$pOrder;
	$this_price_diff='';
	
	?>
	<div style="font-size:12px; padding-bottom:10px;">
	<? if($days==7){?>
		Last Week |
	<? } else { ?>
		<a href="#" onclick="load_options('<?=$product_id?>','','flash',7,<?=substr($row['pOrder'],0,1)?>,<?=$pPrice?>); return false;">Last Week</a> |
	<? }if($days==14){?>
		Last 2 Weeks |
	<? } else { ?>
		<a href="#" onclick="load_options('<?=$product_id?>','','flash',14,<?=substr($row['pOrder'],0,1)?>,<?=$pPrice?>); return false;">Last 2 Weeks</a> |
	<? }if($days==30){?>
		Last Month |
	<? } else { ?>
		<a href="#" onclick="load_options('<?=$product_id?>','','flash',30,<?=substr($row['pOrder'],0,1)?>,<?=$pPrice?>); return false;">Last Month</a> |
	<? }if($days==90){?>
		Last 3 Months
	<? } else { ?>	
		<a href="#" onclick="load_options('<?=$product_id?>','','flash',90,<?=substr($row['pOrder'],0,1)?>,<?=$pPrice?>); return false;">Last 3 Months</a> 
	<? } ?>
	  <? 
	  //showarray($top_prods);
	  $cnt=count($top_prods);
	  for($i=0;$i<$cnt;$i++) { ?>
			<div style="position:relative;height:<?=$td_height?>; float:left; width:<?=$td_height?>; margin:2px 6px;" onclick="set_Options('<?=$top_prods[$i][$type]['optID']?>','<?=$top_prods[$i][$type]['option']?>','','<?=$top_prods[$i][$type]['optColor']?>','<?=$pOrder?>','<?=$top_prods[$i][$type]['pID']?>','<?=$top_prods[$i][$type]['styleID']?>','<?=$media_type?>',<?=$pPrice*$pPrice_adj?>,'<?=$top_prods[$i][$type]['optPriceDiff']?>',<?=$pListPrice*$pPrice_adj?>);">
			<? if($pOrder==3){
			 	$file_name= '../'.$top_prods[$i][$type]['pImage'].'/'.$top_prods[$i][$type]['style'].'.jpg';?>
					<div style="position:absolute; top:0px;left:0px; z-index:0;"><a href="javascript:void(0)"><img class="images_<?=$pOrder?>" src="<?=$file_name?>" border="0" alt="<?=$top_prods[$i][$type]['option'] ?>" /></a></div>
					<div style="border:0px;position:absolute;top:0px;left:0px;z-index:1;" id="<?=$top_prods[$i][$type]['pID']?>_<?=$top_prods[$i][$type]['option']?>" class="type_<?=$pOrder?>"></div>
			<? 
			} else { ?>				
				<a href="javascript:void(0)" alt="<?=$top_prods[$i][$type]['option']?>" ><div style="height:26px;width:30px;background: #<?=$top_prods[$i][$type]['optColor']?> url('/lib/images/circle.gif') no-repeat center;"></div></a>
			<? }?>
			</div>
	  <? } ?>
</div>
<?	
}
// CUSTOM SCREENZ
elseif(strstr($product_id,'-Custom')) { ?>
	<input name="prodID" id="prodID" type="hidden" value="<?=$product_id?>" />
	<?
	$flash_img_size=split('-',$product_id);
	if($flash_img_size[0]=='Z')$flash_img_size[0]='W';
	$sql_price="SELECT pPrice,pListPrice,pOrder,pPricing_group,pDescription FROM products WHERE pID='".$product_id."'";
	$result_price=mysql_query($sql_price);
	$row_price=mysql_fetch_assoc($result_price);
	$pPrice_adj=1;
	if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$row_price["pPricing_group"]);
	$this_price=number_format(($row_price['pPrice']*$pPrice_adj),2);
	$this_list_price=number_format(($row_price['pListPrice']*$pPrice_adj),2);
	$porder=$row_price['pOrder'];
	$this_type=substr($row_price['pOrder'],0,1);
	$this_price_diff='';
	?>
	<div style="clear:both; position:relative; margin-bottom:6px;">
		<div style="position:absolute; right:6px;">
		<a href="#" onclick="load_options('<?=$product_id?>','','flash','','','<?=$this_price?>'); return false;"><img src="/lib/images/refresh.gif" alt="refresh" /></a>
		</div>
		<div>
			<a href="#" style="font-size:14px;" alt="Upload Custom Image" onclick="window.open('/admin/fileselect.php?return=<?=$product_id?>','Custom_Screen','height=720,width=720,scrollbars=yes,resizable=yes')" >Upload Custom Image</a>
		</div>
	</div>
	<?
	$sql="SELECT id,org_img_name,display_image FROM uploaded_images WHERE img_status <> 'deleted' AND flipped<>'' AND sessionid='".session_id()."' ORDER BY id DESC";
	$result=mysql_query($sql);
	$num_rows=mysql_num_rows($result);
	while($row=mysql_fetch_assoc($result)) { 
	?>
		<div style=" position:relative; float:left; margin:2px 3px 4px 2px;padding:2px 0px 2px 0px;text-align:center; background-color:#FFFFFF; width:136px; font-size:12px;">
			<div style="z-index:0;"><img src="/lib/images/imguploads/img_screen/<?=$row['display_image']?>.gif" onclick="set_Options('3000','<?=$row['org_img_name']?>','<?=$row['id']?>','','<?=substr($porder,0,1)?>','<?=$product_id?>','<?='l_'.$row['display_image'].'.gif'?>','<?=$media_type?>','<?=$this_price?>','','<?=$this_list_price?>');" /></div>
			<div style="border:0px;position:absolute;top:0px;left:6px;z-index:1;" id="<?=$product_id.'_'.$row['id']?>" class="type_<?=substr($porder,0,1)?>"></div>
			<?=$row['org_img_name']?> 				
<!--			<a href="../../design/loadflash.php?photoName=<?=$row['display_image']?>" target="_blank" onclick="window.open(this.href,'Custom_Screen','height=700,width=700',false); return false;">edit</a>
-->			<br />
			<input id="optn_<?=$row['id']?>" name="optn0" type="hidden" value="3000">
			<input id="voptn_<?=$row['id']?>" name="voptn0" type="hidden" value="<?=$row['id']?>">
<!--			<input id="btn<?=$row['id']?>" name="add" src="/lib/images/addtocart2.gif" type="image" onClick="addCustomScreen(3000,'<?=$row['id']?>_<?=$row['org_img_name']?>')">
			<div><?=money_format("$%.2n",$price)?></div>-->			
		</div>		
	<? } 
	if($num_rows==0) echo '<div style="margin:10px 5px 5px 5px;padding:4px 0px 4px 0px; text-align:center; background-color:#FFFFFF; float:left; border:1px dashed #CCCCCC; width:150px;"><img src="/lib/images/after_upload.gif" alt="Your image will go here after upload." /></div>';
	if(!empty($row_price["pDescription"])) echo '<div style="font-size: 12px;clear:both;">'.$row_price["pDescription"]."</div>\n";

	?>
	<?
// END CUSTOM SCREENZ

// START CUSTOM HEADPHONES
} else if (in_array($section, array('Custom NervePipes','Custom Fallout','Custom Hype'))) {
	$rowcounter=0;
	$sql1 = "SELECT poOptionGroup, pImage, ".getlangid("pName",1).", p.pOrder, p.pDescription, ".$WSP."pPrice,pListPrice,p.pPricing_group, p.pSection, pTabName 
			 FROM prodoptions po, products p WHERE po.poProdID=p.pID AND poProdID='".$product_id."'";
	//echo $sql1;
	$result1 = mysql_query($sql1) or print(mysql_error());
	$rs1=mysql_fetch_array($result1);
	$pPrice_adj=1;
	if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs1["pPricing_group"]);
	$sSQL="SELECT optID,".getlangid("optName",32).",".getlangid("optGrpName",16)."," . $OWSP . "optPriceDiff,optGrpWorkingName,optType,optFlags,optStock,optPriceDiff,optStyleID,optColor 
		   FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID 
		   WHERE optDisplay = 'yes' 
		   AND optStock > optDisplay_point 
		   AND optGroup = " . $rs1["poOptionGroup"];
	if(stristr($product_id,'-Sale')) $sSQL.=" AND optPriceDiff!=0";
	else $sSQL.=" AND optPriceDiff=0";
	$sSQL.=" ORDER BY optID";
	//echo $sSQL;
	$result = mysql_query($sSQL) or print(mysql_error());
	$result2=mysql_query($sSQL) or print(mysql_error());
	$num_options=mysql_num_rows($result);
	$rs2=mysql_fetch_array($result);
		
	if($num_options>0){
		$this_type=substr($rs1['pOrder'],0,1);
		
		//Display what is in pDescription
		if(!empty($rs1["pDescription"]) && !empty($rs1["pTabName"])) $aOption[1][$index] .='<div class="option_desc">'.$rs1["pDescription"]."</div>\n";
		$cnt=0;
		//screen first number
		$strpos=strpos($product_id,"-");
		$firstnum=substr($product_id,$strpos+1);
		do {
			$pattern=$rs2['optColor'];
			$disc_type='';
			if($rs2['optFlags']==1) $disc_type='%';
			$this_price=number_format(($rs1['pPrice']*$pPrice_adj),2);
			$this_list_price=number_format(($rs1['pListPrice']*$pPrice_adj),2);
			
			$this_price_diff=$rs2['optPriceDiff'].$disc_type;
			
			$isArtwork = false;
			if (!empty($rs1['pTabName'])) {
				$isArtwork = true;
			}
			
			$path = "http://".$_SERVER['HTTP_HOST']."/lib/images/flash/".$product_id."/";
			if ($isArtwork) {
				$path .= "ellipse/";
			}
			
			$aOption[1][$index] .='        <div class="option_layout" onclick="set_Options2(\''.$rs2[getlangid("optID",32)].'\',\''.$rs2[getlangid("optName",32)].'\',\'\',\''.$rs2[getlangid("optColor",32)].'\',\''.$rs1['pOrder'].'\',\''.$product_id.'\',\''.$rs2['optStyleID'].'\',\''.$media_type.'\',\''.$this_price.'\',\''.$this_price_diff.'\',\''.$this_list_price.'\',\''.$isArtwork.'\',\''.$path.'\');">'; 
			$aOption[1][$index] .='           <div style="position:relative;">';
			//set file name depending on type 1,2,3
			if($isArtwork){
				$file_name = "/".$rs1['pImage'].'/'.$rs2['optStyleID'].'.jpg';
				$aOption[1][$index] .='<div style="position:absolute; top:0px;left:0px; z-index:0;"><a href="javascript:void(0)"><img class="images_'.substr($rs1['pOrder'],0,1).'" src="'.$file_name.'" border="0" alt="' . str_replace('"','&quot;',strip_tags($rs2[getlangid("optName",1)])) . '" /></a></div>';
				$aOption[1][$index] .='<div style="border:0px;position:absolute;top:0px;left:0px;z-index:1;" id="'.$product_id.'_'.$rs2[getlangid("optName",32)].'" class="type_'.substr($rs1['pOrder'],0,1).'"></div>';
			} else {				
				if(strstr($rs2['optColor'],'t_')){
					$aOption[1][$index] .='<a href="javascript:void(0)" alt="'. str_replace('"','&quot;',strip_tags($rs2[getlangid("optName",1)])) .'" ><div style="position:relative;"><div style="padding:0px;margin:0px;height:34px;width:34px; border:0px;background: url(\'/lib/images/patterns/'.$rs2['optColor'].'\') no-repeat center;position:absolute; top:0px; left:0px;"><img src="/lib/images/circle2.gif" /></div><div style="position:absolute; top:0px; left:0px;" id="'.$product_id.'_'.$rs2[getlangid("optName",32)].'" class="type_'.substr($rs1['pOrder'],0,1).'"></div></div></a>';
				} else {
					$aOption[1][$index] .='<a href="javascript:void(0)" alt="'. str_replace('"','&quot;',strip_tags($rs2[getlangid("optName",1)])) .'" ><div id="'.$product_id.'_'.$rs2[getlangid("optName",32)].'" class="type_'.substr($rs1['pOrder'],0,1).'" style="padding:0px;margin:0px;height:34px;width:34px; border:0px; background: #'.$rs2['optColor'].' url(/lib/images/circle2.gif) no-repeat center;"></div></a>';
				}
			}
			$aOption[1][$index] .= "</div></div>\n";
			$cnt++;
		} while($rs2=mysql_fetch_array($result));
		$close='';
	} else echo 'We are currently out of stock!';
	
	if (!empty($rs1["pDescription"]) && $this_type != 3) {
		$aOption[1][$index] .='<div class="option_desc">'.$rs1["pDescription"]."</div>\n";
	}
	
	$aOption[1][$index] .= '</div>'."\n";
	print $aOption[1][$index];

// EVERYTHING ELSE
} else {
	$rowcounter=0;
	$sql1 = "SELECT poOptionGroup, pImage, ".getlangid("pName",1).", p.pOrder, p.pDescription, ".$WSP."pPrice,pListPrice,p.pPricing_group, p.pSection 
			 FROM prodoptions po, products p WHERE po.poProdID=p.pID AND poProdID='".$product_id."'";
	//echo $sql1;
	$result1 = mysql_query($sql1) or print(mysql_error());
	$rs1=mysql_fetch_array($result1);
	$pPrice_adj=1;
	if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs1["pPricing_group"]);
	$sSQL="SELECT optID,".getlangid("optName",32).",".getlangid("optGrpName",16)."," . $OWSP . "optPriceDiff,optGrpWorkingName,optType,optFlags,optStock,optPriceDiff,optStyleID,optColor FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optDisplay='yes' AND optStock > optDisplay_point AND optGroup=" . $rs1["poOptionGroup"];
	if(stristr($product_id,'-Sale')) $sSQL.=" AND optPriceDiff!=0";
	else $sSQL.=" AND optPriceDiff=0";
	$sSQL.=" ORDER BY optID";
	//echo $sSQL;
	$result = mysql_query($sSQL) or print(mysql_error());
	$result2=mysql_query($sSQL) or print(mysql_error());
	$num_options=mysql_num_rows($result);
	$rs2=mysql_fetch_array($result);
	
	if($num_options>0){
		//bandz and wrapz number of columns
		$this_type=substr($rs1['pOrder'],0,1);
		// preload images
		
		
		//Display what is in pDescription
		if(!empty($rs1["pDescription"]) && $this_type==3) $aOption[1][$index] .='<div class="option_desc">'.$rs1["pDescription"]."</div>\n";
		$cnt=0;
		//screen first number
		$strpos=strpos($product_id,"-");
		$firstnum=substr($product_id,$strpos+1);
		do {
			$pattern=$rs2['optColor'];
			$disc_type='';
			if($rs2['optFlags']==1) $disc_type='%';
			$this_price=number_format(($rs1['pPrice']*$pPrice_adj),2);
			$this_list_price=number_format(($rs1['pListPrice']*$pPrice_adj),2);
			
			$this_price_diff=$rs2['optPriceDiff'].$disc_type;

			$aOption[1][$index] .='        <div id="option_layout_'.$this_type.'" onclick="set_Options(\''.$rs2[getlangid("optID",32)].'\',\''.$rs2[getlangid("optName",32)].'\',\'\',\''.$rs2[getlangid("optColor",32)].'\',\''.$rs1['pOrder'].'\',\''.$product_id.'\',\''.$rs2['optStyleID'].'\',\''.$media_type.'\',\''.$this_price.'\',\''.$this_price_diff.'\',\''.$this_list_price.'\');">'; 
			$aOption[1][$index] .='           <div style="position:relative;">';
			//set file name depending on type 1,2,3
			if(substr($rs1['pOrder'],0,1)==3){
				$file_name= '../'.$rs1['pImage'].'/'.$rs2['optStyleID'].'.jpg';
				$aOption[1][$index] .='<div style="position:absolute; top:0px;left:0px; z-index:0;"><a href="javascript:void(0)"><img class="images_'.substr($rs1['pOrder'],0,1).'" src="'.$file_name.'" border="0" alt="' . str_replace('"','&quot;',strip_tags($rs2[getlangid("optName",1)])) . '" /></a></div>';
				$aOption[1][$index] .='<div style="border:0px;position:absolute;top:0px;left:0px;z-index:1;" id="'.$product_id.'_'.$rs2[getlangid("optName",32)].'" class="type_'.substr($rs1['pOrder'],0,1).'"></div>';
			} else {				
				if(strstr($rs2['optColor'],'t_')){
					$aOption[1][$index] .='<a href="javascript:void(0)" alt="'. str_replace('"','&quot;',strip_tags($rs2[getlangid("optName",1)])) .'" ><div style="position:relative;"><div style="padding:0px;margin:0px;height:34px;width:34px; border:0px;background: url(/lib/images/patterns/'.$rs2['optColor'].') no-repeat center;position:absolute; top:0px; left:0px;"><img src="/lib/images/circle2.gif" /></div><div style="position:absolute; top:0px; left:0px;" id="'.$product_id.'_'.$rs2[getlangid("optName",32)].'" class="type_'.substr($rs1['pOrder'],0,1).'"></div></div></a>';
				} else {
					$aOption[1][$index] .='<a href="javascript:void(0)" alt="'. str_replace('"','&quot;',strip_tags($rs2[getlangid("optName",1)])) .'" ><div id="'.$product_id.'_'.$rs2[getlangid("optName",32)].'" class="type_'.substr($rs1['pOrder'],0,1).'" style="padding:0px;margin:0px;height:34px;width:34px; border:0px;background: #'.$rs2['optColor'].' url(/lib/images/circle2.gif) no-repeat center;"></div></a>';
				}
			}
			$aOption[1][$index] .= "</div></div>\n";
			$cnt++;
		} while($rs2=mysql_fetch_array($result));
		$close='';
	} else echo 'We are currently out of stock!';
	if (!empty($rs1["pDescription"]) && $this_type != 3) {
		$aOption[1][$index] .='<div class="option_desc">'.$rs1["pDescription"]."</div>\n";
	}
	// Display any 'Other Images'
	$aOption[1][$index] .=  "<!-- chad start -->";
	if (!empty($rs1["pSection"]) && $this_type != 3) {
		$sql_oi = "SELECT * FROM sections WHERE sectionID = " . $rs1["pSection"];
		$res_oi = mysql_query($sql_oi);
		if ($res_oi) {
			$row_oi = mysql_fetch_assoc($res_oi);
			$aOption[1][$index] .=  "<!--  chad  sectionImagesOther is ".$row_oi['sectionImagesOther']." -->";
			if (!empty($row_oi['sectionImagesOther'])) {
				$aOtherImages = explode(",", $row_oi['sectionImagesOther']);
				ob_start();
				print_r($aOtherImages);
				$tmp = ob_get_contents();
				ob_end_clean();
				$aOption[1][$index] .= "<!-- " . $tmp . " -->";
				$aOption[1][$index] .= "<div class='other-images'>\n";
				for ($i = 0; $i < count($aOtherImages); $i++) {
					$pathInfo = pathinfo(trim($aOtherImages[$i]));
					$imgPath = trim($aOtherImages[$i]);
					$thumbPath = $pathInfo['dirname'] . "/thumbs/" . $pathInfo['basename'];
					$thumbWidth = 75;
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $thumbPath)) {
						$thumbInfo = getimagesize($_SERVER['DOCUMENT_ROOT'] . $thumbPath);
						$thumbWidth = $thumbInfo[0];
					}
					$aOption[1][$index] .= "<!-- chad " . $_SERVER['DOCUMENT_ROOT'] . $imgPath. " -->";
					if (file_exists($_SERVER['DOCUMENT_ROOT'] . $imgPath)) {	
						$aOption[1][$index] .= '<a href="../'.$imgPath.'" rel="lightbox"><img src="../'.$thumbPath.'" width="'.$thumbWidth.'" /></a>';
					} else {
						$aOption[1][$index] .= "<!-- \n File not found: ".$imgPath."\n -->";
					}
				}
				$aOption[1][$index] .= "</div>\n";
			}
		}
	}
	$aOption[1][$index] .= '</div>'."\n";
	print $aOption[1][$index];
}
if(!empty($optID)) {
	$sql_opt="SELECT * FROM options WHERE optID=".$_POST['optID'];
	//echo $sql;
	$result_opt=mysql_query($sql_opt);	
	if(!$result_opt) exit();
	$numrows=mysql_num_rows($result_opt);
	
}
if($numrows>0){ 
	$row_opt=mysql_fetch_assoc($result_opt);	
?>
<script language="JavaScript" type="text/javascript">
	
	set_Options('<?=$row_opt["optID"]?>','<?=$row_opt["optName"]?>','','<?=$row_opt["optColor"]?>','<?=substr($rs1['pOrder'],0,1)?>','<?=$product_id?>','<?=$row_opt["optStyleID"]?>','<?=$media_type?>','<?=number_format(($rs1['pPrice']*$pPrice_adj),2)?>','','<?=number_format(($rs1['pListPrice']*$pPrice_adj),2)?>');
	
</script>
<? } ?>
<script language="JavaScript" type="text/javascript">
	set_price_item('<?=$this_type?>','<?=$this_price?>','<?=$product_id?>','<?=$this_price_diff?>','<?=$this_list_price?>');
</script>