<?php
session_cache_limiter('none');
session_start();
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property
//of Internet Business Solutions SL. Any use, reproduction, disclosure or copying
//of any kind without the express and written permission of Internet Business
//Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/languagefile.php');
include(APPPATH.'views/partials/admin/functions.php');
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if((@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])=="") || @$disallowlogin==TRUE){
	header('Location: /user/login/');
	exit();
}
$isprinter=(@$_GET["printer"]=="true");
$usepowersearch = (@$_COOKIE["powersearch"]=="1");
if(@$_POST["powersearch"]=="1"){
	if(@$_POST["startwith"]=="1"){
		setcookie("powersearch","1",time()+(60*60*24*365), "/");
		$usepowersearch = TRUE;
	}else{
		setcookie("powersearch","0",time()+(60*60*24*365), "/");
		$usepowersearch = FALSE;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Admin Orders</title>
<link rel="stylesheet" type="text/css" href="/lib/css/adminstyle.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=<?php print $adminencoding ?>"/>
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-1693518-2']);
		_gaq.push(['_setDomainName', 'ifrogz.com']);
		_gaq.push(['_setAllowHash', false]);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
</head>
<body <?php if ($isprinter) print 'class="printbody"'?>>
<?php if (! $isprinter){ ?>
    <?php include(APPPATH.'views/partials/admin/nav.php'); ?>
<?php } ?>
<?php if(! $isprinter) print '<div id="main">'; else print '<div id="mainprint">';
include(APPPATH.'views/pages/admin/orders.php');
print "</div>"; ?></body>
</html>