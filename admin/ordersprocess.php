<?php
include('init.php');
// Initialize Kohana
if (!defined('KOHANA_EXTERNAL_MODE')) {
	define('KOHANA_EXTERNAL_MODE', TRUE);
}
include_once(DOCROOT.'index.php');

$session = Session::instance();
$employee = $session->get('employee');

include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/functions.php');
include(APPPATH.'views/pages/admin/cartmisc.php');

if(!empty($_POST['aim_submit'])) {
	$sql = "SELECT * FROM transactions WHERE ordID = '".$_POST['aim_inv']."' AND txn = '".$_POST['aim_txn']."'";
	$res = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	
	if (empty($row['cc_num'])) {
		header("Location: /admin/orders.php?id=".$_POST['aim_inv']."&aim_err=Credit Card Number in database is blank&doedit=".(($_POST['aim_doedit'])?'true':'false')."#aim");
		exit();
	}
	
	if (preg_match('/^[0-9]+$/', $row['cc_num'])) {
		$cc_num = $row['cc_num'];
		$cc_exp_date = $row['cc_exp_date'];
	} else {
		$cc_num = Encrypt::instance()->decode($row['cc_num']);
		if (!preg_match('/^[0-9]+$/', $cc_num)) { // Try again in case we encrypted it twice
			$cc_num = Encrypt::instance()->decode($cc_num);
		}
		$cc_exp_date = $row['cc_exp_date'];
	}
	$cc_num = substr($cc_num, -4); // We just need the last four digits
	
	if (!preg_match('/^[0-9]+$/', $cc_num)) {
		header("Location: /admin/orders.php?id=".$_POST['aim_inv']."&aim_err=Credit card number is not an integer&doedit=".(($_POST['aim_doedit'])?'true':'false')."#aim");
		exit();
	}
	
	$rep = aimTransaction($_POST['aim_amt'],$cc_num,$cc_exp_date,$_POST['aim_type'],$_POST['aim_txn'],$_POST['aim_fname'],$_POST['aim_lname'],$_POST['aim_inv'],$_POST['aim_note']);
	if(!$rep) {
		header("Location: /admin/orders.php?id=".$_POST['aim_inv']."&aim_err=Transaction Error&doedit=".(($_POST['aim_doedit'])?'true':'false')."#aim");
		exit();
	}
	$aRep = explode("|",$rep);
	
	$cc_type = getCCType($cc_num);
	
	if($rep[0]==1) {
		// RECORD TRANSACTION
		$sql = "INSERT INTO transactions ( txn , ordID , amt , date_received , type , note , auth_net_status , auth_net_raw , cc_num , cc_type , cc_exp_date , emp_id )
				VALUES ( '".$aRep[6]."' , '".$aRep[7]."' , '".$aRep[9]."' , '".date("Y-m-d H:i:s")."' , '".$aRep[11]."' , '".$aRep[8]."' , '".$aRep[0]."' , '".$rep."' , '".$cc_num."' , '".$cc_type."' , '".$cc_exp_date."' , '".$employee['id']."' )";
		$res = mysql_query($sql) or print(mysql_error());
		
		header("Location: /admin/orders.php?id=".$_POST['aim_inv']."&aim_msg=Transaction Successful&doedit=".(($_POST['aim_doedit'])?'true':'false')."#aim");
		exit();
	}
	
	header("Location: /admin/orders.php?id=".$_POST['aim_inv']."&aim_err=Error Code: ".$aRep[2]." - ".$aRep[3]."&doedit=".(($_POST['aim_doedit'])?'true':'false')."#aim");
	exit();
}

if(!empty($_POST['am_submit'])) {
	
	$cc_num = $_POST['am_cc_num'];
	$cc_exp_date = $_POST['am_exp_mon'].$_POST['am_exp_year'];
	
	$rep = aimTransaction($_POST['am_amt'],$cc_num,$cc_exp_date,$_POST['am_type'],$_POST['am_txn'],$_POST['am_fname'],$_POST['am_lname'],$_POST['am_inv'],$_POST['am_note']);
	if(!$rep) {
		header("Location: /admin/orders.php?id=".$_POST['am_inv']."&aim_err=Transaction Error&doedit=".(($_POST['adj_doedit'])?'true':'false')."#aim");
		exit();
	}
	$aRep = explode("|",$rep);
	
	$cc_type = getCCType($cc_num);
	
	if($rep[0]==1) {
		// RECORD TRANSACTION
		$sql = "INSERT INTO transactions ( txn , ordID , amt , date_received , type , note , auth_net_status , auth_net_raw , cc_num , cc_type , cc_exp_date , emp_id )
				VALUES ( '".$aRep[6]."' , '".$aRep[7]."' , '".$aRep[9]."' , '".date("Y-m-d H:i:s")."' , '".$aRep[11]."' , '".$aRep[8]."' , '".$aRep[0]."' , '".$rep."' , '".$cc_num."' , '".$cc_type."' , '".$cc_exp_date."' , '".$employee['id']."' )";
		$res = mysql_query($sql) or print(mysql_error());
		
		header("Location: /admin/orders.php?id=".$_POST['am_inv']."&aim_msg=Transaction Successful&doedit=".(($_POST['am_doedit'])?'true':'false')."#aim");
		exit();
	}
	
	header("Location: /admin/orders.php?id=".$_POST['am_inv']."&aim_err=Error Code: ".$aRep[2]." - ".$aRep[3]."&doedit=".(($_POST['am_doedit'])?'true':'false')."#aim");
	exit();
}

if(!empty($_POST['adj_submit'])) {
	// GET THE MAX ORDERING VALUE FOR THE ORDER
	$sql = "SELECT MAX(ordering) maxOrd FROM price_adj WHERE ordID = " . $_POST['adj_ordID'];
	$res = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	$ordering = $row['maxOrd'] + 1;

	// ADD PRICE ADJUSTMENT
	$sql = "INSERT INTO price_adj ( ordID , date , type , amt_type , amt , note , ordering )
			VALUES ( ".$_POST['adj_ordID']." , '".date("Y-m-d H:i:s")."' , '".$_POST['adj_type']."' , '".$_POST['adj_amt_type']."' , ".$_POST['adj_amt']." , '".$_POST['adj_note']."' , ".$ordering." )";
	$res = mysql_query($sql) or print(mysql_error());
	
	if($res) {
		header("Location: /admin/orders.php?id=".$_POST['adj_ordID']."&adj_msg=Price Adjustment Successful&doedit=".(($_POST['adj_doedit'])?'true':'false')."#prc_adj");
		exit();
	}else{
		header("Location: /admin/orders.php?id=".$_POST['adj_ordID']."&adj_err=Error adding price adjustment&doedit=".(($_POST['adj_doedit'])?'true':'false')."#prc_adj");
		exit();
	}	
}
?>