<?php
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');

if(!empty($_GET['dhlfile'])) {
	$path = '../batches/dhl/';
	if(is_readable($path.$_GET['dhlfile'])) {
		$handle = fopen($path.$_GET['dhlfile'],"r");
		$aLines = file($path.$_GET['dhlfile']);
		fclose($handle);
		
		$totWeight = 0;
		$totDeclValue = 0;
		$totOrders = 0;
		foreach($aLines as $line) {
			if(!empty($line)) {
				$aFields = explode(",",$line);
				$ordID = trim($aFields[2],'"');
				$sql = "SELECT c.cartOrderID, p.pWeight, p.pCustomsvalue
						FROM cart c, products p
						WHERE c.cartProdID = p.pID
						AND c.cartOrderID = $ordID";
				$res = mysql_query($sql) or print(mysql_error());
				
				while($row=mysql_fetch_assoc($res)) {
					$totWeight += $row['pWeight'];
					$totDeclValue += $row['pCustomsvalue'];
				}
				
				$totOrders++;
			}
		}
	}else{
		echo $_GET['dhlfile']." is not readable.";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>DHL Dat File Statistics</title>
</head>

<body>

<table width="400" border="0" align="center" cellpadding="2" cellspacing="0">
    <tr>
        <th colspan="2" style="font-weight: bold;">DHL File Statistics<br /><span style="font-weight: normal;"><em><?=$_GET['dhlfile']?></em></span></th>
    </tr>
    <tr>
        <td># of Orders</td>
        <td style="text-align: right;"><?=$totOrders?></td>
    </tr>
    <tr>
        <td>Total Weight</td>
        <td style="text-align: right;"><?=round($totWeight,2)?> kg</td>
    </tr>
    <tr>
        <td>Total Declared Value</td>
        <td style="text-align: right;"><?='$'.round($totDeclValue,2).' USD'?></td>
    </tr>
</table>

</body>
</html>