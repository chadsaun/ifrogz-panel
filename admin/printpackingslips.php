<?php
session_start();
ini_set("memory_limit","12M");
include('init.php');
include_once(APPPATH.'views/partials/admin/dbconnection.php');
include_once(APPPATH.'views/partials/admin/functions.php');
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if((@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])=="") || @$disallowlogin==TRUE) {
	header('Location: /user/login');
	exit();
}

// This is used to turn on and off the format for using letterhead
$_GET['test'] = 0;

session_start();
//DETERMINE IF IT IS A REPRINT
$reprint = TRUE;
if ($_GET['batch'] == "") {
	$batch_name = date('Y-m-d H:i:s');
	$reprint = FALSE;
} else {
	$batch_name = $_GET['batch'];
	$reprint = TRUE;
}

//POST
$strOrderIDs = '';
if (is_array($_POST['orderid'])) {
	foreach ($_POST['orderid'] as $value) {
		if($strOrderID == '') $comma = '';
		else $comma = ',';
		$strOrderID .=  $comma . $value;
	}
	//mail('blake@ifrogz.com','Packing Slip Print',$strOrderID,'');
}
//GET ORDERS
$sql_check="SELECT * FROM orders WHERE ";

if ($_GET['order'] != "") {
	$orderID = $_GET['order'];
	$sql_check.=" ordID=" . $orderID;
} else {
	if ($reprint) {
		$sql_check.=" ordBatch='".$batch_name."'";
	} else {
		$ordStatus=3;
		if(!empty($_POST['status'])) {
			$ordStatus = $_POST['status'];
		}
		$sql_check.=" ordStatus='".$ordStatus."'"; 
	}
}
if($strOrderID != '') {
	$sql_check.=" AND ordID IN (" . $strOrderID . ")"; 
}

$sql_check.=" AND ordEID!=34";
/*if (empty($screenz)) {
	if (!empty($etailer)) {
		$sql_check.=" AND ordEID>0";
		$str_batch_type='_e';
	} else {
		$sql_check.=" AND ordEID=0";
	}
}
*/
$sql_check.= " ORDER BY ordShipType,ordID";

if ( $_POST['number'] != "" ) {	
	$sql_check.= " LIMIT " . $_POST['number'];
} 

//echo $sql_check;

$result_check = mysql_query($sql_check) or print(mysql_error());

if (mysql_num_rows($result_check)==0) {
	echo "There are 0 orders to print!";
	exit();
}

$arrOrders = array();
$d=0;
while ($row_check = mysql_fetch_assoc($result_check)) {
	//DETERMINE IF SINGLE ORDER HAS ALREADY BEEN PRINTED
	if ($row_check['ordBatch'] != "" && $_GET['order'] != "") $reprint = TRUE;
	
	//GRAB DATA FROM ORDERS
	$arrOrders[$d] = $row_check;
	
	//SHIP TYPE
	$apoOrder = FALSE;
	if($row_check["ordShipAddress"] != "" && $row_check["ordShipCity"] != "" && $row_check["ordShipZip"] !="" ) {
		if(stristr('AA,AE,AP',$row_address["ordShipState"])) $apoOrder=TRUE;
	} else {
		if(stristr('AA,AE,AP',$row_address["ordState"])) $apoOrder=TRUE;
	}
	if($apoOrder) $shipType='Priority - APO';
	else $shipType = $row_check["ordShipType"];
	$arrOrders[$d]["ordShipType"]=$shipType; 
	$d++;
}
mysql_free_result($result_check);

?>
<style type="text/css">
<!--
td {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;}
-->
</style>
<?php

foreach ($arrOrders as $order) {
	// Get the product information for the order
	$sql = "SELECT IF(p.pSell=1,p.pBin,o.optBin) AS bin, SUM(c.cartQuantity) AS cartQuantity, c.cartProdID, c.cartProdName, c.cartID, co.coCartOption, o.optID, co.coOptGroup, o.optName, o.optStyleID, o.optGroup, c.cartProdPrice, p.p_iscert, p.pDownload, p.pWeight
			FROM cart c JOIN products p ON c.cartProdID=p.pID LEFT JOIN cartoptions co ON c.cartID=co.coCartID LEFT JOIN options o ON co.coOptID=o.optID
			WHERE c.cartOrderID = ".$order['ordID']."
			GROUP BY IF(p.pSell=1,c.cartProdID,co.coOptID)
			ORDER BY c.cartID";
	
	$res = mysql_query($sql) or print(mysql_error());
	
	$arrProducts = array();
	$i = 0;
	$count_email_cert = 0;
	$count_down_loads = 0;	
	$oldCartID = '';
	$orderWeight = 0;
	$has_custom_screen = 0;
	$has_all_custom_screenz = TRUE;
	$num_rows = mysql_num_rows($res);
	while ($row = mysql_fetch_assoc($res)) {		
		//$arrProducts[$i] = $row;
		if($row['p_iscert'] == 1 && $row['optName'] == "Email" || $row['pDownload'] != "") {
			if($row['p_iscert'] == 1 && $row['optName'] == "Email") $count_email_cert++;
			if($row['pDownload'] != "") $count_down_loads++;
			
		} else {
			$arrProducts[$i] = $row;			 
		}
		
		if(strstr($row['cartProdID'],'-Custom')) {
			$cartoption_left5 = (int)substr($row['coCartOption'],0,5);
			$sql_custom = "SELECT *
						FROM uploaded_images  
						WHERE id = ".$cartoption_left5;
			//echo $sql_custom. '<br />';
			$result_custom = mysql_query($sql_custom);
			if( mysql_num_rows($result_custom) > 0){
				$row_custom = mysql_fetch_assoc($result_custom);
				$has_custom_screen++;
				$arrProducts[$i]['custom'] = $row_custom['display_image'];
				$arrProducts[$i]['optName'] = $row_custom['display_image'];
			}
		}
		
		// If this option is a Custom Hype Rim, then make quantity double. (They are ordered as pairs, but inventory is kept individually)
		//  * Remeber to change this in other places too ( release_stock(), getOnOrderStock(), do_stock_management() in /application/views/partials/admin/functions.php )
		if ($row['cartProdID'] == 'customhype' && strstr($row['coOptGroup'], 'Rim')) {
			$arrProducts[$i]['cartQuantity'] = (int)$arrProducts[$i]['cartQuantity'] * 2;
		}
		
		$i++;
	}

	//ADD PRODUCTS TO ORDER
	addProdsToOrder2($order['ordID']);
	
		
	//SORT ARRAY BY BIN NUMBER
	usort($arrProducts, 'cmpBins');
	
	//sort($arrProducts);
	mysql_free_result($res);
	
	
	/*echo "<!-- chad\n";
	print_r($arrProducts);
	echo " -->";*/
	
	//showarray($arrProducts);
	//CALCULATE WEIGHT
	$sql_weight = "SELECT * FROM cart c JOIN products p ON c.cartProdID=p.pID WHERE cartOrderID = ".$order['ordID'];	
	$res_weight = mysql_query($sql_weight) or print(mysql_error());
	while ($row_weight = mysql_fetch_assoc($res_weight)) {
		//echo 'in-';
		//$arrProducts[$i]['pWeight'] = $row_weight['pWeight'] * $row_weight['cartQuantity'];
		$orderWeight += $row_weight['pWeight'] * $row_weight['cartQuantity'];
		//echo 'w='.$row['pWeight'] * $row['cartQuantity']."<br />";
		//number_format(($row['pWeight'] * $row['cartQuantity'] * 35.2739619),3)
	}
	
	$orderWeightOunces = number_format((($orderWeight + 0.03) * 35.2739619),3);
	
	//Skip order if they are all email certs, downloads
	$skip_order = FALSE;	
	if (!$reprint) {		
		$new_status = 6;
		if ($num_rows == $count_email_cert || $num_rows == $count_down_loads) {
			$new_status=12;
			$skip_order = TRUE;
		}
		//UPDATE ORDER STATUS
		$sql_update="UPDATE orders SET ordStatus='" . $new_status . "', ordBatch='" . $batch_name . "', ordFulfilledBy='" . $_SESSION["packingsliplogin"]['firstname'] . "' WHERE ordID='" . $order['ordID'] . "'";
		mysql_query($sql_update)or print(mysql_error());
		// Backup to make sure orders don't get marked as being printed when nobody submitted them to be printed
		if (!empty($_POST['status'])) {
			if(!setNewLocation( $new_status , $order['ordID'] )) print("Unable to record location.");
		}
	}
	
	//$sql_update="UPDATE orders SET ordWeight=" . $orderWeight . " WHERE ordID='" . $order['ordID'] . "'";
	//mysql_query($sql_update)or print(mysql_error());

	
	if (!is_array($order)) echo 'There are no orders to print';
	if ($skip_order) continue;
	
	
	if ($_GET['test'] != 1) {
?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="page-break-after: always;">
<?php
	} else {
?>
	<table border="0" cellspacing="0" cellpadding="0" style="margin-left: 0; margin-top: .50in; page-break-after: always; width: 5.30in;">
<?php
	}
?>
		<tr>
			<td width="170" align="left"><img src="/barcode/wrapper.php?p_bcType=1&p_text=<?=$order['ordID']?>&p_xDim=1&p_w2n=3&p_charGap=1&p_invert=N&p_charHeight=40&p_type=1&p_label=N&p_rotAngle=0&p_checkDigit=N" alt="1234567890" /></td>
			<td align="center"><span style="font-weight: bold; font-size: 14px;">Order # <?=$order['ordID']?><br /><?=$order['ordShipType']?> <br /><?=$orderWeightOunces?> oz</span></td>
			<td width="170" align="right">
	<?php
		if (($_GET['test'] != 1) && ( ! is_special_order($order['ordID']))) {
	?>
				<img src="/lib/images/ifrogz_logo_white2.jpg" width="125" />
	<?php
		} else {
	?>
				<img src="/lib/images/createbutton.jpg" width="153" />
	<?php	
		}
	?>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="100%" border="0" cellpadding="3" cellspacing="0">
					<tr>
						<td align="center" colspan="5" style="font-weight: bold;">Billing Information</td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Full Name:</td>
						<td align="left" colspan="2"><?=$order['ordName']?></td>
						<td align="right" style="font-weight: bold;">Email:</td>
						<td align="left"><?=$order['ordEmail']?></td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Address:</td>
						<td align="left" colspan="2"><?=$order['ordAddress']?></td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Address Line 2:</td>
						<td align="left" colspan="2"><?=$order['ordAddress2']?></td>
						<td align="right" style="font-weight: bold;">City:</td>
						<td align="left"><?=$order['ordCity']?></td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">State:</td>
						<td align="left"><?=$order['ordState']?></td>
						<td align="left">
							<strong>APO/PO:</strong>
							<input type="checkbox" value="1" disabled="disabled"<?=($order['ordPoApo']) ? ' checked="checked"' : ''?> />
						</td>
						<td align="right" style="font-weight: bold;">Country:</td>
						<td align="left"><?=$order['ordCountry']?></td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Zip/Postal Code:</td>
						<td align="left" colspan="2"><?=$order['ordZip']?></td>
						<td align="right" style="font-weight: bold;">Phone:</td>
						<td align="left"><?=$order['ordPhone']?></td>
					</tr>
		<?php
			if (!empty($order['ordShipAddress'])) {
		?>
					<tr>
						<td align="center" colspan="5" style="font-weight: bold;">Shipping Details</td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Full Name:</td>
						<td align="left" colspan="2"><?=$order['ordShipName']?></td>
						<td align="right" style="font-weight: bold;">Email:</td>
						<td align="left"><?=$order['ordShipEmail']?></td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Address:</td>
						<td align="left" colspan="2"><?=$order['ordShipAddress']?></td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Address Line 2:</td>
						<td align="left" colspan="2"><?=$order['ordShipAddress2']?></td>
						<td align="right" style="font-weight: bold;">City:</td>
						<td align="left"><?=$order['ordShipCity']?></td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">State:</td>
						<td align="left"><?=$order['ordShipState']?></td>
						<td align="left">
							<strong>APO/PO:</strong>
							<input type="checkbox" value="1" disabled="disabled"<?=($order['ordShipPoApo']) ? ' checked="checked"' : ''?> />
						</td>
						<td align="right" style="font-weight: bold;">Country:</td>
						<td align="left"><?=$order['ordShipCountry']?></td>
					</tr>
					<tr>
						<td align="right" style="font-weight: bold;">Zip/Postal Code:</td>
						<td align="left" colspan="2"><?=$order['ordShipZip']?></td>
						<td align="right" style="font-weight: bold;">Phone:</td>
						<td align="left"><?=$order['ordShipPhone']?></td>
					</tr>
		<?php
			}
		?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table border="0" cellpadding="3" cellspacing="0" width="100%">
					<tr>
						<td>
							
							<table border="1" cellpadding="4" cellspacing="0" bordercolor="#999999" style="border-collapse: collapse" width="100%">
								<tr>
                                	<td>&nbsp;</td>
									<td style="font-weight: bold;" align="center">Qty</td>
                                    <td style="font-weight: bold;" align="center">Bin</td>
									<td style="font-weight: bold;">Product ID</td>
									<td style="font-weight: bold;">Product Name</td>
									<td style="font-weight: bold;">Options</td>
									<!--<td style="font-weight: bold;">Price</td>-->
                                 </tr>
						<?php
						//prepBins(&$arrProducts); // For testing purposes
						$zoneCount = countZones($arrProducts);
						
						//echo '<!-- '; print_r($arrProducts); print_r($zoneCount); echo ' -->';
						
						$numOfProds = count($arrProducts);
						$curBin = '';
						for ($i = 0; $i < $numOfProds; $i++) {

							if(strlen($arrProducts[$i]['bin']) > 6)
								$bin = strtoupper(substr($arrProducts[$i]['bin'], 2, 2));
							else
								$bin = strtoupper(substr($arrProducts[$i]['bin'], 0, 1));
							
							//$bin = strtoupper(substr($arrProducts[$i]['bin'], 0, 1));
							$rowSpan = $zoneCount[$bin];
						?> 
								<tr>
						<?php
							if ($i == 0) {
						?>
								
									<td align="center" valign="middle" rowspan="<?=$rowSpan?>" style="font-size: 36px; font-weight: bold;"><?=(empty($bin)) ? '&nbsp;' : $bin?></td>
						<?php
							} elseif ($bin != $curBin) {
						?>
                                    <td colspan="7">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td align="center" valign="middle" rowspan="<?=$rowSpan?>" style="font-size: 36px; font-weight: bold;"><?=(empty($bin)) ? '&nbsp;' : $bin?></td>
						<?php
							} 
						?>			
                        			<td valign="top" align="center">
										<? 
										if ($arrProducts[$i]['cartQuantity'] > 1) echo '*';
										echo $arrProducts[$i]['cartQuantity'];
										?>
                                    </td>						                                 
                                    <td valign="top" align="center"><?=$arrProducts[$i]['bin']?></td>
									<td valign="top"><?=$arrProducts[$i]['cartProdID']?></td>
									<td valign="top"><?=$arrProducts[$i]['cartProdName']?></td>
							    <td valign="top">
                                    	<strong><?=$arrProducts[$i]['coOptGroup']?></strong>
										<?=' - '.$arrProducts[$i]['optName']?>
                                        <? 
										if ($arrProducts[$i]['optStyleID'] != "") {
											echo ' (' . $arrProducts[$i]['optStyleID'] . ')';
										}
										if ( $arrProducts[$i]['custom'] != "" ) { ?>
										<span style="margin-left:10px;"><img src="/imguploads/img_screen/<?=$arrProducts[$i]['custom']?>.gif" height="50" align="top"/></span>
									<?  } ?>                                    </td>
								   <!--<td valign="top"><?=$arrProducts[$i]['cartProdPrice']?></td>-->
								</tr>
						<?php
							$curBin = $bin;
						}
						?>
							</table>
						</td>
					</tr>
				</table>
				
			</td>
		</tr>
	<?php
		//if ($_GET['mytest']) {
	?>
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse;">
					<tr>
						<td colspan="4" align="center"><strong>Build List</strong></td>
					</tr>
					<tr>
						<td><strong>Product ID</strong></td>
						<td><strong>Product Name</strong></td>
						<td><strong>Options</strong></td>
						<td><strong>Bin #</strong></td>
						<td><strong>Quantity</strong></td>
					</tr>
	<?php
			$sql_build = "SELECT c.cartProdID, c.cartProdName, c.cartProdPrice, c.cartQuantity, c.cartID, p.pDownload, p.p_iscert, 
							d.dsName, pInStock, pSell, c.cartReason, c.cartNote 
						  FROM cart c, products p LEFT JOIN dropshipper d ON p.pDropship=d.dsID 
						  WHERE c.cartProdID = p.pID 
						  AND c.cartOrderID = ". $order['ordID'];
			$res_build = mysql_query($sql_build) or print(mysql_error());
			
			while ($row = mysql_fetch_assoc($res_build)) {
	?>
					<tr>
						<td><?=$row['cartProdID']?></td>
						<td><?=$row['cartProdName']?></td>
						<td>
	<?php
				$sql_opt = "SELECT coOptGroup, coCartOption, coPriceDiff, coOptID, optGroup, optStock, optStyleID, coExtendShipping, optBin, 
								ui.display_image, ui.org_img_name 
							FROM cartoptions 
								LEFT JOIN options ON cartoptions.coOptID=options.optID 
								LEFT JOIN uploaded_images ui ON cartoptions.coCartOption=ui.id 
							WHERE coCartID = " . $row["cartID"] . " 
							ORDER BY coID";
				$res_opt = mysql_query($sql_opt) or print(mysql_error());
				
				$binText = '';
				if (mysql_num_rows($res_opt) > 0) {
					while ($row_opt = mysql_fetch_assoc($res_opt)) {
						echo "<strong>".$row_opt['coOptGroup'].":</strong> " . str_replace(array("\r\n","\n"),array("<br />","<br />"),$row_opt["coCartOption"]) .' '.$row_opt["optStyleID"] . '<br />';
						$binText .= $row_opt['optBin'] . "<br />";
					}
				}
	?>
						</td>
						<td><?=$binText?></td>
						<td><?=$row['cartQuantity']?></td>
					</tr>
	<?php
			}
	?>
				</table>
			</td>
		</tr>
	<?php
		//}
	?>
	<?php
		if ($_GET['test'] != 1) {
	?>
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="4" cellspacing="0" style="margin: 3px;">
					<?php /*
					<?php // COMMENT - The wish list might get added later. we are commenting it out for now. ?>
					<tr>
						<td><span style="font-size: 150%; font-weight: bold;">Is Someone's Special Day Coming Up?</span><br />Search for their iFrogz Wish List! &nbsp;<span style="text-decoration: underline;">http://ifrogz.com/wishlist/findlist.php</span></td>
					</tr>
					*/?>
				</table>
			</td>
		</tr>
	<?php
		}
	?>
	</table>
<?php
}

function is_special_order($order_id)
{
	$select_query = 'SELECT
		ordAuthNumber as KEYCODE
	FROM
		orders
	WHERE
		ordID='.$order_id;
	
	$result = mysql_query($select_query);
	$result = mysql_fetch_assoc($result);
	
	return strcmp($result['KEYCODE'], 'KC-77') == 0;
}

function countZones($arr) {
	/*$ret['A'] = 0;
	$ret['B'] = 0;
	$ret['C'] = 0;
	$ret['D'] = 0;
	$ret['E'] = 0;
	$ret['F'] = 0;*/
	
	for ($i = 0; $i < count($arr); $i++) {
		//$bin = strtoupper(substr($arr[$i]['bin'], 0, 1));
		
		if(strpos($arr[$i]['bin'], '*') > -1)
			$bin = strtoupper(substr($arr[$i]['bin'], 2, 2));
		else
			$bin = strtoupper(substr($arr[$i]['bin'], 0, 1));
		
		$ret[$bin]++;
	}
	
	return $ret;
}

function prepBins(&$arr) {
	for ($i = 0; $i < count($arr); $i++) {
		if ($i < 2) {
			$arr[$i]['optBin'] = 'B-'.$i;
		} else {
			$arr[$i]['optBin'] = 'A-'.$i;
		}
	}
}

function addProdsToOrder($order) {
	$addArr = array();
	
	$sql = "SELECT IF(p.pSell=1,p.pBin,o.optBin) AS bin, p.pName AS cartProdName, a.addProdID AS cartProdID, a.addOptID AS optID, o.optName, o.optStyleID, og.optGrpName, o.optGroup, c.cartQuantity, p.p_iscert, p.pDownload, p.pWeight 
			FROM cart c JOIN addProdsToOrder a ON c.cartProdID=a.prodID JOIN products p ON a.addProdID=p.pID LEFT JOIN options o ON a.addOptID=o.optID LEFT JOIN optiongroup og ON o.optGroup=og.optGrpID";
	$sql .= " WHERE c.cartOrderID = '" . $order . "'";
	$result = mysql_query($sql) or print(mysql_error().' ' .$sql);
	while ($row = mysql_fetch_assoc($result)) {
		$addArr = $row;
		if (is_array($addArr)) {
			checkDuplicates($row['cartProdID'],$addArr);
		}
	}
	
}	

function addProdsToOrder2($order) {
	$arrProdsToAdd = array();
	$arrProds = array();
	
	// Get the products to add
	$sql = "SELECT *
			FROM cart
			WHERE cartOrderID = $order";
	$res = mysql_query($sql) or print(mysql_error());
	$i = 0; // Each product in cart
	while ($row = mysql_fetch_assoc($res)) {
		// Get optGroup
		$sql3 = "SELECT o.optGroup
				 FROM cart c, cartoptions co, options o
				 WHERE c.cartID = co.coCartID
				 AND co.coOptID = o.optID
				 AND c.cartID = " . $row['cartID'];
		//echo $sql3; exit();
		$res3 = mysql_query($sql3) or print(mysql_error());
		
		
		if (mysql_num_rows($res3) > 0) {
			while ($row3 = mysql_fetch_assoc($res3)) {
				$j = 0; //   Each product to add for that product
				$sql2 = "SELECT * FROM addProdsToOrder a 
						WHERE prodID = '".$row['cartProdID']."'
						AND optGrpID IN(0,".$row3['optGroup'].")";
				//echo $sql2."<br />";
				$res2 = mysql_query($sql2) or print(mysql_error());
				while ($row2 = mysql_fetch_assoc($res2)) {
					if (!is_array($arrProds[$i])) {
						$arrProds[$i] = array();
					}
					if (!in_array($row2['addProdID'], $arrProds[$i])) {
						$arrProdsToAdd[$i][$j]['addProdID'] = $row2['addProdID'];
						$arrProdsToAdd[$i][$j]['addOptID'] = $row2['addOptID'];
						$arrProdsToAdd[$i][$j]['qty'] = $row['cartQuantity'];
						$arrProds[$i][] = $row2['addProdID'];
						$j++;
						
					}
				}
				
			}
			$i++;
		}
	}
	
	//showarray($arrProdsToAdd); 
	
	// Group up the quantities
	// Array Example:
	// Array
	//	(
	//		[ep-zip] => Array
	//			(
	//				[4839] => Array
	//					(
	//						[qty] => 2
	//					)
	//	
	//			)

	$newProdsToAdd = array();
	
	foreach ($arrProdsToAdd as $i => $products) {
		foreach ($products as $j => $prod) {
			$newProdsToAdd[$arrProdsToAdd[$i][$j]['addProdID']][$arrProdsToAdd[$i][$j]['addOptID']]['qty'] += $arrProdsToAdd[$i][$j]['qty'];
		}
	}
	
	/*for ($i = 0; $i < count($arrProdsToAdd); $i++) {
		for ($j = 0; $j < count($arrProdsToAdd[$i]); $j++) {
			$newProdsToAdd[$arrProdsToAdd[$i][$j]['addProdID']][$arrProdsToAdd[$i][$j]['addOptID']]['qty'] += $arrProdsToAdd[$i][$j]['qty'];
		}
	}*/
	
	//showarray($newProdsToAdd); 
	
	// Get the information for the products
	
	foreach ($newProdsToAdd as $prodID => $val) {
		foreach ($val as $optID => $val2) {
			$sql = "SELECT IF(p.pSell=1,p.pBin,o.optBin) AS bin, p.pName AS cartProdName, p.pID AS cartProdID, o.optID AS optID, 
						o.optName, o.optStyleID, og.optGrpName, o.optGroup, p.p_iscert, p.pDownload, p.pWeight
					FROM products p, prodoptions po, optiongroup og, options o
					WHERE p.pID = po.poProdID
					AND po.poOptionGroup = og.optGrpID
					AND og.optGrpID = o.optGroup
					AND p.pID = '$prodID'
					AND o.optID = $optID";
			$res = mysql_query($sql) or print(mysql_error()."<br />".$sql."<br />");
			if ($res) {	
				if (mysql_num_rows($res) > 0) {
					$row = mysql_fetch_assoc($res);
					$addArr = $row;
					$addArr['cartQuantity'] = $val2['qty'];
					if (is_array($addArr)) {
						checkDuplicates($row['cartProdID'],$addArr);
						//showarray($addArr);
					}
				}
			}
		}
	}
}
	
function checkDuplicates ($pID,$addArr) {	
	global $arrProducts,$i,$orderWeight,$reprint;
	//strstr('CC,CCJW,tadpoleCC,tadpoleCCW,tadpoleCCW-Custom,CC2,CCJW,o_treadzCC%,o_blfrogCC%,o_frogzCC%,CC2JU%,tadpoleCC%,bumpzCC%,ribbitzCC%',$pID)) {		
		
	$duplicate = FALSE;
	$cnt = count($arrProducts);
	for ($j=0;$j<$cnt;$j++) {
		if ($arrProducts[$j]['cartProdID'] == $addArr['cartProdID'] && $arrProducts[$j]['optID'] == $addArr['optID']) {
			$arrProducts[$j]['cartQuantity'] += $addArr['cartQuantity'];
			$duplicate = TRUE;
		}
	}
	if (!$duplicate) {		
		
		//IF(p.pSell=1,p.pBin,o.optBin) AS bin, SUM(c.cartQuantity) AS cartQuantity, c.cartProdID, c.cartProdName, c.cartID, o.optID, co.coOptGroup, o.optName, o.optStyleID, o.optGroup, c.cartProdPrice
		$arrProducts[$i]['bin'] = $addArr['bin'];
		$arrProducts[$i]['cartQuantity'] = $addArr['cartQuantity'];
		$arrProducts[$i]['cartProdID'] = $addArr['cartProdID'];
		$arrProducts[$i]['cartProdName'] = $addArr['cartProdName'];
		$arrProducts[$i]['cartID'] = '';
		$arrProducts[$i]['coCartOption'] = '';
		$arrProducts[$i]['optID'] = $addArr['optID'];		
		$arrProducts[$i]['coOptGroup'] = $addArr['optGrpName'];
		$arrProducts[$i]['optName'] = $addArr['optName'];
		$arrProducts[$i]['optStyleID'] = $addArr['optStyleID'];
		$arrProducts[$i]['optGroup'] = $addArr['optGroup'];
		$arrProducts[$i]['cartProdPrice'] = '';
		$arrProducts[$i]['p_iscert'] = '';
		$arrProducts[$i]['pDownload'] = '';
		$arrProducts[$i]['pWeight'] = $addArr['pWeight'];
		$orderWeight += $addArr['pWeight'] * $addArr['cartQuantity'];
		
		
		$i++;
	}
	if (!$reprint) {
		//SUBTRACT INVENTORY FOR ADDED PRODUCTS			
		if ($addArr['optID'] != "") {
			$sql = "SELECT * FROM options WHERE optID = " . $addArr['optID'];
			$res = mysql_query($sql);
			$options = mysql_fetch_assoc($res);
			
			$sSQL = "UPDATE options SET optStock=optStock-" . $addArr['cartQuantity'] . " WHERE optID=" . $addArr['optID'];
			//echo $sSQL;
			mysql_query($sSQL) or print(mysql_error());
			
			$sql = "SELECT p.*
					FROM products p 
						JOIN prodoptions po ON p.pID = po.poProdID
						JOIN optiongroup og ON po.poOptionGroup = og.optGrpID
						JOIN options o ON og.optGrpID = o.optGroup
					WHERE o.optID = '".$addArr['optID']."'";
			$res = mysql_query($sql);
			$prod = mysql_fetch_assoc($res);
			
			$prodstyle = $prod["pID"] . "-" . $options['optStyleID'];
			$newvalue = $options['optStock'] - $addArr['cartQuantity'];
			
			// RECORD INVENTORY CHANGE
			$sql3 = "INSERT INTO inv_adjustments (iaOptID, iaAmt, iaDate, iaProdStyle, iaOldValue, iaNewValue, iareason, iaEmpID)
					 VALUES (".$addArr['optID'].", -".$addArr['cartQuantity'].", '".date('Y-m-d H:i:s')."', '$prodstyle', ".$options['optStock'].", $newvalue, 10, '".$_SESSION['employee']['id']."')";
			$res3 = mysql_query($sql3);
		}
	} 
}

if( is_array($_SESSION['packingsliplogin'])) {
	foreach($_SESSION['packingsliplogin'] as $key => $value) {
		$_SESSION['packingsliplogin'][$key] = '';
	}
}

?>
<? 	if( !$reprint) { ?>
<script type="text/javascript">
	window.opener.reloadparent();
	//parent.refresh();
</script>
<? } ?>