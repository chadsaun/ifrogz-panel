<?php
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/functions.php');

//showarray($_POST);
//exit();

if(!empty($_POST['recOrders'])) {
	foreach($_POST as $key => $value) {
		if(substr($key,0,7)=='receive') {
			if($value=='yes') {
				$order = substr($key,7);
				$nextStatus = getShipStatus($order);
				$qry = "UPDATE orders
						SET ordStatus = '$nextStatus' 
						WHERE ordID = '$order'";
				$res = mysql_query($qry) or print(mysql_error());
				if($res) {
					if(!setNewLocation( $nextStatus , $order )) print("Unable to record location.");
				}
			}	
		}
	}
}elseif(!empty($_POST['create'])) {
	//	Get the status name
	//		- These should match the name of their directory for writing batches too
	if($_POST['status']==5) $statusName = 'rbi';
	if($_POST['status']==6) $statusName = 'usa';
	if($_POST['status']==7) $statusName = 'canada';
	if($_POST['status']==8) $statusName = 'intl';
	
	// Get all orders in the USA status
	$qry = "SELECT * FROM orders WHERE ordStatus = '".$_POST['status']."'";
	$res = mysql_query($qry) or print(mysql_error());
	
	if(mysql_num_rows($res)>0) {
		$produce_date = date("Ymd",mktime(date("H")+14, date("i"), date("s"), date("m"),  date("d"),  date("Y")));
		$fileBuffer = '';
		$fileBuffer .= 
'
0,"020"
1,"MASTER"
19,"IPD-'.(($_POST['status']==7)?'CA':'IND').'"
20,"339676500"
23,"3"
24,"'.$produce_date.'"
31,"New Fortune"
68,"USD"
70,"3"
71,"339676500"
75,"KGS"
79,"Silicon iPod Cases (HS Code 4202999000)"
541,"YNNNNNNNN"
1273,"01"
1274,"18"
1586,"Y"
1486,"ifrogz.com"
1485,"Clay Broadbent"
1487,"919 N 1000 W"
1488,""
1489,"Logan"
1490,"UT"
1491,"84321"
1492,"1-877-443-7641"
1585,"US"
99,""
';
		if($_POST['status']=='5') {
			// Create the new file name
			chdir('batches/'.$statusName);
			$file_not_found=true;
			$temp = rightnow();
			$file_ext = 0;
			do{
				$file_new = 'ifrogz-'.substr($temp,5,2).'-'.substr($temp,8,2).'-'.$file_ext.'.in';
				$inv_ref = substr($temp,5,2).substr($temp,8,2).$file_ext;
				if(is_file($file_new)) $file_ext++;
				else $file_not_found=false;
			}while($file_not_found);
			
			$totWeight = 0;
			$totQuantity = 0;
			while($row=mysql_fetch_assoc($res)) {
				// Get product information from the order so that we can calculate the total weight of the order and total quantity
				$qry2 = "SELECT p.pWeight, cart.cartQuantity, cart.cartProdID
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row['ordID'];
				$res2 = mysql_query($qry2) or print(mysql_error());
				while($row2 = mysql_fetch_assoc($res2)) {
					$weight += $row2['cartQuantity'] * $row2['pWeight'];
					// IF THE PRODUCT IS A SET, MULTIPLY BY 3 TO GET 3 FOR THE QUANTITY INSTEAD OF 1
					if($row2['cartProdID']=='AHX' || $row2['cartProdID']=='BJZ' || $row2['cartProdID']=='AHX') {
						$quantity += ($row2['cartQuantity'])*3;
					}else {
						$quantity += $row2['cartQuantity'];
					}
				}
				$totWeight += $weight;
				$totQuantity += $quantity;
				
				$qryUpdate = "UPDATE orders
						SET ordStatus = '9' 
						WHERE ordID = '".$row['ordID']."'";
				$resUpdate = mysql_query($qryUpdate) or print(mysql_error());
				if($resUpdate) {
					if(!setNewLocation( 9 , $row['ordID'] )) print("Unable to record location.");
				}
			}
				$fileBuffer .=
'
0,"020"
1,"'.$inv_ref.'"
11,"Reminderband.com"
12,"Clay Broadbent"
13,"919 N 1000 W"
14,""
15,"Logan"
16,"UT"
17,"84321"
18,"4357871234"
50,"US"
24,"'.$produce_date.'"
81,"4202999000"
541,"NNNYNNNNN"
1273,"01"
1274,"18"
1355,"IPD"
116,"1"
21,"'.(int)$totWeight.'"
1030,"100000"
82,"'.$totQuantity.'"
79,"Silicon iPod Cases (HS Code 4202999000)"
80,"CN"';
			// Create the trans.in file
			if (!($fp = fopen($file_new, "w+")))  
				exit("Unable to open the input file.");
			fwrite($fp,$fileBuffer);
			fclose($fp);
			chdir('../../vsadmin');
			
			$success = 'Batch '.$file_new.' created';
			header('Location: /admin/shipping.php?success='.$success);
			exit();
			
		}else {
			while($row=mysql_fetch_assoc($res)) {
				if(empty($row['ordShipAddress'])) {
					$shipAddress = $row['ordAddress'];
				}else {
					$shipAddress = $row['ordShipAddress'];
				}
				if(empty($row['ordShipAddress2'])) {
					$shipAddress2 = $row['ordAddress2'];
				}else {
					$shipAddress2 = $row['ordShipAddress2'];
				}
				if(empty($row['ordShipCity'])) {
					$shipCity = $row['ordCity'];
				}else {
					$shipCity = $row['ordShipCity'];
				}
				if(empty($row['ordShipState'])) {
					$shipState = $row['ordState'];
				}else {
					$shipState = $row['ordShipState'];
				}
				if(empty($row['ordShipZip'])) {
					$shipZip = $row['ordZip'];
				}else {
					$shipZip = $row['ordShipZip'];
				}
				if(empty($row['ordShipCountry'])) {
					$shipCountry = $row['ordCountry'];
				}else {
					$shipCountry = $row['ordShipCountry'];
				}
				
				// Get product information from the order so that we can calculate the total weight of the order and total quantity
				$qry2 = "SELECT p.pWeight, cart.cartQuantity, cart.cartProdID
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row['ordID'];
				$res2 = mysql_query($qry2) or print(mysql_error());
				
				$totWeight = 0;
				$totQuantity = 0;
				$totPrice = 0;
				$cost = 0;
				$sets = ",AHX,BJZ,CJZ,CCJZ,CJW,CCJW,";
				$lrgSets = ",BJZ,CJZ,CCJZ,CJW,CCJW,";
				$nanSets = ",AHX,";
				while($row2 = mysql_fetch_assoc($res2)) {
					$totWeight += $row2['cartQuantity'] * $row2['pWeight'];
					// IF THE PRODUCT IS A SET, MULTIPLY BY 3 TO GET 3 FOR THE QUANTITY INSTEAD OF 1
					if(strstr($sets,",".$row2['cartProdID'].",")) {
						$totQuantity += ($row2['cartQuantity'])*3;
					}else {
						$totQuantity += $row2['cartQuantity'];
					}
					$totPrice += $row2['cartProdPrice'];
					
					// CHECK SETS
					if(strstr($lrgSets,",".$row2['cartProdID'].",")) {
						$cost += 1.47;
					}elseif(strstr($nanSets,",".$row2['cartProdID'].",")) {
						$cost += 1.05;
					}else{
						// CHECK WRAPS
						if(strstr($row2['cartProdID'],"B") || strstr($row2['cartProdID'],"CC") || strstr($row2['cartProdID'],"C")) {
							$cost += .99;
						}elseif(strstr($row2['cartProdID'],"A")) {
							$cost += .60;
						}
						
						// CHECK BANDS
						if(strstr($row2['cartProdID'],"J")) {
							$cost += .18;
						}elseif(strstr($row2['cartProdID'],"H")) {
							$cost += .15;
						}
						
						// CHECK SCREENS
						if(strstr($row2['cartProdID'],"W-") || strstr($row2['cartProdID'],"Z-") || strstr($row2['cartProdID'],"X-")) {
							$cost += .30;
						}
					}
					
				}
				
				$totWeight += .014;
				$totWeight *= 10;
				
				$duties = round($totPrice,2);
				$duties = str_replace('.','',$totPrice);
				
				$cost = round($cost,2);
				$cost *= 1000000;
				$cost = str_replace('.','',$totPrice);
				
				// Update the order's status to 'Shipping'
				$qryUpdate = "UPDATE orders
						SET ordStatus = '9' 
						WHERE ordID = '".$row['ordID']."'";
				$resUpdate = mysql_query($qryUpdate) or print(mysql_error());
				if($resUpdate) {
					if(!setNewLocation( 9 , $row['ordID'] )) print("Unable to record location.");
				}
				
				$fileBuffer .=
'
0,"020"
1,"'.$row['ordID'].'"
11,""
12,"'.substr($row['ordName'],0,35).'"
13,"'.$shipAddress.'"
14,"'.$shipAddress2.'"
15,"'.$shipCity.'"
16,"'.$shipState.'"
17,"'.$shipZip.'"
18,"'.$row['ordPhone'].'"
50,"'.$shipCountry.'"
24,"'.$produce_date.'"
25,"Order:'.$row['ordID'].'"
81,"4202999000"
541,"NNNYNNNNN"
1273,"01"
1274,"18"
1355,"IPD"
116,"1"
21,"'.$totWeight.'"
1030,"'.(($_POST['status']==6)?$cost:'100000').'"
82,"'.$totQuantity.'"
79,"Silicon iPod Cases (HS Code 4202999000)"
80,"CN"
99,""
';
			}
			// Create the new file name
			chdir('batches/'.$statusName);
			$file_not_found=true;
			$temp = rightnow();
			$file_ext = 0;
			do{
				$file_new = 'ifrogz-'.substr($temp,5,2).'-'.substr($temp,8,2).'-'.$file_ext.'.in';
				$inv_ref = substr($temp,5,2).substr($temp,8,2).$file_ext;
				if(is_file($file_new)) $file_ext++;
				else $file_not_found=false;
			}while($file_not_found);
			// Create the trans.in file
			if (!($fp = fopen($file_new, "w+")))  
				exit("Unable to open the input file.");
			fwrite($fp,$fileBuffer);
			fclose($fp);
			chdir('../../vsadmin');
			
			$success = 'Batch '.$file_new.' created';
			header('Location: /admin/shipping.php?success='.$success);
			exit();
		}
	}
}elseif(!empty($_POST['viewStatus'])) {
	if($_POST['viewStatus']=='View USA') $status = 'USA';
	if($_POST['viewStatus']=='View CAN') $status = 'CAN';
	if($_POST['viewStatus']=='View RBI') $status = 'RBI';
	if($_POST['viewStatus']=='View INTL') $status = 'INTL';
	if($_POST['viewStatus']=='View ALL') {
		header('Location: /admin/shipping.php');
		exit();
	}
	
	header('Location: /admin/shipping.php?status='.$status);
	exit();
}

header('Location: /admin/shipping.php');
exit();


function getShipStatus($ordID) {
	$qry = "SELECT ordCountry, ordShipCountry FROM orders WHERE ordID = $ordID";
	$res = mysql_query($qry) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	
	if(empty($row['ordShipCountry'])) $country = $row['ordCountry'];
	else $country = $row['ordShipCountry'];
	
	if($row['ordAPO']=='yes') return '5';
	elseif($country=='United States of America') return '6';
	elseif($country=='Canada') return '7';
	else return '8';
}

function rightnow() {
	return date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s"), date("m"),  date("d"),  date("Y")));
}


function showarray($array) {
    if(is_array($array)) {   
        echo '<ul>';
        foreach($array as $k=>$v) {
            if(is_array($v)) {
                echo '<li>K:'.$k.'</li>';
                showarray($v);
            }else {
                echo '<li>'.$k.'='.$v.'</li>';
            }
        }
        echo '</ul>';
    }else {
        return 'Not an array';
    }
}

?>