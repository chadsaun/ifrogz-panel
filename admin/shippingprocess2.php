<?php
// Checking change
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/functions.php');

session_register('error_order');
session_register('error_msg');
session_register('error_msg2');
session_register('error_status');

// BARCODE SCANNER LOOP AND THEN MANUAL RECEIVE LOOP
if(!empty($_POST['recOrdersScan'])) {
	$_SESSION['error_order'] = array();

	if($_POST['recLocationScan']=='usps'){
		$nextStatus = 7;
	}elseif($_POST['recLocationScan']=='fedex') {
		$nextStatus = 8;
	}else{
		$nextStatus = 9;
	}
	$_SESSION['error_status'] = strtoupper($_POST['recLocationScan']);
	$arr=explode("\r\n",trim($_POST['receive_barcode']));
	
	foreach($arr as $key => $value) {
		$thisStatus = $nextStatus;

		$ordStatus = getShipStatus($value);
		
		if($ordStatus == 'USPS' && $thisStatus != 7) {
			array_push($_SESSION['error_order'],array('ordID' => $value, 'shipType' => 'USPS', 'statusNum' => $thisStatus));
		}elseif($ordStatus == 'FedEx' && $thisStatus != 8) {
			array_push($_SESSION['error_order'],array('ordID' => $value, 'shipType' => 'FedEx Express', 'statusNum' => $thisStatus));
		}elseif($ordStatus == 'INTL' && $thisStatus != 9) {
			array_push($_SESSION['error_order'],array('ordID' => $value, 'shipType' => 'International', 'statusNum' => $thisStatus));
		}else{
			$qry = "UPDATE orders
					SET ordStatus = '$thisStatus', ordStatusDate = '".date("Y-m-d H:i:s", time())."'
					WHERE ordID = '$value'";
			$res = mysql_query($qry) or print(mysql_error());
			if($res) {
				if(!setNewLocation( $thisStatus , $value )) print("Unable to record location.");
			}
		}
	}

	if(!empty($_SESSION['error_order'])) {
		header("Location: /admin/shippingerror.php");
		exit();
	}else{
		$_SESSION['error_order'] = '';
		$_SESSION['error_status'] = '';
	}
}
elseif(!empty($_POST['recOrders'])) { // START OF MANUAL RECEIVE LOOP
	$_SESSION['error_order'] = array();
	$_SESSION['error_status'] = strtoupper($_POST['recLocation']);
	
	foreach($_POST as $key => $value) {
		if($_POST['recLocation']=='usps'){
			$nextStatus = 7;
		}elseif($_POST['recLocation']=='fedex') {
			$nextStatus = 8;
		}else{
			$nextStatus = 6;
		}
		if(substr($key,0,7)=='receive') {
			if($value=='yes') {
				$order = substr($key,7);
				
				$ordStatus = getShipStatus($order);
				
				if($ordStatus == 'USPS' && $nextStatus != 7) {
					array_push($_SESSION['error_order'],array('ordID' => $order, 'shipType' => 'USPS', 'statusNum' => $nextStatus));
				}elseif($ordStatus == 'FedEx' && $nextStatus != 8) {
					array_push($_SESSION['error_order'],array('ordID' => $order, 'shipType' => 'FedEx Express', 'statusNum' => $nextStatus));
				}elseif($ordStatus == 'INTL' && $nextStatus != 9) {
					array_push($_SESSION['error_order'],array('ordID' => $order, 'shipType' => 'International', 'statusNum' => $nextStatus));
				}else{
					$qry = "UPDATE orders
							SET ordStatus = '$nextStatus', ordStatusDate = '".date("Y-m-d H:i:s", time())."'
							WHERE ordID = '$order'";
					$res = mysql_query($qry) or print(mysql_error());
					if($res) {
						if(!setNewLocation( $nextStatus , $order )) print("Unable to record location.");
					}
				}
			}	
		}
	}
	if(!empty($_SESSION['error_order'])) {
		header("Location: /admin/shippingerror.php");
		exit();
	}else{
		$_SESSION['error_order'] = '';
		$_SESSION['error_status'] = '';
	}
}elseif(!empty($_POST['create'])) {
	if($_POST['status']=='7') { // Shipping USPS
		// Validate the BOL#s
		/*if(!ereg("^[[:digit:]|[:space:]]+$",stripslashes($_POST['exBOL']))) {
			$_SESSION['error_msg2'] = 'The Express BOL# can only contain numbers and spaces and must not be empty.';
			header("Location: /admin/shipping2.php");
			exit();
		}
		if(!ereg("^[[:digit:]|[:space:]]+$",stripslashes($_POST['smBOL']))) {
			$_SESSION['error_msg2'] = 'The Smart Mail BOL# can only contain numbers and spaces and must not be empty.';
			header("Location: /admin/shipping2.php");
			exit();
		}*/
		
		// GET ALL THE ORDERS FOR THE SELECTED STATUS
		$qry = "SELECT * FROM orders WHERE ordStatus = '".$_POST['status']."'";
		$res = mysql_query($qry) or print(mysql_error());
		
		if(mysql_num_rows($res)>0) {
			$shipAddress = '';
			$shipAddress2 = '';
			$shipCity = '';
			$shipState = '';
			$shipZip = '';
			$shipCountry = '';
			
			// Create the new file name
			chdir($_SERVER['DOCUMENT_ROOT'] . '/batches/usps');
			$file_not_found=true;
			$temp = rightnow();
			$file_ext = 0;
			do{
				$file_new = 'ifrogz'.$file_ext.'_'.date('ymdH').'.csv';
				//$inv_ref = substr($temp,5,2).substr($temp,8,2).$file_ext;
				if(is_file($file_new)) $file_ext++;
				else $file_not_found=false;
			}while($file_not_found);
			
			$totWeight = 0;
			$totQuantity = 0;
			$csv_str='"","","ordID","","ordName","ordName","shipAddress","shipAddress2","shipCity","shipState","shipZip","weight","","","","",""'."\r\n";
	
			while($row=mysql_fetch_assoc($res)) {
				// Insert the BOL#s into the database for the order
/*				$sql3 = "INSERT INTO bol ( ordID , exBOL , smBOL )
						 VALUES ( ".$row['ordID']." , '".mysql_real_escape_string($_POST['exBOL'])."' , ".mysql_real_escape_string($_POST['smBOL'])." )";
				$res3 = mysql_query($sql3) or print(mysql_error());
*/				
				// Get product information from the order so that we can calculate the total weight of the order and total quantity
				$qry2 = "SELECT cart.cartProdID, p.pWeight, cart.cartQuantity, p.pPricing_group, p.pBinQty
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row['ordID'];
				$res2 = mysql_query($qry2) or print(mysql_error());
				while($row2 = mysql_fetch_assoc($res2)) {
					$weight += $row2['cartQuantity'] * $row2['pWeight'];
					// IF THE PRODUCT IS A SET, MULTIPLY BY 3 TO GET 3 FOR THE QUANTITY INSTEAD OF 1
					if($row2['cartProdID']=='AHX' || $row2['cartProdID']=='BJZ' || $row2['cartProdID']=='AHX') {
						$quantity += ($row2['cartQuantity'])*3;
					}else {
						$quantity += $row2['cartQuantity'];
					}				
					// subtract inventory from any orders with shieldzone products
					if($row2['pPricing_group']=='3'){
						$sql_update="UPDATE products SET pBinQty=pBinQty-".$row2['cartQuantity']." WHERE pID='".$row2['cartProdID']."'";
						$result_plot=mysql_query($sql_update) or print(mysql_error());
						updateplotterhistory($row2['cartProdID'],$row2['pBinQty'],($row2['pBinQty']-$row2['cartQuantity']),$_SESSION['employee']['id'],$file_new);
 					}
				}
				$totWeight += $weight;
				$totQuantity += $quantity;
				
				if(!empty($row['ordShipAddress']) && !empty($row['ordShipZip'])) { // Use Shipping Address
					$shipAddress = $row['ordShipAddress'];
					$shipAddress2 = $row['ordShipAddress2'];
					$shipCity = $row['ordShipCity'];
					$shipState = $row['ordShipState'];
					$shipZip = $row['ordShipZip'];
					$shipCountry = $row['ordShipCountry'];
				}else{ // Use Billing Address
					$shipAddress = $row['ordAddress'];
					$shipAddress2 = $row['ordAddress2'];
					$shipCity = $row['ordCity'];
					$shipState = $row['ordState'];
					$shipZip = $row['ordZip'];
					$shipCountry = $row['ordCountry'];
				}
				
				$csv_str.='"'.$row['ordShipType'].'","","'.$row['ordID'].'","","'.$row['ordName'].'.","'.$row['ordName'].'","'.$shipAddress.'","'.$shipAddress2.'","'.$shipCity.'","'.$shipState.'","'.$shipZip.'","'.$weight.'","","","","",""'."\r\n";
				// Update the order's status to 'Shipping' 
				$qryUpdate = "UPDATE orders
						SET ordStatus = '11', ordStatusDate = '".date("Y-m-d H:i:s")."'
						WHERE ordID = '".$row['ordID']."'";
				$resUpdate = mysql_query($qryUpdate) or print(mysql_error());
				if($resUpdate) {
					//google sent cust status change
					$goid=$row['ordID'];
					$gship='ship';
					include(DOCROOT.'admin/ajaxservice.php');
					
					if(!setNewLocation( 11 , $row['ordID'] )) print("Unbale to record location.");
				
					// SEND CUSTOMER AN EMAIL NOTIFYING THEM THAT THEIR ORDER HAS SHIPPED
					if(!$isgoogleorder) notifyCustShipped($row['ordID']);
				} 
			}
	
			// Create the dat file
			if (!($fp = fopen($file_new, "w+")))  
				exit("Unable to open the input file.");
			fwrite($fp,$csv_str);
			fclose($fp);
			chdir($_SERVER['DOCUMENT_ROOT']);
			
			// FTP the dat file to DHL
			/*$connID = ftpLogin("ftp.smartmail.com","ftpuserrband",'5g$Xpr!4');
			if($connID) {
				if(ftpUpload($connID,$file_new,"batches/dhl/".$file_new)) {
					// Record that the file has been uploaded to DHL
					$sql = "INSERT INTO dhl_files ( filename , status , date )
							VALUES ( '".mysql_real_escape_string($file_new)."' , 'uploaded' , '".date("Y-m-d H:i:s")."' )";
					$res = mysql_query($sql);
				}
			}*/
			
			
			$success = 'Batch '.$file_new.' created';
			header('Location: /admin/shipping2.php?success='.$success);
			exit();
		}
		
	}elseif($_POST['status']=='8') { // FedEx Express
		// GET ALL THE ORDERS FOR THE SELECTED STATUS
		$qry1 = "SELECT * FROM orders WHERE ordStatus = '8'";
		$res1 = mysql_query($qry1) or print(mysql_error());
		
		/*$qry2 = "SELECT * FROM orders WHERE ordStatus = '9'";
		$res2 = mysql_query($qry2) or print(mysql_error());*/
		
		// Create the new file name
		chdir($_SERVER['DOCUMENT_ROOT'] . '/batches/fedex');
		$file_not_found=true;
		$temp = rightnow();
		$file_ext = 0;
		do{
			$file_new = 'ifrogz-'.substr($temp,5,2).'-'.substr($temp,8,2).'-'.$file_ext.'.in';
			$inv_ref = substr($temp,5,2).substr($temp,8,2).$file_ext;
			if(is_file($file_new)) $file_ext++;
			else $file_not_found=false;
		}while($file_not_found);
			
		if(mysql_num_rows($res1)>0 || mysql_num_rows($res2)>0) {
			$shipAddress = '';
			$shipAddress2 = '';
			$shipCity = '';
			$shipState = '';
			$shipZip = '';
			$shipCountry = '';
			
			$produce_date = date("Ymd",mktime(date("H")+14, date("i"), date("s"), date("m"),  date("d"),  date("Y")));
			
			// MASTER RECORD
			$fileBuffer = '';
		/*	$fileBuffer .= "\r\n";
			$fileBuffer .= '0,"020"'."\r\n";
			$fileBuffer .= '1,"MASTER"'."\r\n";
			$fileBuffer .= '19,"IPD-IND"'."\r\n";
			$fileBuffer .= '20,"339676500"'."\r\n";
			$fileBuffer .= '23,"3"'."\r\n";
			$fileBuffer .= '24,"'.$produce_date.'"'."\r\n";
			$fileBuffer .= '31,"New Fortune"'."\r\n";
			$fileBuffer .= '68,"USD"'."\r\n";
			$fileBuffer .= '70,"3"'."\r\n";
			$fileBuffer .= '71,"339676500"'."\r\n";
			$fileBuffer .= '75,"KGS"'."\r\n";
			$fileBuffer .= '79,"Accessory (HS Code 4202190000)"'."\r\n";
			$fileBuffer .= '541,"YNNNNNNNN"'."\r\n";
			$fileBuffer .= '1273,"01"'."\r\n";
			$fileBuffer .= '1274,"18"'."\r\n";
			$fileBuffer .= '1586,"Y"'."\r\n";
			$fileBuffer .= '1486,"ifrogz.com"'."\r\n";
			$fileBuffer .= '1487,"919 N 1000 W"'."\r\n";
			$fileBuffer .= '1488,""'."\r\n";
			$fileBuffer .= '1489,"Logan"'."\r\n";
			$fileBuffer .= '1490,"UT"'."\r\n";
			$fileBuffer .= '1491,"84321"'."\r\n";
			$fileBuffer .= '1492,"1-877-443-7641"'."\r\n";
			$fileBuffer .= '1585,"US"'."\r\n";
			$fileBuffer .= '99,""'."\r\n";*/
			
			while($row1=mysql_fetch_assoc($res1)) {
				if(!empty($row1['ordShipAddress']) && !empty($row1['ordShipZip'])) { // Use Shipping Address
					$name = $row1['ordShipName'];
					$shipAddress = $row1['ordShipAddress'];
					$shipAddress2 = $row1['ordShipAddress2'];
					$shipCity = $row1['ordShipCity'];
					$shipState = $row1['ordShipState'];
					$shipZip = $row1['ordShipZip'];
					$shipCountry = $row1['ordShipCountry'];
				}else{ // Use Billing Address
					$name = $row1['ordName'];
					$shipAddress = $row1['ordAddress'];
					$shipAddress2 = $row1['ordAddress2'];
					$shipCity = $row1['ordCity'];
					$shipState = $row1['ordState'];
					$shipZip = $row1['ordZip'];
					$shipCountry = $row1['ordCountry'];
				}
				
				// GET COUNTRY CODE
				$qry_cc = "SELECT * FROM countries
						   WHERE countryName = '$shipCountry'";
				$res_cc = mysql_query($qry_cc);
				$row_cc = mysql_fetch_assoc($res_cc);
				$shipCountryCode = $row_cc['countryCode'];
				
				
				// Get product information from the order so that we can calculate the total weight of the order and total quantity
				$qry2 = "SELECT p.pWeight, cart.cartQuantity, cart.cartProdID, p.isSet, p.pPricing_group, p.pBinQty, p.pCustomsvalue
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row1['ordID'];
				$res2 = mysql_query($qry2) or print(mysql_error());
				
				$totWeight = 0;
				$totQuantity = 0;
				$totPrice = 0;
				$cost = 0;
				while($row2 = mysql_fetch_assoc($res2)) {
					$totWeight += $row2['cartQuantity'] * $row2['pWeight'];
					$totQuantity += $row2['cartQuantity'];
					$totPrice += $row2['cartProdPrice'];
					$cost += $row2['pCustomsvalue'];
					// subtract inventory from any orders with shieldzone products
					if($row2['pPricing_group']=='3'){
						$sql_update="UPDATE products SET pBinQty=pBinQty-".$row2['cartQuantity']." WHERE pID='".$row2['cartProdID']."'";
						$result_plot=mysql_query($sql_update) or print(mysql_error());
						updateplotterhistory($row2['cartProdID'],$row2['pBinQty'],($row2['pBinQty']-$row2['cartQuantity']),$_SESSION['employee']['id'],$file_new);
 					}
				}
				
				$totWeight += .014;
				$totWeight *= 10;
				
				$duties = round($totPrice,2);
				$duties = str_replace('.','',$totPrice);
				
				if($totQuantity==0) $avg_cost = 0;
				else $avg_cost = $cost / $totQuantity;
				
				// Format for FedEx
				$avg_cost = round($avg_cost,2);
				$avg_cost *= 1000000;
				$avg_cost = str_replace('.','',$avg_cost);
				
				// Update the order's status to 'Shipping'
				$qryUpdate = "UPDATE orders
						SET ordStatus = '11', ordStatusDate = '".date("Y-m-d H:i:s")."'
						WHERE ordID = '".$row1['ordID']."'";
				$resUpdate = mysql_query($qryUpdate) or print(mysql_error());
				if($resUpdate) {
					//google sent cust status change
					$goid=$row1['ordID'];
					$gship='ship';
					include(DOCROOT.'admin/ajaxservice.php');
					
					if(!setNewLocation( 11 , $row1['ordID'] )) print("Unable to record location.");
				}
				
				// SEND CUSTOMER AN EMAIL NOTIFYING THEM THAT THEIR ORDER HAS SHIPPED
				if(!$isgoogleorder) notifyCustShipped($row1['ordID']);
				
				$fileBuffer .= "";
				$fileBuffer .= '0,"021"'."\r\n";
				$fileBuffer .= '1,"'.$row1['ordID'].'"'."\r\n";
				$fileBuffer .= '10,"311882961"'."\r\n";//Sender FedEx Account Number
				$fileBuffer .= '498,"236475"'."\r\n";//Meter Number
				$fileBuffer .= '4,"reminderband.com"'."\r\n";//Sender Company
				$fileBuffer .= '5,"919 N 1000 W"'."\r\n";//Sender Address Line 1
				$fileBuffer .= '6,"Suite 107"'."\r\n";//Sender Address Line 2
				$fileBuffer .= '7,"Logan"'."\r\n";//Sender City
				$fileBuffer .= '8,"UT"'."\r\n";//Sender State
				$fileBuffer .= '9,"84321"'."\r\n";//Sender Postal Code
				$fileBuffer .= '117,"US"'."\r\n";//Sender Country Code
				$fileBuffer .= '183,"8774437641"'."\r\n";//Sender Phone Number
				$fileBuffer .= '12,"'.substr($name,0,35).'"'."\r\n";
				$fileBuffer .= '13,"'.$shipAddress.'"'."\r\n";
				$fileBuffer .= '14,"'.$shipAddress2.'"'."\r\n";
				$fileBuffer .= '15,"'.$shipCity.'"'."\r\n";
				$fileBuffer .= '16,"'.$shipState.'"'."\r\n";
				$fileBuffer .= '17,"'.$shipZip.'"'."\r\n";
				$fileBuffer .= '18,"'.$row1['ordPhone'].'"'."\r\n";
				$fileBuffer .= '50,"'.$shipCountryCode.'"'."\r\n";
				$fileBuffer .= '23,"1"'."\r\n";//Pay Type
				$fileBuffer .= '24,"'.$produce_date.'"'."\r\n";//Ship Date
				$fileBuffer .= '25,"Order:'.$row1['ordID'].'"'."\r\n";//Reference Information
				//$fileBuffer .= '116,"1"'."\r\n";//Package Total(Total Number of Pieces)
				$fileBuffer .= '75,"KGS"'."\r\n";//weight units
				//$fileBuffer .= '1273,"01"'."\r\n";//package type
				$fileBuffer .= '3025,"FDXE"'."\r\n";//Carrier Code
				$fileBuffer .= '99,""'."\r\n";
				//$fileBuffer .= '1274,"18"'."\r\n";//service type
				//$fileBuffer .= '21,"'.$totWeight.'"'."\r\n";//weight
				//$fileBuffer .= '1030,"'.$avg_cost.'"'."\r\n";
				//$fileBuffer .= '82,"'.$totQuantity.'"'."\r\n";//unit quantity
				
				
				
				/*$fileBuffer .= "\r\n";
				$fileBuffer .= '0,"020"'."\r\n";
				$fileBuffer .= '1,"'.$row1['ordID'].'"'."\r\n";
				$fileBuffer .= '11,""'."\r\n";
				$fileBuffer .= '12,"'.substr($name,0,35).'"'."\r\n";
				$fileBuffer .= '13,"'.$shipAddress.'"'."\r\n";
				$fileBuffer .= '14,"'.$shipAddress2.'"'."\r\n";
				$fileBuffer .= '15,"'.$shipCity.'"'."\r\n";
				$fileBuffer .= '16,"'.$shipState.'"'."\r\n";
				$fileBuffer .= '17,"'.$shipZip.'"'."\r\n";
				$fileBuffer .= '18,"'.$row1['ordPhone'].'"'."\r\n";
				$fileBuffer .= '50,"'.$shipCountryCode.'"'."\r\n";
				$fileBuffer .= '24,"'.$produce_date.'"'."\r\n";
				$fileBuffer .= '25,"Order:'.$row1['ordID'].'"'."\r\n";//Reference Information
				$fileBuffer .= '81,"4202190000"'."\r\n";//Harmonized Code
				$fileBuffer .= '541,"NNNYNNNNN"'."\r\n";
				$fileBuffer .= '1273,"01"'."\r\n";//package type
				$fileBuffer .= '1274,"18"'."\r\n";//service type
				$fileBuffer .= '3025,"FDXE"'."\r\n";
				$fileBuffer .= '116,"1"'."\r\n";//Package Total(Total Number of Pieces)
				$fileBuffer .= '21,"'.$totWeight.'"'."\r\n";//weight
				$fileBuffer .= '1030,"'.$avg_cost.'"'."\r\n";
				$fileBuffer .= '82,"'.$totQuantity.'"'."\r\n";//unit quantity
				$fileBuffer .= '79,"Accessory (HS Code 4202190000)"'."\r\n";
				$fileBuffer .= '80,"CN"'."\r\n";
				$fileBuffer .= '99,""'."\r\n";*/
				
			}
			
			// CREATE FEDEX INTERNATIONAL RECORD
			$sql_intl = "SELECT * FROM orders WHERE ordStatus = 9";
			$res_intl = mysql_query($sql_intl) or print(mysql_error());
			
			$totWeight = 0;
			$totQuantity = 0;
			$totPrice = 0;
			$cost = 0;
			while($row_intl = mysql_fetch_assoc($res_intl)) {
				// Get product information from the order so that we can calculate the total weight of the order and total quantity
				$qry2 = "SELECT p.pWeight, cart.cartQuantity, cart.cartProdID, p.isSet, p.pPricing_group, p.pBinQty, p.pCustomsvalue
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row_intl['ordID'];
				$res2 = mysql_query($qry2) or print(mysql_error());
				
				while($row2 = mysql_fetch_assoc($res2)) {
					$totWeight += $row2['cartQuantity'] * $row2['pWeight'];
					$totQuantity += $row2['cartQuantity'];
					$totPrice += $row2['cartProdPrice'];
					$cost += $row2['pCustomsvalue'];
					// subtract inventory from any orders with shieldzone products
					if($row2['pPricing_group']=='3'){
						$sql_update="UPDATE products SET pBinQty=pBinQty-".$row2['cartQuantity']." WHERE pID='".$row2['cartProdID']."'";
						$result_plot=mysql_query($sql_update) or print(mysql_error());
						updateplotterhistory($row2['cartProdID'],$row2['pBinQty'],($row2['pBinQty']-$row2['cartQuantity']),$_SESSION['employee']['id'],$file_new);
 					}
				}
				
				// Update the order's status to 'Shipping'
				$qryUpdate = "UPDATE orders
						SET ordStatus = '11', ordStatusDate = '".date("Y-m-d H:i:s")."'
						WHERE ordID = '".$row_intl['ordID']."'";
				$resUpdate = mysql_query($qryUpdate) or print(mysql_error());
				if($resUpdate) {
					//google sent cust status change
					$goid=$row_intl['ordID'];
					$gship='ship';
					include(DOCROOT.'admin/ajaxservice.php');
					if(!setNewLocation( 11 , $row_intl['ordID'] )) print("Unable to record location.");
				}
				
				// SEND CUSTOMER AN EMAIL NOTIFYING THEM THAT THEIR ORDER HAS SHIPPED
				if(!$isgoogleorder)notifyCustShipped($row_intl['ordID']);
			}
			
			$totWeight += .014;
			$totWeight *= 10;
			
			if($totQuantity==0) $avg_cost = 0;
			else $avg_cost = $cost / $totQuantity;
			
			// Format for FedEx
			$avg_cost = round($avg_cost,2);
			$avg_cost *= 1000000;
			$avg_cost = str_replace('.','',$avg_cost);
			
			if(mysql_num_rows($res_intl) > 0) {
				$fileBuffer .= "\r\n";
				$fileBuffer .= '0,"020"'."\r\n";
				$fileBuffer .= '1,"'.date("ndHis").'.in"'."\r\n";
				$fileBuffer .= '11,""'."\r\n";
				$fileBuffer .= '12,"Martin Cordero"'."\r\n";
				$fileBuffer .= '13,"FIMS Mail Center"'."\r\n";
				$fileBuffer .= '14,"700 Dowd Avenue"'."\r\n";
				$fileBuffer .= '15,"Elizabeth"'."\r\n";
				$fileBuffer .= '16,"New Jersey"'."\r\n";
				$fileBuffer .= '17,"07201"'."\r\n";
				$fileBuffer .= '18,""'."\r\n";
				$fileBuffer .= '50,"US"'."\r\n";
				$fileBuffer .= '24,"'.$produce_date.'"'."\r\n";
				$fileBuffer .= '81,"4202190000"'."\r\n";
				$fileBuffer .= '541,"NNNYNNNNN"'."\r\n";
				$fileBuffer .= '1273,"01"'."\r\n";
				$fileBuffer .= '1274,"18"'."\r\n";
				$fileBuffer .= '1355,"IPD"'."\r\n";
				$fileBuffer .= '116,"1"'."\r\n";
				$fileBuffer .= '21,"'.$totWeight.'"'."\r\n";
				$fileBuffer .= '1030,"'.$avg_cost.'"'."\r\n";
				$fileBuffer .= '82,"'.$totQuantity.'"'."\r\n";
				$fileBuffer .= '79,"Accessory (HS Code 4202190000)"'."\r\n";
				$fileBuffer .= '80,"CN"'."\r\n";
				$fileBuffer .= '99,""'."\r\n";
			}
			
			// Create the trans.in file
			if (!($fp = fopen($file_new, "w+")))  
				exit("Unable to open the input file.");
			fwrite($fp,$fileBuffer);
			fclose($fp);
			chdir($_SERVER['DOCUMENT_ROOT']);
			
			$success = 'Batch '.$file_new.' created';
			header('Location: /admin/shipping2.php?status=FedEx&success='.$success);
			exit();
		}
	}else{ // INTL
		// Validate the BOL#s
		if(!ereg("^[[:digit:]|[:space:]]+$",$_POST['exBOL'])) {
			$_SESSION['error_msg2'] = 'The Express BOL# can only contain numbers and spaces and must not be empty.';
			header("Location: /admin/shipping2.php");
			exit();
		}
		if(!ereg("^[[:digit:]|[:space:]]+$",$_POST['gmBOL'])) {
			$_SESSION['error_msg2'] = 'The Global Mail BOL# can only contain numbers and spaces and must not be empty.';
			header("Location: /admin/shipping2.php");
			exit();
		}
		
		// GET ALL THE ORDERS FOR THE SELECTED STATUS
		$qry = "SELECT * FROM orders WHERE ordStatus = '".$_POST['status']."'";
		$res = mysql_query($qry) or print(mysql_error());

		if(mysql_num_rows($res)>0) {
			$shipAddress = '';
			$shipAddress2 = '';
			$shipCity = '';
			$shipState = '';
			$shipZip = '';
			$shipCountry = '';
			
			$GMAcct = '12345678';
			
			// Create the new file name
			chdir($_SERVER['DOCUMENT_ROOT'] . '/batches/intl');
			$file_not_found=true;
			$temp = rightnow();
			$file_ext = 0;
			do{
				$file_new = $GMAcct.'_'.date('Ymd').'_parcel'.$file_ext.'.dat';
				//$inv_ref = substr($temp,5,2).substr($temp,8,2).$file_ext;
				if(is_file($file_new)) $file_ext++;
				else $file_not_found=false;
			}while($file_not_found);
			
			$strCSV = '';
			while($row=mysql_fetch_assoc($res)) {
				// Insert the BOL#s into the database for the order
				$sql3 = "INSERT INTO bol ( ordID , exBOL , gmBOL )
						 VALUES ( ".$row['ordID']." , '".mysql_real_escape_string($_POST['exBOL'])."' , ".mysql_real_escape_string($_POST['gmBOL'])." )";
				$res3 = mysql_query($sql3) or print(mysql_error());
			
				if(!empty($row['ordShipAddress']) && !empty($row['ordShipZip'])) { // Use Shipping Address
					$shipAddress = $row['ordShipAddress'];
					$shipAddress2 = $row['ordShipAddress2'];
					$shipCity = $row['ordShipCity'];
					$shipState = $row['ordShipState'];
					$shipZip = $row['ordShipZip'];
					$shipCountry = $row['ordShipCountry'];
				}else{ // Use Billing Address
					$shipAddress = $row['ordAddress'];
					$shipAddress2 = $row['ordAddress2'];
					$shipCity = $row['ordCity'];
					$shipState = $row['ordState'];
					$shipZip = $row['ordZip'];
					$shipCountry = $row['ordCountry'];
				}
				
				// Get the country code
				$sql = "SELECT * FROM countries WHERE countryName = '".$shipCountry."'";
				$ctry_res = mysql_query($sql) or print(mysql_error());
				$ctry_row = mysql_fetch_assoc($ctry_res);
				$ctryCode = $ctry_row['countryCode'];
				
				// Get product information from the order so that we can calculate the total weight of the order
				$qry2 = "SELECT p.pWeight, p.pPrice, cart.cartQuantity, cart.cartProdID, p.isSet, p.pPricing_group, p.pBinQty
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row['ordID'];
				$res2 = mysql_query($qry2) or print(mysql_error());
				
				$totWeight = 0;
				$totQuantity = 0;
				while($row2 = mysql_fetch_assoc($res2)) {
					// IF THE PRODUCT IS A SET, MULTIPLY BY 3 TO GET 3 FOR THE QUANTITY INSTEAD OF 1
					if($row2['isSet']=='yes') {
						$totQuantity += $row2['cartQuantity']*3;
					}else {
						$totQuantity += $row2['cartQuantity'];
					}
					$totWeight += $row2['cartQuantity'] * $row2['pWeight'];
					// subtract inventory from any orders with shieldzone products
					if($row2['pPricing_group']=='3'){
						$sql_update="UPDATE products SET pBinQty=pBinQty-".$row2['cartQuantity']." WHERE pID='".$row2['cartProdID']."'";
						$result_plot=mysql_query($sql_update) or print(mysql_error());
						updateplotterhistory($row2['cartProdID'],$row2['pBinQty'],($row2['pBinQty']-$row2['cartQuantity']),$_SESSION['employee']['id'],$file_new);
 					}
				}
				
				$qry3 = "SELECT p.pWeight, cart.cartProdName, cart.cartQuantity, cart.cartProdID, p.isSet
						 FROM cart, products p, orders o
						 WHERE o.ordID = cart.cartOrderID
						 AND cart.cartProdID = p.pID
						 AND o.ordID = ".$row['ordID'];
				$res3 = mysql_query($qry3) or print(mysql_error());
				
				// LOOP THROUGH EACH ITEM OF THE ORDER
				while($row3=mysql_fetch_assoc($res3)) {
					$strCSV .= $GMAcct."|".$row['ordID']."|".$row['ordName']."|".$shipAddress."|".$shipAddress2."||".$shipCity."|".$shipState."|".$shipZip."|".$row['ordPhone']."|".$row['ordEmail']."|".$ctryCode."|".$shipCountry."|".$shipCountry."|MP3 Carrying Case|".$row['ordTotal']."|".$totWeight."|0|".$row3['cartQuantity']."|".$row3['cartProdID']."|".$row3['cartProdName']."|".$row3['pPrice']."||\r\n";
				}
			
				// Update the order's status to 'Shipping'
				$qryUpdate = "UPDATE orders
						SET ordStatus = '11', ordStatusDate = '".date("Y-m-d H:i:s")."'
						WHERE ordID = '".$row['ordID']."'";
				$resUpdate = mysql_query($qryUpdate) or print(mysql_error());
				if($resUpdate) {
					//google sent cust status change
					$goid=$row['ordID'];
					$gship='ship';
					include(DOCROOT.'admin/ajaxservice.php');

					if(!setNewLocation( 11 , $row['ordID'] )) print("Unable to record location.");
				}
				
				// SEND CUSTOMER AN EMAIL NOTIFYING THEM THAT THEIR ORDER HAS SHIPPED
				if(!$isgoogleorder)notifyCustShipped($row['ordID']);
			}
			// Create the dat file
			if (!($fp = fopen($file_new, "w+")))  
				exit("Unable to open the input file.");
			fwrite($fp,$strCSV);
			fclose($fp);
			chdir($_SERVER['DOCUMENT_ROOT']);
			
			$success = 'Batch '.$file_new.' created';
			header('Location: /admin/shipping2.php?success='.$success);
			exit();
		}
	}
} else if(!empty($_POST['viewStatus'])) {
	if($_POST['viewStatus']=='View USPS') $status = 'USPS';
	if($_POST['viewStatus']=='View FedEx') $status = 'FedEx';
	if($_POST['viewStatus']=='View INTL') $status = 'INTL';
	
	header('Location: /admin/shipping2.php?status='.$status);
	exit();
}

header('Location: /admin/shipping2.php');
exit();

function getShipStatus($ordID) {
	$apo = false;
	$aAPOStates = array('AE','AA','AP');
	$country = '';

	$qry = "SELECT ordAddress, ordShipAddress, ordCountry, ordShipCountry, ordState, ordShipState, ordShipType, ordPoAPO, ordShipPoApo 
			FROM orders 
			WHERE ordID = $ordID";
	$res = mysql_query($qry) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	
	if(!empty($row['ordShipAddress'])) {
		$state = $row['ordShipState'];
		$country = $row['ordShipCountry'];
		$apo = $row['ordShipPoApo'];
		if(in_array($state,$aAPOStates)) { // IF A MILITARY STATE
			$apo = true;
		}
	}else{
		$state = $row['ordState'];
		$country = $row['ordCountry'];
		$apo = $row['ordPoApo'];
		if(in_array($state,$aAPOStates)) { // IF A MILITARY STATE
			$apo = true;
		}
	}
	
	// Get Postal Zone information
	$sql2 = "SELECT pz.pzName
			 FROM countries c, postalzones pz 
			 WHERE c.countryZone = pz.pzID
			 AND c.countryName = '" . $country . "'";
	$res2 = mysql_query($sql2) or print(mysql_error());
	$row2 = mysql_fetch_assoc($res2);

	if($apo) {
		return 'USPS';
	}elseif($row2['countryZone'] == 'United States') {
		if($row['ordShipType']=='FedEx Express') {	
			return 'FedEx';
		}elseif($row['ordShipType']=='FedEx Overnight') {	
			return 'FedEx';
		}else{
			return 'USPS';
		}
	}elseif($row2['countryZone'] == 'Canada') {
		return 'USPS';
	}elseif($row2['countryZone'] == 'International') {
		return 'USPS';
	}elseif($row2['countryZone'] == 'US Territories') {
		return 'USPS';
	}		
}

function rightnow() {
	return date("Y-m-d H:i:s",mktime(date("H"), date("i"), date("s"), date("m"),  date("d"),  date("Y")));
}



function ftpLogin($host,$user,$pass) {
	$ftp = ftp_connect($host);
	if ($ftp) {
		if (ftp_login($ftp,$user,$pass)) {
			return $ftp;
		}else{
			ftp_quit($connID);
			return false;
		}
	}else{
		ftp_quit($connID);
		return false;
	}
}

function ftpUpload($connID,$destFile,$srcFile) {
	if(ftp_put($connID, $destFile, $srcFile, FTP_ASCII)) {
		ftp_quit($connID);
		return true;
	}else{
		ftp_quit($connID);
		return false;
	}
}

function cTrim(&$value) {
	$value = trim($value); // Trim line breaks
	$value = trim($value,'"'); // Trim off quotes
}



?>