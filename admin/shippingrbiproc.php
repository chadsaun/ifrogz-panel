<?php
session_start();

if($_GET['action']=='parse') {
	$data = parseDHLResponse($dhlFile);
	
	// ALL THE FIELDS IN THE $data ARRAY THAT ARE A NUMBER DATA TYPE
	$strNumFields = ",0,1,11,26,27,";
	
	// ALL THE FIELDS IN THE $data ARRAY THAT ARE A DATE DATA TYPE
	$strDteFields = ",28,30,";

	for($i=0; $i<count($data['orders']); $i++) {
		// INSERT ALL INFORMATION INTO THE DATABASE
		$qry = "INSERT INTO dhl (custFileID,custIDDHLGM,custPackID,custDelConfIDUSPS,mailDestName,mailDestFirm,mailDestAdd1,mailDestAdd2,mailDestCity,mailDestState,mailDestZip,mailWeightLBS,mailRef1,mailRef2,mailRef3,mailRef4,mailRef5,DHLGMTrackNum,delConfID,encDestName,encDestFirm,encDestAdd1,encDestAdd2,encDestCity,encDestState,encDestZip,actWeight,postageWeight,billingDate,mailClass,processDate,CASSCertified,ACECode,status)
				VALUES (";
		for($j=0; $j<count($data['orders'][$i]['fields']); $j++) {
			if($j!=0) {
				$qry .= ",";
			}
			if(strstr($strNumFields,",$j,")) {
				$qry .= $data['orders'][$i]['fields'][$j];
			}elseif(strstr($strDteFields,",$j,")) {
				$newDate = date('Y-m-d H:i:s',strtotime($data['orders'][$i]['fields'][$j]));
				$qry .= "'".$newDate."'";
			}else{
				$qry .= "'".$data['orders'][$i]['fields'][$j]."'";
			}
		}
		$qry .= ")";
		//$res = mysql_query($qry) or print(mysql_error());
		
		// UPDATE THE ORDER'S STATUS TO SHIPPED
		$qry = "UPDATE orders SET ordStatus = 9 WHERE ordID = ".$data['orders'][$i]['fields'][2];
		//$res = mysql_query($qry) or print(mysql_error());
		
		// SEND OUT AN EMAIL TO CUSTOMER INFORMING THEM THAT THEIR ORDER HAS BEEN SHIPPED
		$qry = "SELECT ordEmail FROM orders WHERE ordID=".$data['orders'][$i]['fields'][2];
		//$res = mysql_query($qry) or print(mysql_error());
		//$row = mysql_fetch_assoc($res);
		
		$custEmail = $row['ordEmail'];
		$emailSubject = "Your ifrogz Order has been Shipped";
		$message = "<p>Your ifrogz order ".$data['orders'][$i]['fields'][2]." has been shipped. You can expect your order to arrive in 3-6 business days.</p><p>If you have any questions, please email us at support@ifrogz.com.</p>";
		
		$customheaders = "MIME-Version: 1.0\n";
		$customheaders .= "From: ifrogz <support@ifrogz.com>\n";
		$customheaders .= "Content-type: text/html; charset=iso-8859-1\n";
		
		$headers = str_replace('%from%',$emailAddr,$customheaders);
		$headers = str_replace('%to%',$ordemail,$headers);
		//mail($custEmail, $emailSubject, $message, $headers);
		$_SESSION['shipMessage'] = $dhlFile." has been parsed.";
	}
}

header('Location: /admin/shippingrbi.php');
exit();

function parseDHLResponse($filename,$path="") {
	// VALIDATE PARAMETERS
	if(empty($filename)) {
		return false;
	}
	
	// GET THE CONTENTS OF THE FILE
	$file = file_get_contents('../dhl/From_SMail/'.$filename);
	
	// TRIM OFF EXTRA LINE BREAKS
	$file = trim($file);
	
	// ASSIGN EACH LINE (ORDER) INTO AN ARRAY
	$arrOrders = explode("\n",$file);
	
	// ASSIGN EACH FIELD TO ITS ORDER
	for($i=0; $i<count($arrOrders); $i++) {
		$arrContents['orders'][$i]['order'] = $arrOrders[$i];
		$arrFields = explode(",",$arrContents['orders'][$i]['order']);
		
		// TRIM ALL THE QUOTES OFF ALL THE VALUES
		$arrFields = array_map('trimValue',$arrFields);
		$arrContents['orders'][$i]['fields'] = $arrFields;
	}	
	return $arrContents;	
}

function trimValue($value) {
	$value = trim($value,'"');
	return $value;
}

function showarray($array)
{
    if(is_array($array)) {   
        echo '<ul>';
        foreach($array as $k=>$v) {
            if(is_array($v)) {
                echo '<li>K:'.$k.'</li>';
                showarray($v);
            }else {
                echo '<li>'.$k.'='.$v.'</li>';
            }
        }
        echo '</ul>';
    }else {
        return 'Not an array';
    }
}
?>