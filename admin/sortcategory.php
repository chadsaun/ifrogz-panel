<?php
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');

if (count($_POST['data']) > 0 && !empty($_POST['section_id'])) {
	// Clear out all rows
	$sql = "DELETE FROM RFCategoryOrder WHERE Section_IDParent = {$_POST['section_id']}";
	$res = mysql_query($sql) or print(mysql_error());
	if ($res) {
		foreach ($_POST['data'] as $key => $value) {	
			$type = '';
			$column = '';
			$sequence = $key + 1;

			// Split the value
			$tmp = explode('_', $value);

			$type = $tmp[0];
			$id = $tmp[1];

			if ($type == 'section') {
				$column = 'Section_ID';
			} else {
				$column = 'Product_ID';
			}

			if ($type == 'section') {
				$sql = "INSERT INTO RFCategoryOrder ( Section_IDParent, Section_ID, Sequence ) VALUES ( {$_POST['section_id']} , {$id}, {$sequence} )";
			} else {
				$sql = "INSERT INTO RFCategoryOrder ( Section_IDParent, Product_ID, Sequence ) VALUES ( {$_POST['section_id']} , {$id}, {$sequence} )";
			}
			$res = mysql_query($sql) or print(mysql_error());
		}
	}
}
