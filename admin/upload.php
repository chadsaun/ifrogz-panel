<?php
//	---------------------------------------------
//	Pure PHP Upload version 1.1
//	-------------------------------------------

if (phpversion() > "4.0.6") {
	$HTTP_POST_FILES = &$_FILES;
}
define("MAX_SIZE",300000);
define("DESTINATION_FOLDER", "upload_images");
define("no_error", "loadflash.php?$_name_");
define("yes_error", "/admin/fileselect.php?$errStr");
$_accepted_extensions_ = "jpg";
if(strlen($_accepted_extensions_) > 0){
	$_accepted_extensions_ = @explode(",",$_accepted_extensions_);
} else {
	$_accepted_extensions_ = array();
}
/*	modify */
if(!empty($HTTP_POST_FILES['file'])){
	if(is_uploaded_file($HTTP_POST_FILES['file']['tmp_name']) && $HTTP_POST_FILES['file']['error'] == 0){
		$_file_ = $HTTP_POST_FILES['file'];
		$errStr = "";
		$_name_ = $_file_['name'];
		$_type_ = $_file_['type'];
		$_tmp_name_ = $_file_['tmp_name'];
		$_size_ = $_file_['size'];
		if($_size_ > MAX_SIZE && MAX_SIZE > 0){
			$errStr = "File too large!";
		}
		$_ext_ = explode(".", $_name_);
		$_ext_ = strtolower($_ext_[count($_ext_)-1]);
		if(!in_array($_ext_, $_accepted_extensions_) && count($_accepted_extensions_) > 0){
			$errStr = "This file type is not accepted!";
		}
		if(!is_dir(DESTINATION_FOLDER) && is_writeable(DESTINATION_FOLDER)){
			$errStr = "Current folder is not writeable!";
		}
		if(empty($errStr)){
			if(@copy($_tmp_name_,DESTINATION_FOLDER . "/" . $_name_)){
				header("Location: " . no_error);
			} else {
				header("Location: " . yes_error);
			}
		} else {
			header("Location: " . yes_error);
		}
	}
}
?>