<?php
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');

$upc=$_GET['pUPC'];
if(!empty($upc)){
	$sql="SELECT * FROM products WHERE pPricing_group=3 AND pUPC='$upc'";
	//echo $sql;
	$result=mysql_query($sql);
	if(mysql_num_rows($result)>0){
		$row=mysql_fetch_assoc($result);
		$pname=$row['pName'];		
		$pbin=$row['pBin'];
		$pid=$row['pID'];
		$pinv=$row['pBinQty'];
		$ailsstupload=$row['pLastAiUpload'];
		$upc=$row['pUPC'];
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Weeding Chart for <?=$pid?></title>
</head>
<body>
<table width="680" border="0" height="970" style="border-bottom:1px dotted #000000;border-right:1px dotted #000000;">
  <tr>
    <td height="12" colspan="2" valign="baseline"><strong>Weeding Chart </strong></td>
  </tr>
  <tr>
    <td width="442" height="450" align="center" valign="top"><img src="/plotter/<? echo str_replace(' ','-',$pid)?>.jpg" /></td>
    <td width="227" align="center" valign="top"><table width="100%" border="0">
      <tr>
        <td><div style="font-size:18px; font-weight:bold;">
          <div align="center">
            <?=$pbin?>
            </div>
        </div></td>
      </tr>
      <tr>
        <td><div align="center"><img src="/barcode/wrapper.php?p_bcType=2&amp;p_text=<?=$upc?>&amp;p_xDim=1&amp;p_w2n=3&amp;p_charGap=1&amp;p_invert=N&amp;p_charHeight=40&amp;p_type=1&amp;p_label=Y&amp;p_rotAngle=0&amp;p_checkDigit=N" alt="1234567890" /></div></td>
      </tr>
      <tr>
        <td><div style="font-size:18px; font-weight:bold;">
          <div align="center">
            <?=$pname?>
            </div>
        </div></td>
      </tr>
      <tr>
        <td><div style="font-size:18px; font-weight:bold;">
          <div align="center">
            <?=$pid?>
            </div>
        </div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="12" colspan="2">
	<table width="100%" border="0">
  <tr>
    <td><img src="/barcode/wrapper.php?p_bcType=2&amp;p_text=<?=$upc?>&amp;p_xDim=1&amp;p_w2n=3&amp;p_charGap=1&amp;p_invert=N&amp;p_charHeight=30&amp;p_type=1&amp;p_label=N&amp;p_rotAngle=0&amp;p_checkDigit=N" alt="1234567890" /></td>
    <td><div style="font-size:14px; font-weight:bold;">
      <?=$pid?>
    </div></td>
    <td><div style="font-size:14px; font-weight:bold;">
      <?=$pname?>
    </div></td>
    <td><div style="font-size:18px; font-weight:bold;">
      <div align="right">
        <?=$pbin?>
        </div>
    </div></td>
    <td width="10">&nbsp;</td>
  </tr>
</table>	</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
</body>
</html>