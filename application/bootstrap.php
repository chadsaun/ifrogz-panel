<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('America/Boise');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']) && ($_SERVER['KOHANA_ENV'] != 'UNDEFINED')) {
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
} else if (strstr($_SERVER['HTTP_HOST'], ':8888') && !preg_match('/^admin\.ifrogz\.com$/i', $_SERVER['HTTP_HOST'])) { // i.e. localhost
	Kohana::$environment = Kohana::DEVELOPMENT;
} else if (preg_match('/^dev\.admin\.ifrogz\.com$/i', $_SERVER['HTTP_HOST']) || preg_match('/^panel\.ifrogz\.com$/i', $_SERVER['HTTP_HOST'])) { // i.e. dev site
	Kohana::$environment = Kohana::STAGING;
} else { // i.e. live site
	Kohana::$environment = Kohana::PRODUCTION;
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
$settings = array(
	'base_url' => '/',
	'index_file' => FALSE
);
switch (Kohana::$environment) {
	case Kohana::PRODUCTION:
		$settings += array(
			'profiling' => FALSE,
			'caching' => TRUE
		);
	break;
	default:
		$settings += array(
			'profiling' => TRUE,
			'caching' => FALSE
		);
	break;
}
Kohana::init($settings);

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
	// FirePHP Needs to be before database in order to get query data
	'firephp'	        => MODPATH.'firephp',           // FirePHP library
	'amazon'		    => MODPATH.'amazon',		    // Manages Amazon AWS (including EC2, SQS, S3, and AWIS)
	'barcode'           => MODPATH.'barcode',           // Barcode generators and readers
	'cache-ext'         => MODPATH.'cache-ext',         // Caching (extensions)
	'cache'             => MODPATH.'cache',             // Caching
	'codebench'         => MODPATH.'codebench',         // Benchmarking
	'collection'        => MODPATH.'collection',        // Collection classes
	'common'            => MODPATH.'common',            // Common classes that are used by all sites
	'company'		    => MODPATH.'company',			// Company specific classes
	'database-ext'	    => MODPATH.'database-ext',      // Database access (extensions)
	'database'          => MODPATH.'database',          // Database access
	'datatype'          => MODPATH.'datatype',          // Special data types
	'debug'             => MODPATH.'debug',             // Classes for debugging
	'di'                => MODPATH.'di',                // Dependency injection
	'document'		    => MODPATH.'document',		    // Generates/parses documents
	'eric'			    => MODPATH.'eric',		        // The New PDF Module
	'exception'         => MODPATH.'exception',         // Common exceptions
	'filter'            => MODPATH.'filter',            // Intercepting filters
	'formula'		    => MODPATH.'formula',		    // Formulas for converting UOMs
	'jelly-ext'         => MODPATH.'jelly-ext',         // Object Relationship Mapping (extensions)
	'jelly'             => MODPATH.'jelly',             // Object Relationship Mapping
	'language'          => MODPATH.'language',          // Multilingual support
	'leap'      		=> MODPATH.'leap',      		// Object Relationship Mapping
	'log'               => MODPATH.'log',               // More detailed log
	'messaging'         => MODPATH.'messaging',         // Handles email messages and sms
	'orm-ext'           => MODPATH.'orm-ext',           // Object Relationship Mapping (extensions)
	'orm'               => MODPATH.'orm',               // Object Relationship Mapping
	'pagination-ext'    => MODPATH.'pagination-ext',    // Paging of results (extensions)
	'pagination'        => MODPATH.'pagination',        // Paging of results
	'rest'              => MODPATH.'rest',              // Handles response messages
	'security'          => MODPATH.'security',          // Security
	'system-ext'	    => MODPATH.'system-ext',        // Core Kohana classes (extensions)
	'table'             => MODPATH.'table',             // HTML Table Class
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	'utility'		    => MODPATH.'utility'            // Common utility classes
    ));

/**
 * The cookie class now throws an exception if there isn't a salt set, and no salt is the now the default. You'll
 * need to make sure you set the salt in your bootstrap:
 */
Cookie::$salt = 'kooky';

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
//Kohana::$log->attach(new Kohana_Log_File(APPPATH.'logs'));
Kohana::$log->attach(new Log_File_Writer(APPPATH.'logs'));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set('keycodes', 'keycodes(/<page>)')
    ->defaults(array(
        'controller' => 'keycodes',
        'action' => 'index'
    ));
Route::set('log', 'log(/<date>)', array('date' => '[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}'))
	->defaults(array(
		'controller' => 'log',
		'action' => 'display',
	));
Route::set('error', 'error/<action>/<origuri>/<message>', array('action' => '[0-9]++', 'origuri' => '.+', 'message' => '.+'))
    ->defaults(array(
        'controller' => 'error',
	    'action'	 => 'index'
    ));
Route::set('default', '(<controller>(/<action>(/<id>(/<stuff>))))', array('stuff' => '.*'))
 	->defaults(array(
 		'controller' => 'home',
 		'action'     => 'index',
 	));
Route::set('ericpdf', '<controller>/<action>(/<id>)', array('id' => '.*'))
	->defaults(array(
		'controller' => 'ericpdf',
		'action' => 'index',
	));
?>