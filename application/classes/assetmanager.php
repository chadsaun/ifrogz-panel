<?php defined('SYSPATH') or die('No direct script access.');

/**
 * This class manages assets.
 *
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2011-08-17
 * @copyright (c) 2011, iFrogz (a subsidiary of ZAGG, Inc.)
 * @package cache-ext
 * @category Asset
 */
class AssetManager extends Base_AssetManager {

	/**
	 * This function sets the specified asset.
	 *
	 * @access public
	 * @param Array $asset							the asset to be set
	 */
	public function set_asset(Array $asset) {
		if (isset($asset['uri'])) {
			if (stristr($asset['uri'], 'assets.ifrogz.com')) {
				$asset['uri'] = str_ireplace('assets.ifrogz.com', 'd3ayai91z5ha65.cloudfront.net', $asset['uri']);
			}
		}

		parent::set_asset($asset);
	}

}