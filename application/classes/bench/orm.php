<?php defined('SYSPATH') or die('No direct script access.');

class Bench_ORM extends Codebench {

	public $description = 'Querying methods: ORM vs LEAP vs Jelly';

	public $loops = 240;

	public $subjects = array(
		38,
		1,
		2,
		3,
	);

    public function bench_k3orm($subject) {
    	return ORM::factory('admin_employee')->where('id', '=', $subject)->find()->id;
    }

	public function bench_jelly($subject) {
		return Jelly::select('admin_employee', $subject)->id;
	}

	public function bench_leap($subject) {
		return DB_ORM::select('admin_employee')->where('id', '=', $subject)->query()->get('id');
	}

}
?>