<?php defined('SYSPATH') or die('No direct script access.');

/**
* Cart Class
*
* A Shopping cart class based off of CodeIgniter's Cart class and re-written for Kohana 3.
* It was also altered to just interact with the iFrogz cart database tables, instead of
* using a session variable.
*
* @link		http://codeigniter.com/user_guide/libraries/cart.html
*/
class Cart
{

	public $product_id_rules		= '\.a-z0-9_-'; // alpha-numeric, dashes, underscores, or periods
	public $product_name_rules		= '\.\:\-_ a-z0-9\[\]&\/'; // alpha-numeric, dashes, underscores, colons, periods, or forward slashes
	public $total_items				= 0;
	public $total_weight			= 0;
	public $order_id				= NULL;

    public $_coupon_code			= NULL; // TODO turn back to protected .... being used in cart controller right now
	protected $_certificate_code	= NULL;
	protected $_certificate_amount  = 0.00;

	protected $_sub_total			= 0.00;
	protected $_discount			= 0.00;
	protected $_shipping_price		= 0.00;
	protected $_net_total			= 0.00; // total - tax
	protected $_total				= 0.00;
	
	protected $_tax_rate			= NULL;
	
	protected $session_id			= NULL;

	public static function instance($params = array()) 
	{
	    static $singleton = null;
		if (is_null($singleton))
		{
		    $session = Session::instance();
		    if ($session->get('cart'))
			{
    			$singleton = $session->get('cart');
    		}
    		else 
			{
    			$singleton = new Cart($params);
    			$singleton->_save_cart();
    			$session->set('cart', $singleton);
    		}
		}
		return $singleton;
	}

	public static function factory($params)
	{
		$instance = new Cart($params);
		$instance->_save_cart();
		return $instance;
	}

	protected function __construct($params)
	{
		if (isset($params['ordID']))
		{
			$session_id = ORM::factory('ifrogz_order', $params['ordID'])->ordSessionID;
			$this->session_id = $session_id;
		}
		else
		{
			$this->session_id = session_id();
		}
		$this->_tax_rate = ORM::factory('ifrogz_global')->get_var('ut_sales_tax');
	}

	// --------------------------------------------------------------------
	
	/**
	 * Insert items into the cart and save it to the session table
	 *
	 * @access	public
	 * @param	array
	 * @return	bool
	 */
	public function insert($items = array())
	{
		// Was any cart data passed?
		if (!is_array($items) OR count($items) == 0) 
		{
			throw new Kohana_Exception('The insert method must be passed an array containing data.');
		}
		
		// You can either insert a single product using a one-dimensional array, 
		// or multiple products using a multi-dimensional one. The way we
		// determine the array type is by looking for a required array key named "id"
		// at the top level. If it's not found, we will assume it's a multi-dimensional array.

		$save_cart = FALSE;		
		if (isset($items['id']))
		{			
			if ($this->_insert($items) == TRUE)
			{
				$save_cart = TRUE;
			}
		}
		else
		{
			foreach ($items as $val)
			{
				if (is_array($val) AND isset($val['id']))
				{
					if ($this->_insert($val) == TRUE)
					{
						$save_cart = TRUE;
					}
				}			
			}
		}

		// Save the cart data if the insert was successful
		if ($save_cart == TRUE)
		{
			$this->_save_cart();
			return TRUE;
		}

		return FALSE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Insert
	 *
	 * @access	private
	 * @param	array
	 * @return	bool
	 *
	 * Example of $items array
	 * array(5) (
	 *   "id" => string(11) "IPHONE3G-SG"
	 *   "qty" => string(1) "1"
	 *   "price" => string(5) "19.99"
	 *   "name" => string(28) "iPhone 3G & 3G[s] Soft Gloss"
	 *   "options" => array(1) (
	 *       0 => array(2) (
	 *           "optGrpID" => string(3) "288"
	 *           "optID" => string(4) "5101"
	 *		 )
	 *    )
	 *  )
	 */
	private function _insert($items = array())
	{
		// Was any cart data passed?
		if (!is_array($items) OR count($items) == 0) 
		{
			throw new Kohana_Exception('The insert method must be passed an array containing data.');
		}
		
		// --------------------------------------------------------------------

		// Does the $items array contain an id, quantity, price, and name?  These are required
		if ( ! isset($items['id']) OR ! isset($items['qty']) OR ! isset($items['price']) OR ! isset($items['name']))
		{				
			throw new Kohana_Exception('The cart array must contain a product ID, quantity, price, and name.');
		}

		// --------------------------------------------------------------------

		// Prep the quantity. It can only be a number.  Duh...
		$items['qty'] = trim(preg_replace('/([^0-9])/i', '', $items['qty']));
		// Trim any leading zeros
		$items['qty'] = trim(preg_replace('/(^[0]+)/i', '', $items['qty']));

		// If the quantity is zero or blank there's nothing for us to do
		if ( ! is_numeric($items['qty']) OR $items['qty'] == 0)
		{
			return FALSE;
		}

		// --------------------------------------------------------------------

		// Validate the product ID. It can only be alpha-numeric, dashes, underscores or periods
		// Not totally sure we should impose this rule, but it seems prudent to standardize IDs.
		// Note: These can be user-specified by setting the $this->product_id_rules variable.
		if ( ! preg_match("/^[".$this->product_id_rules."]+$/i", $items['id']))
		{
			throw new Kohana_Exception('Invalid product ID.  The product ID can only contain alpha-numeric characters, dashes, and underscores');
		}

		// --------------------------------------------------------------------

		// Validate the product name. It can only be alpha-numeric, dashes, underscores, colons or periods.
		// Note: These can be user-specified by setting the $this->product_name_rules variable.
		//if ( ! preg_match("/^[".$this->product_name_rules."]+$/i", $items['name'])) {
		//	throw new Kohana_Exception('An invalid name was submitted as the product name: '.$items['name'].' The name can only contain alpha-numeric characters, dashes, underscores, colons, spaces, and forward slashes.');
		//}

		// --------------------------------------------------------------------

		// Prep the price.  Remove anything that isn't a number or decimal point.
		$items['price'] = trim(preg_replace('/([^0-9\.])/i', '', $items['price']));
		// Trim any leading zeros
		$items['price'] = trim(preg_replace('/(^[0]+)/i', '', $items['price']));

		// Is the price a valid number?
		if ( ! is_numeric($items['price']))
		{
			throw new Kohana_Exception('An invalid price was submitted for product ID: '.$items['id']);
		}

		// --------------------------------------------------------------------
		
		// Add the new item to the cart and cartoptions tables		
		
		// See if the product and option already exist.  If yes, increase qty by 1.  If not, insert into cart and cartoptions
		$p_exists = FALSE;
		$cart_id = FALSE;
		$carts = ORM::factory('ifrogz_cart')
			->where('cartSessionID', '=', $this->session_id)
			->where('cartProdID', '=', $items['id'])
			->find_all();
		
		if ($carts->count() > 0) {
			foreach ($carts as $cart) { // Each product
				$all_options_match = TRUE;
				if (empty($items['options'])) {
					$p_exists = TRUE;
					$cart_id = $cart->cartID;
				} else {
					foreach ($items['options'] as $option) { // Each optiongroup
						$co = ORM::factory('ifrogz_cartoption')->where('coCartID', '=', $cart->cartID)->where('coOptID', '=', $option['optID']);
						$count = $co->count_all();
						if ($count <= 0) {
							// The option did not match
							$all_options_match = FALSE;
						}
					}
				}
				
				if ($all_options_match) {
					$p_exists = TRUE;
					$cart_id = $cart->cartID;
				}
			}
		}
		
		// If product is not in stock, throw an error.  If it's an option, we just extend the shipping.
		if (!$this->_is_in_stock($items)) 
		{
			//print_r($items);
			//exit();
			throw new Kohana_Inventory_Exception('Product is not in stock for: '.$items['id']);
		}
		
		if ($p_exists) 
		{
			// Just update the qty for the existing product in the cart table
			$cart = ORM::factory('ifrogz_cart', $cart_id);
			$cart->cartQuantity += 1;
			$cart->save();
		}
		else
		{
			// Insert product into cart table
			$cart = ORM::factory('ifrogz_cart');
			$cart->cartSessionID = $this->session_id;
			$cart->cartProdID = $items['id'];
			$cart->cartProdName = $items['name'];
			$cart->cartProdPrice = $items['price'];
			$cart->cartQuantity = $items['qty'];
			$cart->save();
			
			// Insert options into cartoptions table
			if (is_array($items['options'])) 
			{
				foreach ($items['options'] as $option) 
				{					
					// Get optiongroup
					$og = ORM::factory('ifrogz_optiongroup', $option['optGrpID']);

					// Get option
					$o = ORM::factory('ifrogz_option', $option['optID']);

					// Insert options into cartoptions table
					$co = ORM::factory('ifrogz_cartoption');
					$co->coCartID 			= $cart->cartID;
					$co->coOptID 			= $option['optID'];
					$co->coOptGroup			= $og->optGrpName;
					$co->coCartOption		= $o->optName;
					
					/**
					 * TODO
					 *
					 * Calculate PriceDiff and WeightDiff
					 *   - optType 2 is the only type that requires this calculation
					 *   - optFlags determines if it's a percentage or fixed
					 */
					$co->coPriceDiff		= $o->optPriceDiff;
					$co->coWeightDiff		= $o->optWeightDiff;
					
					/*
						TODO - Calculate ExtendShipping
					*/
					if (isset($option['extend_shipping']) && $option['extend_shipping'] > 0) 
					{
						$co->coExtendShipping	= $option['extend_shipping'];
					}
					else
					{
						$co->coExtendShipping	= 0;
					}
					$co->save();
				}
			}
		}

		// Woot!
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Update the cart
	 *
	 * This function permits the quantity of a given item to be changed. 
	 * Typically it is called from the "view cart" page if a user makes
	 * changes to the quantity before checkout. That array must contain the
	 * product ID and quantity for each item.
	 *
	 * @access	public
	 * @param	array
	 * @param	string
	 * @return	bool
	 */
	public function update($items = array())
	{
		// Was any cart data passed?
		if ( ! is_array($items) OR count($items) == 0)
		{
			throw new Kohana_Exception('No cart data was passed.');
		}
			
		// You can either update a single product using a one-dimensional array, 
		// or multiple products using a multi-dimensional one.  The way we
		// determine the array type is by looking for a required array key named "id".
		// If it's not found we assume it's a multi-dimensional array
		$save_cart = FALSE;
		if (isset($items['cartID']) AND isset($items['qty']))
		{
			if ($this->_update($items) == TRUE)
			{
				$save_cart = TRUE;
			}
		}
		else
		{
			foreach ($items as $val)
			{
				if (is_array($val) AND isset($val['rowid']) AND isset($val['qty']))
				{
					if ($this->_update($val) == TRUE)
					{
						$save_cart = TRUE;
					}
				}			
			}
		}

		// Save the cart data if the update was successful
		if ($save_cart == TRUE)
		{
			$this->_save_cart();
			return TRUE;
		}

		throw new Kohana_Exception('Just returned false.');
	}

	// --------------------------------------------------------------------
	
	/**
	 * Update the cart
	 *
	 * This function permits the quantity of a given item to be changed. 
	 * Typically it is called from the "view cart" page if a user makes
	 * changes to the quantity before checkout. That array must contain the
	 * product ID and quantity for each item.
	 *
	 * @access	private
	 * @param	array
	 * @return	bool
	 */	
	private function _update($items = array())
	{
		// Without these array indexes there is nothing we can do
		if ( ! isset($items['qty']) OR ! isset($items['cartID']))
		{
			throw new Kohana_Exception('Qty or Cart ID is missing.');
			return FALSE; // UNNECESSARY
		}
		
		// Prep the quantity
		$items['qty'] = preg_replace('/([^0-9])/i', '', $items['qty']);

		// Is the quantity a number?
		if ( ! is_numeric($items['qty']))
		{
			throw new Kohana_Exception('The quantity must be a number.');
		}
		
		// Is the new quantity different than what is already saved in the cart?
		// If it's the same there's nothing to do
		$cart = ORM::factory('ifrogz_cart', $items['cartID']);
		if ($cart->cartQuantity == $items['qty'])
		{
			throw new Kohana_Exception('The qty is the same.');
		}

		// Is the quantity zero?  If so we will remove the item from the cart.
		// If the quantity is greater than zero we are updating
		if ($items['qty'] == 0)
		{			
			ORM::factory('ifrogz_cart', $items['cartID'])->delete();		
		}
		else
		{			
			$cart = ORM::factory('ifrogz_cart', $items['cartID']);
			$cart->cartQuantity = $items['qty'];
			$cart->save();
		}
		
		return TRUE;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Save the cart array to the session DB
	 *
	 * @access	private
	 * @return	bool
	 */
	private function _save_cart()
	{
		// Unset these so our total can be calculated correctly below
		unset($this->total_items);
		unset($this->_sub_total);
		unset($this->_discount);
		unset($this->_total);
		unset($this->total_weight);
		unset($this->order_id);
		
		// Lets add up the individual prices and set the cart sub-total
		$total = DB::select(DB::expr('SUM(cart.cartProdPrice * cart.cartQuantity) AS total_price'), DB::expr('SUM(cart.cartQuantity) AS total_qty'), DB::expr('SUM(products.pWeight) * cart.cartQuantity AS total_weight'))
					->from('cart')
					->join('products', 'INNER')
					->on('products.pID', '=', 'cart.cartProdID')
					->where('cart.cartSessionID', '=', $this->session_id)
					->execute()
					->as_array();
		
		// Set the cart total, total items, and total weight
		$this->total_items = ($total[0]['total_qty'] > 0) ? $total[0]['total_qty'] : 0;			
		$this->_sub_total = ($total[0]['total_price'] > 0) ? $total[0]['total_price'] : 0;
		$this->total_weight = ($total[0]['total_weight'] > 0) ? $total[0]['total_weight'] : 0;
		
		// Get and set the order ID, if it exists yet
		$cart = $this->contents();
		
		$Shipping = Shipping::instance();
		
		$this->_discount = Discounts::get_merchandise_discounts_for_cart($cart, $Shipping->get_country(), $this->_coupon_code);
		
		$shipping_price = (Discounts::is_free_shipping_available($Shipping->get_method_flag(), $Shipping->get_country(), $this->_coupon_code)) ? 0.00 : $Shipping->get_price($this->total_weight);
		
		// Round everything
		$this->_sub_total = round($this->_sub_total, 2);
		$this->_discount = round($this->_discount, 2);
		$shipping_price = round($shipping_price, 2);
		
		$this->_net_total = ($this->_sub_total - $this->_discount) + $shipping_price;
		$this->_total = $this->_net_total + $this->get_taxes();
		
		$this->_certificate_amount = 0.00;
		if (Helper_Model::factory('certificate')->is_redeemable_by_code($this->_certificate_code)) {
			$this->_certificate_amount = Helper_Model::factory('certificate')->get_balance_by_code($this->_certificate_code);
			if ($this->_certificate_amount >= $this->_total) {
				$this->_certificate_amount = $this->_total;
			}
		}
		
		// Round certificate amount
		$this->_certificate_amount = round($this->_certificate_amount, 2);
		
		$this->_total -= $this->_certificate_amount;
		
		$this->_total = round($this->_total, 2);
		
		if (isset($cart[0]) && $cart[0]['cartOrderID']) {
			$this->order_id = $cart[0]['cartOrderID'];
		} else {
			$this->order_id = NULL;
		}

		return TRUE;	
	}

	// --------------------------------------------------------------------
	
	protected function _is_in_stock(&$items) 
	{		
		//echo Kohana::debug($items); exit;
		$stock_manage = ORM::factory('ifrogz_admin', 1)->adminStockManage;
		if ($stock_manage > 0) {
			if (!empty($items['options'])) 
			{
				$num_of_options = count($items['options']);
				for ($i=0; $i < $num_of_options; $i++) 
				{ 
					$inv = Inventory::factory(NULL, $items['options'][$i]['optID']);
					if (!$inv->is_in_stock()) 
					{
						return FALSE;
					}
					else 
					{
						if (!$inv->is_over_min_threshold()) 
						{
							$arr = array('extend_shipping' => $inv->get_extend_shipping());
							$items['options'][$i] = array_merge($items['options'][$i], $arr);
							
							return TRUE;
						}
						else
						{
							return TRUE;
						}
					}
				}
			}
			else 
			{
				$inv = Inventory::factory($items['id']);
				
				return $inv->is_in_stock();
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	public function reload()
	{
		$this->_save_cart();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Cart Sub Total
	 *
	 * @access	public
	 * @return	float
	 */
	public function sub_total()
	{
		return $this->_sub_total;
	}

	// --------------------------------------------------------------------

	/**
	* This function returns the discount value.
	*
	* @access public
	* @return float
	*/
	public function discount()
	{
	    return $this->_discount;
	}

	/**
	 * Cart Total
	 *
	 * @access	public
	 * @return	float
	 */
	public function total()
	{
		return $this->_total;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Total Items
	 *
	 * Returns the total item count
	 *
	 * @access	public
	 * @return	integer
	 */
	public function total_items()
	{
		return $this->total_items;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Total Weight
	 *
	 * Returns the total weight
	 *
	 * @access	public
	 * @return	float
	 */
	public function total_weight()
	{
		return $this->total_weight;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Cart Contents
	 *
	 * Returns the entire cart array
	 *
	 * @access	public
	 * @return	array
	 */
	public function contents()
	{
		$carts = DB::select()
					->from('cart')
					->join('cartoptions', 'LEFT')
					->on('cartoptions.coCartID', '=', 'cart.cartID')
					->where('cart.cartSessionID', '=', $this->session_id)
					->order_by('cart.cartID')
					->execute()
					->as_array();
		
		# Get cart columns
		$cart_columns = array();
		$cols = ORM::factory('ifrogz_cart')->list_columns();
		foreach ($cols as $col) 
		{
			$cart_columns[] = $col['column_name'];
		}
		
		# Get cartoptions columns
		$co_columns = array();
		$cols = ORM::factory('ifrogz_cartoption')->list_columns();
		foreach ($cols as $col) 
		{
			$co_columns[] = $col['column_name'];
		}
		
		# Format into a nice structured array
		$data = array();
		$i = -1;
		$last_cart_id = 0;
		foreach ($carts as $key => $cart) 
		{
			if ($cart['cartID'] != $last_cart_id) {
				$i++;
				
				$data[$i]['options'][] = Arr::extract($cart, $co_columns);

				
				
				$discounts = Discounts::get_list_of_merchandise_discounts_for_product(1, $cart['cartProdID']);
				$tot_discounts = 0;
				if ( ! empty($discounts)) {
					foreach ($discounts as $discount) {
						$tot_discounts += $discount[0]['cpnDiscount'] / 100;
					}
				}
				
				$data[$i] = Arr::extract($cart, $cart_columns);
				$data[$i] += array('subtotal' => (($cart['cartProdPrice'] - round(($tot_discounts * $cart['cartProdPrice']), 2)) * $cart['cartQuantity']));
				$data[$i]['cartDiscount'] = round(($tot_discounts * $cart['cartProdPrice']), 2);



				# Get the product's sectionID
				$prod = ORM::factory('ifrogz_product', $cart['cartProdID']);
				$data[$i]['cartProdSection'] = $prod->pSection;
				$data[$i]['pSysproProductClass'] = $prod->pSysproProductClass;
				$data[$i][Model_ORM_iFrogz_Product::NUMBER] = $prod->{Model_ORM_iFrogz_Product::NUMBER};
			}			
			
			$data[$i]['options'][] = Arr::extract($cart, $co_columns);
			$last_cart_id = $cart['cartID'];
		}
		
		# Get additional information
		for ($i=0; $i < count($data); $i++) {
			for ($j=0; $j < count($data[$i]['options']); $j++) {
				$opt = ORM::factory('ifrogz_option', $data[$i]['options'][$j]['coOptID']);
				$data[$i]['options'][$j]['sku'] = $opt->optRegExp;
			}
		}
		
		# Get product cache ids
		$this->get_product_cache_id($data);
		
		# Calculate the extend shipping information
		$this->get_max_extend_shipping($data);

		return $data;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Update cartCompleted
	 */
	public function set_cart_completed($bool = FALSE)
	{
		$val = 0;
		if ($bool) 
		{
			$val = 1;
		}
		
		$total_rows = DB::update('cart')->set(array('cartCompleted' => $val))->where('cartSessionID', '=', $this->session_id)->execute();
	}
	
	// --------------------------------------------------------------------

	/**
	 * Update cartOrderID
	 */
	public function set_cart_order_id($order_id)
	{
		$total_rows = DB::update('cart')->set(array('cartOrderID' => $order_id))->where('cartSessionID', '=', $this->session_id)->execute();
	}
	
	// --------------------------------------------------------------------
	
	public function set_cart_coupon($coupon_code)
	{
	    $this->_coupon_code = $coupon_code;
	}
	
	public function set_cart_certificate_code($certificate_code)
	{
		$this->_certificate_code = $certificate_code;
	}
	
	public function set_state($state) {
		$session = Session::instance();
	    if ($session->get('order')) {
			$order = $session->get('order');
			$order->{Model_ORM_iFrogz_Order::SHIPPING_STATE} = trim($state);
		}
	}
	
    public function get_cart_coupon()
	{
        if (is_null($this->_coupon_code))
		{
            return '';
        }
        return $this->_coupon_code;
    }
	
	public function get_certificate_code()
	{
		return $this->_certificate_code;
	}
	
	public function get_certificate_amount()
	{
		return $this->_certificate_amount;
	}
	
	/**
	 * Get Taxes
	 */
	public function get_taxes()
	{
	    $session = Session::instance();
	    $tax_rate = 0.00;
	    if ($session->get('order'))
		{
			$order = $session->get('order');
			$country = Shipping::instance()->get_country();
			$state = trim($order->{Model_ORM_iFrogz_Order::SHIPPING_STATE});
			if (($country == 1) && preg_match('/^ut(ah)?$/i', $state))
			{
			    $tax_rate = $this->_tax_rate;
		    }
		}
		$tax = $this->_net_total * $tax_rate;
		return round($tax, 2);
	}

	/**
	 * Calculates the max extend shipping for each product in the cart
	 *
	 * @param	array 		the contents array
	 * @return 	boolean
	 **/
	private function get_max_extend_shipping(&$contents)
	{
		if ( ! empty($contents)) 
		{
			$num_of_carts = count($contents);
			for ($i=0; $i < $num_of_carts ; $i++) 
			{ 
				$max_extend_shipping = 0;
				
				if ( ! empty($contents[$i]['options'])) 
				{
					$num_of_options = count($contents[$i]['options']);
					for ($j=0; $j < $num_of_options; $j++) 
					{ 
						if (isset($contents[$i]['options'][$j]['coExtendShipping'])) 
						{
							if ($contents[$i]['options'][$j]['coExtendShipping'] > $max_extend_shipping) 
							{
								$max_extend_shipping = $contents[$i]['options'][$j]['coExtendShipping'];
							}
						}
					}
				}
				
				$contents[$i]['extend_shipping'] = $max_extend_shipping;	
			}
		}
		
		return TRUE;
	}
	
	private function get_product_cache_id(&$contents)
	{
		# Get all the cache ids for the product images and max_extend_shipping
		$img_options = array('width' => 100);
		
		if ( ! empty($contents)) 
		{
			$num_of_carts = count($contents);
			for ($i=0; $i < $num_of_carts ; $i++) 
			{ 
				$product = ORM::factory('ifrogz_product', $contents[$i]['cartProdID']);
				
				if ( ! empty($contents[$i]['options'])) 
				{
					$num_of_options = count($contents[$i]['options']);
					for ($j=0; $j < $num_of_options; $j++) 
					{ 
						if (isset($contents[$i]['options'][$j]['coExtendShipping'])) 
						{
							$max_extend_shipping = $contents[$i]['options'][$j]['coExtendShipping'];
						}
						
						
						if (($product->{Model_ORM_iFrogz_Product::SELL} & 2) == 2) {
							// Has options
							$filename = strtolower(Model_ORM_iFrogz_Option::get_file_safe_alt_prod_id(ORM::factory('ifrogz_option', $contents[$i]['options'][$j]['coOptID'])->{Model_ORM_iFrogz_Option::ALTERNATE_PRODUCT_ID}) . '.png');
						} else {
							$filename = strtolower($product->{Model_ORM_iFrogz_Product::NUMBER} . '.png');
						}
						
						// Create the product image thumbnail
						try 
						{
							$product_image_cache_id = Helper_Image::get_cache_id('/' . Kohana::$config->load('ifrogz.product_images') . strtolower($contents[$i]['cartProdID']) . '/', strtolower($filename), $img_options);
							$contents[$i]['cache_id'] = $product_image_cache_id;
						}
						catch (Kohana_Exception $e)
						{
							// Just do nothing
						}
					}
				}
				else
				{
					# Product has no options
					$filename = strtolower($contents[$i]['cartProdID'] . 'png');
					
					try
					{
						$product_image_cache_id = Helper_Image::get_cache_id('/' . Kohana::$config->load('ifrogz.product_images') . strtolower($contents[$i]['cartProdID']) . '/', strtolower($filename), $img_options);
						$contents[$i]['cache_id'] = $product_image_cache_id;
					}
					catch (Kohana_Exception $e)
					{
						// Just do nothing
					}
					
				}	
			}
		}
	}
	
	/**
	 * Has options
	 *
	 * Returns TRUE if the rowid passed to this function correlates to an item
	 * that has options associated with it.
	 *
	 * @access	public
	 * @return	array
	 */
	public function has_options($cart_id)
	{
		if ( ORM::factory('ifrogz_cart', $cart_id)->cartoption->count_all() <= 0)
		{
			return FALSE;
		}
		
		return TRUE;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Product options
	 *
	 * Returns an array of options, for a particular product row ID
	 *
	 * @access	public
	 * @return	array
	 */
	public function product_options($cart_id)
	{
		$options = ORM::factory('ifrogz_cart', $cart_id)->cartoption->find_all()->as_array();
		if ( ! empty($options))
		{
			return array();
		}

		return $options;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Format Number
	 *
	 * Returns the supplied number with commas and a decimal point.
	 *
	 * @access	public
	 * @return	integer
	 */
	public function format_number($n = '')
	{
		if ($n == '')
		{
			return '';
		}
	
		// Remove anything that isn't a number or decimal point.
		$n = trim(preg_replace('/([^0-9\.])/i', '', $n));
	
		return number_format($n, 2, '.', ',');
	}
		
	// --------------------------------------------------------------------
	
	/**
	 * Destroy the cart
	 *
	 * Empties the cart and kills the session
	 *
	 * @access	public
	 * @return	null
	 */
	public function destroy()
	{
		unset($this->_cart_contents);
	
		$this->_cart_contents['total'] = 0;		
		$this->_cart_contents['total_items'] = 0;		

		Session::instance()->delete('cart_contents');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Recursive Implode
	 *
	 * @access private
	 * @return string
	 */
	private function r_implode($glue, $pieces) 
	{ 
		foreach($pieces as $r_pieces) 
		{ 
			if(is_array($r_pieces)) 
			{ 
				$retVal[] = $this->r_implode($glue, $r_pieces); 
			} 
			else 
			{ 
				$retVal[] = $r_pieces; 
			} 
		} 
		return implode($glue, $retVal); 
	}
}
