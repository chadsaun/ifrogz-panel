<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Contains all the logic for batch shipping
 *
 * @package    iFrogz
 * @author     iFrogz Team
 */
class Controller_AddressLabel extends Controller_Template {

	public $template = 'templates/default';

	public function before() {
		parent::before();
		$this->template->title = 'Admin: Address Label';
		$this->template->content = new View('pages/addresslabel/index');
	}

	public function action_index($id = -1) {
	    $this->action_fetch($id);
    }

    public function action_fetch($id) {
		$sender_address  = '';
		$sender_address .= '<br />&nbsp;&nbsp;MyFrogz HK';
		$sender_address .= '<br />&nbsp;&nbsp;Unit 1107, Tower 3, Enterprise Square 1';
		$sender_address .= '<br />&nbsp;&nbsp;9 Sheung Yuet Road';
		$sender_address .= '<br />&nbsp;&nbsp;Kowloon Bay';
		$sender_address .= '<br />&nbsp;&nbsp;Hong Kong';

		$recipient_address  = '';

		$record = ORM::factory('ifrogz_order', $id);

		if ($record->loaded()) {
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_NAME})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . strtoupper($record->{Model_ORM_iFrogz_Order::SHIPPING_NAME}); }
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_ADDRESS})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'  . strtoupper(stripslashes($record->{Model_ORM_iFrogz_Order::SHIPPING_ADDRESS})); }
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_ADDRESS_2})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . strtoupper(stripslashes($record->{Model_ORM_iFrogz_Order::SHIPPING_ADDRESS_2})); }
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_CITY})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . strtoupper($record->{Model_ORM_iFrogz_Order::SHIPPING_CITY}); }
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_STATE})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . strtoupper($record->{Model_ORM_iFrogz_Order::SHIPPING_STATE}); }
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_POSTAL_CODE})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . strtoupper($record->{Model_ORM_iFrogz_Order::SHIPPING_POSTAL_CODE}); }
		    if (!empty($record->{Model_ORM_iFrogz_Order::SHIPPING_COUNTRY})) { $recipient_address .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . strtoupper($record->{Model_ORM_iFrogz_Order::SHIPPING_COUNTRY}); }
	    }

        //$barcode = "<img src=\"/admin/createmyfrogzbarcode.php?ordID={$id}&rotate=0\">";
        
        $creator = new Code39Barcode($id);
        $barcode = $creator->render();

        $this->template->content->barcode = $barcode;
        $this->template->content->sender_address = $sender_address;
		$this->template->content->recipient_address = $recipient_address;
    }

}
?>