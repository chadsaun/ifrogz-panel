<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Ads extends Controller_iFrogzBase {
	
	public $template = 'templates/ads';
	
	public function action_index() {
		
		// Get Banner ads
		$obj_ads = ORM::factory('ifrogz_ad')
					->where('type', '=', 2)
					->where('begin_date', '<', date('Y-m-d H:i:s'))
					->where('end_date', '>', date('Y-m-d H:i:s'))
					->where('active', '=', 1)
					->order_by('imgOrder')
					->find_all();
					
		$ads = array();
		foreach ($obj_ads as $ad) {
			$ads[] = $ad->as_array();
		}
		
		$obj_button_ads = Jelly::select('iFrogz_Ad')
				->where('type', '=', 3)
				->where('active', '=', 1)
				->where('begin_date', '<', date('Y-m-d H:i:s'))
				->where('end_date', '>', date('Y-m-d H:i:s'))
				->limit(9)
				->order_by('imgOrder', 'ASC')
				->execute();
				
		$button_ads = array();
		foreach ($obj_button_ads as $ad) {
			$button_ads[] = $ad->as_array();
		}
		
		// Get Banner ads
		$obj_ads_staging = ORM::factory('ifrogz_ad')
					->where('type', '=', 2)
					->where('active', '=', 1)
					->where('isTest', '=', 1)
					->order_by('testOrder')
					->find_all();
					
		$ads_staging = array();
		foreach ($obj_ads_staging as $ad) {
			$ads_staging[] = $ad->as_array();
		}
		
		$obj_button_ads_staging = Jelly::select('iFrogz_Ad')
				->where('type', '=', 3)
				->where('active', '=', 1)
				->where('isTest', '=', 1)
				->limit(9)
				->order_by('testOrder', 'ASC')
				->execute();
				
		$button_ads_staging = array();
		foreach ($obj_button_ads_staging as $ad) {
			$button_ads_staging[] = $ad->as_array();
		}
		
		$this->template->content = View::factory('pages/ads/main')
									->bind('ads', $ads)
									->bind('button_ads', $button_ads)
									->bind('ads_staging', $ads_staging)
									->bind('button_ads_staging', $button_ads_staging);
	}
	
	public function action_buttons() {
		$type = 'Button';
		$ads = Jelly::select('iFrogz_Ad')->where('type', '=', 3)->where('active', '=', 1)->order_by('imgOrder')->execute();

		$this->template->content = View::factory('pages/ads/index')
									->bind('ads', $ads)
									->bind('type', $type);
	}
	
	public function action_banners() {
		$type = 'Banner';
		$ads = Jelly::select('iFrogz_Ad')->where('type', '=', 2)->where('active', '=', 1)->order_by('imgOrder')->execute();

		$this->template->content = View::factory('pages/ads/index')
									->bind('ads', $ads)
									->bind('type', $type);
	}
	
	public function action_sort() {
		$ads = Kohana::sanitize($_POST['list_order']);
			
		$returned_data = array();
		$id = 0;
		foreach ($ads as $key => $value) {
			Jelly::update('iFrogz_Ad')
				->where('id', '=', $value)
				->set(array('imgOrder' => $key+1))
				->execute();
				
			$returned_data[$id]['position'] = $key+1;
			$returned_data[$id]['id'] = $value;
			
			$id = $id+1; 
		}
		
		$json['response']['status'] = 'success';
		$json['response']['data']['order'] = $returned_data;
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}
	
	public function action_edit() {
		$data = Kohana::sanitize($_POST);
		
		$ad = Jelly::select('iFrogz_Ad')->where('id', '=', $data['id'])->load();
		
		$view = View::factory('partials/ads/edit')
				->bind('type', $data['type'])
				->bind('ad', $ad)
				->render();
		
		$json['response']['status'] = 'success';
		$json['response']['data']['view'] = $view;
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}
	
	public function action_add() {
		$data = Kohana::sanitize($_POST);
		
		$view = View::factory('partials/ads/add')
				->bind('type', $data['type'])
				->bind('id', $data['id'])
				->render();
		
		$json['response']['status'] = 'success';
		$json['response']['data']['view'] = $view;
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}
	
	public function action_staging() {
		$data = Kohana::sanitize($_POST);
		
		if ($data['ischecked'] == 'true') {
			$checked = 1;
		} else {
			$checked = 0;
		}
		
		Jelly::update('iFrogz_Ad')
			->where('id', '=', $data['id'])
			->set(array(
				'isTest' => $checked,
			))
			->execute();

		$json['response']['status'] = 'success';
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}
	
	public function action_staging_order() {
		$data = Kohana::sanitize($_POST);

		Jelly::update('iFrogz_Ad')
			->where('id', '=', $data['id'])
			->set(array(
				'testOrder' => $data['order'],
			))
			->execute();

		$json['response']['status'] = 'success';
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}
	
	public function action_add_save() {
		$data = Kohana::sanitize($_POST);
		$file = $_FILES;

		$filename = preg_replace('/\s/', '_', $file['image']['name']);
		
		$s3 = new Amazon_S3();
		if ($data['type'] == 'Banner') {
			$save = $s3->upload_file($file['image']['tmp_name'], 'ifrogz.com', 'lib/images/homepagebanners/' . $filename);
			$type = 2;
		} else {
			$save = $s3->upload_file($file['image']['tmp_name'], 'ifrogz.com', 'lib/images/homepagebuttons/' . $filename);
			$type = 3;
		}

		if ( ! isset($save)) {
			$this->request->redirect('/ads/'.strtolower($data['type']));
		}

		$is_test = FALSE;
		if (isset($data['isTest'])) {
			$is_test = TRUE;
		}
		
		$sequence_id = $data['id'] + 1;
		
		Jelly::factory('iFrogz_Ad')
			->set(array(
				'image' => $filename,
				'link' => $data['link'],
				'alt' => $data['alt'],
				'type' => $type,
				'imgOrder' => $sequence_id,
				'begin_date' => $data['begin_date'],
				'end_date' => $data['end_date'],
				'active' => 1,
				'isSWF' => 0,
				'isTest' => $is_test,
				'testOrder' => 0
			))
			->save();
			
		$this->request->redirect('/ads/'.strtolower($data['type'].'s'));
	}

	public function action_edit_save() {
		$data = Kohana::sanitize($_POST);
		
		 Jelly::update('iFrogz_Ad')
			->where('id', '=', $data['id'])
			->set(array(
				'link' => $data['link'],
				'begin_date' => $data['begin_date'],
				'end_date' => $data['end_date']
			))
			->execute();
		
		$ad = Jelly::select('iFrogz_Ad')->where('id', '=', $data['id'])->load();
			
		$view = View::factory('partials/ads/individual')
				->bind('type', $data['type'])
				->bind('ad', $ad)
				->render();
			
		$json['response']['status'] = 'success';
		$json['response']['data']['view'] = $view;
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}
	
	public function action_delete() {
		$id = Kohana::sanitize($_POST['ID']);
		
		Jelly::update('iFrogz_Ad')
			->where('id', '=', $id)
			->set(array('active' => 0))
			->execute();
			
		$json['response']['status'] = 'success';
		$json['response']['data']['id'] = $id;
		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();
	}

}
?>