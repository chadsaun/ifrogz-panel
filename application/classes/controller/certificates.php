<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Certificates extends Controller_iFrogzBase {
	
	public function before() {
		parent::before();
		
		$this->session = Session::instance();
	}
	
	public function action_open() {
		
		// Check authorization
		$certificates_roles = array(
            Roles::ADMIN,
            Roles::MANAGEMENT,
            Roles::IT,
            Roles::ACCOUNTING
        );
        $user_roles = Session::instance()->get('Roles', array());
		Authenticate::regulate_access($certificates_roles, $user_roles);
		
		// Initialize
		$expiration = date('Y-m-d 23:59:59');
		$data = array();
		
		if ($_POST) {
			$expiration = date('Y-m-d 23:59:59', strtotime($_POST['expiration']));
		}
		
		// Run query
		$query = DB::select(array('COUNT("cert_id")', 'total_certificates'), array('SUM("cert_amt")', 'total_amount'))
					->from('certificates')
					->where(DB::expr('UNIX_TIMESTAMP("' . $expiration . '")'), '<', DB::expr('cert_exp_dt'))
					->where('cert_amt', '>', 0);
		$result = $query->execute();
		$data = $result->as_array();
		
		$view = View::factory('pages/certificates/open')
					->bind('data', $data)
					->bind('expiration', $expiration);

		$this->template->content = $view;
		
	}
	
}
?>