<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Chad extends Controller_iFrogzBase {

	public function before() {
		parent::before();

		$this->session = Session::instance();
	}

	public function action_index() {

		echo 'Connected!';

		/*
		$date = '2012-01-01';
		echo date('o', strtotime($date));
		echo date('W', strtotime($date));
		exit;

		error_reporting(E_ALL);
		ini_set('display_errors', '1');
		//ini_set("memory_limit","12M");

		$sales = DB_SQL::select('default_ifrogz')->from('bb_sales')->where('DateOfWeek', 'IS', NULL)->limit(10000)->query();

		foreach ($sales as $sale) {
			$year = Arr::get($sale, 'Year');
			$week = Arr::get($sale, 'Week');
			$week = str_pad($week, 2, 0, STR_PAD_LEFT);
			$newDate = date('Y-m-d', strtotime("{$year}W{$week}"));
			DB_SQL::update('default_ifrogz')
				->table('bb_sales')
				->set('DateOfWeek', $newDate)
				->where('id', '=', $sale['ID'])
				->execute();
		}

		echo 'Done!';

		exit;

		echo date('Y-m-d', strtotime("2012W01"));
		*/
	}

	protected function WeekStartDate($week,$year,$format="d-m-Y") {
		date_default_timezone_set('Europe/Paris');
		$firstDayInYear=date("N",mktime(0,0,0,1,1,$year));
		if ($firstDayInYear<5)
			$shift=-($firstDayInYear-1)*86400;
		else
			$shift=(8-$firstDayInYear)*86400;
		if ($week>1) $weekInSeconds=($week-1)*604800; else $weekInSeconds=0;
		$timestamp=mktime(0,0,0,1,1,$year)+$weekInSeconds+$shift;
		return date($format,$timestamp);
	}
}