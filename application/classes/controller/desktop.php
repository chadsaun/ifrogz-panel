<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Desktop extends Controller_ProceduresTemplate {

	public function action_index() {
		$this->template->content = View::factory('pages/desktop/index');
	}

}
?>