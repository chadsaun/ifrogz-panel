<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Features extends Controller_iFrogzBase {
	
	public $template = 'templates/features';
	
	protected $service_features = NULL;
	
	public function before() {
		parent::before();
		$this->service_features = new Service_Features_Implementation();
	}
	
	public function action_index() {
		$artists = $this->service_features->get_features('active', '(1)');
		$athletes = $this->service_features->get_features('active', '(2)');
		$events = $this->service_features->get_features('active', '(3)');
		
		$inactives = $this->service_features->get_features('inactive', '(1,2,3)');

		$this->template->content = View::factory('pages/features/index')
							->bind('artists', $artists)
							->bind('athletes', $athletes)
							->bind('events', $events)
							->bind('inactives', $inactives);
	}
	
	public function action_view($id) {
		$feature = $this->service_features->get_feature($id);
				
		$image['intro'] = $this->service_features->get_images($feature['ID'], 1);
		$image['hero'] = $this->service_features->get_images($feature['ID'], 4);
		
		$this->template->content = View::factory('pages/features/view')
							->bind('feature', $feature)
							->bind('image', $image);
		
	}
	
	public function action_image($feature) {
		$feature = $this->service_features->get_feature($feature);
		
		$image['thumbs'] = $this->service_features->get_images($feature['ID'], 2);
		$image['large'] = $this->service_features->get_images($feature['ID'], 3);
		
		$this->template->content = View::factory('pages/features/image')
							->bind('feature', $feature)
							->bind('image', $image);
		
	}
	
	public function action_edit($id) {
		$feature = $this->service_features->get_feature($id);

		$this->template->content = View::factory('pages/features/edit')
							->bind('feature', $feature);
		
	}
	
	public function action_add() {
		$this->template->content = View::factory('pages/features/add');
	}
	
	public function action_save() {
		$data = $_POST;
		
		$this->service_features->save_feature($data);
		
		if ( ! isset($data['id'])) {
			$this->request->redirect('/features');
		} else {
			$data['url'] = strtolower(preg_replace('/\s/', '-', $data['name']));
			
			$this->request->redirect('/features/view/' . $data['url']);
		}
	}
	
	public function action_save_image() {
		$data = $_POST;
		$file = $_FILES;
		
		$db_feature = $this->service_features->get_feature($data['id']);
		
		$feature['id'] = $db_feature['ID'];
		$feature['type'] = $data['type'];

		$this->service_features->save_image($file, $db_feature, $feature);
		
		$this->request->redirect('/features/view/' . $db_feature['URL']);
	}
	
	public function action_save_gallery() {
		$data = $_POST;
		$file = $_FILES;

		$db_feature = $this->service_features->get_feature($data['id']);
			
		$feature['id'] = $db_feature['ID'];
		
		$this->service_features->save_gallery($file, $db_feature, $feature);
		
		$this->request->redirect('/features/image/' . $db_feature['URL']);
	}	
	
	public function action_delete_image() {
		$data = $_POST;
		
		$this->service_features->delete_image($data);
		
		$this->request->redirect('/features/image/' . $data['url']);
	}
	
	public function action_save_audio() {
		$data = $_POST;
		$file = $_FILES;
		
		$db_feature = $this->service_features->get_feature($data['id']);
		
		$feature['name'] = $data['name'];

		if (empty($file['file']['name'])) {
			$feature['file'] = $db_feature['FileName'];
		} else {
			$feature['file'] = preg_replace('/\s/', '_', $file['file']['name']);
			$filename = preg_replace('/\s/', '_', $file['file']['name']);
			$s3 = new Amazon_S3();
			$s3->upload_file($file['file']['tmp_name'], 'ifrogz.com', 'lib/audio/features/' . $db_feature['URL'] . '/' . $filename);
		}
		
		$feature['id'] = $db_feature['ID'];

		$this->service_features->save_audio($feature);
		
		$this->request->redirect('/features/view/' . $db_feature['URL']);
	}

}
?>