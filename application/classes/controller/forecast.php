<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Forecast extends Controller_iFrogzBase {
	
	public function before() {
		parent::before();
		$auth_roles = array(
            Roles::ADMIN,
            Roles::MANAGEMENT,
            Roles::IT,
        );
        $user_roles = Session::instance()->get('Roles', array());
		Authenticate::regulate_access($auth_roles, $user_roles);
	}

    public function action_index() {
		$this->template->content = View::factory('pages/forecast/index');
    }
/*
    public function action_data() {
		$records = DB_SQL::select('default_ifrogz')
			->from('forecasts')
			->where('WeekOf', '=', $this->week_of(date('Y-m-d H:i:s')))
			->query();
    }
*/
    public function action_update() {
        /*
        $model = new Model_Leap_iFrogz_Forecast();
        $model->Customer = 'Test';
        $model->Part = 'CASE';
        $model->January = 1;
        $model->February = 2;
        $model->March = 3;
        $model->April = 4;
        $model->May = 5;
        $model->June = 6;
        $model->July = 7;
        $model->August = 8;
        $model->September = 9;
        $model->October = 10;
        $model->November = 11;
        $model->December = 12;
        $model->Total = 13;
        $model->WeekOf = $this->week_of(date('Y-m-d H:i:s'));
        $model->save();

        echo Debug::vars($model);
        exit();
        */
    }

    public function action_export() {
        echo Debug::vars($this->week_of(date('Y-m-d H:i:s')));
        exit();
    }

	public function action_report() {
		$records = DB_SQL::select('default_fishbowl')
		    ->column('ID')
			->column('NAME')
			->from('CUSTOMER')
			->where('ACTIVEFLAG', '=', 1)
			->order_by('NAME')
			->query();
		
        $customers = array();
        $customer = NULL;
        foreach ($records as $record) {
            $id = $record['ID'];
            $customers[$id] = $record['NAME'];
            if (is_null($customer)) {
                $customer = $id;
            }
        }

        $records = DB_SQL::select('default_fishbowl')
            ->column('NUM')
            ->column('DESCRIPTION')
            ->from('PART')
			->where('ACTIVEFLAG', '=', 1)
			->order_by('NUM')
            ->query();

		$this->AssetManager->set_asset(array('type' => 'text/css', 'media' => 'all', 'uri' => '/lib/css/forecast.css'));

	    $this->template->content = View::factory('pages/forecast/report')
			->bind('customers', $customers)
			->bind('parts', $records);
	}

    protected function week_of($date) {
        return implode('-', Timestamp::get_week($date)) . ' 00:00:00';
    }

}
?>