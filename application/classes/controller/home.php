<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_iFrogzBase {
	
	public function before() {
		parent::before();
		$this->session = Session::instance();		
	}
	
	public function action_index() {
		
		$view_orders_roles = array(
            Roles::ADMIN,
            Roles::MANAGEMENT,
            Roles::IT,
            Roles::CUSTOMER_SERVICE_ADMIN,
			Roles::CUSTOMER_SERVICE
        );
        $user_roles = Session::instance()->get('Roles', array());
		
		$view = View::factory('pages/home/home')
					->bind('user_roles', $user_roles)
					->bind('view_orders_roles', $view_orders_roles);

		$this->template->content = $view;
	}
	
}
