<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Controller_iFrogzBase extends Controller_Template {

	public $template = 'templates/treefrog';
	public $header = '';
	protected $AssetManager;

	public function before() {
	    parent::before();

        $Session = Session::instance();
        $this->AssetManager = new AssetManager();

		$employee = $Session->get('employee', array());
		$cookie_ifrogzuser = Cookie::decrypt('ifrogzuser', FALSE);
		if ($cookie_ifrogzuser) {
			$cookie_ifrogzuser = JSON::decode($cookie_ifrogzuser);
		}
		
        $user_id = Arr::get($employee, 'id', FALSE);
        if ( ! $user_id && Arr::get($cookie_ifrogzuser, 'id', FALSE)) {
            $user_id = Arr::get($cookie_ifrogzuser, 'id');
        }

        $user_roles = Arr::get($employee, 'permissions', FALSE);
        if ( ! $user_roles && Arr::get($cookie_ifrogzuser, 'roles', FALSE)) {
            $user_roles = Arr::get($cookie_ifrogzuser, 'roles');
        }
		
		// Signature contains ID, User Roles
        $signature = WSAPI::get_signature(WSAPI::get_secret('admin:login'), array('id' => $user_id, 'roles' => $user_roles));

		$bypass = Bypass::is_registered($this->request->controller(), $this->request->action());
		
		if ( ! $bypass) {
			// If session expired
			if ( ! Arr::get($employee, 'id', FALSE)) {
				// If they have a cookie
				if (Arr::get($cookie_ifrogzuser, 'id', FALSE)) {
					// Query DB and create roles and set session
					$results = DB_ORM::select('admin_employee')
					    ->where('id', '=', Arr::get($cookie_ifrogzuser, 'id'))
					    ->is_active()
					    ->limit(1)
					    ->query();
                    if ($results->is_loaded()) {
                        $record = $results->fetch(0);

						$signature = WSAPI::get_signature(WSAPI::get_secret('admin:login'), array('id' => $record->ID, 'roles' => $record->Permissions));
						
	                    $Session->set('Roles', $record->Roles);
	                    // Signs user's session
	                    $Session->set('Signature', $signature);
						
						// Save them in the session
						$employee = $record->as_array();
						$_SESSION['employee'] = $employee; // Old system
						$_SESSION['loggedon'] = 'virtualstore'; // Old system
						$Session->set('employee', $employee); // New system
						
						// Redirect to the page they requested
						$this->request->redirect($this->request->uri());
					} else {
	                    $this->request->redirect('/user/login');
	                }	
				} else {
					$this->request->redirect('/user/login');
				}
			} else if ($signature != $Session->get('Signature', '')) {				
				$Session->set('Roles', NULL);
                $this->request->redirect('/user/login');
			}	
		}
        
        if ($this->auto_render) {
			$this->template->title = 'iFrogz';
			$this->template->meta_keywords = '';
			$this->template->meta_description = '';
			$this->template->styles = array();
			$this->template->scripts = array();
			$this->template->additions = array();
			$this->template->content = '';

			$this->AssetManager->set_asset(array('type' => 'application/php', 'uri' => APPPATH . 'views/partials/analytics/googleanalytics.php'));
			$this->AssetManager->set_library(Asset::JQUERY_UI);
			$this->AssetManager->set_library(Asset::JQUERY_FORM);
			$this->AssetManager->set_library(Asset::KOHANA_ERROR_LOG);
		}
    }

    public function action_index() {
        $this->request->redirect('error/404/');
    }

    public function after() {
	    $this->template->assets = $this->AssetManager->render();

	    parent::after();
    }

}
?>