<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Minicart extends Controller_iFrogzBase {
	
	public $template = 'templates/products';
	
	public function action_add() {
		$this->template->content = View::factory('pages/minicart/add');
	}
	
	public function action_index() {
		$items = $this->get_minicart_items();

		$this->template->content = View::factory('pages/minicart/index')
							->bind('items', $items);
	}
	
	public function action_edit() {
		$auth_roles = array(
            Roles::FULL_ACCESS,
            Roles::IT
        );

        $user_roles = Session::instance()->get('Roles', array());

		$id = $this->request->param('id');
		
		$item = $this->get_minicart_item($id);

		$this->template->content = View::factory('pages/minicart/edit')
							->bind('item', $item)
							->bind('auth_roles', $auth_roles)
							->bind('user_roles', $user_roles);
	}
	
	public function action_view() {
		$auth_roles = array(
            Roles::FULL_ACCESS,
            Roles::IT
        );

        $user_roles = Session::instance()->get('Roles', array());

		$id = $this->request->param('id');
		
		$Session = Session::instance();
		$success = $Session->get('success', '');
		
		$item = $this->get_minicart_item($id);

		$this->template->content = View::factory('pages/minicart/view')
							->bind('item', $item)
							->bind('auth_roles', $auth_roles)
							->bind('user_roles', $user_roles)
							->bind('success', $success);
							
		$Session->set('success', NULL);
	}
	
	public function action_delete() {

		$id = $this->request->param('id');
		
		$this->delete_minicart_item($id);
		
		$this->request->redirect('/minicart/');
	}
	
	public function action_update() {
		$clean = Kohana::sanitize($_POST);
		
		$file = $_FILES;

		if ( ! empty($file['file']['name'])) {
			$this->new_image($file);

			$filename = preg_replace('/\s/', '_', $file['file']['name']);
		} else {
			$item = $this->get_minicart_item($clean['id']);
			
			$filename = $item->FileName;
		}
		
		$this->save_minicart_item($clean['id'], $clean['product'], $clean['title'], $clean['body'], $clean['status'], $filename);
		$Session = Session::instance();
		$Session->set('success', 'Saved');
		
		$this->request->redirect('/minicart/');
	}
	
	public function action_new() {
		$clean = Kohana::sanitize($_POST);
		
		$file = $_FILES;

		if ( ! empty($file['file']['name'])) {
			$this->new_image($file);

			$filename = preg_replace('/\s/', '_', $file['file']['name']);
		} else {
			$filename = '';
		}
		
		$item = $this->new_minicart_item($clean['product'], $clean['title'], $clean['body'], $clean['status'], $filename);
		$Session = Session::instance();
		$Session->set('success', 'Saved');
		
		$this->request->redirect('/minicart/');
	}
	
	private function get_minicart_items() {
		$items = Jelly::select('iFrogz_MiniCartItem')
					->where('IsActive', '=', 1)
					->order_by('Sequence')
					->execute();
					
		return $items;
	}
	
	private function get_minicart_item($id) {
		$item = Jelly::select('iFrogz_MiniCartItem')
					->where('ID', '=', $id)
					->load();
					
		return $item;
	}
	
	private function new_image($file) {
		$filename = preg_replace('/\s/', '_', $file['file']['name']);
		
		$s3 = new Amazon_S3();
		$save = $s3->upload_file($file['file']['tmp_name'], 'ifrogz.com', 'lib/misc/minicart/' . $filename);

		if ( ! isset($save)) {
			return 'Error in the upload.';
		}
	}
	
	private function save_minicart_item($id, $product_id, $title, $body, $status, $filename) {
		Jelly::update('iFrogz_MiniCartItem')
				->where('ID', '=', $id)
				->set(array(
					'Product' => $product_id,
					'RFEmployee_ID' => '60',
					'Title' => $title,
					'Body' => $body,
					'Status' => $status,
					'FileName' => $filename
				))
				->execute();
	}
	
	private function delete_minicart_item($id) {
		Jelly::update('iFrogz_MiniCartItem')
				->where('ID', '=', $id)
				->set(array('IsActive' => 0))
				->execute();
	}
	
	private function new_minicart_item($product_id, $title, $body, $status, $filename) {
		$item = Jelly::factory('iFrogz_MiniCartItem')
				->set(array(
					'Product' => $product_id,
					'RFEmployee_ID' => '60',
					'Title' => $title,
					'Body' => $body,
					'Status' => $status,
					'FileName' => $filename,
					'Sequence' => 1,
					'IsActive' => 1
				))
				->save();
				
		return $item;
	}
	
}
?>
