<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Password extends Controller_iFrogzBase {

	public function before() {
		parent::before();
		$this->session = Session::instance();
	}

    public function action_change() {
        $this->AssetManager->set_asset(array('type' => 'text/javascript','uri' => '/lib/js/pages/password/change.js'));
		$this->template->content = View::factory('pages/password/change');
    }

    public function action_forgot() {
        $this->AssetManager->set_asset(array('type' => 'text/javascript','uri' => '/lib/js/pages/password/forgot.js'));
		$this->template->content = View::factory('pages/password/forgot');
    }

    public function action_reset() {
		$this->auto_render = FALSE;

        if ( ! isset($_POST['email'])) {
            $this->request->redirect('error/404');
        }

        $response = new REST_Response();

        $results = DB_ORM::select('admin_employee')
            ->where('email', '=', trim($_POST['email']))
            ->is_active()
            ->limit(1)
            ->query();
        if ($results->is_loaded()) {
            $employee = $results->fetch(0);
            $full_name = "{$employee->firstname} {$employee->lastname}";
            
            $password = Text::random('symbolic', 10);

            $message  = '<html>';
            $message .= '<body>';
            $message .= "{$employee->firstname},<br />";
            $message .= '<br />';
            $message .= 'Your password has been reset.  Your new password is:<br />';
            $message .= '<br />';
            $message .= htmlentities($password) . '<br />';
            $message .= '<br />';
            $message .= 'Sincerely,<br />';
            $message .= 'IT Department<br />';
            $message .= '</body>';
            $message .= '</html>';

            $mailer = new Mailer('GMAIL', 'AMAZON_IFROGZ');
		    
		    $mailer->set_content_type('text/html');
			$mailer->set_subject('[iFrogz Admin] Password Reset');
			$mailer->set_message($message);

            $mailer->set_sender(new EmailAddress('it@ifrogz.com', 'IT Department'));
                			
            $mailer->add_recipient(new EmailAddress($employee->email, $full_name));

			if ( ! $mailer->send()) {
				$response->set_status(Status::FAILED);
                $response->add_error('Failed to email new password.');
			}
            else {
                $response->status(Status::SUCCESS);
                $response->add_record('Your new password has been sent to your email account.');
                
                $employee->Password = md5($password);
                $employee->save();
        		
        		$this->delete_cookie();
            }
        }
        else {
			$response->set_status(Status::FAILED);
            $response->add_error('Failed to email new password.');
        }
        $response->output(Mime::JSON);
    }

    public function action_update() {
		$this->auto_render = FALSE;

        $response = new REST_Response();
        
        $Session = Session::instance();
        $employee = $Session->get('employee', array());

        if ( ! isset($_POST['password_1']) || ! isset($_POST['password_2']) || ($_POST['password_1'] != $_POST['password_2']) || ! isset($employee['id'])) {
			$response->set_status(Status::FAILED);
            $response->add_error('Failed to update password.');
        }
        else {
            try {
    			$model = DB_ORM::model('admin_employee');
    			$model->ID = $employee['id'];
    			$model->load();
    			$model->Password = md5($_POST['password_1']);
    			$model->save();

			    $response->status(Status::SUCCESS);
                $response->add_record('Your password has been updated.');
			}
			catch (Exception $ex) {
    			$response->set_status(Status::FAILED);
                $response->add_error('Failed to update password.');
			}
        }

        $response->output(Mime::JSON);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private function delete_cookie() {
        $_SESSION['employee'] = NULL;
		$_SESSION['loggedon'] = NULL;

		$this->session->delete('employee');

		Cookie::$domain = $this->get_cookie_domain();
		Cookie::delete('ifrogzuser');
	}

	private function get_cookie_domain() {
		$domain = '';
		$matches = array();
		// If the server name is not a localhost, then make the cookie available for subdomains
		if (preg_match('/[a-z]+\.[a-z]{2,4}$/i', $_SERVER['SERVER_NAME'], $matches)) {
			$domain = '.' . $matches[0];
		}
		return $domain;
	}

}
?>