<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Press extends Controller_iFrogzBase {

	public function action_index() {
		$active = $this->get_active_releases();
		
		$inactive = $this->get_inactive_releases();
		
		$this->template->content = View::factory('pages/press/index')
							->bind('active', $active)
							->bind('inactive', $inactive);
	}
	
	public function action_edit() {
		$release = $this->get_release();
		$this->template->content = View::factory('pages/press/edit')
							->bind('release', $release);
	}
	
	public function action_add() {
		$this->template->content = View::factory('pages/press/add');
	}
		
	public function action_kabron() {
		$s3 = new Amazon_S3();
        $s3->upload_file('assets/icons/fast_forward.png', 'test.png', 'bucket_name');

		echo Amazon::asset('test.png', 'bucket_name');
		
	}
	public function action_save() {
		$data = $_POST;
		$file = $_FILES;
			
		//print_r($file);
		//exit();
		
		// $validate = Validation::factory($_FILES);
		// $validate->rules('name',
		// array('Upload::valid' => array(),
		// 'Upload::not_empty' => array(), 
		// 'Upload::type' => array('Upload::type' => array('txt')), 
		// 'Upload::size' => array('4KB'))
		// );

		$filename = preg_replace('/\s/', '_', $file['file']['name']);
		
		$s3 = new Amazon_S3();
        // $s3->upload($file['file']['tmp_name'], $filename, 'ifrogz.com/lib/misc/press');
		$s3->upload_file($file['file']['tmp_name'], 'ifrogz.com', 'lib/misc/press/' . $filename);

		$employee_id = $_SESSION['employee']['id'];
		$today = date("Y-m-d H:i:s");
			
		$release['employee_id'] = $employee_id;
		$release['title'] = $data['title'];
		$release['file'] = $filename;
		$release['icon'] = $data['icon'];
		$release['posted'] = $data['posted'];
		$release['active'] = 1;
		$release['created'] = $today;
		
		$this->save_release($release);
		
		$this->request->redirect('/press');

	}
	
	public function action_saveedit() {
		$data = $_POST;
		$file = $_FILES;
				
		// $validate = Validation::factory($_FILES);
		// $validate->rules('name',
		// array('Upload::valid' => array(),
		// 'Upload::not_empty' => array(), 
		// 'Upload::type' => array('Upload::type' => array('txt')), 
		// 'Upload::size' => array('4KB'))
		// );
		 
		$filename = preg_replace('/\s/', '_', $file['file']['name']);
		
		if ( ! empty($file['file']['name'])) {
			$s3 = new Amazon_S3();
	        $s3->upload($file['file']['tmp_name'], $filename, 'ifrogz.com/lib/misc/press');
		} 
		
		$employee_id = $_SESSION['employee']['id'];
		$today = date("Y-m-d H:i:s");
		
		if (isset($data['active'])) {
			if ($data['active'] == 'yes') {
				$release['active'] = 1;
			} 
		} else {
			$release['active'] = 0;
		}
		
		$db_data = $this->get_edit_release($data['id']);

		if (empty($file['file']['name'])) {
			$release['file'] = $db_data[0]['FilePath'];
		} else {
			$release['file'] = $filename;
		}
		
		$release['id'] = $data['id'];
		$release['employee_id'] = $employee_id;
		$release['title'] = $data['title'];
		$release['icon'] = $data['icon'];
		$release['posted'] = date('Y-m-d H:i:s', strtotime($data['posted']));

		$this->save_edit_release($release);
		
		$this->request->redirect('/press');

	}
	
	private function save_release($data) {
		Jelly::factory('iFrogz_DRPressRelease')
		     ->set(array(
		         'RFEmployee_ID' => $data['employee_id'],
		         'Title' => $data['title'],
		         'FilePath' => $data['file'],
				 'IconPath' => $data['icon'],
				 'IsActive' => $data['active'],
				 'DatePosted' => $data['posted'],
				 'DateCreated' => $data['created'],
		     ))->save();
	}
	
	private function save_edit_release($data) {
		$update = Jelly::factory('iFrogz_DRPressRelease');
			$update->RFEmployee_ID = $data['employee_id'];
		    $update->Title = $data['title'];
		    $update->FilePath = $data['file'];
			$update->IconPath = $data['icon'];
			$update->IsActive = $data['active'];
			$update->DatePosted = $data['posted'];
			$update->save($data['id']);
	}
	
	private function get_active_releases() {
		$releases = Jelly::select('iFrogz_DRPressRelease')
				->where('IsActive', '=', '1')
				->execute()
				->as_array();
				
		return $releases;
	}
	
	private function get_inactive_releases() {
		$releases = Jelly::select('iFrogz_DRPressRelease')
				->where('IsActive', '=', '0')
				->execute()
				->as_array();
				
		return $releases;
	}
	
	private function get_release() {
		$id = $this->request->param('id');
		
		$releases = Jelly::select('iFrogz_DRPressRelease')
				->where('ID', '=', $id)
				->execute()
				->as_array();
				
		$releases[0]['employee'] = $this->get_employee_data($releases[0]['RFEmployee_ID']);
		return $releases;
	}
	
	private function get_edit_release($id) {
		$releases = Jelly::select('iFrogz_DRPressRelease')
				->where('ID', '=', $id)
				->execute()
				->as_array();

		return $releases;
	}
	
	private function get_employee_data($id) {
		$employees = ORM::factory('admin_employee')
			->where('id', '=', $id)
			->find_all()
			->as_array();
			
			foreach ($employees as $key => $employee) {
				$temp = $employee->as_array();
				unset($temp['password'], $temp['HPassword']);
				$employees[$key] = $temp;
			}
			
			// print_r($employees);
			// exit();
		return $employees;
	}
	
}
?>