<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Reviews extends Controller_iFrogzBase {

	public function action_index() {

        $review_status_options = array(
            1 => 'all',
            2 => 'reviewed',
            3 => 'pending',
            4 => 'approved',
            5 => 'declined',
        );
        $rank_one_options = array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
        );
        $rank_two_options = array(
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
        );

		$config = Kohana::$config->load('database.default_ifrogz');
		$config = $config['connection'];
		
		$host_server = $config['hostname'];
		$username = $config['username'];
		$password = $config['password'];
		$database = $config['database'];

		$identifier = mysql_connect($host_server, $username, $password);

        if ($_POST) {
            $status = mysql_real_escape_string($_POST['review_status']);
            $rank1= mysql_real_escape_string($_POST['rank1']);
            $rank2 = mysql_real_escape_string($_POST['rank2']);
            $date1 = mysql_real_escape_string($_POST['first_date']);
            $date2 = mysql_real_escape_string($_POST['last_date']);

            $_SESSION['first_date'] = $date1;
            $_SESSION['last_date'] = $date2;
            $_SESSION['rank1'] = $rank1;
            $_SESSION['rank2'] = $rank2;
            $_SESSION['review_status'] = $status;

        } else {

            if (isset($_SESSION['review_status'])) {
                $status = mysql_real_escape_string($_SESSION['review_status']);
            } else {
                $status = '1';
            }
            if (isset($_SESSION['rank1'])) {
                $rank1 = mysql_real_escape_string($_SESSION['rank1']);
            } else {
                $rank1 = '1';
            }
            if (isset($_SESSION['rank2'])) {
                $rank2 = mysql_real_escape_string($_SESSION['rank2']);
            } else {
                $rank2 = '1';
            }
            if (isset($_SESSION["first_date"])) {
                $date1 = mysql_real_escape_string($_SESSION["first_date"]);
            } else {
                $date1 = '';
            }
            if (isset($_SESSION["last_date"])) {
                $date2 = mysql_real_escape_string($_SESSION["last_date"]);
            } else {
                $date2 = '';
            }
        }

        if (isset($status)) {
            if ($status == 1) {
                $status_ID = '1, 2, 3';
            }
            if ($status == 2) {
                $status_ID = '2, 3';
            }
            if ($status == 3) {
                $status_ID = '1';
            }
            if ($status == 4) {
                $status_ID = '3';
            }
            if ($status == 5) {
                $status_ID = '2';
            }
        } else {
                $status_ID = '1, 2, 3';
        }

        // Datebase Query
        $query = "SELECT reviews.revID AS ID, DATE_FORMAT(reviews.revDate, '%Y-%m-%d') AS DateModified, reviews.revTitle AS Title, reviews.revProd, reviews.revReview AS Body, reviews.revScore AS Score, reviews_status.ID AS StatusID, reviews.revPerson FROM reviews LEFT JOIN reviews_status ON (reviews.Status_ID = reviews_status.ID) WHERE reviews.revScore BETWEEN '{$rank1}' AND '{$rank2}' AND reviews.revDate BETWEEN '{$date1}' AND '{$date2}' AND Status_ID IN ({$status_ID}) ORDER BY reviews.revID DESC, reviews.revScore";

        mysql_select_db($database, $identifier);
        $results = mysql_query($query, $identifier);

        // printing table rows
        while($row = mysql_fetch_array($results)) {
            $reviewID[] = $row['ID'];
            $reviewDate[] = $row['DateModified'];
            $reviewTitle[] = $row['Title'];
            $reviewBody[] = $row['Body'];
            $reviewScore[] = $row['Score'];
            $reviewStatus[] = $row['StatusID'];
            $reviewID_Status[] = $row['ID'] . '-' . $row['StatusID'];
            $reviewProd[] = $row['revProd'];
            $reviewPerson[] = $row['revPerson'];
        }

        mysql_close($identifier);

        if (isset($_SESSION['change_status'])) {
            $changeMessage = $_SESSION['change_status'];
            unset($_SESSION['change_status']);
        }

        $this->template->content = View::factory('pages/reviews/index')
            ->bind('date1', $date1)
            ->bind('date2', $date2)
            ->bind('status', $status)
            ->bind('rank1', $rank1)
            ->bind('rank2', $rank2)
            ->bind('reviewID', $reviewID)
            ->bind('reviewTitle', $reviewTitle)
            ->bind('reviewID', $reviewID)
            ->bind('reviewDate', $reviewDate)
            ->bind('reviewTitle', $reviewTitle)
            ->bind('reviewBody', $reviewBody)
            ->bind('reviewScore', $reviewScore)
            ->bind('reviewStatus', $reviewStatus)
            ->bind('reviewProd', $reviewProd)
            ->bind('reviewID_status', $reviewID_Status)
            ->bind('reviewPerson', $reviewPerson)
            ->bind('changeMessage', $changeMessage)
            ->bind('review_status_options', $review_status_options)
            ->bind('rank_one_options', $rank_one_options)
            ->bind('rank_two_options', $rank_two_options);
        }

	public function action_edit($ID) {
		$config = Kohana::$config->load('database.default_ifrogz');
		$config = $config['connection'];
		
		$host_server = $config['hostname'];
		$username = $config['username'];
		$password = $config['password'];
		$database = $config['database'];
		
		$query = "SELECT revID, revTitle, revReview, revScore, revApproved, revProd, revDate, Status_ID, Employee_ID, DateModified FROM reviews WHERE revID = {$ID}";
		$identifier = mysql_connect($host_server, $username, $password);
		mysql_select_db($database, $identifier);
		$results = mysql_query($query, $identifier);

		while($row = mysql_fetch_array($results))
		{
			$ReviewTitle = $row['revTitle'];
			$ReviewBody = $row['revReview'];
			$ReviewStatus = $row['Status_ID'];
		    $ReviewScore = $row['revScore'];
			$DateCreated = $row['revDate'];	
			$ReviewProduct = $row['revProd'];
			$employeeID = $row['Employee_ID'];
			$DateModified = $row['DateModified'];

		}
		
		$ProductURL = "http://ifrogz.com/product/" . $ReviewProduct;

		$query = "SELECT pName FROM products WHERE pID = '{$ReviewProduct}'";
		$identifier = mysql_connect($host_server, $username, $password);
		mysql_select_db($database, $identifier);
		$product_results = mysql_query($query, $identifier);

		while($product_row = mysql_fetch_array($product_results))
		  {	
			$ProductName = $product_row['pName'];				
		  }
		
		$config = Kohana::$config->load('database.default_ifrogz');
		$config = $config['connection'];
		
		$db_user = $config['username'];
		$db_pass = $config['password'];
		$db_tablename = $config['database'];
		$db_ip = $config['hostname'];

		$con=mysql_connect($db_ip, $db_user, $db_pass) or die ('Connect Error');
		mysql_select_db($db_tablename) or die ('Select DB Error');
		
		$employee_result = mysql_query("SELECT firstname, lastname FROM employee WHERE id = '$employeeID'");

		while($employee_row = mysql_fetch_array($employee_result))
			{
				$EmployeeName = $employee_row['firstname'] . " " . $employee_row['lastname'] . " on ";

			}

		// $reviewID = $_POST["record_id"];
		// $reviewStatusID = $_POST["status_id"];
		
		// $revApproved = ($reviewStatusID == '3') ? 'Y' : 'N';
		$today = date("Y-m-d h:i:s");
	
		$this->template->content = View::factory('pages/reviews/edit')
							->bind('ID', $ID)
							->bind('revReviewer', $revReviewer)
							->bind('today', $today)
							->bind('ReviewTitle', $ReviewTitle)
							->bind('ReviewBody', $ReviewBody)
							->bind('ReviewStatus', $ReviewStatus)
							->bind('ReviewScore', $ReviewScore)
							->bind('DateCreated', $DateCreated)
							->bind('ReviewProduct', $ReviewProduct)
							->bind('EmployeeName', $EmployeeName)
							->bind('DateModified', $DateModified)
							->bind('ProductName', $ProductName)
							->bind('ProductURL', $ProductURL);
							
	}
	
	public function action_update() {
		$config = Kohana::$config->load('database.default_ifrogz');
		$config = $config['connection'];
		
		$host_server = $config['hostname'];
		$username = $config['username'];
		$password = $config['password'];
		$database = $config['database'];

		$identifier = mysql_connect($host_server, $username, $password);
		
		// Get data

		function check_input($value)
			{
			// Stripslashes
			if (get_magic_quotes_gpc())
			  {
			  $value = stripslashes($value);
			  }
			// Quote if not a number
			if (!is_numeric($value))
			  {
			  $value =  mysql_real_escape_string($value);
			  }
			return $value;
			}

			$revBody = check_input($_POST["rev_body"]);
			$revTitle = check_input($_POST["title"]);
			$StatusID = mysql_real_escape_string ($_POST["approved"]);
			$revNumber = mysql_real_escape_string ($_POST["ID"]);
			$revReviewer = mysql_real_escape_string ($_SESSION['employee']['id']);
			$revApproved = ($StatusID == '3') ? 'Y' : 'N';
			$today = date("Y-m-d H:i:s");

		// sending query

		$query = "UPDATE reviews SET revApproved='{$revApproved}', revTitle='{$revTitle}', revReview='{$revBody}', Status_ID='{$StatusID}', Employee_ID = '{$revReviewer}', DateModified = '{$today}' WHERE revID = '{$revNumber}'";

		mysql_select_db($database, $identifier);
		$results = mysql_query($query, $identifier);
		$result = mysql_query($query, $identifier);


		$_SESSION['change_status'] = ( ! $result) ? 'failed: ' . mysql_error($result) : 'Review ' . $revNumber . ' updated';

		$this->request->redirect("reviews");
	}

	public function action_updatestatus() {
		$reviewID = $_POST["record_id"];
		$reviewStatusID = $_POST["status_id"];
		$revReviewer = $_SESSION['employee']['id'];
        Fire::log($reviewStatusID);
		$revApproved = ($reviewStatusID == '3') ? 'Y' : 'N';
		$today = date("Y-m-d H:i:s");
		
		$config = Kohana::$config->load('database.default_ifrogz');
		$config = $config['connection'];
		
		$host_server = $config['hostname'];
		$username = $config['username'];
		$password = $config['password'];
		$database = $config['database'];
		
		$query = "UPDATE reviews SET revApproved='{$revApproved}', Status_ID='{$reviewStatusID}', Employee_ID='{$revReviewer}', DateModified='{$today}' WHERE revID = {$reviewID};";
		$identifier = mysql_connect($host_server, $username, $password);
		mysql_select_db($database, $identifier);
		$results = mysql_query($query, $identifier);
		mysql_close($identifier);

		$json = array();
		if ( ! $results) {
			$json['response']['status'] = 'failed';
			$json['response']['errors'][0]['error']['message'] = 'failed: ' . mysql_error($results);
		}
		else {
			$json['response']['status'] = 'success';
			$json['response']['data'] = '<table width="100%" cellspacing="0" cellpadding="2">
			  <tr height="50px">
			    <td bgcolor="#FFFF99" style="color:#000000;  font-size:18px; text-align:center;">Review updated</td>
			  </tr>
			  <tr height="20px"><td></td></tr>
			</table>';
			}

		header('Content-Type: text/x-json');
		echo JSON::encode($json);
		exit();	
	}

    public function after() {

        $this->AssetManager->set_library('formee');
        $this->AssetManager->set_library('jquery-ui');

        parent::after();

    }
							
}
?>