<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_iFrogzBase {

	public function before() {
		parent::before();
		$this->session = Session::instance();
	}

	public function action_deletecookie() {
		$this->auto_render = FALSE;
		$this->delete_cookie();
		$this->request->redirect('home');
	}

	public function action_index() {
		die('goat6');
		if ( ! $this->session->get('employee', FALSE)) {
			$this->request->redirect('user/login');
			exit;
		} else {
			$this->request->redirect('home');
			exit;
		}
	}

	public function action_login() {
		$error_message = NULL;
		if ($_POST) {
			$post = Validation::factory($_POST)
						//->filter(TRUE, 'trim')
						->rule('user', 'not_empty')
						->rule('pass', 'not_empty')
						->rule('rememberme', 'max_length', array(':value', 10));
			if ($post->check()) {
				$data = $post->as_array();
				
				// Validate their login
				$employee = ORM::factory('admin_employee')
                    ->where(DB::expr('LOWER(`'.Model_ORM_Admin_Employee::USERNAME.'`)'), '=', strtolower($data['user']))
                    ->where(Model_ORM_Admin_Employee::PASSWORD, '=', md5($data['pass']))
                    ->where(Model_ORM_Admin_Employee::IS_ACTIVE, '=', TRUE)
                    ->find();

				if ($employee->loaded()) {
					$emp = $employee->as_array();
					
					$signature = WSAPI::get_signature(WSAPI::get_secret('admin:login'), array('id' => $emp[Model_ORM_Admin_Employee::ID], 'roles' => $emp[Model_ORM_Admin_Employee::ROLES]));
					
					// Loads user's roles
                    $user_roles = explode(',', $emp[Model_ORM_Admin_Employee::ROLES]);
                    $length = count($user_roles);
                    for ($i = 0; $i < $length; $i++) {
                        $user_roles[$i] = trim($user_roles[$i]);
                        if (empty($user_roles[$i])) {
                            unset($user_roles[$i]);
                        }
                    }
                    $this->session->set('Roles', $user_roles);
                    // Signs user's session
                    $this->session->set('Signature', $signature);

					// Save them in the session
					$_SESSION['employee'] = $emp; // Old system
					$_SESSION['loggedon'] = 'virtualstore'; // Old system
					$this->session->set('employee', $emp); // New system
					
					// Check to remember their login
					if ( ! empty($data['rememberme'])) {
						$this->save_cookie();
					} else {
						$this->delete_cookie();
					}
					
					// Redirect to main page
					$this->request->redirect('home');
				} else {
					# Give error message
					$error_message = 'The username or password you entered is incorrect.';
				}
			} else {
				$error_message = 'The username or password you entered is incorrect.';
			}
		}
		
		$view = View::factory('pages/user/login')
					->bind('error_message', $error_message);

		$this->template->content = $view;
	}
	
	public function action_logout() {
		$_SESSION['employee'] = NULL;
		$_SESSION['loggedon'] = NULL;

		$this->session->delete('employee');
		
		$this->delete_cookie();
		
		$view = View::factory('pages/user/logout');
		
		$this->template->content = $view;
	}

    public function action_savecookie() {
        $this->auto_render = FALSE;
        $this->save_cookie();
        $this->request->redirect('home');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private function delete_cookie() {
		Cookie::$domain = $this->get_cookie_domain();
		Cookie::delete('ifrogzuser');
	}
	
	private function get_cookie_domain() {
		$domain = '';
		$matches = array();
		// If the server name is not a localhost, then make the cookie available for subdomains
		if (preg_match('/[a-z]+\.[a-z]{2,4}$/i', $_SERVER['SERVER_NAME'], $matches)) {
			$domain = '.' . $matches[0];
		}
		return $domain;
	}

	private function save_cookie() {
		$emp = $this->session->get('employee');
		if ( ! empty($emp)) {
			$expiration = Date::DAY;
			Cookie::$domain = $this->get_cookie_domain();
			$success = Cookie::encrypt('ifrogzuser', JSON::encode(array('id' => $emp['id'], 'roles' => $this->session->get('Roles'))), $expiration);
			if (!$success) {
				echo Debug::vars('failed');
			}
		}
	}

}
?>