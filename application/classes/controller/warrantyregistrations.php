<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Contains all the logic for registering an ifrogz product
 *
 * @package    iFrogz
 * @author     iFrogz Team
 */
class Controller_WarrantyRegistrations extends Controller_iFrogzBase {

	public $template = 'old/main';

	public function before() {
		parent::before();

		$this->template->title = 'Admin: Warranty Registrations';
		$this->template->content = new View('pages/warranty/warranty');

        $search_box = null;

        $categories = array(
		    'ID'            => 'Reg. ID',
		    'FirstName'    => 'First Name',
		    'LastName'     => 'Last Name',
		    'City'          => 'City',
		    'State'         => 'State',
		    'Store'         => 'Store',
		    'Product'       => 'Product',
		    'PurchasePrice' => 'Purchase Price'
		);

        $Session = Session::instance();

		if (isset($_POST['submit'])) {
		    $search_box = array();
		    $search_box['method'] = $_POST['method'];
		    $search_box['categories'] = $categories;
		    $search_box['category'] = $_POST['category'];
		    $search_box['text'] = trim($_POST['text']);
		    $search_box['beg_date'] = $_POST['beg_date'];
		    $search_box['end_date'] = $_POST['end_date'];
		    if (strcmp($search_box['beg_date'], $search_box['end_date']) > 0) {
    			$search_box['end_date'] = $search_box['beg_date'];
    		}
    		$Session->set('warranty_registrations', $search_box);
		}
		else {
		    $search_box = $Session->get('warranty_registrations');
		    if (is_null($search_box)) {
		        $search_box = array();
		        $search_box['method'] = 'keyword';
		        $search_box['categories'] = $categories;
		        $search_box['category'] = 'Reg. ID';
		        $search_box['text'] = '';
		        $search_box['beg_date'] = date("Y-m-d");
		        $search_box['end_date'] = date("Y-m-d");
		        $Session->set('warranty_registrations', $search_box);
	        }
		}

        $this->template->content->search_box = $search_box;
	}

	public function action_index() {
        $this->action_search();
    }

    public function action_search() {
	    $page = $this->request->param('id', 1);

        if (!preg_match('/^(0|[1-9][0-9]*)$/', '' . $page)) {
            $page = '1';
        }

        $limit = 20;
        $offset = (intval($page) - 1) * $limit;

        $Session = Session::instance();
        $search_box = $Session->get('warranty_registrations');

	    $registrations = ORM::factory('iFrogz_oewarrantyregistration');

        if (!is_null($search_box)) {
            if ((strcmp($search_box['method'], 'keyword') == 0) && !empty($search_box['text'])) {
	            $registrations->where($search_box['category'], '=', $search_box['text']);
            } else {
	            $registrations->where('DateCreated', '>', $search_box['beg_date'] . ' 00:00:00')
	                ->where('DateCreated', '<=', $search_box['end_date'] . ' 23:59:59');
	            //$registrations->where(":date_registered BETWEEN '{$search_box['beg_date']} 00:00:00' AND '{$search_box['end_date']} 23:59:59'");
            }
        }

	    $pagination_regs = clone $registrations;

	    $registrations
			    ->limit($limit)
			    ->offset($offset);

	    $pagination = new Pagination(array(
			    'current_page'      => array('source' => 'route', 'key' => 'id'),
			    'items_per_page'    => $limit,
			    'total_items'       => $pagination_regs->find_all()->count(),
			    'view'              => 'pagination/digg',
			    'alt_route_params'  => array('controller' => 'warrantyregistrations', 'action' => 'search')
		    ));

		$this->template->content->content = new View('pages/warranty/search');
		$this->template->content->content->records = $registrations->find_all();
		$this->template->content->content->pagination = $pagination;
    }

	public function action_registration() {
		$id = $this->request->param('id', -1);

		$registration = DB_SQL::select('default_ifrogz')->from('OEWarrantyRegistrations')->where('ID', '=', $id)->query()->fetch(0);

		$this->template->content->content = new View('pages/warranty/registration');
		$this->template->content->content->registration = $registration;
	}

}
?>