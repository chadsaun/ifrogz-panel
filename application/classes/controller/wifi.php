<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_WiFi extends Controller_iFrogzBase {
	
	protected $service_wifi = NULL;
	protected $employee_id = NULL;
	
	public function before() {
		parent::before();
		
		$employee = Session::instance()->get('employee');
		$this->employee_id = $employee['id'];
		
		$dal_wifi = new DAL_WiFi_Implementation();
		$this->service_wifi = new Service_WiFi_Implementation($dal_wifi);
	}
	
	public function action_index() {
		$all_networks = $this->service_wifi->get_all_viewable_networks();
		$this->template->content = View::factory('pages/wifi/index')
					->bind('networks', $all_networks);
		$this->template->title = 'WiFi Password';
	}
	
	public function action_delete() {
		$network_tier_id = $this->request->param('id');
		$network_tier = $this->service_wifi->get_network_tier_by_id($network_tier_id);
		
		if ( ! empty($network_tier)) {
			$this->service_wifi->delete_network_tier_by_id($network_tier_id);
			$this->request->redirect('/wifi/admin');
		} else {
			throw new Exception('The network tier with id: '.$id.' could not be found');
		}
	}
	
	public function action_password() {
		$response = array();
		$response['response']['status'] = Status::SUCCESS;
		if (isset($_POST['network_tier_public_name'])) {
			$password = $this->service_wifi->get_password($_POST['network_tier_public_name']);
			$response['response']['data']['password'] = $password;
			// Now we log that user has retrieved the password.
			$ip_address = $_SERVER['REMOTE_ADDR'];
			$this->service_wifi->log_password_retrieval($_POST['network_tier_public_name'], $this->employee_id, $ip_address);
		} else {
			$response['response']['status'] = Status::FAILED;
			$response['response']['errors']['error'][0]['code'] = '';
			$response['response']['errors']['error'][0]['message'] = 'wrong credentials provided';
		}
		
		echo JSON::encode($response);
		exit();
	}
	
	public function action_admin() {
		$all_networks = $this->service_wifi->get_all_networks();
		$this->template->content = View::factory('pages/wifi/admin')
				->bind('networks', $all_networks);
		$this->template->title = 'Admin: WiFi Password';
	}
	
	public function action_edit() {
		if (isset($_POST['Edit']) && isset($_POST['ID'])) {
			$network = $this->service_wifi->get_network_tier_by_id($_POST['ID']);
			$network['PublicName'] = $_POST['PublicName'];
			$network['PrivateName'] = $_POST['PrivateName'];
			
			$encrypt = Encrypt::instance($network['password']['type']['Name']);
			$password = $encrypt->decode($network['password']['Hash']);
			
			if (strcmp($_POST['Hash'], $password) != 0) {
				$values = $network['password'];
				$values['IsActive'] = 1;
				$values['Hash'] = $_POST['Hash'];
				$values['DateCreated'] = date('Y-m-d H:i:s');
				unset($values['ID']);
				$network['SCPasswords_ID'] = $this->service_wifi->create_password($values);
			}
			if ( ! isset($_POST['IsViewable'])) {
				$network['IsViewable'] = 0;
			} else {
				$network['IsViewable'] = $_POST['IsViewable'];
			}
			if ( ! isset($_POST['IsActive'])) {
				$network['IsActive'] = 0;
			} else {
				$network['IsActive'] = $_POST['IsActive'];
			}
			$network_id = $network['ID'];
			unset($network['password']);
			unset($network['ID']);
			$this->service_wifi->update_network_tier($network_id, $network);
			$this->request->redirect('/wifi/admin');
		} else if (isset($_POST['ID']) && ! empty($_POST['ID'])) {
			$network = $this->service_wifi->get_network_tier_by_id($_POST['ID']);
			$network['SCPasswords_ID'] = $this->service_wifi->get_password($network['PublicName']);
			$network['hidden'] = array('ID' => $network['ID'], 'Edit' => '1');
			unset($network['password']);
			unset($network['ID']);
			
			$this->template->content = View::factory('pages/wifi/edit')
					->bind('network', $network);
			$this->template->title = 'Admin: '.$network['PublicName'].' ('.$network['PrivateName'].')';
		} else {
			$this->request->redirect('error/404/');
		}
	}
	
	public function action_create() {
		if (isset($_POST['Create'])) {
			unset($_POST['Create']);
			$values = $_POST;
			$password_id = $this->service_wifi->create_password($values);
			$values['SCPasswords_ID'] = $password_id;
			$network_tier_id = $this->service_wifi->create_network_tier($values);
			$this->request->redirect('/wifi/admin');
		}
		$columns = $this->service_wifi->get_network_tier_columns();
		if (empty($network)) {
			$network = $columns;
		} else {
			$network = array_merge($columns, $network);
			$network['SCPasswords_ID'] = $network['Hash'];
			unset($network['Hash']);
		}
		$network['hidden'] = array('Create' => '1');
		unset($network['ID']);
		unset($network['IsActive']);
		$this->template->content = View::factory('pages/wifi/create')
				->bind('network', $network);
		$this->template->title = 'Admin: Create Network';
	}
	
	public function after() {
		parent::after();
		
		$styles = array('/lib/css/blueprint/screen.css' => 'screen');
		$this->template->styles = array_merge($this->template->styles, $styles);
		$scripts = array(
			'/lib/js/pages/wifi/index.js'
		);
		$this->template->scripts = array_merge($this->template->scripts, $scripts);
	}
	
}
?>