<?php defined('SYSPATH') or die('No direct script access.');

class DAL_Features_Implementation implements DAL_Features_Interface {
	
	public function __construct() {}
	
	public function get_feature($url) {	
		$data = Jelly::select('iFrogz_Feature')
				->where('URL', '=', $url)		
				->limit(1)
				->execute()
				->as_array();
		
		$data['audio'] = $this->get_audio($data['ID']);

		return $data;
	}
	
	public function get_features($status, $type) {
		$features = Jelly::select('iFrogz_Feature')
				->where('IsActive', '=', $status)
				->where('FeatureTypes_ID', 'IN', DB::expr($type))
				->order_by('Sequence', 'ASC')
				->execute()
				->as_array();
					
		for ($i=0; $i < count($features); $i++) { 
			$features[$i]['image'] = $this->get_images($features[$i]['ID'], 1);
		}
		
		return $features;
	}
	
	public function get_features_data($type) {
		
		$data = Jelly::select('iFrogz_Feature')
				->where('IsActive', '=', '1')
				->where('FeatureTypes_ID', '=', $type)
				->order_by('Sequence', 'ASC')
				->execute()
				->as_array();

		return $data;
	}
	
	public function get_images($id, $type) {
		$data = Jelly::select('iFrogz_FeatureImage')
				->where('Feature', '=', $id)
				->where('FeatureImageType', '=', $type)
				->where('IsActive', '=', 1)
				->order_by('DateCreated', 'DESC')
				->execute()
				->as_array();

		return $data;
	}
	
	public function count_images($id, $type) {
		$data = Jelly::select('iFrogz_FeatureImage')
				->where('Feature', '=', $id)
				->where('FeatureImageType', '=', $type)
				->count();

		return $data;
	}
	
	public function get_audio($id) {
		$data = Jelly::select('iFrogz_FeatureAudio')
				->where('Feature', '=', $id)
				->order_by('DateCreated', 'DESC')
				->limit(1)
				->execute()
				->as_array();

		return $data;
	}
	
	public function get_current_employee() {
		$Session = Session::instance();
		$user = $Session->get('employee', '');
		
		$data = Jelly::select('admin_employee')
				->where('id', '=', $user['id'])
				->limit(1)
				->execute()
				->as_array();
				
		return $data;
	}
	
	public function save_feature($data) {
		$employee = $this->get_current_employee();

		Jelly::factory('iFrogz_Feature')
		     ->set(array(
				 'RFEmployee' => $employee['id'],
		         'Name' => $data['name'],
				 'URL' => $data['url'],
				 'PageTitle' => $data['title'],
				 'FeatureTypes_ID' => $data['type'],
				 'Body' => $data['body'],
				 'Website' => $data['website'],
				 'DateCreated' => $data['posted'],
				 'IsActive' => '0',
			     'Sequence' => '1',
		     ))->save();	
	}
	
	public function save_feature_edit($data) {
		$employee = $this->get_current_employee();
					
		$update = Jelly::update('iFrogz_Feature')
					->where('ID', '=', $data['id'])
					->set(array(
					 'RFEmployee' => $employee['id'],
			         'Name' => $data['name'],
					 'URL' => $data['url'],
					 'PageTitle' => $data['title'],
					 'FeatureTypes_ID' => $data['type'],
					 'Body' => $data['body'],
					 'Website' => $data['website'],
					 'DateCreated' => $data['posted'],
					 'IsActive' => $data['status'],
					 'Sequence' => $data['sequence'],
			     ))->execute();
	}
	
	public function save_image($data) {
		$employee = $this->get_current_employee();

		Jelly::factory('iFrogz_FeatureImage')
		     ->set(array(
				 'RFEmployee' => $employee['id'],
		         'Feature' => $data['id'],
				 'FeatureImageType' => $data['type'],
				 'FileName' => $data['file'],
				 'DateCreated' => $data['posted'],
				 'IsActive' => $data['active']
		     ))->save();
	}
	
	public function save_audio($data) {
		$employee = $this->get_current_employee();

		Jelly::factory('iFrogz_FeatureAudio')
		     ->set(array(
				 'RFEmployee' => $employee['id'],
		         'Feature' => $data['id'],
				 'FileName' => $data['file'],
				 'Name' => $data['name'],
				 'DateCreated' => $data['posted'],
				 'IsActive' => $data['active']
		     ))->save();
	}
	
	public function delete_image($image) {
		$employee = $this->get_current_employee();
					
		$update = Jelly::update('iFrogz_FeatureImage')
					->where('FileName', '=', $image['id'])
					->set(array(
					 'RFEmployee' => $employee['id'],
					 'DateCreated' => $image['posted'],
					 'IsActive' => $image['status']
			     ))->execute();
	}

}
?>