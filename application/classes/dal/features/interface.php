<?php defined('SYSPATH') or die('No direct script access.');

interface DAL_Features_Interface {

	public function get_feature($url);

	public function get_features($status, $type);

	public function get_features_data($type);

	public function get_images($id, $type);

	public function count_images($id, $type);

	public function get_audio($id);

	public function get_current_employee();

	public function save_feature($data);
	
	public function save_feature_edit($data);

	public function save_image($data);

	public function save_audio($data);

	public function delete_image($image);

}
?>