<?php defined('SYSPATH') OR die('No direct access allowed.');

class DAL_WiFi_Implementation implements DAL_WiFi_Interface {
	
	/**
	 * This function will get the password for the selected network tier.
	 * 
	 * @access public
	 * 
	 * @param string $network_tier
	 * @return string.
	 */
	public function get_password($network_tier) {
		$network_tier = ORM::factory('ifrogz_scnetworktier')
				->where('PublicName', '=', $network_tier)
				->find();
		
		$network_tier->password->type->as_array();
		$password = $network_tier->password->as_array();
		return array('password' => $password['Hash'], 'type' => $password['type']['Name']);
	}
	
	/**
	 * This function will record that a user did in fact retrieve one of the
	 * network passwords.
	 * 
	 * @access public
	 * 
	 * @param string $network_tier
	 * @param integer $employee_id
	 * @param string $ip_address
	 * @return void
	 */
	public function log_password_retrieval($network_tier, $employee_id, $ip_address) {
		$network_tier_id = $this->get_network_tier_id($network_tier);
		$network_tier_log = ORM::factory('ifrogz_scnetworktierlog');
		$network_tier_log->RFEmployee_ID = $employee_id;
		$network_tier_log->SCNetworkTiers_ID = $this->get_network_tier_id($network_tier);
		$network_tier_log->IPAddress = $ip_address;
		$network_tier_log->DateViewed = date('Y-m-d H:i:s');
		
		$network_tier_log->save();
		
		if ( ! $network_tier_log->saved()) {
			throw new Kohana_Exception('Could not log that the employeed accessed the password');
		}
	}
	
	/**
	 * This function will get the id of the network based on the public name
	 * for the that network.
	 * 
	 * @access private
	 * 
	 * @param string $network_tier
	 * @return integer
	 */
	private function get_network_tier_id($network_tier) {
		$record = ORM::factory('ifrogz_scnetworktier')
				->where('PublicName', '=', $network_tier)
				->find()
				->as_array();
		
		return $record['ID'];
	}
	
	/**
	 * This function will return all the networks that end users are allowed to
	 * see the password.
	 * 
	 * @access public
	 * 
	 * @return array
	 */
	public function get_all_viewable_networks() {
		$networks = ORM::factory('ifrogz_scnetworktier')
					->where('IsViewable', '=', 1)
					->find_all()
					->as_array();
		
		$all_networks = array();
		foreach ($networks as $network) {
			$all_networks[] = $network->as_array();
		}
		
		return $all_networks;
	}
	
	/**
	 * This function will return all the networks.
	 * 
	 * @access public
	 * 
	 * @return array
	 */
	public function get_all_networks() {
		$networks = ORM::factory('ifrogz_scnetworktier')
				->find_all()
				->as_array();
		
		$all_networks = array();
		foreach ($networks as $network) {
			$network = $network->as_array();
			$all_networks[] = $network;
		}
		return $all_networks;
	}
	
	/**
	 * This function will get a network tier by id.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @return array
	 */
	public function get_network_tier_by_id($id) {
		$network = ORM::factory('ifrogz_scnetworktier', $id);
		$network->password->type;
		$network = $network->as_array();
		return $network;
	}
	
	/**
	 * This function will take the columns provided and create a new password. 
	 * It will return the ID of the newly created record.
	 * 
	 * @access public
	 * 
	 * @param array $values
	 * @return integer
	 */
	public function create_password(array $values) {
		$password = ORM::factory('ifrogz_scpassword');
		$password->values($values);
		$password->save();
		
		if ($password->saved()) {
			return $password->ID;
		} else {
			throw new Exception('Failure to create password: '.$password->last_query());
		}
	}
	
	/**
	 * This function will take the columns provided and create a new network
	 * tier. It will return the ID of the newly created record.
	 * 
	 * @access public
	 * 
	 * @param array $values
	 * @return integer
	 */
	public function create_network_tier(array $values) {
		$network_tier = ORM::factory('ifrogz_scnetworktier');
		$network_tier->values($values);
		$network_tier->save();
		
		if ($network_tier->saved()) {
			return $network_tier->ID;
		} else {
			throw new Exception('Failure to create network tier: '.$network_tier->last_query());
		}
	}

	/**
	 * This function will take the id provided and update that record with the
	 * provided values.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @param array $values
	 * @return integer
	 */
	public function update_network_tier($id, array $values) {
		$network_tier = ORM::factory('ifrogz_scnetworktier', $id);
		$network_tier->values($values);
		$network_tier->save();
		
		if ($network_tier->saved()) {
			return TRUE;
		} else {
			throw new Exception('Failure to update network tier: '.$network_tier->last_query());
		}
	}
	
	/**
	 * This function will delete the record that corresponds to the id.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @return void
	 */
	public function delete_network_tier_by_id($id) {
		//$network_tier = ORM::factory('ifrogz_scnetworktier')->delete($id);
		$network_tier = ORM::factory('ifrogz_scnetworktier', $id)->as_array();
		$password_id = $network_tier['SCPasswords_ID'];
		ORM::factory('ifrogz_scnetworktier')->delete($id);
		ORM::factory('ifrogz_scpassword')->delete($password_id);
	}
	
	/**
	 * This function will grab the columns of the network tiers table.
	 *
	 * @access public
	 *
	 * @return array
	 */
	public function get_network_tier_columns() {
		return ORM::factory('ifrogz_scnetworktier')->as_array();
	}
	
}
?>
