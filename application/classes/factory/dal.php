<?php defined('SYSPATH') or die('No direct script access.');

/**
 * This class creates Dal objects for us.
 * 
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2011-04-11
 * @copyright (c) 2011, Reminderband, Inc. dba iFrogz
 * @package Factory
 */
class Factory_DAL {

	public static function create() {
		return new DAL_Cron_Implementation();
	}

}
?>