<?php defined('SYSPATH') or die('No direct script access.');

/**
 * This class creates DAL objects for us.
 * 
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2011-04-11
 * @copyright (c) 2011, Reminderband, Inc. dba iFrogz
 * @package Factory
 */
class Factory_Service {
	
	public static function create() {
		$dal_cron = Factory_DAL::create();
		return new Service_Cron_Implementation($dal_cron);
		
		//$class = 'Service_'.$type;
		//return new $class;
	}
	
}