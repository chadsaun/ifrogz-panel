<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
* This class enumerates common flags used.
*
* @author Matthew A. Mattson <matt.mattson@ifrogz.com>
* @version 0.0
* @copyright Copyright (c) 2010, Reminderband, Inc.
*
* @final
*/
final class Flags {

    const LOGGED_IN = 'virtualstore';

	/**
	* This function does nothing.
	*
	* @access private
	*/
	private function __construct() { }

}
?>
