<?php defined('SYSPATH') or die('No direct script access.');

/**
* Helper functions for Images
*/
class Helper_Image
{
	/**
	 * Retrieves the cache id - Also modifies and caches the image if needed
	 *
	 * @param	string		directory (relative path)
	 * @param	string		filename
	 * @param	array 		options
	 * 		Example of options array: array('width' => 200, 'height' => 200, 'bgcolor' => '#FFFFFF', 'master' => Image::NONE, 'id_prefix' => $product_id)
	 * 
	 * @return 	string		the cache id
	 **/
	public static function get_cache_id($dir, $file, $options = array())
	{
		$cache 		= Cache::instance('images');
		$directory 	= $_SERVER['DOCUMENT_ROOT'] . $dir . '/';
		
		$id_prefix = '';
		if (!empty($options['id_prefix'])) 
		{
			$id_prefix = $options['id_prefix'];
		}
		
		$get = Validate::factory($options)
				->rule('width', 'digit')
				->rule('height', 'digit')
				->rule('bgcolor', 'color');
		
		if ($get->check()) 
		{
			$get = $get->as_array();
			
			# Create the filename
			$path = $directory . $file;

			# Determine width and height
			$id_width = '';
			$id_height = '';
			$pre_width = NULL;
			$pre_height = NULL;

			if (isset($get['width'])) 
			{
				$id_width = $get['width'];
				$pre_width = $get['width'];
			}
			else
			{
				$id_width = 'x';
			}

			if (isset($get['height'])) 
			{
				$id_height = $get['height'];
				$pre_height = $get['height'];
			}
			else
			{
				$id_height = 'x';
			}

			$cache_id = $id_prefix . '_' . $file . '_' . $id_width . '_' . $id_height;

			# Check for cached image
			$encoded = $cache->get($cache_id);
			if (!$encoded) 
			{
				# Load the image
				$image = Image::factory($path, 'GD');

				# Resize the image
				if (isset($get['width']) || isset($get['height'])) 
				{
					if (!empty($options['master'])) 
					{
						$image->resize($pre_width, $pre_height, $options['master']);
					}
					else
					{
						$image->resize($pre_width, $pre_height);
					}
				}

				# Change background (if needed)
				if (isset($get->bgcolor)) 
				{
					$image->background($get->bgcolor);
				}

				# Get the rendered image
				ob_start();
				echo $image->render();
				$img = ob_get_clean();			

				# Encode
				$encoded = base64_encode($img);

				# Cache the image
				$cache->set($cache_id, $encoded);
			}

			return $cache_id;
		}
		else
		{
			throw new Kohana_Exception('The parameters did not validate when creating the image: ' . Kohana::debug($get->errors()));
		}
	}
	
}
