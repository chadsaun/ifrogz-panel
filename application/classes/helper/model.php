<?php defined('SYSPATH') or die('No direct script access.');


class Helper_Model
{
	
	public static function factory($model)
	{
		// Set class name
		$model = 'Helper_Model_'.ucfirst($model);

		return new $model();
	}
	
}
?>