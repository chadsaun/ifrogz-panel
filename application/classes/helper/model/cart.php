<?php defined('SYSPATH') or die('No direct script access.');

// TODO - Test this class and its functions.
class Helper_Model_Cart
{
	private $cart_orm = NULL;
	
	public function __construct()
	{
		$this->cart_orm = ORM::factory('ifrogz_cart');
	}
	
	public function set_carts_to_complete_by_order_id($order_id)
	{
		$this->set_carts_as_complete_by_order_id($order_id, TRUE);
	}
	
	public function set_carts_to_not_complete_by_order_id($order_id)
	{
		$this->set_carts_as_complete_by_order_id($order_id, FALSE);
	}
	
	private function set_carts_as_complete_by_order_id($order_id, $complete)
	{
		$status = ($complete) ? 1 : 0; 
		$this->cart_orm->where(Model_ORM_iFrogz_Cart::ORDER_ID, '=', $order_id);
		$this->cart_orm->{Model_ORM_iFrogz_Cart::COMPLETED} = $status;
		$this->cart_orm->save();
		
		return $this->cart_orm->saved();
	}
	
	public function get_cart_rows_by_order_id($order_id)
	{
		$cart_rows = $this->orm_carts
			->where(Model_ORM_iFrogz_Cart::ORDER_ID, '=', $order_id)
			->find_all()
			->as_array();
		
		return $cart_rows;
	}
	
}
