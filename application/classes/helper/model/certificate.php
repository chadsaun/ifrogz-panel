<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Model_Certificate {
	
	private $certificate_orm = NULL;
	
	public function __construct() {
		$this->certificate_orm = ORM::factory('ifrogz_certificate');
	}
	
	public function get_certificate_by_id($certificate_id) {
		$record = $this->certificate_orm
			->where(Model_ORM_iFrogz_Certificate::ID, '=', $certificate_id)
			->find()
			->as_array();
		
		return $record;
	}
	
	public function get_certificate_by_code($code) {
		$record = $this->certificate_orm
			->where(Model_ORM_iFrogz_Certificate::CODE, '=', $code)
			->find()
			->as_array();
		
		return $record;
	}
	
	public function get_balance_by_code($code) {
		$record = $this->get_certificate_by_code($code);
		return $this->format_balance($record[Model_ORM_iFrogz_Certificate::BALANCE], $record[Model_ORM_iFrogz_Certificate::ID]);
	}
	
	public function get_balance_by_id($certificate_id) {
		$record = $this->get_certificate_by_id($certificate_id);
		return $this->format_balance($record[Model_ORM_iFrogz_Certificate::BALANCE], $record[Model_ORM_iFrogz_Certificate::ID]);
	}
	
	private function format_balance($balance, $certificate_id) {
		return (empty($certificate_id)) ? 0 : sprintf('%.2f', round($balance, 2));
	}
	
	public function is_redeemable_by_code($code) {
		$record = $this->get_certificate_by_code($code);
		
		if (empty($record[Model_ORM_iFrogz_Certificate::ID])) {
			return FALSE;
		} else {
			$redemption_date = date('Y-m-d H:i:s');
			$certificate_redemption_date = date('Y-m-d H:i:s', $record[Model_ORM_iFrogz_Certificate::EXPIRATION_DATE]);
			return strcmp($certificate_redemption_date, $redemption_date) >= 0;
		}
	}
	
	public function get_email_by_code($code) {
		$record = $this->get_certificate_by_code($code);
		return (empty($record[Model_ORM_iFrogz_Certificate::ID])) ? '' : $record[Model_ORM_iFrogz_Certificate::EMAIL];
	}
	
	public function get_code_by_id($certificate_id) {
		$record = $this->get_certificate_by_id($certificate_id);
		return (empty($record[Model_ORM_iFrogz_Certificate::ID])) ? '' : $record[Model_ORM_iFrogz_Certificate::CODE];
	}
	
	public function get_id_by_code($code) {
		$record = $this->get_certificate_by_code($code);
		return (empty($record[Model_ORM_iFrogz_Certificate::ID])) ? '' : $record[Model_ORM_iFrogz_Certificate::ID];
	}
	
	public function set_balance_by_code($code, $balance) {
		$record = $this->get_certificate_by_code($code);
		
		$record[Model_ORM_iFrogz_Certificate::BALANCE] = $balance;
		$this->certificate_orm->values($record);
		$this->update_orm('Could not update the certificate with the given balance.', $update_values);
	}
	
	public function set_expiration_date_by_code($code, $date) {
		$record = $this->get_certificate_by_code($code);
		$update_values = array(Model_ORM_iFrogz_Certificate::EXPIRATION_DATE => $this->standardize_date($date));
		$this->update_orm('Could not update the certificate with the given expiration date.', $update_values);
	}
	
	public function decrement_balance_amount($amount, $code) {
		$record = $this->get_certificate_by_code($code);
		$current_balance = $record[Model_ORM_iFrogz_Certificate::BALANCE];
		$current_balance = sprintf('%.2f', round($current_balance, 2));
		if ($amount > $current_balance) {
			Kohana::$log->add(Kohana::ERROR, 'Could not update certificate balance from: '.$current_balance.' to '.$amount);
			throw new Kohana_Exception('The balance could not be updated.');
		}
		$update_values = array(Model_ORM_iFrogz_Certificate::BALANCE => $current_balance - $amount);
		$this->update_orm('Could not update the certificate with the given balance.', $update_values);
	}
	
	private function update_orm($error_message, $values) {
		$this->certificate_orm = ORM::factory('ifrogz_certificate', $this->certificate_orm->{Model_ORM_iFrogz_Certificate::ID});
		$this->certificate_orm->values($values);
		if ( ! $this->certificate_orm->check()) {
			throw new Kohana_Exception($error_message);
		} else {
			$this->certificate_orm->save();
		}
	}
	
	// This should return a unix number like 1234567890
	private function standardize_date($date) {
		$regex = '/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/'; // Looking for a Y-m-d H:i:s format date.
		
		if (empty($date) OR preg_match($regex, $date)) {
			$date = strtotime($date);
		}
		return $date;
	}
	
}