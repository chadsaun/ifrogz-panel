<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Model_Country
{
	private $country_orm = NULL;
	
	public function __construct()
	{
		$this->country_orm = ORM::factory('ifrogz_country');
	}
	
	public function get_country_record_by_id($country_id)
	{
		$record = $this->country_orm
			->where(Model_ORM_iFrogz_Country::ID, '=', $country_id)
			->find()
			->as_array();
		
		return $record;
	}
	
	public function get_country_abbreviation_by_country_name($country_name)
	{
		$record = $this->country_orm
			->where(Model_ORM_iFrogz_Country::NAME, '=', $country_name)
			->find()
			->as_array();
		
		return $record[Model_ORM_iFrogz_Country::COUNTRY_CODE];
	}
}