<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Model_Location {

	private $location_orm = NULL;
	
	public function __construct() {
		$this->location_orm = ORM::factory('ifrogz_location');
	}
	
	public function set_new_location($employee_id = 0) {
		$order = Session::instance()->get('order')->as_array();
		
		$location_name = Helper_Model::factory('orderstatus')->get_order_status_record_by_id($order[Model_ORM_iFrogz_Order::STATUS_ID]);
		$location_name = $location_name[Model_ORM_iFrogz_OrderStatus::PRIVATE_STATUS];
		
		$this->location_orm->{Model_ORM_iFrogz_Location::ORDER_ID} = $order[Model_ORM_iFrogz_Order::ID];
		$this->location_orm->{Model_ORM_iFrogz_Location::EMPLOYEE_ID} = $employee_id;
		$this->location_orm->{Model_ORM_iFrogz_Location::ORDER_STATUS_ID} = $order[Model_ORM_iFrogz_Order::STATUS_ID];
		$this->location_orm->{Model_ORM_iFrogz_Location::ORDER_STATUS_PRIVATE_STATUS} = $location_name;
		$this->location_orm->{Model_ORM_iFrogz_Location::LOCATION_TYPE} = 'Automatic';
		$this->location_orm->save();
		
		if ( ! $this->location_orm->saved()) {
			throw new Kohana_Exception('Location creation failed. Query: ' . $location->last_query());
		}
	}
}