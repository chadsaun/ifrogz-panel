<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Model_Order {

	private $order_orm = NULL;
	
	public function __construct()
	{
		$this->order_orm = ORM::factory('ifrogz_order');
	}
	
	public function get_order_record_by_id($order_id)
	{
		$record = $this->order_orm
			->where(Model_ORM_iFrogz_Order::ID, '=', $order_id)
			->find()
			->as_array();
		
		return $record;
	}
	
	public function get_order_record_by_google_order_number($google_order_number)
	{
		$google_checkout = 20;
		
		$record = $this->order_orm
			->where(Model_ORM_iFrogz_Order::AUTHENTICATION_NUMBER, '=', $google_order_number)
			->and_where(Model_ORM_iFrogz_Order::PAYMENT_PROVIDER, '=', $google_checkout)
			->find()
			->as_array();
		
		return $record;
	}
	
	public function get_order_id_by_google_order_number($google_order_number)
	{
		$record = $this->get_order_record_by_google_order_number($google_order_number);
		
		return $record[Model_ORM_iFrogz_Order::ID];
	}
	
	public function get_order_total_amount_by_id($order_id)
	{
		$record = $this->get_order_record_by_id($order_id);
		
		$order_total_amount = $record[Model_ORM_iFrogz_Order::SHIPPING] + $record[Model_ORM_iFrogz_Order::STATE_TAX];
		$order_total_amount += $record[Model_ORM_iFrogz_Order::COUNTRY_TAX] + $record[Model_ORM_iFrogz_Order::TOTAL];
		$order_total_amount += $record[Model_ORM_iFrogz_Order::HANDLING] - $record[Model_ORM_iFrogz_Order::DISCOUNT];
		
		return $order_total_amount;
	}
	
	public function get_order_status_by_id($order_id)
	{
		$record = $this->get_order_record_by_id($order_id);
		
		return $record[Model_ORM_iFrogz_Order::STATUS_ID];
	}
	
	public function set_order_status_by_id($order_id, $order_status)
	{
		$this->order_orm->{Model_ORM_iFrogz_Order::STATUS_ID} = $order_status;
		$this->order_orm->{Model_ORM_iFrogz_Order::STATUS_DATE} = date('Y-m-d H:i:s');
		$this->order_orm->where(Model_ORM_iFrogz_Order::ID, '=', $order_id);
		$this->order_orm->save();
		
		return $this->order_orm->saved();
	}
	
	/*
	This code might be added later. There are some issues as to whether or not it will be valuable.
	
	public function create_order()
	{
		if ($this->is_ready_to_create_order())
		{
			throw new Kohana_Exception('You order does not have items associated with it or you are missing your order information.');
		}
		
		
	}
	
	private function is_ready_to_create_order()
	{
		$session = Session::instance();
		$cart = Cart::instance();
		$order = $session->get('order')->as_array();
		
		$valid_order = ( ! empty($order[Model_ORM_iFrogz_Order::BILLING_NAME]) OR ! empty($order[Model_ORM_iFrogz_Order::SHIPPING_NAME]));
		$valid_cart = count($cart->contents()) > 0;
		
		return ($valid_order AND $valid_cart);
	}
	*/
}
