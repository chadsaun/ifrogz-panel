<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Model_OrderStatus {

	private $order_status_orm = NULL;
	
	public function __construct()
	{
		$this->order_status_orm = ORM::factory('ifrogz_orderstatus');
	}
	
	public function get_order_status_record_by_id($order_status_id)
	{
		$record = $this->order_status_orm
			->where(Model_ORM_iFrogz_OrderStatus::ID, '=', $order_status_id)
			->find()
			->as_array();
		
		return $record;
	}
}
?>