<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Model_Transaction
{
	private $transaction_orm = NULL;
	
	public function __construct()
	{
		$this->transaction_orm = ORM::factory('ifrogz_transaction');
	}
	
	public function add_new_transaction_record($transaction_number, $employee_id = 0, $note = '', $authentication_status = '', $authentication_raw_response = '', $cc_exp_date = '')
	{
		$order = Session::instance()->get('order')->as_array();
		$cart = Session::instance()->get('cart');
		
		# Record transaction in transactions table
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::NUMBER} = $transaction_number;
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::ORDER_ID} = $order[Model_ORM_iFrogz_Order::ID];
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::AMOUNT} = $cart->total();
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::TYPE} = $this->get_transaction_type($order[Model_ORM_iFrogz_Order::PAYMENT_PROVIDER]);
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::NOTE} = $note;
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::AUTHORIZE_NET_STATUS} = $authentication_status;
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::AUTHORIZE_NET_RAW} = $authentication_raw_response;
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::CREDIT_CARD_NUMBER} = Encrypt::instance()->encode($order[Model_ORM_iFrogz_Order::C_NUMBER]);
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::CREDIT_CARD_TYPE} = $order[Model_ORM_iFrogz_Order::CREDIT_CARD_TYPE];
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::CREDIT_CARD_EXPIRATION_DATE} = $cc_exp_date;
		$this->transaction_orm->{Model_ORM_iFrogz_Transaction::EMPLOYEE_ID} = $employee_id;
		$this->transaction_orm->save();
		
		if ( ! $this->transaction_orm->saved())
		{
			throw new Kohana_Exception('Transaction creation failed. Query: '.$this->transaction_orm->last_query());
		}
	}
	
	private function get_transaction_type($payment_provider)
	{
		$type = '';
		switch ($payment_provider)
		{
			case 19:
				$type = 'PayPal';
			break;
			case 20:
				$type = 'Google Checkout';
			break;
			default:
				$type = 'auth_capture';
			break;
		}
		
		return $type;
	}
}