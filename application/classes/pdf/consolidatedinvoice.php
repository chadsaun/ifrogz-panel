<?php defined('SYSPATH') or die('No direct script access.');

/**
* PDF for creating a consolidated invoice
*/
class PDF_ConsolidatedInvoice extends PDF {
	
	public $accept_page_break = TRUE;
	public $customer = array();
	public $total_amount = 0.00;
	public $sos = array();
	
	public function Header() {
		// Font Sizes (points)
		$fs_large = 10;
		$fs_normal = 8;
		$fs_small = 6;
		
		$width_left_col = 170;
		$width_right_col = 25;
		$border = 0;
		$cell_height = 5;
		
		$this->Cell($width_left_col, $cell_height, 'NEW FORTUNE LOGISTICS LTD.', $border, 0, 'C');
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell($width_right_col, $cell_height, 'DATE: ' . date('d-M-y'), $border, 1, 'R');
		$this->Cell($width_left_col, $cell_height, '1107, 11/F TOWER 3 ENTERPRISES SQUARE1, 9 SHEUNG YUET ROAD KOWLOON BAY HONG KONG', $border, 0, 'C');
		$this->SetFont('Arial', 'U', $fs_normal);
		$this->Cell($width_right_col, $cell_height, 'Page ' . $this->PageNo() . ' of {nb}', $border, 1, 'R');
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell($width_left_col, $cell_height, 'TEL: (852) 24288868', $border, 1, 'C');
		$this->SetFont('Arial', 'U', $fs_normal);
		$this->Cell($width_left_col, $cell_height, 'INVOICE NO. : ' . implode('-', $this->sos), $border, 1, 'C');
		
		$this->Ln();
		
		$cust_width = 100;
		
		// Customer
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell(40, $cell_height, 'on account of Messers.', 0, 0, 'R');
		$this->Cell($cust_width, $cell_height, $this->customer['shiptoname'], 0, 1);
		for ($i = 1; $i < 5; $i++) {
			if (isset($this->customer['shiptoaddress' . $i]) && ! empty($this->customer['shiptoaddress' . $i])) {
				//echo Debug::vars($this->customer['shiptoaddress' . $i]);
				$this->Cell(40, $cell_height, '', 0, 0, 'R');
				$this->Cell($cust_width, $cell_height, $this->customer['shiptoaddress' . $i], 0, 1);
			}
		}
		$this->Cell(40, $cell_height, '', 0, 0, 'R');
		$this->Cell($cust_width, $cell_height, $this->customer['shiptocity'] . ', ' . $this->customer['shiptostate'] . ' ' . $this->customer['shiptozip'], 0, 1);
		$this->Cell(40, $cell_height, '', 0, 0, 'R');
		$this->Cell($cust_width, $cell_height, $this->customer['shiptocountry'], 0, 1);
		
		$this->Ln(1);
		
		// Table Header
		$this->SetFont('Arial', '', $fs_normal);
		$header = array('     MARKS & NOS:', 'DESCRIPTION OF GOODS', 'UNIT PRICE', 'AMOUNT');
		$w = array(45, 70, 60, 20);
		for ($i=0; $i < count($header); $i++) { 
			$this->Cell($w[$i], 4, $header[$i], 'TB', 0, 'L');
		}
		$this->Ln();
	}
	
	public function add_line($item) {
		// Column widths
		$w = array(45, 70, 60, 20);
		
		$unit_price = $item['UNITPRICE'];
		if (in_array($item['CARRIERID'], array(20, 33, 34, 35))) {
			$unit_price = $item['ATCOSTVALUE'];
		} else {
			if ($item['ORDERTYPE'] == 'TO') {
				$unit_price = $item['ATCOSTVALUE'];
			} else {
				// Is it a sample order?
				//if ($item['UNITPRICE'] <= 0) {
					$unit_price = $item['CUSTOMPRICE'];
				//}
			}
		}
		
		// First Line
		$this->Cell($w[0], 4, '', 0, 0, 'L');
		$this->Cell(35, 4, 'QUANTITY:', 0, 0, 'L');
		$this->Cell(10, 4, $item['TOTALQTY'] . ' PCS', 0, 0, 'R');
		$this->Cell(18, 4, '', 0, 0, 'L');
		$this->Cell($w[2], 4, 'USD         ' . money_format('%.2n', $unit_price) . ' / PCS', 0, 0, 'L');
		$this->Cell(10, 4, 'USD', 0, 0, 'L');
		$this->Cell(17, 4, money_format('%.2n', ($item['TOTALQTY'] * $unit_price)), 0, 1, 'R');
		
		$this->total_amount += $unit_price * $item['TOTALQTY'];
		
		// Second Line
		$this->Cell($w[0], 4, 'ORDER NO. : ' . $item['ORDERNUM'], 0, 0, 'L');
		$this->Cell(25, 4, 'ITEM NO. :', 0, 0, 'L');
		$this->Cell(40, 4, $item['PRODUCTNUMBER'], 0, 0, 'L');
		$this->Cell(85, 4, 'COUNTRY OF ORIGIN : CHINA', 0, 1, 'C');
		
		// Third Line
		$this->Cell($w[0], 4, 'P.O. NO. : ' . $item['CUSTOMERPO'], 0, 0, 'L');
		$this->Cell(150, 4, 'ITEM NAME:  ' . $item['PRODUCTDESCRIPTION'], 0, 1, 'L');
		
		// Fourth Line
		$this->Cell($w[0], 4, 'C/NO. :', 0, 0, 'L');
		$this->Cell(87, 4, 'UNDER P.O. NO. : ' . $item['CUSTOMERPO'], 0, 0, 'L');
		$this->Cell(63, 4, 'SALES ORDER NO. :  ' . $item['ORDERNUM'], 0, 1, 'L');
		
		// Insert a small line break
		$this->Ln(1);
		
		$this->dashed_line();
		
		// Insert a small line break
		$this->Ln(1);
	}
	
	public function dashed_line($width = 205, $left_margin = 10, $line_length = 2, $line_gap = 2) {
		// Draw dashed line
		$y = $this->GetY();
		for ($i = $left_margin; $i < $width; $i += ($line_gap + $line_length)) {
			$this->Line($i, $y, $i + $line_length, $y);
		}
	}
	
}
