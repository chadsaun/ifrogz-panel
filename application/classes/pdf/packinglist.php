<?php defined('SYSPATH') or die('No direct script access.');

include_once('Image/Barcode.php');

/**
* PDF for creating simulated packing lists
*/
class PDF_PackingList extends PDF {
	
	public $accept_page_break = FALSE;
	public $import_key = NULL;
	public $customer = array('shiptoname' => '', 'shiptoaddress1' => '', 'shiptoaddress2' => '', 'shiptoaddress3' => '', 'shiptoaddress4' => '', 'shiptocity' => '', 'shiptostate' => '', 'shiptozip' => '', 'shiptocountry' => '');
	public $items = array();
	public $is_fedex = FALSE;
	public $is_consolidated = FALSE;
	
	public function Header() {
		// Font Sizes (points)
		$fs_large = 10;
		$fs_normal = 8;
		$fs_small = 6;
		
		$width_left_col = 224;
		$width_right_col = 35;
		$border = 0;
		$cell_height = 5;

		$this->SetFont('Arial', '', $fs_normal);
		
		$this->Cell($width_left_col, $cell_height, 'NEW FORTUNE LOGISTICS LTD.', $border, 0, 'C');
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell($width_right_col, $cell_height, 'DATE: ' . Date::formatted_time('now', 'd-M-y', 'Asia/Hong_Kong'), $border, 1, 'R');
		$this->Cell($width_left_col, $cell_height, '1107, 11/F TOWER 3 ENTERPRISE SQUARE 1, 9 SHEUNG YUET ROAD KOWLOON BAY HONG KONG', $border, 0, 'C');
		$this->SetFont('Arial', 'U', $fs_normal);
		$this->Cell($width_right_col, $cell_height, 'Page ' . $this->PageNo() . ' of {nb}', $border, 1, 'R');
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell($width_left_col, $cell_height, 'TEL: (852) 24288868', $border, 0, 'C');
		$this->SetFont('Arial', 'U', $fs_normal);

		// Insert Barcode
		$username = Kohana::$config->load('authorization.' . Kohana::$environment . '.username');
		$password = Kohana::$config->load('authorization.' . Kohana::$environment . '.password');

		$this->Cell($width_right_col, $cell_height, $this->Image('http://' . $username . ':' . $password . '@' . $_SERVER['HTTP_HOST'] . '/barcode/code39/' . $this->import_key, $this->GetX(), $this->GetY(), $width_right_col, 0, 'PNG'), $border, 1, 'R');

		$this->Cell($width_left_col, $cell_height, 'ORIGINAL PACKING LIST', $border, 1, 'C');
		
		$this->Ln();
		
		// Customer
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell(40, $cell_height, 'on account of Messers.', 0, 0, 'R');
		$this->Cell(200, $cell_height, $this->customer['shiptoname'], 0, 1);
		
		for ($i = 1; $i < 5; $i++) {
			if (isset($this->customer['shiptoaddress' . $i]) && ! empty($this->customer['shiptoaddress' . $i])) {
				//echo Debug::vars($this->customer['shiptoaddress' . $i]);
				$this->Cell(40, $cell_height, '', 0, 0, 'R');
				$this->Cell(200, $cell_height, $this->customer['shiptoaddress' . $i], 0, 1);
			}
		}
		$this->Cell(40, $cell_height, '', 0, 0, 'R');
		$this->Cell(200, $cell_height, $this->customer['shiptocity'] . ', ' . $this->customer['shiptostate'] . ' ' . $this->customer['shiptozip'], 0, 1);
		$this->Cell(40, $cell_height, '', 0, 0, 'R');
		$this->Cell(200, $cell_height, $this->customer['shiptocountry'], 0, 1);
		
		$this->Ln(1);
		
		// Table Header
		$this->SetFont('Arial', '', $fs_normal);
		$header = array('MARKS & NOS:', 'NO OF PACKAGE', 'DESCRIPTION OF GOODS', 'QTY/PACKAGE', 'G.W./CTN', 'MEASUREMENT/CTN');
		$w = array(28, 28, 94, 33, 33, 44);
		for ($i=0; $i < count($header); $i++) { 
			$this->Cell($w[$i], 4, $header[$i], 'TB', 0, 'C');
		}
		$this->Ln();
	}
	
	public function body() {
		
		// Font Sizes (points)
		$fs_large = 10;
		$fs_normal = 8;
		$fs_small = 6;
		
		// Header
		$width_left_col = 224;
		$width_right_col = 35;
		$border = 0;
		$cell_height = 5;
		
		$w = array(28, 28, 94, 33, 33, 44);
		
		//echo Debug::vars($this->items); exit();
		
		// Table Data
		$lines_per_page = 10;
		$total_items = count($this->items);
		//echo Debug::vars($total_items); exit();
		$o = 1;
		$total_cbm = 0;
		$total_packages = 0;
		$total_weight = 0;
		$k = 1;
		foreach ($this->items as $order_num => $order) {
			if ($this->is_fedex) {
				$total_cbm = 0;
				$total_packages = 0;
				$total_weight = 0;
				$k = 1;
			}
			
			foreach ($order as $shipment) {
				foreach ($shipment as $item) {
					// Set Customer
					if ($k == 1 && $this->is_fedex) {
						$this->set_customer($item);
					}
					if ($k % $lines_per_page == 0 && $k != 1) {
						//echo Debug::vars('Top Break');
						$this->AddPage();
					}
				
					$so_num = $item['ORDERNUM'];

					$num_of_cartons = floor($item['QTY'] / $item['MASTERQTY']);
					$remaining = 0;
					$total_pieces = $item['QTY'];
					if ($num_of_cartons >= 1) {
						$remaining = $item['QTY'] % $item['MASTERQTY'];
						$total_pieces = $item['MASTERQTY'] * $num_of_cartons;
					}
					$num_of_cartons = max($num_of_cartons, 1);
					$total_packages += $num_of_cartons;
				
					// Parse Size UOM
					$item['SIZEUOM'] = $this->parse_uom($item['SIZEUOM']);
					
					// Convert size to CM (centimeters)
					$len = $item['LEN'];
					$width = $item['WIDTH'];
					$height = $item['HEIGHT'];
					switch ($item['SIZEUOM']) {
						case 'ft':
							$len = round(UOM::convert($len, UOM::FEET, UOM::CENTIMETERS), 1);
							$width = round(UOM::convert($width, UOM::FEET, UOM::CENTIMETERS), 1);
							$height = round(UOM::convert($height, UOM::FEET, UOM::CENTIMETERS), 1);
							break;
				
						case 'in':
							$len = round(UOM::convert($len, UOM::INCHES, UOM::CENTIMETERS), 1);
							$width = round(UOM::convert($width, UOM::INCHES, UOM::CENTIMETERS), 1);
							$height = round(UOM::convert($height, UOM::INCHES, UOM::CENTIMETERS), 1);
							break;

						case 'm':
							$len = round(UOM::convert($len, UOM::METERS, UOM::CENTIMETERS), 1);
							$width = round(UOM::convert($width, UOM::METERS, UOM::CENTIMETERS), 1);
							$height = round(UOM::convert($height, UOM::METERS, UOM::CENTIMETERS), 1);
							break;

						case 'mm':
							$len = round(UOM::convert($len, UOM::MILLIMETERS, UOM::CENTIMETERS), 1);
							$width = round(UOM::convert($width, UOM::MILLIMETERS, UOM::CENTIMETERS), 1);
							$height = round(UOM::convert($height, UOM::MILLIMETERS, UOM::CENTIMETERS), 1);
							break;

						case 'km':
							$len = round(UOM::convert($len, UOM::KILOMETERS, UOM::CENTIMETERS), 1);
							$width = round(UOM::convert($width, UOM::KILOMETERS, UOM::CENTIMETERS), 1);
							$height = round(UOM::convert($height, UOM::KILOMETERS, UOM::CENTIMETERS), 1);
							break;
				
						default:
					
							break;
					}
			
					// Convert weight to KGS (kilograms)
					$weight = $item['WEIGHT'];
					switch ($item['WEIGHTUOM']) {
						case 'lbs':
							$weight = UOM::convert($weight, UOM::POUNDS, UOM::KILOGRAMS);
							break;
				
						case 'g':
							$weight = UOM::convert($weight, UOM::GRAMS, UOM::KILOGRAMS);
							break;
				
						case 'mg':
							$weight = UOM::convert($weight, UOM::MILLIGRAMS, UOM::KILOGRAMS);
							break;
				
						default:
					
							break;
					}
			
					$weight_per_carton = round(($weight * $item['MASTERQTY']), 2);
					$line_item_weight = round(($weight * $item['QTY']), 2);
					$total_weight += $line_item_weight;
					//echo Debug::vars($total_weight);
					
					$cbm = round(((($len/100) * ($width/100) * ($height/100)) * $num_of_cartons), 6);
					//$cbm = round(((($len) * ($width) * ($height)) * $num_of_cartons) / 1000000, 6);
					//echo 'len/100: ' . ($len/100) . ' width/100: ' . ($width/100) . ' height/100: ' . ($height/100) . ' * cartons: ' . $num_of_cartons . ' = ' . $cbm . '<br />';
					$total_cbm += $cbm;
					//echo 'Total CBM: ' . $total_cbm . '<br />';
			
					$po_num = $item['CUSTOMERPO'];
			
					// First Line
					$this->Cell($w[0], 4, 'ORDER NO. : ' . $so_num, 0, 0, 'L');
					$this->Cell(($w[1] + $w[2]), 4, '            ' . $item['PRODUCTDESCRIPTION'], 0, 0, 'L');
					$this->Cell($w[3], 4, $item['MASTERQTY'] . ' PCS/CTN', 0, 0, 'R');
					$this->Cell($w[4], 4, $weight_per_carton . ' KGS', 0, 0, 'R');
					$this->Cell($w[5], 4, sprintf('%.1f', $len) . '   X   ' . sprintf('%.1f', $width) . '   X   ' . sprintf('%.1f', $height) . ' CM', 0, 1, 'R');
			
					// Second Line (Auto-adjust font size to make customs description fit)
					$border = 0;
					$second_line_height = 4;
					$item_description_width = ($w[2] / 2) + 12;
					$item_description_font_size = $fs_normal;

					$w_description = $this->GetStringWidth($item['CUSTOMSDESCRIPTION']);

					if ($w_description > $item_description_width) {
						for ($i = ($fs_normal - 1); $i > 0; $i--) {
							$this->SetFontSize($i);
							if ($this->GetStringWidth($item['CUSTOMSDESCRIPTION']) < $item_description_width) {
								$item_description_font_size = $i;
								break;
							}
						}
					}

					$this->SetFontSize($fs_normal);

					$this->Cell($w[0], $second_line_height, 'P.O. NO. : ' . $po_num, $border, 0, 'L');
					$this->Cell(($w[1] - 14), $second_line_height, '', $border, 0);
					$this->SetFontSize($item_description_font_size);
					$this->Cell($item_description_width, $second_line_height, $item['CUSTOMSDESCRIPTION'], $border, 0, 'L');
					$this->SetFontSize($fs_normal);
					$this->Cell(($w[2] / 2) - 8, $second_line_height, 'ITEM NO. ' . $item['PRODUCTNUMBER'], $border, 0, 'L');
					$this->Cell(($w[3] + $w[4] + $w[5]), $second_line_height, 'COUNTRY OF ORIGIN: CHINA', $border, 1, 'C');
			
					// Third Line
					$this->Cell($w[0], 4, 'C/NO. :', 0, 0, 'L');
					$this->Cell($w[1], 4, $num_of_cartons . ' CARTONS', 0, 0, 'R');
					$this->Cell(52, 4, '    UNDER P.O. NO. : ' . $po_num, 0, 0, 'L');
					if ($item['ORDERTYPE'] == 'TO') {
						$this->Cell(20, 4, 'UNDER T/O# NO. : ', 0, 0, 'L');
					} else {
						$this->Cell(20, 4, 'UNDER S/O# NO. : ', 0, 0, 'L');
					}
					$this->Cell(22, 4, $so_num, 0, 0, 'R');
					$this->Cell(15, 4, 'TOTAL QTY.: ', 0, 0, 'L');
					$this->Cell(22, 4, $total_pieces . ' PCS', 0, 0, 'R');
					$this->Cell(15, 4, 'TOTAL G.W.:', 0, 0, 'L');
					$this->Cell(25, 4, $line_item_weight . ' KGS', 0, 0, 'R');
					$this->Cell(20, 4, 'TOTAL CBM.', 0, 0, 'L');
					$this->Cell(14, 4, $cbm, 0, 1, 'R');
			
					$this->SetDrawColor(153, 153, 153);
			
					// Draw dashed line
					$dash_width = 270;
					$left_margin = 10;
					$line_length = 2;
					$line_gap = 2;
					$y = $this->GetY();
					for ($i = $left_margin; $i < $dash_width; $i += ($line_gap + $line_length)) {
						$this->Line($i, $y, $i + $line_length, $y);
					}
			
					// Insert a small line break
					$this->Ln(1);
			
					$k++;
			
					if ($remaining > 0) {
						// Check for the need of a page break
						if ($k % $lines_per_page == 0) {
							//echo Debug::vars('Middle Break');
							$this->AddPage();
						}
						
						$weight_per_carton = round(($weight * $remaining), 2);
						$line_item_weight = round(($weight * $remaining), 2);
						$num_of_cartons = 1;
						$total_cbm += ($cbm * $num_of_cartons);
						$cbm = round(((($len/100) * ($width/100) * ($height/100)) * 1), 6);
						$total_pieces = $item['MASTERQTY'] * $num_of_cartons;
						$total_packages += 1;

						// First Line
						$this->Cell($w[0], 4, 'ORDER NO. : ' . $so_num, 0, 0, 'L');
						$this->Cell(($w[1] + $w[2]), 4, '            ' . $item['PRODUCTDESCRIPTION'], 0, 0, 'L');
						$this->Cell($w[3], 4, $item['MASTERQTY'] . ' PCS/CTN', 0, 0, 'R');
						$this->Cell($w[4], 4, $weight_per_carton . ' KGS', 0, 0, 'R');
						$this->Cell($w[5], 4, sprintf('%.1f', $len) . '   X   ' . sprintf('%.1f', $width) . '   X   ' . sprintf('%.1f', $height) . ' CM', 0, 1, 'R');

						// Second Line
						$this->Cell($w[0], 4, 'P.O. NO. : ' . $po_num, 0, 0, 'L');
						$this->Cell($w[1], 4, '', 0, 0);
						$this->Cell(($w[2] / 2), 4, $item['CUSTOMSDESCRIPTION'], 0, 0, 'L');
						$this->Cell(($w[2] / 2), 4, 'ITEM NO. ' . $item['PRODUCTNUMBER'], 0, 0, 'L');
						$this->Cell(($w[3] + $w[4] + $w[5]), 4, 'COUNTRY OF ORIGIN: CHINA', 0, 1, 'C');

						// Third Line
						$this->Cell($w[0], 4, 'C/NO. :', 0, 0, 'L');
						$this->Cell($w[1], 4, $num_of_cartons . ' CARTONS', 0, 0, 'R');
						$this->Cell(52, 4, '    UNDER P.O. NO. : ' . $po_num, 0, 0, 'L');
						$this->Cell(20, 4, 'UNDER S/O# NO. : ', 0, 0, 'L');
						$this->Cell(22, 4, $so_num, 0, 0, 'R');
						$this->Cell(15, 4, 'TOTAL QTY.: ', 0, 0, 'L');
						$this->Cell(22, 4, $remaining . ' PCS', 0, 0, 'R');
						$this->Cell(15, 4, 'TOTAL G.W.:', 0, 0, 'L');
						$this->Cell(25, 4, $line_item_weight . ' KGS', 0, 0, 'R');
						$this->Cell(20, 4, 'TOTAL CBM.', 0, 0, 'L');
						$this->Cell(14, 4, $cbm, 0, 1, 'R');

						$this->SetDrawColor(153, 153, 153);

						// Draw dashed line
						$width = 270;
						$left_margin = 10;
						$line_length = 2;
						$line_gap = 2;
						$y = $this->GetY();
						for ($i = $left_margin; $i < $width; $i += ($line_gap + $line_length)) {
							$this->Line($i, $y, $i + $line_length, $y);
						}

						// Insert a small line break
						$this->Ln(1);

						$k++;
					}
					$last_order_num = $item['ORDERNUM'];
				}
			}
			
			$this->Ln(2);
			
			if ($this->is_fedex) {
				// Totals
				$this->Cell(50, 4, 'Total Packages:', 0, 0, 'L');
				$this->Cell(50, 4, $total_packages . ' CARTONS', 0, 1, 'L');
				$this->Cell(50, 4, 'Total Measurement:', 0, 0, 'L');
				$ref_y = $this->GetY();
				$this->Cell(50, 4, round($total_cbm, 2) . ' CBM', 0, 1, 'L');
				$this->Cell(50, 4, 'Total Gross Weight:', 0, 0, 'L');
				$this->Cell(50, 4, $total_weight . ' KGS', 0, 1, 'L');
				$this->Cell(50, 4, 'Total Volume Weight (L*W*H/6000):', 0, 0, 'L');
				$this->Cell(50, 4, round(($total_cbm * 1000000) / 6000, 2) . ' KGS', 0, 1, 'L');
				$this->Cell(50, 4, 'Total Volume Weight (L*W*H/5000):', 0, 0, 'L');
				$this->Cell(50, 4, round(($total_cbm * 1000000) / 5000, 2) . ' KGS', 0, 1, 'L');

				if ( ! empty($ref)) {
					// Ref #
					$this->SetXY(120, $ref_y);
					$this->SetFont('Arial', '', 11);
					$this->Write(8, 'ref# ' . $ref);
					$this->SetFont('Arial', '', 8);	
				}
			
				// Check to see if this is the last one
				if ($o < $total_items) {
					//echo Debug::vars('Total Items');
					$this->AddPage();
				}
			
				$o++;
			}
			//echo Debug::vars('here');
			
			//exit();
		}
		
		if ( ! $this->is_fedex) {
			// Totals
			$this->Cell(50, 4, 'Total Packages:', 0, 0, 'L');
			$this->Cell(50, 4, $total_packages . ' CARTONS', 0, 1, 'L');
			$this->Cell(50, 4, 'Total Measurement:', 0, 0, 'L');
			$ref_y = $this->GetY();
			$this->Cell(50, 4, round($total_cbm, 2) . ' CBM', 0, 1, 'L');
			$this->Cell(50, 4, 'Total Gross Weight:', 0, 0, 'L');
			$this->Cell(50, 4, $total_weight . ' KGS', 0, 1, 'L');
			$this->Cell(50, 4, 'Total Volume Weight (L*W*H/6000):', 0, 0, 'L');
			$this->Cell(50, 4, round(($total_cbm * 1000000) / 6000, 2) . ' KGS', 0, 1, 'L');
			$this->Cell(50, 4, 'Total Volume Weight (L*W*H/5000):', 0, 0, 'L');
			$this->Cell(50, 4, round(($total_cbm * 1000000) / 5000, 2) . ' KGS', 0, 1, 'L');

			if ( ! empty($ref)) {
				// Ref #
				$this->SetXY(120, $ref_y);
				$this->SetFont('Arial', '', 11);
				$this->Write(8, 'ref# ' . $ref);
				$this->SetFont('Arial', '', 8);	
			}
		}
		
		//exit();
	}

	public function set_customer($item) {
		$this->customer['shiptoname'] = $item['SHIPTONAME'];
		$shiptoaddress = preg_split('/\R/', $item['SHIPTOADDRESS']);
		foreach ($shiptoaddress as $key => $value) {
			$this->customer['shiptoaddress' . ($key + 1)] = $value;
		}
		$this->customer['shiptocity'] = $item['SHIPTOCITY'];
		$this->customer['shiptostate'] = $item['SHIPTOSTATE'];
		$this->customer['shiptocountry'] = $item['SHIPTOCOUNTRY'];
		$this->customer['shiptozip'] = $item['SHIPTOZIP'];			
	}
	
	private function parse_uom($uom_string) {
		$matches = array();
		preg_match('/\(([a-z]+)\)/i', $uom_string, $matches);
		
		if (isset($matches[1])) {
			return $matches[1];
		} else {
			return FALSE;
		}
	}
}
