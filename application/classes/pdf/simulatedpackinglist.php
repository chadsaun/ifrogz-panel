<?php defined('SYSPATH') or die('No direct script access.');

/**
* PDF for creating simulated packing lists
*/
class PDF_SimulatedPackingList extends PDF {
	
	public $accept_page_break = TRUE;
	public $customer = array();
	
	public function Header() {
		// Font Sizes (points)
		$fs_large = 10;
		$fs_normal = 8;
		$fs_small = 6;
		
		$width_left_col = 224;
		$width_right_col = 35;
		$border = 0;
		$cell_height = 5;
		
		$this->Cell($width_left_col, $cell_height, 'NEW FORTUNE LOGISTICS LTD.', $border, 0, 'C');
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell($width_right_col, $cell_height, 'DATE: ' . date('d-M-y'), $border, 1, 'R');
		$this->Cell($width_left_col, $cell_height, '1107, 11/F TOWER 3 ENTERPRISES SQUARE1, 9 SHEUNG YUET ROAD KOWLOON BAY HONG KONG', $border, 0, 'C');
		$this->SetFont('Arial', 'U', $fs_normal);
		$this->Cell($width_right_col, $cell_height, 'Page ' . $this->PageNo() . ' of {nb}', $border, 1, 'R');
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell($width_left_col, $cell_height, 'TEL: (852) 24288868', $border, 1, 'C');
		$this->SetFont('Arial', 'U', $fs_normal);
		$this->Cell($width_left_col, $cell_height, 'ORIGINAL PACKING LIST', $border, 1, 'C');
		
		$this->Ln();
		
		// Customer
		$this->SetFont('Arial', '', $fs_normal);
		$this->Cell(40, $cell_height, 'on account of Messers.', 0, 0, 'R');
		$this->Cell(200, $cell_height, $this->customer['shiptoname'], 0, 1);
		for ($i = 1; $i < 5; $i++) {
			if (isset($this->customer['shiptoaddress' . $i]) && ! empty($this->customer['shiptoaddress' . $i])) {
				//echo Debug::vars($this->customer['shiptoaddress' . $i]);
				$this->Cell(40, $cell_height, '', 0, 0, 'R');
				$this->Cell(200, $cell_height, $this->customer['shiptoaddress' . $i], 0, 1);
			}
		}
		$this->Cell(40, $cell_height, '', 0, 0, 'R');
		$this->Cell(200, $cell_height, $this->customer['shiptocity'] . ', ' . $this->customer['shiptostate'] . ' ' . $this->customer['shiptozip'], 0, 1);
		$this->Cell(40, $cell_height, '', 0, 0, 'R');
		$this->Cell(200, $cell_height, $this->customer['shiptocountry'], 0, 1);
		
		$this->Ln(1);
		
		// Table Header
		$this->SetFont('Arial', '', $fs_normal);
		$header = array('MARKS & NOS:', 'NO OF PACKAGE', 'DESCRIPTION OF GOODS', 'QTY/PACKAGE', 'G.W./CTN', 'MEASUREMENT/CTN');
		$w = array(28, 28, 94, 33, 33, 44);
		for ($i=0; $i < count($header); $i++) { 
			$this->Cell($w[$i], 4, $header[$i], 'TB', 0, 'C');
		}
		$this->Ln();
	}
	
	
	
}
