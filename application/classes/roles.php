<?php defined('SYSPATH') or die('No direct script access.');

/**
* This class defines the constants for each employee role.
*
* @author iFrogz Developers <developers@ifrogz.com>
* @version 2012-01-11
* @copyright (c) 2011-2012, iFrogz (a subsidiary of Zagg, Inc.)
* @package Security
* @category Roles
*/
class Roles extends Base_Roles {

    const PERMIT_ALL = 'i_f_t,i_f_a,i_f_ch,i_f_c,i_f_ca,i_f_com,all,i_f_h,i_f_v,i_f_i,i_f_m,i_f_n,i_f_p,i_f_qc,i_f_rr,i_f_sa,i_f_z,i_f_s,permitted';

    const DENY_ALL = '';

    const ACCOUNTING = 'i_f_t';
    const ADMIN = 'i_f_a';
    const CHARTS = 'i_f_ch';
    const CUSTOMER_SERVICE = 'i_f_c';
    const CUSTOMER_SERVICE_ADMIN = 'i_f_ca'; // deprecated
	const ECOMMERCE = 'i_f_com';
    const FULL_ACCESS = 'all';
    const HONG_KONG = 'i_f_h';
	const INVENTORY = 'i_f_v';
    const IT = 'i_f_i';
    const MANAGEMENT = 'i_f_m';
    const NADAL = 'i_f_n'; // deprecated
    const PRODUCTS = 'i_f_p';
    const QUALITY_CONTROL = 'i_f_qc';
    const RETAIL_REPORTS = 'i_f_rr';
    const SALES = 'i_f_sa';
    const SHIELD_ZONE = 'i_f_z'; // deprecated
    const SHIPPING = 'i_f_s';
    const USER = 'permitted';

}
?>