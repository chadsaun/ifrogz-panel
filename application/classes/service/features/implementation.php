<?php defined('SYSPATH') or die('No direct script access.');

class Service_Features_Implementation implements Service_Features_Interface {
	
	private $gallery_images = array();
	protected $dal_features = NULL;

   	public function __construct() {
		$this->dal_features = new DAL_Features_Implementation();
	}
	
	public function get_feature($url) {
		return $this->dal_features->get_feature($url);
	}
	
	public function get_features($status, $type) {
		
		switch ($status) {
			case 'active':
				$status = '1';
			break;
			case 'inactive':
				$status = '0';
			break;
			default:
				// code...
			break;
		}
		
		return $this->dal_features->get_features($status, $type);
	}
	
	public function get_current_employee() {
		return $this->dal_features->get_current_employee();
	}
	
	public function get_images($id, $type) {
		return $this->dal_features->get_images($id, $type);
	}
	
	public function count_images($id, $type) {
		return $this->dal_features->count_images($id, $type);
	}
	
	public function save_feature($data) {
		
		$data['url'] = strtolower(preg_replace('/\s/', '-', $data['name']));
		
		$data['posted'] = date('Y-m-d H:i:s', strtotime($data['posted']));
		
		if (isset($data['active'])) {
			if ($data['active'] == 'yes') {
				$data['status'] = 1;
			} 
		} else {
			$data['status'] = 0;
		}
		
		if ( ! isset($data['id'])) {
			$this->dal_features->save_feature($data);
		} else {
			$this->dal_features->save_feature_edit($data);
		}
		
	}
	
	public function save_image($file, $old_feature, $new_feature) {
		
		$new_feature['posted'] = date("Y-m-d H:i:s");
		
		$new_feature['active'] = 1;
		
		if (empty($file['file']['name'])) {
			$new_feature['file'] = $old_feature['FileName'];
		} else {
			$new_feature['file'] = preg_replace('/\s/', '_', $file['file']['name']);
			$s3 = new Amazon_S3();
			$s3->upload_file($file['file']['tmp_name'], 'ifrogz.com', 'lib/images/features/' . $old_feature['URL'] . '/' . $new_feature['file']);
		}
		
		$this->dal_features->save_image($new_feature);	
	}
	
	public function save_gallery($file, $old_feature, $new_feature) {
		
		$new_feature['posted'] = date("Y-m-d H:i:s");
		
		$new_feature['active'] = 1;
		
		$image_count = $this->count_images($old_feature['ID'], 2);
		
		$new_feature['type'] = 2;
		$new_feature['file'] = $old_feature['URL'] .'_'. $image_count . '.jpg';
		$s3 = new Amazon_S3();
		$s3->upload_file($file['file1']['tmp_name'], 'ifrogz.com', 'lib/images/features/' . $old_feature['URL'] . '/thumb/' . $new_feature['file']);
		
		$this->dal_features->save_image($new_feature);
		
		$new_feature['type'] = 3;
		$new_feature['file'] = $old_feature['URL'] .'_'. $image_count . '.jpg';
		$s3 = new Amazon_S3();
		$s3->upload_file($file['file2']['tmp_name'], 'ifrogz.com', 'lib/images/features/' . $old_feature['URL'] . '/large/' . $new_feature['file']);
		
		$this->dal_features->save_image($new_feature);	
	}
	
	public function save_audio($data) {
		
		$data['posted'] = date("Y-m-d H:i:s");
		
		$data['active'] = 1;
		
		$this->dal_features->save_audio($data);	
	}
	
	public function delete_image($image) {
		
		$image['posted'] = date("Y-m-d H:i:s");
		$image['status'] = 0;
		
		$this->dal_features->delete_image($image);

	}

}
?>