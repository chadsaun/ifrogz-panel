<?php defined('SYSPATH') or die('No direct script access.');

interface Service_Features_Interface {

	public function get_feature($url);
	
	public function get_features($status, $type);
	
	public function get_current_employee();
	
	public function get_images($id, $type);
	
	public function count_images($id, $type);
	
	public function save_feature($data);
	
	public function save_image($file, $old_feature, $new_feature);
	
	public function save_gallery($file, $old_feature, $new_feature);
	
	public function save_audio($data);
	
	public function delete_image($image);

}
?>