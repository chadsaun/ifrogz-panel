<?php defined('SYSPATH') OR die('No direct access allowed.');

class Service_WiFi_Implementation implements Service_WiFi_Interface {
	
	protected $dal_wifi = NULL;
	
	public function __construct(DAL_WiFi_Interface $dal_wifi) {
		$this->dal_wifi = $dal_wifi;
	}
	
	/**
	 * This function will get the password for the selected network tier.
	 * 
	 * @access public
	 * 
	 * @param string $network_tier
	 * @return string.
	 */
	public function get_password($network_tier) {
		$password = $this->dal_wifi->get_password($network_tier);
		$encrypt = Encrypt::instance($password['type']);
		return $encrypt->decode($password['password']);
	}
	
	/**
	 * This function will record that a user did in fact retrieve one of the
	 * network passwords.
	 * 
	 * @access public
	 * 
	 * @param string $network_tier
	 * @param integer $employee_id
	 * @param string $ip_address
	 * @return void
	 */
	public function log_password_retrieval($network_tier, $employee_id, $ip_address) {
		return $this->dal_wifi->log_password_retrieval($network_tier, $employee_id, $ip_address);
	}
	
	/**
	 * This function will return all the networks that end users are allowed to
	 * see the password.
	 * 
	 * @access public
	 * 
	 * @return array
	 */
	public function get_all_viewable_networks() {
		return $this->dal_wifi->get_all_viewable_networks();
	}
	
	/**
	 * This function will return all the networks.
	 * 
	 * @access public
	 * 
	 * @return array
	 */
	public function get_all_networks() {
		return $this->dal_wifi->get_all_networks();
	}
	
	/**
	 * This function will get a network tier by id.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @return array
	 */
	public function get_network_tier_by_id($id) {
		return $this->dal_wifi->get_network_tier_by_id($id);
	}
	
	/**
	 * This function will take the columns provided and create a new password. 
	 * It will return the ID of the newly created record.
	 * 
	 * @access public
	 * 
	 * @param array $values
	 * @return integer
	 */
	public function create_password(array $values) {
		$encrypt = Encrypt::instance('network_tiers');
		$values['IsActive'] = 1;
		$values['SCPasswordTypes_ID'] = 1;
		$values['DateCreated'] = date('Y-m-d H:i:s');
		$values['Hash'] = $encrypt->encode($values['Hash']);
		return $this->dal_wifi->create_password($values);
	}
	
	/**
	 * This function will take the columns provided and create a new network
	 * tier. It will return the ID of the newly created record.
	 * 
	 * @access public
	 * 
	 * @param array $values
	 * @return integer
	 */
	public function create_network_tier(array $values) {
		return $this->dal_wifi->create_network_tier($values);
	}
	
	/**
	 * This function will take the id provided and update that record with the
	 * provided values.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @param array $values
	 * @return integer
	 */
	public function update_network_tier($id, array $values) {
		return $this->dal_wifi->update_network_tier($id, $values);
	}
	
	/**
	 * This function will delete the record that corresponds to the id.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @return void
	 */
	public function delete_network_tier_by_id($id) {
		return $this->dal_wifi->delete_network_tier_by_id($id);
	}
	
	/**
	 * This function will grab the columns of the network tiers table.
	 *
	 * @access public
	 *
	 * @return array
	 */
	public function get_network_tier_columns() {
		return $this->dal_wifi->get_network_tier_columns();
	}
	
}
?>