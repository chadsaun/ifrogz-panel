<?php defined('SYSPATH') OR die('No direct access allowed.');

interface Service_WiFi_Interface {
	
	/**
	 * This function will get the password for the selected network tier.
	 * 
	 * @access public
	 * 
	 * @param string $network_tier
	 * @return string
	 */
	public function get_password($network_tier);
	
	/**
	 * This function will record that a user did in fact retrieve one of the
	 * network passwords.
	 * 
	 * @access public
	 * 
	 * @param string $network_tier
	 * @param integer $employee_id
	 * @param string $ip_address
	 * @return void
	 */
	public function log_password_retrieval($network_tier, $employee_id, $ip_address);
	
	/**
	 * This function will return all the networks that end users are allowed to
	 * see the password.
	 * 
	 * @access public
	 * 
	 * @return array
	 */
	public function get_all_viewable_networks();
	
	/**
	 * This function will return all the networks.
	 * 
	 * @access public
	 * 
	 * @return array
	 */
	public function get_all_networks();
	
	/**
	 * This function will get a network tier by id.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @return array
	 */
	public function get_network_tier_by_id($id);
	
	/**
	 * This function will take the columns provided and create a new password. 
	 * It will return the ID of the newly created record.
	 * 
	 * @access public
	 * 
	 * @param array $values
	 * @return integer
	 */
	public function create_password(array $values);
	
	/**
	 * This function will take the columns provided and create a new network
	 * tier. It will return the ID of the newly created record.
	 * 
	 * @access public
	 * 
	 * @param array $values
	 * @return integer
	 */
	public function create_network_tier(array $values);
	
	/**
	 * This function will take the id provided and update that record with the
	 * provided values.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @param array $values
	 * @return integer
	 */
	public function update_network_tier($id, array $values);
	
	/**
	 * This function will delete the record that corresponds to the id.
	 * 
	 * @access public
	 * 
	 * @param integer $id
	 * @return void
	 */
	public function delete_network_tier_by_id($id);
	
	/**
	 * This function will grab the columns of the network tiers table.
	 *
	 * @access public
	 *
	 * @return array
	 */
	public function get_network_tier_columns();
	
}
?>