<?php defined('SYSPATH') or die('No direct script access.');

/**
* Used to manage the customer's shipping options
*/
class Shipping {

	public $shipping_methods = array();

	protected $_ship_country = 1;    // United States of America;
	protected $_ship_type = NULL;
	protected $_postal_zone = 1;    // 1 is US
	
	// The rate index corresponds to a 'zcRate' column in the 'zonecharges' table
	protected $_rate_index = '';

	public static function instance() {
		static $singleton = null;
		if (is_null($singleton)) {
		    $session = Session::instance();
		    if ($session->get('shipping')) {
    			$singleton = $session->get('shipping');
    		}
    		else {
    			$singleton = new Shipping();
    			$session->set('shipping', $singleton);
    		}
		}
		return $singleton;
	}

	protected function __construct() {
		// Load the ship type
		$this->_ship_type = DB::select('adminShipping')->from('admin')->where('adminID', '=', 1)->execute()->get('adminShipping');
		$this->shipping_methods = $this->get_methods();
	}

	public function get_country()
	{
		return $this->_ship_country;
	}
	
	public function get_methods($weight = 0.001) 
	{
		if ($this->_ship_type == 2) 
		{
			$zc = ORM::factory('ifrogz_zonecharge')
					->join('postalzones')->on('zonecharges.zcZone', '=', 'postalzones.pzID')
					->join('countries')->on('countries.countryZone', '=', 'postalzones.pzID')
					->where('countries.countryID', '=', $this->_ship_country)
					->where('zonecharges.zcWeight', '>=', $weight)
					->order_by('zonecharges.zcWeight')
					->limit(1)
					->find();
			
			if (!$zc->loaded()) 
			{
				$zc = ORM::factory('ifrogz_zonecharge')
						->join('postalzones')->on('zonecharges.zcZone', '=', 'postalzones.pzID')
						->join('countries')->on('countries.countryZone', '=', 'postalzones.pzID')
						->where('countries.countryID', '=', $this->_ship_country)
						->order_by('zonecharges.zcWeight', 'DESC')
						->limit(1)
						->find();
			}
			
			$zc = $zc->as_array();
			
			//echo Kohana::debug($zc);
			
			# Remove any methods that are not available for this zone
			$multi_shipping = ORM::factory('ifrogz_postalzone', $zc['zcZone'])->pzMultiShipping;
			foreach ($zc as $key => $value) 
			{
				if (substr($key, 0, 6) == 'zcRate') 
				{
					$index = (int) substr($key, -1);
					if ($index <= 0) 
					{
						$index = 1;
					}
					if ($multi_shipping < ($index - 1)) 
					{
						unset($zc[$key]);
					}
				}
			}
			
			//echo Kohana::debug($zc);
			
			$available_rate_indexes = array();
			foreach ($zc as $key => $value) 
			{
				if (preg_match('/^zcRate/i', $key)) 
				{
					$index = (int) substr($key, -1);
					if ($index <= 0) 
					{
						$index = 1;
					}
					if ($value > 0) 
					{
						$available_rate_indexes[] = $index;
					}
				}
			}

			//echo Kohana::debug($available_rate_indexes);
			
			
			# Get the shipping method names
			$pz = ORM::factory('ifrogz_postalzone')
					->join('countries', 'INNER')
					->on('postalzones.pzID', '=', 'countries.countryZone')
					->where('countryID', '=', $this->_ship_country)
					->find()
					->as_array();

			//echo Kohana::debug($pz);

			# Build the array of available shipping methods
			$methods = array();
			foreach ($available_rate_indexes as $key => $value) 
			{
				$methods[$value] = $pz['pzMethodName' . $value];
			}
			
			# If their selected shipping method is not available, then select the first one in the list
			$cur_rate_index = $this->_rate_index;
			if ($cur_rate_index == '') 
			{
				$cur_rate_index = 1;
			}
			if (!in_array($cur_rate_index, $available_rate_indexes)) 
			{
				foreach ($available_rate_indexes as $key => $value) {
					if ($value == 1) 
					{
						$this->_rate_index = '';
					}
					else
					{
						$this->_rate_index = $value;
					}
					
					break;
				}
			}

			if (empty($methods)) 
			{
				throw new Kohana_Exception('No postalzones were found. Query: ' . Database::instance()->last_query);
			}
			
			return $methods;				
		}
		throw new Kohana_Exception('Incorrect ship type used in get_methods: ' . $ship_type);
	}
	
	/**
	 * Gets the shipping price
	 *
	 * @param float			the weight
	 * @return integer		the rate index
	 **/
	public function get_price($weight = 0.001, $_index = 0) 
	{
		$get_specific_price = FALSE;
		
		if (!$_index) 
		{
			$_index = $this->_rate_index;
		}
		else
		{
			$get_specific_price = TRUE;
		}
		
		if ($this->_ship_type == 2) 
		{
			$Cart = Cart::instance();
			if ($Cart->sub_total() <= 0.00) 
			{
			    return 0.00;
			}
			
			//Fire::log($this->_postal_zone);
			
			$rate = ORM::factory('ifrogz_zonecharge')->where('zcZone', '=', $this->_postal_zone)->where('zcWeight', '>=', $weight)->order_by('zcWeight')->find_all()->get('zcRate' . $_index);
			
			if ($rate > 0) 
			{
				return $rate;
			}
			else
			{
				# Rate is not available for their selected shipping method
				
				# Get first available shipping method at their weight
				$charges = ORM::factory('ifrogz_zonecharge')->where('zcZone', '=', $this->_postal_zone)->where('zcWeight', '>=', $weight)->order_by('zcWeight')->limit(1)->find();
				
				if ($charges->loaded()) 
				{					
					$charges = $charges->as_array();
					
					$fields = preg_grep('/^zcRate(\d*)/i', array_keys($charges));
					$methods = Arr::extract($charges, $fields);
					
					//echo Kohana::debug($methods);
					
					if ($get_specific_price) 
					{
						$index_used = -1;
						foreach ($methods as $col => $value) 
						{
							$rate_index = max(1, (int) substr($col, -1));
							if ($rate_index == $_index) 
							{
								return $value;
							}
						}
					}
					else
					{
						$index_used = -1;
						foreach ($methods as $col => $value) 
						{
							$rate_index = max(1, (int) substr($col, -1));
							if ($value > 0) 
							{
								$this->set_method($rate_index);
								return $value;
							}
						}
					}
					
					
				}
				else // There was no available shipping rate at their current weight.  Grab the max and calculate extra
				{
					# No specified rate - grab the available shipping methods at the highest weight allowed
					$temp = ORM::factory('ifrogz_zonecharge')->where('zcZone', '=', $this->_postal_zone)->order_by('zcWeight', 'DESC')->limit(1)->find()->as_array();
					
					$high_weight = $temp['zcWeight'];
					
					//echo Kohana::debug($temp);
					
					$fields = preg_grep('/^zcRate(\d*)/i', array_keys($temp));
					$methods = Arr::extract($temp, $fields);
					
					# Remove all methods that are not available
					foreach ($methods as $key => $value) 
					{
						if ($value <= 0) 
						{
							unset($methods[$key]);
						}
					}
					
					if (!empty($methods)) 
					{
						//echo Kohana::debug($methods);
						//echo Kohana::debug($_index);
						
						# See if their current selected method is available at this weight
						if (!isset($methods['zcRate' . $_index])) 
						{
							# Grab the first available rate_index and it's value
							$first_value = 0;
							$first_rate_index = 0;
							foreach ($methods as $key => $value) 
							{
								$first_rate_index = max(1, (int) substr($key, -1));
								$first_value = $value;

								break;
							}
							if (!$get_specific_price) 
							{
								$this->set_method($first_rate_index);
							}
						}
						else
						{
							$first_value = $methods['zcRate' . $_index];
							$first_rate_index = max(1, (int) $_index);
						}

						//echo Kohana::debug($first_rate_index);
						//echo Kohana::debug($first_value);

						# Calculate the overage charges
						$overage_charge = ORM::factory('ifrogz_zonecharge')->where('zcZone', '=', $this->_postal_zone)->where('zcWeight', '<', 0)->limit(1)->find()->as_array();

						$weight_difference = $weight - $high_weight;
						
						$index = '';
						if ($first_rate_index > 1) 
						{
							$index = $first_rate_index;
						}

						//echo Kohana::debug('((' . $weight_difference . '/' . abs($overage_charge['zcWeight']) . ') * ' . $overage_charge['zcRate' . $index] . ') + ' . $first_value);

						return ((int)($weight_difference / abs($overage_charge['zcWeight'])) * $overage_charge['zcRate' . $index]) + $first_value;
					}
					else
					{
						throw new Kohana_Exception('There are no available shipping methods for this weight: ' . $weight);
					}
				}
			}
			
			return ;
		}
		throw new Kohana_Exception('Incorrect ship type used in get_price: ' . $ship_type);
	}

	public function get_prices($weight = 0.001)	
	{
		if ($this->_ship_type == 2) 
		{
			$tmp = ORM::factory('ifrogz_zonecharge')->where('zcZone', '=', $this->_postal_zone)->where('zcWeight', '>=', $weight)->order_by('zcWeight')->find();
			
			$prices = array();

			for ($i = 1; $i < 6; $i++) 
			{				
				$price = $this->get_price($weight, $i);
				if ($price > 0) 
				{
					$prices[$i] = $this->get_price($weight, $i);
				}
			}
			
			//echo Kohana::debug($prices);
			
			return $prices;
		}
		throw new Kohana_Exception('Incorrect ship type used in get_prices: ' . $ship_type);
	}
	
	public function get_rate_index() 
	{
		return $this->_rate_index;
	}
	
	public function get_method_name($weight) 
	{
		$methods = $this->get_methods($weight);
		$rate_index = $this->_rate_index;
		
		if (empty($rate_index)) 
		{
			$rate_index = 1;
		}
		
		foreach ($methods as $i => $method) 
		{
			if ($rate_index == $i) 
			{
				return $method;
			}
		}
	}

    /**
    * This function will return the ship method as a bitwise number.  This function should be
    * used in conjunction with the methods in the Discounts class.
    *
    * @access public
    * @return integer           the bitwise number
    */
    public function get_method_flag() 
	{
		$rate_index = $this->_rate_index;
		if (!is_integer($rate_index) || ($rate_index < 1)) 
		{
			$rate_index = 1;
		}
		return pow(2, $rate_index) / 2;
	}
	
	/**
	 * Sets the country and returns all available shipping methods based on the weight of the cart
	 *
	 * @param integer $ship_country 
	 * @param float $weight 
	 * @return void
	 */
	public function set_country($ship_country, $weight)
	{
		$this->_ship_country = $ship_country;
		$this->_postal_zone = ORM::factory('ifrogz_country', $ship_country)->countryZone;
		$this->_rate_index = 1; // Default to one
		$this->shipping_methods = $this->get_methods($weight);
	}
	
	public function set_method($rate_index)
	{
		if ($rate_index <= 1) 
		{
			$rate_index = '';
		}
		$this->_rate_index = $rate_index;
	}
}
