<?php defined('SYSPATH') OR die('No direct access allowed.');

$config = array(
	Kohana::STAGING => array(
		'username' => 'devadmin',
		'password' => 'web@ccess!'
	),
	Kohana::PRODUCTION => array(
		'username' => 'remote',
		'password' => 'c0ntr0l'
	)
);

return $config;