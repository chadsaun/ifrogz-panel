<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
* This class allows for pages that would normally require a user to be logged
* into the Web site to bypass the log in requirement.
*
* @author iFrogz Developers <developers@ifrogz.com>
* @version 2011-12-20
* @copyright (c) 2011, iFrogz (a subsidiary of Zagg, Inc.)
* @package Security
* @category Bypass
*/

$config = array(
    'hkpackinglists' => array(
        'getpackinglist' => '1',
        'getinvoice' => '1',
    ),
	'desktop' => array(
	    'index' => '1',
	),
    'password' => array(
		'forgot' => '1',
		'reset' => '1',
    ),
	'user' => array(
		'deletecookie' => '1',
		'login' => '1',
		'savecookie' => '1',
	)
);

return $config;
?>