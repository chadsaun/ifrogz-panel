<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * @package  Encrypt
 *
 * Encrypt configuration is defined in groups which allows you to easily switch
 * between different encryption settings for different uses.
 * Note: all groups inherit and overwrite the default group.
 *
 * Group Options:
 *  key    - Encryption key used to do encryption and decryption. The default option
 *           should never be used for a production website.
 *
 *           For best security, your encryption key should be at least 16 characters
 *           long and contain letters, numbers, and symbols.
 *           @note Do not use a hash as your key. This significantly lowers encryption entropy.
 *
 *  mode   - MCrypt encryption mode. By default, MCRYPT_MODE_NOFB is used. This mode
 *           offers initialization vector support, is suited to short strings, and
 *           produces the shortest encrypted output.
 *           @see http://php.net/mcrypt
 *
 *  cipher - MCrypt encryption cipher. By default, the MCRYPT_RIJNDAEL_128 cipher is used.
 *           This is also known as 128-bit AES.
 *           @see http://php.net/mcrypt
 */
$config = array();

$config['default'] = array(
	'key'    => hash('md5', 'A chorus of frogs'),
	'mode'   => MCRYPT_MODE_NOFB,
	'cipher' => MCRYPT_RIJNDAEL_128
);

$config['network_tiers'] = array(
	'key' => 'this is a test',
	'cipher' => MCRYPT_RIJNDAEL_256,
	'mode'   => MCRYPT_MODE_NOFB,
);

return $config;
?>