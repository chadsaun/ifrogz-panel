<?php defined('SYSPATH') OR die('No direct access allowed.');

$config = array(
	'images' => '/lib/images/',
	'js_inc' => '/lib/js/inc/',
	'js_pages' => '/lib/js/pages/',
	'js_util' => '/lib/js/util/',
	's3_image_path'	=> 'http://cloudfront.ifrogz.com/lib/images/',
	's3_path'	=> 'http://cloudfront.ifrogz.com/lib/',
	'css' => '/lib/css/',
);

return $config;
