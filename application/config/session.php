<?php defined('SYSPATH') or die('No direct script access.');

$config = array(
	'native' => array(
		'name' => 'PHPSESSID',
		'encrypted' => TRUE,
		'lifetime' => 43200,
	),
);

return $config;
?>