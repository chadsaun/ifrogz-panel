<?php 
$lang = array
(
	'RFOrganization_ID' => array
	(
		'required' => 'Please pick an organization',
	),
	'RFDivisions_ID' => array
	(
		'required' => 'Please pick a division',
	),
	'ICProducts_ID' => array
	(
		'required' => 'Please pick a product',
	),
	'RFOrderTypes_ID' => array
	(
		'required' => 'Please pick an order type',
	),
	'BeginningDate' => array
	(
		'required' => 'Please pick a beginning date',
	),
	'EndingDate' => array
	(
		'required' => 'Please pick an ending date',
	),
	'BeginningSequential' => array
	(
		'required' => 'Please pick a beginning sequential',
		'int' => 'Please enter a positive integer',
	),
	'EndingSequential' => array
	(
		'required' => 'Please pick an ending sequential',
		'int' => 'Please enter a positive integer',
		'range_match' => 'The range does not match the quantity',
		'range_conflict' => 'The range specified conflicts with an existing keycode',
		'range_available' => 'The range is larger than the number of codes available for this batch',
	),
	'DRAttCodes_BatchNumber' => array
	(
		'required' => 'Please pick a batch number',
		'exists' => 'That batch number already exists',
		'default' => 'Batch must be 3 digits long with leading zeros',
	),
	'BatchQuantity' => array
	(
		'required' => 'Please choose the batch quantity',
		'int' => 'Please enter a positive integer',
		'default' => 'Please choose the batch quantity',
	),
	'prefix' => array
	(
		'prefix' => 'Your prefix must be 2 characters long and not start with "A"',
	),
	'RFManagers_ID' => array
	(
		'required' => 'Please pick a manager',
	),
	'Value' => array
	(
		'required' => 'A value is required',
		'decimal' => 'Value must be a decimal',
		'default' => 'Invalid input',
	),
	'Description' => array
	(
		'required' => 'A description is required',
		'standard_text' => 'Description must contain standard text',
		'default' => 'Invalid input',
	),
);