<?php

return array(
	'file' => array(
		'not_empty' => 'You forgot to browse to a file',
		'type' => 'Please upload a CSV file'
	),
    'date' => array(
        'date' => 'Please enter a valid date'
    )
);