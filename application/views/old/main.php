<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title><?php echo $title; ?></title>

<link rel="stylesheet" type="text/css" href="/lib/css/adminstyle.css" />
<link rel="stylesheet" type="text/css" href="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8/ui-lightness/jquery-ui.custom.css" />

<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery/1.4.2/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery-form/2.43/jquery.form.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8/jquery-ui.custom.min.js"></script>
</head>
<body>
<?php
    include(APPPATH.'views/partials/admin/language.php');
    function isPermitted($permission = 'all') {
    	global $_SESSION;

    	$mappings = array(
    	    'accounting' => 'i_f_t',
        	'admin' => 'i_f_a',
        	'all' => 'all',
        	'customer service' => 'i_f_c',
        	'customer service admin' => 'i_f_ca', // deprecated
        	'hong kong' => 'i_f_h',
    	    'inventory' => 'i_f_v',
        	'it' => 'i_f_i',
        	'management' => 'i_f_m',
        	'nadal' => 'i_f_n', // deprecated
        	'product' => 'i_f_p',
        	'quality control' => 'i_f_qc',
        	'reports_retail' => 'i_f_rr',
    	    'sales' => 'i_f_sa',
        	'shieldzone' => 'i_f_z', // deprecated
        	'shipping' => 'i_f_s',
        );

    	$permission = strtolower($permission);

        if (isset($mappings[$permission])) {
            $permission = $mappings[$permission];
        }

        if (isset($_SESSION['employee']['permissions'])) {
            $roles = preg_split('/,+/', $_SESSION['employee']['permissions']);
        	$length = count($roles);
        	for ($i = 0; $i < $length; $i++) {
        		$roles[$i] = trim($roles[$i]);
        		if (!empty($roles[$i]) && ($roles[$i] == $permission)) {
        			return TRUE;
        		}
        	}
        }

    	return FALSE;
    }
    include(APPPATH.'views/partials/admin/nav.php');
?>
<div id="main">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
            <td><?php echo $content; ?></td>
        </tr>
    </table>
</div>
</body>
</html>