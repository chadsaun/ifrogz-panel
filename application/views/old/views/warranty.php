<style type="text/css" media="screen">
    td.v1-header        { background-color: #030133; color: #FFFFFF; font-weight: bold; }
	td.v1-left-column   { width: 20px; }
	td.v1-right-column  { width: 833px; }
</style>
<?= form::open('/warranty_registrations/search', array('id' => 'search_box')); ?>
<div>
    <table>
        <thead>
            <tr>
                <td class="v1-header" colspan="2">
                    <table style="width: 100%;">
                        <tr>
                            <td>Search: Warranty Registrations</td>
                            <td style="text-align: right;"><?= form::submit('submit', 'Search'); ?></td>
                        <tr>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>    
                <td class="v1-left-column"><?= form::radio('method', 'keyword', (strcmp($search_box['method'], 'keyword') == 0)); ?></td>
                <td class="v1-right-column"><?= form::label('keyword', 'Keyword:'); ?></td>
            </tr>
            <tr>
                <td class="v1-left-column">&nbsp;</td>
                <td class="v1-right-column"><?= form::dropdown('category', $search_box['categories'], $search_box['category']); ?>&nbsp;<?= form::input('text', $search_box['text'], ' style="width: 130px;"'); ?></td>
            </tr>
            <tr>
                <td class="v1-left-column"><?= form::radio('method', 'time_frame', (strcmp($search_box['method'], 'time_frame') == 0)); ?></td>
                <td class="v1-right-column"><?= form::label('time_frame', 'Time Frame:'); ?></td>
            </tr>
            <tr>
                <td class="v1-left-column">&nbsp;</td>
                <td class="v1-right-column"><?= form::input('beg_date', $search_box['beg_date'], ' class="calendar" style="width: 75px;" readonly="readonly"'); ?>&nbsp;&#151;&nbsp;<?= form::input('end_date', $search_box['end_date'], ' class="calendar" style="width: 75px;" readonly="readonly"'); ?></td>
            </tr>
        </tbody>
    </table>
</div>
<?= form::close(); ?>
<?= $content; ?>