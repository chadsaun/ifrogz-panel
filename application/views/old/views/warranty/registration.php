<style type="text/css" media="screen">
	td.v2-header, p.v2-title    { text-align: center; font-weight: bold; }
	td.v2-header                { background-color: #030133; color: #FFFFFF; height: 25px; }
	td.v2-outter-left-column    { vertical-align: top; width: 427px; }
	td.v2-outter-right-column   { vertical-align: top; width: 427px; }
	td.v2-inner-left-column     { text-align: right; width: 125px; font-weight: bold; }
	td.v2-inner-right-column    { text-align: left;  width: 250px; }
	p.v2-back                   { text-align: center; }
</style>
<?php if (count($records) > 0): ?>
    <p class="v2-title"></p>
    <div>
        <table>
            <thead>
                <tr>
                    <td class="v2-header" colspan="2">Registration &#35;<?= $records[0]->id; ?></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="v2-outter-left-column">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="v2-inner-left-column">First Name:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->first_name; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Last Name:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->last_name; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Address:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->address_1; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->address_2; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">City:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->city; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">State:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->state; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Country:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->country; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Phone:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->phone; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Email:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->email; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="v2-outter-right-column">
                        <table>
                            <tbody>
                                <tr>
                                    <td class="v2-inner-left-column">Store:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->store; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Product:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->product; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Purchase Price:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->purchase_price; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Date Purchased:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->date_purchased; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Comment:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->comment; ?></td>
                                </tr>
                                <tr>
                                    <td class="v2-inner-left-column">Date Registered:&nbsp;</td>
                                    <td class="v2-inner-right-column"><?= $records[0]->date_registered; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <p class="v2-title">Unable to find registration.</p>
<?php endif; ?>
<p class="v2-back"><a href="javascript:history.go(-1);">Back</a></p>