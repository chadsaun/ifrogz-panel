<style type="text/css" media="screen">
	p.v2-title      { text-align: center; font-weight: bold; }
	p.pagination     { text-align: center; }
	tr.v2-header    { background-color: #030133; color: #FFFFFF; font-weight: bold; height: 25px; }
	td.v2-ID        { width: 65px; }
	td.v2-name      { width: 150px; }
	td.v2-city      { width: 75px; }
	td.v2-state     { width: 75px; }
	td.v2-store     { width: 100px; }
	td.v2-product   { width: 150px; }
	td.v2-price     { width: 65px; }
	td.v2-date      { width: 150px; }
</style>
<?php if (count($records) > 0): ?>
    <br />
    <div>
        <table>
            <thead>
                <tr class="v2-header">
                    <td class="v2-ID">Reg. ID</td>
                    <td class="v2-name">Name</td>
                    <td class="v2-city">City</td>
                    <td class="v2-state">State</td>
                    <td class="v2-store">Store</td>
                    <td class="v2-product">Product</td>
                    <td class="v2-price">Cost</td>
                    <td class="v2-date">Date Registered</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($records as $record): ?>
                    <tr>
                        <td class=\"v2-ID\"><a href="/warranty_registrations/registration/<?= $record->id; ?>/"><?= $record->id; ?></a></td>
                        <td class=\"v2-name\"><?= $record->last_name; ?>, <?= $record->first_name; ?></td>
                        <td class=\"v2-city\"><?= $record->city; ?></td>
                        <td class=\"v2-state\"><?= $record->state; ?></td>
                        <td class=\"v2-store\"><?= $record->store; ?></td>
                        <td class=\"v2-product\"><?= $record->product; ?></td>
                        <td class=\"v2-price\">&#36;<?= $record->purchase_price; ?></td>
                        <td class=\"v2-date\"><?= $record->date_registered; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?= $pagination->render(); ?>
<?php else: ?>
    <p class="v2-title">No registrations found.</p>
<? endif; ?>