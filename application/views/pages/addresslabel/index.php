<style type="text/css">
    @media all {
        td { vertical-align: top; }
        td.v1-top-left-column   { font-size: 7pt;}
        td.v1-bottom-column     { width: 400px; font-size: 8pt; font-weight: bold; }
    }
</style>
<div>
    <table>
        <tbody>
            <tr>
                <td class="v1-top-left-column">
                    From:<br />
                    <?= $sender_address; ?>
                </td>
                <td class="v1-top-right-column" style="text-align: right;">
                    <?= $barcode; ?>
                </td>
            </tr>
            <tr>
                <td class="v1-bottom-column" colspan="2">
                    <br />
                    &nbsp;&nbsp;&nbsp;To:<br />
                    <?= $recipient_address; ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>