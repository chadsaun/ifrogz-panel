<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/scriptaculous.js"></script>
<?php
ini_set('memory_limit','60M');
include('init.php');
include_once(IFZROOT.'kohana.php');
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$nprodoptions=0;
$nprodsections=0;
$nalloptions=0;
$nallsections=0;
$nalldropship=0;
$alreadygotadmin = getadminsettings();
$simpleOptions = (($adminTweaks & 2)==2);
$simpleSections = (($adminTweaks & 4)==4);
$dorefresh=FALSE;
if(@$maxprodsects=="") $maxprodsects=20;

// Build employee array
$aEmployees = array();

$config_admin = RBI_Kohana::config('database.default_admin.connection');

$db_admin = mysql_connect($config['hostname'], $config['username'], $config['password']);
mysql_select_db($config['database']) or die ('DB Admin connection failed.</td></tr></table></body></html>');
$sql = "SELECT * FROM employee WHERE department LIKE '%CS%'";
$res = mysql_query($sql, $db_admin) or print(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
	$aEmployees[] = $row;
	$aEmployees2[$row['id']] = $row;
}
mysql_free_result($res);

$aEmpByID = array();
$aEmpByName = array();
$sql = "SELECT * FROM employee";
$res = mysql_query($sql, $db_admin) or print(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
	$aEmpByID[$row['id']] = $row;
	$name = ucwords($row['firstname']) . ' ' . ucwords($row['lastname']);
	$aEmpByName[$name] = $row;
}
mysql_free_result($res);

include(APPPATH.'views/partials/admin/dbconnection.php');

// Process form
$frmError = false;
$frmMsg = '';
$frmSuccess = '';
if ($_POST['submit']) {
	//echo "<pre>"; print_r($_POST); echo "</pre>";
	
	if (!empty($_POST['dscCpn'])) {
		$cpaType = 0;
		foreach ($_POST['assign'] as $assignment) {
			if (preg_match("/^[0-9]+$/i", $assignment)) {
				$cpaType = 1;
			} else {
				$cpaType = 2;
			}
			$sql = "INSERT INTO cpnassign ( cpaCpnID , cpaType , cpaAssignment ) 
					VALUES ( '".$_POST['dscCpn']."' , '$cpaType' , '$assignment' )";
			$res = mysql_query($sql) or print(mysql_error());
		}
		$frmSuccess = 'Items have been assigned successfully!';
	} else {
		$frmError = true;
		$frmMsg = 'You need to select a discount/coupon.';
	}
}

// Prepare variables
$aDiscounts = array();
$sql = "SELECT * 
		FROM coupons 
		WHERE cpnGrpCpnID = 0
		AND cpnEndDate >= '" . date('Y-m-d H:i:s') . "'
		AND (cpnSitewide=0 OR cpnSitewide=3)
		ORDER BY cpnIsCoupon, cpnEndDate DESC, cpnBeginDate DESC, cpnWorkingName";
$res = mysql_query($sql) or print(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
	$aDiscounts[] = $row;
}
mysql_free_result($res);

$aCategories = array();
$sql = "SELECT sectionID,sectionWorkingName,sectionDescription,topSection,rootSection,sectionDisabled,sectionTag FROM sections ORDER BY sectionOrder, sectionWorkingName";
$res = mysql_query($sql) or print(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
	$aCategories[] = $row;
}
mysql_free_result($res);
// Add category path
for ($i = 0; $i < count($aCategories); $i++) {
	$topSectionList = "";
	$topSection = $aCategories[$i]['sectionID'];
	// Loop through 10 times. Each loop queries a subcategory unless there isn't one.
	for ($index=0; $index <= 10; $index++) {
		// This means there is not another sub category and we can quit the loop
		if ($topSection == 0) {
			$topSectionList = 'Home' . $topSectionList;
			break;
		} elseif ($index == 10) {
			$topSectionList = "Loop " . $topSectionList;
		} else {	
			$sql = "SELECT sectionID,topSection,sectionWorkingName,rootSection FROM sections WHERE sectionID=" . $topSection;
			$res = mysql_query($sql) or print(mysql_error());
			if ($res) {
				while ($row = mysql_fetch_assoc($res)) {
					$topSectionList = " &raquo; " . $row["sectionWorkingName"] . $topSectionList;
					$topSection = $row["topSection"];
				}
			} else {
				break;
			}
			mysql_free_result($res);
		}
	}
	$aCategories[$i]['catPath'] = $topSectionList;
}


?>
<script type="text/javascript">
<!--
Event.observe(window, 'load', function() {
	getProds();								 
});

function addItem(id) {
	var divTo = $('to');
	var html = divTo.innerHTML;
	var selObj = $(id);
	var selIndex = selObj.selectedIndex;
	var name = selObj.options[selIndex].text;
	var value = $F(id);
	
	// Check for already existing items as well
	$strSearch = 'value="' + id + '"';
	if (html.indexOf(name) < 0) {
		if (id != '') {
			html += '<div id="asnItem' + value + '" style="display: none;"><input type="button" value=" - " onclick="delItem(\'asnItem' + value + '\')" />&nbsp;<input type="hidden" name="assign[]" value="' + value + '"/>' + name + '</div>';
			divTo.innerHTML = html;
			Effect.Appear('asnItem'+value, { duration: 0.5 });
		}
	} else {
		alert("That item already exists.");
	}
}

function delItem(id) {
	Effect.Fade(id, { duration: 0.5 });
	// Delay removing the element so the effect can finish
	var t = setTimeout("$('"+id+"').remove();", 1000);
}

function getProds() {
	var sectionID = $F('sections');
	var url = '/admin/asndiscountsaj.php';
	
	new Ajax.Updater('prod_sel', url, {
					 parameters: { id: sectionID }
					 });
}

-->
</script>
<style type="text/css">
<!--
#coupons {
	margin: 0 auto;
	/*width: 240px;*/
	height: 200px;
	border: 1px solid #333333;
	overflow-y: auto;
	overflow-x: hidden;
}
#discounts {
	margin: 0 auto;
	/*width: 240px;*/
	height: 200px;
	border: 1px solid #333333;
	overflow-y: auto;
	overflow-x: hidden;
}
#assign {
	margin: 0 auto;
	text-align: center;
}
#to {
	margin: 0 auto;
	padding: 0 5px;
	height: 200px;
	border: 1px inset #333;
	overflow-y: auto;
}
#to div {
	margin: 5px 0;
}
#categories {
	margin: 0 auto;
	text-align: center;
}
#products {
	margin: 0 auto;
	text-align: center;
}

.error {
	background-color: #FFFFCC;
	border: 1px solid #FFCC66;
	padding: 5px;
	margin: 5px;
	color: #000;
}

.success {
	margin: 5px;
	padding: 5px;
	background-color: #E6FFDC;
	border: 1px solid #588048;
	color: #000;
}

-->
</style>
<h2 style="margin-top: 0; padding: 3px; font-size: 14px; font-weight: bold; color: #FFF; background-color: #030133;">Assign Discounts / Coupons</h2>

<div style="width: 800px;">

<?php
	if ($frmError) {
?>
		<div class="error"><?=$frmMsg?></div>
<?php
	}
	if (!empty($frmSuccess)) {
?>
		<div class="success"><?=$frmSuccess?></div>
<?php
	}
?>

	<form id="mainform" name="mainform" method="post" action="/admin/asndiscounts.php">
		<h3>Categories</h3>
		
		<div id="categories">
			<select id="cats" name="cats">
				<option value="">Choose...</option>
	<?php
	for ($i = 0; $i < count($aCategories); $i++) {
	?>
				<option value="<?=$aCategories[$i]['sectionID']?>"><?=$aCategories[$i]['catPath'] . " | " .$aCategories[$i]['sectionWorkingName']?></option>
	<?php
	}
	?>	
			</select>&nbsp;<input type="button" value="Add" onclick="addItem('cats')" />
		</div>
		
		<h3>Products</h3>
		
		<div style="margin: 0 0 0 20px;">
			<strong>Option Group</strong><br />
			<select id="sections" name="sections" onchange="getProds()">
		<?php
		$sql = "SELECT s.sectionID, s.sectionWorkingName, p.pID
				FROM sections s LEFT JOIN products p ON s.sectionID = p.pSection
				WHERE rootSection = 1
				AND pID IS NOT NULL
				GROUP BY sectionWorkingName
				ORDER BY sectionWorkingName";
		$res = mysql_query($sql);
		while ($row = mysql_fetch_assoc($res)) {
		?>
				<option value="<?=$row['sectionID']?>"<?=($row['sectionWorkingName'] == 'iPod Sets') ? ' selected="selected"' : ''?>><?=$row['sectionWorkingName']?></option>
		<?php
		}
		?>
			</select>
		</div>
		
		<div id="products" style="margin: 5px 0 0 20px; text-align: left;">
			<strong>Products</strong><br />
			<span id="prod_sel"></span>&nbsp;<input type="button" value="Add" onclick="addItem('selProds')" />
		</div>
		
		<p>&nbsp;</p>
		
		<h3 style="text-align: center;">Assign</h3>
		<div id="assign">
			<select id="dscCpn" name="dscCpn">
				<option value="">---------</option>
				<option value="">Discounts</option>
				<option value="">---------</option>
	<?php
	$displayedCoupons = false;
	for ($i = 0; $i < count($aDiscounts); $i++) {
		if ($aDiscounts[$i]['cpnIsCoupon'] == 1 && !$displayedCoupons) {
	?>
				<option value="">---------</option>
				<option value="">Coupons</option>
				<option value="">---------</option>
	<?php
			$displayedCoupons = true;
		}
	?>
				<option value="<?=$aDiscounts[$i]['cpnID']?>"><?=$aDiscounts[$i]['cpnWorkingName']?></option>
	<?php
	}
	?>	
			</select>
		</div>
		
		<h3 style="text-align: center;">To</h3>
		<div id="to"></div>
		
		<div style="padding: 5px 0; text-align: right;"><input type="submit" name="submit" value="Assign" /></div>
		
		</div>
	</form>
</div>

<div>&nbsp;</div>

