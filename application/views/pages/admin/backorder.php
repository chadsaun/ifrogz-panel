<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/popcalendar.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/pages/admin/fastinit.js"></script>
<script language="JavaScript" type="text/javascript" src="/includes/tablekit1.2.2/js/tablekit.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="/includes/tablekit1.2.2/css/style.css" />
<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
//showarray($_POST);

if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;

getadminsettings();

// Get the items that are back ordered
$sql = "SELECT o.ordID, o.ordDate, c.cartProdID, c.cartProdName, SUM(c.cartQuantity) AS `totQty`, co.coCartOption, co.coExtendShipping, co.coOptGroup
		FROM orders o, cart c, cartoptions co
		WHERE o.ordID = c.cartOrderID
		AND c.cartID = co.coCartID
		AND coExtendShipping > 0
		AND o.ordStatus = 4
		GROUP BY co.coOptID
		ORDER BY totQty DESC";
$res = mysql_query($sql) or print(mysql_error());

$arrData = array();
while ($row = mysql_fetch_assoc($res)) {
	$arrData[] = $row;
}

if (!empty($arrData)) {
?>
<h1>Current Back Ordered Items</h1>

<p>Click table header to sort</p>
<table width="100%" border="0" cellpadding="1" cellspacing="1" class="sortable">
	<thead>
		<!--<tr bgcolor="#030133">-->
		<tr>
			<th align="center" width="130"><strong>Product&nbsp;ID&nbsp;</strong></th>
			<th align="center" width="300"><strong>Product&nbsp;Name</strong></th>
			<th align="center"><strong>Quantity</strong></th>
			<th align="center"><strong>Option Group</strong></th>			
			<th align="center" width="175"><strong>Option</strong></th>
			<th align="center"><strong>Extended Shipping</strong></th>
		</tr>
	</thead>
	<tbody>
<?php
	for ($i = 0; $i < count($arrData); $i++) {
?>
	<tr>
		<td><?=$arrData[$i]['cartProdID']?></td>
		<td><?=$arrData[$i]['cartProdName']?></td>
		<td align="right"><?=$arrData[$i]['totQty']?></td>
		<td><?=$arrData[$i]['coOptGroup']?></td>
		<td><?=$arrData[$i]['coCartOption']?></td>
		<td align="right"><?=$arrData[$i]['coExtendShipping']?></td>
	</tr>
<?php
	}
?>
	</tbody>
</table>
<?php
}
?>