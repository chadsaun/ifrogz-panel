<?php
include('init.php');
if ( ! defined('KOHANA_EXTERNAL_MODE')) {
    define('KOHANA_EXTERNAL_MODE', TRUE);
}
include_once(DOCROOT.'index.php');

session_register('chads_coupons');
session_register('shp_method');
session_register('hasrelated');

//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
//include(APPPATH.'views/pages/admin/uspsshipping.php');

//echo '<p>Coupons = '.$_SESSION["couponapply"].'</p>';
function vrxmlencode($xmlstr){
	$xmlstr = str_replace('&', '&amp;', $xmlstr);
	$xmlstr = str_replace('<', '&lt;', $xmlstr);
	$xmlstr = str_replace('>', '&gt;', $xmlstr);
	$xmlstr = str_replace("'", '&apos;', $xmlstr);
	return(str_replace('"', '&quot;', $xmlstr));
}						//echo '<strong>mode:</strong>'.$_POST["mode"];
if (@$cartisincluded!=TRUE) {
	include(APPPATH.'views/pages/admin/uspsshipping.php');
	include(DOCROOT.'includes/splitorder.php');
}

//showarray($_POST);

$cartEmpty=FALSE;
$isInStock=TRUE;
$outofstockreason=0;
if(@$dateadjust=="") $dateadjust=0;
$errormsg = "";
$demomode = FALSE;
$WSP = "";
$OWSP = "";
$nodiscounts=FALSE;
$maxshipoptions=20;
$success=TRUE;
$checkIntOptions=FALSE;
$alldata = "";
$shipMethod = "";
$shipping = 0;
$iTotItems = 0;
$iWeight = 0;
$countryTaxRate=0;
$stateTaxRate=0;
$countryTax=0;
$stateTax=0;
$stateAbbrev="";
$international = "";
$thePQuantity = 0;
$thePWeight = 0;
$totalquantity = 0;
$statetaxfree = 0;
$countrytaxfree = 0;
$shipfreegoods = 0;
$totalgoods = 0;
$somethingToShip = FALSE;
$freeshippingapplied = FALSE;
$gotcpncode=FALSE;
$isstandardship = FALSE;
$numshipoptions=0;
$homecountry = FALSE;
$freeshipamnt = 0;
$rowcounter = 0;
$paypalexpress=FALSE;
$token = '';
$ppexpresscancel=FALSE;
$appliedcouponname = $ordAVS = $ordCVV = $stateAbbrev = $international = '';
$appliedcouponamount = $totalquantity = $statetaxfree = $countrytaxfree = $shipfreegoods = $totalgoods = 0;
if(@$cartisincluded != TRUE){
	if(@$_SERVER['CONTENT_LENGTH'] != '' && $_SERVER['CONTENT_LENGTH'] > 10000) exit();
	$cartisincluded=FALSE;
	//$cpncode = trim(str_replace("'",'',@$_REQUEST['cpncode']));
	if(!empty($_SESSION['os'])) $cpncode=$_SESSION['os'];
	else $cpncode = trim(str_replace("'","",@$_POST["cpncode"]));
	if(@$_POST['payerid'] != '') $payerid = $_POST['payerid']; else $payerid = '';
	//$token = trim(@$_REQUEST['token']);
	if(trim(@$_POST["token"]) != '')
		$token = trim(@$_POST["token"]);
	elseif(trim(@$_GET["token"]) != '')
		$token = trim(@$_GET["token"]);
	if(trim(@$_POST['sessionid']) != '') $thesessionid=str_replace("'",'',trim($_POST['sessionid'])); else $thesessionid=session_id();
	$theid = mysql_real_escape_string(trim(@$_POST['id']));
	$checkoutmode	   = trim(@$_POST['mode']);
	$shippingpost	   = trim(@$_POST['shipping']);
	$commerciallocpost = trim(@$_POST['commercialloc']);
	$wantinsurancepost = trim(@$_POST['wantinsurance']);
	$payproviderpost   = trim(@$_POST['payprovider']);
}
//if(@$_POST['payerid'] != '') $payerid = $_POST['payerid']; else $payerid = '';
//if(trim(@$_POST["sessionid"]) != "") $thesessionid=trim($_POST["sessionid"]); else $thesessionid=session_id();
if(@$_SESSION["clientUser"] != ""){
	if(($_SESSION["clientActions"] & 8) == 8){
		$WSP = "pWholesalePrice AS ";
		if(@$wholesaleoptionpricediff==TRUE) $OWSP = 'optWholesalePriceDiff AS ';
		if(@$nowholesalediscounts==TRUE) $nodiscounts=TRUE;
	}
	if(($_SESSION["clientActions"] & 16) == 16){
		$WSP = $_SESSION["clientPercentDiscount"] . "*pPrice AS ";
		if(@$wholesaleoptionpricediff==TRUE) $OWSP = $_SESSION["clientPercentDiscount"] . '*optPriceDiff AS ';
		if(@$nowholesalediscounts==TRUE) $nodiscounts=TRUE;
	}
}
//$theid = mysql_real_escape_string(trim(@$_POST["id"]));
$alreadygotadmin = getadminsettings();
$origShipType=$shipType;
if(@$alternateratesups != "" || @$alternateratesusps != "" || @$alternateratesweightbased != "" || @$alternateratescanadapost !="") $alternaterates=TRUE; else $alternaterates=FALSE;
if(@$_POST["altrates"] != ""){
	$altrate=(int)@$_POST["altrates"];
	if(@$alternateratesups != "" && $altrate==4) $shipType=4;
	if(@$alternateratesusps != "" && $altrate==3) $shipType=3;
	if(@$alternateratesweightbased != "" && $altrate==2) $shipType=2;
	if(@$alternateratescanadapost != "" && $altrate==6) $shipType=6;
}
$ordPayProvider = str_replace("'",'',trim(@$_POST["payprovider"]));
if($ordPayProvider != "") eval('$handling += @$handlingcharge' . $ordPayProvider . ';');
if(@$_SESSION["couponapply"] != ""){
	mysql_query("UPDATE coupons SET cpnNumAvail=cpnNumAvail+1 WHERE cpnID IN (0" . $_SESSION["couponapply"] . ")") or print(mysql_error());
	$_SESSION["couponapply"]="";
}
function show_states($tstate){
	global $xxOutState,$allstates,$numallstates;
	$foundmatch=FALSE;
	print "<option value=''>Choose...</option>";
	print "<option value=''>" . $xxOutState . "</option>";
	for($index=0;$index<$numallstates;$index++){
		print '<option value="' . str_replace('"','&quot;',$allstates[$index]["stateAbbrev"]) . '"';
		if($tstate==$allstates[$index]["stateAbbrev"]){
			print ' selected';
			$foundmatch=TRUE;
		}
		print '>' . $allstates[$index]["stateAbbrev"] . "</option>\n";
	}
	return $foundmatch;
}
function show_countries($tcountry){
	global $numhomecountries,$nonhomecountries,$allcountries,$numallcountries;
	for($index=0;$index<$numallcountries;$index++){
		print '<option value="' . str_replace('"','&quot;',$allcountries[$index]["countryName"]) . '"';
		if($tcountry==$allcountries[$index]["countryName"]) print " selected";
		print '>' . $allcountries[$index][2] . "</option>\n";
	}
}
function checkuserblock($thepayprov){
	global $blockmultipurchase;
	$multipurchaseblocked=FALSE;
	if($thepayprov != "7" && $thepayprov != "13"){
		$theip = @$_SERVER["REMOTE_ADDR"];
		if($theip == "") $theip = "none";
		if(@$blockmultipurchase != ""){
			mysql_query("DELETE FROM multibuyblock WHERE lastaccess<'" . date("Y-m-d H:i:s", time()-(60*60*24)) . "'") or print(mysql_error());
			$sSQL = "SELECT ssdenyid,sstimesaccess FROM multibuyblock WHERE ssdenyip = '" . trim(mysql_real_escape_string($theip)) . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result)){
				mysql_query("UPDATE multibuyblock SET sstimesaccess=sstimesaccess+1,lastaccess='" . date("Y-m-d H:i:s", time()) . "' WHERE ssdenyid=" . $rs["ssdenyid"]) or print(mysql_error());
				if($rs["sstimesaccess"] >= $blockmultipurchase) $multipurchaseblocked=TRUE;
			}else{
				mysql_query("INSERT INTO multibuyblock (ssdenyip,lastaccess) VALUES ('" . trim(mysql_real_escape_string($theip)) . "','" . date("Y-m-d H:i:s", time()) . "')") or print(mysql_error());
			}
			mysql_free_result($result);
		}
		if($theip == "none")
			$sSQL = "SELECT TOP 1 dcid FROM ipblocking";
		else
			$sSQL = "SELECT dcid FROM ipblocking WHERE (dcip1=" . ip2long($theip) . " AND dcip2=0) OR (dcip1 <= " . ip2long($theip) . " AND " . ip2long($theip) . " <= dcip2 AND dcip2 <> 0)";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result) > 0)
			$multipurchaseblocked = TRUE;
	}
	return($multipurchaseblocked);
}
function checkpricebreaks($cpbpid,$origprice){
	global $WSP;
	$newprice="";
	$sSQL = "SELECT SUM(cartQuantity) AS totquant FROM cart WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartProdID='".mysql_real_escape_string($cpbpid)."'";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rs=mysql_fetch_assoc($result);
	if(is_null($rs["totquant"])) $thetotquant=0; else $thetotquant = $rs["totquant"];
	$sSQL="SELECT ".$WSP."pPrice FROM pricebreaks WHERE ".$thetotquant.">=pbQuantity AND pbProdID='".mysql_real_escape_string($cpbpid)."' ORDER BY " . ($WSP==""?"pPrice":str_replace(' AS ','',$WSP));
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs=mysql_fetch_assoc($result))
		$thepricebreak = $rs["pPrice"];
	else
		$thepricebreak = $origprice;
	$sSQL = "UPDATE cart SET cartProdPrice=".$thepricebreak." WHERE cartAltPrice=-1 AND cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartProdID='".mysql_real_escape_string($cpbpid)."'";
	mysql_query($sSQL) or print(mysql_error());
}
function multShipWeight($theweight, $themul){
	return(($theweight*$themul)/100.0);
}
function insuranceandtaxaddedtoshipping(){
	global $shipinsuranceamt,$shippingpost,$somethingToShip,$wantinsurance,$addshippinginsurance,$maxshipoptions;
	global $totalgoods,$shipping,$taxShipping,$shippingpost,$stateTaxRate,$countryTaxRate,$intShipping;
	if(is_numeric(@$shipinsuranceamt) && $shippingpost=='' && $somethingToShip){
		if(($wantinsurance=="Y" && @$addshippinginsurance==2) || @$addshippinginsurance==1){
			for($index3=0; $index3 < $maxshipoptions; $index3++)
				$intShipping[$index3][2] += (((double)$totalgoods*(double)$shipinsuranceamt)/100.0);
			$shipping += (((double)$totalgoods*(double)$shipinsuranceamt)/100.0);
		}elseif(($wantinsurance=="Y" && @$addshippinginsurance==-2) || @$addshippinginsurance==-1){
			for($index3=0; $index3 < $maxshipoptions; $index3++)
				$intShipping[$index3][2] += $shipinsuranceamt;
			$shipping += $shipinsuranceamt;
		}
	}
	if(@$taxShipping==1 && $shippingpost==''){
		for($index3=0; $index3 < $maxshipoptions; $index3++)
			$intShipping[$index3][2] += ((double)$intShipping[$index3][2]*((double)$stateTaxRate+(double)$countryTaxRate))/100.0;
		$shipping += ((double)$shipping*((double)$stateTaxRate+(double)$countryTaxRate))/100.0;
	}
}
function calculatetaxandhandling(){
	global $handlingchargepercent,$handling,$totalgoods,$shipping,$totaldiscounts,$freeshipamnt,$taxHandling,$stateTaxRate,$countryTaxRate,$taxShipping;
	global $stateTax,$countryTax,$canadataxsystem,$shipCountryID,$shipStateAbbrev,$usehst,$statetaxfree,$countrytaxfree,$proratashippingtax,$perproducttaxrate;
	if(@$handlingchargepercent != '') $handling += ((($totalgoods + $shipping + $handling) - ($totaldiscounts + $freeshipamnt)) * $handlingchargepercent / 100.0);
	if(@$taxHandling==1) $handling += ((double)$handling*((double)$stateTaxRate+(double)$countryTaxRate))/100.0;
	if(@$canadataxsystem==true && $shipCountryID==2 && ($shipStateAbbrev=="NB" || $shipStateAbbrev=="NF" || $shipStateAbbrev=="NS")) $usehst=true; else $usehst=false;
	if(@$canadataxsystem==true && $shipCountryID==2 && ($shipStateAbbrev=="PE" || $shipStateAbbrev=="QC")){
		$statetaxable = 0;
		$countrytaxable = 0;
		if(@$taxShipping==2 && ($shipping - $freeshipamnt > 0)){
			if(@$proratashippingtax==TRUE){
				if($totalgoods>0) $statetaxable += (((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree)) / $totalgoods) * ((double)$shipping-(double)$freeshipamnt);
			}else
				$statetaxable += ((double)$shipping-(double)$freeshipamnt);
			$countrytaxable += ((double)$shipping-(double)$freeshipamnt);
		}
		if(@$taxHandling==2){
			$statetaxable += (double)$handling;
			$countrytaxable += (double)$handling;
		}
		if($totalgoods>0){
			$statetaxable += ((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree));
			$countrytaxable += ((double)$totalgoods-((double)$totaldiscounts+(double)$countrytaxfree));
		}
		$countryTax = $countrytaxable*(double)$countryTaxRate/100.0;
		$stateTax = ($statetaxable+(double)$countryTax)*(double)$stateTaxRate/100.0;
	}else{
		if($totalgoods>0){
			$stateTax = ((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree))*(double)$stateTaxRate/100.0;
			if(@$perproducttaxrate != TRUE) $countryTax = ((double)$totalgoods-((double)$totaldiscounts+(double)$countrytaxfree))*(double)$countryTaxRate/100.0;
		}
		if(@$taxShipping==2 && ($shipping - $freeshipamnt > 0)){
			if(@$proratashippingtax==TRUE){
				if($totalgoods>0) $stateTax += (((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree)) / $totalgoods) * (((double)$shipping-(double)$freeshipamnt)*(double)$stateTaxRate/100.0);
			}else
				$stateTax += (((double)$shipping-(double)$freeshipamnt)*(double)$stateTaxRate/100.0);
			$countryTax += (((double)$shipping-(double)$freeshipamnt)*(double)$countryTaxRate/100.0);
		}
		if(@$taxHandling==2){
			$stateTax += ((double)$handling*(double)$stateTaxRate/100.0);
			$countryTax += ((double)$handling*(double)$countryTaxRate/100.0);
		}
	}
	if($stateTax < 0) $stateTax = 0;
	if($countryTax < 0) $countryTax = 0;
}
function subtaxesfordiscounts($theExemptions, $discAmount){
	global $statetaxfree,$countrytaxfree,$shipfreegoods;
	if(($theExemptions & 1)==1) $statetaxfree -= $discAmount;
	if(($theExemptions & 2)==2) $countrytaxfree -= $discAmount;
	if(($theExemptions & 4)==4) $shipfreegoods -= $discAmount;
}
function addadiscount($resset, $groupdiscount, $dscamount, $subcpns, $cdcpncode, $statetaxhandback, $countrytaxhandback, $theexemptions, $thetax){
	global $totaldiscounts, $cpnmessage, $statetaxfree, $countrytaxfree, $gotcpncode, $perproducttaxrate, $countryTax , $cpnIDs , $appliedcouponname , $appliedcouponamount,$thesessionid;
	$totaldiscounts += $dscamount;
	if($groupdiscount){
		$statetaxfree -= ($dscamount * $statetaxhandback);
		$countrytaxfree -= ($dscamount * $countrytaxhandback);
	}else{
		subtaxesfordiscounts($theexemptions, $dscamount);
		if(@$perproducttaxrate) $countryTax -= (($dscamount * $thetax) / 100.0);
	}
	$usecoupon=TRUE;
	//echo '$cdcpncode='.$cdcpncode;
	$cpnDiscount=substr($resset['cpnWorkingName'],0,10);
	//echo '$cpnDiscount='.$cpnDiscount;
	$sql_dd="SELECT * FROM daily_discounts WHERE starttime LIKE '".$cpnDiscount."%'";
	//echo $sql_dd;
	$result_dd=mysql_query($sql_dd);
	if(mysql_num_rows($result_dd)>0){
		$row_dd=mysql_fetch_assoc($result_dd);
		//echo 'starttime='.$row_dd['starttime'];
		$sql_d="SELECT cartDateAdded FROM cart WHERE cartSessionID='".$thesessionid."' AND cartDateAdded<'".$row_dd['starttime']."'";
		//echo $sql_d;
		$result_d=mysql_query($sql_d);
		if(mysql_num_rows($result_d)>0){
			echo '<div style="color:#FF0000; font-weight:bold; margin:4px; font-size:14px;"> You must add the Snatch It product to the cart after the green light flashes to get it for free.</div>';
			$dscamount=0;
			$totaldiscounts=0;
			$usecoupon=FALSE;
		}
	}
	//echo '$dscamount='.$dscamount;
	//echo '$totaldiscounts='.$totaldiscounts;

	//$dscamount=0;
	//$totaldiscounts=0;
	if($usecoupon){
		//echo 'used';
		if(stristr($cpnmessage,"<br />" . $resset[getlangid("cpnName",1024)] . "<br />") == FALSE) $cpnmessage .= $resset[getlangid("cpnName",1024)] . "<br />";
		// added Blake 1/2/07
		// add discount ids to order
		$comma=",";
		if(empty($cpnIDs)) $comma=" ";
		$cpnIDs .= $comma.$resset["cpnID"];
		// end
		if($subcpns){
			$theres = mysql_query("SELECT cpnID FROM coupons WHERE cpnNumAvail>0 AND cpnNumAvail<30000000 AND cpnID=" . $resset["cpnID"]) or print(mysql_error());
			if($theresset = mysql_fetch_assoc($theres)) @$_SESSION["couponapply"] .= "," . $resset["cpnID"];
			mysql_query("UPDATE coupons SET cpnNumAvail=cpnNumAvail-1 WHERE cpnNumAvail>0 AND cpnNumAvail<30000000 AND cpnID=" . $resset["cpnID"]) or print(mysql_error());
		}
		if($cdcpncode!="" && strtolower(trim($resset["cpnNumber"]))==strtolower($cdcpncode)) {$gotcpncode=TRUE;$appliedcouponname = $resset['cpnName']; $appliedcouponamount = $dscamount; }
	}
}
function timesapply($taquant,$tathresh,$tamaxquant,$tamaxthresh,$taquantrepeat,$tathreshrepeat){
	if($taquantrepeat==0 && $tathreshrepeat==0)
		$tatimesapply = 1.0;
	elseif($tamaxquant==0)
		$tatimesapply = (int)(($tathresh - $tamaxthresh) / $tathreshrepeat)+1;
	elseif($tamaxthresh==0)
		$tatimesapply = (int)(($taquant - $tamaxquant) / $taquantrepeat)+1;
	else{
		$ta1 = (int)(($taquant - $tamaxquant) / $taquantrepeat)+1;
		$ta2 = (int)(($tathresh - $tamaxthresh) / $tathreshrepeat)+1;
		if($ta2 < $ta1) $tatimesapply = $ta2; else $tatimesapply = $ta1;
	}
	return($tatimesapply);
}
function calculatediscounts($cdgndtot, $subcpns, $cdcpncode){
	global $totaldiscounts, $cpnmessage, $statetaxfree, $countrytaxfree, $nodiscounts, $WSP, $cpncode, $gotcpncode, $thesessionid, $countryTaxRate, $countryTax;
	$totaldiscounts = 0;
	$cpnmessage = "<br />";
	$cdtotquant=0;
	if($cdgndtot==0){
		$statetaxhandback = 0.0;
		$countrytaxhandback = 0.0;
	}else{
		$statetaxhandback = 1.0 - (($cdgndtot - $statetaxfree) / $cdgndtot);
		$countrytaxhandback = 1.0 - (($cdgndtot - $countrytaxfree) / $cdgndtot);
	}
	if(! $nodiscounts){
		$sSQL = "SELECT cartProdID,SUM(cartProdPrice*cartQuantity) AS thePrice,SUM(cartQuantity) AS sumQuant,pSection,COUNT(cartProdID),pExemptions,p_iscert,pTax FROM products INNER JOIN cart ON cart.cartProdID=products.pID WHERE cartCompleted=0 AND cartSessionID='" . $thesessionid . "' GROUP BY cartProdID,pSection,pExemptions,pTax";
		$cdresult = mysql_query($sSQL) or print(mysql_error());
		$cdadindex=0;
		$cdrsPriceCerts=0;
		while($cdrs = mysql_fetch_assoc($cdresult)){
			$cdalldata[$cdadindex++]=$cdrs;
		}
		for($index=0; $index<$cdadindex; $index++){
			$cdrs = $cdalldata[$index];
			//echo '='.$cdrs["p_iscert"];
			if($cdrs['p_iscert'] == 0 ) {
				$sSQL = "SELECT SUM(coPriceDiff*cartQuantity) AS totOpts FROM cart LEFT OUTER JOIN cartoptions ON cart.cartID=cartoptions.coCartID WHERE cartCompleted=0 AND cartSessionID='" . $thesessionid . "' AND cartProdID='" . $cdrs["cartProdID"] . "'";
				$cdresult2 = mysql_query($sSQL) or print(mysql_error());
				$cdrs2 = mysql_fetch_assoc($cdresult2);
				if(! is_null($cdrs2["totOpts"])) $cdrs["thePrice"] += $cdrs2["totOpts"];
				$cdtotquant += $cdrs["sumQuant"];

				$topcpnids = $cdrs["pSection"];
				$thetopts = $cdrs["pSection"];
				if(is_null($cdrs["pTax"])) $cdrs["pTax"] = $countryTaxRate;
				for($cpnindex=0; $cpnindex<= 10; $cpnindex++){
					if($thetopts==0)
						break;
					else{
						$sSQL = "SELECT topSection FROM sections WHERE sectionID=" . $thetopts;
						$result2 = mysql_query($sSQL) or print(mysql_error());
						if($rs2 = mysql_fetch_assoc($result2)){
							$thetopts = $rs2["topSection"];
							$topcpnids .= "," . $thetopts;
						}else
							break;
					}
				}
				$sSQL = "SELECT DISTINCT cpnID,cpnDiscount,cpnWorkingName,cpnType,cpnNumber,".getlangid("cpnName",1024).",cpnThreshold,cpnQuantity,cpnSitewide,cpnThresholdRepeat,cpnQuantityRepeat FROM coupons LEFT OUTER JOIN cpnassign ON coupons.cpnID=cpnassign.cpaCpnID WHERE cpnNumAvail > 0 AND cpnEndDate >= '" . date("Y-m-d H:i:s",time()) ."' AND cpnBeginDate <= '" . date("Y-m-d H:i:s",time()) ."' AND (cpnIsCoupon=0";
				if($cdcpncode != "") $sSQL .= " OR (cpnIsCoupon=1 AND cpnNumber='" . $cdcpncode . "')";
				$sSQL .= ") AND cpnThreshold<=" . $cdrs["thePrice"] . " AND (cpnThresholdMax>" . $cdrs["thePrice"] . " OR cpnThresholdMax=0) AND cpnQuantity<=" . $cdrs["sumQuant"] . " AND (cpnQuantityMax>" . $cdrs["sumQuant"] . " OR cpnQuantityMax=0) AND (cpnSitewide=0 OR cpnSitewide=2) AND ";
				$sSQL .= "(cpnSitewide=2 OR (cpaType=2 AND cpaAssignment='" . $cdrs["cartProdID"] . "') ";
				$sSQL .= "OR (cpaType=1 AND cpaAssignment IN ('" . str_replace(",","','",$topcpnids) . "')))";
				if(!empty($WSP)) $sSQL .= " AND (cpnIsWholesale=1)";
				else $sSQL .= " AND (cpnIsWholesale=0)";
				$result2 = mysql_query($sSQL) or print(mysql_error());
				while($rs2 = mysql_fetch_assoc($result2)){
					if($rs2["cpnType"]==1){ // Flat Rate Discount
						$thedisc = (double)$rs2["cpnDiscount"] * timesapply($cdrs["sumQuant"], $cdrs["thePrice"], $rs2["cpnQuantity"], $rs2["cpnThreshold"], $rs2["cpnQuantityRepeat"], $rs2["cpnThresholdRepeat"]);
						if($cdrs["thePrice"] < $thedisc) $thedisc = $cdrs["thePrice"];
						addadiscount($rs2, FALSE, $thedisc, $subcpns, $cdcpncode, $statetaxhandback, $countrytaxhandback, $cdrs["pExemptions"], $cdrs["pTax"]);
					}elseif($rs2["cpnType"]==2){ // Percentage Discount
						addadiscount($rs2, FALSE, (((double)$rs2["cpnDiscount"] * (double)$cdrs["thePrice"]) / 100.0), $subcpns, $cdcpncode, $statetaxhandback, $countrytaxhandback, $cdrs["pExemptions"], $cdrs["pTax"]);
					}
				}
			} else {
				$cdrsPriceCerts += $cdrs["thePrice"];
			}
		}
		$sSQL = "SELECT DISTINCT cpnID,cpnDiscount,cpnWorkingName,cpnType,cpnNumber,".getlangid("cpnName",1024).",cpnSitewide,cpnThreshold,cpnThresholdMax,cpnQuantity,cpnQuantityMax,cpnThresholdRepeat,cpnQuantityRepeat FROM coupons WHERE cpnNumAvail>0 AND cpnEndDate >= '" . date("Y-m-d H:i:s",time()) ."' AND cpnBeginDate <= '" . date("Y-m-d H:i:s",time()) ."' AND (cpnIsCoupon=0";
		if($cdcpncode != "") $sSQL .= " OR (cpnIsCoupon=1 AND cpnNumber='" . $cdcpncode . "')";
		$sSQL .= ") AND cpnThreshold<=" . $cdgndtot . " AND cpnQuantity<=" . $cdtotquant . " AND (cpnSitewide=1 OR cpnSitewide=3) AND (cpnType=1 OR cpnType=2)";
		if(!empty($WSP)) $sSQL .= " AND (cpnIsWholesale=1)";
		else $sSQL .= " AND (cpnIsWholesale=0)";
		$result2 = mysql_query($sSQL) or print(mysql_error());
		while($rs2 = mysql_fetch_assoc($result2)){
			$totquant = 0;
			$totprice = 0;
			if($rs2["cpnSitewide"]==3){
				$sSQL = "SELECT cpaAssignment FROM cpnassign WHERE cpaType=1 AND cpacpnID=" . $rs2["cpnID"];
				//echo $sSQL;
				$result3 = mysql_query($sSQL) or print(mysql_error());
				$secids = "";
				$addcomma = "";
				while($rs3 = mysql_fetch_assoc($result3)){
					$secids .= $addcomma . $rs3["cpaAssignment"];
					$addcomma = ",";
				}
				if($secids != ""){
					$secids = getsectionids($secids, FALSE);
					$sSQL = "SELECT SUM(cartProdPrice*cartQuantity) AS totPrice,SUM(cartQuantity) AS totQuant FROM products INNER JOIN cart ON cart.cartProdID=products.pID WHERE cartCompleted=0 AND cartSessionID='" . $thesessionid . "' AND products.pSection IN (" . $secids . ")";
					$result3 = mysql_query($sSQL) or print(mysql_error());
					$rs3 = mysql_fetch_assoc($result3);
					if(is_null($rs3["totPrice"])) $totprice = 0; else $totprice = $rs3["totPrice"];
					if(is_null($rs3["totQuant"])) $totquant=0; else $totquant = $rs3["totQuant"];
					$sSQL = "SELECT SUM(coPriceDiff*cartQuantity) AS optPrDiff FROM products INNER JOIN cart ON cart.cartProdID=products.pID LEFT OUTER JOIN cartoptions ON cart.cartID=cartoptions.coCartID WHERE cartCompleted=0 AND cartSessionID='" . $thesessionid . "' AND products.pSection IN (" . $secids . ")";
					$result3 = mysql_query($sSQL) or print(mysql_error());
					$rs3 = mysql_fetch_assoc($result3);
					if(! is_null($rs3["optPrDiff"])) $totprice = $totprice+$rs3["optPrDiff"];
				}
			}else{ // cpnSitewide==1
				$totquant = $cdtotquant;
				$totprice = $cdgndtot-$cdrsPriceCerts;
			}
			if($totquant > 0 && $rs2["cpnThreshold"] <= $totprice && ($rs2["cpnThresholdMax"] > $totprice || $rs2["cpnThresholdMax"]==0) && $rs2["cpnQuantity"] <= $totquant && ($rs2["cpnQuantityMax"] > $totquant || $rs2["cpnQuantityMax"]==0)){
				if($rs2["cpnType"]==1){ // Flat Rate Discount
					$thedisc = (double)$rs2["cpnDiscount"] * timesapply($totquant, $totprice, $rs2["cpnQuantity"], $rs2["cpnThreshold"], $rs2["cpnQuantityRepeat"], $rs2["cpnThresholdRepeat"]);
					if($totprice < $thedisc) $thedisc = $totprice;
				}elseif($rs2["cpnType"]==2){ // Percentage Discount
					$thedisc = ((double)$rs2["cpnDiscount"] * (double)$totprice) / 100.0;
				}
				addadiscount($rs2, TRUE, $thedisc, $subcpns, $cdcpncode, $statetaxhandback, $countrytaxhandback, 3, 0);
				if(@$perproducttaxrate && $cdgndtot > 0){
					for($index=0; $index<$cdadindex; $index++){
						$cdrs = $cdalldata[$index];
						if($rs2["cpnType"]==1) // Flat Rate Discount
							$applicdisc = $thedisc / ($cdtotquant / $cdrs["sumQuant"]);
						elseif($rs2["cpnType"]==2) // Percentage Discount
							$applicdisc = $thedisc / ($cdgndtot / $cdrs["thePrice"]);
						if(($cdrs["pExemptions"] & 2) != 2) $countryTax -= (($applicdisc * $cdrs["pTax"]) / 100.0);
					}
				}
			}
		}
	}
	if($statetaxfree < 0) $statetaxfree = 0;
	if($countrytaxfree < 0) $countrytaxfree = 0;
	$totaldiscounts = round($totaldiscounts, 2);
}
function calculateshippingdiscounts($subcpns){
	global $freeshippingapplied, $nodiscounts, $WSP, $totalgoods, $totalquantity, $cpncode, $freeshipapplies, $isstandardship, $cpnmessage, $shipping, $freeshipamnt, $gotcpncode, $cpnIDs;
	$freeshipamnt = 0;
	if(! $nodiscounts){
		$sSQL = "SELECT cpnID,".getlangid("cpnName",1024).",cpnNumber,cpnDiscount,cpnThreshold,cpnCntry FROM coupons WHERE cpnType=0 AND cpnSitewide=1 AND cpnNumAvail>0 AND cpnThreshold<=".$totalgoods." AND (cpnThresholdMax>".$totalgoods." OR cpnThresholdMax=0) AND cpnQuantity<=".$totalquantity." AND (cpnQuantityMax>".$totalquantity." OR cpnQuantityMax=0) AND cpnEndDate >= '" . date("Y-m-d H:i:s",time()) ."' AND cpnBeginDate <= '" . date("Y-m-d H:i:s",time()) ."' AND (cpnIsCoupon=0 OR (cpnIsCoupon=1 AND cpnNumber='".$cpncode."'))";
		if(!empty($WSP)) $isWholesale=1;
		else $isWholesale=0;
		$sSQL .= " AND (cpnIsWholesale=".$isWholesale.")";
		$result = mysql_query($sSQL) or print(mysql_error());
		while($rs=mysql_fetch_assoc($result)){
			if($freeshipapplies || (int)$rs["cpnCntry"]==0){
				if($cpncode!="" && strtolower(trim($rs["cpnNumber"]))==strtolower($cpncode)) $gotcpncode=TRUE;
				if($isstandardship){
					if(stristr($cpnmessage,"<br />" . $rs[getlangid("cpnName",1024)] . "<br />") == FALSE) $cpnmessage .= $rs[getlangid("cpnName",1024)] . "<br />";
					// added Blake 1/2/07
					// add discount ids to order
					$comma=",";
					if(empty($cpnIDs)) $comma=" ";
					$cpnIDs .= $comma.$rs["cpnID"];
					// end

					$freeshipamnt = $shipping;

					if($subcpns){
						$theres = mysql_query("SELECT cpnID FROM coupons WHERE cpnNumAvail>0 AND cpnNumAvail<30000000 AND (cpnIsWholesale=".$isWholesale.") AND cpnID=" . $rs["cpnID"]) or print(mysql_error());
						if($theresset = mysql_fetch_assoc($theres)) @$_SESSION["couponapply"] .= "," . $rs["cpnID"];
						mysql_query("UPDATE coupons SET cpnNumAvail=cpnNumAvail-1 WHERE cpnNumAvail>0 AND cpnNumAvail<30000000 AND cpnID=" . $rs["cpnID"]) or print(mysql_error());
					}
				}
				$freeshippingapplied = true;


			}

		}
		mysql_free_result($result);
	}
	if($freeshipamnt > $shipping) $freeshipamnt = $shipping;
}
function initshippingmethods(){
	global $shipType,$allzones,$numzones,$splitUSZones,$shiphomecountry,$numshipoptions,$pzFSA,$intShipping,$success,$errormsg,$commercialloc;
	global $uspsmethods,$numuspsmeths,$international,$shipcountry,$maxshipoptions,$origCountry,$willpickuptext,$shipstate,$xxNoMeth,$shipinsuranceamt;
	global $sXML,$uspsUser,$uspsPw,$upsAccess,$upsUser,$upsPw,$upspickuptype,$origZip,$origCountryCode,$destZip,$shipCountryCode,$adminCanPostUser;
	for($i=0; $i < $maxshipoptions; $i++){
		$intShipping[$i][0]="";
		$intShipping[$i][1]="";
		$intShipping[$i][2]=0;
		$intShipping[$i][3]=0;
	}
	if($shipcountry != $origCountry){
		$international = "Intl";
		$willpickuptext = "";
	}
	if($shipType==2 || $shipType==5){ // Weight / Price based shipping
		$allzones="";
		$index=0;
		$numzones=0;
		$zoneid=0;
		if($splitUSZones && $shiphomecountry)
			$sSQL = "SELECT pzID,pzMultiShipping,pzFSA,pzMethodName1,pzMethodName2,pzMethodName3,pzMethodName4,pzMethodName5 FROM states INNER JOIN postalzones ON postalzones.pzID=states.stateZone WHERE stateAbbrev='" . mysql_real_escape_string($shipstate) . "'";
		else
			$sSQL = "SELECT pzID,pzMultiShipping,pzFSA,pzMethodName1,pzMethodName2,pzMethodName3,pzMethodName4,pzMethodName5 FROM countries INNER JOIN postalzones ON postalzones.pzID=countries.countryZone WHERE countryName='" . mysql_real_escape_string($shipcountry) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$zoneid=$rs["pzID"];
			$numshipoptions=$rs["pzMultiShipping"]+1;
			$pzFSA = $rs["pzFSA"];
			for($index3=0; $index3 < $numshipoptions; $index3++){
				$intShipping[$index3][0]=$rs["pzMethodName" . ($index3+1)];
				$intShipping[$index3][2]=0;
				$intShipping[$index3][3]=TRUE;
			}
		}else{
			$success=FALSE;
			$errormsg = "Country / state shipping zone is unassigned.";
		}
		mysql_free_result($result);
		$sSQL = "SELECT zcWeight,zcRate,zcRate2,zcRate3,zcRate4,zcRate5 FROM zonecharges WHERE zcZone=" . $zoneid . " ORDER BY zcWeight";
		$result = mysql_query($sSQL) or print(mysql_error());
		while($rs = mysql_fetch_row($result))
			$allzones[$index++] = $rs;
		mysql_free_result($result);
		$numzones=$index;
	}elseif($shipType==3 || $shipType==4 || $shipType==6){ // USPS / UPS / Canada Post
		$uspsmethods="";
		$numuspsmeths=0;
		if($shipType==3){
			$sSQL = "SELECT uspsMethod,uspsFSA,uspsShowAs FROM uspsmethods WHERE uspsID<100 AND uspsUseMethod=1 AND uspsLocal=";
			if($international=="") $sSQL .= "1"; else $sSQL .= "0";
		}elseif($shipType==4){
			$shipinsuranceamt='';
			$sSQL = "SELECT uspsMethod,uspsFSA,uspsShowAs FROM uspsmethods WHERE uspsID>100 AND uspsID<200 AND uspsUseMethod=1";
		}else
			$sSQL = "SELECT uspsMethod,uspsFSA,uspsShowAs FROM uspsmethods WHERE uspsID>200 AND uspsID<300 AND uspsUseMethod=1";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result) > 0){
			while($rs = mysql_fetch_row($result))
				$uspsmethods[$numuspsmeths++] = $rs;
		}else{
			$success=FALSE;
			$errormsg = "Admin Error: " . $xxNoMeth;
		}
		mysql_free_result($result);
	}
	if($shipType==3)
		$sXML = "<" . $international . "RateRequest USERID=\"" . $uspsUser . "\" PASSWORD=\"" . $uspsPw . "\">";
	elseif($shipType==4){
		$sXML = "<?xml version=\"1.0\"?><AccessRequest xml:lang=\"en-US\"><AccessLicenseNumber>" . $upsAccess . "</AccessLicenseNumber><UserId>" . $upsUser . "</UserId><Password>" . $upsPw . "</Password></AccessRequest><?xml version=\"1.0\"?>";
		$sXML .= "<RatingServiceSelectionRequest xml:lang=\"en-US\"><Request><TransactionReference><CustomerContext>Rating and Service</CustomerContext><XpciVersion>1.0001</XpciVersion></TransactionReference>";
		$sXML .= "<RequestAction>Rate</RequestAction><RequestOption>shop</RequestOption></Request>";
		if(@$upspickuptype!="") $sXML .= "<PickupType><Code>" . @$upspickuptype . "</Code></PickupType>";
		$sXML .= "<Shipment><Shipper><Address>";
		$sXML .= "<PostalCode>" . $origZip . "</PostalCode>";
		$sXML .= "<CountryCode>" . $origCountryCode . "</CountryCode>";
		$sXML .= "</Address></Shipper><ShipTo><Address>";
		$sXML .= "<PostalCode>" . $destZip . "</PostalCode>";
		$sXML .= "<CountryCode>" . $shipCountryCode . "</CountryCode>";
		if($commercialloc!="Y") $sXML .= "<ResidentialAddress/>";
		$sXML .= "</Address></ShipTo>";
		//sXML = "<Service><Code>11</Code></Service>";
	}elseif($shipType==6){
		$sXML = '<?xml version="1.0" ?> ' .
				"<eparcel>" .
				"<language> en </language>" .
				"<ratesAndServicesRequest>" .
				"<merchantCPCID> " . $adminCanPostUser . " </merchantCPCID>" .
				"<fromPostalCode> " . $origZip . " </fromPostalCode>" .
				"<lineItems>";
	}
}
function addproducttoshipping($apsrs, $prodindex){
	global $shipping,$shipType,$packtogether,$shipThisProd,$somethingToShip,$itemsincart,$intShipping,$international,$shipcountry;
	global $rowcounter,$origZip,$destZip,$sXML,$numshipoptions,$allzones,$numzones,$dHighWeight,$adminUnits,$shipCountryCode;
	global $upspacktype,$splitpackat,$iTotItems,$thePQuantity,$thePWeight,$iWeight,$totalgoods,$shipfreegoods;
	if($packtogether) $iTotItems=1; else $iTotItems += 1;
	$shipThisProd=TRUE;
	if(($apsrs["pExemptions"] & 4)==4){ // No Shipping on this product
		if(! $packtogether) $iTotItems -= (int)$apsrs["cartQuantity"];
		$shipThisProd=FALSE;
	}
	if($shipType==1){ // Flat rate shipping
		if($shipThisProd) $shipping += $apsrs["pShipping"] + $apsrs["pShipping2"] * ($apsrs["cartQuantity"]-1);
	}elseif(($shipType==2 || $shipType==5) && @$_POST["shipping"]==""){ // Weight / Price based shipping
		$havematch=FALSE;
		for($index3=0; $index3 < $numshipoptions; $index3++)
			$dHighest[$index3]=0;
		if(is_array($allzones)){
			if($shipThisProd){
				$somethingToShip=TRUE;
				if($shipType==2) $tmpweight = (double)$apsrs["pWeight"]; else $tmpweight = (double)$apsrs["cartProdPrice"];
				if($packtogether){
					$thePWeight += ((double)($apsrs["cartQuantity"])*$tmpweight);
					$thePQuantity = 1;
				}else{
					$thePWeight = $tmpweight;
					$thePQuantity = (double)$apsrs["cartQuantity"];
				}
			}
			if(((!$packtogether && $shipThisProd) || ($packtogether && ($prodindex == $itemsincart))) && $somethingToShip){ // Only calculate pack together when we have the total
				for($index2=0; $index2 < $numzones; $index2++){
					if($allzones[$index2][0] >= $thePWeight){
						$havematch=TRUE;
						for($index3=0; $index3 < $numshipoptions; $index3++){
							$intShipping[$index3][2] += ((double)$allzones[$index2][1+$index3]*$thePQuantity);
							if((double)$allzones[$index2][1+$index3]==-99999.0) $intShipping[$index3][3]=FALSE;
						}
						break;
					}
					$dHighWeight = $allzones[$index2][0];
					for($index3=0; $index3 < $numshipoptions; $index3++)
						$dHighest[$index3]=$allzones[$index2][1+$index3];
				}
				if(! $havematch){
					for($index3=0; $index3 < $numshipoptions; $index3++){
						$intShipping[$index3][2] += $dHighest[$index3];
						if($dHighest[$index3]==-99999.0) $intShipping[$index3][3]=FALSE;
					}
					if($allzones[0][0] < 0){
						$dHighWeight = $thePWeight - $dHighWeight;
						while($dHighWeight > 0){
							for($index3=0; $index3 < $numshipoptions; $index3++)
								$intShipping[$index3][2] += ((double)($allzones[0][1+$index3])*$thePQuantity);
							$dHighWeight += $allzones[0][0];
						}
					}
				}
				for($index3=$numshipoptions-1; $index3 >=0; $index3--){
					if($intShipping[$index3][3]==FALSE){
						for($index4=$index3+1; $index4<=$numshipoptions; $index4++)
							$intShipping[$index4-1]=$intShipping[$index4];
						$numshipoptions--;
					}
				}
			}
		}
	}elseif($shipType==3 && @$_POST["shipping"]==""){ // USPS Shipping
		if($packtogether){
			if($shipThisProd){
				$somethingToShip=TRUE;
				$iWeight += ((double)$apsrs["pWeight"] * (int)$apsrs["cartQuantity"]);
			}
			if(($prodindex == $itemsincart) && $somethingToShip){
				$numpacks=1;
				if(@$splitpackat != "")
					if($iWeight > $splitpackat) $numpacks=ceil($iWeight/$splitpackat);
				if($numpacks > 1){
					if($international != "")
						$sXML .= addInternational($rowcounter,$splitpackat,$numpacks-1,"Package",$shipcountry);
					else
						$sXML .= addDomestic($rowcounter,"Parcel",$origZip,$destZip,$splitpackat,$numpacks-1,"None","REGULAR","True");
					$iTotItems++;
					$iWeight -= ($splitpackat*($numpacks-1));
					$rowcounter++;
				}
				if($international != "")
					$sXML .= addInternational($rowcounter,$iWeight,1,"Package",$shipcountry);
				else
					$sXML .= addDomestic($rowcounter,"Parcel",$origZip,$destZip,$iWeight,1,"None","REGULAR","True");
				$rowcounter++;
			}
		}else{
			if($shipThisProd){
				$somethingToShip=TRUE;
				$iWeight=$apsrs["pWeight"];
				$numpacks=1;
				if(@$splitpackat != "")
					if($iWeight > $splitpackat) $numpacks=ceil($iWeight/$splitpackat);
				if($numpacks > 1){
					if($international != "")
						$sXML .= addInternational($rowcounter,$splitpackat,$apsrs["cartQuantity"]*($numpacks-1),"Package",$shipcountry);
					else
						$sXML .= addDomestic($rowcounter,"Parcel",$origZip,$destZip,$splitpackat,$apsrs["cartQuantity"]*($numpacks-1),"None","REGULAR","True");
					$iTotItems++;
					$iWeight -= ($splitpackat*($numpacks-1));
					$rowcounter++;
				}
				if($international != "")
					$sXML .= addInternational($rowcounter,$iWeight,$apsrs["cartQuantity"],"Package",$shipcountry);
				else
					$sXML .= addDomestic($rowcounter,"Parcel",$origZip,$destZip,$iWeight,$apsrs["cartQuantity"],"None","REGULAR","True");
				$rowcounter++;
			}
		}
	}elseif(($shipType==4 || $shipType==6) && @$_POST["shipping"]==""){ // UPS Shipping
		if(@$upspacktype=="") $upspacktype="02";
		if($packtogether){
			if($shipThisProd){
				$somethingToShip=TRUE;
				$iWeight += ((double)$apsrs["pWeight"] * (int)$apsrs["cartQuantity"]);
			}
			if(($prodindex == $itemsincart) && $somethingToShip){
				$numpacks=1;
				if(@$splitpackat != "")
					if($iWeight > $splitpackat)
						$numpacks=ceil($iWeight/$splitpackat);
				for($index3=0;$index3 < $numpacks; $index3++)
					if($shipType==4)
						$sXML .= addUPSInternational($iWeight / $numpacks,$adminUnits,$upspacktype,$shipCountryCode,$totalgoods-$shipfreegoods);
					else
						$sXML .= addCanadaPostPackage($iWeight / $numpacks,$adminUnits,$upspacktype,$shipCountryCode,$totalgoods-$shipfreegoods, "");
			}
		}else{
			if($shipThisProd){
				$somethingToShip=TRUE;
				$iWeight=$apsrs["pWeight"];
				$numpacks=1;
				if(@$splitpackat != "")
					if($iWeight > $splitpackat)
						$numpacks=ceil($iWeight/$splitpackat);
				for($index2=0;$index2 < (int)$apsrs["cartQuantity"]; $index2++)
					for($index3=0;$index3 < $numpacks; $index3++)
						if($shipType==4)
							$sXML .= addUPSInternational($iWeight / $numpacks,$adminUnits,$upspacktype,$shipCountryCode,$apsrs["cartProdPrice"]);
						else
							$sXML .= addCanadaPostPackage($iWeight / $numpacks,$adminUnits,$upspacktype,$shipCountryCode,$apsrs["cartProdPrice"],$apsrs["pDims"]);
			}
		}
	}
}
function calculateshipping(){
	global $shipType,$isstandardship,$checkIntOptions,$somethingToShip,$willpickuptext,$willpickupcost,$allzones,$numshipoptions,$upsUser,$upsPw,$shipCountryCode,$destZip;
	global $shipping,$shipMethod,$success,$errormsg,$xxNoMeth,$sXML,$intShipping,$pzFSA,$international,$iTotItems,$uspsmethods,$numuspsmeths,$shipstate,$maxshipoptions;
	if($shipType==1){
		$isstandardship = TRUE;
	}elseif(($shipType==2 || $shipType==5) && ($somethingToShip || @$willpickuptext != "")){
		$checkIntOptions = (@$_POST["shipping"]=="");
		if(is_array($allzones) && $numshipoptions>0){
			$shipping = $intShipping[0][2];
			$shipMethod = $intShipping[0][0];
			$isstandardship = (($pzFSA & 1) == 1);
			if($numshipoptions == 1 && @$willpickuptext=="")
				$checkIntOptions = FALSE;
		}else{
			if(@$willpickuptext != ""){
				if(@$willpickupcost != "") $shipping = $willpickupcost;
				$shipMethod = $willpickuptext;
			}else{
				$success = FALSE;
				$errormsg=$xxNoMeth;
				$checkIntOptions = FALSE;
			}
		}
	}elseif($shipType==3 && $somethingToShip){
		$checkIntOptions = (@$_POST["shipping"]=="");
		if(@$_POST["shipping"]==""){
			$sXML .= "</" . $international . "RateRequest>";
			$success = USPSCalculate($sXML,$international,$shipping, $errormsg, $intShipping);
			if(substr($errormsg, 0, 30)=="Warning - Bound Printed Matter") $success=true;
			if($success && $checkIntOptions){ // Look for a single valid shipping option
				$totShipOptions = 0;
				for($indexmso=0; $indexmso<$maxshipoptions; $indexmso++){
					$shipRow = $intShipping[$indexmso];
					if($iTotItems==$shipRow[3]){
						for($index2=0;$index2<$numuspsmeths;$index2++){
							if(trim($shipRow[0]) == trim($uspsmethods[$index2][0])){
								if($totShipOptions==0){
									$shipping = $shipRow[2];
									$shipMethod = trim($uspsmethods[$index2][2]);
									$isstandardship = (int)$uspsmethods[$index2][1];
								}
								$totShipOptions++;
							}
						}
					}
				}
				if($totShipOptions==1)
					$checkIntOptions=FALSE;
				elseif($totShipOptions==0 && @$willpickuptext==""){
					$checkIntOptions=FALSE;
					$success=FALSE;
					$errormsg=$xxNoMeth;
				}
				if(@$willpickuptext != "") $checkIntOptions = TRUE;
			}
			elseif(! $success)
				$errormsg = "USPS error: " . $errormsg;
		}
	}elseif($shipType==4 && $somethingToShip){
		$checkIntOptions = (@$_POST["shipping"]=="");
		if(@$_POST["shipping"]==""){
			$sXML .= "<ShipmentServiceOptions/></Shipment></RatingServiceSelectionRequest>";
			if(trim($upsUser) != "" && trim($upsPw) != "")
				$success = UPSCalculate($sXML,$international,$shipping, $errormsg, $intShipping);
			else{
				$success = FALSE;
				$errormsg = "You must register with UPS by logging on to your online admin section and clicking the &quot;Register with UPS&quot; link before you can use the UPS OnLine&reg; Shipping Rates and Services Selection";
			}
			if($success){
				$totShipOptions = 0;
				for($indexmso=0; $indexmso<$maxshipoptions; $indexmso++){
					$shipRow = $intShipping[$indexmso];
					if($shipRow[3]==TRUE)
						$totShipOptions++;
				}
				$shipping = $intShipping[0][2];
				$shipMethod = $intShipping[0][0];
				$isstandardship = $intShipping[0][4];
				if($totShipOptions==1)
					$checkIntOptions=FALSE;
				elseif($totShipOptions == 0 && @$willpickuptext==""){
					$checkIntOptions = FALSE;
					$success=FALSE;
					$errormsg=$xxNoMeth;
				}
				if(@$willpickuptext != "") $checkIntOptions = TRUE;
			}
		}
	}elseif($shipType==6 && $somethingToShip){
		$checkIntOptions = (@$_POST["shipping"]=="");
		if(@$_POST["shipping"]==""){
			$sXML .= " </lineItems><city> </city> ";
			if($shipstate!="")
				$sXML .= "<provOrState> " . $shipstate . " </provOrState>";
			else{
				if($shipCountryCode=="US" || $shipCountryCode=="CA"){
					if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != "")
						$sXML .= "<provOrState> " . @$_POST["sstate2"] . " </provOrState>";
					else
						$sXML .= "<provOrState> " . @$_POST["state2"] . " </provOrState>";
				}else
					$sXML .= "<provOrState> </provOrState>";
			}
			$sXML .= "<country>" . $shipCountryCode . "</country><postalCode>" . $destZip . "</postalCode></ratesAndServicesRequest></eparcel>";
			$success = CanadaPostCalculate($sXML,$international,$shipping, $errormsg, $intShipping);
			if($success){
				$totShipOptions = 0;
				for($indexmso=0; $indexmso<$maxshipoptions; $indexmso++){
					$shipRow = $intShipping[$indexmso];
					if($shipRow[3]==TRUE)
						$totShipOptions++;
				}
				$shipping = $intShipping[0][2];
				$shipMethod = $intShipping[0][0];
				$isstandardship = $intShipping[0][4];
				if($totShipOptions==1)
					$checkIntOptions=FALSE;
				elseif($totShipOptions == 0 && @$willpickuptext==""){
					$checkIntOptions = FALSE;
					$success=FALSE;
					$errormsg=$xxNoMeth;
				}
				if(@$willpickuptext != "") $checkIntOptions = TRUE;
			}
		}
	}
	return($success);
}

if($stockManage != 0){
	$sSQL = "SELECT cartOrderID,cartID FROM cart WHERE (cartCustID=0 AND cartCompleted=0 AND cartOrderID=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()+(($dateadjust-$stockManage)*60*60)) . "')";
	if($delAfter != 0)
		$sSQL .= " OR (cartCompleted=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()-($delAfter*60*60*24)) . "')";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result)>0){
		$addcomma = "";
		$delstr="";
		$delcart="";
		while($rs = mysql_fetch_assoc($result)){
			$delcart .= $addcomma . $rs["cartOrderID"];
			$delstr .= $addcomma . $rs["cartID"];
			$addcomma = ",";
		}
		mysql_free_result($result);
	}
//Added June 2 2006 by Blake
//deletes wishlist older than $days_img_on_server_cust variable
} else {
	$addcomma = "";
	$delstr="";
	$delcart="";
}
$sSQL_cust = "SELECT cartOrderID,cartID FROM cart WHERE cartCustID>0 AND (cartCompleted=0 AND cartOrderID=0 AND cartDateAdded<'" . date("Y-m-d H:i:s",time()-(60*60*24*$days_delete_cust)) . "')";
$result_cust = mysql_query($sSQL_cust) or print(mysql_error());
if(mysql_num_rows($result_cust)>0){
	while($rs_cust = mysql_fetch_assoc($result_cust)){
		$delcart .= $addcomma . $rs_cust["cartOrderID"];
		$delstr .= $addcomma . $rs_cust["cartID"];
		$addcomma = ",";
	}
	mysql_free_result($result_cust);
}
if(!empty($delstr)){
	mysql_query("DELETE FROM cart WHERE cartID IN (" . $delstr . ")") or print(mysql_error());
	mysql_query("DELETE FROM cartoptions WHERE coCartID IN (" . $delstr . ")") or print(mysql_error());
}
if(!empty($delcart))
	if($delAfter != 0) mysql_query("DELETE FROM orders WHERE ordID IN (" . $delcart . ")") or print(mysql_error());
//end Added June 2 2006 by Blake

if(@$_GET["token"] != ''){
	$sSQL = "SELECT payProvDemo,payProvData1,payProvData2,payProvMethod FROM payprovider WHERE payProvEnabled=1 AND payProvID=18"; // Check for PayPal Payment Pro
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs = mysql_fetch_assoc($result)){
		$demomode = ((int)$rs["payProvDemo"]==1);
		$username = trim($rs["payProvData1"]);
		$ppmethod = (int)$rs["payProvMethod"];
		$data2arr = split("&",trim($rs["payProvData2"]));
		$password=urldecode(@$data2arr[0]);
		$sslcertpath=urldecode(@$data2arr[1]);
	}
	mysql_free_result($result);
	$sXML = ppsoapheader($username, $password) .
		'<soap:Body><GetExpressCheckoutDetailsReq xmlns="urn:ebay:api:PayPalAPI"><GetExpressCheckoutDetailsRequest><Version xmlns="urn:ebay:apis:eBLBaseComponents">1.00</Version>' .
		'  <Token>' . $_GET["token"] . '</Token>' .
		'</GetExpressCheckoutDetailsRequest></GetExpressCheckoutDetailsReq></soap:Body></soap:Envelope>';
	if($demomode) $sandbox = ".sandbox"; else $sandbox = "";
	if(callcurlfunction('https://api' . $sandbox . '.paypal.com/2.0/', $sXML, $res, $sslcertpath, $errormsg, FALSE)){
		$xmlDoc = new vrXMLDoc($res);
		$nodeList = $xmlDoc->nodeList->childNodes[0];
		$success=FALSE;
		$ordPhone = $ordEmail = $ordName='';
		$countryid=0;
		$ordShipName = '';
		$ordShipAddress = $ordShipAddress2 = '';
		$ordShipCity = '';
		$ordShipState = '';
		$ordShipZip = '';
		$ordShipCountry = '';
		$ordAffiliate = '';
		$ordAddInfo = '';
		$ordExtra1 = $ordExtra2 = '';
		$ordPayProvider = '19';
		$commercialloc = '';
		$wantinsurance = '';
		$ordComLoc = 0;
		$gotaddress = FALSE;
		$token = $_GET["token"];
		if(abs(@$addshippinginsurance)==1) $ordComLoc += 2;
		for($i = 0; $i < $nodeList->length; $i++){
			if($nodeList->nodeName[$i]=="SOAP-ENV:Body"){
				$e = $nodeList->childNodes[$i];
				for($j = 0; $j < $e->length; $j++){
					if($e->nodeName[$j] == "GetExpressCheckoutDetailsResponse"){
						$ee = $e->childNodes[$j];
						for($jj = 0; $jj < $ee->length; $jj++){
							if($ee->nodeName[$jj] == "Ack"){
								if($ee->nodeValue[$jj]=="Success")
									$success=TRUE;
							}elseif($ee->nodeName[$jj] == "GetExpressCheckoutDetailsResponseDetails"){
								$ff = $ee->childNodes[$jj];
								for($kk = 0; $kk < $ff->length; $kk++){
									if($ff->nodeName[$kk] == "PayerInfo"){
										$gg = $ff->childNodes[$kk];
										for($ll = 0; $ll < $gg->length; $ll++){
											if($gg->nodeName[$ll] == "Payer"){
												$ordEmail = $gg->nodeValue[$ll];
											}elseif($gg->nodeName[$ll] == "PayerID"){
												$payerid = $gg->nodeValue[$ll];
											}elseif($gg->nodeName[$ll] == "PayerName"){
												$hh = $gg->childNodes[$ll];
												for($mm = 0; $mm < $hh->length; $mm++){
													if($hh->nodeName[$mm] == "FirstName"){
														$ordName = $hh->nodeValue[$mm] . ($ordName!=''?' '.$ordName:$ordName);
													}elseif($hh->nodeName[$mm] == "LastName"){
														$ordName = ($ordName!=''?$ordName.' ':$ordName) . $hh->nodeValue[$mm];
													}
												}
											}elseif($gg->nodeName[$ll] == "Address"){
												$hh = $gg->childNodes[$ll];
												for($mm = 0; $mm < $hh->length; $mm++){
													if($hh->nodeName[$mm] == "Street1"){
														$ordAddress = $hh->nodeValue[$mm];
													}elseif($hh->nodeName[$mm] == "Street2"){
														$ordAddress2 = $hh->nodeValue[$mm];
													}elseif($hh->nodeName[$mm] == "CityName"){
														$ordCity = $hh->nodeValue[$mm];
													}elseif($hh->nodeName[$mm] == "StateOrProvince"){
														$ordState = $hh->nodeValue[$mm];
													}elseif($hh->nodeName[$mm] == "Country"){
														$sSQL = "SELECT countryName,countryID FROM countries WHERE countryCode='".mysql_real_escape_string($hh->nodeValue[$mm])."'";
														$result = mysql_query($sSQL) or print(mysql_error());
														if($rs = mysql_fetch_array($result)){
															$ordCountry = $rs["countryName"];
															$countryid = $rs["countryID"];
														}
													}elseif($hh->nodeName[$mm] == "PostalCode"){
														$ordZip = $hh->nodeValue[$mm];
													}elseif($hh->nodeName[$mm] == "AddressStatus"){
														$gotaddress = ($hh->nodeValue[$mm] != 'None');
													}
												}
											}
										}
									}elseif($ff->nodeName[$kk] == "Custom"){
										$customarr = split(':', $ff->nodeValue[$kk]);
										$thesessionid = $customarr[0];
										$ordAffiliate = $customarr[1];
										$cpncode = $customarr[2];
									}elseif($ff->nodeName[$kk] == "ContactPhone"){
										$ordPhone=$ff->nodeValue[$kk];
									}
								}
							}elseif($ee->nodeName[$jj] == "Errors"){
								$ff = $ee->childNodes[$jj];
								for($kk = 0; $kk < $ff->length; $kk++){
									if($ff->nodeName[$kk] == "ShortMessage"){
										$errormsg=$ff->nodeValue[$kk].'<br>'.$errormsg;
									}elseif($ff->nodeName[$kk] == "LongMessage"){
										$errormsg.=$ff->nodeValue[$kk];
									}elseif($ff->nodeName[$kk] == "ErrorCode"){
										$errcode=$ff->nodeValue[$kk];
									}
								}
							}
						}
					}
				}
			}
		}
		if(! $gotaddress)
			$ppexpresscancel=TRUE;
		elseif($success){
			$paypalexpress=TRUE;
			if($countryid==1 || $countryid==2){
				$sSQL = "SELECT stateAbbrev FROM states WHERE stateAbbrev='" . mysql_real_escape_string($ordState) . "'";
				$result = mysql_query($sSQL) or print(mysql_error());
				if($rs = mysql_fetch_array($result))
					$ordState=$rs["stateAbbrev"];
				mysql_free_result($result);
			}
		}else{
			print "PayPal Payment Pro error: " . $errormsg;
		}
	}else{
		print "PayPal Payment Pro error: " . $errormsg;
	}
}elseif(@$_POST["mode"]=="paypalexpress1"){
	$sSQL = "SELECT payProvDemo,payProvData1,payProvData2,payProvMethod FROM payprovider WHERE payProvEnabled=1 AND payProvID=18"; // Check for PayPal Payment Pro
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs = mysql_fetch_assoc($result)){
		$demomode = ((int)$rs["payProvDemo"]==1);
		$username = trim($rs["payProvData1"]);
		$ppmethod = (int)$rs["payProvMethod"];
		$data2arr = split("&",trim($rs["payProvData2"]));
		$password=urldecode(@$data2arr[0]);
		$sslcertpath=urldecode(@$data2arr[1]);
	}
	if($demomode) $sandbox = ".sandbox"; else $sandbox = "";
	if(@$pathtossl != ""){
		if(substr($pathtossl,-1) != "/") $storeurl = $pathtossl . "/"; else $storeurl = $pathtossl;
	}
	$sXML = ppsoapheader($username, $password) .
		'<soap:Body><SetExpressCheckoutReq xmlns="urn:ebay:api:PayPalAPI"><SetExpressCheckoutRequest><Version xmlns="urn:ebay:apis:eBLBaseComponents">1.00</Version>' .
		'  <SetExpressCheckoutRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">' .
		'    <OrderTotal currencyID="' . $countryCurrency . '">' . $_POST["estimate"] . '</OrderTotal>' .
		'    <ReturnURL>' . $storeurl . 'cart.php</ReturnURL>' .
		'    <CancelURL>' . $storeurl . 'cart.php</CancelURL>' .
		'    <Custom>' . $thesessionid . ':' . @$_POST["PARTNER"] . ':' . $_POST['cpncode'] . '</Custom>' .
		'    <PaymentAction>' . ($ppmethod==1?'Authorization':'Sale') . '</PaymentAction>' .
		'  </SetExpressCheckoutRequestDetails>' .
		'</SetExpressCheckoutRequest></SetExpressCheckoutReq></soap:Body></soap:Envelope>';
	if(callcurlfunction('https://api' . $sandbox . '.paypal.com/2.0/', $sXML, $res, $sslcertpath, $errormsg, FALSE)){
		$xmlDoc = new vrXMLDoc($res);
		$nodeList = $xmlDoc->nodeList->childNodes[0];
		$success=FALSE;
		$token='';
		for($i = 0; $i < $nodeList->length; $i++){
			if($nodeList->nodeName[$i]=="SOAP-ENV:Body"){
				$e = $nodeList->childNodes[$i];
				for($j = 0; $j < $e->length; $j++){
					if($e->nodeName[$j] == "SetExpressCheckoutResponse"){
						$ee = $e->childNodes[$j];
						for($jj = 0; $jj < $ee->length; $jj++){
							if($ee->nodeName[$jj] == "Ack"){
								if($ee->nodeValue[$jj]=="Success")
									$success=TRUE;
							}elseif($ee->nodeName[$jj] == "Token"){
								$token=$ee->nodeValue[$jj];
							}elseif($ee->nodeName[$jj] == "Errors"){
								$ff = $ee->childNodes[$jj];
								for($kk = 0; $kk < $ff->length; $kk++){
									if($ff->nodeName[$kk] == "ShortMessage"){
										$errormsg=$ff->nodeValue[$kk].'<br>'.$errormsg;
									}elseif($ff->nodeName[$kk] == "LongMessage"){
										$errormsg.=$ff->nodeValue[$kk];
									}elseif($ff->nodeName[$kk] == "ErrorCode"){
										$errcode=$ff->nodeValue[$kk];
									}
								}
							}
						}
					}
				}
			}
		}
		if($success){
			if(ob_get_length()===FALSE){
				print '<meta http-equiv="Refresh" content="0; URL=https://www'.$sandbox.'.paypal.com/webscr?cmd=_express-checkout&token=' . $token . '">';
			}else{
				header('Location: https://www'.$sandbox.'.paypal.com/webscr?cmd=_express-checkout&token=' . $token);
			}
			print '<p align="center">' . $xxAutFo . '</p>';
			print '<p align="center">' . $xxForAut . ' <a href="https://www'.$sandbox.'.paypal.com/webscr?cmd=_express-checkout&token=' . $token . '">' . $xxClkHere . '</a></p>';
		}else{
			print "PayPal Payment Pro error: " . $errormsg;
		}
	}else{
		print "PayPal Payment Pro error: " . $errormsg;
	}
}elseif(@$_POST["mode"]=="update"){
	//checkRelatedDiscounts();
	if(@$estimateshipping==TRUE) $_SESSION["xsshipping"] = "";
	if(@isset($_SESSION["discounts"])) $_SESSION["discounts"] = "";
	mysql_query("UPDATE orders SET ordTotal=0,ordShipping=0,ordStateTax=0,ordCountryTax=0,ordHSTTax=0,ordHandling=0,ordDiscount=0,ordDiscountText='' WHERE ordSessionID='" . session_id() . "' AND ordAuthNumber=''") or print(mysql_error());
	foreach(@$_POST as $objItem => $objValue){
		if(substr($objItem,0,5)=="quant"){
			$thecartid = (int)substr($objItem, 5);
			if((int)$objValue==0){
				$sSQL="DELETE FROM cartoptions WHERE coCartID='" . $thecartid . "'";
				mysql_query($sSQL) or print(mysql_error());
				$sSQL="DELETE FROM cart WHERE cartID='" . $thecartid . "'";
				mysql_query($sSQL) or print(mysql_error());
			}else{
				$totQuant = 0;
				$pPrice = 0;
				$pID = "";
				$sSQL="SELECT cartQuantity,cartAddProd,cartProdPrice,cartAltPrice,pInStock,pID,pSell,".$WSP."pPrice,pPricing_group FROM cart LEFT JOIN products ON cart.cartProdId=products.pID WHERE cartID='" . $thecartid . "'";
				$result = mysql_query($sSQL) or print(mysql_error());
				if($rs = mysql_fetch_array($result)){
					$pID = trim($rs["pID"]);
					$pInStock = (int)$rs["pInStock"];
					$pSell = (int)$rs["pSell"];
					//wholesale prices Added by Blake 6-6-06
					$pPrice_adj=1;
					if(@$_SESSION["clientUser"] != ""){
						if(($_SESSION["clientActions"] & 8) == 8){
							$pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs["pPricing_group"]);
						}
					}
					// Blake 12/20/06
					if($rs["cartAddProd"]=='1') $pPrice = $rs["cartProdPrice"];
					else $pPrice = $rs["pPrice"];
					//
					if($rs["cartAltPrice"]>=0)$pPrice = $rs["cartAltPrice"];
					else $pPrice = $pPrice*$pPrice_adj;
					$cartQuantity = (int)$rs["cartQuantity"];
					mysql_free_result($result);
					$sSQL = "SELECT SUM(cartQuantity) AS cartQuant FROM cart WHERE cartCompleted=0 AND cartCustID=0 AND cartProdID='" . $pID . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs = mysql_fetch_array($result))
						$totQuant = (int)$rs["cartQuant"];
				}
				mysql_free_result($result);
				if($pID != ''){
					if($stockManage != 0){
						$quantavailable = abs((int)$objValue);
						if(($pSell & 2) == 2){
							$hasalloptions=true;
							$sSQL = "SELECT coID,optStock,cartQuantity,coOptID,optExtend_shipping,optMin FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2";
							// ADDED by Chad - Fix to allow quantity change for custom screenz
							if(eregi("^[a-z]{1,3}-Custom$",$pID)) {
								$sSQL .= " OR optType=3";
							}
							// ADD ENDED
							$sSQL .= ") AND cartID='" . $thecartid . "'";
							$result = mysql_query($sSQL) or print(mysql_error());
							if(mysql_num_rows($result)>0){
								while($rs = mysql_fetch_assoc($result)){
									$pInStock = (int)$rs["optStock"]+1000;
									$actualpInStock = (int)$rs["optStock"];
									$extend_shipping = $rs["optExtend_shipping"];//extends shipping time, displayed in the cart
									$min = $rs["optMin"];//sets how many in stock above zero the extend_shipping is displayed in the cart
									$coID = $rs["coID"];
									$totQuant = 0;
									$cartQuantity = (int)$rs["cartQuantity"];
									$sSQL = "SELECT SUM(cartQuantity) AS cartQuant FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID WHERE cartCompleted=0 AND cartCustID=0 AND coOptID=" . $rs["coOptID"];
									$result2 = mysql_query($sSQL) or print(mysql_error());
									if($rs2 = mysql_fetch_assoc($result2))
										if(! is_null($rs2["cartQuant"])) $totQuant = (int)$rs2["cartQuant"];
									mysql_free_result($result2);
									if((int)($pInStock - $totQuant + $cartQuantity) < $quantavailable) $quantavailable = ($pInStock - $totQuant + $cartQuantity);
									if(($pInStock - $totQuant + $cartQuantity - abs((int)$objValue)) < 0) $hasalloptions=false;
									$extend='';
									//echo '<div style"position:absolute; z-index:100;>actual='.$actualpInStock.' total qty='.$totQuant. ' cart qty='.$cartQuantity.' min='. $min. ' new qty='.abs((int)$objValue).'</div>';
									if(($actualpInStock - $totQuant + $cartQuantity - $min - abs((int)$objValue)) < 0) {
										$extend = $extend_shipping;
									}
									$sql_co="UPDATE cartoptions SET coExtendShipping='$extend' WHERE coID=".$coID;
										mysql_query($sql_co);
								}
								$sSQL="UPDATE cart SET cartQuantity=" . $quantavailable . " WHERE cartID='" . $thecartid . "'";
								//echo $sSQL; exit();
								mysql_query($sSQL) or print(mysql_error());
								if(! $hasalloptions) $isInStock = false;
							}
							mysql_free_result($result);
						}else{
							if(($pInStock - $totQuant + $cartQuantity - $quantavailable) < 0){
								$quantavailable = ($pInStock - $totQuant + $cartQuantity);
								if($quantavailable < 0) $quantavailable=0;
								$isInStock = FALSE;
							}
							$sSQL="UPDATE cart SET cartQuantity=" . $quantavailable . " WHERE cartID='" . $thecartid . "'";
							//echo $sSQL; exit();
							mysql_query($sSQL) or print(mysql_error());
						}
					}else{
						$sSQL="UPDATE cart SET cartQuantity=" . abs((int)$objValue) . " WHERE cartID='" . $thecartid . "'";
						//echo $sSQL; exit();
						mysql_query($sSQL) or print(mysql_error());
					}
					//echo '<div>'.$pID.'='.$pPrice.'</div>';
					checkpricebreaks($pID,$pPrice);
					//checkRelatedDiscounts();
				}
			}
		}elseif(substr($objItem,0,5)=="delet"){
			$sSQL="DELETE FROM cart WHERE cartID='" . (int)substr($objItem, 5) . "'";
			mysql_query($sSQL) or print(mysql_error());
			$sSQL="DELETE FROM cartoptions WHERE coCartID='" . (int)substr($objItem, 5) . "'";
			mysql_query($sSQL) or print(mysql_error());
			checkpricebreaks($pID,$pPrice);
			//checkRelatedDiscounts();
		}
	}
}
if(@$_POST["mode"]=="add"){
	if(@$estimateshipping==TRUE) $_SESSION["xsshipping"] = "";
	if(@isset($_SESSION["discounts"])) $_SESSION["discounts"] = "";
	mysql_query("UPDATE orders SET ordTotal=0,ordShipping=0,ordStateTax=0,ordCountryTax=0,ordHSTTax=0,ordHandling=0,ordDiscount=0,ordDiscountText='' WHERE ordSessionID='" . session_id() . "' AND ordAuthNumber=''") or print(mysql_error());
	$bExists = FALSE;
	if(trim(@$_POST["frompage"])!="") $_SESSION["frompage"]=$_POST["frompage"]; else $_SESSION["frompage"]="";
	if(@$_POST["quant"]=="" || ! is_numeric(@$_POST["quant"]))
		$quantity=1;
	else
		$quantity=abs((int)@$_POST["quant"]);
	foreach(@$_POST as $objItem => $objValue){ // Check if the product id is modified
		if(substr($objItem,0,4)=="optn"){
			$sSQL="SELECT optRegExp FROM options WHERE optID='" . mysql_real_escape_string($objValue) . "'";
			$result2 = mysql_query($sSQL) or print(mysql_error());
			$rs=mysql_fetch_assoc($result2);
			$theexp = trim($rs["optRegExp"]);
			if($theexp != "" && substr($theexp, 0, 1) != "!"){
				$theexp = str_replace('%s', $theid, $theexp);
				if(strpos($theexp, " ") !== FALSE){ // Search and replace
					$exparr = split(" ", $theexp, 2);
					$theid = str_replace($exparr[0], $exparr[1], $theid);
				}else
					$theid = $theexp;
			}
			mysql_free_result($result2);
		}
		if(! $bExists) break;
	}
	$sSQL = "SELECT cartID FROM cart WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartProdID='" . $theid . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_assoc($result)){
		$bExists = TRUE;
		$cartID = $rs["cartID"];
		foreach(@$_POST as $objItem => $objValue){ // We have the product. Check we have all the same options
			if(substr($objItem,0,4)=="optn"){
				if(@$_POST["v" . $objItem] != ""){
					$sSQL="SELECT coID FROM cartoptions WHERE coCartID=" . $cartID . " AND coOptID='" . mysql_real_escape_string($objValue) . "' AND coCartOption='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["v" . $objItem]))) . "'";
					$result2 = mysql_query($sSQL) or print(mysql_error());
					if(mysql_num_rows($result2)==0) $bExists=FALSE;
					mysql_free_result($result2);
				}else{
					$sSQL="SELECT coID FROM cartoptions WHERE coCartID=" . $cartID . " AND coOptID='" . mysql_real_escape_string($objValue) . "'";
					$result2 = mysql_query($sSQL) or print(mysql_error());
					if(mysql_num_rows($result2)==0) $bExists=FALSE;
					mysql_free_result($result2);
				}
			}
			if(! $bExists) break;
		}
		if($bExists) break;
	}
	mysql_free_result($result);
	$sSQL = "SELECT ".getlangid("pName",1).",".$WSP."pPrice,pInStock,pWeight,pSell,pPricing_group FROM products WHERE pID='" . $theid . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(! ($rsStock = mysql_fetch_array($result))){
		$rsStock[getlangid("pName",1)]=$theid;
		$stockManage=0;
		$isInStock=FALSE;
		$outofstockreason=2;
	}
	mysql_free_result($result);
	if($stockManage != 0){
		if(($rsStock["pSell"] & 2)==2){
			$isInStock = true;
			foreach(@$_POST as $objItem => $objValue){
				if(substr($objItem,0,4)=="optn"){
					$sSQL="SELECT optStock FROM options INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND optID='" . mysql_real_escape_string($objValue) . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs = mysql_fetch_array($result))
						$isInStock = ($isInStock && ($rs["optStock"]+1000 >= $quantity));
					mysql_free_result($result);
				}
			}
			if($isInStock){ // Check cart
				$bestDate = time()+(60*60*24*62);
				foreach(@$_POST as $objItem => $objValue){
					$totQuant = 0;
					$stockQuant = 0;
					if(substr($objItem,0,4)=="optn"){
						$sSQL = "SELECT cartQuantity,cartDateAdded,cartOrderID,optStock,optExtend_shipping,optMin,coID  FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND cartCompleted=0 AND cartCustID=0 AND coOptID='" . mysql_real_escape_string($objValue) . "'";
						$result = mysql_query($sSQL) or print(mysql_error());
						$extend_shipping_out='';
						if(mysql_num_rows($result)>0){
							$rs = mysql_fetch_array($result);
							$stockQuant = $rs["optStock"]+1000;//adds 1000 to make it never out of stock
							do{
								$totQuant += $rs["cartQuantity"];
								if((int)$rs["cartOrderID"]==0 && strtotime($rs["cartDateAdded"]) < $bestDate) $bestDate = strtotime($rs["cartDateAdded"]);
							}while($rs = mysql_fetch_array($result));
							if(($totQuant+$quantity) > $stockQuant){
								$isInStock=false;
								$outofstockreason=1;
							}
						}
						mysql_free_result($result);
					}
				}
			}
		}else{
			if($isInStock = (($rsStock["pInStock"]-$quantity) >= 0)){ // Check cart
				$totQuant = 0;
				$bestDate = time()+(60*60*24*62);
				$sSQL = "SELECT cartQuantity,cartDateAdded,cartOrderID FROM cart WHERE cartCompleted=0 AND cartCustID=0 AND cartProdID='" . $theid . "'";
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs = mysql_fetch_array($result)){
					$totQuant += $rs["cartQuantity"];
					if((int)$rs["cartOrderID"]==0 && strtotime($rs["cartDateAdded"]) < $bestDate) $bestDate = strtotime($rs["cartDateAdded"]);
				}
				mysql_free_result($result);
				if(($rsStock["pInStock"]-($totQuant+$quantity)) < 0){
					$isInStock = FALSE;
					$outofstockreason=1;
				}
			}
		}
	}
	//check to see if quanity is less than min level added by Blake April 3, 2006
	$extend_shipping_out='';
	foreach(@$_POST as $objItem => $objValue){
		if(substr($objItem,0,4)=="optn"){
			if(trim(@$_POST["v" . $objItem])==""){
				$sSQL = "SELECT cartQuantity,cartDateAdded,cartOrderID,optStock,optExtend_shipping,optMin,coID, optName FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND cartCompleted=0 AND coOptID='" . mysql_real_escape_string($objValue) . "'";
				//echo $sSQL;
				$result = mysql_query($sSQL) or print(mysql_error());
				if(mysql_num_rows($result)>0){
					$rs = mysql_fetch_array($result);
					$stockQuant = $rs["optStock"]+1000;//adds 100 to make it never out of stock
					$actualstockQuant = $rs["optStock"];//this is the actual stock available
					$extend_shipping = $rs["optExtend_shipping"];//extends shipping time, displayed in the cart
					$min = $rs["optMin"];//sets how many in stock above zero the extend_shipping is displayed in the cart
					$coID = $rs["coID"];
					$optname = $rs["optName"];
					do{
						$totQuant += $rs["cartQuantity"];
						if((int)$rs["cartOrderID"]==0 && strtotime($rs["cartDateAdded"]) < $bestDate) $bestDate = strtotime($rs["cartDateAdded"]);
					}while($rs = mysql_fetch_array($result));
					// blake
					//echo '<!-- total qty='.$totQuant.' new qty='.$quantity.' min='.$min.' ='.$actualstockQuant.' name='.$optname." -->";
					if(($totQuant+$quantity+$min)> $actualstockQuant){
						$sql_co="UPDATE cartoptions SET coExtendShipping='$extend_shipping' WHERE coCartID=".$cartID." AND coOptID=".$objValue;
						$extend_shipping_out[$objValue]=$extend_shipping;
						//echo '<br />Update='.$sql_co;
						mysql_query($sql_co);
					}
				} else {
					$sSQL="SELECT optStock,optExtend_shipping,optMin,optExtend_shipping,optName FROM options INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND optID='" . mysql_real_escape_string($objValue) . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs3 = mysql_fetch_array($result)) {
						if($quantity+$rs3["optMin"] > $rs3["optStock"]) {
							//echo $rs3["optName"].' qty='.$quantity.' min='.$rs3["optMin"].'='.$rs3["optStock"];
							$extend_shipping_out[$objValue]=$rs3["optExtend_shipping"];
							//echo '<br />first option in cart. Shipping='.$extend_shipping_out[$objValue];
						}
					}
				}
				mysql_free_result($result);
			}
		}
	}//echo '<br>hi'.getPricingAdj($_SESSION['custid'],$quantity,$rsStock[pPricing_group]);exit();
	//end added
	if($isInStock){
		//wholesale prices Added by Blake 6-6-06
		$pPrice_adj=1;
		if(@$_SESSION["clientUser"] != ""){
			if(($_SESSION["clientActions"] & 8) == 8){
				$pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rsStock["pPricing_group"]);
			}
		}
		// Add Ended
		if($bExists){
			$sSQL = "UPDATE cart SET cartQuantity=cartQuantity+" . $quantity . " WHERE cartID=" . $cartID;
			mysql_query($sSQL) or print(mysql_error());
		}else{
			$sSQL = "INSERT INTO cart (cartSessionID,cartProdID,cartQuantity,cartCompleted,cartProdName,cartProdPrice,cartOrderID,cartDateAdded) VALUES (";
			$sSQL .= "'" . session_id() . "',";
			$sSQL .= "'" . $theid . "',";
			$sSQL .= $quantity . ",";
			$sSQL .= "0,";
			$sSQL .= "'" . mysql_real_escape_string($rsStock[getlangid("pName",1)]) . "',";
			$sSQL .= "'" . $rsStock["pPrice"]*$pPrice_adj . "',";
			$sSQL .= "0,";
			$sSQL .= "'" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "')";
			mysql_query($sSQL) or print(mysql_error());
			$cartID = mysql_insert_id();
			foreach(@$_POST as $objItem => $objValue){
				if(substr($objItem,0,4)=="optn"){
					if(trim(@$_POST["v" . $objItem])==""){
						$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)."," . $OWSP . "optPriceDiff,optWeightDiff,optType,optFlags FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($objValue) . "'";
						$result = mysql_query($sSQL) or print(mysql_error());
						if($rs = mysql_fetch_array($result)){
							if(abs($rs["optType"]) != 3){
								$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coExtendShipping,coPriceDiff,coWeightDiff) VALUES (" . $cartID . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string($rs[getlangid("optName",32)]) ."','" . $extend_shipping_out[$objValue] . "',";
								if(($rs["optFlags"]&1)==0) $sSQL .= $rs["optPriceDiff"] . ","; else $sSQL .= round(($rs["optPriceDiff"] * $rsStock["pPrice"])/100.0, 2) . ",";
								if(($rs["optFlags"]&2)==0) $sSQL .= $rs["optWeightDiff"] . ")"; else $sSQL .= multShipWeight($rsStock["pWeight"],$rs["optWeightDiff"]) . ")";
								//echo $sSQL;
							}else
								$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartID . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','',0,0)";
							mysql_query($sSQL) or print(mysql_error());
						}
						mysql_free_result($result);
					}else{
						$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)." FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($objValue) . "'";
						$result = mysql_query($sSQL) or print(mysql_error());
						$rs = mysql_fetch_array($result);
						$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartID . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string(unstripslashes(trim(@$_POST["v" . $objItem]))) . "',0,0)";
						mysql_query($sSQL) or print(mysql_error());
						mysql_free_result($result);
					}
				}
			}
		}
		checkpricebreaks($theid,$rsStock["pPrice"]*$pPrice_adj);
		//checkRelatedDiscounts();
		if(trim(@$_POST["frompage"])!="" && @$actionaftercart==3)
			print '<meta http-equiv="Refresh" content="3; URL=' . trim(@$_POST["frompage"]) . '">';
		elseif(@$actionaftercart==4){
			if(ob_get_length()===FALSE) print '<meta http-equiv="Refresh" content="0; URL=cart.php">'; else header('Location: '.$storeurl.'cart.php');
		}else
			print '<meta http-equiv="Refresh" content="3; URL=cart.php">';
?>
      <table border="0" cellspacing="<?php print $maintablespacing?>" cellpadding="<?php print $maintablepadding?>" width="<?php print $maintablewidth?>" bgcolor="<?php print $maintablebg?>" align="center">
        <tr>
          <td width="100%" align="center">
            <table width="<?php print $innertablewidth?>" border="0" cellspacing="<?php print $innertablespacing?>" cellpadding="<?php print $innertablepadding?>" bgcolor="<?php print $innertablebg?>">
			  <tr>
			    <td align="center"><p>&nbsp;</p>
<?php		print '<p style=" font-size:16px;">' . $quantity . ' <strong>' . $rsStock[getlangid("pName",1)] . '</strong> ' . $xxAddOrd . '</p>';
			print '<p>' . $xxPlsWait . ' <a href="';
			if(trim(@$_POST["frompage"])!="" && @$actionaftercart==3) print trim(@$_POST["frompage"]); else print 'cart.php';
			print '"><strong>' . $xxClkHere . '</strong></a>.</p>'; ?>
				<p><a href="/cart.php"><img src="/lib/images/viewcart.gif" alt="Checkout Here" border="0" /></a></p>
				<p>&nbsp;</p>
				</td>
			  </tr>
			</table>
		  </td>
        </tr>
      </table>
<?php
	}else{
?>
      <table border="0" cellspacing="<?php print $maintablespacing?>" cellpadding="<?php print $maintablepadding?>" width="<?php print $maintablewidth?>" bgcolor="<?php print $maintablebg?>" align="center">
        <tr>
          <td width="100%" align="center">
            <table width="<?php print $innertablewidth?>" border="0" cellspacing="<?php print $innertablespacing?>" cellpadding="<?php print $innertablepadding?>" bgcolor="<?php print $innertablebg?>">
			  <tr>
			    <td align="center"><p>&nbsp;</p>
				<?php print "<p>" . $xxSrryItm . " <strong>" . $rsStock[getlangid("pName",1)] . "</strong> " . $xxIsCntly;
				if($outofstockreason==1) print " " . $xxTemprly;
				if($outofstockreason==2) print ' not available in our product database.'; else print " " . $xxOutStck . "</p>";
				if($outofstockreason==1){
					print "<p>" . $xxNotChOu . " ";
					$bestDate += $stockManage*(60*60);
					$totMins = (int)($bestDate - (time()+($dateadjust*60*60)));
					$totMins = (int)($totMins / 60)+1;
					if($totMins > 300)
						print $xxShrtWhl;
					else{
						if($totMins >= 60) print (int)($totMins / 60) . " hour";
						if($totMins >= 120) print "s";
						$totMins -= ((int)($totMins / 60) * 60);
						if($totMins > 0) print " " . $totMins . " minute";
						if($totMins > 1) print "s";
					}
					print $xxChkBack . "</p>";
				} ?>
				<p><?php print $xxPlease?> <a href="javascript:history.go(-1)"><strong><?php print $xxClkHere?></strong></a> <?php print $xxToRetrn?></p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				</td>
			  </tr>
			</table>
		  </td>
        </tr>
      </table>
<?php
	}
}elseif(@$_POST["mode"]=="checkout" || $ppexpresscancel || @$_POST["mode"]=="edit"){
	$remember=FALSE;
	$havestate=FALSE;
	if(@$_POST["checktmplogin"]=="1"){
		$sSQL = "SELECT tmploginname FROM tmplogin WHERE tmploginid='" . mysql_real_escape_string(trim(@$_POST["sessionid"])) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$_SESSION["clientUser"]=$rs["tmploginname"];
			mysql_free_result($result);
			mysql_query("DELETE FROM tmplogin WHERE tmploginid='" . mysql_real_escape_string(trim(@$_POST["sessionid"])) . "'") or print(mysql_error());
			$sSQL = "SELECT clientActions,clientLoginLevel FROM clientlogin WHERE clientUser='" . mysql_real_escape_string($_SESSION["clientUser"]) . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result)){
				$_SESSION["clientActions"]=$rs["clientActions"];
				$_SESSION["clientLoginLevel"]=$rs["clientLoginLevel"];
			}
		}
		mysql_free_result($result);
	}
	if(@$_COOKIE["id1"] != "" && @$_COOKIE["id2"] != "" && $_POST['mode']!="edit"){
		$sSQL = "SELECT ordName,ordAddress,ordAddress2,ordCity,ordState,ordZip,ordCountry,ordEmail,ordPhone,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipZip,ordShipCountry,ordPayProvider,ordComLoc,ordExtra1,ordExtra2,ordAddInfo,ordPoApo,ordShipPoApo,ordHowFound FROM orders WHERE ordID='" . mysql_real_escape_string(unstripslashes($_COOKIE["id1"])) . "' AND ordSessionID='" . mysql_real_escape_string(unstripslashes($_COOKIE["id2"])) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$ordName = $rs["ordName"];
			$ordPoApo = $rs["ordPoApo"];
			$ordHowFound = $rs["ordHowFound"];
			$ordAddress = $rs["ordAddress"];
			$ordAddress2 = $rs["ordAddress2"];
			$ordCity = $rs["ordCity"];
			$ordState = $rs["ordState"];
			$ordZip = $rs["ordZip"];
			$ordCountry = $rs["ordCountry"];
			$ordEmail = $rs["ordEmail"];
			$ordPhone = $rs["ordPhone"];
			//$ordShipName = $rs["ordShipName"];
			//$ordShipPoApo = $rs["ordShipPoApo"];
			//$ordShipAddress = $rs["ordShipAddress"];
			//$ordShipAddress2 = $rs["ordShipAddress2"];
			//$ordShipCity = $rs["ordShipCity"];
			//$ordShipState = $rs["ordShipState"];
			//$ordShipZip = $rs["ordShipZip"];
			//$ordShipCountry = $rs["ordShipCountry"];
			$ordPayProvider = $rs["ordPayProvider"];
			$ordComLoc = $rs["ordComLoc"];
			$ordExtra1 = $rs["ordExtra1"];
			$ordExtra2 = $rs["ordExtra2"];
			//$ordAddInfo = $rs["ordAddInfo"];
			$remember=TRUE;
		}
		mysql_free_result($result);
	} else {
		$ordName = $_POST["name"];
		$ordPoApo = $_POST["poapo"];
		$ordHowFound = $_POST["howfound"];
		$ordAddress = $_POST["address"];
		$ordAddress2 = $_POST["address2"];
		$ordCity = $_POST["city"];
		$ordState = $_POST["state"];
		$ordZip = $_POST["zip"];
		$ordCountry = $_POST["country"];
		$ordEmail = $_POST["email"];
		$ordPhone = $_POST["phone"];
		$ordShipName = $_POST["sname"];
		$ordShipPoApo = $_POST["spoapo"];
		$ordShipAddress = $_POST["saddress"];
		$ordShipAddress2 = $_POST["saddress2"];
		$ordShipCity = $_POST["scity"];
		$ordShipState = $_POST["sstate"];
		$ordShipZip = $_POST["szip"];
		$ordShipCountry = $_POST["scountry"];
		$ordPayProvider = $_POST["payprovider"];
		$ordComLoc = $_POST["commercialloc"];
		$ordExtra1 = $_POST["orderextra1"];
		$ordExtra2 = $_POST["orderextra2"];
		$ordAddInfo = $_POST["ordAddInfo"];
		if($_POST["remember"]==1)$remember=TRUE;
	}
	if(! $remember && $_POST['mode']!="edit"){
		$ordState = @$_SESSION["state"];
		$ordCountry = @$_SESSION["country"];
		$ordZip = @$_SESSION["zip"];
	}
	$sSQL = "SELECT stateAbbrev FROM states WHERE stateEnabled=1 ORDER BY stateAbbrev";
	$result = mysql_query($sSQL) or print(mysql_error());
	$numallstates=0;
	$numallcountries=0;
	while($rs = mysql_fetch_array($result))
		$allstates[$numallstates++]=$rs;
	mysql_free_result($result);
	$numhomecountries = 0;
	$nonhomecountries = 0;
	$sSQL = "SELECT countryName,countryOrder,".getlangid("countryName",8)." FROM countries WHERE countryEnabled=1 ORDER BY countryOrder DESC," . getlangid("countryName",8);
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_array($result)){
		$allcountries[$numallcountries++]=$rs;
		if($rs["countryOrder"]==2)$numhomecountries++;else $nonhomecountries++;
	}
	mysql_free_result($result);
?>
<?php include(APPPATH.'views/pages/admin/cartinc.php'); ?>
<script language="JavaScript" type="text/javascript">
// <![CDATA[
function showDetails() {
	var aDetails = document.getElementsByClassName("shippingDetails");
	for(i=0; i<aDetails.length; i++) {
		aDetails[i].style.display = "";
		new Effect.Highlight(aDetails[i]);
	}

	$('sname').value = '<?=$ordShipName?>';
	//$('spoapo').value = '<?=$ordShipPoApo?>';
	$('saddress').value = '<?=$ordShipAddress?>';
	$('saddress2').value = '<?=$ordShipAddress2?>';
	$('scity').value = '<?=$ordShipCity?>';
	for(j=0; j<$('sstate').options.length; j++) {
		if($('sstate').options[j].value == '<?=$ordShipState?>') {
			$('sstate').selectedIndex = j;
		}
	}
	for(j=0; j<$('scountry').options.length; j++) {
		if($('scountry').options[j].value == '<?=$ordShipCountry?>') {
			$('scountry').selectedIndex = j;
		}
	}
	$('szip').value = '<?=$ordShipZip?>';

	$("showShipping").style.display = "none";
}
// ]]>
</script>
<script language="JavaScript" type="text/javascript">
<!--
var checkedfullname=false;
var numhomecountries=0,nonhomecountries=0;
function checkform(frm)
{
<?php if(trim(@$extraorderfield1)!="" && @$extraorderfield1required==TRUE){ ?>
if(frm.ordextra1.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $extraorderfield1?>\".");
	frm.ordextra1.focus();
	return (false);
}
<?php } ?>
if(frm.name.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $xxName?>\".");
	frm.name.focus();
	return (false);
}
gotspace=false;
var checkStr = frm.name.value;
for (i = 0; i < checkStr.length; i++){
	if(checkStr.charAt(i)==" ")
		gotspace=true;
}
if(!checkedfullname && !gotspace){
	alert("<?php print $xxFulNam?> \"<?php print $xxName?>\".");
	frm.name.focus();
	checkedfullname=true;
	return (false);
}
if(frm.email.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $xxEmail?>\".");
	frm.email.focus();
	return (false);
}
validemail=0;
var checkStr = frm.email.value;
for (i = 0; i < checkStr.length; i++){
	if(checkStr.charAt(i)=="@")
		validemail |= 1;
	if(checkStr.charAt(i)==".")
		validemail |= 2;
}
if(validemail != 3){
	alert("<?php print $xxValEm?>");
	frm.email.focus();
	return (false);
}
if(frm.address.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $xxAddress?>\".");
	frm.address.focus();
	return (false);
}
if(frm.city.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $xxCity?>\".");
	frm.city.focus();
	return (false);
}
if($F('state')=='AA' || $F('state')=='AE' || $F('state')=='AP') {
	if(!$F('poapo')) {
		alert("Please check the PO/APO box.");
		frm.poapo.focus();
		return (false);
	}
}
if(frm.country.selectedIndex < numhomecountries){
<?php	if($numallstates>0){ ?>
	if(frm.state.selectedIndex==0){
		alert("<?php print $xxPlsSlct . " " . $xxState?>");
		frm.state.focus();
		return (false);
	}
<?php	} ?>
}else{
<?php	if($nonhomecountries>0){ ?>
	if(frm.state2.value==""){
		alert("<?php print $xxPlsEntr?> \"<?php print str_replace("<br />"," ",$xxNonState)?>\".");
		frm.state2.focus();
		return (false);
	}
<?php	} ?>}
if($F('country') != 'United States of America' && $('poapo').checked) {
	$('poapo').checked = false;
}
if(frm.zip.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $xxZip?>\".");
	frm.zip.focus();
	return (false);
}
if(frm.phone.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $xxPhone?>\".");
	frm.phone.focus();
	return (false);
}
<?php if(trim(@$extraorderfield2)!="" && @$extraorderfield2required==TRUE){ ?>
if(frm.ordextra2.value==""){
	alert("<?php print $xxPlsEntr?> \"<?php print $extraorderfield2?>\".");
	frm.ordextra2.focus();
	return (false);
}
<?php } ?>
<?php if(@$noshipaddress!=TRUE){ ?>
if(frm.saddress.value!="" || frm.saddress2.value!=""){ // This means they've entered shipping details
	if(frm.sname.value==""){
		alert("<?php print $xxShpDtls?>\n\n<?php print $xxPlsEntr?> \"<?php print $xxName?>\".");
		frm.sname.focus();
		return (false);
	}
	if(frm.scity.value==""){
		alert("<?php print $xxShpDtls?>\n\n<?php print $xxPlsEntr?> \"<?php print $xxCity?>\".");
		frm.scity.focus();
		return (false);
	}
	if($F('sstate')=='AE' || $F('sstate')=='AA' || $F('sstate')=='AP') {
		if(!$F('spoapo')) {
			alert("Please check the PO/APO box under the SHIPPING details.");
			frm.spoapo.focus();
			return (false);
		}
	}
	if(frm.scountry.selectedIndex < numhomecountries){
<?php	if($numallstates>0){ ?>
		if(frm.sstate.selectedIndex==0){
			alert("<?php print $xxShpDtls?>\n\n<?php print $xxPlsSlct . " " . $xxState?>.");
			frm.sstate.focus();
			return (false);
		}
<?php	} ?>
	}else{
<?php	if($nonhomecountries>0){ ?>
		if(frm.sstate2.value==""){
			alert("<?php print $xxShpDtls?>\n\n<?php print $xxPlsEntr?> \"<?php print str_replace("<br />"," ",$xxNonState)?>\".");
			frm.sstate2.focus();
			return (false);
		}
<?php	} ?>
	}
	if($F('scountry') != 'United States of America' && $('spoapo').checked) {
		$('spoapo').checked = false;
	}
	if(frm.szip.value==""){
		alert("<?php print $xxShpDtls?>\n\n<?php print $xxPlsEntr?> \"<?php print $xxZip?>\".");
		frm.szip.focus();
		return (false);
	}
	if(frm.saddress.value=="") {
		alert("<?php print $xxShpDtls?>\n\n<?php print $xxPlsEntr?> \"<?php print $xxAddress?>\".");
		frm.saddress.focus();
		return (false);
	}
}
<?php }
if($_SERVER['REMOTE_ADDR'] != '70.97.63.5' && $_SERVER['REMOTE_ADDR'] != '65.103.241.197') {
?>
if(frm.remember.checked==false){
	if(confirm("<?php print $xxWntRem?>")){
		frm.remember.checked=true
	}
}
<?php
}
if(@$termsandconditions==TRUE){ ?>
if(frm.license.checked==false){
	alert("<?php print $xxPlsProc?>");
	frm.license.focus();
	return (false);
}
<?php } ?>



return (true);
} // End of checkform

<?php if(@$termsandconditions==TRUE){ ?>
function showtermsandconds(){
newwin=window.open("policy.php?cart=no","Terms","menubar=no, scrollbars=yes, width=950, height=580, directories=no,location=no,resizable=yes,status=no,toolbar=no");
}
<?php } ?>
var savestate=0;
var ssavestate=0;
function dosavestate(shp){
	thestate = eval('document.forms.mainform.'+shp+'state');
	eval(shp+'savestate = thestate.selectedIndex');
}
function checkNonState(isShipping) {
	if($F('sstate') != "") {
		if(isShipping != "") {
			var hide = document.getElementsByClassName('sstate2Hide');
			for(i=0; i<hide.length; i++) {
				$(hide[i]).style.visibility = "hidden";
				$('sstate2').value = "";
			}
		}else{
			var hide = document.getElementsByClassName('state2Hide');
			for(i=0; i<hide.length; i++) {
				$(hide[i]).style.visibility = "hidden";
				$('state2').value = "";
			}
		}
	}else{
		if(isShipping != "") {
			var hide = document.getElementsByClassName('sstate2Hide');
			for(i=0; i<hide.length; i++) {
				$(hide[i]).style.visibility = "visible";
			}
		}else{
			var hide = document.getElementsByClassName('state2Hide');
			for(i=0; i<hide.length; i++) {
				$(hide[i]).style.visibility = "visible";
			}
		}
	}
}
function checkoutspan(shp){
if(shp=='s' && document.getElementById('saddress').value=="")visib='hidden';else visib='visible';<?php
if($nonhomecountries>0) print "thestyle = document.getElementById(shp+'outspan').style;\r\n";
if($numallstates>0){
	print "theddstyle = document.getElementById(shp+'outspandd').style;\r\n";
	print "thestate = eval('document.forms.mainform.'+shp+'state');\r\n";
} ?>
thecntry = eval('document.forms.mainform.'+shp+'country');
if(thecntry.selectedIndex < numhomecountries){<?php
if($nonhomecountries>0) print "thestyle.visibility='hidden';\r\n";
if($numallstates>0){
	print "theddstyle.visibility=visib;\r\n";
	print "thestate.disabled=false;\r\n";
	print "eval('thestate.selectedIndex='+shp+'savestate');\r\n";
} ?>
}else{<?php
if($nonhomecountries>0) print "thestyle.visibility=visib;\r\n";
if($numallstates>0){ ?>
theddstyle.visibility="hidden";
if(thestate.disabled==false){
thestate.disabled=true;
eval(shp+'savestate = thestate.selectedIndex');
thestate.selectedIndex=0;}
<?php	} ?>
}}
//-->
</script>
<?
// splits the cart page into different funnels
$google_tracker="/cart/customer_info.php";
// end
?>
<div id="cart_nav"><img  src="/lib/images/new_images/subnav_gray_13.gif" alt="View Order" /><img src="/lib/images/new_images/subnav_green_15.gif" alt="Customer Info" /><img src="/lib/images/new_images/subnav_gray_17.gif" alt="Final Review" /><img src="/lib/images/new_images/subnav_gray_19.gif" alt="Confirmation" /><img src="/lib/images/new_images/subnav_gray_21.gif" alt="View Receipt" /></div>

	  <table border="0" cellspacing="<?php print $maintablespacing?>" cellpadding="<?php print $maintablepadding?>" width="<?php print $maintablewidth?>" bgcolor="<?php print $maintablebg?>" align="center">
        <tr>
          <td width="100%">
		    <form method="post" name="mainform" action="cart.php" onsubmit="return checkform(this)">
			  <input type="hidden" name="mode" value="go" />
			  <input type="hidden" name="sessionid" value="<?php print $thesessionid?>" />
			  <input type="hidden" name="PARTNER" value="<?php print @$_POST["PARTNER"]?>" />
			  <table width="<?php print $innertablewidth?>" border="0" cellspacing="<?php print $innertablespacing?>" cellpadding="<?php print $innertablepadding?>" bgcolor="<?php print $innertablebg?>">
				<tr>
				  <td align="center" colspan="4"><strong><?php print $xxCstDtl?></strong></td>
				</tr>
<?php	if(trim(@$extraorderfield1)!=""){ ?>
				<tr>
				  <td align="right"><strong><?php if(@$extraorderfield1required==TRUE) print "<font color='#FF0000'>*</font>";
									print $extraorderfield1 ?>:</strong></td>
				  <td colspan="3"><?php if(@$extraorderfield1html != "")print $extraorderfield1html; else print '<input type="text" name="ordextra1" size="' . atb(20) . '" value="' . @$ordExtra1 . '" />'?></td>
				</tr>
<?php	} ?>
				<tr>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxName?>:</strong></td>
				  <td align="left"><input type="text" name="name" size="<?php print atb(20)?>" value="<?php print @$ordName?>" /></td>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxEmail?>:</strong></td>
				  <td align="left"><input type="text" name="email" size="<?php print atb(20)?>" value="<?php print @$ordEmail?>" /></td>
				</tr>
				<tr>
				  <td align="right"><strong><?php print $xxPOAPO?>:</strong></td>
				  <td align="left" ><input type="checkbox" id="poapo" name="poapo" value="1" <?php if((@$ordPoApo&1)==1) print "checked"?> /> - <em>APOs are sent Priority Postal Service</em></td>
				  <td>&nbsp;</td>
				  <td align="left" style="font-size:11px; font-style:italic;">*AOL emails may be blocked by AOL.</td>

				</tr>
				<tr>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxAddress?>:</strong></td>
				  <td align="left"<?php if(@$useaddressline2==TRUE) print ' colspan="3"'?>><input type="text" name="address" id="address" maxlength="35" size="<?php print atb(25)?>" value="<?php print @$ordAddress?>" /></td>
<?php	if(@$useaddressline2==TRUE){ ?>
				</tr>
				<tr>
				  <td align="right"><strong><?php print $xxAddress2?>:</strong></td>
				  <td align="left"><input type="text" id="address2" name="address2" maxlength="35" size="<?php print atb(25)?>" value="<?php print @$ordAddress2?>" /></td>
<?php	} ?>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxCity?>:</strong></td>
				  <td align="left"><input type="text" name="city" size="<?php print atb(20)?>" value="<?php print @$ordCity?>" /></td>
				</tr>
<?php	if($numallstates>0 || $nonhomecountries != 0){ ?>
				<tr>
<?php		if($numallstates>0){ ?>
				  <td align="right"><strong><font color='#FF0000'><span id="outspandd" style="visibility:hidden">*</span></font><?php print $xxState?>:</strong></td>
				  <td align="left"><select id="state" name="state" size="1" onchange="dosavestate('')"><?php $havestate = show_states(@$ordState) ?></select></td>
<?php		}
			if($nonhomecountries==0){
				print '<td colspan="2">&nbsp;</td>';
			}else{ ?>
				  <td align="right"><strong><font color='#FF0000'><span id="outspan" style="visibility:hidden">*</span></font><?php print $xxNonState?>:</strong></td>
				  <td align="left"><input type="text" id="state2" name="state2" size="<?php print atb(20)?>" value="<?php if(! $havestate) print @$ordState?>" /></td>
<?php		}
			if($numallstates<=0) print '<td colspan="2">&nbsp;</td>'; ?>
				</tr>
<?php	} ?>
				<tr>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxCountry?>:</strong></td>
				  <td align="left"><select id="country" name="country" size="1" onchange="checkoutspan('')">
<?php	show_countries($_SESSION['thisshipcountry']) ?>
					</select>
				  </td>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxZip?>:</strong></td>
				  <td align="left"><input type="text" name="zip" size="<?php print atb(10)?>" value="<?php print @$ordZip?>" /></td>
				</tr>
				<tr>
				  <td align="right"><strong><font color='#FF0000'>*</font><?php print $xxPhone?>:</strong></td>
				  <td align="left"<?php if(trim(@$extraorderfield2)=="") print ' colspan="3"'; ?>><input type="text" name="phone" size="<?php print atb(20)?>" value="<?php print @$ordPhone?>" /></td>
			<?php	if(trim(@$extraorderfield2)!=""){ ?>
				  <td align="right"><strong><?php if(@$extraorderfield2required==TRUE) print '<font color="#FF0000">*</font>';
									print $extraorderfield2 ?>:</strong></td>
				  <td align="left"><?php if(@$extraorderfield2html != "")print $extraorderfield2html; else print '<input type="text" name="ordextra2" size="' . atb(20) . '" value="' . @$ordExtra2 . '" />'?></td>
			<?php	} ?>
				</tr>
			<?php	if(@$commercialloc==TRUE || $shipType==4){ ?>
				<tr>
				  <td align="right"><input type="checkbox" name="commercialloc" value="Y" <?php if((@$ordComLoc&1)==1) print "checked"?> /></td>
				  <td align="left" colspan="3"><font size="1"><?php print $xxComLoc ?></font></td>
				</tr>
<?php				}
					if(abs(@$addshippinginsurance)==2){ ?>
				<tr>
				  <td align="right"><input type="checkbox" name="wantinsurance" value="Y" <?php if((@$ordComLoc&2)==2) print "checked"?> /></td>
				  <td align="left" colspan="3"><font size="1"><?php print $xxWantIns ?></font></td>
				</tr>
<?php				}
					if(@$noshipaddress!=TRUE){ ?>
				<style type="text/css">
				<!--
				img {
					border: 0px;
				}
				-->
				</style>
				<?
					//if($_POST['editwhat']!='shipping') {
						//$shipbuttondisp='';
						//$shipinfodisp='style="display: none;"';
					//} else {
						//$shipbuttondisp='style="display: none;"';
						//$shipinfodisp='';
					//}
				?>
				<!--<tr id="showShipping" <?=$shipbuttondisp?>>
					<td align="center" colspan="4"><a onclick="showDetails();" style="cursor: pointer"><img src="/lib/images/ship_different.gif" alt="click here to ship your order to a different address then your billing address" /></a></td>
				</tr>-->
				<tr class="shippingDetails" <?=$shipinfodisp?>>
				  <td width="100%" align="center" colspan="4"><strong><a name="shippingInfo"></a><?php print $xxShpDiff?></strong></td>
				</tr>
				<tr class="shippingDetails" <?=$shipinfodisp?>>
				  <td align="right"><strong><?php print $xxName?>:</strong></td>
				  <td align="left" colspan="3"><input type="text" id="sname" name="sname" size="<?php print atb(20)?>" value="<?=$ordShipName?>" /></td>
				</tr>
				<tr class="shippingDetails" <?=$shipinfodisp?>>
				  <td align="right"><strong><?php print $xxPOAPO?>:</strong></td>
				  <td align="left" colspan="3"><input type="checkbox" id="spoapo" name="spoapo" value="1" <?php if((@$ordShipPoApo&1)==1) print "checked"?> /> - <em>APOs are sent Priority Postal Service</em></td>
				</tr>
				<tr class="shippingDetails" <?=$shipinfodisp?>>
				  <td align="right"><strong><?php print $xxAddress?>:</strong></td>
				  <td align="left"<?php if(@$useaddressline2==TRUE) print ' colspan="3"'?>><input type="text" name="saddress" id="saddress" maxlength="35" size="<?php print atb(25)?>" value="<?php print @$ordShipAddress?>" /></td>
<?php	if(@$useaddressline2==TRUE){ ?>
				</tr>
				<tr class="shippingDetails" <?=$shipinfodisp?>>
				  <td align="right"><strong><?php print $xxAddress2?>:</strong></td>
				  <td align="left"><input type="text" id="saddress2" name="saddress2" maxlength="35" size="<?php print atb(25)?>" value="<?php print @$ordShipAddress2?>" /></td>
<?php	} ?>
				  <td align="right"><strong><?php print $xxCity?>:</strong></td>
				  <td align="left"><input type="text" id="scity" name="scity" size="<?php print atb(20)?>" value="<?php print @$ordShipCity?>" /></td>
				</tr>
<?php	if($numallstates>0 || $nonhomecountries != 0){ ?>
				<tr class="shippingDetails" <?=$shipinfodisp?>>
<?php		if($numallstates>0){ ?>
				  <td align="right"><strong><font color='#FF0000'><span id="soutspandd" style="visibility:hidden">*</span></font><?php print $xxState?>:</strong></td>
				  <td align="left"><select id="sstate" name="sstate" size="1" onchange="dosavestate('s'); checkNonState('s');"><?php $havestate = show_states(@$ordShipState) ?></select></td>
<?php		}
			if($nonhomecountries==0){
				print '<td colspan="2">&nbsp;</td>';
			}else{ ?>
				  <td class="sstate2Hide" align="right"><strong><font color='#FF0000'><span id="soutspan" style="visibility:hidden">*</span></font><?php print $xxNonState?>:</strong></td>
				  <td class="sstate2Hide" align="left"><input type="text" id="sstate2" name="sstate2" size="<?php print atb(20)?>" value="<?php if(! $havestate) print @$ordShipState?>" /></td>
<?php		}
			if($numallstates<=0) print '<td colspan="2">&nbsp;</td>'; ?>
				</tr>
<?php	} ?>
				<tr class="shippingDetails" <?=$shipinfodisp?>>
				  <td align="right"><strong><?php print $xxCountry?>:</strong></td>
				  <td align="left"><select name="scountry" id="scountry" size="1" onchange="checkoutspan('s')">
<?php		if($_POST['scountry']!="") $scountry2= $_POST['scountry']; else $scountry2 = $_SESSION['thisshipcountry'];
show_countries($scountry2); ?>
					</select>
				  </td>
				  <td align="right"><strong><?php print $xxZip?>:</strong></td>
				  <td align="left"><input type="text" id="szip" name="szip" size="<?php print atb(10)?>" value="<?php print @$ordShipZip?>" /></td>
				</tr>
				  <script type="text/javascript">
				  <!--
				  //$('sname').value = '';
				  //$('spoapo').checked = false;
				  //$('saddress').value = '';
				  //$('saddress2').value = '';
				 // $('scity').value = '';
				 // $('sstate2').value = '';
				 // $('scountry').selectedIndex = 0;
				 // $('szip').value = '';
				 // $('sstate').selectedIndex = 0;
				  -->
				  </script>
			<?php	} // $noshipaddress ?>
				<!--<tr>
				  <td align="center" colspan="4">
					<strong><?php print $xxAddInf?>.</strong><br />
					<textarea name="ordAddInfo" rows="3" wrap=virtual cols="<?php print atb(44)?>"><?php print @$ordAddInfo?></textarea>
				  </td>
				</tr>-->

				<tr >
					<td colspan="4"><div style="border-bottom:dotted 1px #3C3C3C; ">&nbsp;</div></td>
				</tr>
				<tr>
				  <td align="center" colspan="4"><input type="checkbox" name="remember" value="1" <?php if($remember) print "checked"?> />
					<strong><?php print $xxRemMe?></strong><br />
					<font size="1"><?php print $xxOpCook?></font>
				  </td>
				</tr>
<?php				if(!@$nogiftcertificate){ ?>
				<tr>
				  <td align="right" colspan="2"><strong><?php print $xxGifNum?>:</strong></td><td colspan="2"><input type="text" name="cpncode" size="<?php print atb(20)?>" value="<?=@$_POST["cpncode"]?>" /></td>
				</tr>
				<tr>
				  <td align="center" colspan="4"><font size="1"><?php print $xxGifEnt?></font></td>
				</tr>
<?php
					}
				//HCS - DRE 03/07/05 Gift Cert Mode START
				$notcerts = 1;
				$sqlstr = "SELECT cartProdID FROM cart LEFT JOIN products ON cartProdID = pID
					   WHERE cartSessionID='".trim(@$_POST["sessionid"])."' AND cartCompleted = 0 AND p_iscert = 1";
				$notcerts = mysql_num_rows(mysql_query($sqlstr));
				if ($notcerts == 0) {
?>
				<tr>
				  <td align="right" colspan="2"><strong>Gift Certificate Code:</strong></td><td colspan="2"><input type="text" name="cert_code" size="<?php print atb(20)?>" value="<?=$_POST['cert_code'];?>"></td>
				</tr>
				<tr>
				  <td align="center" colspan="4"><font size="1"><a href="/certificates.php#balance">Click here to check your Gift Certificate's status</a></font></td>
				</tr>
			<?php   }
			?>
				<tr>
				  <td align="center" colspan="4"><font size="1">Note: Gift certificates cannot be used to pay for any gift certificates you may have in your current order. <br />Only one gift certificate can be used per order. </font></td>
				</tr>
<?php
				 $arecerts = 1;
				$sqlstr = "SELECT cartProdID FROM cart LEFT JOIN products ON cartProdID = pID
					   WHERE cartSessionID='".trim(@$_POST["sessionid"])."' AND cartCompleted = 0 AND p_iscert > 0";
				$arecerts = mysql_num_rows(mysql_query($sqlstr));
				if ($arecerts > 0) {
					//$sSQL = "SELECT payProvID,                     PayProvShow FROM payprovider WHERE payProvEnabled=1 AND payProvID <> 4 ORDER BY payProvOrder";
					 $sSQL = "SELECT payProvID,".getlangid("payProvShow",128)." FROM payprovider WHERE payProvEnabled=1 AND payProvID <> 4 ORDER BY payProvOrder";
				}else{
					//$sSQL = "SELECT payProvID,PayProvShow FROM payprovider WHERE payProvEnabled=1 ORDER BY payProvOrder";
					 $sSQL = "SELECT payProvID,".getlangid("payProvShow",128)." FROM payprovider WHERE payProvEnabled=1 ORDER BY payProvOrder";
				}
				//HCS - DRE 03/07/05 Gift Cert Mode STOP
?>
				<tr>
					<td colspan="2" style="font-weight: bold; text-align: right">How did you hear about us?</td>
					<td colspan="2">
						<select name="howfound">
						  <option value="" <?php if (!(strcmp("", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>Select...</option>
						  <option value="macworld" <?php if (!(strcmp("macworld", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>MacWorld</option>
						  <option value="ipodlounge" <?php if (!(strcmp("ipodlounge", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>iPod Lounge</option>
						  <option value="rollingstones" <?php if (!(strcmp("rollingstones", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>Rolling Stones</option>
						  <option value="google" <?php if (!(strcmp("google", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>Google</option>
						  <option value="friend" <?php if (!(strcmp("friend", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>A Friend</option>
						  <option value="other" <?php if (!(strcmp("other", $_POST['howfound']))) {echo "selected=\"selected\"";} ?>>Other</option>
					  </select>
					</td>
				</tr>
<?php
					if(@$_SESSION["clientLoginLevel"] != "") $minloglevel=$_SESSION["clientLoginLevel"]; else $minloglevel=0;
					/*$minaccesslevel=0;
					if(@$_SESSION['cust_pay_meth']!= "" && @$_SESSION['cust_pay_meth']!=0){
						if((@$_SESSION['cust_pay_meth'] & 1) == 1) $minaccesslevel=1;
						if((@$_SESSION['cust_pay_meth'] & 2) == 2) $minaccesslevel=2;
					}*/
					$sSQL = "SELECT payProvID,".getlangid("payProvShow",128)." FROM payprovider WHERE payProvEnabled=1 AND payProvID<19";
					if(!empty($WSP)) $sSQL .=" AND payProvID IN ('".str_replace(',',"','",$_SESSION['cust_pay_meth'])."')";
					else $sSQL .=" AND payProvLevel=0";
					$sSQL .=" ORDER BY payProvOrder";
					$result = mysql_query($sSQL) or print(mysql_error());
					if(mysql_num_rows($result)==0){
?>
				<tr>
				  <td colspan="4" align="center"><strong><?php print $xxNoPay?></strong></td>
				</tr>
<?php
					}elseif(mysql_num_rows($result)==1){
						$rs = mysql_fetch_array($result);
?>
				<tr>
				  <td colspan="4" align="center"><input type="hidden" name="payprovider" value="<?php print $rs["payProvID"]?>" /><strong><?php print $xxClkCmp?></strong></td>
				</tr>
<?php
					}else{
?>			    <tr>
				  <td colspan="4" align="center"><p><strong><?php print $xxPlsChz?></strong></p>
				    <p><select name="payprovider" size="1">
<?php
						while($rs = mysql_fetch_array($result)){
							print "<option value='" . $rs["payProvID"] . "'";
							if(@$ordPayProvider==$rs["payProvID"]) print " selected";
							print ">" . $rs[getlangid("payProvShow",128)] . "</option>\n";
						}
?>
				    </select></p>
				  </td>
			    </tr>
<?php
					}
					if($emaillist) {
?>
				<tr>
					<td colspan="4" align="center"><input type="checkbox" name="addtoemaillist" value="1" checked="checked" /> <strong><?=$xxELSub?></strong></td>
				</tr>
<?php
					}
?>
<?php if(@$termsandconditions==TRUE){ ?>
				<tr>
				  <td align="center" colspan="4"><input type="checkbox" name="license" value="1" />
					<?php print $xxTermsCo?>
				  </td>
				</tr>
<?php } ?>

				<tr>
				  <td width="50%" align="center" colspan="4"><input type="image" src="/lib/images/final_review.gif" border="0" /></td>
				</tr>
			  </table>
			</form>
		  </td>
        </tr>
      </table>
<script language="JavaScript" type="text/javascript">
<?php
	if($numallstates>0) print "savestate = document.forms.mainform.state.selectedIndex;\r\n";
	print 'numhomecountries=' . $numhomecountries . ";\r\n";
	print "checkoutspan('');\r\n";
	if(@$noshipaddress!=TRUE){
		if($numallstates>0) print "ssavestate = document.forms.mainform.sstate.selectedIndex;\r\n";
		print "checkoutspan('s')\r\n";
	} ?></script>
<?php
}elseif(@$_POST["mode"]=="go" || $paypalexpress){
	//HCS - DRE 03/07/05 Gift Cert Mode START
	$cert_code = trim(str_replace("'","",@$_POST["cert_code"]));
	$gcdiscount = 0;
	$strsql = "SELECT cert_amt FROM certificates WHERE UPPER(cert_code) = '".strtoupper($cert_code)."' AND cert_exp_dt >=".time();
	$result = mysql_query($strsql);
	$certfound = 1;
	if(isset($_POST["cert_code"]) && $_POST["cert_code"] != "") {
		$certfound = mysql_num_rows($result);
	}
	if($certfound == 0) {
		$gcmessage = "<font size=\"1\">The certificate number you entered was not found, has expired or is not applicable. ";
		$gcmessage .= "Please <strong><a href=\"javascript:history.go(-2)\">click here</a></strong> if you wish to go back and try again.</font>";
	}
	if($rs=mysql_fetch_assoc($result)) {
		$gcdiscount = $rs['cert_amt'];
	}
	//HCS - DRE 03/07/05 Gift Cert Mode STOP

	// COUPON MANAGEMENT ADDED by Chad 08/09/06
	$cc_sql = "SELECT * FROM coupons WHERE cpnNumber = '".$_POST['cpncode']."' AND cpnIsCoupon = 1 AND cpnNumAvail <= 1";
	$cc_res = mysql_query($cc_sql) or print(mysql_error());
	for($j=0;$cc_row = mysql_fetch_assoc($cc_res);$j++) {
		$_SESSION['chads_coupons'][$j] = $cc_row;
	}
	//showarray($_SESSION['chads_coupons']);
	// COUPON MANAGEMENT ENDED

	if ($addtoemaillist) {
        $subscriber = new Subscriber('MAIL_CHIMP_IFROGZ');
        $subscriber->set_mailing_list('iFrogz Promotions');

		$data = array();
		$space = strpos($_POST['name'], ' ');
		if (!$space) {
			$firstname = $_POST['name'];
			$lastname = ' ';
		} else {
			$firstname = substr($_POST['name'], 0, $space);
			$lastname = trim(substr($_POST['name'], $space));
		}
		$data['first_name'] = $firstname;
		$data['last_name'] = $lastname;
		if (!empty($_POST['address'])) { $data['address_1'] = $_POST['address']; }
		if (!empty($_POST['address2'])) { $data['address_2'] = $_POST['address2']; }
		if (!empty($_POST['city'])) { $data['city'] = $_POST['city']; }
		if (!empty($_POST['state']) || !empty($_POST['address'])) { $data['state'] = $_POST['state'] . $_POST['state2']; }
		if (!empty($_POST['zip'])) { $data['postal_code'] = $_POST['zip']; }
		if (!empty($_POST['country'])) { $data['country'] = $_POST['country']; }
		if (!empty($_POST['phone'])) { $data['phone'] = $_POST['phone']; }
		$data['custom_ordered'] = date('Y-m-d H:i:s');
		$data['custom_subscribed'] = 'order';

		if (is_array($_SESSION['itemsInCart'])) {
		    $data['custom_products'] = implode(' ', $_SESSION['itemsInCart']);
		}

        $subscriber->set_subscriber($_POST['email'], $data);
        $subscriber->subscribe();

		// VALIDATE
		if(ereg("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*",$_POST['email'])) {
			// CHECK FOR DUPLICATE EMAIL
			$sql = "SELECT * FROM email_lists
					WHERE email = '".mysql_real_escape_string($_POST['email'])."'
					AND subscribed = 'ifrogz'";
			$res = mysql_query($sql) or print(mysql_error());
			if(mysql_num_rows($res) <= 0) {
				$name=explode(' ',$_POST['name']);
				$sql = "INSERT INTO email_lists ( firstname, lastname, email , ordered , subscribed , status )
						VALUES ( '".$name[0]."','".$name[1]."','".mysql_real_escape_string($_POST['email'])."' , '".date("Y-m-d H:i:s")."' , 'ifrogz' , 'active' )";
				$res = mysql_query($sql) or print(mysql_error());
			}
		}
	}
	// EMAIL LIST OPTION ENDED
	if(! $paypalexpress){
		$thesessionid = trim(unstripslashes(@$_POST["sessionid"]));
		$ordName = trim(unstripslashes(@$_POST["name"]));
		$ordPoApo = trim(unstripslashes(@$_POST["poapo"]));
		$ordHowFound = trim(unstripslashes(@$_POST["howfound"]));
		$ordAddress = trim(unstripslashes(@$_POST["address"]));
		$ordAddress2 = trim(unstripslashes(@$_POST["address2"]));
		$ordCity = trim(unstripslashes(@$_POST["city"]));
		if(trim(@$_POST["state"]) != "")
			$ordState = trim(unstripslashes(@$_POST["state"]));
		else
			$ordState = trim(unstripslashes(@$_POST["state2"]));
		$ordZip = trim(unstripslashes(@$_POST["zip"]));
		$ordCountry = trim(unstripslashes(@$_POST["country"]));
		$ordEmail = trim(unstripslashes(@$_POST["email"]));
		$ordPhone = trim(unstripslashes(@$_POST["phone"]));
		$ordShipName = trim(unstripslashes(@$_POST["sname"]));
		$ordShipPoApo = trim(unstripslashes(@$_POST["spoapo"]));
		$ordShipAddress = trim(unstripslashes(@$_POST["saddress"]));
		$ordShipAddress2 = trim(unstripslashes(@$_POST["saddress2"]));
		$ordShipCity = trim(unstripslashes(@$_POST["scity"]));
		if(trim(@$_POST["sstate"]) != "")
			$ordShipState = trim(unstripslashes(@$_POST["sstate"]));
		else
			$ordShipState = trim(unstripslashes(@$_POST["sstate2"]));
		$ordShipZip = trim(unstripslashes(@$_POST["szip"]));
		$ordShipCountry = trim(unstripslashes(@$_POST["scountry"]));
		$commercialloc = trim(@$_POST["commercialloc"]);
		$wantinsurance = trim(@$_POST["wantinsurance"]);
		if($commercialloc=="Y") $ordComLoc = 1; else $ordComLoc = 0;
		if($wantinsurance=="Y" || abs(@$addshippinginsurance)==1) $ordComLoc += 2;
		$ordAffiliate = trim(unstripslashes(@$_POST["PARTNER"]));
		$ordExtra1 = trim(unstripslashes(@$_POST["ordextra1"]));
		$ordExtra2 = trim(unstripslashes(@$_POST["ordextra2"]));
		$ordAddInfo = trim(unstripslashes(@$_POST["ordAddInfo"]));
	}
	if($ordShipAddress != ""){
		$shipcountry = $ordShipCountry;
		$shipstate = $ordShipState;
		$destZip = $ordShipZip;
	}else{
		$shipcountry = $ordCountry;
		$shipstate = $ordState;
		$destZip = $ordZip;
	}
	$sSQL = "SELECT countryID,countryCode,countryOrder FROM countries WHERE countryName='" . mysql_real_escape_string($ordCountry) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs = mysql_fetch_array($result)){
		$countryID = $rs["countryID"];
		$countryCode = $rs["countryCode"];
		$homecountry = ($rs["countryOrder"]==2);
	}
	mysql_free_result($result);
	if(! $homecountry) $perproducttaxrate=FALSE;
	$sSQL = "SELECT countryID,countryTax,countryCode,countryFreeShip,countryOrder FROM countries WHERE countryName='" . mysql_real_escape_string($shipcountry) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs = mysql_fetch_array($result)){
		$countryTaxRate = $rs["countryTax"];
		$shipCountryID = $rs["countryID"];
		$shipCountryCode = $rs["countryCode"];
		$freeshipapplies = ($rs["countryFreeShip"]==1);
		$shiphomecountry = ($rs["countryOrder"]==2);
	}
	mysql_free_result($result);
	if($homecountry){
		$sSQL = "SELECT stateTax,stateAbbrev FROM states WHERE stateAbbrev='" . mysql_real_escape_string($ordState) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result))
			$stateAbbrev=$rs["stateAbbrev"];
		mysql_free_result($result);
	}
	if($shiphomecountry){
		$sSQL = "SELECT stateTax,stateAbbrev,stateFreeShip FROM states WHERE stateAbbrev='" . mysql_real_escape_string($shipstate) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$stateTaxRate=$rs["stateTax"];
			$shipStateAbbrev=$rs["stateAbbrev"];
			$freeshipapplies = ($freeshipapplies && ($rs["stateFreeShip"]==1));
		}
		mysql_free_result($result);
	}
	initshippingmethods();
	$sSQL = "SELECT cartID,cartProdID,cartProdPrice,cartQuantity,pWeight,pShipping,pShipping2,pExemptions,pSection,topSection,pDims,pTax FROM cart LEFT JOIN products ON cart.cartProdID=products.pId LEFT OUTER JOIN sections ON products.pSection=sections.sectionID WHERE cartCompleted=0 AND cartSessionID='" . $thesessionid . "'";
	$allcart = mysql_query($sSQL) or print(mysql_error());
	if(($itemsincart=mysql_num_rows($allcart))==0) $allcart = "";
	if($success && $allcart != ""){
		$rowcounter = 0;
		$index=0;
		while($rsCart=mysql_fetch_array($allcart)){
			$index++;
			$sSQL = "SELECT SUM(coPriceDiff) AS coPrDff FROM cartoptions WHERE coCartID=". $rsCart["cartID"];
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result)){
				$rsCart["cartProdPrice"] += (double)$rs["coPrDff"];
			}
			mysql_free_result($result);
			$sSQL = "SELECT SUM(coWeightDiff) AS coWghtDff FROM cartoptions WHERE coCartID=". $rsCart["cartID"];
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result)){
				$rsCart["pWeight"] += (double)$rs["coWghtDff"];
			}
			mysql_free_result($result);
			$runTot=$rsCart["cartProdPrice"] * (int)($rsCart["cartQuantity"]);
			$totalquantity += (int)($rsCart["cartQuantity"]);
			$totalgoods += $runTot;
			$thistopcat=0;
			if(trim(@$_SESSION["clientUser"]) != "") $rsCart["pExemptions"] = ((int)$rsCart["pExemptions"] | (int)$_SESSION["clientActions"]);
			if(($shipType==2 || $shipType==3 || $shipType==4 || $shipType==6) && (double)$rsCart["pWeight"]<=0.0)
				$rsCart["pExemptions"] = ($rsCart["pExemptions"] | 4);
			if(($rsCart["pExemptions"] & 1)==1) $statetaxfree += $runTot;
			if(@$perproducttaxrate==TRUE){
				if(is_null($rsCart["pTax"])) $rsCart["pTax"] = $countryTaxRate;
				if(($rsCart["pExemptions"] & 2) != 2) $countryTax += (($rsCart["pTax"] * $runTot) / 100.0);
			}else{
				if(($rsCart["pExemptions"] & 2)==2) $countrytaxfree += $runTot;
			}
			if(($rsCart["pExemptions"] & 4)==4) $shipfreegoods += $runTot;
			addproducttoshipping($rsCart, $index);
		}
		calculatediscounts(round($totalgoods,2), true, $cpncode);
		if(@$_POST["shipping"] != ""){
			$shipArr = split('\|',$_POST["shipping"],3);
			$shipping = (double)$shipArr[0];
			$isstandardship = ((int)$shipArr[1]==1);
			$shipMethod = $shipArr[2];
		}else
			calculateshipping();
		if(trim(@$_POST["shipping"])=="" && $alternaterates && $somethingToShip) $checkIntOptions = TRUE;
		//$checkIntOptions = FALSE;
		if(is_numeric(@$shipinsuranceamt) && trim(@$_POST["shipping"])=="" && $somethingToShip){
			if(($wantinsurance=="Y" && @$addshippinginsurance==2) || @$addshippinginsurance==1){
				for($index3=0; $index3 < $maxshipoptions; $index3++)
					$intShipping[$index3][2] += (((double)$totalgoods*(double)$shipinsuranceamt)/100.0);
				$shipping += (((double)$totalgoods*(double)$shipinsuranceamt)/100.0);
			}elseif(($wantinsurance=="Y" && @$addshippinginsurance==-2) || @$addshippinginsurance==-1){
				for($index3=0; $index3 < $maxshipoptions; $index3++)
					$intShipping[$index3][2] += $shipinsuranceamt;
				$shipping += $shipinsuranceamt;
			}
		}
		if(@$taxShipping==1 && trim(@$_POST["shipping"])==""){
			for($index3=0; $index3 < $maxshipoptions; $index3++)
				$intShipping[$index3][2] += ((double)$intShipping[$index3][2]*((double)$stateTaxRate+(double)$countryTaxRate))/100.0;
			$shipping += ((double)$shipping*((double)$stateTaxRate+(double)$countryTaxRate))/100.0;
		}
		if(@$taxHandling==1){
			$handling += ((double)$handling*((double)$stateTaxRate+(double)$countryTaxRate))/100.0;
		}
		if(! $checkIntOptions){
			calculateshippingdiscounts(true);
			if(@$_SESSION["clientUser"] != "" && @$_SESSION["clientActions"] != 0) $cpnmessage .= $xxLIDis . $_SESSION["clientUser"] . "<br />";
			$cpnmessage = substr($cpnmessage,6);
			if($totaldiscounts > $totalgoods) $totaldiscounts = $totalgoods;
			$usehst=false;
			if(@$canadataxsystem==true && $shipCountryID==2 && ($shipStateAbbrev=="NB" || $shipStateAbbrev=="NF" || $shipStateAbbrev=="NS"))
				$usehst=true;
			if(@$canadataxsystem==true && $shipCountryID==2 && ($shipStateAbbrev=="PE" || $shipStateAbbrev=="QC")){
				$statetaxable = 0;
				$countrytaxable = 0;
				if(@$taxShipping==2 && ($shipping - $freeshipamnt > 0)){
					if(@$proratashippingtax==TRUE){
						if($totalgoods>0) $statetaxable += (((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree)) / $totalgoods) * ((double)$shipping-(double)$freeshipamnt);
					}else
						$statetaxable += ((double)$shipping-(double)$freeshipamnt);
					$countrytaxable += ((double)$shipping-(double)$freeshipamnt);
				}
				if(@$taxHandling==2){
					$statetaxable += (double)$handling;
					$countrytaxable += (double)$handling;
				}
				if($totalgoods>0){
					$statetaxable += ((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree));
					$countrytaxable += ((double)$totalgoods-((double)$totaldiscounts+(double)$countrytaxfree));
				}
				$countryTax = $countrytaxable*(double)$countryTaxRate/100.0;
				$stateTax = ($statetaxable+(double)$countryTax)*(double)$stateTaxRate/100.0;
			}else{
				if($totalgoods>0){
					$stateTax = ((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree))*(double)$stateTaxRate/100.0;
					if(@$perproducttaxrate != TRUE) $countryTax = ((double)$totalgoods-((double)$totaldiscounts+(double)$countrytaxfree))*(double)$countryTaxRate/100.0;
				}
				if(@$taxShipping==2 && ($shipping - $freeshipamnt > 0)){
					if(@$proratashippingtax==TRUE){
						if($totalgoods>0) $stateTax += (((double)$totalgoods-((double)$totaldiscounts+(double)$statetaxfree)) / $totalgoods) * (((double)$shipping-(double)$freeshipamnt)*(double)$stateTaxRate/100.0);
					}else
						$stateTax += (((double)$shipping-(double)$freeshipamnt)*(double)$stateTaxRate/100.0);
					$countryTax += (((double)$shipping-(double)$freeshipamnt)*(double)$countryTaxRate/100.0);
				}
				if(@$taxHandling==2){
					$stateTax += ((double)$handling*(double)$stateTaxRate/100.0);
					$countryTax += ((double)$handling*(double)$countryTaxRate/100.0);
				}
			}
			$totalgoods = round($totalgoods,2);
			$shipping = round($shipping,2);
			$stateTax = round($stateTax,2);
			$countryTax = round($countryTax,2);
			$handling = round($handling,2);
			//HCS - DRE 03/07/05 Gift Cert Mod START
			$notcerts = 1;
			$sqlstr = "SELECT cartProdID FROM cart LEFT JOIN products ON cartProdID = pID
				   WHERE cartSessionID='".trim(@$_POST["sessionid"])."' AND cartCompleted = 0 AND p_iscert = 0";
			$notcerts = mysql_num_rows(mysql_query($sqlstr));
			if($notcerts == 0) {
				$handling = 0;
			}
			//HCS - DRE 03/07/05 Gift Cert Mod STOP
			if($stateTax < 0) $stateTax = 0;
			if($countryTax < 0) $countryTax = 0;
			$freeshipamnt = round($freeshipamnt, 2);
			$totaldiscounts = round($totaldiscounts, 2);
			$grandtotal = round(($totalgoods + $shipping + $stateTax + $countryTax + $handling) - ($totaldiscounts + $freeshipamnt), 2);
			if($grandtotal < 0) $grandtotal = 0;
			//HCS - DRE 03/02/05 Gift Cert Mod START
			$gctotal = 0;
			$sqlstr = "SELECT cartProdID, cartProdPrice, cartQuantity FROM cart LEFT JOIN products ON cartProdID = pID
				   WHERE cartSessionID='".trim(@$_POST["sessionid"])."' AND cartCompleted = 0 AND p_iscert > 0";
			$result = mysql_query($sqlstr);
			while($rs=mysql_fetch_assoc($result)) {
				$gctotal += $rs['cartProdPrice'] * $rs['cartQuantity'];
			}
			$pendingamt = 0;
			if($gcdiscount) {
				if($gcdiscount >= ($grandtotal - $gctotal)) {
					$pendingamt = $grandtotal - $gctotal;
					$grandtotal = $gctotal;
				}else{
					$pendingamt = $gcdiscount;
					$grandtotal -= $gcdiscount;
				}
			}
			//HCS - DRE 03/02/05 Gift Cert Mod STOP
			$sSQL = "SELECT ordID FROM orders WHERE ordSessionID='" . mysql_real_escape_string($thesessionid) . "' AND ordAuthNumber=''";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result))
				$orderid=$rs["ordID"];
			else
				$orderid="";
			mysql_free_result($result);
			if($ordShipName=='' && $ordShipAddress=='' && $ordShipAddress2=='' && $ordShipCity=='') $ordShipCountry='';
			// Blake 12/14/06
			// fedex at standart price
			$shipMethod=str_replace(' At Standard Price','',$shipMethod);
			// end fedex
			
			// Stripslashes from the fields
			$ordName = stripslashes($ordName);
			$ordAddress = stripslashes($ordAddress);
			$ordAddress2 = stripslashes($ordAddress2);
			$ordShipName = stripslashes($ordShipName);
			$ordShipAddress = stripslashes($ordShipAddress);
			$ordShipAddress2 = stripslashes($ordShipAddress2);
			
			if($orderid==""){
				
				$sSQL = "INSERT INTO orders (ordSessionID,ordName,ordAddress,ordAddress2,ordCity,ordState,ordZip,ordCountry,ordEmail,ordPhone,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipZip,ordShipCountry,ordPayProvider,ordAuthNumber,ordShipping,ordStateTax,ordCountryTax,ordHSTTax,ordHandling,ordShipType,ordTotal,ordDate,ordStatus,ordStatusDate,ordComLoc,ordIP,ordAffiliate,ordExtra1,ordExtra2,ordDiscount,ordDiscountText,ordCoupon,ordAddInfo,ordPoApo,ordShipPoApo,ordHowFound,ord_cert_amt,ordEID) VALUES (";
				$sSQL .= "'" . mysql_real_escape_string($thesessionid) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordName) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordAddress) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordAddress2) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordCity) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordState) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordZip) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordCountry) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordEmail) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordPhone) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipName) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipAddress) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipAddress2) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipCity) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipState) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipZip) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordShipCountry) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordPayProvider) . "',";
				$sSQL .= "'',";
				$sSQL .= "'" . mysql_real_escape_string($shipping) . "',";
				if($usehst){
					$sSQL .= "0,";
					$sSQL .= "0,";
					$sSQL .= ($stateTax + $countryTax) . ",";
				}else{
					$sSQL .= "'" . mysql_real_escape_string($stateTax) . "',";
					$sSQL .= "'" . mysql_real_escape_string($countryTax) . "',";
					$sSQL .= "0,";
				}
				$sSQL .= "'" . mysql_real_escape_string($handling) . "',";
				$sSQL .= "'" . mysql_real_escape_string($shipMethod) . "',";
				$sSQL .= "'" . mysql_real_escape_string($totalgoods) . "',";
				$sSQL .= "'" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "',";
				$sSQL .= "2,"; // Status
				$sSQL .= "'" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "',";
				$sSQL .= "'" . $ordComLoc . "',";
				$sSQL .= "'" . @$_SERVER["REMOTE_ADDR"] . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordAffiliate) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordExtra1) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordExtra2) . "',";
				$sSQL .= "'" . mysql_real_escape_string($totaldiscounts+$freeshipamnt) . "',";
				$sSQL .= "'" . mysql_real_escape_string(trim(substr($cpnmessage,0,255))) . "',";
				$sSQL .= "'" . mysql_real_escape_string(trim($cpnIDs)) . "',";
				$sSQL .= "'" . mysql_real_escape_string($ordAddInfo) . "',";
				$sSQL .= "'" . $ordPoApo . "',";
				$sSQL .= "'" . $ordShipPoApo . "',";
				$sSQL .= "'" . $ordHowFound . "',";
				$sSQL .= "'" . $pendingamt . "',";
				$sSQL .= "'" . $_SESSION['custID'] . "')";
				mysql_query($sSQL) or print(mysql_error());
				$orderid = mysql_insert_id();
				if(!setNewLocation( 2 , $orderid )) print("Unable to record new location");
			}else{
				$sSQL = "UPDATE orders SET ";
				$sSQL .= "ordSessionID='" . mysql_real_escape_string($thesessionid) . "',";
				$sSQL .= "ordName='" . mysql_real_escape_string($ordName) . "',";
				$sSQL .= "ordAddress='" . mysql_real_escape_string($ordAddress) . "',";
				$sSQL .= "ordAddress2='" . mysql_real_escape_string($ordAddress2) . "',";
				$sSQL .= "ordCity='" . mysql_real_escape_string($ordCity) . "',";
				$sSQL .= "ordState='" . mysql_real_escape_string($ordState) . "',";
				$sSQL .= "ordZip='" . mysql_real_escape_string($ordZip) . "',";
				$sSQL .= "ordCountry='" . mysql_real_escape_string($ordCountry) . "',";
				$sSQL .= "ordEmail='" . mysql_real_escape_string($ordEmail) . "',";
				$sSQL .= "ordPhone='" . mysql_real_escape_string($ordPhone) . "',";
				$sSQL .= "ordShipName='" . mysql_real_escape_string($ordShipName) . "',";
				$sSQL .= "ordShipAddress='" . mysql_real_escape_string($ordShipAddress) . "',";
				$sSQL .= "ordShipAddress2='" . mysql_real_escape_string($ordShipAddress2) . "',";
				$sSQL .= "ordShipCity='" . mysql_real_escape_string($ordShipCity) . "',";
				$sSQL .= "ordShipState='" . mysql_real_escape_string($ordShipState) . "',";
				$sSQL .= "ordShipZip='" . mysql_real_escape_string($ordShipZip) . "',";
				$sSQL .= "ordShipCountry='" . mysql_real_escape_string($ordShipCountry) . "',";
				$sSQL .= "ordPayProvider='" . mysql_real_escape_string($ordPayProvider) . "',";
				$sSQL .= "ordAuthNumber='',"; // Not yet authorized
				$sSQL .= "ordShipping='" . $shipping . "',";
				if($usehst){
					$sSQL .= "ordStateTax=0,";
					$sSQL .= "ordCountryTax=0,";
					$sSQL .= "ordHSTTax=" . ($stateTax + $countryTax) . ",";
				}else{
					$sSQL .= "ordStateTax='" . $stateTax . "',";
					$sSQL .= "ordCountryTax='" . $countryTax . "',";
					$sSQL .= "ordHSTTax=0,";
				}
				$sSQL .= "ordHandling='" . $handling . "',";
				$sSQL .= "ordShipType='" . $shipMethod . "',";
				$sSQL .= "ordTotal='" . $totalgoods . "',";
				$sSQL .= "ordDate='" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "',";
				$sSQL .= "ordComLoc=" . $ordComLoc . ",";
				$sSQL .= "ordIP='" . @$_SERVER["REMOTE_ADDR"] . "',";
				$sSQL .= "ordAffiliate='" . mysql_real_escape_string($ordAffiliate) . "',";
				$sSQL .= "ordExtra1='" . mysql_real_escape_string($ordExtra1) . "',";
				$sSQL .= "ordExtra2='" . mysql_real_escape_string($ordExtra2) . "',";
				$sSQL .= "ordDiscount='" . ($totaldiscounts + $freeshipamnt) . "',";
				$sSQL .= "ordDiscountText='" . mysql_real_escape_string(trim(substr($cpnmessage,0,255))) . "',";
				$sSQL .= "ordCoupon='" . mysql_real_escape_string(trim($cpnIDs)) . "',";
				$sSQL .= "ord_cert_amt='" . mysql_real_escape_string($pendingamt) . "',";
				$sSQL .= "ordAddInfo='" . mysql_real_escape_string($ordAddInfo) . "'";
				$sSQL .= " WHERE ordID='" . $orderid . "'";
				mysql_query($sSQL) or print(mysql_error());
			}
			//HCS - DRE 03/02/05 Gift Cert Mod START
			if($gcdiscount) {
				$strsql = "UPDATE certificates SET pend_order_id=".$orderid.", pend_order_amt=".$pendingamt." WHERE cert_code ='".$cert_code."'";
				mysql_query($strsql);
			}
			//HCS - DRE 03/02/05 Gift Cert Mod STOP
			$sSQL="UPDATE cart SET cartOrderID=". $orderid . " WHERE cartCompleted=0 AND cartSessionID='" . mysql_real_escape_string($thesessionid) . "'";
			mysql_query($sSQL) or print(mysql_error());
			$descstr="";
			$addcomma = "";
			$sSQL="SELECT cartQuantity,cartProdName FROM cart WHERE cartOrderID=" . $orderid . " AND cartCompleted=0";
			$result = mysql_query($sSQL) or print(mysql_error());
			while($rs=mysql_fetch_assoc($result)){
				$descstr .= $addcomma . $rs["cartQuantity"] . " " . $rs["cartProdName"];
				$addcomma = ", ";
			}
			mysql_free_result($result);
			$descstr = str_replace('"','',$descstr);
			if(@$_POST["remember"]=="1")
				print "<script src='/admin/savecookie.php?id1=" . $orderid . "&id2=" . trim($thesessionid) . "'></script>";
		}
	}else{
		$success=FALSE;
	}
// shipping selection start
	//$checkIntOptions=TRUE;
	$verifyaddress=FALSE;
	if($_POST['verifydone']!="yes") $verifyaddress=TRUE;
	if($checkIntOptions && $success || ($alternaterates && ! $success) || $verifyaddress){
		$hassuccess = $success;
		$success = FALSE; // So not to print the order totals.
?>
<?
// splits the cart page into different funnels
$google_tracker="/cart/address_verification.php";
// end
?>
	<form method="post" name="shipform" id="shipform" action="cart.php" >
	<input name="mode" id="mode" type="hidden" value="go" />
	<input name="verifydone" type="hidden" value="yes" />
<?php
//writehiddenvar('mode', 'go');
writehiddenvar('sessionid', $thesessionid);
writehiddenvar('PARTNER', $ordAffiliate);
writehiddenvar('name', $ordName);
writehiddenvar('poapo', $ordPoApo);
writehiddenvar('email', $ordEmail);
writehiddenvar('address', $ordAddress);
writehiddenvar('address2', $ordAddress2);
writehiddenvar('city', $ordCity);
writehiddenvar('state', $ordState);
writehiddenvar('country', $ordCountry);
writehiddenvar('zip', $ordZip);
writehiddenvar('phone', $ordPhone);
writehiddenvar('sname', $ordShipName);
writehiddenvar('spoapo', $ordShipPoApo);
writehiddenvar('saddress', $ordShipAddress);
writehiddenvar('saddress2', $ordShipAddress2);
writehiddenvar('scity', $ordShipCity);
writehiddenvar('sstate', $ordShipState);
writehiddenvar('scountry', $ordShipCountry);
writehiddenvar('szip', $ordShipZip);
writehiddenvar('ordAddInfo', $ordAddInfo);
writehiddenvar('ordextra1', $ordExtra1);
writehiddenvar('ordextra2', $ordExtra2);
writehiddenvar('cpncode', $cpncode);
writehiddenvar('howfound', $ordHowFound);
writehiddenvar('payprovider', $ordPayProvider);
writehiddenvar('token', $token);
writehiddenvar('payerid', $payerid);
writehiddenvar('wantinsurance', $wantinsurance);
writehiddenvar('commercialloc', $commercialloc);
writehiddenvar('cert_code', $cert_code);
writehiddenvar('remember', @$_POST["remember"]);
include(DOCROOT.'includes/fedex/fedexdc.php');
?>
<input name="editwhat" id="editwhat" type="hidden" value="billing" />
<div id="cart_nav"><img src="/lib/images/new_images/subnav_gray_13.gif" alt="View Order"><img src="/lib/images/new_images/subnav_green_15.gif" alt="Customer Info"><img src="/lib/images/new_images/subnav_gray_17.gif" alt="Final Review"><img src="/lib/images/new_images/subnav_gray_19.gif" alt="Confirmation"><img src="/lib/images/new_images/subnav_gray_21.gif" alt="View Receipt"></div>
<div style="margin:0px 10p;">
<h2>Please Verify Your Information</h2>
	<div>
		<div style="border:#B1B1B1 solid 1px; width:430px; float:left; line-height:18px;padding:10px;">
			<div>
				<span style=" font-weight:normal;text-align:right; margin-left:100px;float:right"><a href="#" onclick="$('mode').value='edit';$('shipform').submit();">edit</a></span>
				<h3 style="width:300px">Billing Address</h3>
				<strong>Full Name:</strong> <?=$ordName?><br />
				<strong>Email: </strong> <?=$ordEmail?> <br />
				<strong>Phone:</strong> <?=$ordPhone?><br />
				<? if($ordPoApo=="1") echo '<strong>APO/PO Box:</strong> yes<br />';?>
				<?
				$showAddVer=FALSE;
				if($ordShipAddress=="" && $ordCountry=="United States of America" && $ordPoApo!="1") $showAddVer=TRUE;
				if($showAddVer) {
					// create new FedExDC object
					$fed = new FedExDC(NULL, NULL);
					$aFedData = array(
						11=>$ordName,
						13=>$ordAddress, // Spelled wrong to test.  Should be "Stuart St"
						14=>$ordAddress2,
						15=>$ordCity,
						16=>$ordState,
						17=>$ordZip,
						1600=>'',   // Urbanization Code?  Used for PR
						1601=>3     // How many retuned items
					);

					$aVData = $fed->address_validate($aFedData);
					//echo "BUFF ". $fed->sBuf;
					if ($error = $fed->getError()) {
						$check_error= split(': ',$error);
					}
					$returnedAddress='';
					$checksame=0;
					$returnedTotal=0;
					for ($i=1; $i<=$aVData['1604']; $i++) {
						foreach (array(13,14,15,16,17) as $key){
							if (isset($aVData[$key.'-'.$i])){
								if($key==13){
									$returnedTotal++;
									$returnedAddress.= $aVData[$key.'-'.$i]."<br />\n";
									$addressReturn=$aVData[$key.'-'.$i];
									if($aVData[$key.'-'.$i]==$ordAddress) $checksame++;
								}elseif($key==14){
									$returnedTotal++;
									$returnedAddress.= $aVData[$key.'-'.$i]."<br />\n";
									$address2Return=$aVData[$key.'-'.$i];
									if($aVData[$key.'-'.$i]==$ordAddress2 || empty($ordAddress2)) {echo 'yes';$checksame++;}
								} elseif($key==17){
									$returnedTotal++;
									$returnedAddress.= $aVData[$key.'-'.$i]."<br />\n";
									$zipReturn=$aVData[$key.'-'.$i];
									if($aVData[$key.'-'.$i]==$ordZip) $checksame++;
								} elseif($key==15) {
									$returnedTotal++;
									$returnedAddress.= $aVData[$key.'-'.$i].", ";
									$cityReturn=$aVData[$key.'-'.$i];
									if($aVData[$key.'-'.$i]==$ordCity) $checksame++;
								} elseif($key==16) {
									$returnedTotal++;
									$returnedAddress.= $aVData[$key.'-'.$i]." ";
									$stateReturn=$aVData[$key.'-'.$i];
									if($aVData[$key.'-'.$i]==$ordState) $checksame++;
								}
							}
						}
					}
				?>
				<? if($checksame!=$returnedTotal || !empty($check_error)){?>
				<div style="float:right; width:200px; border-left: dashed #B1B1B1 1px; padding-left:10px;">
				<? if(!empty($check_error[1])) echo '<span style="color:#FF0000;">'.$check_error[1].'</span>';
				 else {?>
					<div style="font-weight:bold;"><input name="addressselect" type="radio" value="1" onclick="$('address').value='<?=$addressReturn?>';$('zip').value='<?=$zipReturn?>';$('address2').value='<?=$address2Return?>';$('city').value='<?=$cityReturn?>';$('state').value='<?=$stateReturn?>';" />
					Use Verified Address</div>
					<?=$returnedAddress?>
					<?=$ordCountry?>
				<? } ?>
				</div>
				<? } ?>
				<? } ?>
				<div style="width:200px;">
					<? if($showAddVer && $checksame!=$returnedTotal) { ?>
						<input name="addressselect" type="radio" value="0" checked="checked" onclick="$('address').value='<?=$ordAddress?>';$('zip').value='<?=$ordZip?>';$('address2').value='<?=$ordAddress2?>';$('city').value='<?=$ordCity?>';$('state').value='<?=$ordState?>';" />
					<? }?>
					<strong>Address:</strong><br /> <?=$ordAddress?><br />
					<? if($ordAddress2!="") echo $ordAddress2.'<br />';?>
					<?=$ordCity?>, <?=$ordState?>  <?=$ordZip?><br />
					<?=$ordCountry?>
				</div>
			</div>
		</div>

		<div style="border:#B1B1B1 solid 1px; float:right; width:430px; margin-right:2px;line-height:18px;">
			<div style="padding:8px;">
				<span style=" font-weight:normal;text-align:right; float:right; width:30px;"><a href="#" onclick="$('mode').value='edit';$('editwhat').value='shipping';$('shipform').submit();">edit</a></span>
				<h3 width:300px;">Shipping Address<? if(empty($ordShipAddress)) echo ' (same as billing)';?></h3>
				<? if(!empty($ordShipAddress)) {
					$showShipAddVer=FALSE;
					$checkshipsame=0;
					$returnedShipTotal=0;
					if($ordShipCountry=="United States of America" && $ordShipPoApo!="1" ) $showShipAddVer=TRUE;
					if($showShipAddVer) {
						// create new FedExDC object
						$fed = new FedExDC(NULL, NULL);
						$aFedData = array(
							11=>$ordShipName,
							13=>$ordShipAddress, // Spelled wrong to test.  Should be "Stuart St"
							14=>$ordShipAddress2,
							15=>$ordShipCity,
							16=>$ordShipState,
							17=>$ordShipZip,
							1600=>'',   // Urbanization Code?  Used for PR
							1601=>3     // How many retuned items
						);

						$aVData = $fed->address_validate($aFedData);
						//echo "BUFF ". $fed->sBuf;
						if ($error = $fed->getError()) {
							$scheck_error= split(': ',$error);
						}
						$returnedShipAddress='';
						for ($i=1; $i<=$aVData['1604']; $i++) {
							foreach (array(13,14,15,16,17) as $key){
								if (isset($aVData[$key.'-'.$i])){
									if($key==13){
										$returnedShipTotal++;
										$returnedShipAddress.= $aVData[$key.'-'.$i]."<br />\n";
										$addressShipReturn=$aVData[$key.'-'.$i];
										if($aVData[$key.'-'.$i]==$ordShipAddress) $checkshipsame++;
									}elseif($key==14){
										$returnedShipTotal++;
										$returnedShipAddress.= $aVData[$key.'-'.$i]."<br />\n";
										$address2ShipReturn=$aVData[$key.'-'.$i];
										if($aVData[$key.'-'.$i]==$ordShipAddress2) $checkshipsame++;
									} elseif($key==17){
										$returnedShipTotal++;
										$returnedShipAddress.= $aVData[$key.'-'.$i]."<br />\n";
										$zipShipReturn=$aVData[$key.'-'.$i];
										if($aVData[$key.'-'.$i]==$ordShipZip) $checkshipsame++;
									} elseif($key==15) {
										$returnedShipTotal++;
										$returnedShipAddress.= $aVData[$key.'-'.$i].", ";
										$cityShipReturn=$aVData[$key.'-'.$i];
										if($aVData[$key.'-'.$i]==$ordShipCity) $checkshipsame++;
									} elseif($key==16) {
										$returnedShipTotal++;
										$returnedShipAddress.= $aVData[$key.'-'.$i]." ";
										$stateShipReturn=$aVData[$key.'-'.$i];
										if($aVData[$key.'-'.$i]==$ordShipState) $checkshipsame++;
									}
								}
							}
						}
						
					?>
					<? if($checkshipsame!=$returnedShipTotal || !empty($scheck_error)){?>
					<div style="float:right; width:200px; border-left: dashed #B1B1B1 1px; padding-left:10px;">
						<? if(!empty($scheck_error[1])) echo '<span style="color:#FF0000;">'.$scheck_error[1].'</span>';
						else {?>
							<div style="font-weight:bold;"><input name="addressshipselect" type="radio" value="1" onclick="$('saddress').value='<?=$addressShipReturn?>';$('szip').value='<?=$zipShipReturn?>';$('saddress2').value='<?=$address2ShipReturn?>';$('scity').value='<?=$cityShipReturn?>';$('sstate').value='<?=$stateShipReturn?>';" />
							Use Verified Address</div>
							<?=$returnedShipAddress?>
							<?=$ordShipCountry?>
						<? } ?>
					</div>
					<? } ?>
					<? } ?>
					<div style="width:200px;">
					<? if($showShipAddVer && $checkshipsame!=$returnedShipTotal) { ?>
						<input name="addressshipselect" type="radio" value="0" checked="checked" onclick="$('saddress').value='<?=$ordShipAddress?>';$('szip').value='<?=$ordShipZip?>';$('saddress2').value='<?=$ordShipAddress2?>';$('scity').value='<?=$ordShipCity?>';$('sstate').value='<?=$ordShipState?>';" />
					<? }?>
					<? if($ordShipPoApo=="1") echo '<strong>APO/PO Box:</strong> yes<br />';?>
					<strong>Shipping Address:</strong><br /> <?=$ordShipAddress?><br />
					<? if($ordShipAddress2!="") echo $ordShipAddress2.'<br />';?>
					<?=$ordShipCity?>, <?=$ordShipState?>  <?=$ordShipZip?><br />
					<?=$ordShipCountry?>
				</div>
				<? } ?>
			</div>
		</div>

		<div style="clear:both;">&nbsp;</div>
	</div>
</div>
	<?php
	//showarray($_SESSION);
	?>
            <table class="cobtbl" width="<?php print $maintablewidth?>" border="0" bordercolor="#B1B1B1" cellspacing="1" cellpadding="3" bgcolor="#B1B1B1">

			  <tr>
			    <td height="34" align="center" class="cobhl" bgcolor="#EBEBEB"><strong><?php print $xxShpOpt?></strong></td>
			  </tr>
			  <tr>
				<td height="34" align="center" class="cobll" bgcolor="#FFFFFF">
<?php				if($hassuccess){ ?>
				  <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
					<tr>
					  <td height="34" align="right" width="50%" class="cobll" bgcolor="#FFFFFF"><?php if($shipType==4) print '<img src="/lib/images/LOGO_S.gif" alt="UPS" />&nbsp;&nbsp;'; else print "&nbsp;"; ?></td>
					  <td height="34" align="center" class="cobll" bgcolor="#FFFFFF"><?php
						calculateshippingdiscounts(false);
						$show_fedex=TRUE;
						if(empty($ordShipAddress)){
							if($ordPoApo=="1" || strstr('AA,AE,AP',$ordState)) $show_fedex=FALSE;
						} else {
							if($ordShipPoApo=="1" || strstr('AA,AE,AP',$ordShipState)) $show_fedex=FALSE;
						}
						print "<select name='shipping' size='1'><option value=''".((empty($_SESSION['shp_method']))?' selected="selected"':'').">Please Select...</option>";
						// Blake 12/14/06
						// standard shipping for dropshiporders
						if ($_SESSION['hasdropship']) $numshipoptions=1;
						//if($_SESSION['hasdropship'] && $numshipoptions==3)$numshipoptions=$numshipoptions-2;
						//else if($_SESSION['hasdropship'] && $numshipoptions==2)$numshipoptions--;
						// end
						if($shipType==2 || $shipType==5){
							if(is_array($allzones)){
								$ship='2,5';
								for($index3=0; $index3 < $numshipoptions; $index3++){
									$type[$index3]= $intShipping[$index3][0];
									if(!$show_fedex && $intShipping[$index3][0]!="FedEx Express" || $show_fedex) {
										print "<option value='" . $intShipping[$index3][2] . "|" . (($pzFSA & pow(2, $index3))!=0?"1":"0") . "|" . $intShipping[$index3][0] . "' ".($intShipping[$index3][0]==$_SESSION['shp_method'] ? " selected" : "").">";
										print ($freeshippingapplied && ($pzFSA & pow(2, $index3))!=0 ? $xxFree . " " . $intShipping[$index3][0] : $intShipping[$index3][0] . " " . FormatEuroCurrency($intShipping[$index3][2])) . '</option>';
									}
								}
							}
						}else{
							for($indexmso=0; $indexmso<$maxshipoptions; $indexmso++){
								$shipRow = $intShipping[$indexmso];
								if($shipType==3){
									if($iTotItems==$shipRow[3]){
										for($index2=0;$index2<$numuspsmeths;$index2++){
											if(trim($shipRow[0]) == trim($uspsmethods[$index2][0])){
												print "<option value='" . $shipRow[2] . "|". trim($uspsmethods[$index2][1]) ."|" . trim($uspsmethods[$index2][2]) . "'" . (freeshippingapplied && $uspsmethods[$index2][1]==1 ? " selected>" : ">");
												print trim($uspsmethods[$index2][2]) . " (" . $shipRow[1] . ") " . ($freeshippingapplied && $uspsmethods[$index2][1]==1 ? $xxFree : FormatEuroCurrency($shipRow[2]));
												print "</option>";
											}
										}
									}
								}elseif($shipType==4 || $shipType==6){
									if($shipRow[3]){
										print "<option value='" . $shipRow[2] . "|". $shipRow[4] ."|" . $shipRow[0] . "'" . ($freeshippingapplied && $shipRow[4]==1 ? " selected>" : ">") . $shipRow[0] . " ";
										if(trim($shipRow[1]) != "") print "(" . $xxGuar . " " . $shipRow[1] . ") ";
										print ($freeshippingapplied && $shipRow[4]==1 ? $xxFree : FormatEuroCurrency($shipRow[2]));
										print "</option>";
									}
								}
							}
						}
						if(@$willpickuptext != ""){
							if(@$willpickupcost=="") $willpickupcost=0;
							print '<option value="' . $willpickupcost . "|1|" . str_replace('"','&quot;',$willpickuptext) . '">';
							print $willpickuptext . " " . FormatEuroCurrency($willpickupcost) . "</option>";
						}
						print "</select>";
					?></td>
					  <td height="34" align="left" width="50%" class="cobll" bgcolor="#FFFFFF">&nbsp;</td>
					</tr>
				  </table>
<?php				}else{
						print '<input type="hidden" name="shipping" value="">' . $errormsg;
					} ?>
				</td>
			  </tr>

		<?php if(@$alternateratesups != "" || @$alternateratesusps != "" || @$alternateratesweightbased != "" || @$alternateratescanadapost != ""){ ?>
			  <tr>
			    <td height="34" align="center" class="cobhl" bgcolor="#EBEBEB"><strong>Or select an alternate shipping carrier to compare rates.</strong></td>
			  </tr>
			  <tr>
				<td height="34" align="center" class="cobll" bgcolor="#FFFFFF">
					<select name="altrates" size="1" onchange="document.forms.shipform.shipping.value='';document.forms.shipform.shipping.disabled=true;document.forms.shipform.submit();"><?php
				if(@$alternateratesups != "" || $origShipType==4) print '<option value="4"' . ($shipType==4 ? " selected" : "") . ">" . @$alternateratesups . '</option>';
				if(@$alternateratesusps != "" || $origShipType==3) print '<option value="3"' . ($shipType==3 ? " selected" : "") . ">" . @$alternateratesusps . '</option>';
				if(@$alternateratesweightbased != "" || $origShipType==2) print '<option value="2"' . ($shipType==2 ? " selected" : "") . ">" . @$alternateratesweightbased . '</option>';
				if(@$alternateratescanadapost != "" || $origShipType==6) print '<option value="6"' . ($shipType==6 ? " selected" : "") . ">" . @$alternateratescanadapost . '</option>';
						?></select>
				</td>
			  </tr>
		<?php } ?>
			  <tr>

			</table>
			<div style="text-align:center"><input type="image" value="Checkout" border="0" src="/lib/images/final_review.gif" /></div>
		<?php if($shipType==4){ ?>
			<p align="center">&nbsp;<br /><font size="1">UPS&reg;, UPS & Shield Design&reg; and UNITED PARCEL SERVICE&reg;
			  are<br />registered trademarks of United Parcel Service of America, Inc.</font></p>
		<?php } ?>
	</form>
<?php
	//end shipping selection
	}elseif(! $success){
?>
      <table border="0" cellspacing="<?php print $maintablespacing?>" cellpadding="<?php print $maintablepadding?>" width="<?php print $maintablewidth?>" bgcolor="<?php print $maintablebg?>" align="center">
        <tr>
          <td width="100%">
            <table width="<?php print $innertablewidth?>" border="0" cellspacing="<?php print $innertablespacing?>" cellpadding="<?php print $innertablepadding?>" bgcolor="<?php print $innertablebg?>">
			  <tr>
			    <td align="center"><p>&nbsp;</p><p><strong><?php print $xxSryErr?></strong></p><p><strong><?php print "<br />" . $errormsg ?></strong></p><p>&nbsp;</p></td>
			  </tr>
			</table>
		  </td>
        </tr>
      </table>
<?php
	}elseif($ordPayProvider != ""){
		$blockuser=checkuserblock($ordPayProvider);
		if($blockuser){
			$orderid = 0;
			$thesessionid = "";
		}else{
			$sSQL = "SELECT payProvDemo,payProvData1,payProvData2,payProvMethod FROM payprovider WHERE payProvID='" . mysql_real_escape_string($ordPayProvider) . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			$rs = mysql_fetch_array($result);
			$demomode = ((int)$rs["payProvDemo"]==1);
			$data1 = trim($rs["payProvData1"]);
			$data2 = trim($rs["payProvData2"]);
			$ppmethod = (int)$rs["payProvMethod"];
			mysql_free_result($result);
		}
		if(@$pathtossl != ""){
			if(substr($pathtossl,-1) != "/") $pathtossl .= "/";
			$storeurl = $pathtossl;
		}
		if($grandtotal > 0 && $ordPayProvider=="1"){ // PayPal
?>
	<form method="post" action="https://www.<?php if($demomode) print 'sandbox.'?>paypal.com/cgi-bin/webscr">
	<input type="hidden" name="cmd" value="_ext-enter" />
	<input type="hidden" name="redirect_cmd" value="_xclick" />
	<input type="hidden" name="rm" value="2" />
	<input type="hidden" name="business" value="<?php print $data1?>" />
	<input type="hidden" name="return" value="<?php print $storeurl?>thanks.php" />
	<input type="hidden" name="notify_url" value="/admin/ppconfirm.php" />
	<input type="hidden" name="item_name" value="<?php print substr($descstr,0,127)?>" />
	<input type="hidden" name="custom" value="<?php print $orderid?>" />
<?php		if(@$splitpaypalshipping){
				writehiddenvar('shipping', number_format(round(($shipping + $handling) - $freeshipamnt, 2),2,'.',''));
				writehiddenvar('amount', number_format(round(($totalgoods + $stateTax + $countryTax) - $totaldiscounts, 2),2,'.',''));
			}else{
				writehiddenvar('amount', number_format($grandtotal,2,'.',''));
			} ?>
	<input type="hidden" name="currency_code" value="<?php print $countryCurrency?>" />
	<input type="hidden" name="bn" value="ecommercetemplates.php.ecommplus" />
<?php		$thename = trim(@$_POST["name"]);
			if($thename != ""){
				if(strstr($thename," ")){
					$namearr = split(" ",$thename,2);
					print '<input type="hidden" name="first_name" value="' . $namearr[0] . "\" />\n";
					print '<input type="hidden" name="last_name" value="' . $namearr[1] . "\" />\n";
				}else
					print '<input type="hidden" name="last_name" value="' . $thename . "\" />\n";
			}
?>
	<input type="hidden" name="address1" value="<?php print @$_POST["address"]?>" />
	<input type="hidden" name="address2" value="<?php print @$_POST["address2"]?>" />
	<input type="hidden" name="city" value="<?php print @$_POST["city"]?>" />
<?php		if($countryID==1 && $stateAbbrev != ""){ ?>
	<input type="hidden" name="state" value="<?php print $stateAbbrev?>" />
<?php		}else{ ?>
	<input type="hidden" name="state" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
<?php		} ?>
	<input type="hidden" name="country" value="<?php print $countryCode?>" />
	<input type="hidden" name="email" value="<?php print @$_POST["email"]?>" />
	<input type="hidden" name="zip" value="<?php print @$_POST["zip"]?>" />
	<input type="hidden" name="cancel_return" value="<?php print $storeurl?>sorry.php" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="2"){ // 2Checkout
			$courl='https://www.2checkout.com/cgi-bin/sbuyers/cartpurchase.2c';
			if(is_numeric($data1))
				if($data1>200000 || @$use2checkoutv2==TRUE) $courl='https://www2.2checkout.com/2co/buyer/purchase';
?>
	<form method="post" action="<?php print $courl?>">
	<input type="hidden" name="cart_order_id" value="<?php print $orderid?>" />
	<input type="hidden" name="sid" value="<?php print $data1?>" />
	<input type="hidden" name="total" value="<?php print $grandtotal?>" />
	<input type="hidden" name="card_holder_name" value="<?php print @$_POST["name"]?>" />
	<input type="hidden" name="street_address" value="<?php print @$_POST["address"] . (trim(@$_POST["address2"])!='' ? ', ' . unstripslashes(@$_POST["address2"]) : '')?>" />
<?php		if($countryID==1 || $countryID==2){ ?>
	<input type="hidden" name="city" value="<?php print @$_POST["city"]?>" />
	<input type="hidden" name="state" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
<?php		}else{
				if(trim(@$_POST["state"]) != "") $thestate = unstripslashes(@$_POST["state"]); else $thestate = unstripslashes(@$_POST["state2"]); ?>
	<input type="hidden" name="city" value="<?php print @$_POST["city"] . ($thestate!='' ? ', ' . $thestate : '') ?>" />
	<input type="hidden" name="state" value="Outside US and Canada" />
<?php		} ?>
	<input type="hidden" name="zip" value="<?php print @$_POST["zip"]?>" />
	<input type="hidden" name="country" value="<?php print $countryCode?>" />
	<input type="hidden" name="email" value="<?php print @$_POST["email"]?>" />
	<input type="hidden" name="phone" value="<?php print @$_POST["phone"]?>" />
	<input type="hidden" name="id_type" value="1" />
<?php		$sSQL = "SELECT cartID,cartProdID,pName,pPrice,cartQuantity," . (@$digidownloads==TRUE ? "pDownload," : "") . "pDescription FROM cart INNER JOIN products on cart.cartProdID=products.pID WHERE cartCompleted=0 AND cartSessionID='" .  $thesessionid . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			$index=1;
			while($rs=mysql_fetch_assoc($result)){
				$thedesc = substr(trim(preg_replace("(\r\n|\n|\r)",'\\n',strip_tags($rs["pDescription"]))),0,254);
				if($thedesc=="") $thedesc = substr(trim(preg_replace("(\r\n|\n|\r)",'\\n',strip_tags($rs["pName"]))),0,254);
				print '<input type="hidden" name="c_prod_' . $index . '" value="' . str_replace(',','&#44;',str_replace('"','&quot;',$rs["cartProdID"])) . "," . $rs["cartQuantity"] . "\" />\r\n";
				print '<input type="hidden" name="c_name_' . $index . '" value="' . str_replace('"','&quot;',strip_tags($rs["pName"])) . "\" />\r\n";
				print '<input type="hidden" name="c_description_' . $index . '" value="' . str_replace('"','&quot;',$thedesc) . "\" />\r\n";
				print '<input type="hidden" name="c_price_' . $index . '" value="' . number_format($rs["pPrice"],2,'.','') . "\" />\r\n";
				if(@$digidownloads==TRUE)
					if(trim($rs["pDownload"]) != "") print '<input type="hidden" name="c_tangible_' . $index . '" value="N" />' . "\r\n";
				$index++;
			}
			if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != ""){ ?>
	  <input type="hidden" name="ship_name" value="<?php print @$_POST["sname"]?>" />
	  <input type="hidden" name="ship_street_address" value="<?php print @$_POST["saddress"] . (trim(@$_POST["saddress2"])!='' ? ', ' . unstripslashes(@$_POST["saddress2"]) : '')?>" />
	  <input type="hidden" name="ship_city" value="<?php print @$_POST["scity"]?>" />
	  <input type="hidden" name="ship_state" value="<?php print @$_POST["sstate"]?>" />
	  <input type="hidden" name="ship_zip" value="<?php print @$_POST["szip"]?>" />
	  <input type="hidden" name="ship_country" value="<?php print @$_POST["scountry"]?>" />
<?php		}
			if($demomode)
				print "<input type=\"hidden\" name=\"demo\" value=\"Y\" />";
		}elseif($grandtotal > 0 && $ordPayProvider=="3"){ // Authorize.net SIM
			if(@$secretword != ""){
				$data1 = upsdecode($data1, $secretword);
				$data2 = upsdecode($data2, $secretword);
			} ?>
	<form method=POST action="https://secure.authorize.net/gateway/transact.dll">
	<input type="hidden" name="x_Version" value="3.0" />
	<input type="hidden" name="x_Login" value="<?php print $data1?>" />
	<input type="hidden" name="x_Show_Form" value="PAYMENT_FORM" />
<?php
	  if($ppmethod==1) print '<input type="hidden" name="x_type" value="AUTH_ONLY" />';
		function vrhmac($key, $text){
			$idatastr = "                                                                ";
			$odatastr = "                                                                ";
			$hkey = (string)$key;
			$idatastr .= $text;
			for($i=0; $i<64; $i++){
				$idata[$i] = $ipad[$i] = 0x36;
				$odata[$i] = $opad[$i] = 0x5C;
			}
			for($i=0; $i< strlen($hkey); $i++){
				$ipad[$i] ^= ord($hkey{$i});
				$opad[$i] ^= ord($hkey{$i});
				$idata[$i] = ($ipad[$i] & 0xFF);
				$odata[$i] = ($opad[$i] & 0xFF);
			}
			for($i=0; $i< strlen($text); $i++){
				$idata[64+$i] = ord($text{$i}) & 0xFF;
			}
			for($i=0; $i< strlen($idatastr); $i++){
				$idatastr{$i} = chr($idata[$i] & 0xFF);
			}
			for($i=0; $i< strlen($odatastr); $i++){
				$odatastr{$i} = chr($odata[$i] & 0xFF);
			}
			$innerhashout = md5($idatastr);
			for($i=0; $i<16; $i++)
				$odatastr .= chr(hexdec(substr($innerhashout,$i*2,2)));
			return md5($odatastr);
		}
		$thename = unstripslashes(trim(@$_POST["name"]));
		if($thename != ""){
			if(strstr($thename," ")){
				$namearr = split(" ",$thename,2);
				print '<input type="hidden" name="x_First_Name" value="' . str_replace('"','&quot;',$namearr[0]) . "\" />\n";
				print '<input type="hidden" name="x_Last_Name" value="' . str_replace('"','&quot;',$namearr[1]) . "\" />\n";
			}else
				print '<input type="hidden" name="x_Last_Name" value="' . str_replace('"','&quot;',$thename) . "\" />\n";
		}
		$sequence = $orderid;
		if(@$authnetadjust != "")
			$tstamp = time() + $authnetadjust;
		else
			$tstamp = time();
		$fingerprint = vrhmac($data2, $data1 . "^" . $sequence . "^" . $tstamp . "^" . number_format($grandtotal,2,'.','') . "^");
?>
	<input type="hidden" name="x_fp_sequence" value="<?php print $sequence?>" />
	<input type="hidden" name="x_fp_timestamp" value="<?php print $tstamp?>" />
	<input type="hidden" name="x_fp_hash" value="<?php print $fingerprint?>" />
	<input type="hidden" name="x_address" value="<?php print unstripslashes(@$_POST["address"]) . (trim(@$_POST["address2"])!='' ? ', ' . unstripslashes(@$_POST["address2"]) : '')?>" />
	<input type="hidden" name="x_city" value="<?php print unstripslashes(@$_POST["city"])?>" />
	<input type="hidden" name="x_country" value="<?php print unstripslashes(@$_POST["country"])?>" />
	<input type="hidden" name="x_phone" value="<?php print unstripslashes(@$_POST["phone"])?>" />
	<input type="hidden" name="x_state" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
	<input type="hidden" name="x_zip" value="<?php print unstripslashes(@$_POST["zip"])?>" />
	<input type="hidden" name="x_cust_id" value="<?php print $orderid?>" />
	<input type="hidden" name="x_Invoice_Num" value="<?php print $orderid?>" />
	<input type="hidden" name="x_ect_ordid" value="<?php print $orderid?>" />
	<input type="hidden" name="x_Description" value="<?php print substr($descstr,0,255)?>" />
	<input type="hidden" name="x_email" value="<?php print unstripslashes(@$_POST["email"])?>" />
<?php		if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != ""){
				$thename = trim(@$_POST["sname"]);
				if($thename != ""){
					if(strstr($thename," ")){
						$namearr = split(" ",$thename,2);
						print '<input type="hidden" name="x_Ship_To_First_Name" value="' . $namearr[0] . "\" />\n";
						print '<input type="hidden" name="x_Ship_To_Last_Name" value="' . $namearr[1] . "\" />\n";
					}else
						print '<input type="hidden" name="x_Ship_To_Last_Name" value="' . $thename . "\" />\n";
				} ?>
	<input type="hidden" name="x_ship_to_address" value="<?php print unstripslashes(@$_POST["saddress"]) . (trim(@$_POST["saddress2"])!='' ? ', ' . unstripslashes(@$_POST["saddress2"]) : '')?>" />
	<input type="hidden" name="x_ship_to_city" value="<?php print unstripslashes(@$_POST["scity"])?>" />
	<input type="hidden" name="x_ship_to_country" value="<?php print unstripslashes(@$_POST["scountry"])?>" />
	<input type="hidden" name="x_ship_to_state" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["sstate"]); else print unstripslashes(@$_POST["sstate2"]);?>" />
	<input type="hidden" name="x_ship_to_zip" value="<?php print unstripslashes(@$_POST["szip"])?>" />
<?php		} ?>
	<input type="hidden" name="x_Amount" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="x_Relay_Response" value="True" />
	<input type="hidden" name="x_Relay_URL" value="/admin/wpconfirm.php" />
<?php		if($demomode){ ?>
	<input type="hidden" name="x_Test_Request" value="TRUE" />
<?php		}
		}elseif($grandtotal == 0 || $ordPayProvider=="4"){ // Email ?>
	<form method="post" action="thanks.php" style="margin:0px">
	<input type="hidden" name="emailorder" value="<?php print $orderid?>" />
	<input type="hidden" name="thesessionid" value="<?php print $thesessionid?>" />
	<input type="hidden" name="cert_code" value="<?php print $cert_code?>" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="17"){ // Email 2 ?>
	<form method="post" action="thanks.php" style="margin:0px">
	<input type="hidden" name="secondemailorder" value="<?php print $orderid?>" />
	<input type="hidden" name="thesessionid" value="<?php print $thesessionid?>" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="5"){ // WorldPay ?>
	<form method="post" action="https://select.worldpay.com/wcc/purchase">
	<input type="hidden" name="instId" value="<?php print $data1?>" />
	<input type="hidden" name="cartId" value="<?php print $orderid?>" />
	<input type="hidden" name="amount" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="currency" value="<?php print $countryCurrency?>" />
	<input type="hidden" name="desc" value="<?php print substr($descstr,0,255)?>" />
	<input type="hidden" name="name" value="<?php print @$_POST["name"]?>" />
	<input type="hidden" name="address" value="<?php print @$_POST["address"] . (trim(@$_POST["address2"])!='' ? ', ' . unstripslashes(@$_POST["address2"]) : '')?>&#10;<?php print @$_POST["city"]?>&#10;<?php
			if(trim(@$_POST["state"]) != "")
				print @$_POST["state"];
			else
				print @$_POST["state2"]; ?>" />
	<input type="hidden" name="postcode" value="<?php print @$_POST["zip"]?>" />
	<input type="hidden" name="country" value="<?php print $countryCode?>" />
	<input type="hidden" name="tel" value="<?php print @$_POST["phone"]?>" />
	<input type="hidden" name="email" value="<?php print @$_POST["email"]?>" />
	<input type="hidden" name="authMode" value="<?php if($ppmethod==1) print 'E'; else print 'A'; ?>" />
<?php		if($demomode){ ?>
	<input type="hidden" name="testMode" value="100" />
<?php		}
			$data2arr = split("&",$data2);
			$data2 = @$data2arr[0];
			if($data2 != ""){
				print '<input type="hidden" name="signatureFields" value="amount:currency:cartId" />' . "\r\n";
				print '<input type="hidden" name="signature" value="' . md5($data2 . ":" . number_format($grandtotal,2,'.','') . ":" . $countryCurrency . ":" . $orderid) . '" />';
			}
		}elseif($grandtotal > 0 && $ordPayProvider=="6"){ // NOCHEX ?>
	<form method="post" action="https://www.nochex.com/nochex.dll/checkout">
	<input type="hidden" name="email" value="<?php print $data1?>" />
	<input type="hidden" name="returnurl" value="<?php print $storeurl . (TRUE ? 'thanks.php?ncretval=' . $orderid . '&ncsessid=' . $thesessionid : '')?>" />
	<input type="hidden" name="responderurl" value="/admin/ncconfirm.php" />
	<input type="hidden" name="description" value="<?php print substr($descstr,0,255)?>" />
	<input type="hidden" name="ordernumber" value="<?php print $orderid?>" />
	<input type="hidden" name="amount" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="firstline" value="<?php print unstripslashes(@$_POST["address"]) . (trim(@$_POST["address2"])!='' ? ', ' . unstripslashes(@$_POST["address2"]) : '')?>" />
	<input type="hidden" name="town" value="<?php print unstripslashes(@$_POST["city"])?>" />
	<input type="hidden" name="county" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
	<input type="hidden" name="postcode" value="<?php print unstripslashes(@$_POST["zip"])?>" />
	<input type="hidden" name="email_address_sender" value="<?php print unstripslashes(@$_POST["email"])?>" />
<?php	$thename = unstripslashes(trim(@$_POST["name"]));
		if($thename != ""){
			if(strstr($thename," ")){
				$namearr = split(" ",$thename,2);
				print '<input type="hidden" name="firstname" value="' . str_replace('"','&quot;',$namearr[0]) . "\" />\n";
				print '<input type="hidden" name="lastname" value="' . str_replace('"','&quot;',$namearr[1]) . "\" />\n";
			}else
				print '<input type="hidden" name="lastname" value="' . str_replace('"','&quot;',$thename) . "\" />\n";
		}
		if($demomode) print '<input type="hidden" name="status" value="test" />';
		}elseif($grandtotal > 0 && $ordPayProvider=="7"){ // VeriSign Payflow Pro ?>
	<form method="post" action="cart.php" onsubmit="return isvalidcard(this)">
	<input type="hidden" name="mode" value="authorize" />
	<input type="hidden" name="method" value="payflowpro" />
	<input type="hidden" name="ordernumber" value="<?php print $orderid?>" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="8"){ // VeriSign Payflow Link
			$paymentlink = 'https://payments.verisign.com/payflowlink';
			if($data2=="VSA") $paymentlink='https://payments.verisign.com.au/payflowlink'; ?>
	<form method="post" action="<?php print $paymentlink?>" >
	<input type="hidden" name="LOGIN" value="<?php print $data1?>" />
	<input type="hidden" name="PARTNER" value="<?php print $data2?>" />
	<input type="hidden" name="CUSTID" value="<?php print $orderid?>" />
	<input type="hidden" name="AMOUNT" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="TYPE" value="S" />
	<input type="hidden" name="DESCRIPTION" value="<?php print substr($descstr,0,255)?>" />
	<input type="hidden" name="NAME" value="<?php print unstripslashes(@$_POST["name"])?>" />
	<input type="hidden" name="ADDRESS" value="<?php print unstripslashes(@$_POST["address"]) . (trim(@$_POST["address2"])!='' ? ', ' . unstripslashes(@$_POST["address2"]) : '')?>" />
	<input type="hidden" name="CITY" value="<?php print unstripslashes(@$_POST["city"])?>" />
	<input type="hidden" name="STATE" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
	<input type="hidden" name="ZIP" value="<?php print unstripslashes(@$_POST["zip"])?>" />
	<input type="hidden" name="COUNTRY" value="<?php print unstripslashes(@$_POST["country"])?>" />
	<input type="hidden" name="EMAIL" value="<?php print unstripslashes(@$_POST["email"])?>" />
	<input type="hidden" name="PHONE" value="<?php print unstripslashes(@$_POST["phone"])?>" />
	<input type="hidden" name="METHOD" value="CC" />
	<input type="hidden" name="ORDERFORM" value="TRUE" />
	<input type="hidden" name="SHOWCONFIRM" value="FALSE" />
<?php		if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != ""){ ?>
	<input type="hidden" name="NAMETOSHIP" value="<?php print unstripslashes(@$_POST["sname"])?>" />
	<input type="hidden" name="ADDRESSTOSHIP" value="<?php print unstripslashes(@$_POST["saddress"]) . (trim(@$_POST["saddress2"])!='' ? ', ' . unstripslashes(@$_POST["saddress2"]) : '')?>" />
	<input type="hidden" name="CITYTOSHIP" value="<?php print unstripslashes(@$_POST["scity"])?>" />
	<input type="hidden" name="STATETOSHIP" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["sstate"]); else print unstripslashes(@$_POST["sstate2"]);?>" />
	<input type="hidden" name="ZIPTOSHIP" value="<?php print unstripslashes(@$_POST["szip"])?>" />
	<input type="hidden" name="COUNTRYTOSHIP" value="<?php print unstripslashes(@$_POST["scountry"])?>" />
<?php		} ?>
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="9"){ // SECPay ?>
	<form method="post" action="https://www.secpay.com/java-bin/ValCard" >
	<input type="hidden" name="merchant" value="<?php print $data1?>" />
	<input type="hidden" name="trans_id" value="<?php print $orderid?>" />
	<input type="hidden" name="amount" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="callback" value="/admin/wpconfirm.php" />
	<input type="hidden" name="currency" value="<?php print $countryCurrency?>" />
	<input type="hidden" name="cb_post" value="true" />
	<input type="hidden" name="bill_name" value="<?php print unstripslashes(@$_POST["name"])?>" />
	<input type="hidden" name="bill_addr_1" value="<?php print unstripslashes(@$_POST["address"])?>" />
	<input type="hidden" name="bill_addr_2" value="<?php print unstripslashes(@$_POST["address2"])?>" />
	<input type="hidden" name="bill_city" value="<?php print unstripslashes(@$_POST["city"])?>" />
	<input type="hidden" name="bill_state" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
	<input type="hidden" name="bill_post_code" value="<?php print unstripslashes(@$_POST["zip"])?>" />
	<input type="hidden" name="bill_country" value="<?php print unstripslashes(@$_POST["country"])?>" />
	<input type="hidden" name="bill_email" value="<?php print unstripslashes(@$_POST["email"])?>" />
	<input type="hidden" name="bill_tel" value="<?php print unstripslashes(@$_POST["phone"])?>" />
<?php		if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != ""){ ?>
	<input type="hidden" name="ship_name" value="<?php print unstripslashes(@$_POST["sname"])?>" />
	<input type="hidden" name="ship_addr_1" value="<?php print unstripslashes(@$_POST["saddress"])?>" />
	<input type="hidden" name="ship_addr_2" value="<?php print unstripslashes(@$_POST["saddress2"])?>" />
	<input type="hidden" name="ship_city" value="<?php print unstripslashes(@$_POST["scity"])?>" />
	<input type="hidden" name="ship_state" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["sstate"]); else print unstripslashes(@$_POST["sstate2"]);?>" />
	<input type="hidden" name="ship_post_code" value="<?php print unstripslashes(@$_POST["szip"])?>" />
	<input type="hidden" name="ship_country" value="<?php print unstripslashes(@$_POST["scountry"])?>" />
<?php		}
			if($demomode){ ?>
	<input type="hidden" name="test_status" value="true" />
<?php		}
		}elseif($grandtotal > 0 && $ordPayProvider=="10"){ // Capture Card ?>
	<form method="post" action="thanks.php" onsubmit="return isvalidcard(this)" >
	<input type="hidden" name="docapture" value="vsprods" />
	<input type="hidden" name="ordernumber" value="<?php print $orderid?>" />
<?php	}elseif($grandtotal > 0 && ($ordPayProvider=="11" || $ordPayProvider=="12")){ // PSiGate ?>
	<form method="post" action="https://order.psigate.com/psigate.asp" <?php if($ordPayProvider=="12") print 'onsubmit="return isvalidcard(this)"' ?>>
	<input type="hidden" name="MerchantID" value="<?php print $data1?>" />
	<input type="hidden" name="Oid" value="<?php print $orderid?>" />
	<input type="hidden" name="FullTotal" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="ThanksURL" value="<?php print $storeurl?>thanks.php" />
	<input type="hidden" name="NoThanksURL" value="<?php print $storeurl?>thanks.php" />
	<input type="hidden" name="Chargetype" value="<?php if($ppmethod=="1") print "1"; else print "0"; ?>" />
	<?php if($ordPayProvider=="11"){ ?><input type="hidden" name="Bname" value="<?php print unstripslashes(@$_POST["name"])?>" /><?php } ?>
	<input type="hidden" name="Baddr1" value="<?php print unstripslashes(@$_POST["address"])?>" />
	<input type="hidden" name="Baddr2" value="<?php print unstripslashes(@$_POST["address2"])?>" />
	<input type="hidden" name="Bcity" value="<?php print unstripslashes(@$_POST["city"])?>" />
	<input type="hidden" name="IP" value="<?php print @$_SERVER["REMOTE_ADDR"]?>" />
<?php			if($countryID==1 && $stateAbbrev != ""){ ?>
	<input type="hidden" name="Bstate" value="<?php print $stateAbbrev?>" />
<?php			}else{ ?>
	<input type="hidden" name="Bstate" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
<?php			} ?>
	<input type="hidden" name="Bzip" value="<?php print unstripslashes(@$_POST["zip"])?>" />
	<input type="hidden" name="Bcountry" value="<?php print $countryCode?>" />
	<input type="hidden" name="Email" value="<?php print unstripslashes(@$_POST["email"])?>" />
	<input type="hidden" name="Phone" value="<?php print unstripslashes(@$_POST["phone"])?>" />
<?php			if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != ""){ ?>
	<input type="hidden" name="Sname" value="<?php print unstripslashes(@$_POST["sname"])?>" />
	<input type="hidden" name="Saddr1" value="<?php print unstripslashes(@$_POST["saddress"])?>" />
	<input type="hidden" name="Saddr2" value="<?php print unstripslashes(@$_POST["saddress2"])?>" />
	<input type="hidden" name="Scity" value="<?php print unstripslashes(@$_POST["scity"])?>" />
	<input type="hidden" name="Sstate" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["sstate"]); else print unstripslashes(@$_POST["sstate2"]);?>" />
	<input type="hidden" name="Szip" value="<?php print unstripslashes(@$_POST["szip"])?>" />
	<input type="hidden" name="Scountry" value="<?php print unstripslashes(@$_POST["scountry"])?>" />
<?php			}
				if($demomode){ ?>
		<input type="hidden" name="Result" value="1" />
<?php			}
		}elseif($grandtotal > 0 && $ordPayProvider=="13"){ // Authorize.net AIM ?>
<?
// splits the cart page into different funnels
$google_tracker="/cart/payment.php";
// end
?>

	<form method="post" action="cart.php" onsubmit="return isvalidcard(this)" >
	<input type="hidden" name="mode" value="authorize" />
	<input type="hidden" name="method" value="authnetaim" />
	<input type="hidden" name="ordernumber" value="<?php print $orderid?>" />
	<input type="hidden" name="description" value="<?php print substr($descstr,0,254)?>" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="14"){ // Custom Pay Provider
			include(APPPATH.'views/pages/admin/customppsend.php');
		}elseif($grandtotal > 0 && $ordPayProvider=="15"){ // Netbanx ?>
	<form method="post" action="https://www.netbanx.com/cgi-bin/payment/<?php print $data1;?>" >
	<input type="hidden" name="order_id" value="<?php print $orderid?>" />
	<input type="hidden" name="payment_amount" value="<?php print number_format($grandtotal,2,'.','')?>" />
	<input type="hidden" name="currency_code" value="<?php print $countryCurrency?>" />
	<input type="hidden" name="cardholder_name" value="<?php print unstripslashes(@$_POST["name"])?>" />
	<input type="hidden" name="email" value="<?php print unstripslashes(@$_POST["email"])?>" />
	<input type="hidden" name="postcode" value="<?php print unstripslashes(@$_POST["zip"])?>" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="16"){ // Linkpoint
			if($demomode) $theurl='https://staging.linkpt.net/lpc/servlet/lppay'; else $theurl='https://www.linkpointcentral.com/lpc/servlet/lppay';
			$subtotal = round($totalgoods - $totaldiscounts, 2);
			$shipping = round(($shipping + $handling) - $freeshipamnt, 2);
			$tax = round($stateTax + $countryTax, 2);
?>
	<form action="<?php print $theurl?>" method="post"<?php if($data2=="1") print ' onsubmit="return isvalidcard(this)"' ?> >
	<input type="hidden" name="storename" value="<?php print $data1?>" />
	<input type="hidden" name="mode" value="payonly" />
	<input type="hidden" name="ponumber" value="<?php print $orderid?>" />
	<input type="hidden" name="responseURL" value="<?php print $storeurl?>thanks.php" />
	<input type="hidden" name="subtotal" value="<?php print number_format($subtotal,2,'.','')?>" />
	<input type="hidden" name="chargetotal" value="<?php print number_format($subtotal+$shipping+$tax,2,'.','')?>" />
	<input type="hidden" name="shipping" value="<?php print number_format($shipping,2,'.','')?>" />
	<input type="hidden" name="tax" value="<?php print number_format($tax,2,'.','')?>" />
	<?php if($data2!="1"){ ?><input type="hidden" name="bname" value="<?php print unstripslashes(@$_POST["name"])?>" /><?php } ?>
	<input type="hidden" name="baddr1" value="<?php print unstripslashes(@$_POST["address"])?>" />
	<input type="hidden" name="baddr2" value="<?php print unstripslashes(@$_POST["address2"])?>" />
	<input type="hidden" name="bcity" value="<?php print unstripslashes(@$_POST["city"])?>" />
<?php		if($countryID==1 && $stateAbbrev != ""){ ?>
		<input type="hidden" name="bstate" value="<?php print $stateAbbrev?>" />
<?php		}else{ ?>
		<input type="hidden" name="bstate" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["state"]); else print unstripslashes(@$_POST["state2"]);?>" />
<?php		} ?>
	<input type="hidden" name="bzip" value="<?php print unstripslashes(@$_POST["zip"])?>" />
	<input type="hidden" name="bcountry" value="<?php print $countryCode?>" />
	<input type="hidden" name="email" value="<?php print unstripslashes(@$_POST["email"])?>" />
	<input type="hidden" name="phone" value="<?php print unstripslashes(@$_POST["phone"])?>" />
	<input type="hidden" name="txntype" value="<?php if($ppmethod==1) print "preauth"; else print "sale" ?>" />
<?php		if(trim(@$_POST["sname"]) != "" || trim(@$_POST["saddress"]) != ""){ ?>
	<input type="hidden" name="sname" value="<?php print unstripslashes(@$_POST["sname"])?>" />
	<input type="hidden" name="saddr1" value="<?php print unstripslashes(@$_POST["saddress"])?>" />
	<input type="hidden" name="saddr2" value="<?php print unstripslashes(@$_POST["saddress2"])?>" />
	<input type="hidden" name="scity" value="<?php print unstripslashes(@$_POST["scity"])?>" />
	<input type="hidden" name="sstate" value="<?php if(trim(@$_POST["state"]) != "") print unstripslashes(@$_POST["sstate"]); else print unstripslashes(@$_POST["sstate2"]);?>" />
	<input type="hidden" name="szip" value="<?php print unstripslashes(@$_POST["szip"])?>" />
	<input type="hidden" name="scountry" value="<?php print $shipCountryCode?>" />
<?php		}
			if($demomode){ ?>
	<input type="hidden" name="txnmode" value="test" />
<?php		}
		}elseif($grandtotal > 0 && $ordPayProvider=="18"){ // PayPal Direct Payment ?>
	<form method="post" action="cart.php" onsubmit="return isvalidcard(this)" >
	<input type="hidden" name="mode" value="authorize" />
	<input type="hidden" name="method" value="paypalpro" />
	<input type="hidden" name="ordernumber" value="<?php print $orderid?>" />
	<input type="hidden" name="description" value="<?php print str_replace('"','&quot;',substr($descstr,0,254))?>" />
<?php	}elseif($grandtotal > 0 && $ordPayProvider=="19"){ // PayPal Express Payment ?>
	<form method="post" action="thanks.php" >
	<input type="hidden" name="token" value="<?php print $token?>" />
	<input type="hidden" name="method" value="paypalexpress" />
	<input type="hidden" name="ordernumber" value="<?php print $orderid?>" />
	<input type="hidden" name="payerid" value="<?php print $payerid?>" />
	<input type="hidden" name="email" value="<?php print $ordEmail?>" />
<?php	}
	}
	if($success){
?>
	  <div id="cart_nav"><img  src="/lib/images/new_images/subnav_gray_13.gif" alt="View Order" /><img src="/lib/images/new_images/subnav_gray_15.gif" alt="Customer Info" /><img src="/lib/images/new_images/subnav_green_17.gif" alt="Final Review" /><img src="/lib/images/new_images/subnav_gray_19.gif" alt="Confirmation" /><img src="/lib/images/new_images/subnav_gray_21.gif" alt="View Receipt" /></div>
            <table class="cobtbl" width="<?=$maintablewidth?>" border="0" bordercolor="#B1B1B1" cellspacing="1" cellpadding="3" bgcolor="#B1B1B1">
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" colspan="2" align="center"><strong><?php print $xxChkCmp?></strong></td>
			  </tr>
<?php if($cpncode!="" && ! $gotcpncode){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxGifCer?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><font size="1"><?php
							if(@$_POST["shipping"]=="") $jumpback=1; else $jumpback=2;
								printf($xxNoGfCr,$cpncode,$jumpback);?></font></td>
			  </tr>
<?php }
	  if($cpnmessage!=""){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxAppDs?>:</strong></strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print $cpnmessage?></td>
			  </tr>
<?php }
//HCS - DRE 03/07/05 Gift Cert Mod START
	  	if($gcmessage!=""){ ?>
		  	  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong>Gift Certificate Code:</strong></b></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print $gcmessage?></td>
			  </tr>
<?php }
//HCS - DRE 03/07/05 Gift Cert Mod STOP
?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxTotGds?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($totalgoods)?></td>
			  </tr>
<?php if($shipType != 0){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxShippg?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($shipping)?></td>
			  </tr>
<?php }
	  if($handling != 0){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxHndlg?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($handling)?></td>
			  </tr>
<?php }
	  if(($totaldiscounts + $freeshipamnt) !=0){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxTotDs?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><font color="#FF0000"><?php print FormatEuroCurrency($totaldiscounts+$freeshipamnt)?></font></td>
			  </tr>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxSubTot?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency(($totalgoods+$shipping+$handling)-($totaldiscounts+$freeshipamnt))?></td>
			  </tr>
<?php }
	  if($usehst){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxHST?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($stateTax+$countryTax)?></td>
			  </tr>
<?php }else{
		if($stateTax != 0.0){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxStaTax?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($stateTax)?></td>
			  </tr>
<?php	}
		if($countryTax != 0.0){ ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxCntTax?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($countryTax)?></td>
			  </tr>
<?php	}
	  }?>
<?php 			  //HCS - DRE 03/02/05 Gift Cert Mod START
		if($pendingamt != 0) {?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong>Gift Certificate Amount: </strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($pendingamt)?></td>
			  </tr>
<?php 		}
			  //HCS - DRE 03/02/05 Gift Cert Mod STOP
?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong><?php print $xxGndTot?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%"><?php print FormatEuroCurrency($grandtotal)?></td>
			  </tr>
			  <? if($ordPayProvider=="14"){?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" align="right" width="50%"><strong>Terms:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" height="30" align="left" width="50%">Net 30 </td>
			  </tr>
			  <? } ?>

<?php if($grandtotal > 0 && ($ordPayProvider=="7" || $ordPayProvider=="10" || $ordPayProvider=="12" || $ordPayProvider=="13" || ($ordPayProvider=="16" && $data2=="1") || $ordPayProvider=="18")){ // VeriSign Payflow Pro || Capture Card || PSiGate || Auth.NET AIM || PayPal Pro
			if($ordPayProvider=="7" || $ordPayProvider=="12" || $ordPayProvider=="13" || $ordPayProvider=="16" || $ordPayProvider=="18") $data1 = "XXXXXXX0XXXXXXXXXXXXXXXXX";
			$isPSiGate = ($ordPayProvider=="12");
			$isLinkpoint = ($ordPayProvider=="16");
			if($isPSiGate){
				$sscardname="bname";
				$sscardnum = "CardNumber";
				$ssexmon = "ExpMonth";
				$ssexyear = "ExpYear";
			}elseif($isLinkpoint){
				$sscardname="bname";
				$sscardnum = "cardnumber";
				$ssexmon = "expmonth";
				$ssexyear = "expyear";
				$sscvv2 = "cvm";
			}else{
				$sscardname="cardname";
				$sscardnum = "ACCT";
				$ssexmon = "EXMON";
				$ssexyear = "EXYEAR";
				$sscvv2 = "CVV2";
			}
			$acceptecheck = ((@$acceptecheck==true) && ($ordPayProvider=="13"));
?>
<script language="JavaScript" type="text/javascript">
<!--
var isswitchcard=false;
function isCreditCard(st){
  // Encoding only works on cards with less than 19 digits
  if (st.length > 19)
    return (false);

  sum = 0; mul = 1; l = st.length;
  for (i = 0; i < l; i++) {
    digit = st.substring(l-i-1,l-i);
    tproduct = parseInt(digit ,10)*mul;
    if (tproduct >= 10)
      sum += (tproduct % 10) + 1;
    else
      sum += tproduct;
    if (mul == 1)
      mul++;
    else
      mul = mul - 1;
  }
  if ((sum % 10) == 0)
    return (true);
  else
    return (false);

}
function isVisa(cc){ // 4111 1111 1111 1111
  if (((cc.length == 16) || (cc.length == 13)) && (cc.substring(0,1) == 4))
    return isCreditCard(cc);
  return false;
}
function isMasterCard(cc){ // 5500 0000 0000 0004
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 16) && (firstdig == 5) && ((seconddig >= 1) && (seconddig <= 5)))
    return isCreditCard(cc);
  return false;
}
function isAmericanExpress(cc){ // 340000000000009
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 15) && (firstdig == 3) && ((seconddig == 4) || (seconddig == 7)))
    return isCreditCard(cc);
  return false;
}
function isDinersClub(cc){ // 30000000000004
  firstdig = cc.substring(0,1);
  seconddig = cc.substring(1,2);
  if ((cc.length == 14) && (firstdig == 3) &&
      ((seconddig == 0) || (seconddig == 6) || (seconddig == 8)))
    return isCreditCard(cc);
  return false;
}
function isDiscover(cc){ // 6011000000000004
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) && (first4digs == "6011"))
    return isCreditCard(cc);
  return false;
}
function isAusBankcard(cc){ // 5610591000000009
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) && (first4digs == "5610"))
    return isCreditCard(cc);
  return false;
}
function isEnRoute(cc){ // 201400000000009
  first4digs = cc.substring(0,4);
  if ((cc.length == 15) && ((first4digs == "2014") || (first4digs == "2149")))
    return isCreditCard(cc);
  return false;
}
function isJCB(cc){
  first4digs = cc.substring(0,4);
  if ((cc.length == 16) && ((first4digs == "3088") || (first4digs == "3096") || (first4digs == "3112") || (first4digs == "3158") || (first4digs == "3337") || (first4digs == "3528")))
    return isCreditCard(cc);
  return false;
}
function isSwitch(cc){ // 675911111111111128
  first4digs = cc.substring(0,4);
  if ((cc.length == 16 || cc.length == 17 || cc.length == 18 || cc.length == 19) && ((first4digs == "4903") || (first4digs == "4911") || (first4digs == "4936") || (first4digs == "5641") || (first4digs == "6333") || (first4digs == "6759") || (first4digs == "6334") || (first4digs == "6767"))){
    isswitchcard=isCreditCard(cc);
    return(isswitchcard);
  }
  return false;
}
function isvalidcard(theForm){
  cc = theForm.<?php print $sscardnum?>.value;
  newcode = "";
  l = cc.length;
  for(i=0;i<l;i++){
	digit = cc.substring(i,i+1);
	digit = parseInt(digit ,10);
	if(!isNaN(digit)) newcode += digit;
  }
  cc=newcode;
  if (theForm.<?php print $sscardname?>.value==""){
	alert("<?php print $xxPlsEntr . ' \"' . $xxCCName . '\"' ?>");
	theForm.<?php print $sscardname?>.focus();
    return false;
  }
<?php if($acceptecheck==true){ ?>
if(cc!="" && theForm.accountnum.value!=""){
alert("Please enter either Credit Card OR ECheck details");
return(false);
}else if(theForm.accountnum.value!=""){
  if(theForm.accountname.value==""){
    alert("Please enter a value in the field \"Account Name\".");
	theForm.accountname.focus();
    return false;
  }
  if(theForm.bankname.value==""){
    alert("Please enter a value in the field \"Bank Name\".");
	theForm.bankname.focus();
    return false;
  }
  if(theForm.routenumber.value==""){
    alert("Please enter a value in the field \"Routing Number\".");
	theForm.routenumber.focus();
    return false;
  }
  if(theForm.accounttype.selectedIndex==0){
    alert("Please select your account type: (Checking / Savings).");
	theForm.accounttype.focus();
    return false;
  }
<?php	if(@$wellsfargo==true){ ?>
  if(theForm.orgtype.selectedIndex==0){
    alert("Please select your account type: (Personal / Business).");
	theForm.orgtype.focus();
    return false;
  }
  if(theForm.taxid.value=="" && theForm.licensenumber.value==""){
    alert("Please enter either a Tax ID number or Drivers License Details.");
	theForm.taxid.focus();
    return false;
  }
  if(theForm.taxid.value==""){
	  if(theForm.licensestate.selectedIndex==0){
		alert("Please select your Drivers License State.");
		theForm.licensestate.focus();
		return false;
	  }
	  if(theForm.dldobmon.selectedIndex==0){
		alert("Please select your Drivers License D.O.B. Month.");
		theForm.dldobmon.focus();
		return false;
	  }
	  if(theForm.dldobday.selectedIndex==0){
		alert("Please select your Drivers License D.O.B. Day.");
		theForm.dldobday.focus();
		return false;
	  }
	  if(theForm.dldobyear.selectedIndex==0){
		alert("Please select your Drivers License D.O.B. year.");
		theForm.dldobyear.focus();
		return false;
	  }
  }
<?php	} ?>
}else{
<?php } ?>
  if (true <?php
		if(substr($data1,0,1)=="X") print "&& !isVisa(cc) ";
		if(substr($data1,1,1)=="X") print "&& !isMasterCard(cc) ";
		if(substr($data1,2,1)=="X") print "&& !isAmericanExpress(cc) ";
		if(substr($data1,3,1)=="X") print "&& !isDinersClub(cc) ";
		if(substr($data1,4,1)=="X") print "&& !isDiscover(cc) ";
		if(substr($data1,5,1)=="X") print "&& !isEnRoute(cc) ";
		if(substr($data1,6,1)=="X") print "&& !isJCB(cc) ";
		if(substr($data1,7,1)=="X") print "&& !isSwitch(cc) ";
		if(substr($data1,8,1)=="X") print "&& !isAusBankcard(cc) "; ?>){
	<?php if($acceptecheck==true) $xxValCC="Please enter a valid credit card number or bank account details if paying by ECheck."; ?>
	alert("<?php print $xxValCC?>");
	theForm.<?php print $sscardnum?>.focus();
    return false;
  }
  if(theForm.<?php print $ssexmon?>.selectedIndex==0){
    alert("<?php print $xxCCMon?>");
	theForm.<?php print $ssexmon?>.focus();
    return false;
  }
  if(theForm.<?php print $ssexyear?>.selectedIndex==0){
    alert("<?php print $xxCCYear?>");
	theForm.<?php print $ssexyear?>.focus();
    return false;
  }
<?php if(substr($data1,7,1)=="X"){ ?>
  if(theForm.IssNum.value=="" && isswitchcard){
    alert("Please enter an issue number / start date for Switch/Solo cards.");
	theForm.IssNum.focus();
    return false;
  }
<?php }
	  if(@$requirecvv==TRUE){ ?>
  if(theForm.<?php print $sscvv2?>.value==""){
    alert("<?php print $xxPlsEntr . ' \"' . str_replace('"','\"',$xx34code) . '\"'?>");
	theForm.<?php print $sscvv2?>.focus();
    return false;
  }
<?php }
	  if(@$acceptecheck==true) print '}'; ?>
  return true;
}
//-->
</script>

<?php if(@$_SERVER["HTTPS"] != "on" && (@$_SERVER["SERVER_PORT"] != "443") && @$nochecksslserver != TRUE){ ?>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="center" colspan="2"><strong><font color="#FF0000">This site may not be secure. Do not enter real Credit Card numbers.</font></strong></td>
			  </tr>
<?php } ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" colspan="2" align="center"><strong><?php print $xxCCDets ?></strong></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxCCName?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="<?php print $sscardname?>" size="<?php print atb(21)?>" value="<?php print @$_POST["name"]?>" autocomplete="off" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxCrdNum?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="<?php print $sscardnum?>" size="<?php print atb(21)?>" autocomplete="off" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxExpEnd?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%">
				  <select name="<?php print $ssexmon?>" size="1">
					<option value=""><?php print $xxMonth?></option>
					<?php	for($index=1; $index<=12; $index++){
								if($index < 10) $themonth = "0" . $index; else $themonth = $index;
								print "<option value='" . $themonth . "'>" . $themonth . "</option>\n";
							} ?>
				  </select> / <select name="<?php print $ssexyear?>" size="1">
					<option value=""><?php print $xxYear?></option>
					<?php	$thisyear=date("Y", time());
							for($index=$thisyear; $index <= $thisyear+10; $index++){
								if($isPSiGate)
									print "<option value='" . substr($index,-2) . "'>" . $index . "</option>\n";
								else
									print "<option value='" . $index . "'>" . $index . "</option>\n";
							} ?>
				  </select>
				</td>
			  </tr>
<?php			if(! $isPSiGate){ ?>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xx34code?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="<?php print $sscvv2?>" size="<?php print atb(4)?>" autocomplete="off" /> <strong><?php if(@$requirecvv!=TRUE)print $xxIfPres?></strong> <a href="../sec_codes.html" onclick="window.open(this.href,'ccv','directories=no,width=600,height=400,top=20,left=20,location=no,status=no,toolbar=no,resizable=yes',false); return false;">Example</a></td>
			  </tr>
<?php			}
				if(substr($data1,7,1)=="X"){ ?>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Issue Number / Start Date:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="IssNum" size="<?php print atb(4)?>" autocomplete="off" /> <strong>(Switch/Solo Only)</strong></td>
			  </tr>
<?php			}
				if($acceptecheck==true){ // Auth.net ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" colspan="2" align="center"><strong>ECheck Details</strong><br /><font size="1">Please enter either Credit Card OR ECheck details</font></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Account Name:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="accountname" size="<?php print atb(21)?>" autocomplete="off" value="<?php print @$_POST["name"]?>" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Account Number:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="accountnum" size="<?php print atb(21)?>" autocomplete="off" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Bank Name:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="bankname" size="<?php print atb(21)?>" autocomplete="off" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Routing Number:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="routenumber" size="<?php print atb(10)?>" autocomplete="off" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Account Type:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><select name="accounttype" size="1"><option value=""><?php print $xxPlsSel?></option><option value="CHECKING">Checking</option><option value="SAVINGS">Savings</option><option value="BUSINESSCHECKING">Business Checking</option></select></td>
			  </tr>
<?php				if(@$wellsfargo==true){ ?>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Personal or Business Acct.:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><select name="orgtype" size="1"><option value=""><?php print $xxPlsSel?></option><option value="I">Personal</option><option value="B">Business</option></select></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Tax ID:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="taxid" size="<?php print atb(21)?>" autocomplete="off" /></td>
			  </tr>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" colspan="2" align="center"><font size="1">If you have provided a Tax ID then the following information is not necessary</font></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Drivers License Number:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><input type="text" name="licensenumber" size="<?php print atb(21)?>" autocomplete="off" /></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Drivers License State:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%"><select size="1" name="licensestate"><option value=""><?php print $xxPlsSel?></option><?php
				$sSQL = "SELECT stateName,stateAbbrev FROM states WHERE stateEnabled=1 ORDER BY stateName";
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs = mysql_fetch_array($result)){
					print '<option value="' . str_replace('"','&quot;',$rs["stateAbbrev"]) . '"';
					print '>' . $rs["stateAbbrev"] . "</option>\n";
				}
				mysql_free_result($result); ?></select></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong>Date Of Birth On License:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="left" width="50%">
				  <select name="dldobmon" size="1">
					<option value=""><?php print $xxMonth?></option>
					<?php for($index=1; $index <= 12; $index++){ ?>
					<option value="<?php print $index?>"><?php print date("M", mktime(1,0,0,$index,1,1990))?></option>
					<?php } ?>
				  </select>
				  <select name="dldobday" size="1">
					<option value="">Day</option>
					<?php for($index=1; $index <= 31; $index++){ ?>
					<option value="<?php print $index?>"><?php print $index?></option>
					<?php } ?>
				  </select>
				  <select name="dldobyear" size="1">
					<option value=""><?php print $xxYear?></option>
					<?php $thisyear = date("Y");
						  for($index=$thisyear-100; $index <= $thisyear; $index++){ ?>
					<option value="<?php print $index?>"><?php print $index?></option>
					<?php } ?>
				  </select>
				</td>
			  </tr>
<?php				}
				}
	} ?>
			  <tr>
			    <td class="cobhl" bgcolor="#EBEBEB" height="30" colspan="2" align="center"><strong><?php print $xxMstClk?></strong></td>
			  </tr>
			  <tr>
				<td class="cobll" bgcolor="#FFFFFF" colspan="2" align="center"><table width="100%" cellspacing="0" cellpadding="0" border="0">
				    <tr>
					  <td class="cobll" bgcolor="#FFFFFF" width="16" height="26" align="right" valign="bottom">&nbsp;</td>
					  <td class="cobll" bgcolor="#FFFFFF" width="100%" align="center"><input type="image" src="/lib/images/submit_order.gif" border="0" /></td>
					  <td class="cobll" bgcolor="#FFFFFF" width="16" height="26" align="right" valign="bottom"><img src="/lib/images/tablebr.gif" alt="" /></td>
					</tr>
				  </table></td>
			  </tr>
			</table>
	</form>
<?php
	} // success
}elseif(@$_POST["mode"]=="authorize"){
	// COUPON MANAGEMENT ADDED by Chad 08/09/06
	// Double check that all coupons are set to zero
	for($j=0; $j<count($_SESSION['chads_coupons']); $j++) {
		$cc_sql = "SELECT * FROM coupons WHERE cpnID = ".$_SESSION['chads_coupons'][$j]['cpnID']." AND cpnNumAvail = 1";
		$cc_res = mysql_query($cc_sql) or print(mysql_error());
		if(mysql_num_rows($cc_res) > 0) {
			$sql2 = "UPDATE coupons SET cpnNumAvail = 0 WHERE cpnID = ".$_SESSION['chads_coupons'][$j]['cpnID'];
			$res2 = mysql_query($sql2) or print(mysql_error());
		}
	}
	// COUPON MANAGEMENT ENDED

	$blockuser=checkuserblock("");
	$ordID = mysql_real_escape_string(str_replace("'","",@$_POST["ordernumber"]));
	$vsRESULT="x";
	$vsRESPMSG=$vsAVSADDR=$vsAVSZIP=$vsTRANSID='';
	$gobackplaces='-1';
	if(@$_POST["method"]=="payflowpro"){
		$sSQL = "SELECT payProvData1,payProvDemo,payProvMethod FROM payprovider WHERE payProvID=7";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_assoc($result);
		$vsdetails = $rs["payProvData1"];
		$demomode=((int)$rs["payProvDemo"]==1);
		$ppmethod = (int)$rs["payProvMethod"];
		mysql_free_result($result);
		if(is_null($vsdetails)) $vsdetails="";
		$vsdetails = split("&", $vsdetails);
		$vs1=@$vsdetails[0];
		$vs2=@$vsdetails[1];
		$vs3=@$vsdetails[2];
		$vs4=@$vsdetails[3];
		$sSQL = "SELECT ordZip,ordShipping,ordStateTax,ordCountryTax,ordHandling,ordTotal,ordDiscount,ordAddress,ordAddress2,ordAuthNumber FROM orders WHERE ordID='" . $ordID . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_assoc($result);
		$vsAUTHCODE = $rs["ordAuthNumber"];
		if(@$pathtopfpro==""){
			$parmList = array( "TRXTYPE"=>($ppmethod==1 ? "A" : "S"),
								"TENDER"=>"C",
								"ZIP" => $rs["ordZip"],
								"STREET" => $rs["ordAddress"],
								"NAME" => @$_POST["cardname"],
								"COMMENT1" => $ordID,
								"ACCT" => @$_POST["ACCT"],
								"PWD" => $vs4,
								"USER" => $vs1,
								"VENDOR" => $vs2,
								"PARTNER" => $vs3,
								"CVV2" => trim(@$_POST["CVV2"]),
								"EXPDATE" => @$_POST["EXMON"] . substr(@$_POST["EXYEAR"], -2),
								"AMT" => number_format(($rs["ordShipping"]+$rs["ordStateTax"]+$rs["ordCountryTax"]+$rs["ordTotal"]+$rs["ordHandling"])-$rs["ordDiscount"],2,'.','')
							);
		}else{
			$parmList = "TRXTYPE=" . ($ppmethod==1 ? "A" : "S") . "&TENDER=C";
			$parmList .= "&ZIP[" . strlen($rs["ordZip"]) . "]=" . $rs["ordZip"];
			$parmList .= "&STREET[" . strlen($rs["ordAddress"]) . "]=" . $rs["ordAddress"];
			if($rs["ordAddress2"] != "") $parmList .= ', ' . $rs["ordAddress2"];
			$parmList .= "&NAME[" . strlen(@$_POST["cardname"]) . "]=" . @$_POST["cardname"];
			$parmList .= "&COMMENT1=" . $ordID;
			$parmList .= "&ACCT=" . @$_POST["ACCT"];
			$parmList .= "&PWD=" . $vs4;
			$parmList .= "&USER=" . $vs1;
			$parmList .= "&VENDOR=" . $vs2;
			$parmList .= "&PARTNER=" . $vs3;
			$parmList .= "&CVV2=" . trim(@$_POST["CVV2"]);
			$parmList .= "&EXPDATE=" . @$_POST["EXMON"] . substr(@$_POST["EXYEAR"], -2);
			$parmList .= "&AMT=" . number_format(($rs["ordShipping"]+$rs["ordStateTax"]+$rs["ordCountryTax"]+$rs["ordTotal"]+$rs["ordHandling"])-$rs["ordDiscount"],2,'.','');
		}
		mysql_free_result($result);
		function process_pfpro($str, $server, $port, $timeout){
			global $pathtopfpro,$pathtopfprocert,$pathtopfprolib,$parmList;
			if(@$pathtopfprocert!="")
				putenv("PFPRO_CERT_PATH=$pathtopfprocert");
			if(@$pathtopfpro=="COM"){
				$objCOM = new COM("PFProCOMControl.PFProCOMControl.1");
				$ctx1 = $objCOM->CreateContext($server, $port, $timeout, "", 0, "", "");
				$pfret = $objCOM->SubmitTransaction($ctx1, $str, strlen($str));
				$objCOM->DestroyContext($ctx1);
			}elseif(@$pathtopfpro!=""){
				if(! file_exists($pathtopfpro)) print "cannot find pfpro executable. Check \$pathtopfpro<br>";
				if(@$pathtopfprolib!="")
					putenv("LD_LIBRARY_PATH=$pathtopfprolib");
				$sendstr = $pathtopfpro . ' ' . $server . ' ' . $port . ' "' . $str . '" ' . $timeout;
				exec ($sendstr, $pfret, $retvar);
				$pfret = implode("\n",$pfret);
			}else{
				$pfret = pfpro_process($parmList, $server);
			}
			return $pfret;
		}
		if($vsAUTHCODE==""){
			if($vs3=="VSA")
				if($demomode) $theurl = "payflow-test.verisign.com.au"; else $theurl = "payflow.verisign.com.au";
			else
				if($demomode) $theurl = "test-payflow.verisign.com"; else $theurl = "payflow.verisign.com";
			$curString = process_pfpro($parmList, $theurl, 443, 30);
			if(!is_array($curString)){
				$curStringArr = array();
				while(strlen($curString) != 0){
					if(strpos($curString,"&")!==FALSE)
						$varString = substr($curString, 0, strpos($curString , "&" ));
					else
						$varString = $curString;
					$name = substr($varString, 0, strpos($varString, "=" ));
					$curStringArr[$name] = substr($varString, (strlen($name)+1) - strlen($varString));
					if(strlen($curString) != strlen($varString))
						$curString = substr($curString,  (strlen($varString)+1) - strlen($curString));
					else
						$curString = "";
				}
				$curString = $curStringArr;
			}
			$vsRESULT=$curString["RESULT"];
			$vsPNREF=@$curString["PNREF"];
			$vsRESPMSG=@$curString["RESPMSG"];
			$vsAUTHCODE=@$curString["AUTHCODE"];
			$vsAVSADDR=@$curString["AVSADDR"];
			$vsAVSZIP=@$curString["AVSZIP"];
			$vsIAVS=@$curString["IAVS"];
			if($vsRESULT=="0"){
				do_stock_management($ordID);
				$sSQL="UPDATE cart SET cartCompleted=1 WHERE cartOrderID='" . $ordID . "'";
				mysql_query($sSQL) or print(mysql_error());
				//send to back order status
				$orderStatus=3;
				if(isAddInfo($ordID)) {
					$orderStatus = 5;
				}
				if($_SESSION['isExtendShipping']) $orderStatus=4;
				$sSQL="UPDATE orders SET ordStatus=".$orderStatus.",ordAuthNumber='" . mysql_real_escape_string($vsAVSADDR . $vsAVSZIP . "-" . $vsAUTHCODE) . "' WHERE ordID='" . $ordID . "'";
				if(mysql_query($sSQL)) {
					if(!setNewLocation( $orderStatus , $ordID )) print("Unable to record new location");
				}else{
					print(mysql_error());
				}
			}
		}else{
			$vsRESULT="0";
			$vsRESPMSG="Approved";
			if(strpos($vsAUTHCODE,"-") > 0) $vsAUTHCODE = substr($vsAUTHCODE, strpos($vsAUTHCODE,"-"));
		}
	}elseif(@$_POST["method"]=="authnetaim"){ // AUTHNET AIM
		$sSQL = "SELECT payProvDemo,payProvData1,payProvData2,payProvMethod FROM payprovider WHERE payProvID=13";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_array($result);
		$demomode = ((int)$rs["payProvDemo"]==1);
		$login = $rs["payProvData1"];
		$trankey = $rs["payProvData2"];
		if(@$secretword != ""){
			$login = upsdecode($login, $secretword);
			$trankey = upsdecode($trankey, $secretword);
		}
		$ppmethod = (int)$rs["payProvMethod"];
		mysql_free_result($result);
		$sSQL = "SELECT ordID,ordName,ordCity,ordState,ordCountry,ordPhone,ordHandling,ordZip,ordEmail,ordShipping,ordStateTax,ordCountryTax,ordTotal,ordDiscount,ordAddress,ordAddress2,ordIP,ordAuthNumber,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipCountry,ordShipZip,ord_cert_amt FROM orders WHERE ordID='" . $ordID . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_array($result);
		mysql_free_result($result);

		if(empty($rs["ordShipName"]) && empty($rs["ordShipAddress"])) {
			$aName = explode(" ",$rs["ordName"]);
			$an_ship_fname = $aName[0];
			$an_ship_lname = $aName[1];
			$an_ship_address = $rs["ordAddress"]." ".$rs["ordAddress2"];
			$an_ship_city = $rs["ordCity"];
			$an_ship_state = $rs["ordState"];
			$an_ship_zip = $rs["ordZip"];
			$an_ship_country = $rs["ordCountry"];
		}else{
			$aName = explode(" ",$rs["ordShipName"]);
			$an_ship_fname = $aName[0];
			$an_ship_lname = $aName[1];
			$an_ship_address = $rs["ordShipAddress"]." ".$rs["ordShipAddress2"];
			$an_ship_city = $rs["ordShipCity"];
			$an_ship_state = $rs["ordShipState"];
			$an_ship_zip = $rs["ordShipZip"];
			$an_ship_country = $rs["ordShipCountry"];
		}

		$vsAUTHCODE = trim($rs["ordAuthNumber"]);
		$parmList = 'x_version=3.1&x_delim_data=True&x_relay_response=False&x_delim_char=|&x_duplicate_window=15';
		$parmList .= "&x_login=" . $login;
		$parmList .= "&x_tran_key=" . $trankey;
		$parmList .= "&x_cust_id=" . $rs["ordID"];
		$parmList .= "&x_Invoice_Num=" . $rs["ordID"];
		$parmList .= "&x_amount=" . number_format(($rs["ordShipping"]+$rs["ordStateTax"]+$rs["ordCountryTax"]+$rs["ordTotal"]+$rs["ordHandling"])-$rs["ordDiscount"]-$rs["ord_cert_amt"],2,'.','');
		$parmList .= "&x_currency_code=" . $countryCurrency;
		$parmList .= "&x_Description=" . urlencode(unstripslashes(@$_POST["description"]));
		$parmList .= "&x_customer_ip=" . $_SERVER['REMOTE_ADDR'];
		$parmList .= "&x_ship_to_first_name=" . $an_ship_fname;
		$parmList .= "&x_ship_to_last_name=" . $an_ship_lname;
		$parmList .= "&x_ship_to_address=" . $an_ship_address;
		$parmList .= "&x_ship_to_city=" . $an_ship_city;
		$parmList .= "&x_ship_to_state=" . $an_ship_state;
		$parmList .= "&x_ship_to_zip=" . $an_ship_zip;
		$parmList .= "&x_ship_to_country=" . $an_ship_country;
		if(trim(@$_POST["accountnum"]) != ""){
			$parmList .= "&x_method=ECHECK&x_echeck_type=WEB&x_recurring_billing=NO";
			$parmList .= "&x_bank_acct_name=" . urlencode(trim(unstripslashes(@$_POST["accountname"])));
			$parmList .= "&x_bank_acct_num=" . urlencode(trim(@$_POST["accountnum"]));
			$parmList .= "&x_bank_name=" . urlencode(trim(unstripslashes(@$_POST["bankname"])));
			$parmList .= "&x_bank_aba_code=" . urlencode(trim(@$_POST["routenumber"]));
			$parmList .= "&x_bank_acct_type=" . urlencode(trim(@$_POST["accounttype"]));
			$parmList .= "&x_type=AUTH_CAPTURE";
			if(@$wellsfargo==true){
				$parmList .= "&x_customer_organization_type=" . trim(@$_POST["orgtype"]);
				if(trim(@$_POST["taxid"]) != ""){
					$parmList .= "&x_customer_tax_id=" . urlencode(trim(@$_POST["taxid"]));
				}else{
					$parmList .= "&x_drivers_license_num=" . urlencode(trim(@$_POST["licensenumber"]));
					$parmList .= "&x_drivers_license_state=" . urlencode(trim(@$_POST["licensestate"]));
					$parmList .= "&x_drivers_license_dob=" . urlencode(trim(@$_POST["dldobyear"]) . "/" . trim(@$_POST["dldobmon"]) . "/" . trim(@$_POST["dldobday"]));
				}
			}
		}else{
			$parmList .= "&x_card_num=" . urlencode(@$_POST["ACCT"]);
			$parmList .= "&x_exp_date=" . @$_POST["EXMON"] . @$_POST["EXYEAR"];
			if(trim(@$_POST["CVV2"]) != "") $parmList .= "&x_card_code=" . trim(@$_POST["CVV2"]);
			if($ppmethod==1) $parmList .= "&x_type=AUTH_ONLY"; else $parmList .= "&x_type=AUTH_CAPTURE";
		}
		$thename = trim(unstripslashes(@$_POST["cardname"]));
		if($thename != ""){
			if(strstr($thename," ")){
				$namearr = split(" ",$thename,2);
				$parmList .= "&x_first_name=" . urlencode($namearr[0]);
				$parmList .= "&x_last_name=" . urlencode($namearr[1]);
			}else
				$parmList .= "&x_last_name=" . urlencode($thename);
		}
		$parmList .= "&x_address=" . urlencode($rs["ordAddress"]);
		if($rs["ordAddress2"] != '') $parmList .= urlencode(', ' . $rs["ordAddress2"]);
		$parmList .= "&x_city=" . urlencode($rs["ordCity"]);
		$parmList .= "&x_state=" . urlencode($rs["ordState"]);
		$parmList .= "&x_zip=" . urlencode($rs["ordZip"]);
		$parmList .= "&x_country=" . urlencode($rs["ordCountry"]);
		$parmList .= "&x_phone=" . urlencode($rs["ordPhone"]);
		$parmList .= "&x_email=" . urlencode($rs["ordEmail"]);
		$thename = trim($rs["ordShipName"]);
		if($thename != "" || $rs["ordShipAddress"] != ""){
			if($thename != ""){
				if(strstr($thename," ")){
					$namearr = split(" ",$thename,2);
					$parmList .= "&x_ship_to_first_name=" . urlencode($namearr[0]);
					$parmList .= "&x_ship_to_last_name=" . urlencode($namearr[1]);
				}else
					$parmList .= "&x_ship_to_last_name=" . urlencode($thename);
			}
			$parmList .= "&x_ship_to_address=" . urlencode($rs["ordShipAddress"]);
			if($rs["ordShipAddress2"] != '') $parmList .= urlencode(', ' . $rs["ordShipAddress2"]);
			$parmList .= "&x_ship_to_city=" . urlencode($rs["ordShipCity"]);
			$parmList .= "&x_ship_to_state=" . urlencode($rs["ordShipState"]);
			$parmList .= "&x_ship_to_zip=" . urlencode($rs["ordShipZip"]);
			$parmList .= "&x_ship_to_country=" . urlencode($rs["ordShipCountry"]);
		}
		if(trim($rs["ordIP"]) != "") $parmList .= "&x_customer_ip=" . urlencode(trim($rs["ordIP"]));
		if($demomode) $parmList .= "&x_test_request=TRUE";
		if($vsAUTHCODE==""){
			$success=true;
			if($blockuser){
				$success=FALSE;
			}else{
				if(@$pathtocurl != ""){
					exec($pathtocurl . ' --data-binary \'' . str_replace("'","\'",$parmList) . '\' https://secure.authorize.net/gateway/transact.dll', $res, $retvar);
					$res = implode("\n",$res);
				}else{
					if (!$ch = curl_init()) {
						$vsRESPMSG = "cURL package not installed in PHP";
						$success=false;
					}else{
						curl_setopt($ch, CURLOPT_URL,'https://secure.authorize.net/gateway/transact.dll');
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $parmList);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						if(@$curlproxy!=''){
							curl_setopt($ch, CURLOPT_PROXY, $curlproxy);
						}
						$res = curl_exec($ch);
						if(curl_error($ch) != ""){
							$vsRESULT="x";
							$vsRESPMSG= "Error with cURL installation: " . curl_error($ch) . "<br />";
							$success=false;
						}else{
							curl_close($ch);
						}
					}
				}
			}
			if($success){
				$vsRAW = $res;
				$varString = split('\|', $res);
				$vsRESULT=$varString[0];
				$vsRESULT2=$varString[0];
				$vsERRCODE=$varString[2];
				$vsRESPMSG=$varString[3];
     			// SEE IF AUTH.NET DETECTED THE ORDER AS FRAUDULENT
     			if(ereg("^25[0-4]$",$vsERRCODE)) {
          			$headers = str_replace('%from%',$sEmail,$customheaders);
					$headers = str_replace('%to%',$sEmail,$headers);
					mail('kris@reminderband.com,karen@reminderband.com', 'ifrogz fraudulent order', $ordID, $headers);
     			}
				if($vsERRCODE != "1" && $demomode) {
					$vsRESPMSG = $vsERRCODE . " - " . $vsRESPMSG;
				}
				$vsAUTHCODE=$varString[4];
				$vsAVSADDR=$varString[5];
				$vsTRANSID=$varString[6];
				$vsDESCRIPTION=$varString[8];
				$vsAMOUNT=$varString[9];
				$vsTYPE=$varString[11];
				$vsCVV2=$varString[38];
				if((int)$vsRESULT==1){
					$vsRESULT="0"; // Keep in sync with Payflow Pro
					do_stock_management($ordID);
					mysql_query("UPDATE cart SET cartCompleted=1 WHERE cartOrderID='" . $ordID . "'") or print(mysql_error());
					//send to back order status
					//echo "isExtendShipping".$_SESSION['isExtendShipping'];
					$orderStatus=3;
					if(isAddInfo($ordID)) {
						$orderStatus = 5;
					}
					if($_SESSION['isExtendShipping']) $orderStatus=4;
					if(mysql_query("UPDATE orders SET ordStatus=".$orderStatus.",ordAuthNumber='" . mysql_real_escape_string($vsAVSADDR . $vsCVV2 . "-" . $vsAUTHCODE) . "',ordTransID='" . mysql_real_escape_string($vsTRANSID) . "' WHERE ordID='" . $ordID . "'")) {
						if(!setNewLocation( $orderStatus , $ordID )) print("Unable to record new location");
					}else{
						print(mysql_error());
					}
					// ADDED Apr20 by Chad
					// STORE THE TYPE OF CREDIT CARD
					$cc_type = getCCType($_POST["ACCT"]);
					$qry = "UPDATE orders SET ordCCType = '$cc_type' WHERE ordID='" . $ordID . "'";
					$res = mysql_query($qry) or print(mysql_error());
					// ADD ENDED



					// ADDED Mar30 06 by Chad
					// RECORD TRANSACTION
					include(APPPATH.'views/pages/admin/cartmisc.php');
					$now = date("Y-m-d H:i:s");
					$cc_exp_date = $_POST["EXMON"] . $_POST["EXYEAR"];
					$qry = "INSERT INTO transactions ( txn , ordID , amt , date_received , type , note , auth_net_status , auth_net_raw , cc_num , cc_type , cc_exp_date )
							VALUES ( '$vsTRANSID' , '$ordID' , $vsAMOUNT , '$now' , '$vsTYPE' , '$vsDESCRIPTION' , '$vsRESULT2' , '".mysql_real_escape_string($vsRAW)."' , '".mysql_real_escape_string(Encrypt(substr($_POST["ACCT"], -4),$cart_misc))."' , '$cc_type' , '".mysql_real_escape_string(Encrypt($cc_exp_date,$cart_misc))."' )";
					$res = mysql_query($qry) or print(mysql_error());
					// ADD ENDED

					// ADDED July 11, 2006 By Blake
					// Add order to ws_sales table
					if(@$_SESSION["clientUser"] != ""){
						if(($_SESSION["clientActions"] & 8) == 8){
							addWSSalesOrder($_SESSION['custID'],$ordID);
						}
					}
					// ADD ENDED
				}
				if((int)$vsRESULT==27) {
					$gobackplaces='-2';
					// Record the error in the DB
					//$qry = "UPDATE orders SET ordPmtMessage = '$vsRESPMSG' WHERE ordSessionID = '".mysql_real_escape_string($thesessionid)."'";
					//$res = @mysql_query($qry);
				}
			}
		}else{
			$vsRESULT="0";
			$vsRESPMSG="This transaction has been approved.";
			$pos = strpos($vsAUTHCODE, "-");
			if (! ($pos === false))
				$vsAUTHCODE = substr($vsAUTHCODE, $pos + 1);
		}
	}elseif(@$_POST["method"]=="paypalpro"){
		@set_time_limit(120);
		$sSQL = "SELECT payProvDemo,payProvData1,payProvData2,payProvMethod FROM payprovider WHERE payProvEnabled=1 AND payProvID=18"; // Check for PayPal Payment Pro
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_assoc($result)){
			$demomode = ((int)$rs["payProvDemo"]==1);
			$username = trim($rs["payProvData1"]);
			$ppmethod = (int)$rs["payProvMethod"];
			$data2arr = split("&",trim($rs["payProvData2"]));
			$password=urldecode(@$data2arr[0]);
			$sslcertpath=urldecode(@$data2arr[1]);
		}
		mysql_free_result($result);
		$sSQL = "SELECT ordID,ordName,ordCity,ordState,ordCountry,ordPhone,ordHandling,ordZip,ordEmail,ordShipping,ordStateTax,ordCountryTax,ordTotal,ordDiscount,ordAddress,ordAddress2,ordIP,ordAuthNumber,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipCountry,ordShipZip FROM orders WHERE ordID='" . $ordID . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_array($result);
		mysql_free_result($result);
		$sSQL = "SELECT countryCode FROM countries WHERE countryName='" . mysql_real_escape_string($rs["ordCountry"]) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs2 = mysql_fetch_array($result)){
			$countryCode = $rs2["countryCode"];
		}
		mysql_free_result($result);
		$sSQL = "SELECT countryCode FROM countries WHERE countryName='" . mysql_real_escape_string($rs["ordShipCountry"]) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs2 = mysql_fetch_array($result)){
			$shipCountryCode = $rs2["countryCode"];
		}else
			$shipCountryCode = '';
		mysql_free_result($result);
		if($countryCode == 'US' || $countryCode == 'CA'){
			$sSQL = "SELECT stateAbbrev FROM states WHERE stateAbbrev='" . mysql_real_escape_string($rs["ordState"]) . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs2 = mysql_fetch_array($result)) $rs["ordState"]=$rs2["stateAbbrev"];
			mysql_free_result($result);
		}
		if($shipCountryCode == 'US' || $shipCountryCode == 'CA'){
			$sSQL = "SELECT stateAbbrev FROM states WHERE stateAbbrev='" . mysql_real_escape_string($rs["ordShipState"]) . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs2 = mysql_fetch_array($result)) $rs["ordShipState"]=$rs2["stateAbbrev"];
			mysql_free_result($result);
		}
		$vsAUTHCODE = trim($rs["ordAuthNumber"]);
		$thename = unstripslashes(trim(@$_POST["cardname"]));
		if(strstr($thename," ")){
			$namearr = split(" ",$thename,2);
			$firstname = $namearr[0];
			$lastname = $namearr[1];
		}else{
			$firstname = '';
			$lastname = $thename;
		}
		$cardnum = preg_replace('/\s+/', '', trim(@$_POST["ACCT"]));
		$cartype = "Visa";
		if(substr($cardnum, 0, 1)=="5")
			$cartype="MasterCard";
		elseif(substr($cardnum, 0, 1)=="6")
			$cartype="Discover";
		elseif(substr($cardnum, 0, 1)=="3")
			$cartype="Amex";
		$sXML = ppsoapheader($username, $password) .
			'  <soap:Body><DoDirectPaymentReq xmlns="urn:ebay:api:PayPalAPI">' .
			'    <DoDirectPaymentRequest><Version xmlns="urn:ebay:apis:eBLBaseComponents">1.00</Version>' .
			'      <DoDirectPaymentRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">' .
			'        <PaymentAction>' . ($ppmethod==1?'Authorization':'Sale') . '</PaymentAction>' .
			'        <PaymentDetails>' .
			'          <OrderTotal currencyID="' . $countryCurrency . '">' . number_format(($rs["ordShipping"]+$rs["ordStateTax"]+$rs["ordCountryTax"]+$rs["ordTotal"]+$rs["ordHandling"])-$rs["ordDiscount"],2,'.','') . '</OrderTotal>' .
			'          <ButtonSource>ecommercetemplates.php.ecommplus</ButtonSource>';
		if(trim($rs["ordShipAddress"]) != '')
			$sXML .= '<ShipToAddress><Name>' . vrxmlencode($rs["ordShipName"]) . '</Name><Street1>' . vrxmlencode($rs["ordShipAddress"]) . '</Street1><Street2>' . vrxmlencode($rs["ordShipAddress2"]) . '</Street2><CityName>' . $rs["ordShipCity"] . '</CityName><StateOrProvince>' . $rs["ordShipState"] . '</StateOrProvince><Country>' . $shipCountryCode . '</Country><PostalCode>' . $rs["ordShipZip"] . '</PostalCode></ShipToAddress>';
		else
			$sXML .= '<ShipToAddress><Name>' . vrxmlencode($rs["ordName"]) . '</Name><Street1>' . vrxmlencode($rs["ordAddress"]) . '</Street1><Street2>' . vrxmlencode($rs["ordAddress2"]) . '</Street2><CityName>' . $rs["ordCity"] . '</CityName><StateOrProvince>' . $rs["ordState"] . '</StateOrProvince><Country>' . $countryCode . '</Country><PostalCode>' . $rs["ordZip"] . '</PostalCode></ShipToAddress>';
		$sXML .= '   </PaymentDetails>' .
			'        <CreditCard>' .
			'          <CreditCardType>' . $cartype . '</CreditCardType>' .
			'          <CreditCardNumber>' . vrxmlencode($cardnum) . '</CreditCardNumber>' .
			'          <ExpMonth>' . @$_POST["EXMON"] . '</ExpMonth><ExpYear>' . @$_POST["EXYEAR"] . '</ExpYear>' .
			'          <CardOwner>' .
			'            <Payer>' . vrxmlencode($rs["ordEmail"]) . '</Payer>' .
			'            <PayerName><FirstName>' . vrxmlencode($firstname) . '</FirstName><LastName>' . vrxmlencode($lastname) . '</LastName></PayerName>' .
			'            <PayerCountry>' . $countryCode . '</PayerCountry>' .
			'            <Address><Street1>' . vrxmlencode($rs["ordAddress"]) . '</Street1><Street2>' . vrxmlencode($rs["ordAddress2"]) . '</Street2><CityName>' . $rs["ordCity"] . '</CityName><StateOrProvince>' . $rs["ordState"] . '</StateOrProvince><Country>' . $countryCode . '</Country><PostalCode>' . $rs["ordZip"] . '</PostalCode></Address>' .
			'          </CardOwner>' .
			'          <CVV2>' . trim(@$_POST["CVV2"]) . '</CVV2>' .
			'        </CreditCard>' .
			'        <IPAddress>' . trim($rs["ordIP"]) . '</IPAddress><MerchantSessionId>' . $rs["ordID"] . '</MerchantSessionId>' .
			'      </DoDirectPaymentRequestDetails>' .
			'    </DoDirectPaymentRequest></DoDirectPaymentReq></soap:Body></soap:Envelope>';
		if($demomode) $sandbox = ".sandbox"; else $sandbox = "";
		$vsRESULT="-1";
		if($vsAUTHCODE==""){
			if($blockuser)
				$success=FALSE;
			else
				$success = callcurlfunction('https://api' . $sandbox . '.paypal.com/2.0/', $sXML, $res, $sslcertpath, $vsRESPMSG, TRUE);
			if($success){
				$xmlDoc = new vrXMLDoc($res);
				$vsAUTHCODE="";
				$vsERRCODE="";
				$vsRESPMSG="";
				$vsAVSADDR="";
				$vsTRANSID="";
				$vsCVV2="";
				$nodeList = $xmlDoc->nodeList->childNodes[0];
				for($i = 0; $i < $nodeList->length; $i++){
					if($nodeList->nodeName[$i]=="SOAP-ENV:Body"){
						$e = $nodeList->childNodes[$i];
						for($j = 0; $j < $e->length; $j++){
							if($e->nodeName[$j] == "DoDirectPaymentResponse"){
								$ee = $e->childNodes[$j];
								for($jj = 0; $jj < $ee->length; $jj++){
									if($ee->nodeName[$jj] == "Ack"){
										if($ee->nodeValue[$jj]=="Success"){
											$vsRESULT=1;
											$vsRESPMSG = $ee->nodeValue[$jj];
										}
									}elseif($ee->nodeName[$jj] == "TransactionID"){
										$vsAUTHCODE=$ee->nodeValue[$jj];
									}elseif($ee->nodeName[$jj] == "AVSCode"){
										$vsAVSADDR=$ee->nodeValue[$jj];
									}elseif($ee->nodeName[$jj] == "CVV2Code"){
										$vsCVV2=$ee->nodeValue[$jj];
									}elseif($ee->nodeName[$jj] == "Errors"){
										$ff = $ee->childNodes[$jj];
										for($kk = 0; $kk < $ff->length; $kk++){
											if($ff->nodeName[$kk] == "ShortMessage"){
												//$vsRESPMSG=$ff->nodeValue[$kk].'<br>'.$vsRESPMSG;
											}elseif($ff->nodeName[$kk] == "LongMessage"){
												$vsRESPMSG.=$ff->nodeValue[$kk];
											}elseif($ff->nodeName[$kk] == "ErrorCode"){
												$vsERRCODE=$ff->nodeValue[$kk];
											}
										}
									}
								}
							}
						}
					}
				}
				if((int)$vsRESULT==1){
					$vsRESULT="0"; // Keep in sync with Payflow Pro
					do_stock_management($ordID);
					$sSQL="UPDATE cart SET cartCompleted=1 WHERE cartOrderID='" . $ordID . "'";
					mysql_query($sSQL) or print(mysql_error());
					//send to back order status
					$orderStatus=3;
					if(isAddInfo($ordID)) {
						$orderStatus = 5;
					}

					if(!$_SESSION['isExtendShipping']) $orderStatus=4;
					$sSQL="UPDATE orders SET ordStatus=".$orderStatus.",ordAuthNumber='" . mysql_real_escape_string($vsAVSADDR . $vsCVV2 . "-" . $vsAUTHCODE) . "' WHERE ordID='" . $ordID . "'";
					if(mysql_query($sSQL)) {
						if(!setNewLocation( $orderStatus , $ordID )) print("Unable to record new location");
					}else{
						print(mysql_error());
					}
				}
			}
		}else{
			$vsRESULT="0";
			$vsRESPMSG="This transaction has been approved.";
			$pos = strpos($vsAUTHCODE, "-");
			if (! ($pos === false))
				$vsAUTHCODE = substr($vsAUTHCODE, $pos + 1);
		}
	}
?>	  <div id="cart_nav"><img  src="/lib/images/new_images/subnav_gray_13.gif" alt="View Order" /><img src="/lib/images/new_images/subnav_gray_15.gif" alt="Customer Info" /><img src="/lib/images/new_images/subnav_gray_17.gif" alt="Final Review" /><img src="/lib/images/new_images/subnav_green_19.gif" alt="Confirmation" /><img src="/lib/images/new_images/subnav_gray_21.gif" alt="View Receipt" /></div>
            <table class="cobtbl" width="<?php print $maintablewidth?>" border="0" bordercolor="#B1B1B1" cellspacing="1" cellpadding="3" bgcolor="#B1B1B1">
<?php	if($vsRESULT=="0"){ ?>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="center" colspan="2"><strong><?php print $xxTnxOrd?></strong></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxTrnRes?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" width="50%"><strong><?php print $vsRESPMSG?></strong></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxOrdNum?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" width="50%"><strong><?php print $ordID?></strong></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxAutCod?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" width="50%"><strong><?php print $vsAUTHCODE?></strong></td>
			  </tr>
			  <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" colspan="2">
				  <table width="100%" cellspacing="0" cellpadding="0" border="0">
				    <tr>
					  <td width="16" height="26" align="right" valign="bottom">&nbsp;</td>
					  <td class="cobll" bgcolor="#FFFFFF" width="100%" align="center">&nbsp;<br />
<?
// splits the cart page into different funnels
$google_tracker="/cart/complete.php";
// end
?>

	<form method="post" action="thanks.php<?php  $_SESSION[hidden_id]=$ordID;?>" name="checkoutform">
	<input type="hidden" name="xxpreauth" value="<?php print $ordID?>" />
					  <input type="image"  src="/lib/images/complete_order.gif" name="submit" border="0" /><br />&nbsp;
	</form>
					  </td>
					  <td width="16" height="26" align="right" valign="bottom"><img src="/lib/images/tablebr.gif" alt="" /></td>
					</tr>
				  </table>
				</td>
			  </tr>
<?php		if(@$forcesubmit==TRUE){
				if(@$forcesubmittimeout=="") $forcesubmittimeout=5000;
				print '<script language="JavaScript" type="text/javascript">setTimeout("document.checkoutform.submit()",'.$forcesubmittimeout.');</script>'."\r\n";
			}
			// Added June 2 2006 By Blake
			//removes uploaded images for custom screenz
			deleteUploadedImages();
			//end
			// Added Dec 21 2006 By Blake
			//split orders
			$_SESSION['neworderID']=splitOrders($ordID);
			//end
		}else{ ?>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="center" colspan="2"><strong><?php print $xxSorTrn?></strong></td>
			  </tr>
			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" align="right" width="50%"><strong><?php print $xxTrnRes?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" width="50%"><strong><?php print $vsRESPMSG?></strong></td>
			  </tr>
			  <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" colspan="2">
				  <table width="100%" cellspacing="0" cellpadding="0" border="0">
				    <tr>
					  <td width="16" height="26" align="right" valign="bottom">&nbsp;</td>
					  <td class="cobll" bgcolor="#FFFFFF" width="100%" align="center">&nbsp;<br />
	<form method="post" action="thanks.php" name="checkoutform">
	<input type="hidden" name="xxpreauth" value="<?php print $ordID?>" />
					  <input type="button" value="<?php print $xxGoBack?>" onclick="javascript:history.go(<?php print $gobackplaces?>)" /><br />&nbsp;
	</form>
					  </td>
					  <td width="16" height="26" align="right" valign="bottom"><img src="/lib/images/tablebr.gif" alt="" /></td>
					</tr>
				  </table>
				</td>
			  </tr>
<?php	} ?>
			</table>
<?php
}elseif(@$_GET["token"] == '' && @$_POST["mode"] != "paypalexpress1" && $cartisincluded != TRUE){
	$requiressl = FALSE;
	if(@$pathtossl == ''){
		$sSQL = "SELECT payProvID FROM payprovider WHERE payProvEnabled=1 AND (payProvID IN (7,10,12,13,18) OR (payProvID=16 AND payProvData2='1'))"; // All the ones that require SSL
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result) > 0) $requiressl = TRUE;
		mysql_free_result($result);
	}
	if($requiressl || @$pathtossl != ''){
		if(@$pathtossl != ''){
			if(substr($pathtossl,-1) != '/') $pathtossl .= '/';
			$cartpath = $pathtossl . 'cart.php';
		}else{
			//$cartpath = str_replace('http:','https:',$storeurl) . 'cart.php';
			$cartpath = 'cart.php';
		}
	}else{
		$cartpath='cart.php';
	}
	$addextrarows=0;
	$wantstateselector=FALSE;
	$wantcountryselector=FALSE;
	$wantzipselector=FALSE;
	$wantshipppingmethod=FALSE;
	$wantcouponcode=FALSE;
	if(@$estimateshipping==TRUE){
		$addextrarows=1;
		if($shipType==2 || $shipType==5){ // weight / price based
			$wantcountryselector=TRUE;
			$wantshipppingmethod=TRUE;
			$wantcouponcode=TRUE;
			if($splitUSZones){
				$addextrarows=3;
				$wantstateselector=TRUE;
			}else
				$addextrarows=2;
		}elseif($shipType==3 || $shipType==4 || $shipType==6){
			$addextrarows=3;
			$wantzipselector=TRUE;
			$wantcountryselector=TRUE;
			$wantshipppingmethod=TRUE;
			$wantcouponcode=TRUE;
		}
		$shiphomecountry=TRUE;
		if(@$_POST["state"] != ""){
			$shipstate = unstripslashes(@$_POST["state"]);
			$_SESSION["state"] = unstripslashes(@$_POST["state"]);
		}elseif(@$_SESSION["state"] != "")
			$shipstate = $_SESSION["state"];
		else
			$shipstate = $defaultshipstate;
		if(@$_POST["zip"] != ""){
			$destZip = trim(unstripslashes(@$_POST["zip"]));
			$_SESSION["zip"] = trim(unstripslashes(@$_POST["zip"]));
		}elseif(@$_SESSION["zip"] != "")
			$destZip = $_SESSION["zip"];
		else
			$destZip = $origZip;
		if(@$_POST["country"] != ""){
			$shipcountry = unstripslashes(@$_POST["country"]);
			$_SESSION["country"] = unstripslashes(@$_POST["country"]);

			if(trim(@$_POST["state"])=='') $shipstate="";
		}elseif(@$_SESSION["country"] != "")
			$shipcountry = $_SESSION["country"];
		else{
			$shipCountryCode = $origCountryCode;
			$shipcountry = $origCountry;
		}
		$sSQL = "SELECT countryID,countryTax,countryCode,countryFreeShip,countryOrder FROM countries WHERE countryName='" . mysql_real_escape_string($shipcountry) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$countryTaxRate = $rs["countryTax"];
			$shipCountryID = $rs["countryID"];
			$shipCountryCode = $rs["countryCode"];
			$freeshipapplies = ($rs["countryFreeShip"]==1);
			$shiphomecountry = ($rs["countryOrder"]==2);
		}
		mysql_free_result($result);
		//if(@$_SESSION["xsshipping"] == "")
		initshippingmethods();
		$_SESSION['thisshipcountry']=$shipcountry;
	}
	checkRelatedDiscounts();
	$sSQL = "SELECT cartID,cartProdID,cartProdName,cartProdPrice,cartOrderID,cartQuantity,pImage,pWeight,pShipping,pShipping2,pExemptions,pSection,pDims,topSection,isSet FROM cart LEFT JOIN products ON cart.cartProdID=products.pID LEFT OUTER JOIN sections ON products.pSection=sections.sectionID WHERE cartCompleted=0 AND cartSessionID='" .  session_id() . "' ORDER BY cartID";
	$result_check=mysql_query($sSQL) or print(mysql_error());
	$_SESSION['isExtendShipping']=FALSE;
	$hasset=FALSE;
	$totalgoods=0;
	while($alldata_check=mysql_fetch_assoc($result_check)){
		$totalgoods += ($alldata_check["cartProdPrice"]*(int)$alldata_check["cartQuantity"]);
		$prod=$alldata_check['cartProdID'];
		$ordernum=$alldata_check['cartOrderID'];
		//getRelatedProducts($prod,$ordernum);
		if($alldata_check['isSet']=='yes') $hasset=TRUE;
		$sSQL = "SELECT coOptGroup,coCartOption,coPriceDiff,coWeightDiff,coExtendShipping FROM cartoptions WHERE coCartID=" . $alldata_check["cartID"] . " ORDER BY coID";
		$opts_check = mysql_query($sSQL) or print(mysql_error());
		while($rs_check=mysql_fetch_assoc($opts_check)){
			if($rs_check['coExtendShipping']>0){
				$_SESSION['isExtendShipping']=TRUE;
			}
			$totalgoods += ($rs_check["coPriceDiff"]*(int)$alldata_check["cartQuantity"]);
		}
	}
	mysql_free_result($result_check);
	//check for added products and display message if there is any
	$sSQL = "SELECT cartAddProd FROM cart WHERE cartAddProd=1 AND cartCompleted=0 AND cartSessionID='" .  session_id() . "' ORDER BY cartID";
	$result = mysql_query($sSQL) or print(mysql_error());
	$num_added_prods=mysql_num_rows($result);
	mysql_free_result($result);
	//

	if(@$_POST["cpncode"]=='' && $_SESSION['os']=='') $coupon_code=FALSE;
	elseif($_SESSION['os']!='') $coupon_code=$_SESSION['os'];
	else $coupon_code=@$_POST["cpncode"];



	calculatediscounts($totalgoods, false, $coupon_code);

	if($totaldiscounts > $totalgoods) $totaldiscounts = $totalgoods;
	if($totaldiscounts==0)
		$_SESSION["discounts"] = "";
	else{
		$_SESSION["discounts"] = $totaldiscounts;

		checkRelatedDiscounts();

		$addextrarows++;
		$glicpnmessage = substr($cpnmessage, 6, -6);
		//$googlelineitems .= '<item><merchant-private-item-data><discountflag>true</discountflag></merchant-private-item-data><item-name>' . xmlencodecharref(strip_tags($xxAppDs)) . '</item-name><item-description>' . xmlencodecharref(strip_tags(str_replace('<br />', ' - ', $glicpnmessage))) . '</item-description><unit-price currency="' . $countryCurrency . '">-' . number_format($totaldiscounts,2,'.','') . '</unit-price><quantity>1</quantity></item>';
	}







	$alldata="";
	$sSQL = "SELECT cartID,cartProdID,cartProdName,cartProdPrice,cartQuantity,cartAddProd,pImage,pWeight,pShipping,pShipping2,pExemptions,pSection,pDims,topSection,isSet,pDescription,pDropship,pCat FROM cart LEFT JOIN products ON cart.cartProdID=products.pID LEFT OUTER JOIN sections ON products.pSection=sections.sectionID WHERE cartCompleted=0 AND cartSessionID='" .  session_id() . "' ORDER BY cartID";
	$result = mysql_query($sSQL) or print(mysql_error());
?>
            <div id="cart_nav"><img src="/lib/images/new_images/subnav_green_13.gif" alt="View Order"><img src="/lib/images/new_images/subnav_gray_15.gif" alt="Customer Info"><img src="/lib/images/new_images/subnav_gray_17.gif" alt="Final Review"><img src="/lib/images/new_images/subnav_gray_19.gif" alt="Confirmation"><img src="/lib/images/new_images/subnav_gray_21.gif" alt="View Receipt"></div>
				<div id="related">
<?
			include(DOCROOT.'ectmods/crosssale/inc/incxsale.php'); ?></div>
<script type="text/javascript" defer="defer">
<!--
function shippingJump() {
	var shippingest = $('shippingest').value;
	if(shippingest != '') {
		var aShip = shippingest.split("|");
		var method = aShip[1];
	}else{
		var method = '';
	}
	/*if($('org_shippingest').value!=aShip[0]) {
		$('change_shippingest').value=1;
	}else{
		$('change_shippingest').value="";
		$('shp_method').value = "";
	}*/
	if(method=='') {
		$('shp_method').value = '';
	}else{
		$('shp_method').value = method;
	}

	document.checkoutform.submit();
}
-->
</script>
<?php
//showarray($_POST);
//showarray($_SESSION);

// Temporarily Added 2008-04-21
$tmpMsg = "";
/*$tmpsql = "SELECT * FROM cart WHERE binary cartProdID LIKE '%U%' AND cartSessionID = '" . session_id() . "'";
$tmpres = mysql_query($tmpsql);
if ($tmpres) {
	if (mysql_num_rows($tmpres) > 0) {
		$tmpMsg = "All iPod Classic screenz are on backorder at the moment.";
	}
}*/


?>
	<form method="post" action="cart.php" name="checkoutform">
	<input type="hidden" name="mode" value="update" />
		<? if($num_added_prods>0 || $_SESSION['isExtendShipping']){
		 		$msg='';
				if($_SESSION['isExtendShipping']) $msg=$xxNoInvStok.'<br />';
				if($num_added_prods>0) $msg.=$hasrelated['disc_display_text'];
		?>
			<div style="color:#FF0000; margin-bottom:10px; padding:10px; border:#EBEBEB dashed 1px; background-color: #FFFFFF; font-weight:bold;"><img src="/lib/images/warning.jpg" align="middle" hspace="10px" />
			<?=$msg?></div>

		<? }
		//showarray($hasrelated);
		?>
		<?php
		if (!empty($tmpMsg)) {
		?>
			<div style="color:#FF0000; margin-bottom:10px; padding:10px; border:#EBEBEB dashed 1px; background-color: #FFFFFF; font-weight:bold;"><img src="/lib/images/warning.jpg" align="middle" hspace="10px" />
			<?=$tmpMsg?></div>
		<?php
		}
		?>
		<table class="cobtbl" width="<?php print $maintablewidth?>" border="0" bordercolor="#B1B1B1" cellspacing="1" cellpadding="3" bgcolor="#B1B1B1">
<?php
	if(($itemsincart = mysql_num_rows($result)) > 0){
		if(! $isInStock){ ?>
			  <tr height="30">
			    <td class="cobll" bgcolor="#FFFFFF" colspan="6" align="center"><font color="#FF0000"><strong><?php print $xxNoStok?></strong></font></td>
			  </tr>
<?php	} ?>


			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB" width="15%"><strong><?php print $xxCODets?></strong></td>
			    <td class="cobhl" bgcolor="#EBEBEB" width="39%"><strong><?php print $xxCOName?></strong></td>
				<td class="cobhl" bgcolor="#EBEBEB" width="12%" align="center"><strong><?php print $xxCOUPri?></strong></td>
				<td class="cobhl" bgcolor="#EBEBEB" width="12%" align="center"><strong><?php print $xxQuant?></strong></td>
				<td class="cobhl" bgcolor="#EBEBEB" width="12%" align="center"><strong><?php print $xxTotal?></strong></td>
				<td class="cobhl" bgcolor="#EBEBEB" width="10%" align="center"><strong><?php print $xxCOSel?></strong></td>
			  </tr>
<?php	//$totaldiscounts = 0;
		$changechecker = "";
		$googlelineitems = '';
		$index = 0;
		$_SESSION['hasdropship']=FALSE;
		$totalgoods2=0;

		$itemsInCart='';
		while($alldata=mysql_fetch_assoc($result)){
			$index++;
			$changechecker .= 'if(document.checkoutform.quant' . $alldata["cartID"] . ".value!=" . $alldata["cartQuantity"] . ") dowarning=true;\n";
			$theoptions = "";
			$theoptionspricediff = 0;
			if($alldata['pDropship']>0)$_SESSION['hasdropship']=TRUE;
			$sSQL = "SELECT coOptGroup,coCartOption,coPriceDiff,coWeightDiff,coExtendShipping,ui.id,ui.display_image,ui.org_img_name FROM cartoptions co LEFT JOIN uploaded_images ui ON co.coCartOption=ui.id WHERE coCartID=" . $alldata["cartID"] . " ORDER BY coID";
			$opts = mysql_query($sSQL) or print(mysql_error());
			$cartID=$alldata["cartID"];
			$optPriceDiff=0;
			while($rs=mysql_fetch_assoc($opts)){
				$theoptionspricediff += $rs["coPriceDiff"];
				$alldata["pWeight"] += (double)$rs["coWeightDiff"];
				$theoptions .= '<tr height="25">';
				$theoptions .= '<td class="cobhl" bgcolor="#EBEBEB" align="right" valign="top"><font style="font-size: 10px"><strong>' . $rs["coOptGroup"] . ':</strong></font></td>';
				$extendshipping='';
				if(!empty($rs["coExtendShipping"])){
					if($rs["coExtendShipping"]>19999999) {
						$thisyeart=substr($rs["coExtendShipping"],0,4);
						$thismontht=substr($rs["coExtendShipping"],4,2);
						$thisdayt=substr($rs["coExtendShipping"],6,2);
						$extendshipping= '<span style="color:#FF0000; font-weight:bold;">*(PRE-ORDER. Will ship after '.$thismontht.'-'.$thisdayt.'-'.$thisyeart .')</span>';
					}else $extendshipping= '<span style="color:#FF0000; font-weight:bold;">*(increases shipping time by '.$rs["coExtendShipping"].' days)</span>';
				}
				$imgsrc='';
				if(!empty($rs["id"])){
					$rs["coCartOption"]=$rs["org_img_name"];
					$imgsrc='<div><img src="/imguploads/img_screen/'.$rs["display_image"].'.gif" align="top" /></div>';
					if (!empty($custom_screen_msg)) {
						$extendshipping= '<div style="color:#FF0000; font-weight:bold;">'.$custom_screen_msg.'</div>';
					}
				}
				$theoptions .= '<td class="cobll" bgcolor="#FFFFFF" valign="top"><font style="font-size: 10px">&nbsp;- ' . str_replace(array("\r\n","\n"),array("<br />","<br />"),$rs["coCartOption"]) .' '.$extendshipping.' </font></td>';
				$theoptions .= '<td class="cobll" bgcolor="#FFFFFF" align="right"><font style="font-size: 10px; color:#FF0000;">' . ($rs["coPriceDiff"]==0 || @$hideoptpricediffs==TRUE ? "" : FormatEuroCurrency($rs["coPriceDiff"])) . '</font>'.$imgsrc.'</td>';
				$theoptions .= '<td class="cobll" bgcolor="#FFFFFF" align="right">&nbsp;</td>';
				$theoptions .= '<td class="cobll" bgcolor="#FFFFFF" align="right"><font style="font-size: 10px; color:#FF0000;">' . ($rs["coPriceDiff"]==0 || @$hideoptpricediffs==TRUE ? " " : FormatEuroCurrency($rs["coPriceDiff"]*$alldata["cartQuantity"])) . '</font></td>';
				$theoptions .= '<td class="cobll" bgcolor="#FFFFFF" align="center">&nbsp;</td>';
				$theoptions .= "</tr>\n";
				$totalgoods2 += ($rs["coPriceDiff"]*(int)$alldata["cartQuantity"]);
			}
			$googlelineitems .= '<item><merchant-private-item-data><product-id>' . xmlencodecharref($alldata['cartProdID']) . '</product-id></merchant-private-item-data><item-name>' . xmlencodecharref(strip_tags($alldata['cartProdName'])) . '</item-name><item-description>' . xmlencodecharref(strip_tags($alldata['pDescription'])) . '</item-description><unit-price currency="' . $countryCurrency . '">' . number_format($alldata['cartProdPrice'] + $theoptionspricediff,2,'.','') . '</unit-price><quantity>' . $alldata['cartQuantity'] . '</quantity></item>';
			mysql_free_result($opts);
			?>


			  <tr height="30">
			    <td class="cobhl" bgcolor="#EBEBEB"><strong><?php print $alldata["cartProdID"]?></strong></td>
			    <td class="cobll" bgcolor="#FFFFFF"><?php print $alldata["cartProdName"].' <span style="font-size: 10px; color:#FF0000; font-weight:bold;">'.$hasrelated[$cartID].'</span>' ?>
				<?
/*					$img_preview='';
					$imgsrc=explode(',',$alldata["pImage"]);
					echo '<div><img src="'.$imgsrc[0].'" align="top" /></div>';
*/				?>
				</td>
				<td class="cobll" bgcolor="#FFFFFF" align="right">
				<?php
					if($alldata["cartProdPrice"]==0) print '<span style="color:#FF0000; font-weight:bold;">FREE</span>';
					else print (@$hideoptpricediffs==TRUE ? FormatEuroCurrency($alldata["cartProdPrice"] + $theoptionspricediff) : FormatEuroCurrency($alldata["cartProdPrice"]))
				?>
				</td>
				<td class="cobll" bgcolor="#FFFFFF" align="center">
				<? if($alldata["cartAddProd"]==0){ ?>
				 <input type="text" name="quant<?php print $alldata["cartID"]?>" value="<?php print $alldata["cartQuantity"]?>" size="2" maxlength="5" />
				<? }  else { ?>
					<input type="hidden" name="quant<?php print $alldata["cartID"]?>" value="<?php print $alldata["cartQuantity"]?>" size="2" maxlength="5" /><?= $alldata["cartQuantity"]?>
				<? } ?>
				</td>
				<td class="cobll" bgcolor="#FFFFFF" align="right"><?php print (@$hideoptpricediffs==TRUE ? FormatEuroCurrency(($alldata["cartProdPrice"] + $theoptionspricediff)*$alldata["cartQuantity"]) : FormatEuroCurrency($alldata["cartProdPrice"]*$alldata["cartQuantity"]))?></td>
				<td class="cobll" bgcolor="#FFFFFF" align="center"><input type="checkbox" name="delet<?php print $alldata["cartID"]?>" /></td>
			  </tr>
<?php		print $theoptions;
			$totalgoods2 += ($alldata["cartProdPrice"]*(int)$alldata["cartQuantity"]);
			$totalquantity += (int)$alldata["cartQuantity"];
			$alldata["cartProdPrice"] += $theoptionspricediff;
			if(($shipType==2 || $shipType==3 || $shipType==4 || $shipType==6) && (double)$alldata["pWeight"]<=0.0)
				$alldata["pExemptions"] = ($alldata["pExemptions"] | 4);
			//echo 'xsshipping'.@$_SESSION["xsshipping"];
			@$_SESSION["xsshipping"] = "";
			//if(@$estimateshipping==TRUE && @$_SESSION["xsshipping"] == "")
				addproducttoshipping($alldata, $index);

			if(!is_array($itemsInCart))$itemsInCart[$t++]=$alldata["pCat"];
			else if(!in_array($alldata["pCat"],$itemsInCart))$itemsInCart[$t++]=$alldata["pCat"];

		}
		$_SESSION['itemsInCart']=$itemsInCart;
		//showarray($_SESSION['itemsInCart']);
		if($addextrarows > 0){ ?>
              <tr height="30">
				<!-- changed from 3 to 5 ; added the shipping info--><td class="cobhl" bgcolor="#EBEBEB" rowspan="<?php print $addextrarows+5;?>">&nbsp;</td>
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><strong><?php print $xxSubTot?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="right"><?php print FormatEuroCurrency($totalgoods2)?></td>
				<td class="cobll" bgcolor="#FFFFFF" align="center"><a href="#" onclick="javascript:document.checkoutform.submit()"><strong><?php print $xxDelete?></strong></a></td>
			  </tr>
<?php	}
		if($totaldiscounts>0){
			calculatediscounts($totalgoods2, false, $coupon_code);
		?>
			  <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><font color="#FF0000"><strong><?php print $xxDsApp.' '.$cpnmessage?></strong></font></td>
				<td class="cobll" bgcolor="#FFFFFF" align="right"><font color="#FF0000"><?php print FormatEuroCurrency($totaldiscounts)?></font></td>
				<td class="cobll" bgcolor="#FFFFFF" align="center">&nbsp;</td>
			  </tr>
<?php	}
		if(@$estimateshipping==TRUE){
			//if(@$_SESSION["xsshipping"] == ""){
				calculateshipping();
				if(is_numeric(@$shipinsuranceamt) && abs(@$addshippinginsurance)==1)
					$shipping += ($addshippinginsurance==1 ? (((double)$totalgoods*(double)$shipinsuranceamt)/100.0) : $shipinsuranceamt);
				calculateshippingdiscounts(FALSE);
				$_SESSION["xsshipping"]=$shipping-$freeshipamnt;
			//}else{
				//$shipping = $_SESSION["xsshipping"];
			//}
			//select shipping type
				//calculateshipping();
				//echo 'shipping='.$_SESSION["xsshipping"];
				//echo 'opt='.$_SESSION["xsshipping"];


				/*if(@$_POST["change_shippingest"]==1) {
					$aShipping = explode("|",$_POST['shippingest']);
					$shipping = $aShipping[0];
				}else{
					$shipping = $_SESSION['shippingestimate'];
				}
				$_SESSION['shippingestimate']=$shipping;*/
				$foundMethod = false;
				for($k=0; $k < $numshipoptions; $k++) {
					if($intShipping[$k][0] == $_SESSION['shp_method']) {
						$foundMethod = true;
					}
				}

				if(!empty($_POST['shippingest'])) {
					if(strstr($_POST['shippingest'],"|")) {
						$aShpMethod = explode("|",$_POST['shippingest']);
						$_SESSION['shp_method'] = $aShpMethod[1];
					}else{
						$_SESSION['shp_method'] = 'default';
						$shipping = 'default';
						$foundMethod = true;
					}
				}else{
					$_SESSION['shp_method'] = 'default';
					$shipping = 'default';
					$foundMethod = true;
				}

		?>
			<!--<tr>
				<td colspan="6" bgcolor="#FFFFFF"><?=showarray($aShpMethod)?></td>
			</tr>-->
		<?php

				//echo 'shipping is this='.$shipping;
				//calculateshippingdiscounts($coupon_code);
				// Blake 12/14/06
				// fedex for standard price
				if ($_SESSION['hasdropship']) $numshipoptions=1;
				//if($_SESSION['hasdropship'] && $numshipoptions==3)$numshipoptions=$numshipoptions-2;
				//else if($_SESSION['hasdropship'] && $numshipoptions==2)$numshipoptions--;
				$_SESSION['total_goods']=$totalgoods;

				// end fedex
				$shipping_type= "<select id='shippingest' name='shippingest' size='1' onChange=' shippingJump();'><option value=''>Please Select...</option>";
				if($shipType==2 || $shipType==5){
					if(is_array($allzones)){
						for($index3=0; $index3 < $numshipoptions; $index3++){
							if($_SESSION['shp_method'] == 'default') {
								$_SESSION['shp_method'] = $intShipping[$index3][0];
							}
							if($shipping == 'default') {
								$shipping = $intShipping[$index3][2];
							}
							$shipping_type.= "<option value='" . $intShipping[$index3][2] . "|" . $intShipping[$index3][0] . "'";
							if($_SESSION['shp_method']==$intShipping[$index3][0]) {
								$shipping_type.= ' selected="selected"';
								$shipping = $intShipping[$index3][2];
							}elseif(!$foundMethod) {
								$shipping_type.= ' selected="selected"';
								$foundMethod = true;
								$_SESSION['shp_method'] = $intShipping[$index3][0];
							}
							$shipping_type.= ">";
							//Blake Dec 14 2006
							//fed ex sale
							//$fedex='';
							//if($totalgoods>50 && $index3==$numshipoptions-1) $fedex=' At Standard Price';
							// end fed ex sale
							$shipping_type.= ($freeshippingapplied && ($pzFSA & pow(2, $index3))!=0 ? $xxFree . " " . $intShipping[$index3][0] : $intShipping[$index3][0]. $fedex . " " . FormatEuroCurrency($intShipping[$index3][2])) . '</option>';
						}
					}
				}else{
					for($indexmso=0; $indexmso<$maxshipoptions; $indexmso++){
						$shipRow = $intShipping[$indexmso];
						if($shipType==3){
							if($iTotItems==$shipRow[3]){
								for($index2=0;$index2<$numuspsmeths;$index2++){
									if(trim($shipRow[0]) == trim($uspsmethods[$index2][0])){
										$shipping_type.= "<option value='" . $shipRow[2] . "|". trim($uspsmethods[$index2][1]) ."|" . trim($uspsmethods[$index2][2]) . "'" . (freeshippingapplied && $uspsmethods[$index2][1]==1 ? " selected>" : ">");
										$shipping_type.= trim($uspsmethods[$index2][2]) . " (" . $shipRow[1] . ") " . ($freeshippingapplied && $uspsmethods[$index2][1]==1 ? $xxFree : FormatEuroCurrency($shipRow[2]));
										$shipping_type.= "</option>";
									}
								}
							}
						}elseif($shipType==4 || $shipType==6){
							if($shipRow[3]){
								$shipping_type.= "<option value='" . $shipRow[2] . "|". $shipRow[4] ."|" . $shipRow[0] . "'" . ($freeshippingapplied && $shipRow[4]==1 ? " selected>" : ">") . $shipRow[0] . " ";
								if(trim($shipRow[1]) != "") print "(" . $xxGuar . " " . $shipRow[1] . ") ";
								$shipping_type.= ($freeshippingapplied && $shipRow[4]==1 ? $xxFree : FormatEuroCurrency($shipRow[2]));
								$shipping_type.= "</option>";
							}
						}
					}
				}
				if(@$willpickuptext != ""){
					if(@$willpickupcost=="") $willpickupcost=0;
					$shipping_type.= '<option value="' . $willpickupcost . "|1|" . str_replace('"','&quot;',$willpickuptext) . '">';
					$shipping_type.= $willpickuptext . " " . FormatEuroCurrency($willpickupcost) . "</option>";
				}
				$shipping_type.= "</select>";
				$_SESSION['shippingestimate'] = $shipping;
			//

			if($errormsg != ""){ ?>
              <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><strong><?php print $xxShpEst?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" colspan="2"><font style="font-size: 10px" color="#FF0000"><strong><?php print $errormsg?></strong></font></td>
			  </tr>
<?php		}elseif(!empty($_SESSION['shippingestimate']) || !empty($_SESSION["discounts"]))
			{ ?>
              		<tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><strong><?php print $xxShpEst?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="right"><?php if($freeshipamnt==$shipping) print '<p align="center"><font color="#FF0000"><strong>' . $xxFree . '</strong></font></p>'; else print FormatEuroCurrency($shipping)?></td>
				<td class="cobll" bgcolor="#FFFFFF" align="center">&nbsp;</td>
			  </tr>
<?php		}
			if($wantcountryselector){ ?>
              <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><strong><?php print $xxCountry?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" colspan="2">
				<select name="country"  size="1" onchange="if(checkoutform.org_country.value!=this.value) {checkoutform.change_country.value=1;checkoutform.shippingest.disabled=true} else {checkoutform.change_country.value='';checkoutform.shippingest.disabled=false;}"><?php
				$sSQL = "SELECT countryName,countryCode," . getlangid("countryName",8) . " AS cnameshow FROM countries WHERE countryEnabled=1 ORDER BY countryOrder DESC," . getlangid("countryName",8);
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs = mysql_fetch_assoc($result)){
					print '<option value="' . $rs["countryName"] . '"';
					if($shipcountry==$rs["countryName"]) print ' selected';
					print '>' . $rs["cnameshow"] . "</option>\r\n";
				}
				mysql_free_result($result); ?></select></td>
			  </tr>
<?php		}
			if($wantshipppingmethod){ ?>
              <tr height="30">
				<td colspan="3" align="right" valign="top" bgcolor="#FFFFFF" class="cobll">
					<input name="org_shippingest" id="org_shippingest" type="hidden" value="<?=$shipping?>"><input name="change_shippingest" id="change_shippingest"  type="hidden" value="">
					<input name="org_country" id="org_country" type="hidden" value="<?=$shipcountry?>"><input name="change_country" id="change_country" type="hidden" value="">
					<strong>Shipping Method:</strong>
			<?php
				// Christmas Shipping Message
				$now = mktime();
				/*if ($now > strtotime("2008-12-16 00:00:00") && $now < strtotime("2008-12-25 00:00:00")) {
					echo '<div style="font-size: 11px; font-weight: bold; color: green;">For Christmas delivery we recommend <u>';
					if ($now > strtotime("2008-12-17 14:00:00") && $now < strtotime("2008-12-22 00:00:00")) {
						echo "FedEx Express";
					} else if ($now > strtotime("2008-12-22 00:00:00") && $now < strtotime("2008-12-23 00:00:00")) {
						echo "FedEx Overnight";
					} else {
						echo "Priority Mail";
					}
					echo '</u></div>';
				}*/
			?>
			
			<?php
			// MODIFIED SHIPPING TIMES
			//if(strtotime("20 December 2006") < mktime()) { // GO BACK TO REGULAR SHIPPING TIMES
				$mst_standard = "5-8";
			//}else{ // MODIFIED SHIPPING TIMES FOR CHRISTMAS
				//$mst_standard = "10-15";
			//}
			?>
				  <div style="padding: 2px; border: 1px solid #555; background-color: #FFFFCC; width: 500px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#555; line-height:15px;"><strong>Standard</strong> = <?=$mst_standard?> business days.<br /> <strong>Priority Mail</strong> = 3-4 business days.<br /><strong>FedEx</strong> = 2-3 business days.<br />(before 2PM MST) <strong>FedEx Overnight</strong> = 1 business day.<br />
				    <strong>International</strong> = 7 business days.</div></td>
				<td class="cobll" bgcolor="#FFFFFF" colspan="2">
					<?=$shipping_type?>
					<input type="hidden" id="shp_method" name="shp_method" value="<?=$_SESSION['shp_method']?>" />
				</td>
			  </tr>
<?php		}
			if($wantcouponcode){ ?>
              <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3" style="line-height: normal"><strong>Coupon Code:</strong><br />
				<span style="font-size: 10px; font-style: italic;">(1 per order, please enter gift certificates on next page)</span></td>
				<td class="cobll" bgcolor="#FFFFFF" colspan="2"><input name="cpncode" type="text" value="<?=$_POST["cpncode"]?>" onchange="mainform.cpncode.value=this.value;"></td>
			  </tr>
<?php		}
			if($wantzipselector){ ?>
              <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><strong><?php print $xxZip?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" colspan="2"><input type="text" name="zip" size="8" value="<?php print str_replace('"','&quot;',$destZip)?>"></td>

			  </tr>
<?php		}
		} ?>
              <tr height="30">
			  <?php	if($addextrarows==0){ ?>
				<td class="cobhl" bgcolor="#EBEBEB" rowspan="2">&nbsp;</td>
			  <?php } ?>
				<td class="cobll" bgcolor="#FFFFFF" align="right" colspan="3"><strong><?php print $xxGndTot?>:</strong></td>
				<td class="cobll" bgcolor="#FFFFFF" align="right"><?php print FormatEuroCurrency(($totalgoods2+$shipping)-($totaldiscounts+$freeshipamnt))?></td>
				<td class="cobll" bgcolor="#FFFFFF" align="center"><?php if($addextrarows==0) print '<a href="#" onclick="javascript:document.checkoutform.submit()"><strong>' . $xxDelete . '</strong></a>'; else print '&nbsp;'; ?></td>
			  </tr>
			  <tr height="30">
				<td class="cobll" bgcolor="#FFFFFF" colspan="5">
				  <table width="100%" cellspacing="0" cellpadding="0" border="0">
				    <tr>
					  <td class="cobll" bgcolor="#FFFFFF" width="50%" align="center"><a href="<?php if(trim(@$_SESSION["frompage"])!="" && (@$actionaftercart==2 || @$actionaftercart==3)) print $_SESSION["frompage"]; else print $xxHomeURL?>"><strong><?php print $xxCntShp?></strong></a></td>
					  <td class="cobll" bgcolor="#FFFFFF" width="50%" align="center"><a href="#" onclick="document.checkoutform.submit()"><strong><?php print $xxUpdTot?></strong></a></td>
					  <td class="cobll" bgcolor="#FFFFFF" width="16" height="26" align="right" valign="bottom"><img src="/lib/images/tablebr.gif" alt="" /></td>
					</tr>
				  </table>
				</td>
			  </tr>
<script language="JavaScript" type="text/javascript">
<!--
function changechecker(){
dowarning=false;
<?php print $changechecker?>
if(dowarning){
	if(confirm('<?php print str_replace("'","\'",$xxWrnChQ)?>')){
		document.checkoutform.submit();
		return false;
	}else
		return(true);
}
return true;
}
//--></script>
<?php
	}else{
		$cartEmpty=TRUE;
?>
              <tr>
			    <td class="cobll" bgcolor="#FFFFFF" colspan="6" align="center">
				  <p>&nbsp;</p>
				  <p><?php print $xxSryEmp?></p>
				  <p>&nbsp;</p>
<script language="JavaScript" type="text/javascript">
<!--
if(document.cookie=="") document.write("<?php print str_replace('"', '\"', $xxNoCk . " " . $xxSecWar)?>");
//--></script>
<noscript><?php print $xxNoJS . " " . $xxSecWar?></noscript>
				  <p><a href="<?php if(trim(@$_SESSION["frompage"])!="" && (@$actionaftercart==2 || @$actionaftercart==3)) print $_SESSION["frompage"]; else print $xxHomeURL?>"><strong><?php print $xxCntShp?></strong></a></p>
				  <p>&nbsp;</p>
				</td>
			  </tr>
<?php
	}
?>			</table>
	</form>
<?php
///////showarray($_SESSION);
}
if(@$_GET["token"] == '' && @$_POST["mode"] != "paypalexpress1" && @$_POST["mode"] != "go" && @$_POST["mode"] != "checkout" && @$_POST["mode"] != "add" && @$_POST["mode"] != "authorize" && ! $cartEmpty && $_POST['mode']!="edit" && $cartisincluded != TRUE){
	if(session_id()=='') print 'The PHP session has not been started. This can cause problems with the shopping cart function. For help please go to <a href="http://www.ecommercetemplates.com/support/">http://www.ecommercetemplates.com/support/</a>';
	$gshipmethods=array();
	function writeuniquegoogleshipmethod($theshipmethod){
		global $countryCurrency,$sXML,$googledefaultshipping,$gshipmethods;
		if(@$googledefaultshipping=='') $googledefaultshipping='999.99';
		$gotshipmethod=FALSE;
		if(! in_array($theshipmethod,$gshipmethods)){
			array_push($gshipmethods, $theshipmethod);
			$sXML .= '<merchant-calculated-shipping name="' . $theshipmethod . '"><price currency="' . $countryCurrency . '">' . $googledefaultshipping . '</price></merchant-calculated-shipping>';
		}
	}
	function generatemerchantcalcshiptypes($theshiptype){
		global $countryCurrency,$sXML,$xxShipHa,$somethingToShip,$googledefaultshipping,$splitUSZones,$gshipmethods;
		if($theshiptype==1 || ! $somethingToShip){
			writeuniquegoogleshipmethod(xmlencodecharref($xxShipHa));
		}elseif($theshiptype==2 || $theshiptype==5){
			for($index3=1; $index3<=5; $index3++){
				$sSQL = "SELECT DISTINCT pzMethodName" . $index3 . " FROM postalzones WHERE pzName<>'' AND pzMethodName" . $index3 . "<>''";
				if(! $splitUSZones) $sSQL .= ' AND pzID < 100';
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs = mysql_fetch_assoc($result)){
					writeuniquegoogleshipmethod(trim(xmlencodecharref($rs['pzMethodName' . $index3])));
				}
			}
		}elseif($theshiptype==3 || $theshiptype==4 || $theshiptype==6 || $theshiptype==7){
			if($theshiptype==3) $startid=0;
			if($theshiptype==4) $startid=1;
			if($theshiptype==6) $startid=2;
			if($theshiptype==7) $startid=3;
			$sSQL = "SELECT DISTINCT uspsShowAs,uspsFSA FROM uspsmethods WHERE (uspsID>" . ($startid*100) . " AND uspsID<" . (($startid+1)*100) . ") AND uspsUseMethod=1 ORDER BY uspsFSA DESC,uspsShowAs";
			$result = mysql_query($sSQL) or print(mysql_error());
			while($rs = mysql_fetch_assoc($result)){
				writeuniquegoogleshipmethod(xmlencodecharref($rs['uspsShowAs']));
			}
		}
	}
	function writegoogleparams($data1, $data2, $demomode){
		global $shipType,$adminIntShipping,$willpickuptext,$willpickupcost,$countryCurrency,$storeurl,$googlelineitems,$thesessionid,$sXML,$gcallbackpath;
		$sSQL = "SELECT cpnID FROM coupons WHERE cpnIsCoupon=1 AND cpnNumAvail > 0 AND cpnEndDate >= '" . date('Y-m-d H:i:s',time()) ."' AND cpnBeginDate <= '" . date('Y-m-d H:i:s',time()) ."'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result)==0) $acoupondefined='false'; else $acoupondefined='true';
		$sXML = '<?xml version="1.0" encoding="UTF-8"?><checkout-shopping-cart xmlns="http://checkout.google.com/schema/2"><shopping-cart>';
		$sXML .= '<items>' . $googlelineitems . '</items>';
		$sXML .= '<merchant-private-data><privateitems><sessionid>' . (@$_SESSION['clientID'] != '' ? 'cid' . $_SESSION['clientID'] : 'sid' . $thesessionid) . '</sessionid><partner>' . xmlencodecharref(trim(@$_COOKIE['PARTNER'])) . '</partner></privateitems></merchant-private-data></shopping-cart>';
		$sXML .= '<checkout-flow-support><merchant-checkout-flow-support><platform-id>236638029623651</platform-id>';
		$sXML .= '<edit-cart-url>' . $storeurl . 'cart.php</edit-cart-url><continue-shopping-url>' . $storeurl . 'categories.php</continue-shopping-url>';
		$sXML .= '<shipping-methods>';
		generatemerchantcalcshiptypes($shipType);
		if($adminIntShipping != 0 && $adminIntShipping != $shipType) generatemerchantcalcshiptypes($adminIntShipping);
		if(@$willpickuptext != ''){
			if(@$willpickupcost=='') $willpickupcost=0;
			$sXML .= '<merchant-calculated-shipping name="' . xmlencodecharref($willpickuptext) . '"><price currency="' . $countryCurrency . '">' . $willpickupcost . '</price></merchant-calculated-shipping>';
		}
		$sXML .= '</shipping-methods>';
		//$sXML .='<analytics-data>%analitics_data%</analytics-data>';
		$sXML .= '<request-buyer-phone-number>true</request-buyer-phone-number><tax-tables merchant-calculated="true"><default-tax-table><tax-rules></tax-rules></default-tax-table></tax-tables>';
		$sXML .= '<merchant-calculations><merchant-calculations-url>' . $gcallbackpath . '</merchant-calculations-url><accept-merchant-coupons>' . $acoupondefined . '</accept-merchant-coupons><accept-gift-certificates>false</accept-gift-certificates></merchant-calculations></merchant-checkout-flow-support></checkout-flow-support>';
		$sXML .= '</checkout-shopping-cart>';
		// print str_replace("<","<br />&lt;",str_replace("</","&lt;/",$sXML)) . "<br />\n";
		$thecart = base64_encode($sXML);
		$thesignature = base64_encode(CalcHmacSha1($sXML,$data2));
		$theurl = 'https://' . ($demomode ? 'sandbox' : 'checkout') . '.google.com' . ($demomode ? '/checkout' : '') . '/cws/v2/Merchant/' . $data1 . '/checkout'; // . '/diagnose';
		writehiddenvar('cart', $thecart);
		writehiddenvar('signature', $thesignature);
		writehiddenvar('analyticsdata', '');
		//writehiddenvar('theurl', $theurl);
		//writehiddenvar('auth',base64_encode($data1.':'.$data2));
		return($theurl);
	}
	$requiressl = FALSE;
	if(@$pathtossl == ""){
		$sSQL = "SELECT payProvID FROM payprovider WHERE payProvEnabled=1 AND (payProvID IN (7,10,12,13,18) OR (payProvID=16 AND payProvData2='1'))"; // All the ones that require SSL
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result) > 0) $requiressl = TRUE;
		mysql_free_result($result);
	}
	$sSQL = "SELECT payProvID FROM payprovider WHERE payProvEnabled=1 AND payProvID=18"; // Check for PayPal Payment Pro
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result) > 0) $paypalexpress = TRUE;
	mysql_free_result($result);
	//echo '@$pathtossl='.@$pathtossl;
	if($requiressl || @$pathtossl != ""){
		if(@$pathtossl != ""){
			if(substr($pathtossl,-1) != "/") $pathtossl .= "/";
			$cartpath = $pathtossl . "cart.php";
			$gcallbackpath = $pathtossl . 'admin/gcallback.php';
		}else
			//$cartpath = str_replace("http:","https:",$storeurl) . "cart.php";
			$cartpath = 'cart.php';
			$gcallbackpath = str_replace('http:','https:',$storeurl) . 'admin/gcallback.php';
	}else{
		$cartpath="cart.php";
		$gcallbackpath= $storeurl . 'admin/gcallback.php';
	}
	//$gcallbackpath= $storeurl . 'admin/gcallback.php';

?>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="<?php print $maintablebg?>" align="center">
        <tr>
          <td width="100%">
		    <div class="cart_box"> <form method="post" name="mainform" action="<?php print $cartpath?>" onsubmit="return changechecker(this)" >
			  <input type="hidden" name="mode" value="checkout" />
			  <input type="hidden" name="sessionid" value="<?php print session_id();?>" />
			  <input type="hidden" name="PARTNER" value="<?php print trim(@$_COOKIE["PARTNER"]).trim(@$_COOKIE["ifrogz_affiliate"])//Blake added ifrogz_affiliate ?>" />
			  <input type="hidden" name="estimate" value="<?php print number_format(($totalgoods+$shipping)-($totaldiscounts+$freeshipamnt),2,'.','') ?>" />
				<input name="cpncode" type="hidden" value="<?=$_POST["cpncode"]?>" />
<?php			if(trim(@$_SESSION["clientUser"]) != ""){
					mysql_query("DELETE FROM tmplogin WHERE tmplogindate < '" . date("Y-m-d H:i:s", time()-(3*60*60*24)) . "' OR tmploginid='" . session_id() . "'") or print(mysql_error());
					mysql_query("INSERT INTO tmplogin (tmploginid, tmploginname, tmplogindate) VALUES ('" . session_id() . "','" . trim($_SESSION["clientUser"]) . "','" . date("Y-m-d H:i:s", time()) . "')") or print(mysql_error());
					print '<input type="hidden" name="checktmplogin" value="1" />';
					if(($_SESSION["clientActions"] & 8) == 8 || ($_SESSION["clientActions"] & 16) == 16){
						if(@$minwholesaleamount!="") $minpurchaseamount=$minwholesaleamount;
						if(@$minwholesalemessage!="") $minpurchasemessage=$minwholesalemessage;
					}
				}
?>
			 <table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="<?php print $innertablebg?>">
<?php		if($totalgoods < @$minpurchaseamount){ ?>
				<tr>
				  <td width="100%" align="center" colspan="2"><?php print @$minpurchasemessage?></td>
				</tr>
<?php 		//} elseif($paypalexpress){ ?>
				<!--<tr>
				  <td align="center" colspan="2"><?php print $xxPPPBlu?></td>
				</tr>
				<tr>
				  <td colspan="2" align="center"><input type="image" src="/lib/images/ppexpress.gif" border="0" onclick="javascript:document.forms.mainform.mode.value='paypalexpress1';" /></td>
				</tr>-->
<?php
			}else{ ?>
				<!-- <tr>
				  <td width="50%" align="right">
				 	<div style="width: 170px; text-align: center">
						<div style="font-size: 16px; background: #E3E3E3; border: 1px solid #E3E3E3"><strong>Have a promo code?</strong></div>
						<div style="border: 1px solid #E3E3E3; font-size: 12px">Enter it on the next page.</div>
					</div>
				  </td>
				  <td width="50%" align="left">
					<div style="width: 150px">
						<div style="text-align: center; background: #FF9900; border-top: 1px solid #FF9900; border-right: 1px solid #FF9900; border-left: 1px solid #FF9900; color: #FFFFFF; font-weight: bold">
							<span style="margin: 3px">Shipping Prices</span>
						</div>
						<div style="font-size: 12px; font-weight: bold; border-bottom: 1px solid #FF9900; border-right: 1px solid #FF9900; border-left: 1px solid #FF9900">
							<div style="clear: both">
								<div style="float: left; margin-left: 10px">U.S.</div><div style="float:right; margin-right: 10px">$5</div>
							</div>
							<div style="clear: both">
								<div style="float: left; margin-left: 10px">Canada</div><div style="float:right; margin-right: 10px">$9</div>
							</div>
							<div style="clear: both">
								<div style="float: left; margin-left: 10px">International</div><div style="float:right; margin-right: 10px">$15</div>
							</div>
							<div style="clear: both"></div>
						</div>
					</div>
				  </td>
				</tr> -->
				<? if(@$_SESSION['clientLoginLevel'] != '') $minloglevel=$_SESSION['clientLoginLevel']; else $minloglevel=0;
				$sSQL = "SELECT payProvID,payProvData1,payProvData2,payProvDemo FROM payprovider WHERE payProvEnabled=1 AND payProvLevel<=" . $minloglevel . " ORDER BY payProvOrder";
				$result = mysql_query($sSQL) or print(mysql_error());
				$regularcheckoutshown=FALSE;
				$showgoogle=FALSE;
				while($rs = mysql_fetch_assoc($result)){
					?>

<?php				if($rs['payProvID']==20){
						$showgoogle=TRUE;
						$payProvData1=$rs['payProvData1'];
						$payProvData2=$rs['payProvData2'];
						$payProvDemo=$rs['payProvDemo'];
						//echo '$gcallbackpath='.$gcallbackpath;
					}elseif(! $regularcheckoutshown){
						$regularcheckoutshown=TRUE; ?>

				<tr>
				  <td width="100%" align="center" colspan="2"><strong><?php print $xxPrsChk?></strong></td>
				</tr>
				<tr>
				  <td align="center" colspan="2"><input type="image" src="/lib/images/design/checkout.gif" border="0"  onclick="javascript:document.forms.mainform.mode.value='checkout';" /></td>
				</tr>

<?php				}
				if($rs['payProvID']==19){ ?>
				<tr><th align="center" colspan="2">OR<?php //print $xxPPPBlu?></th></tr>
				<tr><td colspan="2" align="center"><input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckoutsm.gif" border="0" onclick="javascript:document.forms.mainform.mode.value='paypalexpress1';" alt="PayPal Express" /></td></tr>
				<? }
					}
					mysql_free_result($result);
				} ?>
			  </table>
			</form>
			<? if($showgoogle && empty($_COOKIE['ifrogz_affiliate'])){?>
			<div style="text-align:center; font-weight:bold;">OR</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="<?php print $innertablebg?>">
			<? if($xxGooCo != ''){ ?><tr><td align="center" colspan="2"><strong><?php print $xxGooCo?></strong></td></tr><?php } ?>
				<tr><td colspan="2" align="center">

				<form action="" name="googleform" method="POST" onsubmit="setUrchinInputCode();" >
				<? $theurl = writegoogleparams($payProvData1, $payProvData2, $payProvDemo);?>
					<input type="image" name="GBuy" alt="Google Checkout" src="https://checkout.google.com/buttons/checkout.gif?merchant_id=<?php print $rs['payProvData1'] . (@$googlebuttonparams!='' ? $googlebuttonparams : '&w=160&h=43&style=white&variant=text&loc=en_US') ?>" onclick="document.forms.googleform.action='<?php print $theurl?>';">
				</form>
				<script src="https://checkout.google.com/files/digital/urchin_post.js" type="text/javascript"></script>
				</td></tr>
			  </table>
			<? } ?>
		  </div>
		</td>
        </tr>
      </table>

<?php

}

?>