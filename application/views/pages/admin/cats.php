<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $.noConflict();
</script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8/jquery-ui.custom.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8/ui-lightness/jquery-ui.custom.css" type="text/css" media="screen" title="no title" charset="utf-8">
<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
//showarray($_POST);

if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$maxcatsperpage = 100;
$maxfilters = 20;
if(@$maxloginlevels=="") $maxloginlevels=5;
$sSQL = "";
$alldata = "";
$alreadygotadmin = getadminsettings();
if(@$_POST["act"]=="changepos"){
	$currentorder = (int)@$_POST["selectedq"];
	$neworder = (int)@$_POST["newval"];
	$sectionTag=$_POST['sectionTag'];
	$sSQL = "SELECT sectionID, sectionTag FROM sections ORDER BY sectionOrder";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rowcounter=1;
	while($rs = mysql_fetch_assoc($result)){
		$theorder = $rowcounter;		 
		$theTag = $rs['sectionTag'];
		if($currentorder == $theorder){
			$theorder = $neworder;
			$theTag = $sectionTag;
		}elseif(($currentorder > $theorder) && ($neworder <= $theorder))
			$theorder++;
		elseif(($currentorder < $theorder) && ($neworder >= $theorder))
			$theorder--;
		$sSQL="UPDATE sections SET sectionOrder=" . $theorder . ",sectionTag='".$theTag."' WHERE sectionID=" . $rs["sectionID"];
		mysql_query($sSQL) or print(mysql_error());
		$rowcounter++;
	}	
	print '<meta http-equiv="refresh" content="1; url=/admin/cats.php?pg=' . @$_POST["pg"] . '">';
}elseif(@$_POST["posted"]=="1"){
	if(@$_POST["act"]=="delete"){
		$sSQL = "DELETE FROM cpnassign WHERE cpaType=1 AND cpaAssignment='" . @$_POST["id"] . "'";
		mysql_query($sSQL) or print(mysql_error());
		$sSQL = "DELETE FROM sections WHERE sectionID=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error());
		$sSQL = "DELETE FROM multisections WHERE pSection=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error());
		print '<meta http-equiv="refresh" content="2; url=/admin/cats.php?pg=' . @$_POST["pg"] . '">';
	}elseif(@$_POST["act"]=="domodify"){
		$sSQL = "UPDATE sections 
				 SET sectionName='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secname"]))) . "',sectionDescription='" . mysql_real_escape_string(unstripslashes(@$_POST["secdesc"])) . "',sectionImage='" . mysql_real_escape_string(unstripslashes(@$_POST["secimage"])) . "',sectionSideImage='" . mysql_real_escape_string(unstripslashes(@$_POST["secsideimage"])) . "',sectionBannerImage='" . mysql_real_escape_string(unstripslashes(@$_POST["secbannerimage"])) . "',flash_movie='" . mysql_real_escape_string(unstripslashes(@$_POST["secflash"])) . "',prod_body_format='" . mysql_real_escape_string(unstripslashes(@$_POST["secformat"])) . "',sectionDetailURL='" . @$_POST["secDetailURL"] . "',prod_body_desc='" . @$_POST["prod_body_desc"] . "',topSection=" . @$_POST["tsTopSection"] . ",sectionDisplay='".@$_POST["sectionDisplay"]."',rootSection=" . @$_POST["catfunction"].", sectionImagesOther = '".$_POST['otherImages']."'";//custom code
		$workname = mysql_real_escape_string(unstripslashes(trim(@$_POST["secworkname"])));
		if($workname != "")
			$sSQL .= ",sectionWorkingName='" . $workname . "'";
		else
			$sSQL .= ",sectionWorkingName='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secname"]))) . "'";
		for($index=2; $index <= $adminlanguages+1; $index++){
			if(($adminlangsettings & 256)==256) $sSQL .= ",sectionName" . $index . "='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secname" . $index]))) . "'";
			if(($adminlangsettings & 512)==512) $sSQL .= ",sectionDescription" . $index . "='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secdesc" . $index]))) . "'";
		}
		$sSQL .= ",sectionDisabled = " . trim(@$_POST["sectionDisabled"]);
		$sSQL .= ",sectionurl = '" . mysql_real_escape_string(unstripslashes(trim(@$_POST["sectionurl"]))) . "'";
		$sSQL .= ",sectionTitleTag = '" . mysql_real_escape_string(unstripslashes(trim(@$_POST["title_tag"]))) . "'";
		$sSQL .= ",sectionMetaDescription = '" . mysql_real_escape_string(unstripslashes(trim(@$_POST["meta_description"]))) . "'";
		
		$sSQL .= " WHERE sectionID=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error() . $sSQL);
		print '<meta http-equiv="refresh" content="2; url=/admin/cats.php?pg=' . @$_POST["pg"] . '">';
	}elseif(@$_POST["act"]=="doaddnew"){
		$sSQL = "SELECT MAX(sectionOrder) AS mxOrder FROM sections";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_assoc($result);
		$mxOrder = $rs["mxOrder"];
		if(is_null($mxOrder) || $mxOrder=="") $mxOrder=1; else $mxOrder++;
		mysql_free_result($result);
		$sSQL = "INSERT INTO sections (sectionName,sectionDescription,sectionImage,sectionDisplay,sectionSideImage,sectionBannerImage,flash_movie,prod_body_format,prod_body_desc,sectionOrder,topSection,rootSection,sectionWorkingName";//custom code
		for($index=2; $index <= $adminlanguages+1; $index++){
			if(($adminlangsettings & 256)==256) $sSQL .= ",sectionName" . $index;
			if(($adminlangsettings & 512)==512) $sSQL .= ",sectionDescription" . $index;
		}
		$sSQL .= ",sectionDisabled,sectionurl,sectionDetailURL, sectionImagesOther, sectionTitleTag, sectionMetaDescription) VALUES ('" . mysql_real_escape_string(unstripslashes(@$_POST["secname"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["secdesc"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["secimage"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["sectionDisplay"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["secsideimage"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["secbannerimage"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["secflash"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["secformat"])) . "','" . mysql_real_escape_string(unstripslashes(@$_POST["prod_body_desc"])) . "'," . $mxOrder . "," . @$_POST["tsTopSection"] . "," . @$_POST["catfunction"];
		$workname = mysql_real_escape_string(unstripslashes(trim(@$_POST["secworkname"])));
		if($workname != "")
			$sSQL .= ",'" . $workname . "'";
		else
			$sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secname"]))) . "'";
		for($index=2; $index <= $adminlanguages+1; $index++){
			if(($adminlangsettings & 256)==256) $sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secname" . $index]))) . "'";
			if(($adminlangsettings & 512)==512) $sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["secdesc" . $index]))) . "'";
		}
		$sSQL .= "," . trim(@$_POST["sectionDisabled"]);
		$sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["sectionurl"]))) . "'";
		$sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["sectionDetailURL"]))) . "'";
		$sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["otherImages"]))) . "'";
		$sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["title_tag"]))) . "'";
		$sSQL .= ",'" . mysql_real_escape_string(unstripslashes(trim(@$_POST["meta_description"]))) . "')";
		
		//echo $sSQL; exit();
		
		mysql_query($sSQL) or print(mysql_error());
		$inserted_cat=mysql_insert_id();
		print '<meta http-equiv="refresh" content="2; url=/admin/cats.php?pg=' . @$_POST["pg"] . '">';
	}elseif(@$_POST["act"]=="dodiscounts"){
		$sSQL = "INSERT INTO cpnassign (cpaCpnID,cpaType,cpaAssignment) VALUES (" . @$_POST["assdisc"] . ",1,'" . @$_POST["id"] . "')";
		mysql_query($sSQL) or print(mysql_error());
		print '<meta http-equiv="refresh" content="2; url=/admin/cats.php?pg=' . @$_POST["pg"] . '">';
	}elseif(@$_POST["act"]=="deletedisc"){
		$sSQL = "DELETE FROM cpnassign WHERE cpaID=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error());
		print '<meta http-equiv="refresh" content="2; url=/admin/cats.php?pg=' . @$_POST["pg"] . '">';
	}
}
?>
<script language="JavaScript" type="text/javascript">
<!--

function formvalidator(theForm)
{
  if (theForm.secname.value == "")
  {
    alert("<?php print $yyPlsEntr?> \"<?php print $yyCatNam?>\".");
    theForm.secname.focus();
    return (false);
  }
  if (theForm.tsTopSection[theForm.tsTopSection.selectedIndex].value == "")
  {
    alert("<?php print $yyPlsSel?> \"<?php print $yyCatSub?>\".");
    theForm.tsTopSection.focus();
    return (false);
  }
  return (true);
}
//-->
</script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		
		if (jQuery('#sortable')) {
			jQuery("#sortable").sortable({ 
				cursor: 'move',
				update: function (event, ui) {
					var list_order = jQuery(this).sortable('toArray');
					var section_id = jQuery('#id').val();
					console.log(list_order);
					jQuery.post('/admin/sortcategory.php', { data: list_order, section_id: section_id }, function(data){
						
					}, 'json');
				}
			});
			jQuery("#sortable").disableSelection();
		}
		
	});
</script>
<style type="text/css" media="screen">
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 4px; padding-left: 15px; font-size: 14px; height: 18px; cursor: move; }
	#sortable li span { position: absolute; margin-left: -15px; }
</style>
      <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
	  
<?php
if(@$_POST["posted"]=="1" && (@$_POST["act"]=="modify" || @$_POST["act"]=="addnew")){
		$ntopsections=0;
		$sSQL = "SELECT sectionID, sectionWorkingName FROM sections WHERE rootSection=0 ORDER BY sectionWorkingName";
		$result = mysql_query($sSQL) or print(mysql_error());
		while($rs = mysql_fetch_assoc($result))
			$alltopsections[$ntopsections++] = $rs;
		mysql_free_result($result);
		if(@$_POST["act"]=="modify"){
			$sSQL = "SELECT sectionID,sectionName,sectionName2,sectionDisplay,sectionName3,sectionDescription,sectionDescription2,sectionDescription3,sectionImage,sectionSideImage,sectionBannerImage,sectionWorkingName,topSection,sectionDisabled,rootSection,sectionurl,flash_movie,prod_body_format,prod_body_desc,sectionDetailURL, sectionImagesOther, sectionTitleTag, sectionMetaDescription FROM sections WHERE sectionID=" . @$_POST["id"];//custom code
			$result = mysql_query($sSQL) or print(mysql_error());
			$rs = mysql_fetch_assoc($result);
			$sectionID = $rs["sectionID"];
			$sectionName = $rs["sectionName"];
			$sectionDescription = $rs["sectionDescription"];
			for($index=2; $index <= $adminlanguages+1; $index++){
				$sectionNames[$index] = $rs["sectionName" . $index];
				$sectionDescriptions[$index] = $rs["sectionDescription" . $index];
			}
			$sectionImage = $rs["sectionImage"];
			$sectionSideImage = $rs["sectionSideImage"];
			$sectionBannerImage = $rs["sectionBannerImage"];
			$sectionFlash = $rs["flash_movie"];// custom code
			$sectionFormat = $rs["prod_body_format"];// custom code
			$sectionDisplay = $rs["sectionDisplay"];// custom code
			$sectionDetailURL = $rs["sectionDetailURL"];
			$prod_body_desc = $rs["prod_body_desc"];// custom code
			$sectionWorkingName = $rs["sectionWorkingName"];
			$topSection = $rs["topSection"];
			$sectionDisabled = $rs["sectionDisabled"];
			$rootSection = $rs["rootSection"];
			$sectionurl = $rs["sectionurl"];
			$otherImages = $rs["sectionImagesOther"];// custom code
			$sectionTitleTag = $rs['sectionTitleTag'];
			$sectionMetaDescription = $rs['sectionMetaDescription'];
			mysql_free_result($result);
		}else{
			$sectionID = "";
			$sectionName = "";
			$sectionDescription = "";
			for($index=2; $index <= $adminlanguages+1; $index++){
				$sectionNames[$index] = "";
				$sectionDescriptions[$index] = "";
			}
			$sectionImage = "";
			$sectionSideImage = "";
			$sectionBannerImage = "";
			$sectionFlash = "";// custom code
			$sectionFormat ="";// custom code
			$prod_body_desc ="";// custom code
			$sectionWorkingName = "";
			$topSection = 0;
			$sectionDisabled = 0;
			$rootSection = 1;
			$sectionurl = "";
			$sectionDetailURL = "";
			$sectionDisplay = "";
			$otherImages = "";// custom code
			$sectionTitleTag = '';
			$sectionMetaDescription = '';
		}
?>
        
		<tr>
		
		  <td width="100%">
			
            <form name="mainform" method="post" action="/admin/cats.php" onsubmit="return formvalidator(this)">
			<input type="hidden" name="posted" value="1" />
			<?php if(@$_POST["act"]=="modify"){ ?>
			<input type="hidden" name="act" value="domodify" />
			<?php }else{ ?>
			<input type="hidden" name="act" value="doaddnew" />
			<?php } ?>
			<input type="hidden" name="id" value="<?php print @$_POST["id"]?>" />
			<input type="hidden" name="pg" value="<?php print @$_POST["pg"]?>" />
			<table border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td colspan="2" align="center"><strong><?php print $yyCatAdm?></strong><br />&nbsp;</td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><strong><?php print $yyCatNam?></strong><br /><input type="text" name="secname" size="30" value="<?php print str_replace("\"","&quot;",$sectionName)?>" />
				  <br />
<?php	for($index=2; $index <= $adminlanguages+1; $index++){
			if(($adminlangsettings & 256)==256){ ?>
				<strong><?php print $yyCatNam . " " . $index ?></strong><br />
				<input type="text" name="secname<?php print $index?>" size="30" value="<?php print str_replace('"','&quot;',$sectionNames[$index])?>" /><br />
<?php		}
		} ?>                </td>
				<td rowspan="19" align="center" valign="top"><strong><?php print $yyCatDes?></strong> <a href="#info">?</a><br /><textarea name="secdesc" cols="38" rows="8" wrap=virtual><?php print $sectionDescription?></textarea><br />
<?php	for($index=2; $index <= $adminlanguages+1; $index++){
			if(($adminlangsettings & 512)==512){ ?>
				<strong><?php print $yyCatDes . " " . $index ?></strong> <a href="#info">?</a><br />
				<textarea name="secdesc<?php print $index?>" cols="38" rows="8" wrap=virtual><?php print $sectionDescriptions[$index]?></textarea><br />
<?php		}
		} ?>
				&nbsp;<br /><select name="sectionDisabled" size="1">
				<option value="0"><?php print $yyNoRes?></option>
			<?php	for($index=1; $index<= $maxloginlevels; $index++){
						print '<option value="' . $index . '"';
						if($sectionDisabled==$index) print ' selected';
						print '>' . $yyLiLev . ' ' . $index . '</option>';
					} ?>
				<option value="127"<?php if($sectionDisabled==127) print ' selected'?>><?php print $yyDisCat?></option>
				</select>
				 Do not display: 
				 <input type="checkbox" name="sectionDisplay" value="1" <?= $sectionDisplay==1 ? 'checked="checked"' : '' ?> />
				 <a href="#info">?</a><br />
				&nbsp;<br /><strong>Category URL (Optional)</strong><br />
				<input type="text" name="sectionurl" size="40" value="<?php print str_replace('"','&quot;',$sectionurl)?>" /> <a href="#info">?</a>                
				
				<br /><br />
				<strong>Title Tag</strong><br />
				<input type="text" name="title_tag" id="title_tag" size="40" value="<?php echo $sectionTitleTag ?>" />
				
				<br /><br />
				<strong>Meta Description</strong><br />
				<input type="text" name="meta_description" id="meta_description" size="40" value="<?php echo $sectionMetaDescription ?>" />
				
				</td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><strong><?php print $yyCatWrNa?></strong></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><input type="text" name="secworkname" size="30" value="<?php print str_replace("\"","&quot;",$sectionWorkingName)?>" /></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><strong><?php print $yyCatSub?> </strong> <a href="#info">?</a></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><select name="tsTopSection" size="1"><option value="0"><?php print $yyCatHom?></option>
				<?php	$foundcat=($topSection==0);
						for($index=0;$index<$ntopsections; $index++){
							if($alltopsections[$index]["sectionID"] != $sectionID){
								print '<option value="' . $alltopsections[$index]["sectionID"] . '"';
								if($topSection==$alltopsections[$index]["sectionID"]){
									print " selected";
									$foundcat=TRUE;
								}
								print ">" . $alltopsections[$index]["sectionWorkingName"] . "</option>\n";
							}
						}
						if(! $foundcat) print '<option value="" selected>**undefined**</option>';
					?></select>                 <a href="#info"></a></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><strong><?php print $yyCatFn?></strong> <a href="#info">?</a> </td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><select name="catfunction" size="1">
				  <option value="1"><?php print $yyCatPrd?></option>
				  <option value="0" <?php if($rootSection==0) print "selected"?>><?php print $yyCatCat?></option>
				  </select> </td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><strong><?php print $yyCatImg?> </strong> <a href="#info">?</a></td>
			  </tr>
			  <tr>
				<td align="center" valign="top"><input type="text" name="secimage" size="30" value="<?php print str_replace("\"","&quot;",$sectionImage)?>" />
			    </td>
			  </tr>
			  <tr>
			    <td align="center" valign="top"><strong>Category Side Image</strong> <a href="#info">?</a></td>
		      </tr>
			  <tr>
			    <td align="center" valign="top"><input type="text" name="secsideimage" size="30" value="<?php print $sectionSideImage?>" />
		        </td>
		      </tr>
			  <tr>
			    <td align="center" valign="top"><strong>Category Banner Image</strong> <a href="#info">?</a></td>
		      </tr>
			  <tr>
			    <td align="center" valign="top"><input type="text" name="secbannerimage" size="30" value="<?php print $sectionBannerImage?>" /></td>
		      </tr>
			  
			  <tr>
			    <td align="center" valign="top">&nbsp;</td>
		      </tr>
			  <tr>
			    <td align="center" valign="top">					
					<fieldset style="width:300px; "><legend><strong>Use Only For Custom Ordering Pages</strong></legend>
					<strong><?php print $yyFlaMov?></strong> <a href="#info">?</a><br />
					<input type="text" name="secflash" id="secflash" size="30" value="<?php print $sectionFlash?>" />
					<br />
					<strong><?php print $yyFormat?></strong> <a href="#info">?</a><br />
					<input name="secformat" type="text" id="secformat" value="<?php print $sectionFormat?>" size="4" />
					<br />
					<strong>Details URL</strong> <a href="#info">?</a><br />
					<input name="secDetailURL" type="text" id="secDetailURL" value="<?php print $sectionDetailURL?>"  />
					<br />
					<strong>Custom Cat Description</strong> <a href="#info">?</a>
					<textarea name="prod_body_desc" cols="38" rows="8"><?=$prod_body_desc?></textarea>
					<br />
					<strong>Other Images</strong>&nbsp;<a href="#info">?</a>
					<textarea name="otherImages" id="otherImages" cols="38" rows="8"><?=$otherImages?></textarea>
					</fieldset>				
				</td>
		      </tr>			  
			  <tr> 
                <td colspan="2" align="center"><br /><input type="submit" value="<?php print $yySubmit?>" /></td>
			  </tr>
			  <tr> 
                <td colspan="2"><br /><ul>
				  <li><a name="info" id="info"></a><?php print $yyCatEx1?></li>
				  <li><?php print $yyCatEx2?></li>
				  <li><strong>Category Image:</strong> the url of the image you want to display for this category.</li>
                  <li><strong>Category Side Image:</strong> sets the side image you want to use for this category instead of just a random image.</li>
                  <li><strong>Category Banner Image:</strong> sets the banner image for this category.</li>
                  <li><strong>Category Flash Movie:</strong> the url to the flash movie that will display for this product. (ex. preloader.swf?loadProduct=nameofmovie.swf) 
                  <li><strong>Body Format:</strong> set the display format of the product pages. 4 is for the custom flash page, only put 4 if you have a flash movie otherwise leave blank.</li>
                  <li><strong>Details Link:</strong> url of a details page for this category. This button will appear on the flash page linking to this url. <img src="/lib/images/design/product_details.gif" /> </li>
                  <li><strong>Custom Cat Description:</strong> is intended to display filler content/text on the custom page after the Wrapz, Bandz and Screenz sections.</li>
				  <li><strong>Category Description:</strong> description of the category that shows up on the category page. Does not show up on any products page.</li>
                  <li><strong>Restrictions/Login Level:</strong> No restrictions will display category. Login level 1 will only show category if they have an account set to login level 1. 5 Levels.</li>
                  <li><strong>Do not display:</strong> will not show category no matter what the Restriction/Login Level is set at.</li>
                  <li><strong>Category URL:</strong> url of the category if you have created a different page than the default category.php page. Mostly used to improve SEO rankings.</li>
				  <li><strong>Other Images:</strong> A comma seperated list of image paths.  These images are displayed as thumbnails in the empty area under wrapz and uses Lightbox to display the full image.</li>
                  </ul>                  </td>
			  </tr>
			  <tr> 
                <td colspan="2" align="center"><br />
                          <a href="/admin/index.php"><strong><?php print $yyAdmHom?></strong></a><br />
                          &nbsp;</td>
			  </tr>
            </table>
            </form>
		  </td>
        </tr>
		
<?php
}elseif(@$_POST["act"]=="discounts"){
		$sSQL = "SELECT sectionName FROM sections WHERE sectionID=" . @$_POST["id"];
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_assoc($result);
		$thisname=$rs["sectionName"];
		mysql_free_result($result);
		$numassigns=0;
		$sSQL = "SELECT cpaID,cpaCpnID,cpnWorkingName,cpnSitewide,cpnEndDate,cpnType,cpnBeginDate FROM cpnassign LEFT JOIN coupons ON cpnassign.cpaCpnID=coupons.cpnID WHERE cpaType=1 AND cpaAssignment='" . @$_POST["id"] . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		while($rs=mysql_fetch_assoc($result))
			$alldata[$numassigns++]=$rs;
		mysql_free_result($result);
		$numcoupons=0;
		$sSQL = "SELECT cpnID,cpnWorkingName,cpnSitewide FROM coupons WHERE (cpnSitewide=0 OR cpnSitewide=3) AND cpnEndDate >='" . date("Y-m-d H:i:s",time()) ."'";
		$result = mysql_query($sSQL) or print(mysql_error());
		while($rs=mysql_fetch_assoc($result))
			$alldata2[$numcoupons++]=$rs;
		mysql_free_result($result);
?>
<script language="JavaScript" type="text/javascript">
<!--
function delrec(id) {
cmsg = "<?php print $yyConAss?>\n"
if (confirm(cmsg)) {
	document.mainform.id.value = id;
	document.mainform.act.value = "deletedisc";
	document.mainform.submit();
}
}
// -->
</script>
        <tr>
		<form name="mainform" method="post" action="/admin/cats.php">
		  <td width="100%">
			<input type="hidden" name="posted" value="1" />
			<input type="hidden" name="act" value="dodiscounts" />
			<input type="hidden" name="id" value="<?php print @$_POST["id"]?>" />
			<input type="hidden" name="pg" value="<?php print @$_POST["pg"]?>" />
            <table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><strong><?php print $yyAssDis?> &quot;<?php print $thisname?>&quot;.</strong><br />&nbsp;</td>
			  </tr>
<?php
	$gotone=FALSE;
	if($numcoupons>0){
		$thestr = '<tr><td colspan="4" align="center">' . $yyAsDsCp . ': <select name="assdisc" size="1">';
		for($index=0;$index < $numcoupons;$index++){
			$alreadyassign=FALSE;
			if($numassigns>0){
				for($index2=0;$index2<$numassigns;$index2++){
					if($alldata2[$index]["cpnID"]==$alldata[$index2]["cpaCpnID"]) $alreadyassign=TRUE;
				}
			}
			if(! $alreadyassign){
				$thestr .= "<option value='" . $alldata2[$index]["cpnID"] . "'>" . $alldata2[$index]["cpnWorkingName"] . "</option>\n";
				$gotone=TRUE;
			}
		}
		$thestr .= "</select> <input type='submit' value='Go' /></td></tr>";
	}
	if($gotone){
		print $thestr;
	}else{
?>
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyNoDis?></td>
			  </tr>
<?php
	}
	if($numassigns>0){
?>
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyCurDis?> &quot;<?php print $thisname?>&quot;.</strong><br />&nbsp;</td>
			  </tr>
			  <tr> 
                <td><strong><?php print $yyWrkNam?></strong></td>
				<td><strong><?php print $yyDisTyp?></strong></td>
				<td><strong><?php print $yyBegin?></strong></td>
				<td><strong><?php print $yyExpire?></strong></td>
				<td align="center"><strong><?php print $yyDelete?></strong></td>
			  </tr>
<?php
		for($index=0;$index<$numassigns;$index++){
			$prefont = "";
			$postfont = "";
			if((int)$alldata[$index]["cpnSitewide"]==1 || ($alldata[$index]["cpnEndDate"] != '3000-01-01 00:00:00' && strtotime($alldata[$index]["cpnEndDate"])-time() < 0)){
				$prefont = '<font color="#FF0000">';
				$postfont = "</font>";
			}
?>
			  <tr> 
                <td><?php	print $prefont . $alldata[$index]["cpnWorkingName"] . $postfont ?></td>
				<td><?php	if($alldata[$index]["cpnType"]==0)
								print $prefont . $yyFrSShp . $postfont;
							elseif($alldata[$index]["cpnType"]==1)
								print $prefont . $yyFlatDs . $postfont;
							elseif($alldata[$index]["cpnType"]==2)
								print $prefont . $yyPerDis . $postfont; ?></td>
				<td><?php
					if ($alldata[$index]["cpnBeginDate"] != '0000-00-00 00:00:00') {
						echo $prefont . $alldata[$index]["cpnBeginDate"] . $postfont;
					} else {
						echo $prefont . "&nbsp;" . $postfont;
					}
				?></td>
				<td><?php	print $prefont;
							if($alldata[$index]["cpnEndDate"] == '3000-01-01 00:00:00')
								print $yyNever;
							elseif(strtotime($alldata[$index]["cpnEndDate"])-time() < 0)
								print $yyExpird;
							else
								print date("Y-m-d H:i:s",strtotime($alldata[$index]["cpnEndDate"]));
							print $postfont; ?></td>
				<td align="center"><input type="button" name="discount" value="Delete Assignment" onclick="delrec('<?php print $alldata[$index]["cpaID"]?>')" /></td>
			  </tr>
<?php
		}
	}else{
?>
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyNoAss?></td>
			  </tr>
<?php
	}
?>
			  <tr>
                <td width="100%" colspan="4" align="center"><br />&nbsp;</td>
			  </tr>
			  <tr> 
                <td width="100%" colspan="4" align="center"><br />
                          <a href="/admin/index.php"><strong><?php print $yyAdmHom?></strong></a><br />
                          &nbsp;</td>
			  </tr>
            </table></td>
		  </form>
        </tr>
<?php
}elseif(@$_POST["act"]=="changepos"){ ?>
        <tr>
          <td width="100%" align="center">
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p><strong><?php print $yyUpdat?> . . . . . . . </strong></font></p>
			<p>&nbsp;</p>
			<p><?php print $yyNoFor?> <a href="/admin/cats.php"><?php print $yyClkHer?></a>.</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		  </td>
		</tr>
<?php
} else if ($_POST['act'] == 'sort') {
	// Get the current section name
	$sql = "SELECT * FROM sections WHERE sectionID = {$_POST['id']}";
	$res = mysql_query($sql) or print(mysql_error());
	$section = mysql_fetch_assoc($res);
	
	// Get all child categories and products
	$list = array();
	$sql = "SELECT * 
			FROM sections 
			WHERE topSection = {$_POST['id']} 
			AND sectionDisabled <= 10 
			AND sectionDisplay = 0
			ORDER BY sectionOrder";
	//echo $sql;
	$res = mysql_query($sql) or print(mysql_error());
	
	while ($row = mysql_fetch_assoc($res)) {
		$list[] = $row;
	}
	
	/*
	echo '<pre>';
	print_r($list);
	echo '</pre>';
	*/
	
	$sql = "SELECT *
	        FROM products p
	        WHERE pSection = {$_POST['id']}
	        OR p.pID IN(
                SELECT m.pID
                FROM multisections m
                WHERE m.pSection = {$_POST['id']} 
            )
	        AND pDisplay = 1
	        ORDER BY pOrder";
	$res = mysql_query($sql) or print(mysql_error());
	
	while ($row = mysql_fetch_assoc($res)) {
		$list[] = $row;
	}
	
	// Get the sort order
	$sort = array();
	$sql = "SELECT * FROM RFCategoryOrder WHERE Section_IDParent = {$_POST['id']} ORDER BY Sequence";
	$res = mysql_query($sql) or print(mysql_error());
	
	while ($row = mysql_fetch_assoc($res)) {
		if ($row['Section_ID'] > 0) {
			$sort['section_' . $row['Section_ID']] = $row;
		} else {
			$sort['product_' . $row['Product_ID']] = $row;
		}
	}
	
	/*
	echo '<pre>';
	print_r($sort);
	echo '</pre>';
	*/
	
	// Now re-index the list
	$new_list = array();
	$last = 999;
	for ($i=0; $i < count($list); $i++) { 
		if ($list[$i]['ID']) { // It's a product
			$key = 'product_' . $list[$i]['ID'];
			if (array_key_exists($key, $sort)) {
				$new_list[$sort[$key]['Sequence']] = $list[$i];
			} else {
				$new_list[$last++] = $list[$i];
			}
		} else { // It's a section
			$key = 'section_' . $list[$i]['sectionID'];
			if (array_key_exists($key, $sort)) {
				$new_list[$sort[$key]['Sequence']] = $list[$i];
			} else {
				$new_list[$last++] = $list[$i];
			}
		}
	}
	
	/*
	echo '<pre>';
	print_r(array_keys($new_list));
	echo '</pre>';
	*/

	function mysort($a, $b) {
		if ($a == $b) {
			return 0;
		}
		return ($a < $b) ? -1 : 1;
	}

	// Sort the list
	uksort($new_list, 'mysort');
	
	/*
	echo '<pre>';
	print_r($new_list);
	echo '</pre>';
	*/
	
?>

		<tr>
			<td height="50"><span style="margin-left: 20px; font-size: 24px; font-weight: bold;"><?php echo $section['sectionName'] ?></span></td>
		</tr>
		<tr>
			<td>
				<form name="mainform" method="post" action="/admin/cats.php">
				<input type="hidden" id="id" name="id" value="<?php print @$_POST["id"]?>" />
				
				<ul id="sortable">
					<?php
					$i = 0;
					foreach ($new_list as $key => $value) {
						$text = '';
						if ($value['sectionID']) {
							$text = $value['sectionName'];
							$id = 'section_' . $value['sectionID'];
						} else {
							$text = $value['pName'];
							$id = 'product_' . $value['ID'];
						}
					?>
					<li class="ui-state-default" id="<?php echo $id ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php echo $text ?></li>
					<?php
					}
					?>
				</ul>
				
				</form>
			</td>
		</tr>
<?php
}elseif(@$_POST["posted"]=="1" && $success){ ?>
        <tr>
          <td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><br /><strong><?php print $yyUpdSuc?></strong><br /><br /><?php print $yyNowFrd?><br /><br />
                        <?php print $yyNoAuto?> <a href="/admin/cats.php?pg=<?=$_POST['pg']?>"><strong><?php print $yyClkHer?></strong></a>.<br />
                        <br />
				<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" />
                </td>
			  </tr>
			</table></td>
        </tr>
<?php
}elseif(@$_POST["posted"]=="1"){ ?>
        <tr>
          <td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><br /><font color="#FF0000"><strong><?php print $yyOpFai?></strong></font><br /><br /><?php print $errmsg?><br /><br />
				<a href="javascript:history.go(-1)"><strong><?php print $yyClkBac?></strong></a></td>
			  </tr>
			</table></td>
        </tr>
<?php
}else{
function writeposition($currpos,$maxpos){
	$reqtext="<select name='newpos" . $currpos . "' onChange='chi(" . $currpos . ");'>";
	for($i = 1; $i <= $maxpos; $i++){
		$reqtext .= '<option'; // value='" . $i . "'";
		if($currpos==$i) $reqtext .= " selected";
		$reqtext .= ">" . $i; // . "</option>";
		if($i >= 10 && $i < ($maxpos-15) && abs($currpos-$i) > 40) $i += 9;
	}
	return($reqtext . "</select>");
}
	$allcoupon="";
	$numcoupons=0;
	$sSQL = "SELECT DISTINCT cpaAssignment FROM cpnassign WHERE cpaType=1";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs=mysql_fetch_array($result))
		$allcoupon[$numcoupons++]=$rs;
	mysql_free_result($result);
?>
<script language="JavaScript" type="text/javascript">
<!--
function chi(currindex){
	var i = eval("document.mainform.newpos"+currindex+".selectedIndex");
	document.mainform.newval.value = eval("document.mainform.newpos"+currindex+".options[i].text");
	document.mainform.selectedq.value = currindex;
	document.mainform.sectionTag.value = eval("document.mainform.sectionTag"+currindex+".value");
	document.mainform.act.value = "changepos";
	document.mainform.submit();
}
function mrk(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "modify";
	document.mainform.submit();
}
function srk(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "sort";
	document.mainform.submit();
}
function newrec(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "addnew";
	document.mainform.submit();
}
function dsk(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "discounts";
	document.mainform.submit();
}
function drk(id) {
cmsg = "<?php print $yyConDel?>\n"
if (confirm(cmsg)) {
	document.mainform.id.value = id;
	document.mainform.act.value = "delete";
	document.mainform.submit();
}
}
// -->
</script>
<style type="text/css">
<!--
tr.data:hover {background-color: #FFFFCC; color: #000;}
-->
</style>
        <tr>
		<form name="mainform" method="post" action="/admin/cats.php">
		  <td width="100%">
			<input type="hidden" name="posted" value="1" />
			<input type="hidden" name="act" value="xxxxx" />
			<input type="hidden" name="id" value="xxxxx" />
			<input type="hidden" name="pg" value="<?php print @$_GET["pg"]?>" />
			<input type="hidden" name="selectedq" value="1" />
			<input type="hidden" name="newval" value="1" />
			<input type="hidden" name="sectionTag" value="1" />
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bgcolor="">
			  <tr> 
                <td width="100%" colspan="7" align="center"><strong><?php print $yyCatAdm?></strong><br />&nbsp;</td>
			  </tr>
			  <tr>
			  	<td width="100%" colspan="7" align="center"><input type="button" value="<?php print $yyNewCat?>" onclick="newrec()" /></td>
			  </tr>
<?php
function writepagebar($CurPage, $iNumPages){
	$sLink = "<a href='/admin/cats.php?pg=";
	$startPage = max(1,round(floor((double)$CurPage/10.0)*10));
	$endPage = min($iNumPages,round(floor((double)$CurPage/10.0)*10)+10);
	if($CurPage > 1)
		$sStr = $sLink . "1" . "'><strong><font face='Verdana'>&laquo;</font></strong></a> " . $sLink . ($CurPage-1) . "'>Previous</a> | ";
	else
		$sStr = "<strong><font face='Verdana'>&laquo;</font></strong> Previous | ";
	for($i=$startPage;$i <= $endPage; $i++){
		if($i==$CurPage)
			$sStr .= $i . " | ";
		else{
			$sStr .= $sLink . $i . "'>";
			if($i==$startPage && $i > 1) $sStr .= "...";
			$sStr .= $i;
			if($i==$endPage && $i < $iNumPages) $sStr .= "...";
			$sStr .= "</a> | ";
		}
	}
	if($CurPage < $iNumPages)
		return $sStr . $sLink . ($CurPage+1) . "'>Next</a> " . $sLink . $iNumPages . "'><strong><font face='Verdana'>&raquo;</font></strong></a>";
	else
		return $sStr . " Next <strong><font face='Verdana'>&raquo;</font></strong>";
}
	if(! is_numeric(@$_GET["pg"]))
		$CurPage = 1;
	else
		$CurPage = (int)(@$_GET["pg"]);
	$sSQL = "SELECT COUNT(*) AS bar FROM sections";
	$result = mysql_query($sSQL) or print(mysql_error());
	$numids = mysql_result($result,0,"bar");
	$iNumOfPages = ceil($numids/$maxcatsperpage);
	mysql_free_result($result);
	$sSQL = "SELECT sectionID,sectionWorkingName,sectionDescription,topSection,rootSection,sectionDisabled,sectionTag FROM sections ORDER BY sectionOrder LIMIT " . ($maxcatsperpage*($CurPage-1)) . ", $maxcatsperpage";
	$result = mysql_query($sSQL) or print(mysql_error());
	if($numids > 0){
		$islooping=FALSE;
		$noproducts=FALSE;
		$hascatinprodsection=FALSE;
		$rowcounter=0;
		$bgcolor="";
		if($iNumOfPages > 1) print '<tr><td align="center" colspan="7">' . writepagebar($CurPage, $iNumOfPages) . '<br /><br /></td></tr>';
?>
			  <tr>
				<td width="5%"><strong><?php print $yyOrder?></strong></td>
				<td align="left"><strong><?php print $yyCatPat?></strong></td>				
				<td align="left"><strong><?php print $yyCatNam?></strong></td>
				<td width="210" align="center"><strong>Category Tag</strong></td>
				<td width="5%" align="center"><font size="1"><strong><?php print $yyDiscnt?></strong></font></td>
				<td width="5%" align="center"><font size="1"><strong><?php print $yyModify?></strong></font></td>
				<td width="5%" align="center"><font size="1"><strong><?php print $yyDelete?></strong></font></td>
				<td width="5%" align="center"><font size="1"><strong>Sort</strong></font></td>
			  </tr>
<?php	

while($rs = mysql_fetch_assoc($result)){
			if($bgcolor=="#E7EAEF") $bgcolor="#FFFFFF"; else $bgcolor="#E7EAEF"; ?>
<tr class="data" bgcolor="<?php print $bgcolor?>">
<td><?php 
$current_counter=($maxcatsperpage*($CurPage-1))+$rowcounter+1;
print writeposition(($maxcatsperpage*($CurPage-1))+$rowcounter+1,$numids);?></td>
<td><?php
$tslist = "";
$thetopts = $rs["topSection"];
for($index=0; $index <= 10; $index++){
	if($thetopts==0){
		$tslist = $yyHome . $tslist;
		break;
	}elseif($index==10){
		$tslist = '<strong><font color="#FF0000">' . $yyLoop . '</font></strong>' . $tslist;
		$islooping=TRUE;
	}else{
		$sSQL = "SELECT sectionID,topSection,sectionWorkingName,rootSection FROM sections WHERE sectionID=" . $thetopts;
		$result2 = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result2) > 0){
			$rs2 = mysql_fetch_assoc($result2);
			$errstart = "";
			$errend = "";
			if($rs2["rootSection"]==1){
				$errstart = "<strong><font color='#FF0000'>";
				$errend = "</font></strong>";
				$hascatinprodsection=TRUE;
			}
			$tslist = " &raquo; " . $errstart . $rs2["sectionWorkingName"] . $errend . $tslist;
			$thetopts = $rs2["topSection"];
		}else{
			$tslist = '<strong><font color="#FF0000">' . $yyTopDel . '</font></strong>' . $tslist;
			break;
		}
		mysql_free_result($result2);
	}
}
print '<font size="1">' . $tslist . '</font></td><td>';
if($rs["rootSection"]==1) print "<strong>";
if($rs["sectionDisabled"]==127) print '<strike><font color="#FF0000">';
print $rs["sectionWorkingName"] . " (" . $rs["sectionID"] . ")";
if($rs["sectionDisabled"]==127) print '</font></strike>';
if($rs["rootSection"]==1) print "</strong>";

print '</td><td align="center">';
?>
<div id="tag_update<?=$current_counter?>" style="display:none;" ><input id="tag_box<?=$current_counter?>" type="text" name="sectionTag<?=$current_counter?>" value="<?=$rs["sectionTag"]?>"  /><input type="button" value="go" name="btn_tag" onclick="chi(<?=$current_counter?>)" /> <a href="javascript:void(0);" onclick="Element.hide('tag_update<?=$current_counter?>'),Element.show('tag<?=$current_counter?>');">cancel</a></div>

<div id="tag<?=$current_counter?>" onclick="Element.show('tag_update<?=$current_counter?>'),$('tag_box<?=$current_counter?>').focus(),Element.hide('tag<?=$current_counter?>');"><?=$rs["sectionTag"]?>&nbsp;</div>

<?
print '</td><td><input';
	for($index=0;$index<$numcoupons;$index++){
		if((int)$allcoupon[$index][0]==$rs["sectionID"]){
			print ' style="color: #FF0000" ';
			break;
		}
	}
?> type="button" value="<?php print $yyAssign?>" onclick="dsk('<?php print $rs["sectionID"]?>')"></td>

<td><input type="button" value="<?php print $yyModify?>"  onclick="mrk('<?php print $rs["sectionID"]?>')" /></td>
<td><input type="button" value="<?php print $yyDelete?>" onclick="drk('<?php print $rs["sectionID"]?>')" /></td>
<td><input type="button" value="Sort" onclick="srk('<?php print $rs["sectionID"]?>')" /></td>
</tr><?php	$rowcounter++;
		}
		if($iNumOfPages > 1) print '<tr><td align="center" colspan="6"><br />' . writepagebar($CurPage, $iNumOfPages) . '</td></tr>';
		if($islooping){
?>
			  <tr><td width="100%" colspan="8"><br /><strong><font color='#FF0000'>** </font></strong><?php print $yyCatEx3?></td></tr>
<?php
		}
		if($hascatinprodsection){
?>
			  <tr><td width="100%" colspan="8"><br /><ul><li><?php print $yyCPErr?></li></ul></td></tr>
<?php
		}
?>
			  <tr><td width="100%" colspan="8"><br /><ul><li><?php print $yyCatEx4?></li></ul></td></tr>
<?php
	}else{
?>
			  <tr><td width="100%" colspan="8" align="center"><br /><strong><?php print $yyCatEx5?><br />&nbsp;</td></tr>
<?php
	}
?>
			  <tr> 
                <td width="100%" colspan="8" align="center"><br /><strong><?php print $yyCatNew?></strong>&nbsp;&nbsp;<input type="button" value="<?php print $yyNewCat?>" onclick="newrec()" /><br />&nbsp;</td>
			  </tr>
			  <tr> 
                <td width="100%" colspan="8" align="center"><br />
                          <a href="/admin/index.php"><strong><?php print $yyAdmHom?></strong></a><br />
				<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
            </table></td>
		  </form>
        </tr>
<?php
}
?>
     
	  </table>
