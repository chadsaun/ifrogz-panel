<?php
if($_SESSION["loggedon"] != "virtualstore") exit();

if(isset($_POST['apID']) && $_POST['apID'] != "") {
     $strsql="UPDATE products SET p_iscert = 1 WHERE pID ='".$_POST['apID']."'";
     mysql_query($strsql);
}

if(isset($_POST['rpID']) && $_POST['rpID'] != "" && !empty($_POST['Remove'])) {
     $strsql="UPDATE products SET p_iscert = 0 WHERE pID ='".$_POST['rpID']."'";
     mysql_query($strsql);
}

switch ($_REQUEST['mode']){
     case "1":
          display_purchased_certificates();
          break;
     case "2":
          display_certificate_use_history();
          break;
     case "3":
          modify_existing_certificate();
          break;
     case "4":
         delete_existing_certificate();
          break;
	 case "5":
         batch_certificate($_REQUEST['rpID'],$_POST['group_name'],$_POST['num_certs']);
          break;
     default:
          display_product_relations();
}
?>
<?php
function display_product_relations() {
?>
     <? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>
	 <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
     <hr>
     <table width="500">
     <tr>
     <td colspan="2" align="center"><h3>Add Product to Gift Certificates</h3></td>
     </tr>
     <form action="/admin/certs.php" method="post">
     <tr>
     <td align="right" width="400">
          <select name="apID">
          <?php print_pIDS();?>
          </select>
     </td>
     <td align="left" width="100">
          <input type="submit" name="add" value="Add Product">
     </td>
     </tr>
     </form>
     </table>
     <br>
     <hr>
     <table width="500">
     <tr>
     <td colspan="2" align="center"><h3>Current Gift Certificates Offered</h3></td>
     </tr>
     <?php
     $strsql="SELECT pID, pName FROM products WHERE p_iscert > 0 ORDER BY pName";
     $result=mysql_query($strsql);
     while($rs=mysql_fetch_assoc($result)) {
               echo "<form action=\"/admin/certs.php\" method=\"post\"><tr>";
               echo "<td align=\"right\" width=\"300\">".$rs['pName']."</td>";
               echo "<td width=\"200\"><input type=\"hidden\" name=\"rpID\" value=\"".$rs['pID']."\">";
			   echo "<input type=\"hidden\" name=\"mode\" value=\"5\"><input type=\"submit\" name=\"batch\" value=\" Batch \">";
               echo "<input type=\"submit\" name=\"Remove\" value=\" Remove \"></td></tr></form>";
          }

     ?>
     </table>
     <br>
     <hr>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
<?php }

function print_pIDS() {
     $strsql="SELECT pID, pName FROM products WHERE p_iscert = 0 ORDER BY pName";
     $result=mysql_query($strsql);
     while($rs=mysql_fetch_assoc($result)) {
          echo "<option value=\"".$rs['pID']."\">".$rs['pName']."</option>";
     }
}

function display_purchased_certificates() {
     ?>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
     <hr>
     <table cellpadding="4" cellspacing="0" width="90%">
     <tr>
       <td colspan="8" style="border:0 solid #222;border-bottom-width:1px;"><h3>Gifts Certificates Purchased</h3></td>
     </tr>
     <tr>
	 <td colspan="6" style="border:0 solid #222;border-bottom-width:1px;">
	 <form action="/admin/certs.php?mode=1" method="post">
		<strong>Description:</strong> 
		<select name="group_names_list">
		  <option value="" <?php if (!(strcmp("", group_names_list))) {echo "selected=\"selected\"";} ?>>&lt; All &gt;</option>
		  <? 
		$SQL_certs="SELECT DISTINCT cert_group AS cert_group FROM certificates WHERE cert_group<>''";
		$result_certs=mysql_query($SQL_certs);
		while($row=mysql_fetch_assoc($result_certs)){?>
		  <option value="<?=$row['cert_group']?>"  <?php if (!(strcmp($row['cert_group'], $_POST['group_names_list']))) {echo "selected=\"selected\"";} ?>><?=$row['cert_group']?></option>
		  <? } ?>
		</select>
		<strong>Cert:</strong>
		<select name="sbcode">
         	<option value="">&lt;All&gt;</option>
		 <?php print_cert();?>
        </select>
	    <strong>Batch:</strong> 
	    <input name="sbbatch" type="text" />
		<input name="group_btn" type="submit" value="go" />
	</form>	</td>
     <td colspan="2" align="right" style="border:0 solid #222;border-bottom-width:1px; font-weight:bold;">	   <?php
     $strsql="SELECT *
          FROM certificates WHERE cert_id<>''";
	 if(!empty($_REQUEST['sbcode'])) $strsql.= " AND cert_id=".$_REQUEST['sbcode'];		  
	 if(!empty($_POST['group_names_list'])) $strsql.= " AND cert_group='".$_POST['group_names_list']."'";
	 if(!empty($_POST['sbbatch'])) $strsql.= " AND batch_number='".sbbatch."'";	 
	 $strsql.=" ORDER BY batch_number,sequential,cert_exp_dt DESC, cert_amt DESC";
     //echo $strsql;
	 $result=mysql_query($strsql);
	 echo mysql_num_rows($result)." gift certs";
	 ?></td>
     </tr>
     <tr>
          <td><strong>Date of Expiration</strong></td>
          <td><strong>Certificate Code</strong></td>
          <td><strong>Remaining Balance</strong></td>
          <td><strong>Purchaser's E-mail</strong></td>
          <td><strong>Description</strong></td>
          <td><strong>Batch-Sequential</strong></td>
          <td><strong>&nbsp;</strong></td>
		  <td><strong>&nbsp;</strong></td>
       </tr>
     
	 <?
	 
     while($rs=mysql_fetch_assoc($result)) {
          $expdt = date("M j, Y", $rs['cert_exp_dt']);
          echo "<form action=\"/admin/certs.php?mode=3\" method=\"POST\">";
		  
          echo "<tr><td>".$expdt."</td>";
		  
          echo "<td>".$rs['cert_code']."</td>";
		  
          echo "<td>\$".number_format($rs['cert_amt'], 2)."</td>";
		  
          echo "<td>".$rs['cert_email']."</td>";
		  echo "<td>".$rs['cert_group']."</td>";
		  if(!empty($rs['batch_number'])){
		  echo "<td>".$rs['batch_number'].'-'.$rs['sequential']."</td>";
		  } else {
		  echo "<td>&nbsp;</td>";
		  }
          if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {
		  echo "<td><input type=\"hidden\" name=\"cert_id\" value=\"".$rs['cert_id']."\">";
		  echo "<input type=\"submit\" name=\"Modify\" value=\"Modify\"></td></form>";
		  echo "<form action=\"/admin/certs.php?mode=4\" method=\"POST\">";
          echo "<td><input type=\"hidden\" name=\"cert_id\" value=\"".$rs['cert_id']."\">";
		  echo "<input type=\"submit\" name=\"Delete\" value=\"Delete\"></td></tr></form>\n";
			}
     }
     ?>
     </table>
     <hr>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
<?php }

function display_certificate_use_history() {
     ?>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
     <hr>
          <table width="90%" align="center">
     <tr>
     <td colspan="3" align="center"><h3>Select Option to Search by</h3></td>
     </tr>
     <form action="/admin/certs.php?mode=2" method="post">
     <tr>
       <td align="right"><strong>Batch:</strong></td>
       <td align="left"><input name="sbbatch" type="text" />
         <input type="submit" name="searchb" value="Search" /></td>
       <td align="left">&nbsp;</td>
     </tr>
	 <tr>
	   <td align="right"><strong>Description:</strong></td>
       <td align="left"><select name="sbgroup">
         <?php  print_batch();?>
       </select>
         <input type="submit" name="searchg" value="Search" /></td>
       <td align="left">&nbsp;</td>
     </tr>
     <tr>
       <td align="right" width="314"><strong>Cert Code:</strong> </td>
     <td align="left" width="324"><select name="sbcode">
         <?php print_cert();?>
       </select>
       <input type="submit" name="searchc" value="Search" /></td>
     <td align="left" width="197">&nbsp;</td>
     </tr>
     </form>
     <form action="/admin/certs.php?mode=2" method="post">
     <tr>
       <td align="right" width="314"><strong>email:</strong></td>
     <td align="left" width="324"><select name="sbemail">
          <?php print_email();?>
       </select>
       <input type="submit" name="searche" value="Search" /></td>
     <td align="left" width="197">&nbsp;</td>
     </tr>
     </form>
     </table>
     <br>
     <hr>
     <table width="90%" align="center">
     <tr>
     <td colspan="7" align="center" style="border:0 solid #222;border-bottom-width:1px;"><h3>Gifts Certificates Use History</h3></td>
     </tr>
     <tr>
          <td align="center"><strong>Order ID</strong></td>
          <td align="center"><strong>Date of Order</strong></td>
          <td align="center"><strong>Certificate Code</strong></td>
          <td align="right"><strong>Amt Used</strong></td>
          <td align="center"><strong>Purchaser's E-mail</strong></td>
		  <td align="center"><strong>Description</strong></td>
		  <td align="center"><strong>Batch-Sequence</strong></td>
     </tr>
     <?php
     if(isset($_REQUEST['sbcode']) && $_REQUEST['sbcode'] != "" && $_POST['searchc']!="") {
          $strsql="SELECT ordID, ordDate, cert_code, ord_cert_amt, cert_email, cert_group , batch_number, sequential
                FROM orders LEFT JOIN certificates ON ord_cert_id = cert_id
                WHERE cert_id =".$_REQUEST['sbcode']."
                ORDER BY ordDate";
          //echo  $strsql;
		  $result=mysql_query($strsql);
          if(mysql_num_rows($result)==0) echo "<tr><td colspan=\"5\" align=\"center\">There is no gift certificate history for this order!</td>";
		  while($rs=mysql_fetch_assoc($result)) {
               echo "<tr><td align=\"center\">".$rs['ordID']."</td>";
               echo "<td align=\"center\">".$rs['ordDate']."</td>";
               echo "<td align=\"center\">".$rs['cert_code']."</td>";
               echo "<td align=\"right\">\$".number_format($rs['ord_cert_amt'], 2)."</td>";
               echo "<td align=\"center\">".$rs['cert_email']."</td>\n";
			   echo "<td align=\"center\">".$rs['cert_group']."</td></tr>\n";
          }
     }
     if(isset($_POST['sbemail']) && $_POST['sbemail'] != "" && $_POST['searche']!="") {
          $strsql="SELECT ordID, ordDate, cert_code, ord_cert_amt, cert_email, cert_group, batch_number, sequential
                FROM orders LEFT JOIN certificates ON ord_cert_id = cert_id
                WHERE cert_email ='".$_POST['sbemail']."'
                ORDER BY cert_code, ordDate";
          $result=mysql_query($strsql);
          while($rs=mysql_fetch_assoc($result)) {
               echo "<tr><td align=\"center\">".$rs['ordID']."</td>";
               echo "<td align=\"center\">".$rs['ordDate']."</td>";
               echo "<td align=\"center\">".$rs['cert_code']."</td>";
               echo "<td align=\"right\">\$".number_format($rs['ord_cert_amt'], 2)."</td>";
               echo "<td align=\"center\">".$rs['cert_email']."</td>\n";
			   echo "<td align=\"center\">".$rs['cert_group']."</td></tr>\n";
          }
     }
	 if(isset($_POST['sbgroup']) && $_POST['sbgroup'] != "" && $_POST['searchg']!="") {
          $strsql="SELECT ordID, ordDate, cert_code, ord_cert_amt, cert_email, cert_group, batch_number, sequential
                FROM orders LEFT JOIN certificates ON ord_cert_id = cert_id
                WHERE cert_group ='".$_POST['sbgroup']."'
                ORDER BY cert_code, ordDate";
          $result=mysql_query($strsql);
          while($rs=mysql_fetch_assoc($result)) {
               echo "<tr><td align=\"center\">".$rs['ordID']."</td>";
               echo "<td align=\"center\">".$rs['ordDate']."</td>";
               echo "<td align=\"center\">".$rs['cert_code']."</td>";
               echo "<td align=\"right\">\$".number_format($rs['ord_cert_amt'], 2)."</td>";
               echo "<td align=\"center\">".$rs['cert_email']."</td>\n";
			   echo "<td align=\"center\">".$rs['cert_group']."</td></tr>\n";
          }
     }
	 if($_POST['searchb']!="") {
          $strsql="SELECT ordID, ordDate, cert_code, ord_cert_amt, cert_email, cert_group, batch_number, sequential
                FROM orders LEFT JOIN certificates ON ord_cert_id = cert_id
                WHERE batch_number ='".$_POST['sbbatch']."'
                ORDER BY cert_code, ordDate";
          $result=mysql_query($strsql);
          while($rs=mysql_fetch_assoc($result)) {
               echo "<tr><td align=\"center\">".$rs['ordID']."</td>";
               echo "<td align=\"center\">".$rs['ordDate']."</td>";
               echo "<td align=\"center\">".$rs['cert_code']."</td>";
               echo "<td align=\"right\">\$".number_format($rs['ord_cert_amt'], 2)."</td>";
               echo "<td align=\"center\">".$rs['cert_email']."</td>\n";
			   echo "<td align=\"center\">".$rs['cert_group']."</td></tr>\n";
          }
     }
     ?>
     </table>
     <hr>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
<?php }
function print_cert() {
     $strsql="SELECT DISTINCT cert_id, cert_code
           FROM orders, certificates
           WHERE ord_cert_id = cert_id AND ord_cert_id > 0
           ORDER BY cert_code";
		   
     $result=mysql_query($strsql);
     while($rs=mysql_fetch_assoc($result)) {
          echo "<option value=\"".$rs['cert_id']."\">".$rs['cert_code']."</option>";
     }
}
function print_email() {
     $strsql="SELECT DISTINCT cert_email
           FROM orders, certificates
           WHERE ord_cert_id = cert_id AND ord_cert_id > 0
           ORDER BY cert_email";
     $result=mysql_query($strsql);
     while($rs=mysql_fetch_assoc($result)) {
          echo "<option value=\"".$rs['cert_email']."\">".$rs['cert_email']."</option>";
     }
}
function print_batch() {
     $strsql="SELECT DISTINCT cert_group
           FROM orders, certificates
           WHERE ord_cert_id = cert_id AND ord_cert_id > 0
           ORDER BY cert_group";
     $result=mysql_query($strsql);
     while($rs=mysql_fetch_assoc($result)) {
          echo "<option value=\"".$rs['cert_group']."\">".$rs['cert_group']."</option>";
     }
}
function delete_existing_certificate() {
     $message = "";
     $certid = 0;
     if(isset($_POST['cert_id']) && $_POST['cert_id'] != "") $certid = $_POST['cert_id'];

     $sqlStr = mysql_query("DELETE FROM certificates WHERE cert_id=".$certid);
     if ($sqlStr)
     {
           $message .= "<strong><font color='green'>CERTIFICATE DELETED SUCCESSFULLY.</font></strong><br><br>";
     }
     else
     {
           $message .= "<strong><font color='red'>ERROR!!</font>CERTIFICATE NOT DELETED.</strong><br><br>";
     }

?>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
     <hr>
     <br>
     <?php echo $message;?>

     <hr>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
<?php
}



 function modify_existing_certificate()
{
     $message = "";
     if(isset($_POST['update']) && $_POST['update'] == "1")
     {
          $certid = 0;
          $certamt = 0;
          $certexpdt = "";
          if(isset($_POST['cert_id']) && $_POST['cert_id'] != "")
              $certid = $_POST['cert_id'];
          if(isset($_POST['cert_exp_dt']) && $_POST['cert_exp_dt'] != "")
          {
               if (($certexpdt = strtotime(trim($_POST['cert_exp_dt']))) === -1)
               {
                    $message .= "<strong>DATE NOT UPDATED: Please enter a valid expiration date.</strong><br><br>";
               }else{
                    mysql_query("UPDATE certificates SET cert_exp_dt=".$certexpdt.", cert_email='".trim($_POST['cert_email'])."' WHERE cert_id=".$certid);
                    $message .= "<strong>DATE UPDATED SUCCESSFULLY.</strong><br><br>";
               }
          }
          if(isset($_POST['cert_amt']) && $_POST['cert_amt'] >= 0) {
               if (!is_numeric($certamt=trim($_POST['cert_amt']))) {
                    $message .= "<strong>AMOUNT NOT UPDATED: Please enter a valid amount.</strong><br><br>";
               }else{
                    mysql_query("UPDATE certificates SET cert_amt=".number_format($certamt, 2, '.', '').", cert_email='".trim($_POST['cert_email'])."' WHERE cert_id=".$certid);
                    $message .= "<strong>AMOUNT UPDATED SUCCESSFULLY.</strong><br><br>";
               }
          }
		  if(isset($_POST['cert_order_id']) && $_POST['cert_order_id'] !="") {
               if (!is_numeric($certorder=trim($_POST['cert_order_id']))) {
                    $message .= "<strong>ORDER NUMBER NOT UPDATED: Please enter a valid order number.</strong><br><br>";
               }else{
                    mysql_query("UPDATE certificates SET cert_email='".trim($_POST['cert_email'])."', cert_order_id='".trim($_POST['cert_order_id'])."' WHERE cert_id=".$certid);
                    $message .= "<strong>ORDER NUMBER UPDATED SUCCESSFULLY.</strong><br><br>";
               }
          }
     }

?>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
     <hr>
     <br>
     <?php echo $message;?>
     <table>
     <tr>
     <td colspan="2" align="center" style="border:0 solid #222;border-bottom-width:1px;"><h3>Modify Gift Certificate</h3></td>
     </tr>
<?php
     $certid = 0;
     if(isset($_POST['cert_id']) && $_POST['cert_id'] != "") {
          $certid = $_POST['cert_id'];
          $sqlstr = "SELECT cert_code, cert_amt, cert_email, cert_exp_dt, cert_order_id
                  FROM certificates
                  WHERE cert_id =".$certid;
          $result = mysql_query($sqlstr);
          if ($rs=mysql_fetch_assoc($result)) {
               echo "<form action=\"/admin/certs.php?mode=3\" method=\"POST\">";
               echo "<tr><td align=\"right\"><strong>Certificate Code: </strong></td>";
               echo "<td align=\"left\">".$rs['cert_code']."</td></tr>";
               echo "<tr><td align=\"right\"><strong>Order Purchased on: </strong></td>";
               echo "<td align=\"left\"><input type=\"text\" name=\"cert_order_id\" value=\"".$rs['cert_order_id']."\"></td></tr>";
               echo "<tr><td align=\"right\"><strong>Purchaser's E-mail: </strong></td>";
               echo "<td align=\"left\"><input type=\"text\" name=\"cert_email\" value=\"".$rs['cert_email']."\" size=\"30\"></td></tr>";
               echo "<tr><td align=\"right\"><strong>Certificate Amount: </strong></td>";
               echo "<td align=\"left\"><input type=\"text\" name=\"cert_amt\" value=\"".number_format($rs['cert_amt'], 2)."\" size=\"11\"></td></tr>";
               echo "<tr><td align=\"right\"><strong>Certificate Expires (MM/DD/CCYY): </strong></td>";
               echo "<td align=\"left\"><input type=\"text\" name=\"cert_exp_dt\" value=\"".date("m/d/Y",$rs['cert_exp_dt'])."\" size=\"11\"></td></tr>";
               echo "<tr><td colspan=\"2\" align=\"center\"><font size=\"1\">Note: To deactive a gift certificate, change <br>";
               echo "the \"Certificate Expires\" date to ".date("m/d/Y",time()).".</font></td></tr>";
               echo "<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" name=\"submit\" value=\"Update Certificate\"></td></tr>";
               echo "<input type=\"hidden\" name=\"update\" value=\"1\">";
               echo "<input type=\"hidden\" name=\"cert_id\" value=\"".$certid."\"></form>";
          }
     }
?>
     </table>
     <br>
     <hr>
<? if(isPermitted('admin')||isPermitted('it')||isPermitted('management')||isPermitted('accounting')) {?>
	 <a href="/admin/certs.php?mode=0">Certificate Management</a>&nbsp;|&nbsp;
     <? } ?>     <a href="/admin/certs.php?mode=1">View Purchased Certificates</a>&nbsp;|&nbsp;
     <a href="/admin/certs.php?mode=2">View Certificate Use History</a>
<?php
}
?>
<?
function batch_certificate($pID,$group_name,$num_certs){ ?>		
	<div style="margin:6px;">
	<h2 >Create Certificate Batch</h2>
	<form action="/admin/certs.php?mode=5&rpID=<?=$pID?>" method="post" name="batchfrm">
	 <strong> Group Name:</strong>
	  <input name="group_name" type="text"  /><br />
	  <strong>How Many:&nbsp;&nbsp;&nbsp;&nbsp;</strong> 
	  <input name="num_certs" type="text" size="6"  />
	  <input name="submit" type="submit" value="go" />	
	</form>
	<br />	
<? 
	if(!empty($num_certs) && !empty($pID) && !empty($group_name)) echo create_certificates($pID,$group_name,$num_certs);?>
	</div>
	<div style="clear:both;">&nbsp;</div>	
<? } ?>
	
<?
function create_certificates($pID,$group_name,$num_to_create=1){
	$strsql = "SELECT pPrice FROM products WHERE pID='".$pID."'";
	$result = mysql_query($strsql);
	while ($rs=mysql_fetch_assoc($result)) {
		$expdate = time()+(720*24*3600);
		for ($x = 0; $x < $num_to_create;$x++) {
			do {
				$sqlrows = 1;
				$certcode = 'G'.RandomString(11);
				$strsql = "SELECT * FROM certificates WHERE cert_code='".$certcode."'";
				$sqlrows = mysql_num_rows(mysql_query($strsql));
			} while ($sqlrows > 0);
			if($x<9) $count='&nbsp; '.($x+1);
			else $count=$x+1;
			$certarray[] = $count."-(\$".number_format($rs['pPrice']).") ".$certcode;
			$strsql = "INSERT INTO certificates (cert_code, cert_prod_id, cert_amt, cert_order_id, cert_email, cert_exp_dt, cert_group, date_created)
				  VALUES ('".$certcode."','".$pID."',".$rs['pPrice'].",'','',".$expdate.",'".$group_name."','".date('Y-m-d H:i:s')."')";
			mysql_query($strsql);
		}
	}
	return implode("<br />",$certarray);
}

?>