<?php
include('init.php');
include_once(DOCROOT.'includes/ofc/php-ofc-library/open_flash_chart_object.php');
?>
	<h2>iFrogz Daily Charts</h2>
	
	<table width="600" border="0" cellspacing="0" cellpadding="10" align="center">
		<tr>
			<td align="center"><span style="font-weight: bold; font-size: 24px; padding-bottom: 5px;">Daily Orders</span><br /><em>Last 31 days</em></td>
			<td align="center"><span style="font-weight: bold; font-size: 24px;">Daily Revenue</span><br /><em>Last 31 days</em></td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<?php
					open_flash_chart_object( 420, 200, '/admin/dailyorders.php', false , '/includes/ofc/' );
				?>
			</td>
			<td align="center" valign="top">
				<?php
					open_flash_chart_object( 420, 200, '/admin/dailyrevenue.php', false , '/includes/ofc/' );
				?>
			</td>
		</tr>
		<tr>
			<td align="center"><em>Last week compared to week before</em></td>
			<td align="center"><em>Last week compared to week before</em></td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<?php
					open_flash_chart_object( 420, 200, '/admin/dailycmporders.php', false , '/includes/ofc/' );
				?>
			</td>
			<td align="center" valign="top">
				<?php
					open_flash_chart_object( 420, 200, '/admin/dailycmprevenue.php', false , '/includes/ofc/' );
				?>
			</td>
		</tr>
		<tr>
			<td align="center"><span style="font-weight: bold; font-size: 24px; padding-bottom: 5px;">Daily Average Order Value</span><br /><em>Last 31 days</em></td>
			<td align="center"><span style="font-weight: bold; font-size: 24px; padding-bottom: 5px;">Daily Order Sizes</span><br /><em>Last 31 days</em></td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<?php
					open_flash_chart_object( 420, 200, '/admin/dailyaov.php', false , '/includes/ofc/' );
				?>
			</td>
			<td align="center" valign="top">
				<?php
					open_flash_chart_object( 420, 400, '/admin/dailyordersizes.php', false , '/includes/ofc/' );
				?>
			</td>
		</tr>
	</table>
