<?php

function getDeliveryDate($ordID) {
	if(empty($ordID)) {
		$ordID = 12345;
	}
	
	// DECLARE VARIABLES
	$shipping_method = '';
	$hasCustomScreen = false;
	$customScreenDays = 5;
	$hasItemOutOfStock = false;
	$extendShipping = 0;
	$shipping_days = 0;
	
	// GET SHIPPING METHOD
	$sql = "SELECT * FROM orders WHERE ordID = " . $ordID;
	$res = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	$shipping_method = $row['ordShipType'];
	
	// CHECK FOR CUSTOM SCREENZ
	$sql = "SELECT coOptGroup FROM cart c, cartoptions co
			WHERE c.cartID = co.coCartID
			AND c.cartOrderID = " . $ordID;
	$res = mysql_query($sql) or print(mysql_error());
	while($row = mysql_fetch_assoc($res)) {
		if($row['coOptGroup'] == 'Custom Screenz') {
			$hasCustomScreen = true;
		}
	}
	
	// CHECK FOR ITEMS THAT ARE OUT OF STOCK
	$sql = "SELECT co.coExtendShipping FROM cart c, cartoptions co
			WHERE c.cartID = co.coCartID
			AND c.cartOrderID = " . $ordID;
	$res = mysql_query($sql) or print(mysql_error());
	while($row = mysql_fetch_assoc($res)) {
		if($row['coExtendShipping'] > 0) {
			$hasItemOutOfStock = true;
			if($row['coExtendShipping']>19999999){
				$thisyear=substr($row["coExtendShipping"],0,4);
				$thismonth=substr($row["coExtendShipping"],4,2);
				$thisday=substr($row["coExtendShipping"],6,2);
				$time=mktime(0,0,0,$thismonth,$thisday,$thisyear);
				$now=time();
				$extendShipping=round(($time-$now)/60/60/24);
			}else $extendShipping = $row['coExtendShipping'];
		}
	}
	
	//if(strtotime("20 December 2006") < mktime()) { // GO BACK TO REGULAR SHIPPING TIMES
		if($shipping_method=='Standard') {
			$shipping_days = 8; // 7-10 business days
			$aReturn['days_to_arrive'] = '5-8';
		}elseif($shipping_method=='Priority Mail') {
			$shipping_days = 4; // 3-4 business days
			$aReturn['days_to_arrive'] = '3-4';
		}elseif($shipping_method=='FedEx Express') {
			$shipping_days = 3; // 3-5 business days
			$aReturn['days_to_arrive'] = '2-3';
		}elseif($shipping_method=='FedEx Overnight') {
			$shipping_days = 2; // 1 business days
			$aReturn['days_to_arrive'] = '1-2';
		}elseif($shipping_method=='International') {
			$shipping_days = 7; // 7-10 business days
			$aReturn['days_to_arrive'] = '7';
		}elseif($shipping_method=='Two-Day Shipping') {
			$shipping_days = 2; // 2 business days
			$aReturn['days_to_arrive'] = '2';
		}else{
			$shipping_days = 7; // 7-10 business days
			$aReturn['days_to_arrive'] = '7';
		}
	//}else{ // MODIFIED SHIPPING TIMES FOR CHRISTMAS
		//if($shipping_method=='Standard') {
			//$shipping_days = 15; // 10-15 business days
			//$aReturn['days_to_arrive'] = '10-15';
		//}elseif($shipping_method=='FedEx Express') {
			//$shipping_days = 5; // 3-5 business days
			//$aReturn['days_to_arrive'] = '3-5';
		//}elseif($shipping_method=='International') {
			//$shipping_days = 20; // 10-20 business days
			//$aReturn['days_to_arrive'] = '10-20';
		//}else{
			//$shipping_days = 15; // 10-15 business days
			//$aReturn['days_to_arrive'] = '10-15';
		//}
	//}
	
	if($hasCustomScreen && $hasItemOutOfStock) {
		if($customScreenDays > $extendShipping) {
			$deliveryDate = bus_days($shipping_days+$customScreenDays);
			$aReturn['messageToUse'] = 'custom screen';
			$aReturn['extended'] = $customScreenDays;
		}else{
			$deliveryDate = bus_days($shipping_days+$extendShipping);
			$aReturn['messageToUse'] = 'stock';
			$aReturn['extended'] = $extendShipping;
		}
	}elseif($hasCustomScreen) {
		$deliveryDate = bus_days($shipping_days+$customScreenDays);
		$aReturn['messageToUse'] = 'custom screen';
		$aReturn['extended'] = $customScreenDays;
	}elseif($hasItemOutOfStock) {
		$deliveryDate = bus_days($shipping_days+$extendShipping);
		$aReturn['messageToUse'] = 'stock';
		$aReturn['extended'] = $extendShipping;
	}else{
		$deliveryDate = bus_days($shipping_days);
		$aReturn['messageToUse'] = 'standard';
		$aReturn['extended'] = 0;
	}
	
	$aReturn['deliveryDate'] = $deliveryDate;
	$aReturn['method'] = $shipping_method;
	
	return $aReturn;
}

?>