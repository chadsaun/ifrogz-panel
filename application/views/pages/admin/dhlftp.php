<script type="text/javascript">
<!--
function checkDelete() {
	var decision = confirm("Are you sure you want to delete this file?");
	if(decision==true) {
		return true;
	}else{
		return false;
	}
}

function checkReceive() {
	var decision = confirm("Are you sure you want to receive this file?\nExisting information will NOT be overwritten.");
	if(decision==true) {
		return true;
	}else{
		return false;
	}
}

-->
</script>
<div>
	<div style="margin: 0 10px; padding: 3px; font-weight: bold; font-size: 18px; text-align: center; background-color: #030133; color: #E7EAEF; border: 1px solid black; border-bottom: none">DHL File Administration</div>
	<div style="margin: 0 10px 10px 10px; padding: 3px 5px; text-align: center; border: 1px solid black; border-top: none; background-color: #E7EAEF; color: #333333">Manage the CSV files we send them, and the CSV files they send back to us.</div>

	<?php
		if(!empty($_SESSION['dhlMsg'])) {
			echo "<div style='margin-bottom: 10px; color: green; text-align: center; font-weight: bold'>".$_SESSION['dhlMsg']."</div>";
			$_SESSION['dhlMsg'] = '';
		}
	?>
	<div style="margin-right: 10px; float: right; width: 45%">
		<div style="margin-bottom: 10px 10px 0 0; text-align: center; font-size: 12px; font-weight: bold; background-color: #030133; color: #E7EAEF; border: 1px solid #030133; border-bottom: none">
			<p style="margin: 0; padding: 3px 0; font-size: 12px">Sent DHL Files</p>
		</div>
		
		<div style="padding: 5px; border: 1px solid #030133; background-color: #E7EAEF">
	<?php
		if(!$connID = ftpLogin("ftp.smartmail.com","ftpuserrband",'5g$Xpr!4')) {
			echo "Could not login to the DHL's FTP server.";
		}else{
			$files = ftpListFiles($connID,'','ifrogz');
			if($files===false) {
				echo "<div style='text-align: center; font-weight: bold'>Problem getting the list of files.</div>";
			}elseif (empty($files)) {
				echo "<div style='text-align: center; font-weight: bold'>No ifrogz files found.</div>";
			}else{
				foreach($files as $filename) {
					echo '<form method="post" action="/admin/dhlftpprocess.php"><div style="line-height: 24px">'.$filename.'<input type="submit" name="delete" value="Delete" style="margin-left: 5px" onclick="return checkDelete();" /><input type="hidden" name="filename" value="'.$filename.'" /></div></form>';
				}
			}
		}
	?>
		</div>
		
		<div style="margin: 10px 0 0 0; text-align: center; font-size: 12px; font-weight: bold; background-color: #030133; color: #E7EAEF; border: 1px solid #030133; border-bottom: none">
			<p style="margin: 0; padding: 3px 0; font-size: 12px">Received DHL Files</p>
		</div>
		
		<div style="padding: 5px; border: 1px solid #030133; background-color: #E7EAEF">
	<?php
		if(!$connID = ftpLogin("ftp.smartmail.com","ftpuserrband",'5g$Xpr!4')) {
			echo "Could not login to the DHL's FTP server.";
		}else{
			$files = ftpListFiles($connID,'From_SMail','');
			if($files===false) {
				echo "<div style='text-align: center; font-weight: bold'>Problem getting the list of files.</div>";
			}elseif(empty($files)) {
				echo "<div style='text-align: center; font-weight: bold'>DHL hasn't sent us anything yet.</div>";
			}else{
				$sql = "SELECT * FROM dhl_files WHERE status = 'received'";
				$res = mysql_query($sql) or print(mysql_error());
				$aRec = array();
				while($row=mysql_fetch_assoc($res)) {
					array_push($aRec,$row['filename']);
				}
				
				sort($files); // sort just in-case files are out of order on FTP site
				array_reverse($files); // Sort list in Descending order
				for($i=0; $i<count($files) && $i<10; $i++) {
					$thisfile = $files[(count($files)-1-$i)]; // So that it shows most recent to oldest
					$strDate = "20".substr($thisfile,9,2)."-".substr($thisfile,11,2)."-".substr($thisfile,13,2)." ".substr($thisfile,15,2).":00:00";
					$date = strtotime($strDate);
					
					$received = '';
					if(in_array($thisfile,$aRec)) {
						$received = 'Received';
					}
					
					echo '<form method="post" action="/admin/dhlftpprocess.php"><div style="line-height: 24px"><a href="/admin/dhlftpprocess.php?view=yes&amp;path='.urlencode("From_SMail/").'&amp;file='.urlencode($thisfile).'">'.$thisfile.'</a><input type="submit" name="receive" value="Receive" style="margin-left: 5px" onclick="return checkReceive();" /><input type="hidden" name="filename" value="'.$thisfile.'" /><span style="color: #006600; font-weight: bold"> '.$received.'</span></div></form>';
				}
			}
		}
	?>
		</div>
		
	</div>
	<div style="margin-left: 10px; float: left; width: 45%">
		<div style="margin-bottom: 10px 10px 0 0; text-align: center; font-size: 12px; font-weight: bold; background-color: #030133; color: #E7EAEF; border: 1px solid #030133; border-bottom: none">
			<p style="margin: 0; padding: 3px 0; font-size: 12px">Our DHL Files</p>
		</div>
		
		<div style="padding: 5px; border: 1px solid #030133; background-color: #E7EAEF">
	<?php
		$dir = "../batches/dhl";
		$dh  = opendir($dir);
		$index=0;
		while (false !== ($filename = readdir($dh))) {
			if ($filename != "." && $filename != ".." && $filename != "error_log" && $filename != "tmp") {
				ereg("^ifrogz[0-9]*_([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})_in\.dat$",$filename,$regs);
				$year = $regs[1];
				$month = $regs[2];
				$day = $regs[3];
				$hour = $regs[4];
				$timestamp = mktime($hour,0,0,$month,$day,"20".$year);
				
				$listofiles[$index]['filename'] = $filename;
				$listofiles[$index]['timestamp'] = $timestamp;
				$index++;
			}
		}
		usort($listofiles,'compare');
		if(!empty($listofiles)) {
			$sql = "SELECT * FROM dhl_files WHERE status = 'uploaded'";
			$res = mysql_query($sql) or print(mysql_error());
			$aUp = array();
			while($row=mysql_fetch_assoc($res)) {
				array_push($aUp,$row['filename']);
			}
			
			for($i=0; $i<count($listofiles)&&$i<20; $i++) {
				$thisfile = $listofiles[$i]['filename'];
				
				$uploaded = '';
				if(in_array($thisfile,$aUp)) {
					$uploaded = 'Uploaded';
				}
				
				echo '<form method="post" action="/admin/dhlftpprocess.php"><div style="line-height: 24px"><a href="/admin/download.php?path='.urlencode('../batches/dhl/').'&amp;file='.urlencode($thisfile).'">'.$thisfile.'</a><input type="submit" name="upload" value="Upload" style="margin-left: 5px" /><input type="hidden" name="filename" value="'.$thisfile.'" /><span style="color: #006600; font-weight: bold"> '.$uploaded.'</span></div></form>';
			}
		}else {?>
			<div style="margin: 5px auto">No batches found.</div>
	<?	}	?>
		</div>
	</div>
	<div style="clear:both; height: 20px"></div>
		
</div>

<?php
function ftpLogin($host,$user,$pass) {
	$ftp = ftp_connect($host);
	if ($ftp) {
		if (ftp_login($ftp,$user,$pass)) {
			return $ftp;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function ftpListFiles($connID,$dir="",$filter="") {
	if(!ftp_chdir($connID,$dir)) {
		ftp_quit($connID);
		return false;
	}
	
	$contents = ftp_rawlist($connID,"");
	//showarray($contents);
	if(!$contents) {
		ftp_quit($connID);
		return false;
	}
	
	$files = array();
	foreach($contents as $value) {
		$item = split("[ ]+",$value);
		$item_type = substr($item[0],0,1);
		$filename = $item[8];
		if($item_type=='-') { // IF ITS A FILE NOT A DIRECTORY
			if(!empty($filter)) {
				if(strstr($filename,$filter)) {
					$files[] = $filename;
				}
			}else{
				$files[] = $filename;
			}
		}
	}
	
	ftp_quit($connID);
	//showarray($files);
	return $files;
}

function compare($x, $y) {
	if($x['timestamp'] == $y['timestamp']) {
		return 0;
	}elseif($x['timestamp'] > $y['timestamp']) {
		return false;
	}else{
		return true;
	}
}

?>