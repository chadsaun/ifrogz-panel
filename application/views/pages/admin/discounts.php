<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$sSQL = "";
$alreadygotadmin = getadminsettings();
if (@$_POST["posted"]=="1") {
	if (@$_POST["act"]=="delete") {
		$sSQL = "DELETE FROM cpnassign WHERE cpaCpnID=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error());
		$sSQL = "DELETE FROM coupons WHERE cpnID=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error());
		print '<meta http-equiv="refresh" content="3; url=/admin/discounts.php">';
	}
	else if (@$_POST["act"]=="deleteGrp") {
		$sql = "DELETE FROM grpCpn WHERE grpCpnID = ". @$_POST["id"];
		mysql_query($sql) or print(mysql_error());
		$sql = "SELECT * FROM coupons WHERE cpnGrpCpnID = '".@$_POST["id"]."'";
		$res = mysql_query($sql) or print(mysql_error());
		$coupons = '';
		if(mysql_num_rows($res) > 0) {
			for($i=0; $row = mysql_fetch_assoc($res); $i++) {
				$coupons .= $row['cpnID'];
				if($i+1!=mysql_num_rows($res)) {
					$coupons .= ",";
				}
			}
		}
		$sql = "DELETE FROM coupons WHERE cpnGrpCpnID = '".@$_POST["id"]."'";
		mysql_query($sql) or print(mysql_error());
		$sql = "DELETE FROM cpnassign WHERE cpaCpnID IN($coupons)";
		mysql_query($sql) or print(mysql_error());
		print '<meta http-equiv="refresh" content="3; url=/admin/discounts.php">';
	}
	else if (@$_POST["act"]=="domodify") {
		// Creates lock
		$lSQL = 'LOCK TABLES coupons WRITE, fsadiscount WRITE;';
        mysql_query($lSQL) or print(mysql_error().'<br />'.$lSQL);
        // Updates coupon
		$sSQL = "UPDATE coupons SET cpnName='" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName"])) . "'";
			for($index=2; $index <= $adminlanguages+1; $index++){
				if(($adminlangsettings & 1024)==1024) $sSQL .= ",cpnName" . $index . "='" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName" . $index])) . "'";
			}
			if(trim(@$_POST["cpnWorkingName"]) != "")
				$sSQL .= ",cpnWorkingName='" . mysql_real_escape_string(unstripslashes(@$_POST["cpnWorkingName"])) . "'";
			else
				$sSQL .= ",cpnWorkingName='" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName"])) . "'";
			if(@$_POST["cpnIsCoupon"]=="0")
				$sSQL .= ",cpnNumber='',";
			else
				$sSQL .= ",cpnNumber='" . mysql_real_escape_string(unstripslashes(@$_POST["cpnNumber"])) . "',";
			$sSQL .= "cpnType=" . @$_POST["cpnType"] . ",";
			/*$numdays=0;
			if(is_numeric(@$_POST["cpnEndDate"])) $numdays = (int)@$_POST["cpnEndDate"];
			if($numdays > 0)
				$sSQL .= "cpnEndDate='" . date("Y-m-d",(time() + ($numdays*60*60*24))) . "',";
			else
				$sSQL .= "cpnEndDate='3000-01-01',";*/
			if (!empty($_POST["cpnEndDate"])) {
				$sSQL .= "cpnEndDate='" . $_POST["cpnEndDate"] . "',";
			} else {
				$sSQL .= "cpnEndDate='3000-01-01',";
			}
			if (!empty($_POST["cpnBeginDate"])) {
				$sSQL .= "cpnBeginDate='" . $_POST["cpnBeginDate"] . "',";
			} else {
				$sSQL .= "cpnBeginDate='0000-00-00 00:00:00',";
			}
			if(is_numeric(@$_POST["cpnDiscount"]) && @$_POST["cpnType"] != "0")
				$sSQL .= "cpnDiscount=" . @$_POST["cpnDiscount"] . ",";
			else
				$sSQL .= "cpnDiscount=0,";
			if(is_numeric(@$_POST["cpnThreshold"]))
				$sSQL .= "cpnThreshold=" . @$_POST["cpnThreshold"] . ",";
			else
				$sSQL .= "cpnThreshold=0,";
			if(is_numeric(@$_POST["cpnThresholdMax"]))
				$sSQL .= "cpnThresholdMax=" . @$_POST["cpnThresholdMax"] . ",";
			else
				$sSQL .= "cpnThresholdMax=0,";
			if(is_numeric(@$_POST["cpnThresholdRepeat"]))
				$sSQL .= "cpnThresholdRepeat=" . @$_POST["cpnThresholdRepeat"] . ",";
			else
				$sSQL .= "cpnThresholdRepeat=0,";
			if(is_numeric(@$_POST["cpnQuantity"]))
				$sSQL .= "cpnQuantity=" . @$_POST["cpnQuantity"] . ",";
			else
				$sSQL .= "cpnQuantity=0,";
			if(is_numeric(@$_POST["cpnQuantityMax"]))
				$sSQL .= "cpnQuantityMax=" . @$_POST["cpnQuantityMax"] . ",";
			else
				$sSQL .= "cpnQuantityMax=0,";
			if(is_numeric(@$_POST["cpnQuantityRepeat"]))
				$sSQL .= "cpnQuantityRepeat=" . @$_POST["cpnQuantityRepeat"] . ",";
			else
				$sSQL .= "cpnQuantityRepeat=0,";
			if(trim(@$_POST["cpnNumAvail"]) != "" && is_numeric(@$_POST["cpnNumAvail"]))
				$sSQL .= "cpnNumAvail=" . @$_POST["cpnNumAvail"] . ",";
			else
				$sSQL .= "cpnNumAvail=30000000,";
			if(@$_POST["cpnType"]=="0")
				$sSQL .= "cpnCntry=" . @$_POST["cpnCntry"] . ",";
			else
				$sSQL .= "cpnCntry=0,";
			$sSQL .= "cpnIsCoupon=" . @$_POST["cpnIsCoupon"] . ",";
			if(@$_POST["cpnType"]=="0")
				$sSQL .= "cpnSitewide=1,";
			else
				$sSQL .= "cpnSitewide=" . @$_POST["cpnSitewide"]. ",";
			//wholesale only coupon added by Blake 6-6-06
			if(@$_POST["cpnIsWholesale"]=="")
				$sSQL .= "cpnIsWholesale = 0,";
			else
				$sSQL .= "cpnIsWholesale = " . @$_POST["cpnIsWholesale"] . ",";
			//
			if ($_POST["cpnStackable"] == "") {
				$sSQL .= "cpnStackable = 0";
			} else {
				$sSQL .= "cpnStackable = " . $_POST["cpnStackable"];
			}
			$sSQL .= " WHERE cpnID=" . @$_POST["id"];
		mysql_query($sSQL) or print(mysql_error().$sSQL);
		$cpnID = @$_POST["id"];

		if ($_POST['cpnType'] == 0) {
    		$sSQL = "SELECT * FROM fsadiscount WHERE cpnID = {$cpnID};";
    		
    		$result = mysql_query($sSQL) or print(mysql_error().$sSQL);

    		$buffer = array();
    		while ($record = mysql_fetch_assoc($result)) {
    			$buffer[] = $record;
    		}
    		
    		mysql_free_result($result);

			if (isset($_POST['pz'])) {
            	foreach ($_POST['pz'] as $pz) {
	                $data = explode(',', $pz); // 0 = pzID, 1 = methodID
	                $doInsert = TRUE;
	                foreach ($buffer as &$record) {
	                    if (($data[0] == $record['pzID']) && ($data[1] == $record['methodID'])) { 
							$record = NULL;
	                        $doInsert = FALSE;
	                        break;
	                    }
	                }
					unset($record);
	                if ($doInsert) {
	                    $iSQL = "INSERT INTO fsadiscount (cpnID, pzID, methodID) VALUES ({$cpnID}, {$data[0]}, {$data[1]});";
	                    mysql_query($iSQL) or print(mysql_error().'<br />'.$iSQL);
	                }
	            }
			}

            foreach ($buffer as $record) {
                if ($record != NULL) {
					$dSQL = "DELETE FROM fsadiscount WHERE ID = {$record['ID']};";
	                mysql_query($dSQL) or print(mysql_error().'<br />'.$dSQL);
				}
            }
        }

        $uSQL = 'UNLOCK TABLES;';
        mysql_query($uSQL) or print(mysql_error().'<br />'.$uSQL);

		print '<meta http-equiv="refresh" content="3; url=/admin/discounts.php">';
	}
	else if (@$_POST["act"]=="doaddnew") {
		if (!empty($_POST['cpnIsGrp'])) {
			/*
			// Creates lock
			$lSQL = 'LOCK TABLES coupons WRITE, fsadiscount WRITE;';
            mysql_query($lSQL) or print(mysql_error().'<br />'.$lSQL);
            // Inserts new coupon
			*/
			$qty = $_POST['grpQty'];
			
			if ($_POST['linkGrp']!="none") {
				$sql_orig_grp = "SELECT * FROM coupons WHERE cpnGrpCpnID = ".$_POST['linkGrp'];
				$res_orig_grp = mysql_query($sql_orig_grp) or print(mysql_error());
				$qty = mysql_num_rows($res_orig_grp);
			}
			
			$sql = "INSERT INTO grpCpn (grpCpnName, grpCpnQty";
			if ($_POST['linkGrp']!="none") {
				$sql .= ",grpCpnLinkID)";
			}
			else{
				$sql .= ") ";
			}
			$sql .= "VALUES ('".$_POST['grpName']."',".$qty;
			if($_POST['linkGrp']!="none") {
				$sql .= ",".$_POST['linkGrp'].")";
			}
			else{
				$sql .= ")";
			}
			$res = mysql_query($sql) or die(mysql_error());
			$lastID = mysql_insert_id();
						
			for($i=0; $i<$qty; $i++) {
				if($_POST['linkGrp']!="none") {
					$row_orig_grp = mysql_fetch_assoc($res_orig_grp);
					$promoCode = $row_orig_grp['cpnNumber'];
					$cpnEnd = $row_orig_grp['cpnEndDate'];
					$cpnBegin = $row_orig_grp['cpnBeginDate'];	
				}else{
					$promoCode = getPromoCode();
					$cpnEnd = @$_POST["cpnEndDate"];
					$cpnBegin = @$_POST["cpnBeginDate"];
				}
				$sSQL = "INSERT INTO coupons (cpnName";
					for($index=2; $index <= $adminlanguages+1; $index++){
						if(($adminlangsettings & 1024)==1024) $sSQL .= ",cpnName" . $index;
					}
					$sSQL .= ",cpnWorkingName,cpnNumber,cpnType,cpnEndDate,cpnDiscount,cpnThreshold,cpnThresholdMax,cpnThresholdRepeat,cpnQuantity,cpnQuantityMax,cpnQuantityRepeat,cpnNumAvail,cpnCntry,cpnIsCoupon,cpnSitewide,cpnIsWholesale,cpnGrpCpnID,cpnBeginDate, cpnStackable) VALUES (";
					$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName"])) . "',";
					for($index=2; $index <= $adminlanguages+1; $index++){
						if(($adminlangsettings & 1024)==1024) $sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName" . $index])) . "',";
					}
					if(trim(@$_POST["cpnWorkingName"]) != "")
						$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnWorkingName"])) . "',";
					else
						$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName"])) . "',";
					if(@$_POST["cpnIsCoupon"]=="0")
						$sSQL .= "'',";
					else
						$sSQL .= "'" . mysql_real_escape_string($promoCode) . "',";
					$sSQL .= @$_POST["cpnType"] . ",";
					/*$numdays=0;
					if(is_numeric($cpnEnd) && $_POST['linkGrp']=="none") $numdays = (int)$cpnEnd;
					if($numdays > 0 && $_POST['linkGrp']=="none")
						$sSQL .= "'" . date("Y-m-d",(time() + ($numdays*60*60*24))) . "',";
					elseif($_POST['linkGrp']!="none")
						$sSQL .= "'".$cpnEnd."',";
					else
						$sSQL .= "'3000-01-01',";*/
					if (!empty($cpnEnd)) {
						$sSQL .= "'" . $cpnEnd . "',";
					} else {
						$sSQL .= "'3000-01-01 00:00:00',";
					}
					if(is_numeric(@$_POST["cpnDiscount"]) && @$_POST["cpnType"] != "0")
						$sSQL .= @$_POST["cpnDiscount"] . ",";
					else
						$sSQL .= "0,";
					if(is_numeric(@$_POST["cpnThreshold"]))
						$sSQL .= @$_POST["cpnThreshold"] . ",";
					else
						$sSQL .= "0,";
					if(is_numeric(@$_POST["cpnThresholdMax"]))
						$sSQL .= @$_POST["cpnThresholdMax"] . ",";
					else
						$sSQL .= "0,";
					if(is_numeric(@$_POST["cpnThresholdRepeat"]))
						$sSQL .= @$_POST["cpnThresholdRepeat"] . ",";
					else
						$sSQL .= "0,";
					if(is_numeric(@$_POST["cpnQuantity"]))
						$sSQL .= @$_POST["cpnQuantity"] . ",";
					else
						$sSQL .= "0,";
					if(is_numeric(@$_POST["cpnQuantityMax"]))
						$sSQL .= @$_POST["cpnQuantityMax"] . ",";
					else
						$sSQL .= "0,";
					if(is_numeric(@$_POST["cpnQuantityRepeat"]))
						$sSQL .= @$_POST["cpnQuantityRepeat"] . ",";
					else
						$sSQL .= "0,";
					if(trim(@$_POST["cpnNumAvail"]) != "" && is_numeric(@$_POST["cpnNumAvail"]))
						$sSQL .= @$_POST["cpnNumAvail"] . ",";
					else
						$sSQL .= "30000000,";
					if(@$_POST["cpnType"]=="0")
						$sSQL .= @$_POST["cpnCntry"] . ",";
					else
						$sSQL .= "0,";
					$sSQL .= @$_POST["cpnIsCoupon"] . ",";
					if(@$_POST["cpnType"]=="0")
						$sSQL .= "1,";
					else
						$sSQL .= @$_POST["cpnSitewide"] . ",";
					//wholesale only coupon added by Blake 6-6-06
					if(@$_POST["cpnIsWholesale"]=="")
						$sSQL .= "0,";
					else
						$sSQL .= @$_POST["cpnIsWholesale"] . ",";
					//
					$sSQL .= "'" .$lastID . "',";
					if (!empty($cpnBegin)) {	
						$sSQL .= "'" . $cpnBegin . "',";
					} else {
						$sSQL .= "'0000-00-00 00:00:00',";
					}
					if ($_POST["cpnStackable"] == "") {
						$sSQL .= "0)";
					} else {
						$sSQL .= $_POST["cpnStackable"] . ")";
					}
				mysql_query($sSQL) or print(mysql_error());
				/*
				$cpnID = mysql_insert_id();
    			if (($_POST['cpnType'] == 0) && isset($_POST['pz']) && (count($_POST['pz']) > 0)) {
                    foreach ($_POST['pz'] as $pz) {
                        $data = explode(',', $pz); // 0 = pzID, 1 = methodID
                        $iSQL = "INSERT INTO fsadiscount (cpnID, pzID, methodID) VALUES ({$cpnID}, {$data[0]}, {$data[1]});";
                        mysql_query($iSQL) or print(mysql_error().'<br />'.$iSQL);
                    }
                }
                $uSQL = 'UNLOCK TABLES;';
                mysql_query($uSQL) or print(mysql_error().'<br />'.$uSQL);
                */
			}
		}
		else { // doaddnew: non-group
			// Creates lock
			$lSQL = 'LOCK TABLES coupons WRITE, fsadiscount WRITE;';
            mysql_query($lSQL) or print(mysql_error().'<br />'.$lSQL);
            // Inserts new coupon
			$sSQL = "INSERT INTO coupons (cpnName";
			for($index=2; $index <= $adminlanguages+1; $index++){
				if(($adminlangsettings & 1024)==1024) $sSQL .= ",cpnName" . $index;
			}
			$sSQL .= ",cpnWorkingName,cpnNumber,cpnType,cpnEndDate,cpnDiscount,cpnThreshold,cpnThresholdMax,cpnThresholdRepeat,cpnQuantity,cpnQuantityMax,cpnQuantityRepeat,cpnNumAvail,cpnCntry,cpnIsCoupon,cpnSitewide,cpnIsWholesale,cpnBeginDate, cpnStackable) VALUES (";
			$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName"])) . "',";
			for($index=2; $index <= $adminlanguages+1; $index++){
				if(($adminlangsettings & 1024)==1024) $sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName" . $index])) . "',";
			}
			if(trim(@$_POST["cpnWorkingName"]) != "")
				$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnWorkingName"])) . "',";
			else
				$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnName"])) . "',";
			if(@$_POST["cpnIsCoupon"]=="0")
				$sSQL .= "'',";
			else
				$sSQL .= "'" . mysql_real_escape_string(unstripslashes(@$_POST["cpnNumber"])) . "',";
			$sSQL .= @$_POST["cpnType"] . ",";
			/*$numdays=0;
			if(is_numeric(@$_POST["cpnEndDate"])) $numdays = (int)@$_POST["cpnEndDate"];
			if($numdays > 0)
				$sSQL .= "'" . date("Y-m-d",(time() + ($numdays*60*60*24))) . "',";
			else
				$sSQL .= "'3000-01-01',";*/
			if (!empty($_POST["cpnEndDate"])) {
				$sSQL .= "'" . $_POST["cpnEndDate"] . "',";
			} else {
				$sSQL .= "'3000-01-01 00:00:00',";
			}
			if(is_numeric(@$_POST["cpnDiscount"]) && @$_POST["cpnType"] != "0")
				$sSQL .= @$_POST["cpnDiscount"] . ",";
			else
				$sSQL .= "0,";
			if(is_numeric(@$_POST["cpnThreshold"]))
				$sSQL .= @$_POST["cpnThreshold"] . ",";
			else
				$sSQL .= "0,";
			if(is_numeric(@$_POST["cpnThresholdMax"]))
				$sSQL .= @$_POST["cpnThresholdMax"] . ",";
			else
				$sSQL .= "0,";
			if(is_numeric(@$_POST["cpnThresholdRepeat"]))
				$sSQL .= @$_POST["cpnThresholdRepeat"] . ",";
			else
				$sSQL .= "0,";
			if(is_numeric(@$_POST["cpnQuantity"]))
				$sSQL .= @$_POST["cpnQuantity"] . ",";
			else
				$sSQL .= "0,";
			if(is_numeric(@$_POST["cpnQuantityMax"]))
				$sSQL .= @$_POST["cpnQuantityMax"] . ",";
			else
				$sSQL .= "0,";
			if(is_numeric(@$_POST["cpnQuantityRepeat"]))
				$sSQL .= @$_POST["cpnQuantityRepeat"] . ",";
			else
				$sSQL .= "0,";
			if(trim(@$_POST["cpnNumAvail"]) != "" && is_numeric(@$_POST["cpnNumAvail"]))
				$sSQL .= @$_POST["cpnNumAvail"] . ",";
			else
				$sSQL .= "30000000,";
			if(@$_POST["cpnType"]=="0")
				$sSQL .= @$_POST["cpnCntry"] . ",";
			else
				$sSQL .= "0,";
			$sSQL .= @$_POST["cpnIsCoupon"] . ",";
			if(@$_POST["cpnType"]=="0")
				$sSQL .= "1,";
			else
				$sSQL .= @$_POST["cpnSitewide"]  . ",";
			//wholesale only coupon added by Blake 6-6-06
			if(@$_POST["cpnIsWholesale"]=="")
				$sSQL .= "0,";
			else
				$sSQL .= @$_POST["cpnIsWholesale"]. ",";
			if (!empty($_POST["cpnBeginDate"])) {
				$sSQL .= "'" . $_POST["cpnBeginDate"] . "',";
			}
			else {
				$sSQL .= "'0000-00-00 00:00:00',";
			}
			if ($_POST["cpnStackable"] == "") {
				$sSQL .= "0)";
			}
			else {
				$sSQL .= $_POST["cpnStackable"] . ")";
			}
			mysql_query($sSQL) or print(mysql_error().'<br />'.$sSQL);
			$cpnID = mysql_insert_id();
			if (($_POST['cpnType'] == 0) && isset($_POST['pz']) && (count($_POST['pz']) > 0)) {
                foreach ($_POST['pz'] as $pz) {
                    $data = explode(',', $pz); // 0 = pzID, 1 = methodID
                    $iSQL = "INSERT INTO fsadiscount (cpnID, pzID, methodID) VALUES ({$cpnID}, {$data[0]}, {$data[1]});";
                    mysql_query($iSQL) or print(mysql_error().'<br />'.$iSQL);
                }
            }
            $uSQL = 'UNLOCK TABLES;';
            mysql_query($uSQL) or print(mysql_error().'<br />'.$uSQL);
		}
		print '<meta http-equiv="refresh" content="3; url=/admin/discounts.php">';
	}
}
?>
<link rel="stylesheet" type="text/css" media="all" href="http://assets.ifrogz.com/lib/packages/jscalendar/1.0/calendar-blue2.css" title="win2k-cold-1" />

<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jscalendar/1.0/calendar.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jscalendar/1.0/lang/calendar-en.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jscalendar/1.0/calendar-setup.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery/1.4.2/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
jQuery.noConflict();
var savebg, savebc, savecol;
function formvalidator(theForm)
{
<?php
if($_POST['act']=="addnew") {
?>
  if($F('cpnIsGrp')==1 && $F('grpQty') == '' && $F('linkGrp')=='none') {
    alert("<?php print $yyPlsEntr?> \"<?php print $yyNumCpn?>\".");
    theForm.grpQty.focus();
    return (false);
  }
  if($F('cpnIsGrp')==1 && $F('grpName') == '') {
    alert("<?php print $yyPlsEntr?> \"<?php print $yyGrpName?>\".");
    theForm.grpName.focus();
    return (false);
  }
<?php
}
?>
  if(theForm.cpnName.value == "")
  {
    alert("<?php print $yyPlsEntr?> \"<?php print $yyDisTxt?>\".");
    theForm.cpnName.focus();
    return (false);
  }
  if(theForm.cpnName.value.length > 255)
  {
    alert("<?php print $yyMax255?> \"<?php print $yyDisTxt?>\".");
    theForm.cpnName.focus();
    return (false);
  }
  if(theForm.cpnType.selectedIndex!=0){
	if(theForm.cpnDiscount.value == "")
	{
	  alert("<?php print $yyPlsEntr?> \"<?php print $yyDscAmt?>\".");
	  theForm.cpnDiscount.focus();
	  return (false);
	}
	if(theForm.cpnType.selectedIndex==2){
	  if(theForm.cpnDiscount.value < 0 || theForm.cpnDiscount.value > 100){
		alert("<?php print $yyNum100?> \"<?php print $yyDscAmt?>\".");
		theForm.cpnDiscount.focus();
		return (false);
	  }
	}
  }
  if(theForm.cpnIsCoupon.selectedIndex==1){
<?php
if($_POST['act']=="addnew") {
?>
  	if($F('cpnIsGrp')!=1) {
<?php
}
?>
		if(theForm.cpnNumber.value == "")
		{
		  alert("<?php print $yyPlsEntr?> \"<?php print $yyCpnCod?>\".");
		  theForm.cpnNumber.focus();
		  return (false);
		}
		var checkOK = "0123456789abcdefghijklmnopqrstuvwxyz-_";
		var checkStr = theForm.cpnNumber.value.toLowerCase();
		var allValid = true;
		for (i = 0;  i < checkStr.length;  i++)
		{
			ch = checkStr.charAt(i);
			for (j = 0;  j < checkOK.length;  j++)
				if (ch == checkOK.charAt(j))
					break;
			if (j == checkOK.length){
				allValid = false;
					break;
			}
		}
		if (!allValid)
		{
			alert("<?php print $yyAlpha2?> \"<?php print $yyCpnCod?>\".");
			theForm.cpnNumber.focus();
			return (false);
		}
<?php
if($_POST['act']=="addnew") {
?>
	}
<?php
}
?>
  }
  var checkOK = "0123456789";
  var checkStr = theForm.cpnNumAvail.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyNum?> \"<?php print $yyNumAvl?>\".");
	theForm.cpnNumAvail.focus();
	return (false);
  }
  if(theForm.cpnNumAvail.value != "" && theForm.cpnNumAvail.value > 1000000)
  {
    alert("<?php print $yyNumMil?> \"<?php print $yyNumAvl?>\"<?php print $yyOrBlank?>");
    theForm.cpnNumAvail.focus();
    return (false);
  }
  /*
  var checkOK = "0123456789";
  var checkStr = theForm.cpnEndDate.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyNum?> \"<?php print $yyDaysAv?>\".");
	theForm.cpnEndDate.focus();
	return (false);
  }*/
  var checkOK = "0123456789.";
  var checkStr = theForm.cpnThreshold.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyDec?> \"<?php print $yyMinPur?>\".");
	theForm.cpnThreshold.focus();
	return (false);
  }
  var checkOK = "0123456789.";
  var checkStr = theForm.cpnThresholdRepeat.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyDec?> \"<?php print $yyRepEvy?>\".");
	theForm.cpnThresholdRepeat.focus();
	return (false);
  }
  var checkOK = "0123456789.";
  var checkStr = theForm.cpnThresholdMax.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyDec?> \"<?php print $yyMaxPur?>\".");
	theForm.cpnThresholdMax.focus();
	return (false);
  }
  var checkOK = "0123456789";
  var checkStr = theForm.cpnQuantity.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyNum?> \"<?php print $yyMinQua?>\".");
	theForm.cpnQuantity.focus();
	return (false);
  }
  var checkOK = "0123456789";
  var checkStr = theForm.cpnQuantityRepeat.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyNum?> \"<?php print $yyRepEvy?>\".");
	theForm.cpnQuantityRepeat.focus();
	return (false);
  }
  var checkOK = "0123456789";
  var checkStr = theForm.cpnQuantityMax.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyNum?> \"<?php print $yyMaxQua?>\".");
	theForm.cpnQuantityMax.focus();
	return (false);
  }
  var checkOK = "0123456789.";
  var checkStr = theForm.cpnDiscount.value;
  var allValid = true;
  for (i = 0;  i < checkStr.length;  i++)
  {
	ch = checkStr.charAt(i);
	for (j = 0;  j < checkOK.length;  j++)
		if (ch == checkOK.charAt(j))
			break;
	if (j == checkOK.length){
		allValid = false;
			break;
	}
  }
  if (!allValid)
  {
	alert("<?php print $yyOnlyDec?> \"<?php print $yyDscAmt?>\".");
	theForm.cpnDiscount.focus();
	return (false);
  }
  document.mainform.cpnNumber.disabled=false;
  document.mainform.cpnDiscount.disabled=false;
  document.mainform.cpnCntry.disabled=false;
  document.mainform.cpnSitewide.disabled=false;
  document.mainform.cpnThresholdRepeat.disabled=false;
  document.mainform.cpnQuantityRepeat.disabled=false;
  return (true);
}
function couponcodeactive(forceactive){
	if(document.mainform.cpnIsCoupon.selectedIndex==0){
		document.mainform.cpnNumber.style.backgroundColor="#DDDDDD";
		document.mainform.cpnNumber.style.borderColor="#aa3300";
		document.mainform.cpnNumber.style.color="#aa3300";
		document.mainform.cpnNumber.disabled=true;
	}
	else if(document.mainform.cpnIsCoupon.selectedIndex==1<?=($_POST['act']=='addnew')?' && $F("cpnIsGrp")==0':''?>){
		document.mainform.cpnNumber.style.backgroundColor=savebg;
		document.mainform.cpnNumber.style.borderColor=savebc;
		document.mainform.cpnNumber.style.color=savecol;
		document.mainform.cpnNumber.disabled=false;
	}
}
function changecouponeffect(forceactive){
	if(document.mainform.cpnType.selectedIndex==0){
		document.mainform.cpnDiscount.style.backgroundColor="#DDDDDD";
		document.mainform.cpnDiscount.style.borderColor="#aa3300";
		document.mainform.cpnDiscount.style.color="#aa3300";
		document.mainform.cpnDiscount.disabled=true;

		document.mainform.cpnCntry.style.backgroundColor=savebg;
		document.mainform.cpnCntry.style.borderColor=savebc;
		document.mainform.cpnCntry.style.color=savecol;
		document.mainform.cpnCntry.disabled=false;

		document.mainform.cpnSitewide.style.backgroundColor="#DDDDDD";
		document.mainform.cpnSitewide.style.borderColor="#aa3300";
		document.mainform.cpnSitewide.style.color="#aa3300";
		document.mainform.cpnSitewide.disabled=true;
		jQuery('input.ShippingMethod').each(function() {
    		jQuery('input.ShippingMethod').removeAttr('disabled');
    	});
	}else{
		document.mainform.cpnDiscount.style.backgroundColor=savebg;
		document.mainform.cpnDiscount.style.borderColor=savebc;
		document.mainform.cpnDiscount.style.color=savecol;
		document.mainform.cpnDiscount.disabled=false;

		document.mainform.cpnCntry.style.backgroundColor="#DDDDDD";
		document.mainform.cpnCntry.style.borderColor="#aa3300";
		document.mainform.cpnCntry.style.color="#aa3300";
		document.mainform.cpnCntry.disabled=true;

		document.mainform.cpnSitewide.style.backgroundColor=savebg;
		document.mainform.cpnSitewide.style.borderColor=savebc;
		document.mainform.cpnSitewide.style.color=savecol;
		document.mainform.cpnSitewide.disabled=false;
		jQuery('input.ShippingMethod').each(function() {
    		jQuery('input.ShippingMethod').attr('disabled', 'disabled');
    	});
	}
	if(document.mainform.cpnType.selectedIndex==1){
		document.mainform.cpnThresholdRepeat.style.backgroundColor=savebg;
		document.mainform.cpnThresholdRepeat.style.borderColor=savebc;
		document.mainform.cpnThresholdRepeat.style.color=savecol;
		document.mainform.cpnThresholdRepeat.disabled=false;

		document.mainform.cpnQuantityRepeat.style.backgroundColor=savebg;
		document.mainform.cpnQuantityRepeat.style.borderColor=savebc;
		document.mainform.cpnQuantityRepeat.style.color=savecol;
		document.mainform.cpnQuantityRepeat.disabled=false;
	}else{
		document.mainform.cpnThresholdRepeat.style.backgroundColor="#DDDDDD";
		document.mainform.cpnThresholdRepeat.style.borderColor="#aa3300";
		document.mainform.cpnThresholdRepeat.style.color="#aa3300";
		document.mainform.cpnThresholdRepeat.disabled=true;

		document.mainform.cpnQuantityRepeat.style.backgroundColor="#DDDDDD";
		document.mainform.cpnQuantityRepeat.style.borderColor="#aa3300";
		document.mainform.cpnQuantityRepeat.style.color="#aa3300";
		document.mainform.cpnQuantityRepeat.disabled=true;
	}
}

function cpnGrp(oSel) {
	var val = $F('cpnIsGrp');
	var aTR = document.getElementsByClassName('trGroup');
	if(val == 1) {
		for(i=0; i<aTR.length; i++) {
			aTR[i].style.display = '';
		}
		$('cpnNumber').disabled=true;
		$('cpnNumber').style.backgroundColor="#DDDDDD";
		$('cpnNumber').style.borderColor="#aa3300";
	}else{
		for(i=0; i<aTR.length; i++) {
			aTR[i].style.display = 'none';
		}
		$('grpQty').value = '';
		$('linkGrp').selectedIndex = 0;
		$('cpnNumber').disabled=false;
		$('cpnNumber').style.backgroundColor="";
		$('cpnNumber').style.borderColor="";
	}
}

function qty() {
	if($F('linkGrp')!='none') {
		$('grpQty').disabled=true;
		$('grpQty').style.backgroundColor="#DDDDDD";
		$('grpQty').style.borderColor="#aa3300";
		
		$('cpnEndDate').disabled=true;
		$('cpnEndDate').style.backgroundColor="#DDDDDD";
		$('cpnEndDate').style.borderColor="#aa3300";
	}else{
		$('grpQty').disabled=false;
		$('grpQty').style.backgroundColor="";
		$('grpQty').style.borderColor="";
		
		$('cpnEndDate').disabled=false;
		$('cpnEndDate').style.backgroundColor="";
		$('cpnEndDate').style.borderColor="";
	}
}
//-->
</script>
      <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
<?php if(@$_POST["posted"]=="1" && (@$_POST["act"]=="modify" || @$_POST["act"]=="addnew")){
		if(@$_POST["act"]=="modify"){
			$sSQL = "SELECT cpnName,cpnName2,cpnName3,cpnWorkingName,cpnNumber,cpnType,cpnEndDate,cpnDiscount,cpnThreshold,cpnThresholdMax,cpnThresholdRepeat,cpnQuantity,cpnQuantityMax,cpnQuantityRepeat,cpnNumAvail,cpnCntry,cpnIsCoupon,cpnSitewide,cpnGrpCpnID,cpnIsWholesale,cpnBeginDate, cpnStackable FROM coupons WHERE cpnID=" . @$_POST["id"];
			$result = mysql_query($sSQL) or print(mysql_error());
			$rs = mysql_fetch_array($result);
			$cpnName = $rs["cpnName"];
			for($index=2; $index <= $adminlanguages+1; $index++) {
				$cpnNames[$index] = $rs["cpnName" . $index];
			}
			$cpnWorkingName = $rs["cpnWorkingName"];
			$cpnNumber = $rs["cpnNumber"];
			$cpnType = $rs["cpnType"];
			$cpnEndDate = $rs["cpnEndDate"];
			$cpnDiscount = $rs["cpnDiscount"];
			$cpnThreshold = $rs["cpnThreshold"];
			$cpnThresholdMax = $rs["cpnThresholdMax"];
			$cpnThresholdRepeat = $rs["cpnThresholdRepeat"];
			$cpnQuantity = $rs["cpnQuantity"];
			$cpnQuantityMax = $rs["cpnQuantityMax"];
			$cpnQuantityRepeat = $rs["cpnQuantityRepeat"];
			$cpnNumAvail = $rs["cpnNumAvail"];
			$cpnCntry = $rs["cpnCntry"];
			$cpnIsCoupon = $rs["cpnIsCoupon"];
			$cpnSitewide = $rs["cpnSitewide"];
			$cpnIsWholesale = $rs["cpnIsWholesale"];
			$cpnIsGrp = $rs["cpnGrpCpnID"];
			$cpnBeginDate = $rs["cpnBeginDate"];
			$cpnStackable = $rs["cpnStackable"];
			mysql_free_result($result);
		}else{
			$cpnName = "";
			for($index=2; $index <= $adminlanguages+1; $index++)
				$cpnNames[$index] = "";
			$cpnWorkingName = "";
			$cpnNumber = "";
			$cpnType = 0;
			$cpnEndDate = '3000-01-01 00:00:00';
			$cpnDiscount = "";
			$cpnThreshold = 0;
			$cpnThresholdMax = 0;
			$cpnThresholdRepeat = 0;
			$cpnQuantity = 0;
			$cpnQuantityMax = 0;
			$cpnQuantityRepeat = 0;
			$cpnNumAvail = 30000000;
			$cpnCntry = 0;
			$cpnIsCoupon = 0;
			$cpnSitewide = 0;
			$cpnIsGrp = 0;
			$cpnBeginDate = '0000-00-00 00:00:00';
			$cpnStackable = 1;
		}
?>
        <tr>
		<form name="mainform" method="post" action="/admin/discounts.php" onsubmit="return formvalidator(this)">
		  <td width="100%">
			<input type="hidden" name="posted" value="1" />
		<?php	if(@$_POST["act"]=="modify"){ ?>
			<input type="hidden" name="act" value="domodify" />
			<input type="hidden" name="id" value="<?php print @$_POST["id"]?>" />
		<?php	}else{ ?>
			<input type="hidden" name="act" value="doaddnew" />
		<?php	} ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><strong><?php print $yyDscNew?></strong><br />&nbsp;</td>
			  </tr>
		<?php
			if(@$_POST["act"]=="addnew"){
		?>
			  <tr>
			  	<td width="40%" align="right"><strong>Create Coupon Group:</strong></td>
				<td width="60%"><select id="cpnIsGrp" name="cpnIsGrp" onchange="cpnGrp(this);">
					<option value="0"<?=((int)$cpnIsGrp!=1)?' selected="selected"':''?>>No</option>
					<option value="1"<?=((int)$cpnIsGrp==1)?' selected="selected"':''?>>Yes</option>
					</select>				</td>
			  </tr>
			  <tr class="trGroup" style="display: none">
			  	<td width="40%" align="right"><strong>Group Name:</strong></td>
				<td width="60%"><input type="text" id="grpName" name="grpName" value="" /></td>
			  </tr>
			  <tr class="trGroup" style="display: none">
			  	<td width="40%" align="right"><strong>Number of Coupons:</strong></td>
				<td width="60%"><input type="text" id="grpQty" name="grpQty" value="" /></td>
			  </tr>
			  <tr class="trGroup" style="display: none">
			  	<td width="40%" align="right"><strong>Link to Group:</strong></td>
				<td width="60%">
					<select id="linkGrp" name="linkGrp" onchange="qty()">
			<?php
				$qry = "SELECT * FROM grpCpn WHERE grpCpnLinkID IS NULL OR grpCpnLinkID = ''";
				$res = mysql_query($qry) or print(mysql_error());
				echo '<option value="none">None</option>'."\n";
				while($row = mysql_fetch_assoc($res)) {
					echo '<option value="'.$row['grpCpnID'].'">'.$row['grpCpnName'].'</option>'."\n";
				}
			?>
					</select>
				</td>
			  </tr>
			  <tr>
			  	<td colspan="2">&nbsp;</td>
			  </tr>
		<?php
			}
		?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyCpnDsc?>:</strong></td>
				<td width="60%"><select name="cpnIsCoupon" size="1" onchange="couponcodeactive(false);">
					<option value="0"><?php print $yyDisco?></option>
					<option value="1" <?php if((int)$cpnIsCoupon==1) print "selected" ?>><?php print $yyCoupon?></option>
					</select></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDscEff?>:</strong></td>
				<td width="60%"><select name="cpnType" size="1" onchange="changecouponeffect(false);">
					<option value="0"><?php print $yyFrSShp?></option>
					<option value="1" <?php if((int)$cpnType==1) print "selected" ?>><?php print $yyFlatDs?></option>
					<option value="2" <?php if((int)$cpnType==2) print "selected" ?>><?php print $yyPerDis?></option>
					</select></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDisTxt?>:</strong></td>
				<td width="60%"><input type="text" name="cpnName" size="30" value="<?php print str_replace('"',"&quot;",$cpnName)?>" /></td>
			  </tr>
<?php		for($index=2; $index <= $adminlanguages+1; $index++){
				if(($adminlangsettings & 1024)==1024){ ?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDisTxt . " " . $index?>:</strong></td>
				<td width="60%"><input type="text" name="cpnName<?php print $index?>" size="30" value="<?php print str_replace('"',"&quot;",$cpnNames[$index])?>" /></td>
			  </tr>
<?php			}
			} ?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyWrkNam?>:</strong></td>
				<td width="60%"><input type="text" name="cpnWorkingName" size="30" value="<?php print str_replace('"',"&quot;",$cpnWorkingName)?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyCpnCod?>:</strong></td>
				<td width="60%"><input type="text" id="cpnNumber" name="cpnNumber" size="30" value="<?php print $cpnNumber?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyNumAvl?>:</strong></td>
				<td width="60%"><input type="text" id="cpnNumAvail" name="cpnNumAvail" size="10" value="<?php if((int)$cpnNumAvail != 30000000) print $cpnNumAvail?>" /></td>
			  </tr>
			  <?php /*?><tr>
				<td width="40%" align="right"><strong><?php print $yyDaysAv?>:</strong></td>
				<td width="60%"><input type="text" id="cpnEndDate" name="cpnEndDate" size="10" value="<?php
				if($cpnEndDate != '3000-01-01 00:00:00')
					if(strtotime($cpnEndDate)-time() < 0) print "Expired"; else print floor((strtotime($cpnEndDate)-time())/(60*60*24))+1; ?>"/></td>
			  </tr><?php */?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyBeginDate?>:</strong></td>
				<td width="60%"><input type="text" id="cpnBeginDate" name="cpnBeginDate" size="19" value="<?php if ($cpnBeginDate != "0000-00-00 00:00:00") { echo $cpnBeginDate; }?>" /><button type="reset" id="beginDte">...</button>
                        <script type="text/javascript">
                        Calendar.setup({
                            inputField     :    "cpnBeginDate",      // id of the input field
                            ifFormat       :    "%Y-%m-%d 00:00:00",       // format of the input field
                            showsTime      :    false,            // will display a time selector
                            button         :    "beginDte",   // trigger for the calendar (button ID)
                            singleClick    :    true,           // double-click mode
                            step           :    1                // show all years in drop-down boxes (instead of every other year as default)
                        });
                        </script></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyEndDate?>:</strong></td>
				<td width="60%"><input type="text" id="cpnEndDate" name="cpnEndDate" size="19" value="<?php 
				if($cpnEndDate != '3000-01-01 00:00:00')
					if(strtotime($cpnEndDate)-time() < 0) print "Expired"; else print $cpnEndDate; ?>" /><button type="reset" id="endDte">...</button>
                        <script type="text/javascript">
                        Calendar.setup({
                            inputField     :    "cpnEndDate",      // id of the input field
                            ifFormat       :    "%Y-%m-%d 00:00:00",       // format of the input field
                            showsTime      :    false,            // will display a time selector
                            button         :    "endDte",   // trigger for the calendar (button ID)
                            singleClick    :    true,           // double-click mode
                            step           :    1                // show all years in drop-down boxes (instead of every other year as default)
                        });
                        </script></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMinPur?>:</strong></td>
				<td width="60%"><input type="text" name="cpnThreshold" size="10" value="<?php if((int)$cpnThreshold>0) print $cpnThreshold?>" /> <strong><?php print $yyRepEvy?>:</strong> <input type="text" name="cpnThresholdRepeat" size="10" value="<?php if((int)$cpnThresholdRepeat > 0) print $cpnThresholdRepeat?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMaxPur?>:</strong></td>
				<td width="60%"><input type="text" name="cpnThresholdMax" size="10" value="<?php if((int)$cpnThresholdMax>0) print $cpnThresholdMax?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMinQua?>:</strong></td>
				<td width="60%"><input type="text" name="cpnQuantity" size="10" value="<?php if((int)$cpnQuantity>0) print $cpnQuantity?>" /> <strong><?php print $yyRepEvy?>:</strong> <input type="text" name="cpnQuantityRepeat" size="10" value="<?php if((int)$cpnQuantityRepeat > 0) print $cpnQuantityRepeat?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMaxQua?>:</strong></td>
				<td width="60%"><input type="text" name="cpnQuantityMax" size="10" value="<?php if((int)$cpnQuantityMax>0) print $cpnQuantityMax?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDscAmt?>:</strong></td>
				<td width="60%"><input type="text" name="cpnDiscount" size="10" value="<?php print $cpnDiscount?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyScope?>:</strong></td>
				<td width="60%"><select name="cpnSitewide" size="1">
					<option value="0"><?php print $yyIndCat?></option>
					<option value="3" <?php if((int)$cpnSitewide==3) print "selected" ?>><?php print $yyDsCaTo?></option>
					<option value="2" <?php if((int)$cpnSitewide==2) print "selected" ?>><?php print $yyGlInPr?></option>
					<option value="1" <?php if((int)$cpnSitewide==1) print "selected" ?>><?php print $yyGlPrTo?></option>
					</select></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyRestr?>:</strong></td>
				<td width="60%"><select name="cpnCntry" size="1">
					<option value="0"><?php print $yyAppAll?></option>
					<option value="1" <?php if((int)$cpnCntry==1) print "selected" ?>><?php print $yyYesRes?></option>
					</select></td>
			  </tr>
			  <tr>
			  	<td align="right"><strong>Is Stackable:</strong></td>
			  	<td><input <?php 
			  				if ($cpnStackable != 0) {
			  					echo "checked=\"checked\"";
			  				} ?>type="checkbox" name="cpnStackable" value="1" /></td>
			  </tr>
			  <tr>
			    <td align="right"><strong><?php print $yyAppliesToWS?>:</strong></td>
			    <td><input <?php if (!(strcmp($cpnIsWholesale,'1'))) {echo "checked=\"checked\"";} ?> type="checkbox" name="cpnIsWholesale" value="1" /></td>
		      </tr>
			  <tr>
  		        <td style="vertical-align: top; text-align: right;"><strong>Applicable Shipping Methods:</strong></td>
  		        <td>
  		            <?php
  		                $iFrogzConnection = DB_Connection_Pool::instance()->get_connection(new DB_DataSource('default_ifrogz'));

                        $ResultSet = $iFrogzConnection->query("SELECT * FROM postalzones WHERE pzID <= 100 AND pzName != '';");
                        $postalzones = $ResultSet->as_array();
                        $ResultSet->free();

                        $buffer = '';

                        foreach ($postalzones as $record) {
                            $buffer .= $record['pzName'] . '<br />';
                            for ($i = 1; $i <= 5; $i++) {
                                $key = "pzMethodName{$i}";
                                if ($record[$key] != '') {
                                    $isChecked = '';
                                    if (isset($_POST['id'])) {
                                        try {
                                            $ResultSet = $iFrogzConnection->query("SELECT * FROM fsadiscount WHERE cpnID = {$_POST['id']} AND pzID = {$record['pzID']} AND methodID = {$i};");
                                            $fsadiscount = $ResultSet->as_array();
                                            if (!empty($fsadiscount)) {
                                                $isChecked = 'checked="checked" ';
                                            }
                                            $ResultSet->free();
                                        }
                                        catch (Exception $ex) { }
                                    }
                                    $buffer .= "&nbsp;&nbsp;&nbsp;<input class=\"ShippingMethod\" type=\"checkbox\" name=\"pz[]\" value=\"{$record['pzID']},{$i}\" {$isChecked}/>" . $record[$key] . '<br />';
                                }
                            }
                        }
                        
                        echo $buffer;

                        $iFrogzConnection->close();
  		            ?>
  		        </td>
  			  </tr>
		<?php
			if($_POST['act']!="viewGrp") {
		?>
			  <tr>
                <td width="100%" colspan="2" align="center"><br /><input type="submit" value="<?php print $yySubmit?>" /><br />&nbsp;</td>
			  </tr>
		<?php
			}
		?>
			  <tr> 
                <td width="100%" colspan="2" align="center"><br />
                          <a href="/admin/index.php"><strong><?php print $yyAdmHom?></strong></a><br />
                          &nbsp;</td>
			  </tr>
            </table>
		  </td>
		</form>

        </tr>
<script language="JavaScript" type="text/javascript">
<!--
savebg=document.mainform.cpnNumber.style.backgroundColor;
savebc=document.mainform.cpnNumber.style.borderColor;
savecol=document.mainform.cpnNumber.style.color;
couponcodeactive(false);
changecouponeffect(false);
//-->
</script>
<?php 
		}elseif(@$_POST["posted"]=="1" && @$_POST["act"]=="viewGrp"){
			$sSQL = "SELECT * FROM grpCpn WHERE grpCpnID = " . $_POST['id'];
			$res = mysql_query($sSQL) or print(mysql_error());
			$row = mysql_fetch_assoc($res);
			
			$grpCpnID = $row['grpCpnID'];
			$grpCpnName = $row['grpCpnName'];
			$grpLinkID = $row['grpCpnLinkID'];
			$grpCpnQty = $row['grpCpnQty'];
			
			mysql_free_result($res);
			$sSQL = "SELECT * FROM coupons WHERE cpnGrpCpnID = " . $_POST['id'];
			$res = mysql_query($sSQL) or print(mysql_error());
			$rs = mysql_fetch_assoc($res);
			
			$cpnName = $rs["cpnName"];
			for($index=2; $index <= $adminlanguages+1; $index++)
				$cpnNames[$index] = $rs["cpnName" . $index];
			$cpnWorkingName = $rs["cpnWorkingName"];
			$cpnNumber = $rs["cpnNumber"];
			$cpnType = $rs["cpnType"];
			$cpnEndDate = $rs["cpnEndDate"];
			$cpnDiscount = $rs["cpnDiscount"];
			$cpnThreshold = $rs["cpnThreshold"];
			$cpnThresholdMax = $rs["cpnThresholdMax"];
			$cpnThresholdRepeat = $rs["cpnThresholdRepeat"];
			$cpnQuantity = $rs["cpnQuantity"];
			$cpnQuantityMax = $rs["cpnQuantityMax"];
			$cpnQuantityRepeat = $rs["cpnQuantityRepeat"];
			$cpnNumAvail = $rs["cpnNumAvail"];
			$cpnCntry = $rs["cpnCntry"];
			$cpnIsCoupon = $rs["cpnIsCoupon"];
			$cpnSitewide = $rs["cpnSitewide"];
			$cpnIsGrp = $rs["cpnGrpCpnID"];
			$cpnBeginDate = $rs["cpnBeginDate"];
			mysql_free_result($res);
?>
        <tr>
		<form name="mainform" method="post" action="/admin/discounts.php" onsubmit="return formvalidator(this)">
		  <td width="100%">
			<input type="hidden" name="posted" value="1" />
		<?php	if(@$_POST["act"]=="modify"){ ?>
			<input type="hidden" name="act" value="domodify" />
			<input type="hidden" name="id" value="<?php print @$_POST["id"]?>" />
		<?php	}else{ ?>
			<input type="hidden" name="act" value="doaddnew" />
		<?php	} ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><strong><?php print $yyDscNew?></strong><br />&nbsp;</td>
			  </tr>
			  <tr class="trGroup">
			  	<td width="40%" align="right"><strong>Group Name:</strong></td>
				<td width="60%"><input type="text" id="grpName" name="grpName" value="<?=$grpCpnName?>" disabled="disabled" style="background-color: #DDD; border-color: #aa3300; color: #A30;" /></td>
			  </tr>
			  <tr class="trGroup">
			  	<td width="40%" align="right"><strong>Number of Coupons:</strong></td>
				<td width="60%"><input type="text" id="grpQty" name="grpQty" value="<?=$grpCpnQty?>" /></td>
			  </tr>
			  <tr class="trGroup">
			  	<td width="40%" align="right"><strong>Link to Group:</strong></td>
				<td width="60%">
					<select id="linkGrp" name="linkGrp" onchange="qty()">
			<?php
				$qry = "SELECT * FROM grpCpn WHERE grpCpnLinkID IS NULL OR grpCpnLinkID = ''";
				$res = mysql_query($qry) or print(mysql_error());
				echo '						<option value="none">None</option>'."\n";
				while($row = mysql_fetch_assoc($res)) {
			?>
						<option value="<?=$row['grpCpnID']?>"<?=($row['grpCpnID']==$grpLinkID)?'selected="selected"':''?>><?=$row['grpCpnName']?></option>
			<?php
				}
			?>
					</select>
				</td>
			  </tr>
			  <tr>
			  	<td colspan="2">&nbsp;</td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyCpnDsc?>:</strong></td>
				<td width="60%"><select name="cpnIsCoupon" size="1" onchange="couponcodeactive(false);">
					<option value="0"><?php print $yyDisco?></option>
					<option value="1" <?php if((int)$cpnIsCoupon==1) print "selected" ?>><?php print $yyCoupon?></option>
					</select></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDscEff?>:</strong></td>
				<td width="60%"><select name="cpnType" size="1" onchange="changecouponeffect(false);">
					<option value="0"><?php print $yyFrSShp?></option>
					<option value="1" <?php if((int)$cpnType==1) print "selected" ?>><?php print $yyFlatDs?></option>
					<option value="2" <?php if((int)$cpnType==2) print "selected" ?>><?php print $yyPerDis?></option>
					</select></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDisTxt?>:</strong></td>
				<td width="60%"><input type="text" name="cpnName" size="30" value="<?php print str_replace('"',"&quot;",$cpnName)?>" /></td>
			  </tr>
<?php		for($index=2; $index <= $adminlanguages+1; $index++){
				if(($adminlangsettings & 1024)==1024){ ?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDisTxt . " " . $index?>:</strong></td>
				<td width="60%"><input type="text" name="cpnName<?php print $index?>" size="30" value="<?php print str_replace('"',"&quot;",$cpnNames[$index])?>" /></td>
			  </tr>
<?php			}
			} ?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyWrkNam?>:</strong></td>
				<td width="60%"><input type="text" name="cpnWorkingName" size="30" value="<?php print str_replace('"',"&quot;",$cpnWorkingName)?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyCpnCod?>:</strong></td>
				<td width="60%"><input type="text" id="cpnNumber" name="cpnNumber" size="30" value="<?php print $cpnNumber?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyNumAvl?>:</strong></td>
				<td width="60%"><input type="text" id="cpnNumAvail" name="cpnNumAvail" size="10" value="<?php if((int)$cpnNumAvail != 30000000) print $cpnNumAvail?>" /></td>
			  </tr>
			  <?php /*?><tr>
				<td width="40%" align="right"><strong><?php print $yyDaysAv?>:</strong></td>
				<td width="60%"><input type="text" id="cpnEndDate" name="cpnEndDate" size="10" value="<?php
				if($cpnEndDate != '3000-01-01 00:00:00')
					if(strtotime($cpnEndDate)-time() < 0) print "Expired"; else print floor((strtotime($cpnEndDate)-time())/(60*60*24))+1; ?>"/></td>
			  </tr><?php */?>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyBeginDate?>:</strong></td>
				<td width="60%"><input type="text" name="cpnBeginDate" size="19" value="<?php if ($cpnBeginDate != "0000-00-00 00:00:00") { echo $cpnBeginDate; }?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyEndDate?>:</strong></td>
				<td width="60%"><input type="text" name="cpnEndDate" size="19" value="<?php 
				if($cpnEndDate != '3000-01-01 00:00:00')
					if(strtotime($cpnEndDate)-time() < 0) print "Expired"; else print $cpnEndDate; ?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMinPur?>:</strong></td>
				<td width="60%"><input type="text" name="cpnThreshold" size="10" value="<?php if((int)$cpnThreshold>0) print $cpnThreshold?>" /> <strong><?php print $yyRepEvy?>:</strong> <input type="text" name="cpnThresholdRepeat" size="10" value="<?php if((int)$cpnThresholdRepeat > 0) print $cpnThresholdRepeat?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMaxPur?>:</strong></td>
				<td width="60%"><input type="text" name="cpnThresholdMax" size="10" value="<?php if((int)$cpnThresholdMax>0) print $cpnThresholdMax?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMinQua?>:</strong></td>
				<td width="60%"><input type="text" name="cpnQuantity" size="10" value="<?php if((int)$cpnQuantity>0) print $cpnQuantity?>" /> <strong><?php print $yyRepEvy?>:</strong> <input type="text" name="cpnQuantityRepeat" size="10" value="<?php if((int)$cpnQuantityRepeat > 0) print $cpnQuantityRepeat?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyMaxQua?>:</strong></td>
				<td width="60%"><input type="text" name="cpnQuantityMax" size="10" value="<?php if((int)$cpnQuantityMax>0) print $cpnQuantityMax?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyDscAmt?>:</strong></td>
				<td width="60%"><input type="text" name="cpnDiscount" size="10" value="<?php print $cpnDiscount?>" /></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyScope?>:</strong></td>
				<td width="60%"><select name="cpnSitewide" size="1">
					<option value="0"><?php print $yyIndCat?></option>
					<option value="3" <?php if((int)$cpnSitewide==3) print "selected" ?>><?php print $yyDsCaTo?></option>
					<option value="2" <?php if((int)$cpnSitewide==2) print "selected" ?>><?php print $yyGlInPr?></option>
					<option value="1" <?php if((int)$cpnSitewide==1) print "selected" ?>><?php print $yyGlPrTo?></option>
					</select></td>
			  </tr>
			  <tr>
				<td width="40%" align="right"><strong><?php print $yyRestr?>:</strong></td>
				<td width="60%"><select name="cpnCntry" size="1">
					<option value="0"><?php print $yyAppAll?></option>
					<option value="1" <?php if((int)$cpnCntry==1) print "selected" ?>><?php print $yyYesRes?></option>
					</select></td>
			  </tr>
		<?php
			if($_POST['act']!="viewGrp") {
		?>
			  <tr>
                <td width="100%" colspan="2" align="center"><br /><input type="submit" value="<?php print $yySubmit?>" /><br />&nbsp;</td>
			  </tr>
		<?php
			}
		?>
			  <tr> 
                <td width="100%" colspan="2" align="center"><br />
                          <a href="/admin/index.php"><strong><?php print $yyAdmHom?></strong></a><br />
                          &nbsp;</td>
			  </tr>
            </table>
		  </td>
		</form>
        </tr>
<script language="JavaScript" type="text/javascript">
<!--
savebg=document.mainform.cpnNumber.style.backgroundColor;
savebc=document.mainform.cpnNumber.style.borderColor;
savecol=document.mainform.cpnNumber.style.color;
couponcodeactive(false);
changecouponeffect(false);

var oForm = document.forms["mainform"];
for(i=0; i<oForm.elements.length; i++) {
	oForm.elements[i].disabled=true;
	oForm.elements[i].style.backgroundColor="#DDDDDD";
	oForm.elements[i].style.borderColor="#aa3300";
	oForm.elements[i].style.color="#aa3300";
}

//-->
</script>
<?php
		}elseif(@$_POST["posted"]=="1" && $success){ ?>
        <tr>
          <td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><br /><strong><?php print $yyUpdSuc?></strong><br /><br /><?php print $yyNowFrd?><br /><br />
                        <?php print $yyNoAuto?> <a href="/admin/discounts.php"><strong><?php print $yyClkHer?></strong></a>.<br />
                        <br />
				<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" />
                </td>
			  </tr>
			</table></td>
        </tr>
<?php }elseif(@$_POST["posted"]=="1"){ ?>
        <tr>
          <td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><br /><font color="#FF0000"><strong><?php print $yyOpFai?></strong></font><br /><br /><?php print $errmsg?><br /><br />
				<a href="javascript:history.go(-1)"><strong><?php print $yyClkBac?></strong></a></td>
			  </tr>
			</table></td>
        </tr>
<?php }else{
?>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function modrec(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "modify";
	document.mainform.submit();
}
function newrec(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "addnew";
	document.mainform.submit();
}
function delrec(id) {
    var cmsg = "<?php print $yyConDel?>\n";
	if (confirm(cmsg)) {
		document.mainform.id.value = id;
		document.mainform.act.value = "delete";
		document.mainform.submit();
	}
}
function delGrp(id) {
	var cmsg = "Are you sure you want to delete this Group?";
	if(confirm(cmsg)) {
		document.mainform.id.value = id;
		document.mainform.act.value = "deleteGrp";
		document.mainform.submit();
	}
}
function viewGrp(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "viewGrp";
	document.mainform.submit();
}
function viewAssoc() {
	window.location = "/admin/discountinfo.php";
}
function toggleGrp(id) {
	var grp = document.getElementsByClassName("grp"+id);
	var imgSrc = document.getElementById("grpImg"+id).src;

	if (imgSrc.match("/lib/images/expand.gif")) {
		$("grpImg"+id).src="/lib/images/collapse.gif";
	} else {
		$("grpImg"+id).src="/lib/images/expand.gif";
	}
	
	for (i = 0; i < grp.length; i++) {
		if (grp[i].style.display == "") {
			grp[i].style.display = "none;";
		} else{
			grp[i].style.display = "";
		}
	}
}
// -->
</script>
<style type="text/css">
<!--
a img{
border: none;
}

.discountrow:hover {
	background-color: #FFFFCC;
	color: #000;
}
-->
</style>
        <tr>
		  <form name="mainform" method="post" action="/admin/discounts.php">
		  <td width="100%">
			<input type="hidden" name="posted" value="1" />
			<input type="hidden" name="act" value="xxxxx" />
			<input type="hidden" name="id" value="xxxxx" />
			<input type="hidden" name="selectedq" value="1" />
			<input type="hidden" name="newval" value="1" />
            <table width="100%" border="0" cellspacing="0" cellpadding="1" bgcolor="">
			  <tr> 
                <td width="100%" colspan="6" align="center"><br /><strong><?php print $yyDscAdm?></strong><br />&nbsp;</td>
			  </tr>
			  <tr>
			  	<td width="100%" colspan="6" align="center"><strong>View Products Associated to Discounts</strong>&nbsp;&nbsp;<input type="button" value="View" onclick="viewAssoc()" /></td>
			  </tr>
			  <tr> 
                <td width="100%" colspan="6" align="center"><br /><strong><?php print $yyPOClk?> </strong>&nbsp;&nbsp;<input type="button" value="<?php print $yyNewDsc?>" onclick="newrec()" /><br />&nbsp;</td>
			  </tr>
			  <tr>
				<td width="30%" align="left"><strong><?php print $yyWrkNam?></strong></td>
				<td width="10%" align="center"><strong><?php print $yyType?></strong></td>
                <td width="16px" align="center">&nbsp;</td>
				<td width="10%" align="center"><strong><?php print $yyBegDat?></strong></td>
				<td width="10%" align="center"><strong><?php print $yyExpDat?></strong></td>
				<td width="10%" align="center"><strong>Code</strong></td>
				<td width="10%" align="center"><strong><?php print $yyGlobal?></strong></td>
				<td width="10%" align="center"><strong><?php print $yyModify?></strong></td>
				<td width="10%" align="center"><strong><?php print $yyDelete?></strong></td>
			  </tr>
<?php
	$bgcolor="";
	$sSQL = "SELECT cpnID, cpnWorkingName, cpnSitewide, cpnIsCoupon, cpnEndDate, cpnGrpCpnID, cpnNumber, cpnBeginDate,cpnType, cpnDiscount FROM coupons ORDER BY cpnIsCoupon,cpnEndDate DESC,cpnBeginDate DESC,cpnWorkingName";
	$result = mysql_query($sSQL) or print(mysql_error());
	$grpShown = array();
	$i=0;
	if(mysql_num_rows($result) > 0){
		while($alldata = mysql_fetch_row($result)){
			if($i%2==1) $bgcolor="#FFFFFF"; else $bgcolor="#E7EAEF"; ?>
			<?php
			if(!empty($alldata[5]) && !in_array($alldata[5],$grpShown)) {
				$sql_grp = "SELECT * FROM grpCpn WHERE grpCpnID = '".$alldata[5]."'";
				$res_grp = mysql_query($sql_grp);
				$row_grp = mysql_fetch_assoc($res_grp) or print(mysql_error());
			?>
			  <tr class="discountrow" bgcolor="<?php print $bgcolor?>" style="font-weight: bold">
			    <td valign="center"><!--<a onclick="toggleGrp(<?=$alldata[5]?>)" style="cursor: pointer"><img id="grpImg<?=$alldata[5]?>" src="/lib/images/expand.gif" width="9" height="9" alt="Expand Group" style="padding-right: 2px" /></a>--><?=$alldata[1]?><?php if(!empty($row_grp['grpCpnLinkID'])){?><img src="/lib/images/misc/linked.gif" width="14" height="9" style="padding-left: 5px" /><?php }?><a href="/admin/expgroup.php?grpID=<?=$alldata[5]?>"><img src="/lib/images/icon-download.gif" width="20" height="16" alt="Export to Excel" style="margin: 0 5px" /></a></td>
				<td align="center">Group</td>
                <td>&nbsp;</td>
				<td align="center"><?php if ($alldata[7] != "0000-00-00 00:00:00") {
										 	echo str_replace(" ", "&nbsp;", date("M j 'y g:i A",strtotime($alldata[7]))); 
										 }
								   ?>
				</td>
				<td align="center"><?php	if($alldata[4]=='3000-01-01 00:00:00')
												print $yyNever;
											elseif(strtotime($alldata[4])-time() < 0)
												print '<font color="#FF0000">' . $yyExpird . '</font>';
											else
												print str_replace(" ", "&nbsp;", date("M j 'y g:i A",strtotime($alldata[4]))); ?></td>
				<td align="center">---</td>
				<td align="center"><?php if($alldata[2]==1 || $alldata[2]==2) print $yyYes; else print $yyNo; ?></td>
				<td align="center"><input type="button" value="View" onclick="viewGrp('<?=$alldata[5]?>')" /></td>
				<td align="center"><input type="button" value="<?php print $yyDelete?>" onclick="delGrp('<?=$alldata[5]?>')" /></td>
			  </tr>
			<?php
				$i++;
				array_push($grpShown,$alldata[5]);
			}elseif(empty($alldata[5])){
				if($i%2==1) $bgcolor="#FFFFFF"; else $bgcolor="#E7EAEF";
			?>
			  <tr class="discountrow" bgcolor="<?php print $bgcolor?>">
				<td><?php print $alldata[1]?></td>
				<td align="center"><?php	if($alldata[3]==1) print $yyCoupon; else print $yyDisco;?></td>
                <td align="center" valign="middle"><?php 
									$isBW = "";
									if ((strtotime($alldata[4])-time() < 0) || (strtotime($alldata[7]) - time() > 0)) { // If its expired
										$isBW = "_bw";
									}
									if ($alldata[8] == 2) {
										echo "<span style='font-weight: bold;'>".$alldata[9]."</span>&nbsp;<img src='/lib/images/misc/icons/percent".$isBW.".png' width='16' height='16' />";
									} else if ($alldata[8] == 1) {
										echo "<span style='font-weight: bold;'>".sprintf("%.2f", $alldata[9])."</span>&nbsp;<img src='/lib/images/misc/icons/dollar".$isBW.".png' width='16' height='16' />";
									} else {
										echo "&nbsp;";
									}
									
								   
								   
								   ?></td>
				<td align="center" style="padding-right: 10px;"><?php if ($alldata[7] != "0000-00-00 00:00:00") {
										 	echo str_replace(" ", "&nbsp;", date("M j 'y g:i A",strtotime($alldata[7]))); 
										 }
								   ?>
				</td>
				<td align="center"><?php	if($alldata[4]=='3000-01-01 00:00:00')
												print $yyNever;
											elseif(strtotime($alldata[4])-time() < 0)
												print '<font color="#FF0000">' . $yyExpird . '</font>';
											else
												print str_replace(" ", "&nbsp;", date("M j 'y g:i A",strtotime($alldata[4]))); ?></td>
				<td align="center"><?=$alldata[6]?></td>
				<td align="center"><?php if($alldata[2]==1 || $alldata[2]==2) print $yyYes; else print $yyNo; ?></td>
				<td align="center"><input type="button" value="<?php print $yyModify?>" onclick="modrec('<?php print $alldata[0]?>')" /></td>
				<td align="center"><input type="button" value="<?php print $yyDelete?>" onclick="delrec('<?php print $alldata[0]?>')" /></td>
			  </tr>
			<?php
				$i++;
			}
			?>
<?php	}
	}else{
?>
			  <tr> 
                <td width="100%" colspan="6" align="center"><br /><strong><?php print $yyNoDsc?></strong><br />&nbsp;</td>
			  </tr>
<?php
	}
?>
			  <tr> 
                <td width="100%" colspan="6" align="center"><br /><strong><?php print $yyPOClk?> </strong>&nbsp;&nbsp;<input type="button" value="<?php print $yyNewDsc?>" onclick="newrec()" /><br />&nbsp;</td>
			  </tr>
			  <tr> 
                <td width="100%" colspan="6" align="center"><br />
                          <a href="/admin/index.php"><strong><?php print $yyAdmHom?></strong></a><br />
				<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
            </table></td>
		  </form>
        </tr>
<?php
}
?>
      </table>
<?php

function getPromoCode() {
	$code = get_unique_id(8);
	
	$sql = "SELECT cpnNumber FROM coupons WHERE cpnNumber = '".mysql_real_escape_string($code)."'";
	$res = mysql_query($sql) or print(mysql_error());
	if(mysql_num_rows($res) > 0) {
		getPromoCode();
	}
	return $code;
}

function get_unique_id($length=32, $pool=""){
 // set pool of possible char
 if($pool == ""){
  $pool = "abcdefghijklmnopqrstuvwxyz";
  $pool .= "0123456789";
 }// end if
 mt_srand ((double) microtime() * 1000000);
 $unique_id = "";
 for ($index = 0; $index < $length; $index++) {
  $unique_id .= substr($pool, (mt_rand()%(strlen($pool))), 1);
 }// end for
 return($unique_id);
}// end get_unique_id 

/*
* Calculates the difference between two dates
*
* @param unix_timestamp
* @param unix_timestamp
* @return array
*/
function date_diff2($earlierDate, $laterDate) 
{
  //returns an array of numeric values representing days, hours, minutes & seconds respectively
  $ret=array('days'=>0,'hours'=>0,'minutes'=>0,'seconds'=>0);

  $totalsec = $laterDate - $earlierDate;
  if ($totalsec >= 86400) {
   $ret['days'] = floor($totalsec/86400);
   $totalsec = $totalsec % 86400;
  }
  if ($totalsec >= 3600) {
   $ret['hours'] = floor($totalsec/3600);
   $totalsec = $totalsec % 3600;
  }
  if ($totalsec >= 60) {
   $ret['minutes'] = floor($totalsec/60);
  }
  $ret['seconds'] = $totalsec % 60;
  return $ret;
}
?>