<?php
include('init.php');
if ( ! defined('KOHANA_EXTERNAL_MODE')) {
    define('KOHANA_EXTERNAL_MODE', TRUE);
}
include_once(DOCROOT.'index.php');
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
//HCS - DRE 03/06/2005 Gift Cert Mod START
function bus_days($days_to_add) {
	$start_date = strtotime(date("Y-m-d"));
	$workdays_only = true;
	$skip_holidays = true;

	$seconds_in_a_day = 86400;
	$sunday_val = "0";
    $saturday_val = "6";
	$holiday_array = array();
	
    $ptr_year = intval(date("Y", $start_date));
	$holiday_array[$ptr_year] = get_holidays(date("Y",$start_date));
	//showarray($holiday_array); exit();
	
	$cur_day = $start_date;
	while($days_to_add > 0) {
		$cur_day += $seconds_in_a_day;
		$day_of_week = date("w",$cur_day);
		//echo date("Y-m-d H:i:s",$cur_day).' '.$days_to_add.'<br />';
		if($workdays_only) {
			if(($day_of_week != $sunday_val) && ($day_of_week != $saturday_val)) {
				if($skip_holidays) {
                    if(intval(date("Y", $cur_day)) != $ptr_year){
                        $ptr_year = intval(date("Y", $cur_day));
                        $holiday_array[$ptr_year] = get_holidays(date("Y", $cur_day));
                    }
					
                    if(!in_array($cur_day, $holiday_array[date("Y", $cur_day)])){
                        $days_to_add--;
                    }
				}else{
					$days_to_add--;
				}
			}
		}else{
            if($skip_holidays){
                if(intval(date("Y", $cur_day))!=$ptr_year){
                    $ptr_year = intval(date("Y", $cur_day));
                    $holiday_array[$ptr_year] = get_holidays(date("Y", $cur_day));
                }
                if(!in_array($cur_day, $holiday_array[date("Y", $cur_day)])){
                    $days_to_add--;
                }
            }else{
                $days_to_add--;
            }
		}
	}
	
	return $cur_day;
}

/**
 * Takes a date in yyyy-mm-dd format and returns a PHP timestamp
 *
 * @param string $MySqlDate
 * @return unknown
 */
function get_timestamp($MySqlDate){

    $date_array = explode("-",$MySqlDate); // split the array

    $var_year = $date_array[0];
    $var_month = $date_array[1];
    $var_day = $date_array[2];

    $var_timestamp = mktime(0,0,0,$var_month,$var_day,$var_year);
    return($var_timestamp); // return it to the user
}

/**
 * Returns the date of the $ord $day of the $month.
 * For example ordinal_day(3, 'Sun', 5, 2001) returns the
 * date of the 3rd Sunday of May (ie. Mother's Day).
 *
 * @author  heymeadows@yahoo.com
 *
 * @param int $ord
 * @param string $day (must be 3 char abbrev, per date("D);)
 * @param int $month
 * @param int $year
 * @return unknown
 */
function ordinal_day($ord, $day, $month, $year) {

    $firstOfMonth = get_timestamp("$year-$month-01");
    $lastOfMonth  = $firstOfMonth + date("t", $firstOfMonth) * 86400;
    $dayOccurs = 0;

    for ($i = $firstOfMonth; $i < $lastOfMonth ; $i += 86400){
        if (date("D", $i) == $day){
            $dayOccurs++;
            if ($dayOccurs == $ord){
                $ordDay = $i;
            }
        }
    }
    return $ordDay;
}

function memorial_day($inc_year){
    for($date_stepper = intval(date("t", strtotime("$inc_year-05-01"))); $date_stepper >= 1; $date_stepper--){
        if(date("l", strtotime("$inc_year-05-$date_stepper"))=="Monday"){
            return strtotime("$inc_year-05-$date_stepper");
            break;
        }
    }
}


/**
 * Looks through a lists of defined holidays and tells you which
 * one is coming up next.
 *
 * @author heymeadows@yahoo.com
 *
 * @param int $inc_year The year we are looking for holidays in
 * @return array
 */
function get_holidays($inc_year){
    //$year = date("Y");
    $year = $inc_year;

    $holidays[] = new Holiday("New Year's Day", get_timestamp("$year-1-1"));
    //$holidays[] = new Holiday("Australia Day", get_timestamp("$year-1-26"));
    $holidays[] = new Holiday("Labour Day", ordinal_day(1, 'Mon', 3, $year));
    //$holidays[] = new Holiday("Anzac Day", get_timestamp("$year-4-25"));
    //$holidays[] = new Holiday("St. Patrick's Day", get_timestamp("$year-3-17"));
    // TODO: $holidays[] = new Holiday("Good Friday", easter_date($year));
	$holidays[] = new Holiday("Easter", easter_date($year));
    // TODO: $holidays[] = new Holiday("Easter Monday", easter_date($year));
    //$holidays[] = new Holiday("Foundation Day", ordinal_day(1, 'Mon', 6, $year));
    //$holidays[] = new Holiday("Queen's Birthday", ordinal_day(1, 'Mon', 10, $year));
    $holidays[] = new Holiday("Memorial Day", memorial_day($year));
    //$holidays[] = new Holiday("Mother's Day", ordinal_day(2, 'Sun', 5, $year));
    //$holidays[] = new Holiday("Father's Day", ordinal_day(3, 'Sun', 6, $year));
    $holidays[] = new Holiday("Independence Day", get_timestamp("$year-7-4"));
	$holidays[] = new Holiday("Pioneer Day", get_timestamp("$year-7-24"));
    $holidays[] = new Holiday("Labor Day", ordinal_day(1, 'Mon', 9, $year));
	$holidays[] = new Holiday("Thanksgiving Day", ordinal_day(4, 'Thu', 11, $year));
	$holidays[] = new Holiday("Day after Thanksgiving", ordinal_day(4, 'Fri', 11, $year));
	//$holidays[] = new Holiday("Christmas Eve", get_timestamp("$year-12-24"));
    $holidays[] = new Holiday("Christmas", get_timestamp("$year-12-25"));
    //$holidays[] = new Holiday("Boxing Day", get_timestamp("$year-12-26"));
	$holidays[] = new Holiday("New Years Eve", get_timestamp("$year-12-31"));

    $numHolidays = count($holidays);
    $out_array = array();

    for ($i = 0; $i < $numHolidays; $i++){
        $out_array[] = $holidays[$i]->date;
    }
    unset($holidays);
    return $out_array;
}

class Holiday{
    //var $name;
    //var $date;
    var $name;
    var $date;

    // Contructor to define the details of each holiday as it is created.
    function holiday($name, $date){
        $this->name   = $name;   // Official name of holiday
        $this->date   = $date;   // UNIX timestamp of date
    }
}

 
//HCS - DRE 03/06/2005 Gift Cert Mod STOP
function vrhmac($key, $text){
	$idatastr = "                                                                ";
	$odatastr = "                                                                ";
	$hkey = (string)$key;
	$idatastr .= $text;
	for($i=0; $i<64; $i++){
		$idata[$i] = $ipad[$i] = 0x36;
		$odata[$i] = $opad[$i] = 0x5C;
	}
	for($i=0; $i< strlen($hkey); $i++){
		$ipad[$i] ^= ord($hkey{$i});
		$opad[$i] ^= ord($hkey{$i});
		$idata[$i] = ($ipad[$i] & 0xFF);
		$odata[$i] = ($opad[$i] & 0xFF);
	}
	for($i=0; $i< strlen($text); $i++){
		$idata[64+$i] = ord($text{$i}) & 0xFF;
	}
	for($i=0; $i< strlen($idatastr); $i++){
		$idatastr{$i} = chr($idata[$i] & 0xFF);
	}
	for($i=0; $i< strlen($odatastr); $i++){
		$odatastr{$i} = chr($odata[$i] & 0xFF);
	}
	$innerhashout = md5($idatastr);
	for($i=0; $i<16; $i++)
		$odatastr .= chr(hexdec(substr($innerhashout,$i*2,2)));
	return md5($odatastr);
}
function order_success($sorderid,$sEmail,$sendstoreemail){
	do_order_success($sorderid,$sEmail,$sendstoreemail,TRUE,TRUE,TRUE,TRUE);
}
function do_order_success($sorderid,$sEmail,$sendstoreemail,$doshowhtml,$sendcustemail,$sendaffilemail,$sendmanufemail){
	global $maintablebg,$innertablebg,$maintablewidth,$innertablewidth,$maintablespacing,$innertablespacing,$maintablepadding,$innertablepadding,$thereference,$emlNl,$htmlemails,$extraorderfield1,$extraorderfield2,$shipType,$emailheader,$emailfooter,$emailencoding,$hideoptpricediffs,$xxWtIns,$ordGrandTotal,$ordID,$digidownloads,$dropshipfooter,$dropshipheader,$digidownloademail;
	global $xxHndlg,$xxDscnts,$xxOrdId,$xxCusDet,$xxEmail,$xxPhone,$xxShpDet,$xxShpMet,$xxAddInf,$xxPrId,$xxPrNm,$xxQuant,$xxUnitPr,$xxOrdTot,$xxStaTax,$xxCntTax,$xxShippg,$xxGndTot,$xxOrdStr,$xxTnxOrd,$xxTouSoo,$xxAff1,$xxAff2,$xxAff3,$xxThnks,$xxThkYou,$xxRecEml,$storeurl,$xxHomeURL,$xxCntShp,$success,$ordAuthNumber,$orderText,$ordTotal,$customheaders,$digidownloadsecret,$useaddressline2,$xxTouSooShipping,$xxTouSooDate,$xxTwoOrdIds;
	if(@$htmlemails==TRUE) $emlNl = "<br />"; else $emlNl="\n";
	if(@$customheaders == ""){
		$customheaders = "MIME-Version: 1.0\n";
		$customheaders .= "From: %from% <%from%>\n";
		//$customheaders .= "To: " . $custEmail . " <" . $custEmail . ">\n";
		if(@$htmlemails==TRUE)
			$customheaders .= "Content-type: text/html; charset=".$emailencoding."\n";
		else
			$customheaders .= "Content-type: text/plain; charset=".$emailencoding."\n";
	}
	$affilID = "";
	$saveHeader = "";
	$ordID = $sorderid;
	$hasdownload=FALSE;
	$ndropshippers=0;
	//HCS - DRE 03/07/05 Gift Cert Mod START
	$strsql = "SELECT * FROM orders WHERE ordID =".$ordID." AND ordStatus >= 3";
	$result = mysql_query($strsql);
	if($rs=mysql_fetch_assoc($result)) { 
		$certstr = create_certificate($ordID);
	}else{
		$prodarray = array();
		$certorder = FALSE;
		$strsql = "SELECT pID FROM products WHERE p_iscert > 0";
		$result = mysql_query($strsql);
		while ($rs=mysql_fetch_assoc($result)) {
			$prodarray[] = $rs['pID'];
		}
		$prodarray = "'".implode("','",$prodarray)."'";
		if($prodarray == "") $prodarray = "NOVALIDPRODIDFORTHISQUERY696986";
		$strsql = "SELECT cartProdID FROM cart, orders  WHERE ordID =".$ordID."  AND cartOrderID =".$ordID." AND cartProdID IN (".$prodarray.")";
		$result = mysql_query($strsql);
		if(mysql_num_rows($result) > 0) $certorder = TRUE;
	}
	$strsql2 = "SELECT pend_order_amt, cert_amt, cert_exp_dt, cert_id, cert_code FROM certificates WHERE pend_order_id =".$ordID;
	$result2 = mysql_query($strsql2);
	$pendingamt = 0;
	$remainbal = 0;
	$certexpdt = 0;
	$certid = 0;
	$certcode = 0;
	$certend = "";
	if($rs2=mysql_fetch_assoc($result2)) {
		$pendingamt = $rs2['pend_order_amt'];
		$remainbal = $rs2['cert_amt'] - $rs['pend_order_amt'];
		$certexpdt = $rs2['cert_exp_dt'];
		$certid = $rs2['cert_id'];
		$certcode = $rs2['cert_code'];
		$certend = "#".substr($certcode, strlen($certcode)- 4, 4);
		$strsql = "UPDATE certificates SET cert_amt = cert_amt - pend_order_amt WHERE pend_order_id =".$ordID;
		mysql_query($strsql);
		$strsql = "UPDATE certificates SET pend_order_amt = 0, pend_order_id = 0 WHERE pend_order_id =".$ordID;
		mysql_query($strsql);
	//	$strsql = "DELETE certificates WHERE cert_amt <= 0";
	//	mysql_query($strsql);
		$strsql = "UPDATE orders SET ord_cert_id=".$certid.", ord_cert_amt=".$pendingamt." WHERE ordID =".$ordID;
		mysql_query($strsql);
	
	}
	//HCS - DRE 03/07/05 Gift Cert Mod STOP
	
	// GET DELIVERY DATE
	$delivery = getDeliveryDate($ordID);
	
	$sSQL = "SELECT ordID,ordName,ordAddress,ordAddress2,ordCity,ordState,ordZip,ordCountry,ordEmail,ordPhone,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipZip,ordShipCountry,ordPayProvider,ordAuthNumber,ordTotal,ordDate,ordStateTax,ordCountryTax,ordHSTTax,ordHandling,ordShipping,ordAffiliate,ordDiscount,ordDiscountText,ordComLoc,ordExtra1,ordExtra2,ordSessionID,ordAddInfo,ordShipType,payProvID FROM orders LEFT JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordAuthNumber<>'' AND ordID='" . mysql_real_escape_string($sorderid) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result) > 0){
		$rs = mysql_fetch_assoc($result);
		$orderText = "";
		$success=TRUE;
		$orderID2 = $rs["ordID"];
		$ordAuthNumber = $rs["ordAuthNumber"];
		$ordSessionID = $rs["ordSessionID"];
		$payprovid = $rs["payProvID"];
		if(@$emailheader != "") $saveHeader .= $emailheader;
		eval('global $emailheader' . $payprovid . ';$emailheader = @$emailheader' . $payprovid . ';');
		if(@$emailheader != "") $saveHeader .= $emailheader;
		if(!empty($_SESSION['neworderID'])) {
			$orderText .= $xxTwoOrdIds . $emlNl;
			$orderText .= $xxOrdId . ": " . $rs["ordID"] .', '.$_SESSION['neworderID']. $emlNl;
		} else $orderText .= $xxOrdId . ": " . $rs["ordID"] . $emlNl;
		if($thereference != "") $orderText .= "Transaction Ref" . ": " . $thereference . $emlNl;
		$orderText .= $xxCusDet . ": " . $emlNl;
		if(trim(@$extraorderfield1)!="") $orderText .= $extraorderfield1 . ": " . $rs["ordExtra1"] . $emlNl;
		$orderText .= $rs["ordName"] . $emlNl;
		$orderText .= $rs["ordAddress"] . $emlNl;
		if(@$useaddressline2==TRUE && trim($rs["ordAddress2"]) != '') $orderText .= $rs["ordAddress2"] . $emlNl;
		$orderText .= $rs["ordCity"] . ", " . $rs["ordState"] . $emlNl;
		$orderText .= $rs["ordZip"] . $emlNl;
		$orderText .= $rs["ordCountry"] . $emlNl;
		$orderText .= $xxEmail . ": " . $rs["ordEmail"] . $emlNl;
		$custEmail = $rs["ordEmail"];
		$orderText .= $xxPhone . ": " . $rs["ordPhone"] . $emlNl;
		if(trim(@$extraorderfield2)!="") $orderText .= $extraorderfield2 . ": " . $rs["ordExtra2"] . $emlNl;
		if(trim($rs["ordShipName"]) != "" || trim($rs["ordShipAddress"]) != ""){
			$orderText .= $xxShpDet . ": " . $emlNl;
			$orderText .= $rs["ordShipName"] . $emlNl;
			$orderText .= $rs["ordShipAddress"] . $emlNl;
			if(@$useaddressline2==TRUE && trim($rs["ordShipAddress2"]) != '') $orderText .= $rs["ordShipAddress2"] . $emlNl;
			$orderText .= $rs["ordShipCity"] . ", " . $rs["ordShipState"] . $emlNl;
			$orderText .= $rs["ordShipZip"] . $emlNl;
			$orderText .= $rs["ordShipCountry"] . $emlNl;
		}
		$ordShipType = $rs["ordShipType"];
		if($ordShipType != ""){
			$orderText .= $emlNl . $xxShpMet . ": " . $ordShipType;
			$orderText .= $emlNl . "Estimated Delivery Date: " . date("M j, Y",$delivery['deliveryDate']);
			if($delivery['extended'] > 0) {
				if($delivery['messageToUse'] == 'custom screen') {
					$orderText .= $emlNl . "Your order contains a custom screen. It should take an additional " . $delivery['extended'] . " business days to produce your custom screen before your order will ship.";
				}elseif($delivery['messageToUse'] == 'stock') {
					$orderText .= $emlNl . "Your order contains a back-ordered item. It should take an additional " . $delivery['extended'] . " business days before your order will ship.";
				}
				$orderText .= $emlNl . "Once your order ships it will ship via " . $delivery['method'] . " and will take " . $delivery['days_to_arrive'] . " business days to arrive.";
			}
			if(($rs["ordComLoc"] & 2)==2) $orderText .= $xxWtIns;
			$orderText .= $emlNl;
		}
		$ordAddInfo = trim($rs["ordAddInfo"]);
		if($ordAddInfo != ""){
			$orderText .= $emlNl . $xxAddInf . ": " . $emlNl;
			$orderText .= $ordAddInfo . $emlNl;
		}
		$ordTotal = $rs["ordTotal"];
		$ordDate = $rs["ordDate"];
		$ordStateTax = $rs["ordStateTax"];
		$ordDiscount = $rs["ordDiscount"];
		$ordDiscountText = $rs["ordDiscountText"];
		$ordCountryTax = $rs["ordCountryTax"];
		$ordHSTTax = $rs["ordHSTTax"];
		$ordShipping = $rs["ordShipping"];
		$ordHandling = $rs["ordHandling"];
		$affilID = trim($rs["ordAffiliate"]);
	}else{
		$orderText = "Cannot find customer details for order id: " . $sorderid . $emlNl;
		$sendstoreemail=FALSE;
		$sendcustemail=FALSE;
		$sendaffilemail=FALSE;
		$sendmanufemail=FALSE;
	}
	mysql_free_result($result);
	
	// DETERMINE WETHER TO SEND AN EMAIL TO THE DROPSHIPPER (Mike) FOR A GIFT CERTIFICATE
	$sql_hc = "SELECT co.coCartOption FROM orders o, cart c, cartoptions co
			   WHERE o.ordID = c.cartOrderID
			   AND c.cartID = co.coCartID
			   AND o.ordID = " . $sorderid;
	$res_hc = mysql_query($sql_hc);
	$ctr_email=0;
	$ctr_card=0;
	while($row_hc=mysql_fetch_assoc($res_hc)) {
		if($row_hc['coCartOption'] == 'Email') {
			$ctr_email++;
		}elseif($row_hc['coCartOption'] == 'Card via Mail') {
			$ctr_card++;
		}
	}
	if($ctr_email > 0) {
		if($ctr_card <= 0) {
			$sendmanufemail=FALSE;
		}
	}
	
	$google_form='';
	$showshipdate=FALSE;
	$saveCustomerDetails=$orderText;
	$orderText = $saveHeader . '%digidownloadplaceholder%' . $orderText;
	$sSQL = "SELECT cartID, cartProdId,cartProdName,cartProdPrice,cartQuantity,cartOrderID,p_iscert,pDropship".(@$digidownloads==TRUE?',pDownload':'')." FROM cart INNER JOIN products ON cart.cartProdId=products.pID WHERE cartOrderID='" . mysql_real_escape_string($sorderid) . "'";
	// Blake 12/21/06
	if(!empty($_SESSION['neworderID'])) $sSQL .= " OR cartOrderID='" . mysql_real_escape_string($_SESSION['neworderID']) . "'";
	$sSQL .= " ORDER BY pDropship";
	//
	//echo $sSQL;
	$result = mysql_query($sSQL) or print(mysql_error());
	$numProdsCart=mysql_num_rows($result);
	if($numProdsCart > 0){
		$old_orderID='';
		while($rs = mysql_fetch_assoc($result)){			
			if(trim($rs["pDownload"]) == "" && trim($rs["p_iscert"]) == 0) $showshipdate=TRUE;
			$localhasdownload=FALSE;
			if(@$digidownloads==TRUE)
			if(trim($rs["pDownload"]) != "") $localhasdownload=TRUE;			
			$saveCartItems='';
			if(!empty($_SESSION['neworderID']) && $old_orderID!=$rs["cartOrderID"]) $saveCartItems = $emlNl . $rs["cartOrderID"] . $emlNl;
			$saveCartItems .= "--------------------------" . $emlNl;
			$saveCartItems .= $xxPrId . ": " . $rs["cartProdId"] . $emlNl;
			$saveCartItems .= $xxPrNm . ": " . $rs["cartProdName"] . $emlNl;
			$saveCartItems .= $xxQuant . ": " . $rs["cartQuantity"] . $emlNl;
			$orderText .= $saveCartItems;
			//changed by blake 6-13-06
			$down_usr_pass='';
			if($localhasdownload==TRUE) {
				$sql_d="SELECT * FROM digitaldownloads WHERE orderID=".$rs["cartOrderID"]." AND type='".$rs["cartProdId"]."'";
					$result_d=mysql_query($sql_d);	
					$num_rows_d=mysql_num_rows($result_d);
					while($row_d=mysql_fetch_assoc($result_d)){
						$allready_has_download=TRUE;
						$down_usr_pass.=$emlNl.'License ID: '.$row_d['licenseID'].$emlNl;
						$down_usr_pass.='Password: '.$row_d['password'].$emlNl.$emlNl;
					}
					if($num_rows_d==0) $allready_has_download=FALSE; 
				for($i=0;$i<$rs["cartQuantity"];$i++) {				
					if(!$allready_has_download){
						$sql_download="SELECT * FROM digitaldownloads WHERE active=1 AND type='".$rs["cartProdId"]."'";
						$result_download=mysql_query($sql_download);
						$num_rows_download=mysql_num_rows($result_download);
						if($num_rows_download>0){
							$row_download=mysql_fetch_assoc($result_download);
							$down_usr_pass.=$emlNl.'License ID: '.$row_download['licenseID'].$emlNl;
							$down_usr_pass.='Password: '.$row_download['password'].$emlNl.$emlNl;
							$sql_update="UPDATE digitaldownloads SET active=0, orderID=".$sorderid." WHERE id=".$row_download['id'];
							mysql_query($sql_update);
						}
					}
				}
			$orderText .= $down_usr_pass;				
			}
			// end change
			
			$theoptions = "";
			$theoptionspricediff=0;
			$sSQL = "SELECT coOptGroup,coCartOption,coPriceDiff,optRegExp,coExtendShipping FROM cartoptions INNER JOIN options ON cartoptions.coOptID=options.optID WHERE coCartID=" . $rs["cartID"] . " ORDER BY coID";
			$result2 = mysql_query($sSQL) or print(mysql_error());
			while($rs2 = mysql_fetch_assoc($result2)){
				$theoptionspricediff += $rs2["coPriceDiff"];
				$optionline = (@$htmlemails==true?"<span style='margin-left: 15px;'>- ":"- - - ") . $rs2["coOptGroup"] . " : " . str_replace(array("\r\n","\n"),array($emlNl,$emlNl),$rs2["coCartOption"]) . ($htmlemails==true ? '</span>' : '');
				if(!empty($rs2["coExtendShipping"])){
					if($rs2["coExtendShipping"]>19999999) {
						$thisyeart=substr($rs2["coExtendShipping"],0,4);
						$thismontht=substr($rs2["coExtendShipping"],4,2);
						$thisdayt=substr($rs2["coExtendShipping"],6,2);
						$optionline .= " (PRE-ORDER. Order will ship after ".$thismontht.'-'.$thisdayt.'-'.$thisyeart.")";
					} else $optionline .= " (This option will extend your shipping time by ".$rs2["coExtendShipping"]." days)";
				}
				$theoptions .= $optionline;
				$saveCartItems .= $optionline . $emlNl;
				if($rs2["coPriceDiff"]==0 || @$hideoptpricediffs==TRUE)
					$theoptions .= $emlNl;
				else{
					$theoptions .= " (";
					if($rs2["coPriceDiff"] > 0) $theoptions .= "+";
					$theoptions .= FormatEmailEuroCurrency($rs2["coPriceDiff"]) . ")" . $emlNl;
				}
				if($rs2["optRegExp"] == "!!") $localhasdownload=FALSE;
			}
			$orderText .= $xxUnitPr . ": " . (@$hideoptpricediffs==TRUE ? FormatEmailEuroCurrency($rs["cartProdPrice"] + $theoptionspricediff) : FormatEmailEuroCurrency($rs["cartProdPrice"])) . $emlNl;
			
			$orderText .= $theoptions;
			if($rs["pDropship"] != 0){
				$index=0;
				for($index=0; $index<$ndropshippers; $index++){
					if($dropShippers[$index][0]==$rs["pDropship"]) break;
				}
				if($index>=$ndropshippers){
					$ndropshippers=$index+1;
					$dropShippers[$index][1]="";
				}
				$dropShippers[$index][0] = $rs["pDropship"];
				$dropShippers[$index][1] .= $saveCartItems;
			}
			if($localhasdownload==TRUE) $hasdownload=TRUE;
			$old_orderID=$rs["cartOrderID"];
			mysql_free_result($result2);
		}
		$orderText .= "--------------------------" . $emlNl;

		//HCS - DRE 03/02/05 Gift Cert Mod START
		if($certstr != "") {
			$certarray = explode("|",$certstr);
			for ($x = 0; $x < count($certarray);$x++) {
				$orderText .= "Gift Certificate Value/Code: ".$certarray[$x].$emlNl;
			}
			$orderText .= "You can enter the certificate code the next time you order or print a personalized gift certificate here. http://ifrogz.com/order_info.php Enter your email and order number to login. Once logged in click on 'Print Gift Certificates'. " . $emlNl;
			$orderText .= "--------------------------" . $emlNl;
		}elseif($certorder) {
			$orderText .= "Certificate Code(s) will be issued when Payment has been confirmed." . $emlNl;
			$orderText .= "--------------------------" . $emlNl;
		}
		//HCS - DRE 03/02/05 Gift Cert Mod STOP
		
		$orderText .= $xxOrdTot . " : " . FormatEmailEuroCurrency($ordTotal) . $emlNl;
		if($shipType != 0) $orderText .= $xxShippg . " : " . FormatEmailEuroCurrency($ordShipping) . $emlNl;
		if((double)$ordHandling!=0.0) $orderText .= $xxHndlg . " : " . FormatEmailEuroCurrency($ordHandling) . $emlNl;
		if((double)$ordDiscount!=0.0) $orderText .= $xxDscnts . " : " . FormatEmailEuroCurrency($ordDiscount) . $emlNl;
		if((double)$ordStateTax!=0.0) $orderText .= $xxStaTax . " : " . FormatEmailEuroCurrency($ordStateTax) . $emlNl;
		if((double)$ordCountryTax!=0.0) $orderText .= $xxCntTax . " : " . FormatEmailEuroCurrency($ordCountryTax) . $emlNl;
		if((double)$ordHSTTax!=0.0) $orderText .= $xxHST . " : " . FormatEmailEuroCurrency($ordHSTTax) . $emlNl;
		$ordGrandTotal = ($ordTotal+$ordStateTax+$ordCountryTax+$ordHSTTax+$ordShipping+$ordHandling)-$ordDiscount;
		
		//HCS - DRE 03/07/05 Gift Cert Mod START
		if($pendingamt > 0) {
			$ordGrandTotal -= $pendingamt;
			if($ordGrandTotal == 0) {
				$strsql = "UPDATE orders SET ordStatus=3 WHERE ordID =".$ordID;
				mysql_query($strsql);
			}
	
			$orderText .= "Gift Certificate Amount Applied to Order: " . FormatEmailEuroCurrency($pendingamt) ." (ending in ".$certend.")". $emlNl;
		}
		//HCS - DRE 03/07/05 Gift Cert Mod STOP
		
		$orderText .= $xxGndTot . " : " . FormatEmailEuroCurrency($ordGrandTotal) . $emlNl;
		
		//HCS - DRE 03/06/05 Gift Cert Mod START
		if($pendingamt > 0) {
			if ($remainbal > 0) {
				$orderText .= $emlNl."--------------------------".$emlNl;
				$orderText .= "Amount Unused on Gift Certificate: " . FormatEmailEuroCurrency($remainbal) ." (ending in ".$certend.")". $emlNl;
				$orderText .= "This Gift Certificate will Expire On: " . date("M j, Y", $certexpdt) . $emlNl;
			}else{
				$orderText .= $emlNl."--------------------------".$emlNl;			
				$orderText .= "You have exhausted all funds on this Gift Certificate " . $emlNl;
			}
		}
		//HCS - DRE 03/06/05 Gift Cert Mod STOP
		
		eval('global $emailfooter' . $payprovid . ';$emailheader = @$emailfooter' . $payprovid . ';');
		if(@$emailheader != "") $orderText .= $emailheader;
		if(@$emailfooter != "") $orderText .= $emailfooter;                                                                                                                                                                                                                                        
	}else{
		$orderText .= "Cannot find order details for order id: " . $sorderid . $emlNl;
	}	
	mysql_free_result($result);
	if($hasdownload==TRUE && @$digidownloademail != ""){
		$fingerprint = vrhmac($digidownloadsecret, $sorderid . $ordAuthNumber . $ordSessionID);
		$fingerprint = substr($fingerprint, 0, 14);
		$digidownloademail = str_replace('%orderid%',$ordID,$digidownloademail);
		$digidownloademail = str_replace('%password%',$fingerprint,$digidownloademail);
		$digidownloademail = str_replace('%nl%',$emlNl,$digidownloademail);
		$orderEmailText = str_replace('%digidownloadplaceholder%',$digidownloademail,$orderText);
	} else {
		$orderEmailText = str_replace('%digidownloadplaceholder%',"",$orderText);
	}
	$orderText = str_replace('%digidownloadplaceholder%',"",$orderText);
	if($sendstoreemail){
		$headers = str_replace('%from%',$sEmail,$customheaders);
		$headers = str_replace('%to%','orders@ifrogz.com',$headers);
		mail('orders@ifrogz.com', $xxOrdStr, $orderEmailText, $headers);
	}
	// And one for the customer
	if($sendcustemail){
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$headers = str_replace('%from%',$sEmail,$customheaders);
		$headers = str_replace('%to%',$custEmail,$headers);
		$strdaterange='';
		if($showshipdate) {
			$strrange=$xxTouSooShipping[$ordShipType];
			$strdaterange=str_replace("%date_range%",$strrange,$xxTouSooDate);
		}
		$strfinal=str_replace("%dateorno%",$strdaterange,$xxTouSoo);
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Mail services
        $mailer = new Mailer('AMAZON_IFROGZ', 'GMAIL', 'SEND_MAIL');

        // Sender
        $mailer->set_sender(new EmailAddress($sEmail));
        $mailer->set_reply_to(new EmailAddress('support@ifrogz.com', 'iFrogz Customer Service'));

        // Recepients
		$mailer->add_recipient(new EmailAddress($custEmail));

		// Subject
		$mailer->set_subject($xxTnxOrd);
        
        // Message
        $mailer->set_content_type('text/html');
        $mailer->set_message($strfinal . $emlNl . $emlNl . $orderEmailText);

        // Send
        $mailer->send();
	}
	// Drop Shippers
	if($sendmanufemail){
		for($index=0; $index < $ndropshippers; $index++){
			if(@$dropshipsubject=="") $dropshipsubject="We have received the following order";
			$sSQL = "SELECT dsEmail,dsAction FROM dropshipper WHERE dsID=" . $dropShippers[$index][0];
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_assoc($result)){
				if(($rs["dsAction"] & 1)==1 || $sendmanufemail==2){
					$saveHeader = "";
					$saveFooter = "";
					$saveHeader .= @$dropshipheader;
					eval('global $dropshipheader' . $dropShippers[$index][0] . ';$emailheader = @$dropshipheader' . $dropShippers[$index][0] . ';');
					if($emailheader != "") $saveHeader .= $emailheader;
					eval('global $dropshipfooter' . $dropShippers[$index][0] . ';$saveFooter = @$dropshipfooter' . $dropShippers[$index][0] . ';');
					$saveFooter .= @$dropshipfooter;
					$headers = str_replace('%from%','orders@ifrogz.com',$customheaders);
					$headers = str_replace('%to%','dropship@ifrogz.com',$headers);
					mail('dropship@ifrogz.com', $dropshipsubject, $saveHeader . $saveCustomerDetails . $dropShippers[$index][1] . $saveFooter, $headers);
					//mail('dropship@ifrogz.com', 'A dropship order has been placed on ifrogz', $saveHeader . $saveCustomerDetails . $dropShippers[$index][1] . $saveFooter, $headers);
				}
			}
		}
	}
	if($sendaffilemail){
		if($affilID != ""){
			$sSQL = "SELECT affilEmail,affilInform FROM affiliates WHERE affilID='" . mysql_real_escape_string($affilID) . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_assoc($result)){
				if((int)$rs["affilInform"]==1){
					$affiltext = $xxAff1 . " " . FormatEmailEuroCurrency($ordTotal-$ordDiscount) . ".".$emlNl.$emlNl.$xxAff2.$emlNl.$emlNl.$xxThnks.$emlNl;
					$headers = str_replace('%from%',$sEmail,$customheaders);
					$headers = str_replace('%to%',trim($rs["affilEmail"]),$headers);
					mail(trim($rs["affilEmail"]), $xxAff3, $emlNl . $affiltext, $headers);
				}
			}
			mysql_free_result($result);
		}
	}
	if($doshowhtml){
?>
     <style type="text/css">
	 <!--
	 
	 -->
	 </style>
	 <div id="cart_nav"><img  src="/lib/images/new_images/subnav_gray_13.gif" alt="View Order" /><img src="/lib/images/new_images/subnav_gray_15.gif" alt="Customer Info" /><img src="/lib/images/new_images/subnav_gray_17.gif" alt="Final Review" /><img src="/lib/images/new_images/subnav_gray_19.gif" alt="Confirmation" /><img src="/lib/images/new_images/subnav_green_21.gif" alt="View Receipt" /></div>
	  <table border="0" cellspacing="<?php print $maintablespacing?>" cellpadding="<?php print $maintablepadding?>" width="<?php print $maintablewidth?>" bgcolor="<?php print $maintablebg?>" align="center">
        <tr>
          <td width="100%">
            <table width="<?php print $innertablewidth?>" border="0" cellspacing="<?php print $innertablespacing?>" cellpadding="<?php print $innertablepadding?>" bgcolor="<?php print $innertablebg?>">
		<?php	//if(@$digidownloads!=TRUE){ ?>
			  <tr> 
                <td width="100%" align="left">
					<h2>Order Placed Successfully</h2>
					<?php	if(@$digidownloads && $hasdownload) echo '<div style="font-size: 16px;color:#9F0000;font-weight:bold;">Please scroll to the bottom of the page to download your software.</div>';?>

					<div style="margin-bottom: 10px; padding: 0">
						<p style="font-size: 16px"><span style="font-weight:bold">Thank You for shopping
								at ifrogz!</span> The ordering process is now complete. Please
								read the following information about your order.</p>
					</div>
					<div style="border: 1px solid #888; padding: 0; margin-top: 0">
						<div style="float:left; width: 400px; margin: 20px 0 20px 20px">
							<h2>YOUR ORDER
								HAS BEEN RECEIVED.</h2>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">Your order has been
								received and will be processed as soon as payment is verified.</p>
							<? if(!empty($_SESSION['neworderID'])){?> 
							<h2>Your Order Will Be Shipped in Two Shipments.  Order Numbers are</h2>
							<h3><?=$orderID2?>,<?=$_SESSION['neworderID']?></h3>
							<? } else { ?>
							<h2>Your Order Numbers is</h2>
							<h3><?=$orderID2?></h3>
							<? } ?>
							<? if(!empty($_SESSION['neworderID'])){?>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">We will send your products in two separate orders because they will be shipped from two of our warehouses. Please make a note
								of these number in the event that you need to contact us about your orders.</p>
							<? } else { ?>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">Please make a note
								of this number in the event that you need to contact us about your order.</p>
							<? } ?>
							<h2>Your Delivery Date</h2>
							<h3><!--<span style="color: #000;">-->
								Delivery Date:<!--</span>--> <?=date("M j, Y",$delivery['deliveryDate'])?></h3>
					<?php
					if($delivery['extended'] > 0) {
						if($delivery['messageToUse'] == 'custom screen') {
					?>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">Your order contains a custom screen. It will
							take an additional <?=$delivery['extended']?> business days to produce your custom screen before your 
							order will ship.  When it ships, your order will ship via <?=$delivery['method']?> and will take 
							<?=$delivery['days_to_arrive']?> business days to arrive.</p>
					<?php
						}elseif($delivery['messageToUse'] == 'stock') {
					?>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">Your order contains a back-ordered item. It will
							take an additional <?=$delivery['extended']?> business days before your order will ship. When it ships, 
							it will ship via <?=$delivery['method']?> and will take <?=$delivery['days_to_arrive']?> to arrive.</p>
					<?php
						}
					}
					?>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">You should receive your 
								order in the mail by this date.  You will also receive an e-mail with your tracking 
								number once we get your tracking number.</p>
						</div>
						<div style="float:right; width: 400px; margin: 20px 20px 4px 0">
							<h2>Your Receipt</h2>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">You have been sent
								an e-mail to the address you provided containing your order information.
								If you would like an invoice, you can print one from our <a href="http://www.ifrogz.com/order_info.php" style="font-size: 14px">invoice</a> page.</p>
							<h2>Your Order Status</h2>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">We keep track of
								your order in realtime! If you would like to know what the status of your
								order is, visit our <a href="http://www.ifrogz.com/order_info.php" style="font-size: 14px">tracking</a> 
								page.</p>
							<h2>We're Here for You</h2>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">ifrogz will
								provide you with the best service possible. If you have any questions or
								concerns regarding your order, please contact us at support@ifrogz.com
								or at the phone number below.</p>
							<h2 style="text-align: left; margin:0 0 10px 0;">Your Shopping Cart Has Been
								Emptied</h2>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px">The items you purchased
								have been removed.</p>
							<!--				<h2 style="text-align: left; margin:0 0 10px 0;">Want Individual Bands?</h2>
							<p style="margin:0 0 10px 0; padding:0; font-size: 14px"><a href="http://bandsonhand.com" title="Bands On Hand">BandsonHand.com</a> carries 100% silicone bracelets in-stock so you don't have to wait. Choose a category and find a band that meets your needs. Order individual bands or groups of bands. All our bands are in-stock and guaranteed to ship the next business day after you order.</p>
			-->
						</div>
						<div style="clear: both"></div>
					</div>
					
					
                </td>
			  </tr>
		<?php	//} ?>
			</table>
		  </td>
        </tr>
      </table>
<?php
 	}
/* print $google_form; */
} 


?>