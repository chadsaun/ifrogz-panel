<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$maxcatsperpage = 100;
if(@$maxloginlevels=="") $maxloginlevels=5;
$sSQL = "";
$alldata = "";
$alreadygotadmin = getadminsettings();

//get filter types
$sql="SELECT * FROM filter_types";
$result=mysql_query($sql);
$i=0;
$filter_types='';
while($row=mysql_fetch_assoc($result)){
	$filter_types[$i]['id']=$row['id'];
	$filter_types[$i]['type']=$row['type_name'];
	$filter_types[$i]['display']=$row['display_name'];
	$i++;
}

if(empty($ftID)) $ftID=$filter_types[0]['id'];
//echo '$ftID='.$ftID;
//get filter options
if(!empty($ftID)) {
	$sql="SELECT id,filter_option FROM filter_options WHERE ftID=".$ftID;
	$result=mysql_query($sql);
	$i=0;
	$filter_options='';
	while($row=mysql_fetch_assoc($result)){
		$filter_options[$i]['id']=$row['id'];
		$filter_options[$i]['option']=$row['filter_option'];
		$i++;
	}
}
?>
<table width="100%" cellspacing="0" cellpadding="4">
  
  <tr>
    <td><strong>Add Filter Type</strong></td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>
	<form action="/admin/filterprocess.php" method="post" name="addfilter">	
	Filter Name: <input name="addfiltertype" id="addfiltertype" type="text" /><br />
	Display Name: <input name="addfilterdisplayname" id="addfilterdisplayname" type="text" /><br />
	<input name="btnaddfilter" type="submit" value="go" /></form></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Add Option</strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<form action="/admin/filterprocess.php" method="post" name="frmaddoptions">
	  Filter: 
      <select name="ftID" onchange="this.form.submit();">
        <? for($i=0;$i<count($filter_types);$i++) { ?><option value="<?=$filter_types[$i]['id']?>" <?php if (!(strcmp($filter_types[$i]['id'], $ftID))) {echo "selected=\"selected\"";} ?>>
      <?=$filter_types[$i]['type']?>
        </option>
        <? } ?>
      </select><br />
      Add Option:
      <input type="text" name="addoption" id="addoption" />
	  <input name="btnaddoption" type="submit" id="btnaddoption" value="go" /><br /><br />
        
		<table width="300" cellspacing="0" cellpadding="2">
		  <tr>
			<th style="border-bottom:solid #999999 1px;"><div align="left">Option</div></th>
			<td style="border-bottom:solid #999999 1px;">&nbsp;</td>
		  </tr>
		  <? for($i=0;$i<count($filter_options);$i++) { ?>
		  <tr>
			<td><?=$filter_options[$i]['option']?></td>
			<td align="right"><a href="/admin/filterprocess.php?delete=delete&id=<?=$filter_options[$i]['id']?>">delete</a></td>
		  </tr>
		  <? } ?>
		</table>
    </form>	
	</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><label for="textfield"></label></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><label for="textfield"></label></td>
    <td>&nbsp;</td>
  </tr>
</table>


 
