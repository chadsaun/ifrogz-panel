<?php 
function updateBackorders($id){
	// GET ALL ORDERS THAT ARE BACKORDERED WITH THIS OPTION IN THEM
	// CHECK THAT EXTEND SHIPPING IS NOT BLANK
	// CHECK THAT STOCK LEVEL IS ABOVE THE MIN THRESHOLD
	$qry_back = "SELECT o.ordID
				 FROM orders o, cart c, cartoptions co, options opt
				 WHERE o.ordID = c.cartOrderID
				 AND c.cartID = co.coCartID
				 AND co.coOptID = opt.optID
				 AND o.ordStatus = 4
				 AND co.coExtendShipping != ''
				 AND opt.optStock > opt.optMin
				 AND opt.optID = ".$id."
				 ORDER BY ordDate ASC";
	$res_back = mysql_query($qry_back) or print(mysql_error());
	//echo $qry_back;
	while($rw_back = mysql_fetch_assoc($res_back)) {
		// CHECK ALL THE OTHER OPTIONS OF THE ORDER AND MAKE SURE ALL THE EXTEND SHIPPING ARE BLANK
		// IF NOT, DON'T AUTHORIZE IT
		$sql = "SELECT opt.*, co.coExtendShipping 
				FROM cart c, cartoptions co, options opt 
				WHERE c.cartID = co.coCartID 
				AND co.coOptID = opt.optID
				AND c.cartOrderID = ".$rw_back['ordID'];
		$res_sql = mysql_query($sql);
		
		$authorize = true;
		
		for($k=0; $rw_res = mysql_fetch_assoc($res_sql); $k++) {
			if($rw_res['optStock'] <= $rw_res['optMin']) $authorize = false;
		}
		
		if($authorize) {
			$qry_auth = "UPDATE orders
						 SET ordStatus = 3
						 WHERE ordID = ".$rw_back['ordID'];
			$res_auth = mysql_query($qry_auth) or print(mysql_error());
			
			if(!setNewLocation( 3 , $row['ordID'] ,'Automatic','Update Backorders' )) print("Unable to record status change.");
		}
	}
}
?>