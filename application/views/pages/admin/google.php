<?php
function googleAnalytics($orderID) {
	global $google_form;
	$sSQL = "SELECT ordID,ordName,ordAddress,ordAddress2,ordCity,ordState,ordZip,ordCountry,ordEmail,ordPhone,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipZip,ordShipCountry,ordPayProvider,ordAuthNumber,ordTotal,ordDate,ordStateTax,ordCountryTax,ordHSTTax,ordHandling,ordShipping,ordAffiliate,ordDiscount,ordDiscountText,ordComLoc,ordExtra1,ordExtra2,ordSessionID,ordAddInfo,ordShipType,payProvID,ord_cert_amt,ordCoupon FROM orders LEFT JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordAuthNumber<>'' AND ordID='" . mysql_real_escape_string($orderID) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result) > 0){
			$rs = mysql_fetch_assoc($result);
			$google_city=$rs["ordCity"];
			$google_state=$rs["ordState"];
			$google_country=$rs["ordCountry"];
			$google_postal_code=$rs["ordZip"];
			$google_name=$rs["ordName"];
			$google_address=$rs["ordAddress"];
			$google_address2=$rs["ordAddress2"];
			if(trim($rs["ordShipName"]) != "" || trim($rs["ordShipAddress"]) != ""){
				$google_city=$rs["ordShipCity"];
				$google_state=$rs["ordShipState"];
				$google_country=$rs["ordShipCountry"];
				$google_postal_code=$rs["ordShipZip"];
				$google_name=$rs["ordShipName"];
				$google_address=$rs["ordShipAddress"];
				$google_address2=$rs["ordShipAddress2"];
			}
			$ordPhone = $rs["ordPhone"];
			$ordEmail = $rs["ordEmail"];
			$ordTotal = $rs["ordTotal"];
			$ordDate = $rs["ordDate"];
			$ordStateTax = $rs["ordStateTax"];
			$ordDiscount = $rs["ordDiscount"];
			$ordDiscountText = $rs["ordDiscountText"];
			$ordCountryTax = $rs["ordCountryTax"];
			$ordHSTTax = $rs["ordHSTTax"];
			$ordShipping = $rs["ordShipping"];
			$ordHandling = $rs["ordHandling"];
			$affilID = trim($rs["ordAffiliate"]);
			$certAmt = $rs["ord_cert_amt"];
			// check for free shipping discount
			$hasfreeshipping=FALSE;
			$sql="SELECT * FROM coupons WHERE cpnID IN ('".str_replace(",","','",$rs["ordCoupon"])."') AND cpnType=0";
			$res=mysql_query($sql);
			if(mysql_num_rows($res)>0) $hasfreeshipping=TRUE;
		}
	mysql_free_result($result);
	
	$ordGrandTotal = ($ordTotal+$ordStateTax+$ordCountryTax+$ordHSTTax+$ordShipping+$ordHandling)-$ordDiscount-$certAmt;
	
	// Order affiliate totals
	if($hasfreeshipping) $ordAffilTotal= $ordTotal+$ordShipping-$ordDiscount-$certAmt;
	else $ordAffilTotal= $ordTotal-$ordDiscount-$certAmt;
	//end 
	$productIDs='';
	$qtys='';
	$prices='';
	$items='';	
	$sSQL = "SELECT cartProdID,cartProdName,cartProdPrice,cartQuantity,cartID,pDropship".(@$digidownloads==TRUE?',pDownload':'')." FROM cart INNER JOIN products ON cart.cartProdId=products.pID WHERE cartOrderID='" . mysql_real_escape_string($orderID) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result) > 0){
		$google_form='<form style="display:none;" name="utmform">'."\r\n".'<textarea id="utmtrans">';
		$google_form.='UTM:T|'.$orderID.'|'.$affilID.'|'.$ordGrandTotal.'|'.$ordStateTax.'|'.$ordShipping.'|'.$google_city.'|'.$google_state.'|'.$google_country."\r\n"; 
		$num_items=0;
		while($rs = mysql_fetch_assoc($result)){			
			$theoptions = "";
			$sSQL = "SELECT coOptGroup,coCartOption,coPriceDiff,optRegExp FROM cartoptions INNER JOIN options ON cartoptions.coOptID=options.optID WHERE coCartID=" . $rs["cartID"] . " ORDER BY coID";
			$result2 = mysql_query($sSQL) or print(mysql_error());
			$google_optionline='';
			while($rs2 = mysql_fetch_assoc($result2)){
				$theoptionspricediff += $rs2["coPriceDiff"];
				if($google_optionline=='')$google_optionline .= $rs2["coOptGroup"] . "-" . $rs2["coCartOption"];
				else $google_optionline .=','.$rs2["coOptGroup"] . "-" . $rs2["coCartOption"];
			}
			$google_price=(@$hideoptpricediffs==TRUE ? ($rs["cartProdPrice"] + $theoptionspricediff) : ($rs["cartProdPrice"]));
			$google_form.='UTM:I|'.$orderID.'|'.$rs["cartProdID"].'|'.$rs["cartProdName"].'|'.$google_optionline.'|'.$google_price.'|'.$rs["cartQuantity"]."\r\n";
			if($qtys=='')$qtys="'".$rs["cartQuantity"]."'";
			else $qtys.=",'".$rs["cartQuantity"]."'";
			if($prices=='')$prices="'".$google_price."'";
			else $prices.=",'".$google_price."'";
			if($productIDs=='')$productIDs="'".$rs["cartProdID"]."'";
			else $productIDs.=",'".$rs["cartProdID"]."'";
			if($items=='')$items="'".$rs["cartProdName"].':'.$google_optionline."'";
			else $items.=",'".$rs["cartProdName"].':'.$google_optionline."'";
			$num_items++;
			mysql_free_result($result2);
		}
		$google_form.='</textarea>'."\r\n".'</form>';
	}	
	mysql_free_result($result);
	//print $google_form."\r\n";

	print '<!-- Google Code for Purchase Conversion Page -->
	<script language="JavaScript" type="text/javascript">
	<!--
	var google_conversion_id = 1061416320;
	var google_conversion_language = "en_US";
	var google_conversion_format = "1";
	var google_conversion_color = "FFFFFF";
	if (1.0) {
	  var google_conversion_value = '.$ordGrandTotal.';
	}
	var google_conversion_label = "Purchase";
	//-->
	</script>
	
	<script language="JavaScript" src="https://www.googleadservices.com/pagead/conversion.js">
	</script>
	
	<img height=1 width=1 border=0 src="https://www.googleadservices.com/pagead/conversion/1061416320/?value='.$ordGrandTotal.'&label=Purchase&script=0">
	
	<!-- Begin Tracking Data  -->
	<script language="JavaScript">
	<!--
		/* ROI Tracking Data */
		var mid            = "141657";
		var cust_type      = "";
		var order_value    = "'.$ordGrandTotal.'";
		var order_id       = "'.$orderID.'";
		var units_ordered  = "";
	//-->
	</script>

	
	<script language="JavaScript">
	var merchant_id = "418373";
	</script>
	
	<SCRIPT language="JavaScript" type="text/javascript">
	<!-- Yahoo! Inc.
	window.ysm_customData = new Object();
	window.ysm_customData.conversion = "transId='.$orderID.',currency=,amount='.$ordGrandTotal.'";
	var ysm_accountid  = "19CS6Q5FV7HOGJG6HJF8DE5JO2O";
	document.write("<SCR" + "IPT language=\'JavaScript\' type=\'text/javascript\' " 
	+ "SRC=//" + "srv3.wa.marketingsolutions.yahoo.com" + "/script/ScriptServlet" + "?aid=" + ysm_accountid 
	+ "></SCR" + "IPT>");
	// -->
	</SCRIPT>
	
	<!-- MSN Tacking Code -->
	<SCRIPT>
		microsoft_adcenterconversion_domainid = 110512;
	 	microsoft_adcenterconversion_cp = 5050;
	</script>
	<SCRIPT	SRC="https://0.r.msn.com/scripts/microsoft_adcenterconversion.js"></SCRIPT>
	<NOSCRIPT><IMG width=1 height=1	SRC="https://110512.r.msn.com/?type=1&cp=1"/></NOSCRIPT>
	<!-- End MSN Tacking Code -->'	
	."\r\n";

	
	if(empty($_SESSION['clientUser']) && (($ordAffilTotal)>0)) {
		print '<script language="JavaScript" src="https://stat.DealTime.com/ROI/ROI.js?mid=418373"></script>
			
		<img src="https://shareasale.com/sale.cfm?amount='.($ordAffilTotal).'&tracking='.$orderID.'&transtype=SALE&merchantID=10446" width="1" height="1" />
	
		<!-- End Tracking Data -->
		';		
	}
}
?>

