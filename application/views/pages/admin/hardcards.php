<?php
//^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$
if($_SESSION["loggedon"] != "virtualstore") exit();

/*
session_register("form_error");
session_register("form");
session_register("viewed");
session_register("viewed_form");*/


if(!empty($_GET['cert_id'])) {
	// GET THE CERTIFICATE'S INFORMATION
	$sql = "SELECT * FROM certificates WHERE cert_id = " . $_GET['cert_id'];
	$res = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	
?>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function goBack() {
	window.location = '<?=$_SERVER['PHP_SELF']?>';
}
-->
</script>
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr bgcolor="#030133">
			<td colspan="6" style="color: #E7EAEF; font-weight: bold">Viewable Fields</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Batch #:</td>
			<td><?=$row['batch_number']?></td>
			<td align="right">Sequential #:</td>
			<td><?=$row['sequential']?></td>
			<td align="right">Code:</td>
			<td><?=$row['cert_code']?></td>
		</tr>
	</table>
	<p style="margin: 0;">&nbsp;</p>
	
	<form id="mainform" name="mainform" method="post" action="<?=$_SERVER['PHP_SELF']?>">
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr bgcolor="#030133">
			<td colspan="6" style="color: #E7EAEF; font-weight: bold">Editable Fields</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Group Name:</td>
			<td><input type="text" id="cert_group" name="cert_group" value="<?=$row['cert_group']?>" /></td>
			<td align="right">Exp. Date:</td>
			<td><input type="text" id="cert_exp_dt" name="cert_exp_dt" value="<?=date("Y-m-d",$row['cert_exp_dt'])?>" />&nbsp;<a href="/includes/calendar.php?form_date=mainform.cert_exp_dt" onClick="window.open(this.href,'Calendar','right=200,top=240,width=212,height=200'); return false;"><img src="/lib/images/calendar.gif" align="absmiddle" width="16" height="15" style="border: none;" /></a></td>
			<td align="right">Prod ID:</td>
			<td width="280">
				<select id="cert_prod_id" name="cert_prod_id">
			<?php
			$sql_prod = "SELECT * FROM products WHERE p_iscert > 0 ORDER BY pID";
			$res_prod = mysql_query($sql_prod) or print(mysql_error());
			while($row_prod=mysql_fetch_assoc($res_prod)) {
				$selected = '';
				if($row['cert_prod_id'] == $row_prod['pID']) {
					$selected = ' selected="selected"';
				}
			?>
					<option value="<?=$row_prod['pID']?>"<?=$selected?>><?=$row_prod['pID']?></option>
			<?php
			}
			?>
				</select>
			</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">E-mail:</td>
			<td><input type="text" id="cert_email" name="cert_email" value="<?=$row['cert_email']?>" /></td>
			<td align="right">Amount:</td>
			<td><input type="text" id="cert_amt" name="cert_amt" value="<?=$row['cert_amt']?>" /></td>
			<td align="right">Order ID:</td>
			<td><input type="text" id="cert_order_id" name="cert_order_id" value="<?=$row['cert_order_id']?>" /></td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Pend. Order Amt:</td>
			<td><input type="text" id="pend_order_amt" name="pend_order_amt" value="<?=$row['pend_order_amt']?>" /></td>
			<td align="right">Pend. Order ID:</td>
			<td><input type="text" id="pend_order_id" name="pend_order_id" value="<?=$row['pend_order_id']?>" /></td>
			<td align="right">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td colspan="6" align="right">
				<input type="submit" id="modify_cert" name="modify_cert" value="Modify Certificate" />
				<input type="hidden" id="cert_id" name="cert_id" value="<?=$row['cert_id']?>" />
				<input type="button" id="go_back" name="go_back" value="Go Back" onclick="goBack();" />
			</td>
		</tr>
	</table>
	<p>&nbsp;</p>
<?php
}elseif(!empty($_POST['modify_cert'])) {
	$_SESSION['form_error'] = false;
	foreach($_POST as $key => $value) {
		$_SESSION['form'][$key] = false;
	}

	// VALIDATE FIELDS
	if(!empty($_POST['cert_amt'])) {
		if(!ereg("^([0-9]+)+(\.[0-9]+)?$",$_POST['cert_amt'])) {
			$_SESSION['form_error'] = true;
			$_SESSION['form']['cert_amt'] = true;
		}
	}
	
	if(!empty($_POST['pend_order_amt'])) {
		if(!ereg("^([0-9]+)+(\.[0-9]+)?$",$_POST['pend_order_amt'])) {
			$_SESSION['form_error'] = true;
			$_SESSION['form']['pend_order_amt'] = true;
		}
	}

	if(!empty($_POST['cert_order_id'])) {
		if(!ereg("^[0-9]+$",$_POST['cert_order_id'])) {
			$_SESSION['form_error'] = true;
			$_SESSION['form']['cert_order_id'] = true;
		}
	}

	if(!empty($_POST['pend_order_id'])) {
		if(!ereg("^[0-9]+$",$_POST['pend_order_id'])) {
			$_SESSION['form_error'] = true;
			$_SESSION['form']['pend_order_id'] = true;
		}
	}
	
	if(!empty($_POST['cert_exp_dt'])) {
		if(!ereg("^[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}:[0-9]{2})?$",$_POST['cert_exp_dt'])) {
			$_SESSION['form_error'] = true;
			$_SESSION['form']['cert_exp_dt'] = true;
		}
	}

	if(!$_SESSION['form_error']) {
		// UPDATE EVERYTHING
		$sql = "UPDATE certificates
				SET cert_group = '".E($_POST['cert_group'])."', cert_exp_dt = '".strtotime($_POST['cert_exp_dt'])."',
					cert_prod_id = '".E($_POST['cert_prod_id'])."', cert_email = '".E($_POST['cert_email'])."',
					cert_amt = '".E($_POST['cert_amt'])."', cert_order_id = '".E($_POST['cert_order_id'])."',
					pend_order_amt = '".E($_POST['pend_order_amt'])."', pend_order_id = '".E($_POST['pend_order_id'])."'
				WHERE cert_id = " . $_POST['cert_id'];
		//echo $sql;
		$res = mysql_query($sql) or print(mysql_error());
		
		$_SESSION['viewed'] = '';
	}
	
	// GET THE CERTIFICATE'S INFORMATION
	$sql = "SELECT * FROM certificates WHERE cert_id = " . $_POST['cert_id'];
	$res = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
?>
	<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
	<script language="JavaScript" type="text/javascript">
	<!--
	
	function goBack() {
		window.location = '<?=$_SERVER['PHP_SELF']?>';
	}
	
	-->
	</script>
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr bgcolor="#030133">
			<td colspan="6" style="color: #E7EAEF; font-weight: bold">Viewable Fields</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Batch #:</td>
			<td><?=$row['batch_number']?></td>
			<td align="right">Sequential #:</td>
			<td><?=$row['sequential']?></td>
			<td align="right">Code:</td>
			<td><?=$row['cert_code']?></td>
		</tr>
	</table>
	<p style="margin: 0;">&nbsp;</p>
	
	<form id="mainform" name="mainform" method="post" action="<?=$_SERVER['PHP_SELF']?>">
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr bgcolor="#030133">
			<td colspan="6" style="color: #E7EAEF; font-weight: bold">Editable Fields</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Group Name:</td>
			<td><input type="text" id="cert_group" name="cert_group" value="<?=$row['cert_group']?>" /></td>
			<td align="right">Exp. Date:</td>
			<td><input type="text" id="cert_exp_dt" name="cert_exp_dt" value="<?=date("Y-m-d",$row['cert_exp_dt'])?>" />&nbsp;<a href="/includes/calendar.php?form_date=mainform.cert_exp_dt" onClick="window.open(this.href,'Calendar','right=200,top=240,width=212,height=200'); return false;"><img src="/lib/images/calendar.gif" align="absmiddle" width="16" height="15" style="border: none;" /></a></td>
			<td align="right">Prod ID:</td>
			<td width="280">
				<select id="cert_prod_id" name="cert_prod_id">
			<?php
			$sql_prod = "SELECT * FROM products WHERE p_iscert > 0 ORDER BY pID";
			$res_prod = mysql_query($sql_prod) or print(mysql_error());
			while($row_prod=mysql_fetch_assoc($res_prod)) {
				$selected = '';
				if($row['cert_prod_id'] == $row_prod['pID']) {
					$selected = ' selected="selected"';
				}
			?>
					<option value="<?=$row_prod['pID']?>"<?=$selected?>><?=$row_prod['pID']?></option>
			<?php
			}
			?>
				</select>
			</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">E-mail:</td>
			<td><input type="text" id="cert_email" name="cert_email" value="<?=$row['cert_email']?>" /></td>
			<td align="right">Amount:</td>
			<td><input type="text" id="cert_amt" name="cert_amt" value="<?=$row['cert_amt']?>" /></td>
			<td align="right">Order ID:</td>
			<td><input type="text" id="cert_order_id" name="cert_order_id" value="<?=$row['cert_order_id']?>" /></td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Pend. Order Amt:</td>
			<td><input type="text" id="pend_order_amt" name="pend_order_amt" value="<?=$row['pend_order_amt']?>" /></td>
			<td align="right">Pend. Order ID:</td>
			<td><input type="text" id="pend_order_id" name="pend_order_id" value="<?=$row['pend_order_id']?>" /></td>
			<td align="right">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td colspan="6" align="right">
				<input type="submit" id="modify_cert" name="modify_cert" value="Modify Certificate" />
				<input type="hidden" id="cert_id" name="cert_id" value="<?=$row['cert_id']?>" />
				<input type="button" id="go_back" name="go_back" value="Go Back" onclick="goBack();" />
			</td>
		</tr>
	</table>
	<p>&nbsp;</p>
<?php
}elseif($_GET['action'] == 'modifySelected') {
	$certs_csv = '';
	$comma = '';
	foreach($_GET as $key => $value) {
		if(strstr($key,'cert')) {
			$certs_csv .= $comma.$value;
			$comma = ',';
		}
	}
	
	$aFieldsInCommon = array();
	$aFields = array('batch_number','sequential','cert_group','cert_exp_dt','cert_prod_id','cert_email','cert_amt','cert_order_id','pend_order_amt','pend_order_id');
	foreach($aFields as $field) {
		$sql = "SELECT DISTINCT($field) as value FROM certificates WHERE cert_id IN($certs_csv)";
		$res = mysql_query($sql) or print(mysql_error());
		
		if(mysql_num_rows($res) == 1) {
			$row = mysql_fetch_assoc($res);
			$aFieldsInCommon[$field] = $row['value'];
		}
	}
	
?>
	<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
	<script language="JavaScript" type="text/javascript">
	<!--
	
	function goBack() {
		window.location = '<?=$_SERVER['PHP_SELF']?>';
	}
	
	-->
	</script>
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr bgcolor="#030133">
			<td colspan="6" style="color: #E7EAEF; font-weight: bold">Viewable Fields</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Batch #:</td>
			<td><?=$aFieldsInCommon['batch_number']?></td>
			<td align="right">Sequential #:</td>
			<td><?=$aFieldsInCommon['sequential']?></td>
			<td align="right">Code:</td>
			<td><?=$aFieldsInCommon['cert_code']?></td>
		</tr>
	</table>
	<p style="margin: 0;">&nbsp;</p>
	
	<form id="mainform" name="mainform" method="post" action="/admin/hardcardsprocess.php">
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr bgcolor="#030133">
			<td colspan="6" style="color: #E7EAEF; font-weight: bold">Editable Fields</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Group Name:</td>
			<td><input type="text" id="cert_group" name="cert_group" value="<?=$aFieldsInCommon['cert_group']?>" /></td>
			<td align="right">Exp. Date:</td>
			<td><input type="text" id="cert_exp_dt" name="cert_exp_dt" value="<?=date("Y-m-d",$aFieldsInCommon['cert_exp_dt'])?>" />&nbsp;<a href="/includes/calendar.php?form_date=mainform.cert_exp_dt" onClick="window.open(this.href,'Calendar','right=200,top=240,width=212,height=200'); return false;"><img src="/lib/images/calendar.gif" align="absmiddle" width="16" height="15" style="border: none;" /></a></td>
			<td align="right">Prod ID:</td>
			<td width="280">
				<select id="cert_prod_id" name="cert_prod_id">
			<?php
			$sql_prod = "SELECT * FROM products WHERE p_iscert > 0 ORDER BY pID";
			$res_prod = mysql_query($sql_prod) or print(mysql_error());
			while($row_prod=mysql_fetch_assoc($res_prod)) {
				$selected = '';
				if($aFieldsInCommon['cert_prod_id'] == $row_prod['pID']) {
					$selected = ' selected="selected"';
				}
			?>
					<option value="<?=$row_prod['pID']?>"<?=$selected?>><?=$row_prod['pID']?></option>
			<?php
			}
			?>
				</select>
			</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">E-mail:</td>
			<td><input type="text" id="cert_email" name="cert_email" value="<?=$aFieldsInCommon['cert_email']?>" /></td>
			<td align="right">Amount:</td>
			<td><input type="text" id="cert_amt" name="cert_amt" value="<?=$aFieldsInCommon['cert_amt']?>" /></td>
			<td align="right">Order ID:</td>
			<td><input type="text" id="cert_order_id" name="cert_order_id" value="<?=$aFieldsInCommon['cert_order_id']?>" /></td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td align="right">Pend. Order Amt:</td>
			<td><input type="text" id="pend_order_amt" name="pend_order_amt" value="<?=$aFieldsInCommon['pend_order_amt']?>" /></td>
			<td align="right">Pend. Order ID:</td>
			<td><input type="text" id="pend_order_id" name="pend_order_id" value="<?=$aFieldsInCommon['pend_order_id']?>" /></td>
			<td align="right">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td colspan="6" align="right">
				<input type="submit" id="modify_cert" name="modify_cert" value="Modify All Certificates" />
				<input type="hidden" id="action" name="action" value="mod_all_certs" />
				<input type="button" id="go_back" name="go_back" value="Go Back" onclick="goBack();" />
			</td>
		</tr>
	</table>
	<p>&nbsp;</p>
	
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
	<?php
	$sql = "SELECT * FROM certificates WHERE cert_id IN($certs_csv)";
	$res = mysql_query($sql) or print(mysql_error());
	if(mysql_num_rows($res) > 0) {
	?>
		<tr bgcolor="#030133" style="color: #E7EAEF; font-weight: bold">
			<th width="60">Cert ID</th>
			<th width="50">Batch #</th>
			<th width="85">Sequential #</th>
			<th width="90">Prod ID</th>
			<th width="40">Amount</th>
			<th width="80">Exp. Date</th>
			<th>E-Mail</th>
			<th>Group Name</th>
			<th width="115">Code</th>
			<th width="80">Order ID</th>
		</tr>
	<?php
		while($row=mysql_fetch_assoc($res)) {
	?>
		<tr bgcolor="#E7EAEF">
			<td align="center">
				<?=$row['cert_id']?>
				<input type="hidden" name="certs[]" value="<?=$row['cert_id']?>" />
			</td>
			<td><?=$row['batch_number']?></td>
			<td><?=$row['sequential']?></td>
			<td><?=$row['cert_prod_id']?></td>
			<td><?=$row['cert_amt']?></td>
			<td><?=date("M j, Y",$row['cert_exp_dt'])?></td>
			<td><?=$row['cert_email']?></td>
			<td><?=$row['cert_group']?></td>
			<td><?=$row['cert_code']?></td>
			<td><?=$row['cert_order_id']?></td>
		</tr>
	<?php
			}
		}else{
	?>
		<tr bgcolor="#E7EAEF">
			<td align="center" colspan="10" height="50">No Hard Cards</td>
		</tr>
	<?php
		}
	?>
	</table>
	</form>
	<p>&nbsp;</p>






<?php
}else{
	if(!empty($_POST['power_search'])) {
		$_SESSION['form_error'] = false;
		foreach($_POST as $key => $value) {
			$_SESSION['form'][$key] = false;
			$_SESSION['viewed_form'][$key] = $value;
		}
	
		$sql = "SELECT *, FORMAT(cert_amt,2) cert_amt FROM certificates WHERE batch_number != 0";
		if($_POST['batch_by'] != 'none') {
			if($_POST['batch_by'] == 'number') {
				if(!empty($_POST['batch_number'])) { // IF IT'S EMPTY, JUST SKIP IT
					if(preg_match("/^[0-9]+$/",$_POST['batch_number'])) { // CHECK IF ITS VALID
						$sql .= " AND batch_number = '" . str_pad($_POST['batch_number'],5,"0", STR_PAD_LEFT) . "'";
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['batch_number'] = true;
					}
				}
			}elseif($_POST['batch_by'] == 'range') {
				if(!empty($_POST['batch_from']) || !empty($_POST['batch_to'])) { // IF IT'S EMPTY, JUST SKIP IT
					if(ereg("^[0-9]+$",$_POST['batch_from'])) { // CHECK IF ITS VALID
						if(ereg("^[0-9]+$",$_POST['batch_to'])) {
							$addcomma = '';
							$batch_csv = '';
							if($_POST['batch_to'] < $_POST['batch_from']) {
								$from = (int)$_POST['batch_to'];
								$to = (int)$_POST['batch_from'];
							}else{
								$from = (int)$_POST['batch_from'];
								$to = (int)$_POST['batch_to'];
							}
							for($j=$from; $j<=(int)$to; $j++) {
								$batch_csv .= $addcomma . "'" . str_pad($j,5,"0", "STR_PAD_LEFT") . "'";
								$addcomma = ',';
							}
							$sql .= " AND batch_number IN($batch_csv)";
						}else{
							$_SESSION['form_error'] = true;
							$_SESSION['form']['batch_to'] = true;
						}
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['batch_from'] = true;
					}
				}
			}elseif($_POST['batch_by'] == 'list') {
				if(!empty($_POST['batch_list'])) { // IF IT'S EMPTY, JUST SKIP IT
					if(ereg("^([0-9]+[,]?)+[0-9]+$",$_POST['batch_list'])) { // CHECK IF ITS VALID
						$aBatches = explode(",",$_POST['batch_list']);
						$new_csv = '';
						$addcomma = '';
						for($i=0; $i<count($aBatches); $i++) {
							$new_csv .= $addcomma . "'" . str_pad($aBatches[$i], 5, "0", "STR_PAD_LEFT") . "'";
							$addcomma = ',';
						}
						$sql .= " AND batch_number IN($new_csv)";
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['batch_list'] = true;
					}
				}
			}
		}
		
		if($_POST['seq_by'] != 'none') {
			if($_POST['seq_by'] == 'number') {
				if(!empty($_POST['seq_number'])) {
					if(ereg("^[0-9]+$",$_POST['seq_number'])) {
						$sql .= " AND sequential = '" . $_POST['seq_number'] . "'";
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['seq_number'] = true;
					}
				}
			}elseif($_POST['seq_by'] == 'range') {
				if(!empty($_POST['seq_from']) || !empty($_POST['seq_to'])) { // IF ANY HAVE BEEN FILLED IN, THEN GO AHEAD AND VALIDATE
					if(ereg("^[0-9]+$",$_POST['seq_from'])) {
						if(ereg("^[0-9]+$",$_POST['seq_to'])) {
							$addcomma = '';
							$seq_csv = '';
							if($_POST['seq_to'] < $_POST['seq_from']) {
								$from = (int)$_POST['seq_to'];
								$to = (int)$_POST['seq_from'];
							}else{
								$from = (int)$_POST['seq_from'];
								$to = (int)$_POST['seq_to'];
							}
							for($j=$from; $j<=$to; $j++) {
								$seq_csv .= $addcomma . "'" . str_pad($j,7,"0", "STR_PAD_LEFT") . "'";
								$addcomma = ',';
							}
							$sql .= " AND sequential IN($seq_csv)";
						}else{
							$_SESSION['form_error'] = true;
							$_SESSION['form']['seq_to'] = true;
						}
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['seq_from'] = true;
					}
				}
			}elseif($_POST['seq_by'] == 'list') {
				if(!empty($_POST['seq_list'])) {
					if(ereg("^([0-9]+[,]?)+[0-9]+$",$_POST['seq_list'])) {
						$aBatches = explode(",",$_POST['seq_list']);
						$new_csv = '';
						$addcomma = '';
						for($i=0; $i<count($aBatches); $i++) {
							$new_csv .= $addcomma . "'" . str_pad($aBatches[$i], 7, "0", "STR_PAD_LEFT") . "'";
							$addcomma = ',';
						}
						$sql .= " AND sequential IN($new_csv)";
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['seq_list'] = true;
					}
				}
			}
		}
		
		if($_POST['exp_date_by'] != 'none') {
			if($_POST['exp_date_by'] == 'one_day') {
				if(!empty($_POST['exp_date'])) {
					if(ereg("^[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}:[0-9]{2})?$",$_POST['exp_date'])) { // MYSQL FORMAT
						$sql .= " AND cert_exp_dt = '".strtotime($_POST['exp_date'])."'";
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['exp_date'] = true;
					}
				}
			}elseif($_POST['exp_date_by'] == 'range') {
				if(!empty($_POST['exp_date_from']) || !empty($_POST['exp_date_to'])) {
					if(ereg("^[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}:[0-9]{2})?$",$_POST['exp_date_from'])) { // MYSQL FORMAT
						if(ereg("^[0-9]{4}-[0-9]{2}-[0-9]{2}( [0-9]{2}:[0-9]{2}:[0-9]{2})?$",$_POST['exp_date_to'])) {	
							$sql .= " AND cert_exp_dt BETWEEN '".strtotime($_POST['exp_date_from'])."' AND '".strtotime($_POST['exp_date_to'])."'";
						}else{
							$_SESSION['form_error'] = true;
							$_SESSION['form']['exp_date_to'] = true;
						}
					}else{
						$_SESSION['form_error'] = true;
						$_SESSION['form']['exp_date_from'] = true;
					}
				}
			}
		}
		
		if(!empty($_POST['group_name'])) {
			$sql .= " AND cert_group LIKE '%".mysql_real_escape_string(trim($_POST['group_name']))."%'";
		}
		
		if(!empty($_POST['order_number'])) {
			if(ereg("^[0-9]+$",$_POST['order_number'])) {	
				$sql .= " AND cert_order_id = ".mysql_real_escape_string(trim($_POST['order_number']))."";
			}else{
				$_SESSION['form_error'] = true;
				$_SESSION['form']['order_number'] = true;
			}
		}
		
		if(!empty($_POST['cert_code'])) {
			$sql .= " AND cert_code = '".mysql_real_escape_string(trim($_POST['cert_code']))."'";
		}
		
		$sql .= " ORDER BY batch_number, sequential";
		
		//echo $sql;
		
		if(!$_SESSION['form_error']) {
			// PROCESS EVERYTHING
			$res = mysql_query($sql) or print(mysql_error().'<br />'.$sql);
			
			$i=0;
			$_SESSION['viewed'] = array();
			while($row=mysql_fetch_assoc($res)) {
				$_SESSION['viewed'][$i] = $row;
				$i++;
			}
		}
	}
	
	?>
	<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
	<script language="JavaScript" type="text/javascript">
	//<![CDATA[
	Event.observe(window,'load', changeBatch, false);
	Event.observe(window,'load', changeSequential, false);
	Event.observe(window,'load', changeExpDate, false);
	
	function changeBatch() {
		var html_text = '&nbsp;';
		var html_form = '&nbsp;';
		var error = '';
		var error2 = '';
		var selected = $F('batch_by');
		
		if(selected == 'number') {
		<?php
		if($_SESSION['form']['batch_number']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Batch #:</div>';
			html_form = '<input name="batch_number" type="text" id="batch_number" maxlength="7" value="<?=$_SESSION['viewed_form']['batch_number']?>" />';
		}else if(selected == 'range') {
		<?php
		if($_SESSION['form']['batch_from']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		if($_SESSION['form']['batch_to']) {
		?>
			error2 = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Batch&nbsp;</div>';
			html_form = '<input name="batch_from" type="text" id="batch_from" size="10" value="<?=$_SESSION['viewed_form']['batch_from']?>" /> <span' + error2 + '>to</span> <input name="batch_to" type="text" id="batch_to" size="10" value="<?=$_SESSION['viewed_form']['batch_to']?>" />';
		}else if(selected == 'list') {
		<?php
		if($_SESSION['form']['batch_list']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Batches:</div>';
			html_form = '<input type="text" name="batch_list" value="<?=$_SESSION['viewed_form']['batch_list']?>" /> (ex. 34,37,39)';
		}
		
		$('batch_td_text').innerHTML = html_text;
		$('batch_td_form').innerHTML = html_form;
	}
	
	function changeSequential() {
		var html_text = '&nbsp;';
		var html_form = '&nbsp;';
		var error = '';
		var error2 = '';
		var selected = $F('seq_by');
		
		if(selected == 'number') {
		<?php
		if($_SESSION['form']['seq_number']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Sequential #:</div>';
			html_form = '<input name="seq_number" type="text" id="seq_number" maxlength="7" value="<?=$_SESSION['viewed_form']['seq_number']?>" />';
		}else if(selected == 'range') {
		<?php
		if($_SESSION['form']['seq_from']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}if($_SESSION['form']['seq_to']) {
		?>
			error2 = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Sequential&nbsp;</div>';
			html_form = '<input name="seq_from" type="text" id="seq_from" size="10" value="<?=$_SESSION['viewed_form']['seq_from']?>" /> <span' + error2 + '>to</span> <input name="seq_to" type="text" id="seq_to" size="10" value="<?=$_SESSION['viewed_form']['seq_to']?>" />';
		}else if(selected == 'list') {
		<?php
		if($_SESSION['form']['seq_list']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Sequentials:</div>';
			html_form = '<input type="text" name="seq_list" value="<?=$_SESSION['viewed_form']['seq_list']?>" /> (ex. 54,67,89)';
		}
		
		$('seq_td_text').innerHTML = html_text;
		$('seq_td_form').innerHTML = html_form;
	}
	
	function changeExpDate() {
		var html_text = '&nbsp;';
		var html_form = '&nbsp;';
		var error = '';
		var error2 = '';
		var selected = $F('exp_date_by');
		
		if(selected == 'one_day') {
		<?php
		if($_SESSION['form']['exp_date']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Exp. Date:</div>';
			html_form = '<input name="exp_date" type="text" id="exp_date" value="<?=$_SESSION['viewed_form']['exp_date']?>" />&nbsp;<a href="/includes/calendar.php?form_date=mainform.exp_date" onClick="window.open(this.href,\'Calendar\',\'right=200,top=240,width=212,height=200\'); return false;"><img src="/lib/images/calendar.gif" align="absmiddle" width="16" height="15" style="border: none;" /></a>';
		}else if(selected == 'range') {
		<?php
		if($_SESSION['form']['exp_date_from']) {
		?>
			error = ' style="color: #F00; font-weight: bold;"';
		<?php
		}if($_SESSION['form']['exp_date_to']) {
		?>
			error2 = ' style="color: #F00; font-weight: bold;"';
		<?php
		}
		?>
			html_text = '<div align="right"' + error + '>Exp. Date&nbsp;</div>';
			html_form = '<input name="exp_date_from" type="text" id="exp_date_from" size="10" value="<?=$_SESSION['viewed_form']['exp_date_from']?>" />&nbsp;<a href="/includes/calendar.php?form_date=mainform.exp_date_from" onClick="window.open(this.href,\'Calendar\',\'right=200,top=240,width=212,height=200\'); return false;"><img src="/lib/images/calendar.gif" align="absmiddle" width="16" height="15" style="border: none;" /></a> <span' + error2 + '>to</span> <input name="exp_date_to" type="text" id="exp_date_to" size="10" value="<?=$_SESSION['viewed_form']['exp_date_to']?>" />&nbsp;<a href="/includes/calendar.php?form_date=mainform.exp_date_to" onClick="window.open(this.href,\'Calendar\',\'right=200,top=240,width=212,height=200\'); return false;"><img src="/lib/images/calendar.gif" align="absmiddle" width="16" height="15" style="border: none;" /></a>';
		}
		
		$('exp_td_text').innerHTML = html_text;
		$('exp_td_form').innerHTML = html_form;
	}
	
	function selectAll() {
		var nodeList = document.getElementsByClassName('boxes');
		
		for(i=0; i<nodeList.length; i++) {
			nodeList[i].checked = true;
		}
	}
	
	function selectNone() {
		var nodeList = document.getElementsByClassName('boxes');
		
		for(i=0; i<nodeList.length; i++) {
			nodeList[i].checked = false;
		}
	}
	
	function modifyCert(id) {
		window.location = '<?=$_SERVER['PHP_SELF']?>?cert_id=' + id;
	}
	
	function deactivateCert(cert_id) {
		var goahead = confirm("Are you sure you want to deactivate this hard card?");
		
		if(goahead) {
			window.location = '/admin/hardcardsprocess.php?action=deletecert&cert_id=' + cert_id;
		}
	}
	
	function deactivateSelected() {
		var goahead = confirm("Are you sure you want to deactivate all selected hard cards?");
		
		if(goahead) {
			var boxes = Form.getInputs($('mainform'),'checkbox','selected[]');
			var selectedBoxes = new Array();
			j=0;
			for(i=0; i<boxes.length; i++) {
				if(boxes[i].checked) {
					selectedBoxes[j] = boxes[i].value;
					j++;
				}
			}
			
			var url = '';
			for(i=0; i<selectedBoxes.length; i++) {
				url += '&cert' + i + '=' + selectedBoxes[i];
			}
			
			window.location = '/admin/hardcardsprocess.php?action=deactivateselected&' + url;
		}
	}
	
	function modifySelected() {
		var boxes = Form.getInputs($('mainform'),'checkbox','selected[]');
		var selectedBoxes = new Array();
		j=0;
		for(i=0; i<boxes.length; i++) {
			if(boxes[i].checked) {
				selectedBoxes[j] = boxes[i].value;
				j++;
			}
		}
		
		var url = '';
		for(i=0; i<selectedBoxes.length; i++) {
			url += '&cert' + i + '=' + selectedBoxes[i];
		}
		
		window.location = '<?=$_SERVER['PHP_SELF']?>?action=modifySelected' + url;
	}
	
	//]]>
	</script>
	<h1 style="margin: 10px auto 10px auto; text-align: center; font-family: Georgia, 'Times New Roman', Times, serif; font-variant: small-caps; letter-spacing: .1em; border-bottom: 1px solid #000; border-top: 1px solid #000; width: 300px;">View Hard Cards</h1>
	<?php
		if($_SESSION['form_error']) {
	?>
			<div style="margin: 5px auto; width: 250px; color: #FF0000;">The following fields marked in red are not valid.</div>
	<?php
		}
	?>
	<form id="mainform" name="mainform" method="post" action="<?=$_SERVER['PHP_SELF']?>">
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr>
			<td colspan="6" bgcolor="#030133" style="color: #E7EAEF; font-weight: bold">Power Search</td>
		</tr>
		<tr>
			<td bgcolor="#E7EAEF">
				<div align="right">Batch by: </div>
			</td>
			<td bgcolor="#E7EAEF">
				<select name="batch_by" id="batch_by" onchange="changeBatch();">
					<option value="none"<?=($_SESSION['viewed_form']['batch_by'] == 'none' || empty($_SESSION['viewed_form']['batch_by']))?' selected="selected"':''?>>none</option>
					<option value="number"<?=($_SESSION['viewed_form']['batch_by'] == 'number')?' selected="selected"':''?>>#</option>
					<option value="range"<?=($_SESSION['viewed_form']['batch_by'] == 'range')?' selected="selected"':''?>>Range</option>
					<option value="list"<?=($_SESSION['viewed_form']['batch_by'] == 'list')?' selected="selected"':''?>>List</option>
				</select>
			</td>
			<td bgcolor="#E7EAEF">
				<div align="right">Sequential by:</div>
			</td>
			<td bgcolor="#E7EAEF">
				<select name="seq_by" id="seq_by" onchange="changeSequential();">
					<option value="none"<?=($_SESSION['viewed_form']['seq_by'] == 'none' || empty($_SESSION['viewed_form']['seq_by']))?' selected="selected"':''?>>none</option>
					<option value="number"<?=($_SESSION['viewed_form']['seq_by'] == 'number')?' selected="selected"':''?>>#</option>
					<option value="range"<?=($_SESSION['viewed_form']['seq_by'] == 'range')?' selected="selected"':''?>>Range</option>
					<option value="list"<?=($_SESSION['viewed_form']['seq_by'] == 'list')?' selected="selected"':''?>>List</option>
				</select>
			</td>
			<td bgcolor="#E7EAEF"><div align="right">Exp. Date by:</div></td>
			<td bgcolor="#E7EAEF">
				<select name="exp_date_by" id="exp_date_by" onchange="changeExpDate();">
					<option value="none"<?=($_SESSION['viewed_form']['exp_date_by'] == 'none' || empty($_SESSION['viewed_form']['exp_date_by']))?' selected="selected"':''?>>none</option>
					<option value="one_day"<?=($_SESSION['viewed_form']['exp_date_by'] == 'one_day')?' selected="selected"':''?>>One Day</option>
					<option value="range"<?=($_SESSION['viewed_form']['exp_date_by'] == 'range')?' selected="selected"':''?>>Range</option>
				</select>
			</td>
		</tr>
		<tr>
			<td id="batch_td_text" bgcolor="#E7EAEF">&nbsp;</td>
			<td id="batch_td_form" bgcolor="#E7EAEF">&nbsp;</td>
			<td id="seq_td_text" bgcolor="#E7EAEF">&nbsp;</td>
			<td id="seq_td_form" bgcolor="#E7EAEF">&nbsp;</td>
			<td id="exp_td_text" bgcolor="#E7EAEF">&nbsp;</td>
			<td id="exp_td_form" bgcolor="#E7EAEF">&nbsp;</td>
		</tr>
		<tr>
			<td bgcolor="#E7EAEF">
				<div align="right"<?=($_SESSION['form']['group_name'])?' style="color: #F00; font-weight: bold;"':''?>>Group Name:</div>
			</td>
			<td bgcolor="#E7EAEF">
				<input name="group_name" type="text" id="group_name" value="<?=$_POST['group_name']?>" />
			</td>
			<td bgcolor="#E7EAEF">
				<div align="right"<?=($_SESSION['form']['order_number'])?' style="color: #F00; font-weight: bold;"':''?>>Order #:</div>
			</td>
			<td bgcolor="#E7EAEF">
				<input name="order_number" type="text" id="order_number" value="<?=$_POST['order_number']?>" />
			</td>
			<td bgcolor="#E7EAEF">
				<div align="right"<?=($_SESSION['form']['cert_code'])?' style="color: #F00; font-weight: bold;"':''?>>Code:</div>
			</td>
			<td bgcolor="#E7EAEF">
				<input name="cert_code" type="text" id="cert_code" value="<?=$_POST['cert_code']?>" />
			</td>
		</tr>
		<tr bgcolor="#E7EAEF">
			<td colspan="6" align="right"><input type="submit" id="power_search" name="power_search" value="Power Search" /></td>
		</tr>
	</table>
	
	<?php
		if(!empty($_SESSION['viewed'])) {
			$num_of_hard_cards_found = count($_SESSION['viewed']);
	?>
	<p style="margin-bottom: 0; margin-left: 2px;"><span style="font-weight: bold;"><?=$num_of_hard_cards_found?></span> Hard Cards Found</p>
	<?php
		}else{
			echo '<p style="margin: 0;">&nbsp;</p>';
		}
	?>
	
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
		<tr>
			<td colspan="4">
				<input type="button" id="select_all" name="select_all" value="Select All" onclick="selectAll();" />
				<input type="button" id="select_none" name="select_none" value="Select None" onclick="selectNone();" />
			</td>
			<td colspan="8" align="right">
				<input type="button" id="modify_sel" name="modify_sel" value="Modify Selected" onclick="modifySelected();" />
				<input type="button" id="deactivate_sel" name="deactivate_sel" value="Deactivate Selected" onclick="deactivateSelected();" />
			</td>
		</tr>
		<tr bgcolor="#030133" style="color: #E7EAEF; font-weight: bold">
			<th width="15">&nbsp;</th>
			<th width="50">Batch #</th>
			<th width="85">Sequential #</th>
			<th width="90">Prod ID</th>
			<th width="40">Amount</th>
			<th width="80">Exp. Date</th>
			<th>E-Mail</th>
			<th>Group Name</th>
			<th width="115">Code</th>
			<th width="80">Order ID</th>
			<th width="50">&nbsp;</th>
			<th width="50">&nbsp;</th>
		</tr>
	<?php
		if(!empty($_SESSION['viewed'])) {
			for($i=0; $i<$num_of_hard_cards_found; $i++) {
				$is_active = false;
				$is_not_active = false;
				
				if((empty($_SESSION['viewed'][$i]['cert_amt']) || $_SESSION['viewed'][$i]['cert_amt'] == 0) && $_SESSION['viewed'][$i]['cert_order_id'] == 0 && $_SESSION['viewed'][$i]['pend_order_id'] == 0 && (empty($_SESSION['viewed'][$i]['pend_order_amt']) || $_SESSION['viewed'][$i]['pend_order_amt'] == 0)) {
					$is_active = false;
					$is_not_active = true;
				}else{
					$is_active = true;
					$is_not_active = false;
				}
	?>
		<tr bgcolor="#E7EAEF">
			<td align="center"><input class="boxes" type="checkbox" id="selected<?=$i?>" name="selected[]" value="<?=$_SESSION['viewed'][$i]['cert_id']?>" /></td>
			<td><?=$_SESSION['viewed'][$i]['batch_number']?></td>
			<td><?=$_SESSION['viewed'][$i]['sequential']?></td>
			<td><?=$_SESSION['viewed'][$i]['cert_prod_id']?></td>
			<td><?=$_SESSION['viewed'][$i]['cert_amt']?></td>
			<td><?=date("M j, Y",$_SESSION['viewed'][$i]['cert_exp_dt'])?></td>
			<td><?=$_SESSION['viewed'][$i]['cert_email']?></td>
			<td><?=$_SESSION['viewed'][$i]['cert_group']?></td>
			<td><?=$_SESSION['viewed'][$i]['cert_code']?></td>
			<td><?=$_SESSION['viewed'][$i]['cert_order_id']?></td>
			<td align="center">
				<input type="button" id="modify<?=$i?>" name="modify[]" value="Modify" onclick="modifyCert($F('cert_id<?=$i?>'));" />
				<input type="hidden" id="cert_id<?=$i?>" name="cert_id[]" value="<?=$_SESSION['viewed'][$i]['cert_id']?>" />
			</td>
			<td align="center"><input type="button" id="deactivate<?=$i?>" name="deactivate[]" value="Deactivate"<?=($is_not_active)?' disabled="disabled" style="border-right: #B2C1D1; border-bottom: #B2C1D1;"':''?> onclick="deactivateCert(<?=$_SESSION['viewed'][$i]['cert_id']?>);" /></td>
		</tr>
	<?php
			}
		}else{
	?>
		<tr bgcolor="#E7EAEF">
			<td align="center" colspan="12" height="50">Please search above to view Hard Cards</td>
		</tr>
	<?php
		}
	?>
	</table>
	</form>
	<p>&nbsp;</p>
<?php
}
?>

<?php
$_SESSION['form'] = '';
$_SESSION['form_error'] = false;

function E($val) {
	$new_val = mysql_real_escape_string($val);
	return $new_val;
}
?>
