<?php
ini_set('memory_limit','86M');
include('init.php');
include_once(IFZROOT.'kohana.php');
include_once(DOCROOT.'includes/paginateit.class.php');

$PaginateIt = new PaginateIt();
$PaginateIt->pageSeparator = '&nbsp;|&nbsp;';
?>
<link rel="stylesheet" type="text/css" href="http://assets.ifrogz.com/lib/packages/jquery-ui/1.7.2/ui-lightness/jquery-ui.custom.css" />

<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/controls.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/effects.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery/1.3.2/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery-ui/1.7.2/jquery-ui.custom.min.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8">
jQuery.noConflict();

jQuery(document).ready(function() {
	jQuery("input.calendar").datepicker({ 
						showOn: 'button', 
						buttonImage: '/lib/images/misc/calendar.png', 
						buttonImageOnly: true, 
						dateFormat: 'yy-mm-dd', 
						showButtonPanel: true,
						changeMonth: true,
						changeYear: true,
						minDate: '-5Y',
						maxDate: '+5Y'
						}).css('margin-right', '5px');
						
	jQuery('img.ui-datepicker-trigger').css({'vertical-align' : 'middle', 'cursor' : 'pointer'});
});
</script>

<style>
div.autocomplete {
      position:absolute;      
      background-color:white;
      border:1px solid #CCC;
      margin:0px;
      padding:0px;
	  
    }
    div.autocomplete ul {
      list-style-type:none;
      margin:0px;
      padding:0px;
	 
    }
    div.autocomplete ul li.selected { background-color: #E7EAEF;}
    div.autocomplete ul li {
	  list-style-type:none;
      display:block;
      margin:0;
      padding:2px;
      height:14px;
      cursor:pointer;
	  border : none;
	  
	  
    }

</style>

<?php

if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$noQuery=FALSE;
$themask='yyyy-mm-dd';
$admindatestr="Y-m-d";
if(@$admindateformat=="") $admindateformat=0;
if($admindateformat==1)
	$admindatestr="m/d/Y";
elseif($admindateformat==2)
	$admindatestr="d/m/Y";
$alreadygotadmin = getadminsettings();
$fromdate = trim(@$_POST["fromdate"]);
$todate = trim(@$_POST["todate"]);
if($fromdate != ""){
	if(is_numeric($fromdate))
		$thefromdate = time()-($fromdate*60*60*24);
	else
		$thefromdate = parsedate($fromdate);
	if($todate=="")
		$thetodate = $thefromdate;
	elseif(is_numeric($todate))
		$thetodate = time()-($todate*60*60*24);
	else
		$thetodate = parsedate($todate);
	if($thefromdate > $thetodate){
		$tmpdate = $thetodate;
		$thetodate = $thefromdate;
		$thefromdate = $tmpdate;
	}
}else{
	$noQuery=TRUE;
	$thefromdate = time()-(24);
	$thetodate = time();
}

if($ordIDFrom!='' && $ordIDTo!='') $noQuery=FALSE; 
//if(!$noQuery) {
	
	$sSQL= "SELECT iaOptID,iaAmt,iaDate,o.optRegExp,iaProdStyle,iaOldValue,iaNewValue,iar.reason,iaEmpID, iaNotes
			FROM inv_adjustments ia 
			INNER JOIN inv_adj_reasons iar ON ia.iaReason=iar.rID
			JOIN options o ON o.optID = ia.iaOptID 
			WHERE iar.rID!=''";
	if (!empty($thefromdate)) $sSQL .= " AND ia.iaDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59' ";
	if (!empty($searchby)) {
		$sSQL .= " AND ia.iaProdStyle LIKE '".$searchby."%'";
	} else if (!empty($_POST['searchby2'])) {
		$sSQL .= " AND ia.iaOptID LIKE '".$_POST['searchby2']."%'";
	}
	if (!empty($_POST['byEmp'])) {
		$sSQL .= " AND iaEmpID = " . $_POST['byEmp'];
	} else {
		$sSQL .= " AND iaEmpID != 81";
	}
	if ($_POST['reason']) {
		$sSQL .= " AND ia.iaReason = " . $_POST['reason'];
	}
	if (!empty($_POST['change_amt'])) {
		$sSQL .= " AND ia.iaAmt " . $_POST['change_amt_cond'] . " " . $_POST['change_amt'];
	}
	$sSQL .= " ORDER BY ia.iaDate DESC, ia.iaReason";
	//echo $sSQL;
	
	$result2 = mysql_query($sSQL) or die(print(mysql_error() . '<br />SQL: ' . $sSQL));
	
	$tot_num_rows = mysql_num_rows($result2);
	
	$PaginateIt->SetItemCount($tot_num_rows);
	$PaginateIt->SetItemsPerPage(1000000);
	$PaginateIt->SetLinksToDisplay(10);
	
	$sSQL .= $PaginateIt->GetSqlLimit();
	
	$result = mysql_query($sSQL) or print(mysql_error());
	
	if (mysql_num_rows($result) > 0) {
		$i=0;
		while($rs = mysql_fetch_assoc($result)){
			$report[$i]=$rs;
		$i++;
		}
	}


$config_admin = RBI_Kohana::config('database.default_admin.connection');

$db_admin = mysql_connect($config['hostname'], $config['username'], $config['password']);
mysql_select_db($config['database']) or die ('DB Admin connection failed.</td></tr></table></body></html>');

// Get Employee Info
$arrEmployees = array();
$sql = "SELECT * FROM employee 
		WHERE department LIKE 'CS'
		OR id IN(81, 38, 2, 75, 78, 26)
		ORDER BY firstname";
$res = mysql_query($sql, $db_admin) or print(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
	$arrEmployees[] = $row;
}

include(APPPATH.'views/partials/admin/dbconnection.php');

?>
	  <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="" style="margin-bottom:10px;">
<form method="post" action="/admin/invchangehistory.php" name="psearchform">
			  <input type="hidden" name="powersearch" value="1" />
			  <tr bgcolor="#030133"><td colspan="9"><strong><font color="#E7EAEF">&nbsp;<?php print $yyPowSea?></font></strong></td></tr>
			  <tr bgcolor="#E7EAEF">
			    <td align="right"><strong>Product:</strong> </td>
	      <td align="left" >
				<div id="autocontainer" style="position:relative; height:16px;">
                        <input id="searchby" name="searchby" type="text" value="<?=$_POST['searchby']?>" size="30" />
                        <span id="indicator" style="display: none;"><img src="/lib/images/indicator.gif" alt="Working..." width="14" height="14" /></span>
                   		<div id="autocomplete" class="autocomplete"></div>
            	</div>
                    
                    <script language="JavaScript" type="text/javascript">
                        new Ajax.Autocompleter("searchby", "autocomplete", "/admin/relatedautocomplete.php", {indicator: 'indicator', paramName: 'pID'});
                        
/*						function getSelectionId(text, li) {
                            $('autocomplete<?=$alldata[0]?>').value='';
                        }
*/						
                    </script>				
                </td> 
                <td align="right"><strong><?php print $yyOrdFro?>:</strong></td>
				<td align="left" width="14%"><input type="text" size="14" name="fromdate" id="fromdate" class="calendar" value="<?php print date("Y-m-d",$thefromdate)?>" /></td>
				<td align="right"><strong><?php print $yyOrdTil?>:</strong></td>
				<td align="left"><input type="text" size="14" name="todate" id="todate" class="calendar" value="<?php print date("Y-m-d",$thetodate)?>" /></td>
			    <td align="right"><strong>Employee</strong></td>
				<td align="left">
					<select name="byEmp">
			<?php
			$selected2 = "";
			if (empty($_POST['byEmp'])) {
				$selected2 = " selected='selected'";
			}
			?>
						<option value=""<?=$selected2?>>Anyone</option>
			<?php
				foreach ($arrEmployees as $emp) {
					$selected = "";
					if (($_POST['byEmp'] == $emp['id'])) {
						$selected = " selected='selected'";
					}
			?>
						<option value="<?=$emp["id"]?>"<?=$selected?>><?=$emp["firstname"]." ".strtoupper(substr($emp["lastname"],0,1))?></option>
			<?php
				}
			
			?>
					</select>
				</td>
				<td align="left">&nbsp;</td>
			  </tr>
			<tr bgcolor="#E7EAEF">
				<td><strong>Reason</strong></td>
				<td>
					<select name="reason" id="reason">
						<option value=""></option>
						<?php
						$sql = "SELECT * FROM inv_adj_reasons";
						$res = mysql_query($sql) or print(mysql_error());
						while ($row = mysql_fetch_assoc($res)) {
							$selected = "";
							if ($row['rID'] == $_POST['reason']) {
								$selected = " selected='selected'";
							}
						?>
							<option value="<?=$row['rID']?>"<?=$selected?>><?=$row['reason']?></option>
						<?php
						}
						?>
					</select>
				</td>
				<td><strong>Part:</strong></td>
				<td>
					<div id="autocontainer2" style="position:relative; height:16px;">
						<input id="searchby2" name="searchby2" type="text" value="<?=$_POST['searchby2']?>" />
						<span id="indicator2" style="display: none;"><img src="/lib/images/indicator.gif" alt="Working..." width="14" height="14" /></span>
						<div id="autocomplete2" class="autocomplete"></div>
					</div>

					<script language="JavaScript" type="text/javascript">
					new Ajax.Autocompleter("searchby2", "autocomplete2", "/admin/relatedautocomplete.php", {indicator: 'indicator', paramName: 'part'});						
					</script>				
				</td>
				
				<td colspan="5" align="center">&nbsp;</td>
			</tr>
			<tr bgcolor="#E7EAEF">
				<td colspan="4">
					<strong>Change Amount</strong>&nbsp;
					<select id="change_amt_cond" name="change_amt_cond">
						<option value="="<?=($_POST['change_amt_cond'] == '=') ? ' selected="selected"' : ''?>>=</option>
						<option value="<"<?=($_POST['change_amt_cond'] == '<') ? ' selected="selected"' : ''?>>&lt;</option>
						<option value="<="<?=($_POST['change_amt_cond'] == '<=') ? ' selected="selected"' : ''?>>&lt;=</option>
						<option value=">"<?=($_POST['change_amt_cond'] == '>') ? ' selected="selected"' : ''?>>&gt;</option>
						<option value=">="<?=($_POST['change_amt_cond'] == '>=') ? ' selected="selected"' : ''?>>&gt;=</option>
					</select>&nbsp;
					<input id="change_amt" name="change_amt" value="<?=$_POST['change_amt']?>" />
				</td>
				<td colspan="5" align="center"><input name="searchbydate" type="submit" id="searchbydate" value="Search" /></td>
			</tr>
			  </form>
			</table>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
		<tr>
          <td width="100%" align="center">
			<input type="hidden" name="posted" value="1">
			<input type="hidden" name="act" value="domodify">
            <table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
	<tr><td colspan="3" align="left">
	  <table width="99%"  border="0" cellspacing="1" cellpadding="2" align="center">
		<tr>
		    <td colspan="2"> <div style="font-size:14px; "><strong>Inventory Change History  </strong></div></td>
			<td colspan="6" align="center"><?=$PaginateIt->GetPageLinks()?></td>
			<td colspan="2"><div align="right"><strong><? if(empty($tot_num_rows)) echo '0'; else echo $tot_num_rows;?>
  Row(s)</strong></div></td>
		    </tr>
		  <tr bgcolor="#030133">
			<th  bgcolor="#030133" scope="col"><strong><font color="#E7EAEF">Date</font></strong></th>
			<th  bgcolor="#030133" scope="col"><strong><font color="#E7EAEF">Part</font></strong></th>
			<th  bgcolor="#030133" scope="col"><strong><font color="#E7EAEF">Product</font></strong></th>
			<th scope="col" ><strong><font color="#E7EAEF">Option</font></strong></th>			
			<th scope="col" ><strong><font color="#E7EAEF">Change Amt</font></strong></th>
			<th scope="col" ><strong><font color="#E7EAEF">Old Amt</font></strong></th>
		    <th nowrap="nowrap" scope="col"><strong><font color="#E7EAEF">New Amt</font></strong></th>
			<th scope="col" ><strong><font color="#E7EAEF">Reason</font></strong></th>		  	
			<th scope="col" ><strong><font color="#E7EAEF">Employee</font></strong></th>
			<th scope="col" ><strong><font color="#E7EAEF">Notes</font></strong></th>			
		  </tr>
		  <?php
		  	for ($i = 0; $i < count($report); $i++) { 
		  		$strdate = date("M j, Y h:i:s a", strtotime($report[$i]['iaDate']));
		  ?>
		  <tr style="background-color: #<?=($i%2==0)?'E7EAEF':'EAECEB'?>">
			<td><?=str_replace(" ", "&nbsp;", $strdate) ?></td>
			<td><?=$report[$i]['optRegExp']?></td>
			<td><?=$report[$i]['iaProdStyle']?></td>			
			<td><?=$report[$i]['iaOptID']?></td>			
			<td><?=$report[$i]['iaAmt']?></td>
		    <td><?=$report[$i]['iaOldValue']?></td>
			<td><?=$report[$i]['iaNewValue']?></td>
			<td><?=$report[$i]['reason']?></td>
			<?
			  $config_admin = RBI_Kohana::config('database.default_admin.connection');

			  $db_admin = mysql_connect($config['hostname'], $config['username'], $config['password']);
			  mysql_select_db($config['database']) or die ('DB Admin connection failed.</td></tr></table></body></html>');
			
				$rbiSQL = 'SELECT firstname,lastname FROM employee WHERE id='.$report[$i]['iaEmpID'];
				//echo $rbiSQL;
				$rs_rbi = mysql_query($rbiSQL, $db_admin);
				$rbi_row = mysql_fetch_assoc($rs_rbi);
			?>
			<td><?=$rbi_row['firstname'].' '.$rbi_row['lastname'] ?></td>
			<td><?=$report[$i]['iaNotes']?></td>			
		  </tr>		
		  <? }
		  if($i==0) echo '<tr>
			<td colspan="8" align="center">There are no records returned for your search criteria.</td>
		  </tr>';
		  ?>
		<tr>
			<td colspan="10" align="center"><?=$PaginateIt->GetPageLinks()?></td>
		</tr>  
		</table>	 
            </table>
		</td>
    </tr>
</table>