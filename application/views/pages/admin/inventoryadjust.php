<?php
include('init.php');
?>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/scriptaculous.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/popcalendar.js"></script>
<script language="JavaScript" type="text/javascript">
function validateForm()
{
	var yessubmit=true;
	var msg='';
	var maxitems= $('maxitems').value;
	for(var i = 0; i < maxitems; i++) {
    	var error=false;
		var adjustment=$('adjustment'+i);
		var reason=$('reason'+i);
		if(isNaN(adjustment.value)) error=true;
		if(adjustment.value!='' && reason.value=='') error=true;
		if(adjustment.value=='' && reason.value!='') error=true;
		if(adjustment.value=='' && reason.value=='') $('update'+i).value='';
		if(error) {
			adjustment.style.backgroundColor='#FF0000';
			reason.style.backgroundColor='#FF0000';
			yessubmit=false;
		} else {
			adjustment.style.backgroundColor='';
			reason.style.backgroundColor='';
		}
  	}
	if(yessubmit) {
		return true;
	} else {
		msg='Error in form! Change all highlighted fields!';
		alert(msg);
		return false;
	}
	
}
function toggleDisplay(type) {
	var toggle='';
	if(type=='adjust') toggle='none';
	var r = document.getElementsByClassName("noshow");
	for(i=0; i<r.length; i++) {
		r[i].style.display = toggle;
		//new Effect.Highlight(r[i]);
	}
}
</script>
<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
$themask='yyyy-mm-dd';
//confirmation
if(isset($_POST['back'])) {
	for($i=0;$i<$_POST['maxitems'];$i++) {
		$id=$_POST['update'.$i];
		if(!empty($id)) {
			$alldata2[$i]['prod_style']=$_POST['prodstyle'.$i];
			$alldata2[$i]['optName']=$_POST['optname'.$i];
			$alldata2[$i]['optColor']=$_POST['optcolor'.$i];
			$alldata2[$i]['optID']=$id;
			$alldata2[$i]['optID_hidden']=$id;
			$alldata2[$i]['adjustment']=$_POST['adjustment'.$i];
			$alldata2[$i]['reason']=$_POST['reason'.$i];
			$alldata2[$i]['optStock']=$_POST['optStock'.$i];
		}
	}
}
//showarray($alldata2);
if(isset($submit)) {
	$alldata='';
	for($i=0;$i<$maxitems;$i++) {
		$id=$_POST['update'.$i];
		if(!empty($id)) {
			$alldata[$i]['prod_style']=$_POST['prodstyle'.$i];
			$alldata[$i]['optName']=$_POST['optname'.$i];
			$alldata[$i]['optColor']=$_POST['optcolor'.$i];
			$alldata[$i]['optID']=$id;
			$alldata[$i]['optID_hidden']=$id;
			$alldata[$i]['adjustment']=$_POST['adjustment'.$i];
			$alldata[$i]['reason']=$_POST['reason'.$i];
			$alldata[$i]['optStock']=$_POST['optStock'.$i];
		}
	}
	
}
//update
if(isset($submit2)) {
	$alldata2='';
	include(APPPATH.'views/pages/admin/functions.php');
	for($i=0;$i<$maxitems;$i++) {
		$id=$_POST['update'.$i];
		if(!empty($id)) {
			$adjustment=$_POST['adjustment'.$i];
			$optStock=$_POST['optStock'.$i];
			$reason=$_POST['reason'.$i];
			$total_stock=$optStock-$adjustment;
			$sql="UPDATE options SET optStock=".$total_stock." WHERE optID=".$id;
			mysql_query($sql) or print(mysql_error());
			
			$sql="INSERT INTO inv_adjustments (iaOptID,iaAmt,iaDate,iaReason) 
					VALUES ('$id','$adjustment','".date('Y-m-d H:i:s')."','$reason')";
			mysql_query($sql) or print(mysql_error());
			
			//checks for backorders when inventory is updated
			updateBackorders($id);
		}
	}
}
//reasons
$sql_reasons="SELECT * FROM inv_adj_reasons";
$result_reasons=mysql_query($sql_reasons);
$i=0;
while($rs_reasons=mysql_fetch_assoc($result_reasons)) {
	$reasons_arr[$i]['rID']=$rs_reasons['rID'];
	$reasons_arr[$i]['reason']=$rs_reasons['reason'];
	$i++;
}

if($reporttype=="adjust" || $reporttype=='') {$reporttype="adjust"; $doedit=TRUE;} else $doedit=FALSE;

if(!isset($submit) && $reporttype=="adjust") {
	$sSQL = "SELECT Distinct(optID),optGroup,optName,optStock,optStyleID,optColor,optMin,optExtend_shipping,optReorder_point,optReorder_qty FROM options o, optiongroup og, prodoptions po WHERE o.optGroup=og.optGrpID AND o.optGroup=po.poOptionGroup";
		//product type
		if($product_type=='other') $sSQL .= " AND og.optGrpName NOT LIKE '%wrap%' AND og.optGrpName NOT LIKE '%band%' AND og.optGrpName NOT LIKE '%screen%'";
		elseif($product_type=='') {
			$product_type='wrap';//default product type
			$sSQL .= " AND og.optGrpName LIKE '%".$product_type."%'";
		}
		else $sSQL .= " AND og.optGrpName LIKE '%".$product_type."%'";
	//echo $sSQL;
	
	$result=mysql_query($sSQL);
	$num_rows_order=mysql_num_rows($result);
	$i=0;
		$alldata='';
		while($rs = mysql_fetch_assoc($result)){	
			$sql2="SELECT * FROM prodoptions po, products p WHERE po.poProdID=p.pID AND po.poOptionGroup='".$rs["optGroup"]."' ORDER BY p.pOrder";
			$result2=mysql_query($sql2);
			$rs2 = mysql_fetch_assoc($result2);
			if($rs2["pDisplay"]=='1') {
				if(!strstr($rs2["poProdID"],'-')) $prod_style=$rs2["poProdID"] . '-' . $rs["optStyleID"];
				else $prod_style=$rs2["poProdID"] . $rs["optStyleID"];
				$alldata[$i]['prod_style']=$prod_style;
				$alldata[$i]['optName']=$rs['optName'];
				$alldata[$i]['optColor']=$rs['optColor'];
				$alldata[$i]['optID']=$rs['optID'];
				$alldata[$i]['optStock']=$rs['optStock'];
				$alldata[$i]['optStock_old']=$rs['optStock'];
				$alldata[$i]['reason']='';
				
				for($j=0;$j<count($alldata2);$j++) {
					if($alldata2[$j]['optID']==$rs['optID']) {
						echo 'alldata2='.$alldata2[$j]['optID'].'='.$rs['optID'];
						$alldata[$i]['optStock']=$alldata2[$j]['optStock'];
						$alldata[$i]['optStock_old']=$alldata2[$j]['optStock_old'];
						$alldata[$i]['adjustment']=$alldata2[$j]['adjustment'];
						$alldata[$i]['reason']=$alldata2[$j]['reason'];
						$alldata[$i]['adjustment_old']=$alldata2[$j]['adjustment_old'];
						//echo '$alldata stock='.$alldata[$i]['optStock'];
					}
				}
				$i++;
			}
		}
}
elseif($reporttype=="report") {
	$fromdate = trim(@$_POST["fromdate"]);
	$todate = trim(@$_POST["todate"]);
	if($fromdate != ""){
		if(is_numeric($fromdate))
			$thefromdate = time()-($fromdate*60*60*24);
		else
			$thefromdate = parsedate($fromdate);
		if($todate=="")
			$thetodate = $thefromdate;
		elseif(is_numeric($todate))
			$thetodate = time()-($todate*60*60*24);
		else
			$thetodate = parsedate($todate);
		if($thefromdate > $thetodate){
			$tmpdate = $thetodate;
			$thetodate = $thefromdate;
			$thefromdate = $tmpdate;
		}
	}else{
		$thefromdate = time()-(24);
		$thetodate = time();
	}
	$sSQL = "SELECT ia.*, iar.reason FROM inv_adjustments ia, inv_adj_reasons iar WHERE ia.iaReason=iar.rID AND iaDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59'";
	//reason type
	if(!empty($reason_type)) $sSQL .= " AND ia.iaReason='".$reason_type."'";
	//echo $sSQL;
	
	$result=mysql_query($sSQL);
	$num_rows_order=mysql_num_rows($result);

	while($rs = mysql_fetch_assoc($result)){	
		$sql2="SELECT *, o.optStyleID,o.optName,o.optStock,o.optColor,og.optGrpName FROM prodoptions po, products p, options o, optiongroup og WHERE po.poProdID=p.pID AND po.poOptionGroup=o.optGroup AND o.optGroup=og.optGrpID AND o.optID='".$rs["iaOptID"]."' ORDER BY p.pOrder";
		$result2=mysql_query($sql2);
		$rs2 = mysql_fetch_assoc($result2);
			if(!strstr($rs2["poProdID"],'-')) $prod_style=$rs2["poProdID"] . '-' . $rs2["optStyleID"];
			else $prod_style=$rs2["poProdID"] . $rs2["optStyleID"];
			$alldata[$i]['prod_style']=$prod_style;
			$alldata[$i]['optName']=$rs2['optName'];
			$alldata[$i]['optColor']=$rs2['optColor'];
			$alldata[$i]['optID']=$rs2['optID'];
			$alldata[$i]['optStock']=$rs2['optStock'];
			$alldata[$i]['optGrpName']=$rs2['optGrpName'];
			$alldata[$i]['reason']=$rs['reason'];
			$alldata[$i]['adjustment']=$rs['iaAmt'];
			$alldata[$i]['adjustment_date']=$rs['iaDate'];
		$i++;
	}	
} 
if(!empty($alldata)) sort($alldata);
//showarray($alldata);
?>

      <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="">
        <tr>
          <td width="100%" align="center">
			<span name="searchspan" id="searchspan">
            <? if(isset($submit)) { ?>				
					<div style="color:#FF0000; font-size:14px; font-weight:bold;">Please verify that all levels have been changed correctly!</div>
			<? } else { ?>
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="">
				  <form method="post" action="/admin/inventoryadjust.php" name="psearchform">
				  <input type="hidden" name="powersearch" value="1" />
				  <tr bgcolor="#030133"><td colspan="7"><strong><font color="#E7EAEF">&nbsp;Power Inventory Search</font></strong></td></tr>
				  <tr bgcolor="#E7EAEF"> 
					<td width="13%" align="right"><strong>Report Type:</strong></td>
					<td width="17%" align="left"><select name="reporttype" id="reporttype" onChange="toggleDisplay(this.value);">
					  <option value="adjust" <?php if ($reporttype=="adjust") {echo "SELECTED";} ?>>Adjust Inventory</option>
					  <option value="report" <?php if ($reporttype=="report") {echo "SELECTED";} ?>>Report</option>
					</select></td>
					<td width="10%" align="left"><div align="right"><strong>Product:</strong></div></td>
					<td width="18%" align="left">				  <select name="product_type" id="product_type">
					  <option value="Wrap" <?php if (!(strcmp("Wrap", $product_type))) {echo "SELECTED";} ?>>Wrap</option>
					  <option value="Band" <?php if (!(strcmp("Band", $product_type))) {echo "SELECTED";} ?>>Band</option>
					  <option value="Screen" <?php if (!(strcmp("Screen", $product_type))) {echo "SELECTED";} ?>>Screen</option>
					  <option value="other" <?php if (!(strcmp("other", $product_type))) {echo "SELECTED";} ?>>Other</option>
					  </select></td>
					<td width="8%" align="right"><strong> </strong></td>
					<td width="13%" align="right"><div align="left">
					  
				    </div></td>
					<td width="21%" align="right"><!-- <strong>Products ID:</strong> -->
			          <div align="center">			            
			            <input name="submit3" type="submit" id="submit33" value="<?php print $yySearch?>" />
			            &nbsp;				        <!-- <input name="productid" type="text" id="productid2"> -->					  
			        &nbsp;			      </div></td>
					</tr>
				  <tr bgcolor="#E7EAEF" class="noshow" style="<? if($reporttype=="adjust") echo 'display: none'; else echo '';?>">
                    <td align="right"><strong><?php print $yyOrdFro?>:</strong></td>
                    <td align="left"><input type="text" size="14" name="fromdate" value="<?php print $fromdate?>" />
                        <input type=button onclick="popUpCalendar(this, document.forms.psearchform.fromdate, '<?php print $themask?>', 0)" value='DP' /></td>
                    <td align="right"><strong><?php print $yyOrdTil?>:</strong></td>
                    <td align="left"><input type="text" size="14" name="todate" value="<?php print $todate?>" />
                        <input type=button onclick="popUpCalendar(this, document.forms.psearchform.todate, '<?php print $themask?>', -205)" value='DP' /></td>
                    <td align="center"><strong>Reason:</strong></td>
                    <td align="left"><select name="reason_type" id="select2">
                      <option value="" <?php if(empty($reason_type)) {echo "SELECTED";} ?>>&lt; Select &gt;</option>
                      <? for($j=0;$j<count($reasons_arr);$j++) {	?>
                      <option value="<?=$reasons_arr[$j]['rID']?>" <?php if ($reasons_arr[$j]['rID']==$reason_type) {echo "SELECTED";} ?>>
                      <?=$reasons_arr[$j]['reason']?>
                      </option>
                      <? }?>
                    </select></td>
                    <td align="center">&nbsp;</td>
				    </tr>
				  </form>
			</table>				
				<? } ?>
			</span>
			<table width="100%" border="0" cellspacing="1" cellpadding="2" >
			 <form method="post" name="mainform" action="/admin/inventoryadjust.php" onSubmit="return validateForm()">
			  	<input name="product_type" type="hidden" value="<?=$product_type?>">
				<input name="reporttype" type="hidden" value="<?=$reporttype?>">
			  <tr > 
                <td align="right" colspan="4"><? if(isset($submit)) echo '<input type="submit" name="back" value="&laquo;Back" /><input type="submit" name="submit2" value="Update" />'; else echo '<input type="submit" name="submit" value="Update"; /> <input type="reset" value="'.$yyReset.'" />'; ?></td>
			  </tr>
			  <tr bgcolor="#030133"> 
                <td align="center"><strong><font color="#E7EAEF">Product</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Current Inventory </font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Adjustment</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Reason</font></strong></td>
			  </tr>
			  
<?php
	if(is_array($alldata)){
		$rowcounter=0;
		$ordTot=0;
		//echo 'product_type='.$product_type;
		for($i=0;$i<count($alldata);$i++){
			//echo '<br />group='.$alldata[$i]['optGrpName'];
			if($reporttype=='report') {
				if($alldata[$i]['optGrpName']!='other') {
					if(!strstr($alldata[$i]['optGrpName'],$product_type)) continue;
				} else {
					if(strstr('Wrap,Band,Screen',$alldata[$i]['optGrpName'])) continue;
				}
			}
			if($i%2==0) $bgcolor="#EAECEB"; else $bgcolor="#E7EAEF";	
?>
			  <tr bgcolor="<?php print $bgcolor?>"> 
                <td align="center" valign="middle" width="300">
					<input type="hidden" id="update<?=$i?>" name="update<?=$i?>" value="<?=$alldata[$i]['optID_hidden']?>"/>
					<input type="hidden" id="prodstyle<?=$i?>" name="prodstyle<?=$i?>" value="<?=$alldata[$i]["prod_style"]?>"/>
					<input type="hidden" id="optcolor<?=$i?>" name="optcolor<?=$i?>" value="<?=$alldata[$i]['optColor']?>"/>
					<input type="hidden" id="optname<?=$i?>" name="optname<?=$i?>" value="<?=$alldata[$i]['optName']?>"/> 
					<div style="width:50px; float:left;"><?=$alldata[$i]["prod_style"]?></div><div style="background-color:#<?=$alldata[$i]['optColor']?>;width:40px;height:15px;float:left;margin:2px;">&nbsp;</div><div style="float:left;width:160px; "><?=$alldata[$i]['optName']?></div></td>
				<td align="center"><input type="hidden" id="optStock<?=$i?>" name="optStock<?=$i?>" value="<?=$alldata[$i]["optStock"]?>"/><?=$alldata[$i]["optStock"];?></td>
				<td align="center">-
				  <input type="hidden" id="adjustment_old<?=$i?>" name="adjustment_old<?=$i?>" value="<?=$alldata[$i]["adjustment_old"]?>"/><? if($doedit) echo '<input type="text" id="adjustment'.$i.'" name="adjustment'.$i.'" size="6" onChange="$(\'update'.$i.'\').value='.$alldata[$i]['optID'].'" value="'.$alldata[$i]['adjustment'].'" /> '.$alldata[$i]["adjustment_old"]; else {echo $alldata[$i]["adjustment"];$total_adjustments+=$alldata[$i]["adjustment"];}?></td>
				<td align="center">
				<? if($doedit) { ?>
				<select name="reason<?=$i?>" id="reason<?=$i?>" onChange="$('update<?=$i?>').value='<?=$alldata[$i]['optID']?>'">
				  <option value="" <?php if(empty($alldata[$i]['reason'])) {echo "SELECTED";} ?>>&lt; Select &gt;</option>
					<? for($j=0;$j<count($reasons_arr);$j++) {	?>
				  <option value="<?=$reasons_arr[$j]['rID']?>" <?php if ($reasons_arr[$j]['rID']==$alldata[$i]['reason']) {echo "SELECTED";} ?>><?=$reasons_arr[$j]['reason']?></option>
				  	<? }?>
				  </select>
				<? }  else echo $alldata[$i]['reason'].' '.$alldata[$i]['adjustment_date']?>
				</td>
			  </tr>
<?php		$rowcounter++;			
		}
	}
	if($rowcounter==0) {
?>
			  <tr bgcolor="<?php print $bgcolor?>"> 
                <td align="center" colspan="4">There are no record returnd from your search!</td>
				
			  </tr>
<?  } ?>  
			  <tr>
				<td align="center"><?=$rowcounter?> Options</td>
				<td align="right">&nbsp;<? if(!$doedit) echo '<strong>Total:</strong>'?></td>
				<td align="center"><? if(!$doedit) echo '<strong>'.$total_adjustments.'</strong>'; else echo '&nbsp;';?></td>
				<td align="right"><input type="hidden" name="maxitems" id="maxitems" value="<?php print $rowcounter?>" /><? if(isset($submit)) echo '<input type="submit" name="back" value="&laquo;Back" /><input type="submit" name="submit2" value="Update" />'; else echo '<input type="submit" name="submit" value="Update"; /> <input type="reset" value="'.$yyReset.'" />'; ?></td>
			  </tr>
			  </form>
			  <!-- <tr> 
                <td align="center"></td>
				<td align="center">&nbsp;</td>
				<td colspan="2" align="center"> <input type="submit" value="<?php print $yyDmpOrd?>" onclick="document.dumpform.details.value='false';" /> </td>
				<td colspan="2" align="center"> <input type="submit" value="<?php print $yyDmpDet?>" onclick="document.dumpform.details.value='true';" /> </td>
			  </tr> -->
			</table>
		  </td>
		</tr>
      </table>

