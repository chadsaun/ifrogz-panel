<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<?php
include('init.php');
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
//session_register('alldata2');
//session_register('alldata');
//confirmation
getadminsettings();
if(isset($submit)) {
	$alldata2='';
	$alldata='';
	for($i=0;$i<$maxitems;$i++) {
		$id=$_POST['update'.$i];
		if(!empty($id)) {			
			$alldata[$i]['pID']=$_POST['pID'.$i];
			$alldata[$i]['invStatsID']=$_POST['invStatsID'.$i];
			$alldata[$i]['pName']=$_POST['pname'.$i];
			$alldata[$i]['pInStock']=$_POST['pstock'.$i];
			$alldata[$i]['pInStock_old']=$_POST['pstock_old'.$i];
			$alldata[$i]['invMin']=$_POST['min_level'.$i];
			$alldata[$i]['invMin_old']=$_POST['min_level_old'.$i];
			$alldata[$i]['invExtendShipping']=$_POST['extend_shipping'.$i];
			$alldata[$i]['invExtendShipping_old']=$_POST['extend_shipping_old'.$i];
			$alldata[$i]['invReorderPoint']=$_POST['reorder_point'.$i];
			$alldata[$i]['invReorderPoint_old']=$_POST['reorder_point_old'.$i];
			$alldata[$i]['invReorderQty']=$_POST['reorder_qty'.$i];
			$alldata[$i]['invReorderQty_old']=$_POST['reorder_qty_old'.$i];
			$alldata[$i]['invDisplayPoint']=$_POST['display_point'.$i];
			$alldata[$i]['invDisplayPoint_old']=$_POST['display_point_old'.$i];
			$alldata[$i]['invBin']=$_POST['bin'.$i];
			$alldata[$i]['invBin_old']=$_POST['bin_old'.$i];
			$alldata[$i]['onhand']=$_POST['onhand'.$i];
			$alldata[$i]['onhand_old']=$_POST['onhand_old'.$i];
		}
	}
	showarray($alldata);
	//$alldata2=$alldata;
	//showarray($alldata2);
}
//update
if(isset($submit2)) {
	$alldata2='';
	include(APPPATH.'views/pages/admin/functions.php');
	for($i=0;$i<$maxitems;$i++) {
		if(!empty($id)) {
			$sql="UPDATE invstats 
				  SET invMin = " . $_POST['min_level'.$i] . ", invExtendShipping = " . $_POST['extend_shipping'.$i] . ", 
				  invReorderPoint = " . $_POST['reorder_point'.$i] . ", invReorderQty = " . $_POST['reorder_qty'.$i] . ", 
				  invDisplayPoint = " . $_POST['display_point'.$i] . ", invBin = " . $_POST['bin'.$i] . " 
				  WHERE id = " . $_POST['invStatsID'.$i];
			mysql_query($sql) or print(mysql_error());
			 
			//record change
//			$sql="INSERT INTO inv_adjustments (iaOptID,iaProdStyle,iaAmt,iaDate,iaReason,iaEmpID,iaOldValue,iaNewValue) 
//					VALUES ('$id','$prodstyle','$adjustment','".date('Y-m-d H:i:s')."','4','".$_SESSION['employee']['id']."',".$optstock_old.",".$_POST['optstock'.$i].")";
//			mysql_query($sql) or print('2='.mysql_error());
			//echo $sql;
			//checks for backorders when inventory is updated
			//updateBackorders($id);
		}
	}
}
if($reporttype=="View" || $reporttype=='' ) {$reporttype="View"; $doedit=FALSE;} else $doedit=TRUE;

if(!isset($submit)) {
	$sql_sections = "SELECT * FROM sections WHERE sectionDisabled = 0 ORDER BY sectionName";
	$res_sections = mysql_query($sql_sections) or print(mysql_error());	

	$sSQL = "SELECT * FROM invstats invs, products p WHERE p.pInvStatsID = invs.id AND p.pDisplay = 1 AND p.pInStock != 0";
	if(!empty($_POST['category'])) {
		$sSQL .= " AND p.pSection = " . $_POST['category'];
	}else{
		// Get first category
		$sql = "SELECT sectionID FROM sections WHERE sectionDisabled = 0 ORDER BY sectionName";
		$res = mysql_query($sql) or print(mysql_error());
		$row = mysql_fetch_assoc($res);
		mysql_free_result($res);
		
		$sSQL .= " AND p.pSection = " . $row['sectionID'];
	}
	$sSQL .= " ORDER BY p.pName";
	
	//echo $sSQL;
	
	$result=mysql_query($sSQL);
	$num_rows_order=mysql_num_rows($result);
	$i=0;
	$alldata='';
	while($rs = mysql_fetch_assoc($result)){	
		$alldata[$i]['pID']=$rs["pID"];
		$alldata[$i]['pID_hidden']=$rs["pID"];
		$alldata[$i]['invStatsID']=$rs["id"];
		$alldata[$i]['pName']=$rs["pName"];
		$alldata[$i]['pInStock']=$rs['pInStock'];
		$alldata[$i]['pInStock_old']=$rs['pInStock'];
		$alldata[$i]['invMin']=$rs['invMin'];
		$alldata[$i]['invMin_old']=$rs['invMin'];
		$alldata[$i]['invExtendShipping']=$rs['invExtendShipping'];
		$alldata[$i]['invExtendShipping_old']=$rs['invExtendShipping'];
		$alldata[$i]['invReorderPoint']=$rs['invReorderPoint'];
		$alldata[$i]['invReorderPoint_old']=$rs['invReorderPoint'];
		$alldata[$i]['invReorderQty']=$rs['invReorderQty'];
		$alldata[$i]['invReorderQty_old']=$rs['invReorderQty'];
		$alldata[$i]['invDisplayPoint']=$rs['invDisplayPoint'];
		$alldata[$i]['invDisplayPoint_old']=$rs['invDisplayPoint'];
		$alldata[$i]['invBin']=$rs['invBin'];
		$alldata[$i]['invBin_old']=$rs['invBin'];
		
		$i++;
	}
} 
if(!empty($alldata)) sort($alldata);
//showarray($alldata);
?>
      <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="">
        <tr>
          <td width="100%" align="center">
			<span name="searchspan" id="searchspan">
            <? if(isset($submit)) { ?>				
					<div style="color:#FF0000; font-size:14px; font-weight:bold;">Please verify that all levels have been changed correctly!</div>
			<? } else { ?>
				<table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="">
				  <form method="post" action="<?=$_SERVER['PHP_SELF']?>" name="psearchform">
				  <input type="hidden" name="powersearch" value="1" />
				  <tr bgcolor="#030133"><td colspan="7"><strong><font color="#E7EAEF">&nbsp;Power Inventory Search</font></strong></td></tr>
				  <tr bgcolor="#E7EAEF"> 
					<td width="18%" align="right"><strong>Report Type :</strong></td>
					<td width="21%" align="left"><select name="reporttype" id="reporttype">
					  <option value="View" <?php if ($reporttype=="View") {echo "SELECTED";} ?>>View Inventory</option>
					  <option value="Edit" <?php if ($reporttype=="Edit") {echo "SELECTED";} ?>>Adjust Inventory</option>
					</select></td>
					<td width="12%" align="left"><div align="right"><strong>Type:</strong></div></td>
					<td width="14%" align="left">
						<select name="category" id="category">
					<?php
						while($row_section = mysql_fetch_assoc($res_sections)) {
					?>
					 		<option value="<?=$row_section['sectionID']?>"<?php if ($row_section['sectionID'] == $_POST['category']) {echo " selected='selected'";} ?>><?=$row_section['sectionName']?></option>
					<?php
						}
					?>
						</select>
					</td>
					<td width="35%" align="right"><!-- <strong>Products ID:</strong> -->
			          <div align="center">
				        <input name="submit3" type="submit" id="submit3" value="<?php print $yySearch?>" />					  
&nbsp;				        <!-- <input name="productid" type="text" id="productid2"> -->					  
			        &nbsp;			      </div></td>
					</tr>
				  <tr bgcolor="#E7EAEF">
					<td colspan="7" align="right">&nbsp;								      </td>
					</tr>
				  </form>
				</table>				
				<? } ?>
			</span>
			<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="">
			  <tr bgcolor="#030133"> 
                <td align="center"><strong><font color="#E7EAEF">Product ID</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">On Hand</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">On Order</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Available</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Min Threshold</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Extend Shipping</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Display Point</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Reorder Point</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Reorder Quantity</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Bin</font></strong></td>
			  </tr>
			  <form method="post" name="mainform" action="<?=$_SERVER['PHP_SELF']?>">
			  	<input name="category" type="hidden" value="<?=$_POST['category']?>">
				<input name="reporttype" type="hidden" value="<?=$reporttype?>">
<?php
	if(is_array($alldata)){
		$rowcounter=0;
		$ordTot=0;
		for($i=0;$i<count($alldata);$i++){
			if($i%2==0) $bgcolor="#EAECEB"; else $bgcolor="#E7EAEF";
			$onOrder=getOnOrderStock($alldata[$i]['pID']);
			if(!isset($submit)) $alldata[$i]['onhand']=$alldata[$i]["pInStock"]+$onOrder;
			if(isset($submit)) $alldata[$i]['pInStock']=$alldata[$i]["onhand"]-$onOrder;	
?>
			  <tr bgcolor="<?php print $bgcolor?>"> 
                <td align="left" valign="middle" width="150">
					<input type="hidden" id="update<?=$i?>" name="update<?=$i?>" value="<?=$alldata[$i]['pID_hidden']?>"/>
					<input type="hidden" id="prodID<?=$i?>" name="pID<?=$i?>" value="<?=$alldata[$i]["pID"]?>"/>
					<input type="hidden" id="invStatsID<?=$i?>" name="invStatsID<?=$i?>" value="<?=$alldata[$i]["invStatsID"]?>"/>
					<input type="hidden" id="pname<?=$i?>" name="pname<?=$i?>" value="<?=$alldata[$i]['pName']?>"/>
					<?=$alldata[$i]['pID']?></td>
				<td align="center">
				
				<input type="hidden" id="onhand_old<?=$i?>" name="onhand_old<?=$i?>" value="<?=$onhand_old?>"/><? if($doedit) echo '<input type="text" id="onhand'.$i.'" name="onhand'.$i.'" value="'.$alldata[$i]['onhand'].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'onhand'.$i.'\').value='.$alldata[$i]['onhand'].';$(\'update'.$i.'\').value=\'\';"> '; else echo $alldata[$i]['onhand'];?>
				
				</td>
				<td align="center"><?=$onOrder?></td>
				<td align="center"><input type="hidden" id="pstock_old<?=$i?>" name="pstock_old<?=$i?>" value="<?=$alldata[$i]["pInStock_old"]?>"/><input type="hidden" id="pstock<?=$i?>" name="pstock<?=$i?>" value="<?=$alldata[$i]["pInStock"]?>"/><?= $alldata[$i]["pInStock"];?></td>
				<td align="center"><input type="hidden" id="min_level_old<?=$i?>" name="min_level_old<?=$i?>" value="<?=$alldata[$i]["invMin_old"]?>"/><? if($doedit) echo '<input type="text" id="min_level'.$i.'" name="min_level'.$i.'" value="'.$alldata[$i]["invMin"].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'min_level'.$i.'\').value='.$alldata[$i]["invMin_old"].';$(\'update'.$i.'\').value=\'\';"> '.$alldata[$i]["invMin_old"]; else echo $alldata[$i]["invMin"];?></td>
				<td align="center"><input type="hidden" id="extend_shipping_old<?=$i?>" name="extend_shipping_old<?=$i?>" value="<?=$alldata[$i]["invExtendShipping_old"]?>"/><? if($doedit) echo '<input type="text" id="extend_shipping'.$i.'" name="extend_shipping'.$i.'" value="'.$alldata[$i]["invExtendShipping"].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'extend_shipping'.$i.'\').value='.$alldata[$i]["invExtendShipping_old"].';$(\'update'.$i.'\').value=\'\';"> '.$alldata[$i]["invExtendShipping_old"]; else echo $alldata[$i]["invExtendShipping"];?></td>
				<td align="center"><input type="hidden" id="display_point_old<?=$i?>" name="display_point_old<?=$i?>" value="<?=$alldata[$i]["invDisplayPoint_old"]?>"/><? if($doedit) echo '<input type="text" id="display_point'.$i.'" name="display_point'.$i.'" value="'.$alldata[$i]["invDisplayPoint"].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'display_point'.$i.'\').value='.$alldata[$i]["invDisplayPoint_old"].';$(\'update'.$i.'\').value=\'\';"> '.$alldata[$i]["invDisplayPoint_old"]; else echo $alldata[$i]["invDisplayPoint"];?></td>
				<td align="center"><input type="hidden" id="reorder_point_old<?=$i?>" name="reorder_point_old<?=$i?>" value="<?=$alldata[$i]["invReorderPoint_old"]?>"/><? if($doedit) echo '<input type="text" id="reorder_point'.$i.'" name="reorder_point'.$i.'" value="'.$alldata[$i]["invReorderPoint"].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'reorder_point'.$i.'\').value='.$alldata[$i]["invReorderPoint_old"].';$(\'update'.$i.'\').value=\'\';"> '.$alldata[$i]["invReorderPoint_old"]; else echo $alldata[$i]["invReorderPoint"];?></td>
				<td align="center"><input type="hidden" id="reorder_qty_old<?=$i?>" name="reorder_qty_old<?=$i?>" value="<?=$alldata[$i]["invReorderQty_old"]?>"/><? if($doedit) echo '<input type="text" id="reorder_qty'.$i.'" name="reorder_qty'.$i.'" value="'.$alldata[$i]["invReorderQty"].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'reorder_qty'.$i.'\').value='.$alldata[$i]["invReorderQty_old"].';$(\'update'.$i.'\').value=\'\';"> '.$alldata[$i]["invReorderQty_old"]; else echo $alldata[$i]["invReorderQty"];?></td>
				<td align="center"><input type="hidden" id="bin_old<?=$i?>" name="bin_old<?=$i?>" value="<?=$alldata[$i]["invBin_old"]?>"/><? if($doedit) echo '<input type="text" id="bin'.$i.'" name="bin'.$i.'" value="'.$alldata[$i]["invBin"].'" size="6" onChange="$(\'update'.$i.'\').value=\''.$alldata[$i]['pID'].'\'" /> <input name="reset'.$i.'" type="button" value="&laquo;" onclick="$(\'bin'.$i.'\').value='.$alldata[$i]["invBin_old"].';$(\'update'.$i.'\').value=\'\';"> '.$alldata[$i]["invBin_old"]; else echo $alldata[$i]["invBin"];?></td>
			  </tr>
<?php		$rowcounter++;			
		}
	} else {
?>
			  <tr bgcolor="<?php print $bgcolor?>"> 
                <td align="center" colspan="9">There are no records returnd from your search!</td>
			  </tr>
<?  } ?>  
			  <tr>
				<td align="center"><?=$rowcounter?> Options</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td align="center">&nbsp;</td>
				<td colspan="3">&nbsp;&nbsp;&nbsp;</td>
				<td colspan="2"><div align="right">
			      <input type="hidden" name="maxitems" value="<?php print $rowcounter?>" />				  
			      <? if(isset($submit)) echo '<input type="submit" name="back" value="&laquo;Back" /><input type="submit" name="submit2" value="Update" />'; else echo '<input type="submit" name="submit" value="Update" /> <input type="reset" value="'.$yyReset.'" />'; ?>
			    </div></td>
				</tr>
			  </form>
			   <?php /*?><tr> 
                <td align="center"></td>
				<td align="center">&nbsp;</td>
				<td colspan="2" align="center"> <input type="submit" value="<?php print $yyDmpOrd?>" onclick="document.dumpform.details.value='false';" /> </td>
				<td colspan="2" align="center"> <input type="submit" value="<?php print $yyDmpDet?>" onclick="document.dumpform.details.value='true';" /> </td>
			  </tr> <?php */?>
			</table>
		  </td>
		</tr>
      </table>
	  
	 <ul>
	 	<li>On Hand: Physical inventory in stock.</li>
	    <li>On Order: Inventory currently held in order that have not been shipped.  </li>
	    <li>Available: Current level of inventory. On Hand minus On Order. </li>
	    <li>Min Threshold:  Amount where a message is displayed on the site that shipping times will increase by the field &quot;Extend Shipping&quot; amount.</li>
	    <li>Display Point: Point where the product will not show up on the site. </li>
	    <li>Reorder Point: At this level the inventory needs to be reordered. </li>
	    <li>Reorder Quanity: How many of this option needs to be reordered.</li>
        <li>&laquo;: Reset to original value. </li>
	 </ul>
     <? $alldata2='';?>