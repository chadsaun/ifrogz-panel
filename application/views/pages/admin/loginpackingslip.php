<style>
 th  {
 	background-color:#030133;
	color:#FFFFFF;
	font-weight:bold;
}

.td1 {
	background-color:#E7EAEF;
	}
</style>

<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');

function is_myfrogz($product_pid) {
	switch ($product_pid) {
		case 'MYFROGZ':
		case 'MF-BB-83':
		case 'MF-BB-852':
		case 'MF-BB-89':
		case 'MF-BB-95':
		case 'MF-BB-97':
		case 'MF-T2G':
			return TRUE;
		default:
			return FALSE;
	}
}

function get_bin($bin, $is_myfrogz = FALSE) {
	$prefix = ($is_myfrogz) ? 'MF*' : '';
	$first_asterisk = strpos($bin, '*');
	$second_asterisk = strpos(substr($bin, $first_asterisk + 1), '*');
	$second_position = ($second_asterisk != 0) ? $second_asterisk : strlen($bin);
	$temp = $prefix.strtoupper(substr($bin, $first_asterisk + 1, $second_position));
	return $temp;
}

//POSTS
$status = $_POST['status'];
$_POST['status'] = $status = '3';
if (is_array($_POST['shipmethod'])) {
	$comma1 = '';
	foreach ($_POST['shipmethod'] as $value) {
		$allshipmethods .= $comma1 . $value;
		$comma1 = ',';
	}
}

if (is_array($_POST['shipmethod'])) {
	$comma2 = '';
	if(is_array($_POST['bin'])) {
		foreach ($_POST['bin'] as $value2) {
			$allbins .= $comma2 . $value2;
			$comma2 = ',';
		}
	} else {
		$_POST['bin'] = array();
	}
}

//They didn't select a bin so we want to select all bins
if ($allbins == "") $bin_all = "all";

include(APPPATH.'views/partials/admin/dbconnection.php');

//GET SHIP TYPES
$sql = 'SELECT
	orders.ordShipType,
	COUNT(DISTINCT orders.ordID) AS shipCount
FROM
	orders
	JOIN cart on cart.cartOrderID = orders.ordID
WHERE
	orders.ordStatus = '.$status.'
GROUP BY
	orders.ordShipType';
//$sql = 'SELECT DISTINCT ordShipType , COUNT( ordID ) AS shipCount FROM orders WHERE ordStatus = '.$status.' GROUP BY ordShipType';
$result = mysql_query($sql);
$arrShip = array();
$shipTotal  = 0;
while ($row = mysql_fetch_assoc($result)) {
	$arrShip[] = $row;
	$shipTotal +=  $row['shipCount'];
}
$arrShipCount = count($arrShip);

//GET BINS
$sql = "SELECT IF(p.pSell=1,p.pBin,o.optBin) AS bin, ord.ordID, c.cartProdID, c.cartProdName, c.cartID, o.optID, co.coOptGroup, o.optName, o.optStyleID, o.optGroup, c.cartProdPrice, p.p_iscert, p.pDownload, p.pWeight, co.coCartOption
		FROM orders ord JOIN cart c ON ord.ordID=c.cartOrderID JOIN products p ON c.cartProdID=p.pID LEFT JOIN cartoptions co ON c.cartID=co.coCartID LEFT JOIN options o ON co.coOptID=o.optID
		WHERE ord.ordStatus = ".$status."
		ORDER BY c.cartID";
$resbin = mysql_query($sql) or print(mysql_error());

$totalbins = 0;
$arrBins = array();
$j = 0;
$k = 0;
$strCustom = '';
$commaCust = '';
while ($rowbin = mysql_fetch_assoc($resbin)) {
	if(strstr($rowbin['cartProdID'],'-Custom')) {
		$cartoption_left5 = (int)substr($rowbin['coCartOption'],0,5);
		$sql_custom = "SELECT *
					FROM uploaded_images  
					WHERE id = ".$cartoption_left5;
		//echo $sql_custom. '<br />';
		$result_custom = mysql_query($sql_custom);
		if( mysql_num_rows($result_custom) > 0){
			$row_custom = mysql_fetch_assoc($result_custom);
			$arrProducts[$i]['custom'] = $row_custom['display_image'];
			$arrProducts[$i]['optName'] = $row_custom['display_image'];
			if ( $row_custom['img_status'] != 'received Logan' ) {
				$strCustom .= $commaCust .  $rowbin['ordID'];
				$commaCust = ',';
			}
		}
		mysql_free_result($result_custom);
	}
	
	if ($rowbin['bin'] == '') $rowbin['bin'] = '-';
	
	if (is_myfrogz($rowbin['cartProdID'])) {
		$rowbin['bin'] = get_bin($rowbin['bin'], TRUE);
	} else {
		$rowbin['bin'] = get_bin($rowbin['bin'], FALSE);
	}
	
	if (!in_array($rowbin['bin'],$arrBins)) {
		
		$arrBins[$j] = $rowbin['bin'];
		$j++;
	}
	
	$arrBinsCnt[$rowbin['bin']] += 1;
	$totalbins += 1;
	
	if (!strstr($ordBins[$rowbin['bin']],$rowbin['ordID'])) {
		if ($ordBins[$rowbin['bin']] == '') {
			$comma = '';
		} else {
			$comma = ',';
		}
		$ordBins[$rowbin['bin']] .=  $comma . $rowbin['ordID'];
		
	}
	
	if (!strstr($ordBinLoc[$rowbin['ordID']],$rowbin['bin'])) {
		if ($ordBinLoc[$rowbin['ordID']] == '') {
			$commabin = '';
		} else {
			$commabin = ',';
		}
		$ordBinLoc[$rowbin['ordID']] .= $commabin.$rowbin['bin'];
	}
}

$arrBinsCount = count($arrBins);

//showarray($ordBins);
//showarray($ordBinLoc);

?>
<script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#selected_orders_form').submit(function() {
			//A psuedo associative array.
			var array = [];
			array["others"] = 0;
			array["MF*IP3G3GS"] = 0;
			array["MF*T2G"] = 0;
			array["MF*BB8300"] = 0;
			array["MF*BB8520"] = 0;
			array["MF*BB8900"] = 0;
			array["MF*BB9550"] = 0;
			array["MF*BB9700"] = 0;
			var checkboxes = $('input[name=orderid[]]');
			var letters = $('div.letter_value');
			for(var i = 0; i < checkboxes.size(); i++) {
				if($(checkboxes[i]).attr('checked')) {
					if($(letters[i]).text() == 'MF*IP3G3GS') {
						array["MF*IP3G3GS"]++;
					} else if($(letters[i]).text() == 'MF*BB8300') {
						array["MF*BB8300"]++;
					} else if($(letters[i]).text() == 'MF*BB8520') {
						array["MF*BB8520"]++;
					} else if($(letters[i]).text() == 'MF*BB8900') {
						array["MF*BB8900"]++;
					} else if($(letters[i]).text() == 'MF*BB9550') {
						array["MF*BB9550"]++;
					} else if($(letters[i]).text() == 'MF*BB9700') {
						array["MF*BB9700"]++;
					} else if($(letters[i]).text() == 'MF*T2G') {
						array["MF*T2G"]++;
					} else {
						array["others"]++;
					}
				}
			}
			for (i in array) {
				if (array[i] > 0 && only_bin_selected(array, i)) {
					//This bin has orders selected and it is a unique MyFrogz bin or general orders.
					if (i == 'others') {
						//They didn't select a MyFrogz bin so we run the default.
						$('#selected_orders_form').attr('action', '/admin/printpackingslips.php');
						return true;
					} else {
						//They only a MyFrogz bin was selected so we go to another page.
						$('#selected_orders_form').attr('action', '/admin/myfrogzprintpickticket.php');
						return true;
					}
				}
			}
			//They didn't select anything so we do nothing.
			return false;
		});
		
		$('#select_bins_and_shipping_form').submit(function() {
			var all_bin = $('#bin_all');
			//alert(all_bin.attr('id'));
			if(all_bin.is(':checked')) {
				select_all("bin",all_bin);
				$('#bin_all').attr('checked', false);
			}
			return true;
		});
		
		$('#clear_all_checkboxes').click(function() {
			if($(this).is(':checked')) {
				$('#all_orders_table input').each(function() {
					$(this).attr('checked', 'checked');
				});
			} else {
				$('#all_orders_table input').each(function() {
					$(this).attr('checked', '');
				});
			}
		});
	});
	
	function select_all(ele,check) {
		contents = $('.'+ele);
		var is_checked = check.attr('checked');
		//alert(check.attr('id')+" is checked ? "+is_checked);
		for(var i = 0; i < contents.length; i++) {
			if(is_checked && !is_myfrogz_bin(contents[i].id)) {
				contents[i].checked = true;
			} else {
				contents[i].checked = false;
			}
		}
	}
	
	function only_bin_selected(array, value) {
		for(i in array) {
			if((i != value) && (array[i] > 0)) {
				alert("You must select only 1 MyFrogz bin at a time.\n"
				+"Myfrogz bins are; MF*IP3G3GS, MF*T2G, MF*BB8300, MF*BB8520, MF*BB8900, MF*BB9550, and MF*BB9700");
				return false;
			}
		}
		return true;
	}
	
	function is_myfrogz_bin(bin) {
		var myfrogz_bins = ["MF*IP3G3GS", "MF*T2G", "MF*BB8300", "MF*BB8520", "MF*BB8900", "MF*BB9550", "MF*BB9700"];
		for(var i = 0; i < myfrogz_bins.length; i++) {
			var temp = 'bin_'+myfrogz_bins[i];
			if(bin == 'bin_'+myfrogz_bins[i])
				return true;
		}
		return false;
	}
</script>





<div style="float:left">
	<div id="left1">
		<div style="padding:5px; background-color:#FFFFFF;">
            <form id="select_bins_and_shipping_form" method="post" action="/admin/loginpackingslip.php"> 
                           
                <h3>Print Packing Slip</h3>    
    
                <hr />
                <h4>Shipping Method</h4>
				<?
				for ($i=0;$i<$arrShipCount;$i++) {
					if (
						//Hong Kong login or Admin user can see the Internation HK shipping type.
						((strcmp($arrShip[$i]['ordShipType'], 'International HK') == 0) && (isPermitted('hong kong')||isPermitted('admin'))) || 
						//Non Hong Kong users will see all the others.
						((strcmp($arrShip[$i]['ordShipType'], 'International HK') != 0) && (!isPermitted('hong kong')))
						) {
				?>
   	                <input class="shipmethod" <?php if (strpos($allshipmethods,$arrShip[$i]['ordShipType'])>-1) {echo "checked=\"checked\"";} ?> id="ship_<?=$arrShip[$i]['ordShipType']?>" name="shipmethod[]" type="checkbox" value="<?=$arrShip[$i]['ordShipType']?>" />
              <label for="ship_<?=$arrShip[$i]['ordShipType']?>"><?=$arrShip[$i]['ordShipType']?> (<?=$arrShip[$i]['shipCount']?>)</label><br />
				<?php
					}
				} 
				?>
               <br /> 
               <hr /> 
               <h4>Bins</h4>
			   	   <input <?php if (!(strcmp($bin_contains, "contains"))) {echo "checked=\"checked\"";} ?> id="bin_contains" name="bin_contains" type="checkbox" value="contains" style="margin-bottom: 5px;" />
				   <label for="bin_contains">Contains</label><hr style="width: 20px;" />
   	               <input <?php if (!(strcmp($bin_all,"all"))) {echo "checked=\"checked\"";} ?> id="bin_all" name="bin_all" type="checkbox" value="all" onclick="select_all('bin',$(this));" />
             <label for="bin_all">All (<?=$totalbins?>)</label><br />
			   <?
				for ($i=0;$i<$arrBinsCount;$i++) { 
			   ?>
   	                <input class="bin" <?php if (strpos($allbins,$arrBins[$i])>-1) {echo "checked=\"checked\"";} ?> id="bin_<?=$arrBins[$i]?>" name="bin[]" type="checkbox" value="<?=$arrBins[$i]?>" /> <label for="bin_<?=$arrBins[$i]?>"><?=$arrBins[$i]?> (<?=$arrBinsCnt[$arrBins[$i]]?>)</label><br />
               
             <? } ?>
               <br /> 
               <hr />
               
               <? if($_SESSION['packingsliplogin']['id']==54) {?>
               <input name="status" type="text" value="<?=$status?>" />
               
               <hr />
               <? } ?>
                <input type="submit" value="<?php print $yySubmit?>">
              </form>
          </div>
    </div>
</div>    
<div id="main">

<?php if(! $success){ ?>
               <div style="margin:10px;"> <font color="#FF0000"><?php print $errmsg?></font>    </div>           
    <?php } ?>

     
<? 

$sqlclose = '';
$sql = "SELECT * FROM orders WHERE ordStatus=".$status;

if ($allshipmethods != "") {
	$sql.=" AND ordShipType IN ('" . str_replace(",","','",$allshipmethods) . "')";
}
if ($allbins != "" && $_POST['bin_all'] == "") {
	$allbinsArray = explode(',',$allbins);
	$l = 0;
	foreach ($allbinsArray AS $value3) {
		if($l == 0) {
			$sql .= " AND (";
			$sqlclose = ")";
		} else $sql .= " OR";
		$sql.=" ordID IN (" . $ordBins[$value3] . ")";
		
		$l++;
	}
	if ($sqlclose != "") $sql .= $sqlclose;
	
	if (empty($_POST['bin_contains'])) {
		foreach($arrBins as $value4) {
			if (!in_array($value4,$allbinsArray)) {
				//echo $ordBins[$value4];
				$sql .= " AND ordID NOT IN (" . $ordBins[$value4] . ")";
			}
		}
	}
}
if ($strCustom != "") {
	$sql .= " AND ordID NOT IN (" . $strCustom . ")";
}

$sql .= " ORDER BY ordShipType, ordID";

$result_ord = mysql_query($sql);
$num_rows = mysql_num_rows($result_ord);
$total_orders = $num_rows;

if ((strcmp($allbins, "") == 0) && (strcmp($_POST['bin_all'], "") == 0)) {
	$num_rows = 0;
}

?>
	<input id="clear_all_checkboxes" type="checkbox"> Clear all checkboxes
    <form id="selected_orders_form" action="" method="post"> 
    <input name="status" type="hidden" value="<?=$status?>" />
    
        <table id="all_orders_table" width="90%"  align="center" cellpadding="1" cellspacing="1" style="margin-top:10px; margin-bottom:20px;">
          <tr>
            <td colspan="2" align="left">
              <? if ($_SESSION['packingsliplogin']['id'] != "" && $num_rows > 0) { ?>
              <strong><input name="Print Selected" type="submit" value="Print" style="font-size:14px;" />
              
            </strong>
              <input name="number" type="text" value="25" size="4"  /> 
              <strong>Orders</strong>
  			<? } ?>
  			</td>
            <td align="left">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="right"><strong>
            <?=$num_rows?> Order(s)</strong></td>
          </tr>
          <tr>
            <th>&nbsp;</th>
            <th>Order #</th>
            <th>Ship Type</th>
            <th>Bins</th>
            <th>Date</th>
          </tr>
        <? 
        $m =1;
		if($num_rows) {
        	while ($row_ord = mysql_fetch_assoc($result_ord)) {
	        ?>  
	          <tr <? if ($m % 2 == 0) echo 'class="td1"'?>>
	            <td align="center"><label>
	              <input type="checkbox" name="orderid[]" value="<?=$row_ord['ordID'] ?>" checked="checked" />
	            </label></td>
	            <td align="center"><?=$row_ord['ordID'] ?></td>
	            <td align="center"><?=$row_ord['ordShipType'] ?></td>
	            <td align="center"><div class="letter_value"><?=$ordBinLoc[$row_ord['ordID']]?></div></td>
	            <td align="center"><?=$row_ord['ordDate'] ?></td>
	          </tr>
	        <?
	        $m++;
	        }
		}
		?> 
        <? if(!$num_rows) {?>
			<tr>
            	<td colspan="5" align="center"><strong>There are <?=$total_orders?> order(s) to print.</strong></td>
            </tr>
		<? } ?> 
        </table>
  </form>
</div>
<script type="text/javascript">
<? 
	if ($bin_all == "all") { ?>
		select_all('bin',$('#bin_all'));
<?  } ?>
</script>
