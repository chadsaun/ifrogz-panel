<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(trim(@$_POST["sessionid"]) != "")
	$thesession = trim(@$_POST["sessionid"]);
else
	$thesession = session_id();
$thesession = mysql_real_escape_string($thesession);
$useEuro=false;
$mcgndtot=0;
$totquant=0;
$shipping=0;
$discounts=0;
$optPriceDiff=0;
$mcpdtxt="";
if(@$incfunctionsdefined==TRUE){
	$alreadygotadmin = getadminsettings();
}else{
	$sSQL = "SELECT countryLCID,countryCurrency,adminStoreURL FROM admin INNER JOIN countries ON admin.adminCountry=countries.countryID WHERE adminID=1";
	$result_cur = mysql_query($sSQL) or print(mysql_error());
	$rs_cur = mysql_fetch_array($result_cur);
	$adminLocale = $rs_cur["countryLCID"];
	$useEuro = ($rs_cur["countryCurrency"]=="EUR");
	$storeurl = $rs_cur["adminStoreURL"];
	if((substr(strtolower($storeurl),0,7) != "http://") && (substr(strtolower($storeurl),0,8) != "https://"))
		$storeurl = "http://" . $storeurl;
	if(substr($storeurl,-1) != "/") $storeurl .= "/";
	mysql_free_result($result_cur);
}
$sSQL = "SELECT cartID,cartProdID,cartProdName,cartProdPrice,cartQuantity FROM cart WHERE cartCompleted=0 AND cartSessionID='" . $thesession . "'";
$result_cart = mysql_query($sSQL) or print(mysql_error());
while($rs_cart = mysql_fetch_assoc($result_cart)){
	$optPriceDiff=0;
	$mcpdtxt .= '<tr><td class="mincart" bgcolor="#F0F0F0">' . $rs_cart["cartQuantity"] . ' ' . $rs_cart["cartProdName"] . "</td></tr>";
	$sSQL = "SELECT SUM(coPriceDiff) AS sumDiff FROM cartoptions WHERE coCartID=" . $rs_cart["cartID"];
	$result2_cart = mysql_query($sSQL) or print(mysql_error());
	$rs2_cart = mysql_fetch_assoc($result2_cart);
	if(! is_null($rs2_cart["sumDiff"])) $optPriceDiff=$rs2_cart["sumDiff"];
	mysql_free_result($result2_cart);
	$subtot = (($rs_cart["cartProdPrice"]+$optPriceDiff)*(int)$rs_cart["cartQuantity"]);
	$totquant++;
	$mcgndtot += $subtot;
}
mysql_free_result($result_cart);
?>
      <table width="130" border="0" class="mincart">
        <!-- <tr>
          <td class="mincart" bgcolor="#F0F0F0" align="center"><img src="/lib/images/littlecart1.gif" align="top" width="16" height="15" alt="<?php print $xxMCSC?>" /> 
            &nbsp;<strong><a href="<?php print $storeurl?>cart.php"><?php print $xxMCSC?></a></strong></td>
        </tr> -->
<?php		if(@$_POST["mode"]=="update"){ ?>
		<tr> 
          <td class="mincart" bgcolor="#F0F0F0" align="center"><?php print $xxMainWn?></td>
        </tr>
<?php		}else{ ?>
        <tr> 
          <td class="mincart" bgcolor="#F0F0F0" align="center"> 
<?php			print $totquant . " " . $xxMCIIC ?></td>
        </tr>
<?php			print $mcpdtxt;
				if($mcpdtxt != "" && @$_SESSION["discounts"] != ""){
					$discounts = (double)$_SESSION["discounts"]; ?>
        <tr> 
          <td class="mincart" bgcolor="#F0F0F0" align="center"><font color="#FF0000"><?php print $xxDscnts . " " . FormatEuroCurrency($discounts)?></font></td>
        </tr>
<?php			}
				if($mcpdtxt != "" && (string)@$_SESSION["xsshipping"] != ""){
					$shipping = (double)$_SESSION["xsshipping"];
					if($shipping==0) $showshipping='<font color="#FF0000"><strong>'.$xxFree.'</strong></font>'; else $showshipping=FormatEuroCurrency($shipping); ?>
        <tr> 
          <td class="mincart" bgcolor="#F0F0F0" align="center"><?php print $xxMCShpE . " " . $showshipping?></td>
        </tr>
<?php			} ?>
        <tr> 
          <td class="mincart" bgcolor="#F0F0F0" align="center"><?php print $xxTotal . " " . FormatEuroCurrency(($mcgndtot+$shipping)-$discounts)?></td>
        </tr>
<?php		} ?>
        <tr> 
          <td class="mincart" bgcolor="#F0F0F0" align="center">&gt; <a href="<?php print $storeurl?>cart.php"><strong><?php print $xxMCCO?></strong></a></td>
        </tr>
</table>