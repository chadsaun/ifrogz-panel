<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');
include(APPPATH.'views/pages/admin/cartmisc.php');
include_once(IFZROOT.'kohana.php');
session_register('order_id_commas');
$lisuccess=0;
if(@$dateadjust=="") $dateadjust=0;
if(@$dateformatstr == "") $dateformatstr = "m/d/Y";
$admindatestr="Y-m-d";
if(@$admindateformat=="") $admindateformat=0;
if($admindateformat==1)
	$admindatestr="m/d/Y";
elseif($admindateformat==2)
	$admindatestr="d/m/Y";
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if(@$_GET["doedit"]=="true") $doedit=TRUE; else $doedit=FALSE;
function editfunc($data,$col,$size){
	global $doedit;
	if($doedit) return('<input type="text" id="' . $col . '" name="' . $col . '" value="' . str_replace('"','&quot;',$data) . '" size="' . $size . '">'); else return($data);
}
function editnumeric($data,$col,$size){
	global $doedit;
	if($doedit) return('<input type="text" id="' . $col . '" name="' . $col . '" value="' . number_format($data,2,'.','') . '" size="' . $size . '">'); else return(FormatEuroCurrency($data));
}
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])!=""){
	$config = RBI_Kohana::config('database.default_ifrogz');
	$config = $config['connection'];
	$db=mysql_connect($config['hostname'], $config['username'], $config['password']);
	mysql_select_db($config['database']) or die ('RBI connection failed.</td></tr></table></body></html>');
	$rbiSQL = 'SELECT * 
			   FROM employee 
			   WHERE username="'.mysql_real_escape_string(unstripslashes(trim(@$_COOKIE["WRITECKL"]))).'" and password="'.mysql_real_escape_string(unstripslashes(trim(@$_COOKIE["WRITECKP"]))).'"';
	$rs_rbi = mysql_query($rbiSQL);
	if(mysql_num_rows($rs_rbi) > 0) {
		@$_SESSION["loggedon"] = $storesessionvalue;
	}else{
		$lisuccess=2;
	}
	mysql_free_result($rs_rbi);
	
	include(APPPATH.'views/partials/admin/dbconnection.php');
}
if(($_SESSION["loggedon"] != $storesessionvalue && $lisuccess!=2) || @$disallowlogin==TRUE) exit();
if(@$htmlemails==TRUE) $emlNl = "<br />"; else $emlNl="\n";
if($lisuccess==2){
?>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><p>&nbsp;</p><p>&nbsp;</p>
				  <p><strong><?php print $yyOpFai?></strong></p><p>&nbsp;</p>
				  <p><?php print $yyCorCoo?> <?php print $yyCorLI?> <a href="/admin/login.php"><?php print $yyClkHer?></a>.</p>
				</td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
}else{
$success=true;
$alreadygotadmin = getadminsettings();
if(@$_POST["updatestatus"]=="1"){
	mysql_query("UPDATE orders SET ordStatusInfo='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ordStatusInfo"]))) . "' WHERE ordID=" . @$_POST["orderid"]) or print(mysql_error());
}elseif(@$_GET["id"] != ""){
	if(@$_POST["delccdets"] != ""){
		mysql_query("UPDATE orders SET ordCNum='' WHERE ordID=" . @$_GET["id"]);
	}
	$sSQL = "SELECT cartProdId,cartProdName,cartProdPrice,cartQuantity,cartID FROM cart WHERE cartOrderID=" . $_GET["id"];
	$allorders = mysql_query($sSQL) or print(mysql_error());
}else{
	// Delete old uncompleted orders.
	if($delccafter != 0){
		$sSQL = "UPDATE orders SET ordCNum='' WHERE ordDate<'" . date("Y-m-d H:i:s", time()-($delccafter*60*60*24)) . "'";
		mysql_query($sSQL) or print(mysql_error());
	}
	if($delAfter != 0){
		$sSQL = "SELECT cartOrderID,cartID FROM cart WHERE cartCompleted=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()-($delAfter*60*60*24)) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result)>0){
			$delStr="";
			$delOptions="";
			$addcomma = "";
			while($rs = mysql_fetch_assoc($result)){
				$delStr .= $addcomma . $rs["cartOrderID"];
				$delOptions .= $addcomma . $rs["cartID"];
				$addcomma = ",";
			}
			mysql_query("DELETE FROM orders WHERE ordID IN (" . $delStr . ")") or print(mysql_error());
			mysql_query("DELETE FROM cartoptions WHERE coCartID IN (" . $delOptions . ")") or print(mysql_error());
			mysql_query("DELETE FROM cart WHERE cartID IN (" . $delOptions . ")") or print(mysql_error());
		}
		mysql_free_result($result);
	}else{
		$sSQL = "SELECT cartOrderID,cartID FROM cart WHERE cartCompleted=0 AND cartOrderID=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()-(3*60*60*24)) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result)>0){
			$delStr="";
			$delOptions="";
			$addcomma = "";
			while($rs = mysql_fetch_assoc($result)){
				$delStr .= $addcomma . $rs["cartOrderID"];
				$delOptions .= $addcomma . $rs["cartID"];
				$addcomma = ",";
			}
			mysql_query("DELETE FROM cartoptions WHERE coCartID IN (" . $delOptions . ")") or print(mysql_error());
			mysql_query("DELETE FROM cart WHERE cartID IN (" . $delOptions . ")") or print(mysql_error());
		}
		mysql_free_result($result);
	}
	$numstatus=0;
	$sSQL = "SELECT statID,statPrivate FROM orderstatus WHERE statPrivate<>'' ORDER BY statID";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_assoc($result)){
		$allstatus[$numstatus++]=$rs;
	}
	mysql_free_result($result);
}
if(@$_POST["updatestatus"]=="1"){
?>
<script language="JavaScript" type="text/javascript">
<!--
setTimeout("history.go(-2);",1100);
// -->
</script>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyUpdSuc?></strong><br /><br /><?php print $yyNowFrd?><br /><br />
                        <?php print $yyNoAuto?> <a href="javascript:history.go(-2)"><strong><?php print $yyClkHer?></strong></a>.<br /><br />
						<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
}elseif(@$_POST["doedit"] == "true"){
	if(!empty($_POST['clone'])) {
		// GET ALL THE INFORMATION TO CLONE THE ORDER
		//   - To clone an order we need to duplicate the order's information in the orders, cart, and cartoptions tables
		$aNewOrder = array();
		
		$sql_orders = "SELECT * FROM orders WHERE ordID = " . $_POST["orderid"] ;
		$res_orders = mysql_query($sql_orders) or print(mysql_error());
		$row_orders = mysql_fetch_assoc($res_orders);
		$aNewOrder["orders"] = $row_orders;
		
		$sql_cart = "SELECT * FROM cart c, cartoptions co 
					 WHERE c.cartID = co.coCartID
					 AND c.cartOrderID = " . $_POST["orderid"] . "
					 ORDER BY c.cartID, c.cartProdID";
		$res_cart = mysql_query($sql_cart) or print(mysql_error());
		$prod_id = '';
		$i=0;
		$j=0;
		$isfirst = true;
		while($row_cart = mysql_fetch_assoc($res_cart)) {
			if($isfirst) {
				$prod_id = $row_cart["cartProdID"];
			}
			if(($prod_id != $row_cart["cartProdID"]) && !$isfirst) {
				$i++;
				$j=0;
				$prod_id = $row_cart["cartProdID"];
			
				$aNewOrder["cart"][$i]["cartID"] = $row_cart["cartID"];
				$aNewOrder["cart"][$i]["cartSessionID"] = $row_cart["cartSessionID"];
				$aNewOrder["cart"][$i]["cartProdID"] = $row_cart["cartProdID"];
				$aNewOrder["cart"][$i]["cartProdName"] = $row_cart["cartProdName"];
				$aNewOrder["cart"][$i]["cartProdPrice"] = $row_cart["cartProdPrice"];
				$aNewOrder["cart"][$i]["cartDateAdded"] = $row_cart["cartDateAdded"];
				$aNewOrder["cart"][$i]["cartQuantity"] = $row_cart["cartQuantity"];
				$aNewOrder["cart"][$i]["cartOrderID"] = $row_cart["cartOrderID"];
				$aNewOrder["cart"][$i]["cartCompleted"] = $row_cart["cartCompleted"];
				$aNewOrder["cart"][$i]["cartCustID"] = $row_cart["cartCustID"];
				
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coID"] = $row_cart["coID"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coCartID"] = $row_cart["coCartID"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coOptID"] = $row_cart["coOptID"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coOptGroup"] = $row_cart["coOptGroup"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coCartOption"] = $row_cart["coCartOption"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coPriceDiff"] = $row_cart["coPriceDiff"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coWeightDiff"] = $row_cart["coWeightDiff"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coExtendShipping"] = $row_cart["coExtendShipping"];
			}else{
				$aNewOrder["cart"][$i]["cartID"] = $row_cart["cartID"];
				$aNewOrder["cart"][$i]["cartSessionID"] = $row_cart["cartSessionID"];
				$aNewOrder["cart"][$i]["cartProdID"] = $row_cart["cartProdID"];
				$aNewOrder["cart"][$i]["cartProdName"] = $row_cart["cartProdName"];
				$aNewOrder["cart"][$i]["cartProdPrice"] = $row_cart["cartProdPrice"];
				$aNewOrder["cart"][$i]["cartDateAdded"] = $row_cart["cartDateAdded"];
				$aNewOrder["cart"][$i]["cartQuantity"] = $row_cart["cartQuantity"];
				$aNewOrder["cart"][$i]["cartOrderID"] = $row_cart["cartOrderID"];
				$aNewOrder["cart"][$i]["cartCompleted"] = $row_cart["cartCompleted"];
				$aNewOrder["cart"][$i]["cartCustID"] = $row_cart["cartCustID"];
				
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coID"] = $row_cart["coID"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coCartID"] = $row_cart["coCartID"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coOptID"] = $row_cart["coOptID"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coOptGroup"] = $row_cart["coOptGroup"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coCartOption"] = $row_cart["coCartOption"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coPriceDiff"] = $row_cart["coPriceDiff"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coWeightDiff"] = $row_cart["coWeightDiff"];
				$aNewOrder["cart"][$i]["cartoptions"][$j]["coExtendShipping"] = $row_cart["coExtendShipping"];
				
				$j++;
			}
			$isfirst = false;
		}
		
		//showarray($aNewOrder); exit();
		
		// UPDATE SOME OF THE INFORMATION FOR THE CLONED ORDER
		$aNewOrder["orders"]["ordID"] = '';
		$aNewOrder["orders"]["ordSessionID"] = session_id();
		$aNewOrder["orders"]["ordAuthNumber"] = 'CLONED';
		$aNewOrder["orders"]["ordTransID"] = 0;
		$aNewOrder["orders"]["ordShipping"] = 0;
		$aNewOrder["orders"]["ordStateTax"] = 0;
		$aNewOrder["orders"]["ordCountryTax"] = 0;
		$aNewOrder["orders"]["ordHSTTax"] = 0;
		$aNewOrder["orders"]["ordHandling"] = 0;
		$aNewOrder["orders"]["ordTotal"] = 0;
		$aNewOrder["orders"]["ordDate"] = date("Y-m-d H:i:s");
		$aNewOrder["orders"]["ordIP"] = $_SERVER['REMOTE_ADDR'];
		$aNewOrder["orders"]["ordDiscount"] = 0;
		$aNewOrder["orders"]["ordDiscountText"] = '';
		$aNewOrder["orders"]["ordStatus"] = 2;
		$aNewOrder["orders"]["ordStatusDate"] = date("Y-m-d H:i:s");
		$aNewOrder["orders"]["ordStatusInfo"] = '';
		$aNewOrder["orders"]["order_changed"] = 'no';
		
		for($i=0; $i<count($aNewOrder["cart"]); $i++) {
			$aNewOrder["cart"][$i]["cartSessionID"] = session_id();
			$aNewOrder["cart"][$i]["cartDateAdded"] = date("Y-m-d H:i:s");
		}
		
		//showarray($aNewOrder); exit();
		
		// CREATE CLONED ORDER
		$clone_error = false;
		$sql_cr_ord = "INSERT INTO orders ( ordSessionID , ordName , ordAddress , ordAddress2 , ordCity , ordState , 
											ordZip , ordCountry , ordEmail , ordPhone , ordShipName , ordShipAddress , 
											ordShipAddress2 , ordShipCity , ordShipState , ordShipZip , ordShipCountry , 
											ordAuthNumber , ordAffiliate , ordPayProvider , ordTransID , ordShipping , 
											ordStateTax , ordCountryTax , ordHSTTax , ordHandling , ordShipType , ordTotal , 
											ordDate , ordIP , ordDiscount , ordDiscountText , ordExtra1 , ordExtra2 , ordAddInfo , 
											ordCNum , ordComLoc , ordStatus , ordStatusDate , ordStatusInfo , ordPoApo , 
											ordShipPoApo , ordHowFound , ordSupportInfo , order_changed , ordPmtMessage , 
											ordCCType , ord_cert_id , ord_cert_amt , ordExtra3 , ordExtra4 , ordExtra5 , ordEID , 
											ordEOrderID )
						VALUES ( '".$aNewOrder["orders"]["ordSessionID"]."' , 
								 '".$aNewOrder["orders"]["ordName"]."' , '".$aNewOrder["orders"]["ordAddress"]."' , 
								 '".$aNewOrder["orders"]["ordAddress2"]."' , '".$aNewOrder["orders"]["ordCity"]."' , 
								 '".$aNewOrder["orders"]["ordState"]."' , '".$aNewOrder["orders"]["ordZip"]."' , 
								 '".$aNewOrder["orders"]["ordCountry"]."' , '".$aNewOrder["orders"]["ordEmail"]."' , 
								 '".$aNewOrder["orders"]["ordPhone"]."' , '".$aNewOrder["orders"]["ordShipName"]."' , 
								 '".$aNewOrder["orders"]["ordShipAddress"]."' , '".$aNewOrder["orders"]["ordShipAddress2"]."' , 
								 '".$aNewOrder["orders"]["ordShipCity"]."' , '".$aNewOrder["orders"]["ordShipState"]."' , 
								 '".$aNewOrder["orders"]["ordShipZip"]."' , '".$aNewOrder["orders"]["ordShipCountry"]."' , 
								 '".$aNewOrder["orders"]["ordAuthNumber"]."' , '".$aNewOrder["orders"]["ordAffiliate"]."' , 
								 '".$aNewOrder["orders"]["ordPayProvider"]."' , '".$aNewOrder["orders"]["ordTransID"]."' , 
								 '".$aNewOrder["orders"]["ordShipping"]."' , '".$aNewOrder["orders"]["ordStateTax"]."' , 
								 '".$aNewOrder["orders"]["ordCountryTax"]."' , '".$aNewOrder["orders"]["ordHSTTax"]."' , 
								 '".$aNewOrder["orders"]["ordHandling"]."' , '".$aNewOrder["orders"]["ordShipType"]."' , 
								 '".$aNewOrder["orders"]["ordTotal"]."' , '".$aNewOrder["orders"]["ordDate"]."' , 
								 '".$aNewOrder["orders"]["ordIP"]."' , '".$aNewOrder["orders"]["ordDiscount"]."' , 
								 '".$aNewOrder["orders"]["ordDiscountText"]."' , '".$aNewOrder["orders"]["ordExtra1"]."' , 
								 '".$aNewOrder["orders"]["ordExtra2"]."' , '".mysql_real_escape_string($aNewOrder["orders"]["ordAddInfo"])."' , 
								 '".$aNewOrder["orders"]["ordCNum"]."' , '".$aNewOrder["orders"]["ordComLoc"]."' , 
								 '".$aNewOrder["orders"]["ordStatus"]."' , '".$aNewOrder["orders"]["ordStatusDate"]."' , 
								 '".mysql_real_escape_string($aNewOrder["orders"]["ordStatusInfo"])."' , '".$aNewOrder["orders"]["ordPoApo"]."' , 
								 '".$aNewOrder["orders"]["ordShipPoApo"]."' , '".$aNewOrder["orders"]["ordHowFound"]."' , 
								 '".mysql_real_escape_string($aNewOrder["orders"]["ordSupportInfo"])."' , '".$aNewOrder["orders"]["order_changed"]."' , 
								 '".$aNewOrder["orders"]["ordPmtMessage"]."' , '".$aNewOrder["orders"]["ordCCType"]."' , 
								 '".$aNewOrder["orders"]["ord_cert_id"]."' , '".$aNewOrder["orders"]["ord_cert_amt"]."' , 
								 '".$aNewOrder["orders"]["ordExtra3"]."' , '".$aNewOrder["orders"]["ordExtra4"]."' , 
								 '".$aNewOrder["orders"]["ordExtra5"]."' , '".$aNewOrder["orders"]["ordEID"]."' , 
								 '".$aNewOrder["orders"]["ordEOrderID"]."' )";
		//echo $sql_cr_ord; exit();
		$res_cr_ord = mysql_query($sql_cr_ord);
		if(!$res_cr_ord) {
			print(mysql_error());
			$clone_error = true;
		}
		$ordID = mysql_insert_id();
		$aNewOrder["orders"]["ordID"] = $ordID;
		
		if(!$clone_error) {
			for($i=0; $i<count($aNewOrder["cart"]); $i++) {	
				$aNewOrder["cart"][$i]["cartOrderID"] = $ordID;
				$sql_cr_cart = "INSERT INTO cart ( cartSessionID , cartProdID , cartProdName , cartProdPrice , cartDateAdded , 
												   cartQuantity , cartOrderID , cartCompleted )
								VALUES ( '".session_id()."' , '".$aNewOrder["cart"][$i]["cartProdID"]."' , 
										 '".$aNewOrder["cart"][$i]["cartProdName"]."' , '".$aNewOrder["cart"][$i]["cartProdPrice"]."' , 
										 '".date("Y-m-d H:i:s")."' , '".$aNewOrder["cart"][$i]["cartQuantity"]."' , 
										 '".$aNewOrder["cart"][$i]["cartOrderID"]."' , 1 )";
				//echo $sql_cr_cart; exit();
				$res_cr_cart = mysql_query($sql_cr_cart) or print(mysql_error());
				$cart_id = mysql_insert_id();
				$aNewOrder["cart"][$i]["cartID"] = $cart_id;
				
				for($j=0; $j<count($aNewOrder["cart"][$i]["cartoptions"]); $j++) {
					$sql_cr_cartoptions = "INSERT INTO cartoptions ( coCartID , coOptID , coOptGroup , coCartOption , coPriceDiff , 
																	 coWeightDiff , coExtendShipping )
										   VALUES ( ".$aNewOrder["cart"][$i]["cartID"]." , 
													".$aNewOrder["cart"][$i]["cartoptions"][$j]["coOptID"]." , 
													'".$aNewOrder["cart"][$i]["cartoptions"][$j]["coOptGroup"]."' , 
													'".$aNewOrder["cart"][$i]["cartoptions"][$j]["coCartOption"]."' , 
													".$aNewOrder["cart"][$i]["cartoptions"][$j]["coPriceDiff"]." , 
													".$aNewOrder["cart"][$i]["cartoptions"][$j]["coWeightDiff"]." , 
													'".$aNewOrder["cart"][$i]["cartoptions"][$j]["coExtendShipping"]."' )";
					//echo $sql_cr_cartoptions; exit();
					$res_cr_cartoptions = mysql_query($sql_cr_cartoptions) or print(mysql_error());
				}
			}
			
			// UPDATE INVENTORY
			$sSQL="SELECT cartID,cartProdID,cartQuantity,pSell FROM cart INNER JOIN products ON cart.cartProdID=products.pID WHERE cartOrderID='" . $ordID . "'";
			$result1 = mysql_query($sSQL) or print(mysql_error());
			while($rs1 = mysql_fetch_array($result1)){
				if(($rs1["pSell"] & 2) == 2){
					// Determine extended shipping
					$sSQL2 = "SELECT coID,optStock,cartQuantity,coOptID,optExtend_shipping,optMin FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2";
					// ADDED by Chad - Fix to allow quantity change for custom screenz
					if(eregi("^[a-z]{1,3}-Custom$",$pID)) {
						$sSQL2 .= " OR optType=3";
					}
					// ADD ENDED
					$sSQL2 .= ") AND cartID='" . $rs1['cartID'] . "'";
					$result2 = mysql_query($sSQL2) or print(mysql_error());
					if(mysql_num_rows($result2)>0){
						while($rs2 = mysql_fetch_assoc($result2)){
							$pInStock = (int)$rs2["optStock"]+1000;
							$actualpInStock = (int)$rs2["optStock"];
							$extend_shipping = $rs2["optExtend_shipping"];//extends shipping time, displayed in the cart
							$min = $rs2["optMin"];//sets how many in stock above zero the extend_shipping is displayed in the cart 
							$coID = $rs2["coID"];
							$totQuant = 0;
							$cartQuantity = (int)$rs2["cartQuantity"];
							$sSQL3 = "SELECT SUM(cartQuantity) AS cartQuant FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID WHERE cartCompleted=0 AND cartCustID=0 AND coOptID=" . $rs2["coOptID"];
							$result3 = mysql_query($sSQL3) or print(mysql_error());
							if($rs3 = mysql_fetch_assoc($result3))
								if(! is_null($rs3["cartQuant"])) $totQuant = (int)$rs3["cartQuant"];
							mysql_free_result($result3);
							$extend='';
							//echo '<div style"position:absolute; z-index:100;>actual='.$actualpInStock.' total qty='.$totQuant. ' cart qty='.$cartQuantity.' min='. $min. ' new qty='.abs((int)$objValue).'</div>';
							if(($actualpInStock - $totQuant + $cartQuantity - $min) < 0) {
								$extend = $extend_shipping;
							}
							$sql_co="UPDATE cartoptions SET coExtendShipping='$extend' WHERE coID=".$coID;
								mysql_query($sql_co);
						}
					}
					// End of Determine extended shipping
					
					$sSQL = "SELECT coOptID FROM cartoptions INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND coCartID=" . $rs1["cartID"];
					$result2 = mysql_query($sSQL) or print(mysql_error());
					while($rs2 = mysql_fetch_array($result2)){
						$sql = "SELECT * FROM options WHERE optID = " . $rs2["coOptID"];
						$res = mysql_query($sql);
						$options = mysql_fetch_assoc($res);
						
						$sSQL = "UPDATE options SET optStock=optStock-" . $rs1["cartQuantity"] . " WHERE optID=" . $rs2["coOptID"];
						mysql_query($sSQL) or print(mysql_error());
						
						$sql = "SELECT p.*
								FROM products p 
									JOIN prodoptions po ON p.pID = po.poProdID
									JOIN optiongroup og ON po.poOptionGroup = og.optGrpID
									JOIN options o ON og.optGrpID = o.optGroup
								WHERE o.optID = '".$rs2["coOptID"]."'";
						$res = mysql_query($sql);
						$prod = mysql_fetch_assoc($res);
						
						$prodstyle = $prod["pID"] . "-" . $options['optStyleID'];
						$newvalue = $options['optStock'] - $rs1["cartQuantity"];
						
						// RECORD INVENTORY CHANGE
						$sql3 = "INSERT INTO inv_adjustments (iaOptID, iaAmt, iaDate, iaProdStyle, iaOldValue, iaNewValue, iareason, iaEmpID)
								 VALUES (".$rs2["coOptID"].", -".$rs1["cartQuantity"].", '".date('Y-m-d H:i:s')."', '$prodstyle', ".$options['optStock'].", $newvalue, 8, '".$_SESSION['employee']['id']."')";
						$res3 = mysql_query($sql3);
					}
					mysql_free_result($result2);
				}else{
					$sSQL = "UPDATE products SET pInStock=pInStock-" . $rs1["cartQuantity"] . " WHERE pID='" . $rs1["cartProdID"] . "'";
					mysql_query($sSQL) or print(mysql_error());
				}
			}
			mysql_free_result($result1);
					
			// INSERT LOCATION
			setNewLocation(2,$ordID);
		}
		
		if($clone_error) {
			$pg_msg = "There was a problem cloning your order.";
		}else{
			$pg_msg = "Your order has been cloned successfully.";
?>
<script language="JavaScript" type="text/javascript">
<!--
//setTimeout("history.go(-2);",1100);
setTimeout("window.location='/admin/orders.php?id=<?=$ordID?>&doedit=true'",4000);
// -->
</script>
<?php
		}
		
		if(!$clone_error) {
			$auto_link = "/admin/orders.php?id=$ordID&doedit=true";
		}else{
			$auto_link = "/admin/orders.php";
		}
?>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?=$pg_msg?></strong><br /><br />You will now be forwarded to view the order.<br /><br />
                        <?php print $yyNoAuto?> <a href="<?=$auto_link?>"><strong><?php print $yyClkHer?></strong></a>.<br /><br />
						<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
	}else{
		$OWSP = "";
		$sSQL = "SELECT ordSessionID FROM orders WHERE ordID='" . $_POST["orderid"] . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_array($result);
		$thesessionid = $rs["ordSessionID"];
		mysql_free_result($result);
		$sSQL = "UPDATE orders SET ";
		$sSQL .= "ordName='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["name"]))) . "',";
		$sSQL .= "ordAddress='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["address"]))) . "',";
		if(@$useaddressline2==TRUE) $sSQL .= "ordAddress2='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["address2"]))) . "',";
		$sSQL .= "ordCity='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["city"]))) . "',";
		$sSQL .= "ordPoApo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["APO"]))) . "',";
		$sSQL .= "ordState='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["state"]))) . "',";
		$sSQL .= "ordZip='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["zip"]))) . "',";
		$sSQL .= "ordCountry='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["country"]))) . "',";
		$sSQL .= "ordEmail='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["email"]))) . "',";
		$sSQL .= "ordPhone='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["phone"]))) . "',";
		$sSQL .= "ordShipName='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["sname"]))) . "',";
		$sSQL .= "ordShipAddress='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["saddress"]))) . "',";
		if(@$useaddressline2==TRUE) $sSQL .= "ordShipAddress2='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["saddress2"]))) . "',";
		$sSQL .= "ordShipCity='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["scity"]))) . "',";
		$sSQL .= "ordShipPoApo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ShipAPO"]))) . "',";
		$sSQL .= "ordShipState='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["sstate"]))) . "',";
		$sSQL .= "ordShipZip='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["szip"]))) . "',";
		$sSQL .= "ordShipCountry='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["scountry"]))) . "',";
		$sSQL .= "ordShipType='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["shipmethod"]))) . "',";
		$sSQL .= "ordIP='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ipaddress"]))) . "',";
		$ordComLoc = 0;
		if(trim(@$_POST["commercialloc"])=="Y") $ordComLoc = 1;
		if(trim(@$_POST["wantinsurance"])=="Y") $ordComLoc += 2;
		$sSQL .= "ordComLoc=" . $ordComLoc . ",";
		$sSQL .= "ordAffiliate='" . trim(@$_POST["PARTNER"]) . "',";
		$sSQL .= "ordAddInfo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ordAddInfo"]))) . "',";
		$sSQL .= "ordStatusInfo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ordStatusInfo"]))) . "',";
		$sSQL .= "ordSupportInfo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ordSupportInfo"]))) . "',";
		$sSQL .= "order_changed='yes',";
		$sSQL .= "ordDiscountText='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["discounttext"]))) . "',";
		$sSQL .= "ordExtra1='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ordextra1"]))) . "',";
		$sSQL .= "ordExtra2='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ordextra2"]))) . "',";
		$sSQL .= "ordShipping='" . mysql_real_escape_string(trim(@$_POST["ordShipping"])) . "',";
		$sSQL .= "ordStateTax='" . mysql_real_escape_string(trim(@$_POST["ordStateTax"])) . "',";
		$sSQL .= "ordCountryTax='" . mysql_real_escape_string(trim(@$_POST["ordCountryTax"])) . "',";
		if(@$canadataxsystem==TRUE) $sSQL .= "ordHSTTax='" . mysql_real_escape_string(trim(@$_POST["ordHSTTax"])) . "',";
		$sSQL .= "ordDiscount='" . mysql_real_escape_string(trim(@$_POST["ordDiscount"])) . "',";
		$sSQL .= "ordHandling='" . mysql_real_escape_string(trim(@$_POST["ordHandling"])) . "',";
		$sSQL .= "ordAuthNumber='" . mysql_real_escape_string(trim(@$_POST["ordAuthNumber"])) . "',";
		$sSQL .= "ordTransID='" . mysql_real_escape_string(trim(@$_POST["ordTransID"])) . "',";
		$sSQL .= "ordTotal='" . mysql_real_escape_string(trim(@$_POST["ordtotal"])) . "',";
		$sSQL .= "ord_cert_amt='" . mysql_real_escape_string(trim(@$_POST["ord_cert_amt"])) . "'";
		$sSQL .= " WHERE ordID='" . $_POST["orderid"] . "'";
		mysql_query($sSQL) or print(mysql_error());
	
		foreach($_POST as $objItem => $objValue){
			//print $objItem . " : " . $objValue . "<br>";
			if(substr($objItem,0,6)=="prodid"){
				$idno = (int)substr($objItem, 6);
				$cartid = trim(@$_POST["cartid" . $idno]);
				$prodid = trim(@$_POST["prodid" . $idno]);
				$quant = trim(@$_POST["quant" . $idno]);
				$theprice = trim(@$_POST["price" . $idno]);
				$prodname = trim(@$_POST["prodname" . $idno]);
				$delitem = trim(@$_POST["del_" . $idno]);
				if($delitem=="yes"){
					mysql_query("DELETE FROM cart WHERE cartID=" . $cartid) or print(mysql_error());
					mysql_query("DELETE FROM cartoptions WHERE coCartID=" . $cartid) or print(mysql_error());
					$cartid = "";
				}elseif($cartid != ""){
					$sSQL = "UPDATE cart SET cartProdID='" . mysql_real_escape_string(trim(unstripslashes($prodid))) . "',cartProdPrice=" . $theprice . ",cartProdName='" . mysql_real_escape_string(trim(unstripslashes($prodname))) . "',cartQuantity=" . $quant . " WHERE cartID=" . $cartid;
					mysql_query($sSQL) or print(mysql_error());
					mysql_query("DELETE FROM cartoptions WHERE coCartID=" . $cartid) or print(mysql_error());
				}else{
					$sSQL = "INSERT INTO cart (cartSessionID,cartProdID,cartQuantity,cartCompleted,cartProdName,cartProdPrice,cartOrderID,cartDateAdded) VALUES (";
					$sSQL .= "'" . $thesessionid . "',";
					$sSQL .= "'" . mysql_real_escape_string(trim(unstripslashes($prodid))) . "',";
					$sSQL .= $quant . ",";
					$sSQL .= "1,";
					$sSQL .= "'" . mysql_real_escape_string(trim(unstripslashes($prodname))) . "',";
					$sSQL .= "'" . $theprice . "',";
					$sSQL .= @$_POST["orderid"] . ",";
					$sSQL .= "'" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "')";
					mysql_query($sSQL) or print(mysql_error());
					$cartid = mysql_insert_id();
				}
				if($cartid != ""){
					$optprefix = "optn" . $idno . '_';
					$prefixlen = strlen($optprefix);
					foreach($_POST as $kk => $kkval){
						if(substr($kk,0,$prefixlen)==$optprefix && trim($kkval) != ''){
							$optidarr = split('\|', $kkval);
							$optid = $optidarr[0];
							if(@$_POST["v" . $kk] == ""){
								$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)."," . $OWSP . "optPriceDiff,optWeightDiff,optType,optFlags FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($kkval) . "'";
								$result = mysql_query($sSQL) or print(mysql_error());
								if($rs = mysql_fetch_array($result)){
									if(abs($rs["optType"]) != 3){
										$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartid . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string($rs[getlangid("optName",32)]) . "',";
										$sSQL .= $optidarr[1] . ",0)";
									}else
										$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartid . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','',0,0)";
									mysql_query($sSQL) or print(mysql_error());
								}
								mysql_free_result($result);
							}else{
								$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)." FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($kkval) . "'";
								$result = mysql_query($sSQL) or print(mysql_error());
								$rs = mysql_fetch_array($result);
								$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartid . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string(unstripslashes(trim(@$_POST["v" . $kk]))) . "',0,0)";
								mysql_query($sSQL) or print(mysql_error());
								mysql_free_result($result);
							}
						}
					}
				}
			}
		}
?>
<script language="JavaScript" type="text/javascript">
<!--
setTimeout("history.go(-2);",1100);
// -->
</script>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyUpdSuc?></strong><br /><br /><?php print $yyNowFrd?><br /><br />
                        <?php print $yyNoAuto?> <a href="javascript:history.go(-2)"><strong><?php print $yyClkHer?></strong></a>.<br /><br />
						<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
	}
?>
			<tr>
			  <td align="center" colspan="6">&nbsp;<br /></td>
			</tr>
		  </table>
		  </td>
		</tr>
<?php	if($isprinter && @$invoicefooter != ""){ ?>
		<tr> 
          <td width="100%"><?php print $invoicefooter?></td>
		</tr>
<?php	}elseif($doedit){ ?>
		<tr> 
          <td align="center" width="100%">&nbsp;<br />&nbsp;<br />
          &nbsp;</td>
		</tr>
<?php	} ?>
	  </table>
<?php
	if($doedit) print '</form>';
	if($doedit){
	// ADDED by Chad Jun-06-06
	// PRICE ADJUSTMENTS
?>
	



	</table>
<?php
if(strstr($_SESSION['employee']['permissions'],"all") || $_SESSION['employee']['id']==9 || $_SESSION['employee']['id']==12 || $_SESSION['employee']['id']==2) {
?>
<div id="div_trans" style="display: none">
	<form id="aim_frm" name="aim_frm" method="post" action="/admin/ordersprocess.php" onsubmit="return checkAIM(this);">
	<table cellpadding="3" cellspacing="0" border="1" style="margin: 5px auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
		<tr style="background-color: #BFC9E0; color: #194C7F">
			<td colspan="4" style="text-align: center; font-weight: bold; font-size: 14px">Credit or Void a Transaction</td>
		</tr>
		<tr>
			<th style="text-align: center">Type</th>
			<th style="text-align: center">TXN</th>
			<th style="text-align: center">Amount</th>
			<th>Note</th>
		</tr>
		<tr>
			<td valign="top">
				<select name="aim_type" id="aim_type">
					<option value="" selected="selected">Choose...</option>
					<option value="CREDIT">Credit</option>
					<option value="VOID">Void</option>
				</select>
			</td>
			<td valign="top"><input type="text" id="aim_txn" name="aim_txn" value="" autocomplete="off" /></td>
			<td valign="top"><input type="text" id="aim_amt" name="aim_amt" value="" autocomplete="off" /></td>
			<td valign="top"><textarea id="aim_note" name="aim_note"></textarea></td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: right"><input type="submit" id="aim_submit" name="aim_submit" value="Submit" /></td>
		</tr>
	</table>
	<input type="hidden" id="aim_inv" name="aim_inv" value="<?=$_GET["id"]?>" />
<?php
$tmp = explode(" ",$alldata["ordName"]);
?>
	<input type="hidden" id="aim_fname" name="aim_fname" value="<?=$tmp[0]?>" />
	<input type="hidden" id="aim_lname" name="aim_lname" value="<?=(!empty($tmp[1]))?$tmp[1]:''?>" />
	<input type="hidden" id="aim_doedit" name="aim_doedit" value="<?=$_GET['doedit']?>" />
	</form>
</div>

<div id="div_charge" style="display: none">
	<?php
		if(!empty($alldata["ordEID"])){
			$sql_eid="SELECT * FROM customers WHERE custID=".$alldata["ordEID"];
			$result_eid=mysql_query($sql_eid);
			if(mysql_num_rows($result_eid)>0){
				$row_eid=mysql_fetch_assoc($result_eid);
			}
		}
	?>
	<form id="frmCharge" name="frmCharge" method="post" action="/admin/ordersprocess.php">
	<table cellpadding="3" cellspacing="0" border="1" style="margin: 5px auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
		<tr style="background-color: #BFC9E0; color: #194C7F">
			<td colspan="2" style="font-size: 14px; font-weight: bold; text-align: center">Authorize &amp; Capture</td>
		</tr>
		<tr>
			<td>First Name:</td>
			<td><input type="text" id="am_fname" name="am_fname" value="<?=$row_eid['Name']?>" /></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><input type="text" id="am_lname" name="am_lname" value="" /></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td><input type="text" id="am_address" name="am_address" value="<?=$row_eid['Address']?>" /></td>
		</tr>
		<tr>
			<td>Address 2:</td>
			<td><input type="text" id="am_address2" name="am_address2" value="<?=$row_eid['Address2']?>" /></td>
		</tr>
		<tr>
			<td>City:</td>
			<td><input type="text" id="am_city" name="am_city" value="<?=$row_eid['City']?>" /></td>
		</tr>
		<tr>
			<td>State:</td>
			<td><input type="text" id="am_state" name="am_state" value="<? if(!empty($row_eid['State2'])) echo $row_eid['State2']; else echo $row_eid['State']; ?>" /></td>
		</tr>
		<tr>
			<td>Zip:</td>
			<td><input type="text" id="am_zip" name="am_zip" value="<?=$row_eid['Zip']?>" /></td>
		</tr>
		<tr>
			<td>Amount:</td>
			<td><input type="text" id="am_amt" name="am_amt" value="" autocomplete="off" /></td>
		</tr>
		<tr>
			<td>CC#:</td>
			<td><input type="text" id="am_cc_num" name="am_cc_num" value="<?=Decrypt($row_eid['ccNum'], $cart_misc)?>" autocomplete="off" /></td>
		</tr>
		<tr>
			<td>Exp. Date:</td>
			<td> 
				<select id="am_exp_mon" name="am_exp_mon">
<?
		$ccexp=explode('/',$row_eid['ccExp']);
		for($i=1; $i<=12; $i++)
		{
			if($i<10)
				$i = '0'.$i;
			if($i == $ccexp[0])
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else if(date('n') == $i )
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else
				echo '<option value="'.$i.'">'.$i.'</option>';
		}
?>
				</select>&nbsp;/&nbsp;
				<select id="am_exp_year" name="am_exp_year">
<?
		$cur_year = date('Y');
		for($i=($cur_year); $i<=$cur_year+10; $i++)
		{
			if($i == $ccexp[1])
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			elseif(date('Y') == $i || $i==$ccexp[1])
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else
				echo '<option value="'.$i.'">'.$i.'</option>';
		}
?>				
				</select>
			</td>
		</tr>
		<tr>
			<td>CCV:</td>
			<td><input type="text" id="am_ccv" name="am_ccv" value="<?=$row_eid['ccCCV']?>" autocomplete="off" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit" id="am_submit" name="am_submit" value="Submit" /></td>
		</tr>
	</table>
	<input type="hidden" id="am_inv" name="am_inv" value="<?=$_GET["id"]?>" />
	<input type="hidden" id="am_type" name="am_type" value="AUTH_CAPTURE" />
	<input type="hidden" id="am_doedit" name="am_doedit" value="<?=$_GET['doedit']?>" />
	</form>
</div>
<?php
}
} // End of User permission check

	// ADDED by Chad Apr-04-06
	// LOCATION HISTORY
	$qry = "SELECT * FROM location WHERE ordID = '".$_GET["id"]."'";
	$res = mysql_query($qry) or print(mysql_error());
	$i=0;
	while($row = mysql_fetch_assoc($res)) {
		$locations[$i++]=$row;
	}
?>
<?php
	include(APPPATH.'views/partials/admin/dbconnection.php');
	// ADD ENDED
	
	// ADDED by Chad Apr-04-06
	// FEDEX TRACKING
	$qry = "SELECT * FROM fedex WHERE ordID = '".$_GET["id"]."'";
	$res = mysql_query($qry);
	if(mysql_num_rows($res) > 0) {
		$row = mysql_fetch_assoc($res);
		$trackNum = $row['trackNum'];
		if(!empty($trackNum)) {
			include(DOCROOT.'includes/fedex/fedexdc.php');
			$fed = new FedExDC();
			$track_Ret = $fed->track(
				array(
					'1537' => $trackNum, //Tracking Number
					'1534' =>'Y' // detail_scan_indicator (Show me all the tracking data)
				)
			);
			
			$ctr = 0;
			$hasChanged = false;
			$isDelivered = false;
			for($i=1; $i<=$track_Ret[1584]; $i++) {
				// See Customer Service Page for displaying results
	?>
<div style="margin: 10px auto; border: 2px solid #2C578A; width: 550px">
		<table align="center" width="550" border="0" cellpadding="3" cellspacing="0" style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif">
			<tr>
				<th colspan="2" style="font-size: 18px; background-color: #2C578A; color: #FFF; font-weight: bold; text-align: left">Fed<span style="margin-left: -3px; color: #FF6600">Ex</span></th>
				<th colspan="3" style="font-size: 14px; text-align: right; background-color: #2C578A; color: #FFF; font-weight: bold">Tracking# <?=$trackNum?></th>
			</tr>
	<?php
			if(!empty($track_Ret['1339-'.$i]))
			{
				$tmp_date = $track_Ret['1339-'.$i];
				$est_del = substr($tmp_date,0,4).'-'.substr($tmp_date,4,2).'-'.substr($tmp_date,6,2);
	?>
			<tr>
				<td colspan="5" style="background-color: #2C578A; height: 15px">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="5" style="background-color: #DFE7FF"><strong>Estimated Delivery Date:</strong> <?=date('M j, Y',strtotime($est_del))?></td>
			</tr>
	<?php
			}
	?>
			<tr>
				<th align="left" width="150" colspan="2" style="background-color: #2C578A; color: #FFF; font-weight: bold">Date/Time</th>
				<th align="left" style="background-color: #2C578A; color: #FFF; font-weight: bold">Activity</th>
				<th align="left" style="background-color: #2C578A; color: #FFF; font-weight: bold">Location</th>
				<th align="left" style="background-color: #2C578A; color: #FFF; font-weight: bold">Details</th>
			</tr>
	<?php
				for($j=1; $j<=$track_Ret['1715-'.$i]; $j++)
				{
					$date = $track_Ret['1162-'.$i.'-'.$j];
					$year = substr($date,0,4);
					$mon = substr($date,4,2);
					$day = substr($date,6,2);
					$hrs = substr($track_Ret['1163-'.$i.'-'.$j],0,2);
					$min = substr($track_Ret['1163-'.$i.'-'.$j],2,2);
					$sec = substr($track_Ret['1163-'.$i.'-'.$j],4,2);
					
					$date = $year.'-'.$mon.'-'.$day.' '.$hrs.':'.$min.':'.$sec;
					$unixDate = strtotime($date);
					
					if(empty($track_Ret['1161-'.$i.'-'.$j]))
					{
						$state = $track_Ret['1164-'.$i.'-'.$j];
					}
					else
					{
						$state = $track_Ret['1161-'.$i.'-'.$j];
					}
										
					if(date('Ymd',$unixDate) != date('Ymd',$last_date))
					{
						$ctr++;
						$hasChanged = true;
					}
					
					if($track_Ret['1159-'.$i.'-'.$j] == 'Delivered' && empty($track_Ret['1711-'.$i.'-'.$j]))
					{
						$isDelivered = true;
					}
	?>
					<tr style="background-color: #<?=($ctr%2==0)?'FFF':'DFE7FF'?>">
			<?php
						if($hasChanged)
						{
			?>
						<td style="text-align: left" valign="top">
							<span style="font-weight: bold"><?=date('M j, Y',$unixDate)?></span>
						</td>
						<td valign="top" style="text-align: right; border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=date('g:i A',$unixDate)?></td>
			<?php
							$hasChanged = false;
						}
						else
						{
			?>
						<td valign="top" colspan="2" style="text-align: right; border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=date('g:i A',$unixDate)?></td>
			<?php
						}
			?>		
						</td>
						<td valign="top" style="border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=($isDelivered)?'<strong>':''?><?=$track_Ret['1159-'.$i.'-'.$j]?><?=($isDelivered)?'</strong>':''?></td>
						<td valign="top" style="border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=$track_Ret['1160-'.$i.'-'.$j]?>, <?=$state?></td>
						<td valign="top"><?=$track_Ret['1711-'.$i.'-'.$j]?></td>
					</tr>
	<?php
					if($isDelivered)
					{
						$isDelivered = false;
					}
					
					$last_date = $unixDate;
				}
			}
	?>
  </table>
</div>
	<?php
		}
	}
	// ADD ENDED
	
	// ADDED by Chad Apr-03-06
	// DHL AND USPS TRACKING
	$qry = "SELECT * FROM dhl WHERE custPackID = '".$_GET["id"]."'";
	$res = mysql_query($qry) or print(mysql_error());
	if(mysql_num_rows($res) > 0) {
		$row = mysql_fetch_assoc($res);
		$trackNum = $row['DHLGMTrackNum'];
		if(!empty($trackNum)) {
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,"http://api.smartmail.com/tnt2.cfm?number=$trackNum&criteria=3&type=wddx&custid=rband&passwd=sm36732");
			//curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, "number=$trackNum&criteria=3&type=wddx&custid=rband&passwd=sm36732");
			$res = curl_exec($ch);
			curl_close($ch);
			$info = wddx_deserialize($res);
			if(empty($info['Detail'][0])) { //If no errors
?><?php
				// CHECK USPS TRACKING INFO
				if(!empty($info['TRACK_DELV_CONF'][0])) {
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL,"http://Production.ShippingAPIs.com/ShippingAPI.dll");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch,CURLOPT_HEADER,0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'API=TrackV2&XML=<TrackFieldRequest USERID="268REMIN3619"><TrackID ID="'.$info['TRACK_DELV_CONF'][0].'"></TrackID></TrackFieldRequest>');
					$res = curl_exec($ch);
					curl_close($ch);
					
					include(APPPATH.'views/pages/admin/xml2array.php');
					
					$xmlData = new xml2array();
					$uspsData = $xmlData -> parseXMLintoarray($res);
?>
    <?php
				}
			}
		}
	}
	
	// ADD ENDED
	
}else{
	$sSQL = "SELECT ordID FROM orders WHERE ordStatus=1";
	if(@$_POST["act"] != "purge") $sSQL .= " AND ordStatusDate<'" . date("Y-m-d H:i:s", time()-(3*60*60*24)) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_assoc($result)){
		$theid = $rs["ordID"];
		$delOptions = "";
		$addcomma = "";
		$result2 = mysql_query("SELECT cartID FROM cart WHERE cartOrderID=" . $theid) or print(mysql_error());
		while($rs2 = mysql_fetch_assoc($result2)){
			$delOptions .= $addcomma . $rs2["cartID"];
			$addcomma = ",";
		}
		if($delOptions != ""){
			$sSQL = "DELETE FROM cartoptions WHERE coCartID IN (" . $delOptions . ")";
			mysql_query($sSQL) or print(mysql_error());
		}
		mysql_query("DELETE FROM cart WHERE cartOrderID=" . $theid) or print(mysql_error());
		mysql_query("DELETE FROM orders WHERE ordID=" . $theid) or print(mysql_error());
	}
	if(@$_POST["act"]=="authorize"){
		do_stock_management(trim($_POST["id"]));
		if(trim($_POST["authcode"]) != "")
			$sSQL = "UPDATE orders set ordAuthNumber='" . mysql_real_escape_string(trim($_POST["authcode"])) . "',ordStatus=3 WHERE ordID=" . $_POST["id"];
		else
			$sSQL = "UPDATE orders set ordAuthNumber='" . mysql_real_escape_string($yyManAut) . "',ordStatus=3 WHERE ordID=" . $_POST["id"];
		if(mysql_query($sSQL)) {
			if(!setNewLocation( 3 , $_POST["id"],'Manual' )) print("Unable to record status change.");
		}else{
			print(mysql_error());
		}
		mysql_query("UPDATE cart SET cartCompleted=1 WHERE cartOrderID=" . $_POST["id"]) or print(mysql_error());
	}elseif(@$_POST["act"]=="status"){
		$maxitems=(int)($_POST["maxitems"]);
		for($index=0; $index < $maxitems; $index++){
			$iordid = trim($_POST["ordid" . $index]);
			$ordstatus = trim($_POST["ordstatus" . $index]);
			$ordauthno = "";
			$oldordstatus=999;
			$result = mysql_query("SELECT ordStatus,ordAuthNumber,ordEmail,ordDate,".getlangid("statPublic",64).",ordStatusInfo,ordName FROM orders INNER JOIN orderstatus ON orders.ordStatus=orderstatus.statID WHERE ordID=" . $iordid) or print(mysql_error());
			if($rs = mysql_fetch_assoc($result)){
				$oldordstatus=$rs["ordStatus"];
				$ordauthno=$rs["ordAuthNumber"];
				$ordemail=$rs["ordEmail"];
				$orddate=strtotime($rs["ordDate"]);
				$oldstattext=$rs[getlangid("statPublic",64)];
				$ordstatinfo=$rs["ordStatusInfo"];
				$ordername=$rs["ordName"];
			}
			if(! ($oldordstatus==999) && ($oldordstatus < 3 && $ordstatus >=3)){
				// This is to force stock management
				mysql_query("UPDATE cart SET cartCompleted=0 WHERE cartOrderID=" . $iordid) or print(mysql_error());
				do_stock_management($iordid);
				mysql_query("UPDATE cart SET cartCompleted=1 WHERE cartOrderID=" . $iordid) or print(mysql_error());
				if($ordauthno=="") mysql_query("UPDATE orders SET ordAuthNumber='". mysql_real_escape_string($yyManAut) . "' WHERE ordID=" . $iordid) or print(mysql_error());
			}
			if(! ($oldordstatus==999) && ($oldordstatus >=3 && $ordstatus < 3)) release_stock($iordid);
			if($iordid != "" && $ordstatus != ""){
				if($oldordstatus != (int)$ordstatus && @$_POST["emailstat"]=="1"){
					$result = mysql_query("SELECT ".getlangid("statPublic",64)." FROM orderstatus WHERE statID=" . $ordstatus);
					if($rs = mysql_fetch_assoc($result))
						$newstattext = $rs[getlangid("statPublic",64)];
					$emailsubject = "Order status updated";
					if(@$orderstatussubject != "") $emailsubject=$orderstatussubject;
					$ose = $orderstatusemail;
					$ose = str_replace("%orderid%", $iordid, $ose);
					$ose = str_replace("%orderdate%", date($dateformatstr, $orddate), $ose);// . " " . date("H:i", $orddate), $ose);
					$ose = str_replace("%oldstatus%", $oldstattext, $ose);
					$ose = str_replace("%newstatus%", $newstattext, $ose);
					$thetime = time() + ($dateadjust*60*60);
					$ose = str_replace("%date%", date($dateformatstr, $thetime), $ose);// . " " . date("H:i", $thetime), $ose);
					$ose = str_replace("%statusinfo%", $ordstatinfo, $ose);
					$ose = str_replace("%ordername%", $ordername, $ose);
					$ose = str_replace("%nl%", $emlNl, $ose);
					if(@$customheaders == ""){
						$customheaders = "MIME-Version: 1.0\n";
						$customheaders .= "From: %from% <%from%>\n";
						if(@$htmlemails==TRUE)
							$customheaders .= "Content-type: text/html; charset=".$emailencoding."\n";
						else
							$customheaders .= "Content-type: text/plain; charset=".$emailencoding."\n";
					}
					$headers = str_replace('%from%',$emailAddr,$customheaders);
					$headers = str_replace('%to%',$ordemail,$headers);
					if((int)$ordstatus==9) {
						$ose = $orderstatusshippedemail;
						$ose = str_replace("%orderid%", $iordid, $ose);
						$ose = str_replace("%orderdate%", date($dateformatstr, $orddate) . " " . date("H:i", $orddate), $ose);
						$emailsubject = $orderstatusshippedsubject;
					}
					mail($ordemail, $emailsubject, $ose, $headers);
				}
				if($oldordstatus != (int)$ordstatus) {
					if(mysql_query("UPDATE orders SET ordStatus=" . $ordstatus . ",ordStatusDate='" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "' WHERE ordID=" . $iordid)) {
						if(!setNewLocation( $ordstatus , $iordid ,'Manual' )) print("Unable to record status change.");
					}else{
						print(mysql_error());
					}
				}
			}
		}
	}
	if(@$_POST["sd"] != "")
		$sd = @$_POST["sd"];
	elseif(@$_GET["sd"] != "")
		$sd = @$_GET["sd"];
	else
		$sd = date($admindatestr, time() + ($dateadjust*60*60));
	if(@$_POST["ed"] != "")
		$ed = @$_POST["ed"];
	elseif(@$_GET["ed"] != "")
		$ed = @$_GET["ed"];
	else
		$ed = date($admindatestr, time() + ($dateadjust*60*60));
	$sd = parsedate($sd);
	$ed = parsedate($ed);
	if($sd > $ed) $ed = $sd;
	$fromdate = trim(@$_POST["fromdate"]);
	$todate = trim(@$_POST["todate"]);
	$ordid = trim(str_replace('"',"",str_replace("'","",@$_POST["ordid"])));
	$origsearchtext = trim(unstripslashes(@$_POST["searchtext"]));
	$searchtext = trim(mysql_real_escape_string(unstripslashes(@$_POST["searchtext"])));
	$ordstatus = "";
	
	$sSQL = "SELECT cart.*, ordID,ordName,payProvName,ordDropshipSent,ordAuthNumber,ordDate,ordStatus,ordTotal-ordDiscount AS ordTot,ordTransID,order_changed,ordEID,ordAffiliate FROM cart,orders,payprovider,products WHERE cart.cartProdID=products.pID AND products.pPricing_group=3 AND payprovider.payProvID=orders.ordPayProvider AND orders.ordStatus>=3 AND orders.ordStatus!=17  AND cart.cartOrderID=orders.ordID AND cartCompleted=1 AND ordEID!=34 AND ordDate BETWEEN '" . date("Y-m-d", $sd) . "' AND '" . date("Y-m-d", $ed) . " 23:59:59' ORDER BY ordID DESC";
	
	$alldata = mysql_query($sSQL) or print(mysql_error().'<br />'.$sSQL);
	//echo $sSQL;
	$hasdeleted=false;
	$sSQL = "SELECT COUNT(*) AS NumDeleted FROM orders WHERE ordStatus=1";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rs = mysql_fetch_assoc($result);
	if($rs["NumDeleted"] > 0) $hasdeleted=true;
	mysql_free_result($result);
?>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/popcalendar.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function delrec(id) {
cmsg = "<?php print $yyConDel?>\n"
if (confirm(cmsg)) {
	document.mainform.id.value = id;
	document.mainform.act.value = "delete";
	document.mainform.sd.value="<?php print date($admindatestr, $sd)?>";
	document.mainform.ed.value="<?php print date($admindatestr, $ed)?>";
	document.mainform.submit();
}
}
function authrec(id) {
var aucode;
cmsg = "<?php print $yyEntAuth?>"
if ((aucode=prompt(cmsg,'<?php print $yyManAut?>'))!=null) {
	document.mainform.id.value = id;
	document.mainform.act.value = "authorize";
	document.mainform.authcode.value = aucode;
	document.mainform.sd.value="<?php print date($admindatestr, $sd)?>";
	document.mainform.ed.value="<?php print date($admindatestr, $ed)?>";
	document.mainform.submit();
}
}
function checkcontrol(tt,evt){
<?php if(strstr(@$HTTP_SERVER_VARS["HTTP_USER_AGENT"], "Gecko")){ ?>
theevnt = evt;
return;
<?php }else{ ?>
theevnt=window.event;
<?php } ?>
if(theevnt.ctrlKey){
	maxitems=document.mainform.maxitems.value;
	for(index=0;index<maxitems;index++){
		if(eval('document.mainform.ordstatus'+index+'.length') > tt.selectedIndex){
			eval('document.mainform.ordstatus'+index+'.selectedIndex='+tt.selectedIndex);
			eval('document.mainform.ordstatus'+index+'.options['+tt.selectedIndex+'].selected=true');
		}
	}
}
}
function displaysearch(){
thestyle = document.getElementById('searchspan').style;
if(thestyle.display=='none')
	thestyle.display = 'block';
else
	thestyle.display = 'none';
}
function checkprinter(tt,evt){
<?php if(strstr(@$HTTP_SERVER_VARS["HTTP_USER_AGENT"], "Gecko")){ ?>
if(evt.ctrlKey || evt.altKey || document.mainform.ctrlmod[document.mainform.ctrlmod.selectedIndex].value=="1"){
	tt.href += "&printer=true";
	window.location.href = tt.href;
}
if(document.mainform.ctrlmod[document.mainform.ctrlmod.selectedIndex].value=="2"){
	tt.href += "&doedit=true";
	window.location.href = tt.href;
}
<?php }else{ ?>
theevnt=window.event;
if(theevnt.ctrlKey || document.mainform.ctrlmod[document.mainform.ctrlmod.selectedIndex].value=="1")tt.href += "&printer=true";
if(document.mainform.ctrlmod[document.mainform.ctrlmod.selectedIndex].value=="2")tt.href += "&doedit=true";
<?php } ?>
return(true);
}
// -->
</script>
      <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="">
        <tr>
          <td width="100%" align="center">
<?php	$themask = 'yyyy-mm-dd';
		if($admindateformat==1)
			$themask='mm/dd/yyyy';
		elseif($admindateformat==2)
			$themask='dd/mm/yyyy';
		if(! $success) print "<p><font color='#FF0000'>" . $errmsg . "</font></p>"; ?>
			
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="">
			  <form method="post" action="/admin/ordersifrogzsz.php">
			  <tr>
			    <td align="center"><h2>Shieldzone Orders On ifrogz </h2></td>
			    </tr>
			  <tr>
			    <td align="center"><p><strong><?php print $yyShoFrm?>:</strong> <select name="sd" size="1"><?php
					$gotmatch=FALSE;
					$thetime = time() + ($dateadjust*60*60);
					$dayToday = date("d",$thetime);
					$monthToday = date("m",$thetime);
					$yearToday = date("Y",$thetime);
					for($index=$dayToday; $index > 0; $index--){
						$thedate = mktime(0, 0, 0, $monthToday, $index, $yearToday);
						$thedatestr = date($admindatestr, $thedate);
						print "<option value='" . $thedatestr . "'";
						if($thedate==$sd){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					for($index=1; $index<=12; $index++){
						$thedatestr = date($admindatestr, $thedate = mktime(0,0,0,date("m",$thetime)-$index,1,date("Y",$thetime)));
						if(! $gotmatch && $thedate < $sd){
							print "<option value='" . date($admindatestr, $sd) . "' selected>" . date($admindatestr, $sd) . "</option>";
							$gotmatch=TRUE;
						}
						print "<option value='" . $thedatestr . "'";
						if($thedate==$sd){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					if(!$gotmatch) print "<option value='" . date($admindatestr, $sd) . "' selected>" . date($admindatestr, $sd) . "</option>";
				?></select> <strong><?php print $yyTo?>:</strong> <select name="ed" size="1"><?php
					$gotmatch=FALSE;
					$dayToday = date("d",$thetime);
					$monthToday = date("m",$thetime);
					$yearToday = date("Y",$thetime);
					for($index=$dayToday; $index > 0; $index--){
						$thedate = mktime(0, 0, 0, $monthToday, $index, $yearToday);
						$thedatestr = date($admindatestr, $thedate);
						print "<option value='" . $thedatestr . "'";
						if($thedate==$ed){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					for($index=1; $index<=12; $index++){
						if(! $gotmatch && $thedate < $ed){
							print "<option value='" . date($admindatestr, $ed) . "' selected>" . date($admindatestr, $ed) . "</option>";
							$gotmatch=TRUE;
						}
						$thedatestr = date($admindatestr, $thedate = mktime(0,0,0,date("m",$thetime)-$index,1,date("Y",$thetime)));
						print "<option value='" . $thedatestr . "'";
						if($thedate==$ed){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					if(!$gotmatch) print "<option value='" . date($admindatestr, $sd) . "' selected>" . date($admindatestr, $sd) . "</option>";
				?></select> <input type="submit" value="Go" />
			    </td>
			    </tr>
			  <tr>
			    <td>&nbsp;</td>
			    </tr>
			  </form>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr bgcolor="#030133"> 
                <td align="center"><strong><font color="#E7EAEF"><?php print $yyOrdId?></font></strong></td>
				<td width="150" align="center"><strong><font color="#E7EAEF"><?php print $yyDate?></font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Status</font></strong></td>
				<td width="150" align="center"><strong><font color="#E7EAEF">Date Sent </font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Product</font></strong></td>				
				<td align="center"><strong><font color="#E7EAEF">Price</font></strong></td>
				<td align="center"><strong><font color="#E7EAEF">Qty</font></strong></td>
			    <td align="center"><strong><font color="#E7EAEF">Total</font></strong></td>
			  </tr>
			  <form method="post" name="mainform" action="/admin/orders.php">
			  <?php if(@$_POST["powersearch"]=="1"){ ?>
			  <input type="hidden" name="powersearch" value="1" />
			  <input type="hidden" name="fromdate" value="<?php print trim(@$_POST["fromdate"])?>" />
			  <input type="hidden" name="todate" value="<?php print trim(@$_POST["todate"])?>" />
			  <input type="hidden" name="ordid" value="<?php print trim(str_replace('"','',str_replace("'",'',@$_POST["ordid"])))?>" />
			  <input type="hidden" name="origsearchtext" value="<?php print trim(str_replace('"','&quot;',@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="searchtext" value="<?php print trim(str_replace('"',"&quot;",@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="ordstatus[]" value="<?php print $ordstatus?>" />
			  <input type="hidden" name="startwith" value="<?php if($usepowersearch) print "1"?>" />
			  <?php } ?>
			  <input type="hidden" name="act" value="xxx" />
			  <input type="hidden" name="id" value="xxx" />
			  <input type="hidden" name="authcode" value="xxx" />
			  <input type="hidden" name="ed" value="<?php print date($admindatestr, $ed)?>" />
			  <input type="hidden" name="sd" value="<?php print date($admindatestr, $sd)?>" />
<?php
	if(mysql_num_rows($alldata) > 0){
		$rowcounter=0;
		$ordTot=0;
		$i=0;
		$num_rows_order=mysql_num_rows($alldata);
		$old_order='';
		$ordercompare='';
		while($rs = mysql_fetch_assoc($alldata)){
			if($rs["ordStatus"]>=3) {
				$ordTot += ($rs["cartProdPrice"]*$rs["cartQuantity"]);
				$qty += $rs["cartQuantity"];
				$price += $rs["cartProdPrice"];
			}
			if($rs["ordStatus"]>=3 && $old_order!=$rs["ordID"]) {
				$num_auth_order += 1;
				$old_order=$rs["ordID"];
			}
						
			$order_id_array[$i]=$rs["ordID"];
			if($ordercompare==$rs["ordID"]){
				if(@$bgcolor=="#E7EAEF") $bgcolor="#E7EAEF"; else $bgcolor="#FFFFFF";
			} else {
				if(@$bgcolor=="#E7EAEF") $bgcolor="#FFFFFF"; else $bgcolor="#E7EAEF";
			}
			
			if($rs["ordAuthNumber"]=="" || is_null($rs["ordAuthNumber"])){
				$isauthorized=FALSE;
				//print '<input type="button" name="auth" value="' . $yyAuthor . '" onclick="authrec(\'' . $rs["ordID"] . '\')" />';
			}else{
				//print '<a href="#" title="' . FormatEuroCurrency($rs["ordTot"]) . '" onclick="authrec(\'' . $rs["ordID"] . '\');return(false);">' . $startfont . $rs["ordAuthNumber"] . $endfont . '</a>';
				$isauthorized=TRUE;
			}
			?>
			  <tr bgcolor="<?php print $bgcolor?>"> 
                <td align="center"><?php echo $ordercompare==$rs["ordID"] ? "" : "<strong>" . $rs["cartOrderID"] . "</strong>"  ?></td>
				<td align="center" nowrap="nowrap"><font size="1"><?php print $startfont . date($admindatestr . " H:i:s", strtotime($rs["cartDateAdded"])) . $endfont?></font></td>
				<td align="center">
				<? 
				$gotitem=FALSE;
				for($index=0; $index<$numstatus; $index++){
					if(! $isauthorized && $allstatus[$index]["statID"]>2) break;
					if(! ($rs["ordStatus"] != 2 && $allstatus[$index]["statID"]==2)){
						if($rs["ordStatus"]==$allstatus[$index]["statID"]){
							print $allstatus[$index]["statPrivate"];
							$gotitem=TRUE;
						}
					}
				}
				if(! $gotitem) print '<option value="" selected>' . $yyUndef . '</option>' ?></select>				</td>
				<td align="center"><?=$rs["ordDropshipSent"]?></td>
				<td align="center"><?=$rs["cartProdID"]?></td>				
				<td align="center"><?=FormatEuroCurrency($rs["cartProdPrice"])?></td>
				<td align="center"><?=$rs["cartQuantity"]?></td>
				<td align="center"><?=FormatEuroCurrency(($rs["cartProdPrice"]*$rs["cartQuantity"]))?></td>
				</tr>
<?php		
			$ordercompare=$rs["ordID"];
			$rowcounter++;
			/*if($rowcounter>=1000){
				print "<tr><td colspan='6' align='center'><strong>Limit of " . $rowcounter . " orders reached. Please refine your search.</strong></td></tr>";
				break;
			}*/
		}
?>
			  <tr>
				<td colspan="2" align="left" style="border-top:#030133 2px solid;">
			      				  
		       <!-- Authorized Orders--></td>
				<td align="right" style="border-top:#030133 2px solid;">&nbsp;</td>
				<td align="center" style="border-top:#030133 2px solid;">&nbsp;</td>
				<td align="center" style="border-top:#030133 2px solid;">&nbsp;</td>
				<td align="center" style="border-top:#030133 2px solid;"><strong>Totals:</strong></td>
				<td align="center" style="border-top:#030133 2px solid;"><?=$qty?></td>
				<td align="center" style="border-top:#030133 2px solid;"><?php print FormatEuroCurrency($ordTot)?></td>
			  </tr>
			  </form>
			  <form method="post" action="/admin/dumporders.php" name="dumpform">
			  <?php if(@$_POST["powersearch"]=="1"){ ?>
			  <input type="hidden" name="powersearch" value="1" />
			  <input type="hidden" name="fromdate" value="<?php print trim(@$_POST["fromdate"])?>" />
			  <input type="hidden" name="todate" value="<?php print trim(@$_POST["todate"])?>" />
			  <input type="hidden" name="ordid" value="<?php print trim(str_replace('"','',str_replace("'",'',@$_POST["ordid"])))?>" />
			  <input type="hidden" name="origsearchtext" value="<?php print trim(str_replace('"','&quot;',@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="searchtext" value="<?php print trim(str_replace('"',"&quot;",@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="ordstatus[]" value="<?php print $ordstatus?>" />
			  <input type="hidden" name="startwith" value="<?php if($usepowersearch) print "1"?>" />
			  <?php } ?>
			  <input type="hidden" name="sd" value="<?php print date($admindatestr, $sd)?>" />
			  <input type="hidden" name="ed" value="<?php print date($admindatestr, $ed)?>" />
			  <input type="hidden" name="details" value="false" />
			  </form>
<?php
	}else{
?>
			  <?php if($hasdeleted){ ?>
			  <tr> 
				<td colspan="8">&nbsp;</td>
			  </tr>
			  <?php } ?>
			  </form>
<?php
	} ?>
			  <tr> 
                <td colspan="8" align="center">
				  <p><br />
					<a href="adminorders_nadal.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd)-1,date("d",$sd),date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed)-1,date("d",$ed),date("Y",$ed)))?>"><strong>- <?php print $yyMonth?></strong></a> | 
					<a href="adminorders_nadal.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)-7,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)-7,date("Y",$ed)))?>"><strong>- <?php print $yyWeek?></strong></a> | 
					<a href="adminorders_nadal.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)-1,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)-1,date("Y",$ed)))?>"><strong>- <?php print $yyDay?></strong></a> | 
					<a href="adminorders_nadal.php"><strong><?php print $yyToday?></strong></a> | 
					<a href="adminorders_nadal.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)+1,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)+1,date("Y",$ed)))?>"><strong><?php print $yyDay?> +</strong></a> | 
					<a href="adminorders_nadal.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)+7,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)+7,date("Y",$ed)))?>"><strong><?php print $yyWeek?> +</strong></a> | 
					<a href="adminorders_nadal.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd)+1,date("d",$sd),date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)+1,date("Y",$ed)))?>"><strong><?php print $yyMonth?> +</strong></a>				  </p>				</td>
			  </tr>
			</table>
		  </td>
		</tr>
      </table>
<?php
}
}

?>
<script language="JavaScript" type="text/javascript">
function dorecalc(onlytotal){
var thetotal=0,totoptdiff=0;
for(var i in document.forms.editform){
if(i.substr(0,5)=="quant"){
	theid = i.substr(5);
	totopts=0;
	delbutton = document.getElementById("del_"+theid);
	if(delbutton==null)
		isdeleted=false;
	else
		isdeleted=delbutton.checked;
	if(! isdeleted){
	for(var ii in document.forms.editform){
		var opttext="optn"+theid+"_";
		if(ii.substr(0,opttext.length)==opttext){
			theitem = document.getElementById(ii);
			if(document.getElementById('v'+ii)==null){
				thevalue = theitem[theitem.selectedIndex].value;
				if(thevalue.indexOf('|')>0){
					totopts += parseFloat(thevalue.substr(thevalue.indexOf('|')+1));
				}
			}
		}
	}
	thequant = parseInt(document.getElementById(i).value);
	if(isNaN(thequant)) thequant=0;
	theprice = parseFloat(document.getElementById("price"+theid).value);
	if(isNaN(theprice)) theprice=0;
	document.getElementById("optdiffspan"+theid).value=totopts;
	optdiff = parseFloat(document.getElementById("optdiffspan"+theid).value);
	if(isNaN(optdiff)) optdiff=0;
	thetotal += thequant * (theprice + optdiff);
	totoptdiff += thequant * optdiff;
	}
}
}
document.getElementById("optdiffspan").innerHTML=totoptdiff.toFixed(2);
//document.getElementById("ordtotal").value = thetotal.toFixed(2);
document.getElementById("ordTot").innerHTML = thetotal.toFixed(2);
document.getElementById("ordtotal").value = thetotal.toFixed(2);

if(onlytotal==true) return;
<? if(!empty($prcTot)){?>
thetotal+=<?=$prcTot?>;
<? } ?>
statetaxrate = parseFloat(document.getElementById("staterate").value);
if(isNaN(statetaxrate)) statetaxrate=0;
countrytaxrate = parseFloat(document.getElementById("countryrate").value);
if(isNaN(countrytaxrate)) countrytaxrate=0;
discount = parseFloat(document.getElementById("ordDiscount").value);
if(isNaN(discount)){
	discount=0;
	document.getElementById("ordDiscount").value=0;
}
statetaxtotal = (statetaxrate * (thetotal-discount)) / 100.0;
countrytaxtotal = (countrytaxrate * (thetotal-discount)) / 100.0;
shipping = parseFloat(document.getElementById("ordShipping").value);
if(isNaN(shipping)){
	//shipping=0;
	//document.getElementById("ordShipping").value=0;
}
handling = parseFloat(document.getElementById("ordHandling").value);
if(isNaN(handling)){
	handling=0;
	document.getElementById("ordHandling").value=0;
}
<?php	if(@$taxShipping==2){ ?>
statetaxtotal += (statetaxrate * shipping) / 100.0;
countrytaxtotal += (countrytaxrate * shipping) / 100.0;
<?php	}
		if(@$taxHandling==2){ ?>
statetaxtotal += (statetaxrate * handling) / 100.0;
countrytaxtotal += (countrytaxrate * handling) / 100.0;
<?php	} ?>
document.getElementById("ordStateTax").value = statetaxtotal.toFixed(2);
document.getElementById("ordCountryTax").value = countrytaxtotal.toFixed(2);
hstobj = document.getElementById("ordHSTTax");
hsttax=0;
if(! (hstobj==null)){
	hsttax = parseFloat(hstobj.value);
}
grandtotal = (thetotal + shipping + handling + statetaxtotal + countrytaxtotal + hsttax) - discount;


document.getElementById("grandtotalspan").innerHTML = grandtotal.toFixed(2);
}
</script>
