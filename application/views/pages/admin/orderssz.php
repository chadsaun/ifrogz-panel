<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
include('init.php');
include(APPPATH.'views/pages/admin/cartmisc.php');
include_once(IFZROOT.'kohana.php');
session_register('order_id_commas');
$lisuccess=0;
if(@$dateadjust=="") $dateadjust=0;
if(@$dateformatstr == "") $dateformatstr = "m/d/Y";
$admindatestr="Y-m-d";
if(@$admindateformat=="") $admindateformat=0;
if($admindateformat==1)
	$admindatestr="m/d/Y";
elseif($admindateformat==2)
	$admindatestr="d/m/Y";
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if(@$_GET["doedit"]=="true") $doedit=TRUE; else $doedit=FALSE;
function editfunc($data,$col,$size){
	global $doedit;
	if($doedit) return('<input type="text" id="' . $col . '" name="' . $col . '" value="' . str_replace('"','&quot;',$data) . '" size="' . $size . '">'); else return($data);
}
function editnumeric($data,$col,$size){
	global $doedit;
	if($doedit) return('<input type="text" id="' . $col . '" name="' . $col . '" value="' . number_format($data,2,'.','') . '" size="' . $size . '">'); else return(FormatEuroCurrency($data));
}
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])!=""){
	$config = RBI_Kohana::config('database.default_ifrogz');
	$config = $config['connection'];
	$db=mysql_connect($config['hostname'], $config['username'], $config['password']);
	mysql_select_db($config['database']) or die ('RBI connection failed.</td></tr></table></body></html>');
	$rbiSQL = 'SELECT * 
			   FROM employee 
			   WHERE username="'.mysql_real_escape_string(unstripslashes(trim(@$_COOKIE["WRITECKL"]))).'" and password="'.mysql_real_escape_string(unstripslashes(trim(@$_COOKIE["WRITECKP"]))).'"';
	$rs_rbi = mysql_query($rbiSQL);
	if(mysql_num_rows($rs_rbi) > 0) {
		@$_SESSION["loggedon"] = $storesessionvalue;
	}else{
		$lisuccess=2;
	}
	mysql_free_result($rs_rbi);
	
	include(APPPATH.'views/partials/admin/dbconnection.php');
}
if(($_SESSION["loggedon"] != $storesessionvalue && $lisuccess!=2) || @$disallowlogin==TRUE) exit();
if(@$htmlemails==TRUE) $emlNl = "<br />"; else $emlNl="\n";
if($lisuccess==2){
?>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><p>&nbsp;</p><p>&nbsp;</p>
				  <p><strong><?php print $yyOpFai?></strong></p><p>&nbsp;</p>
				  <p><?php print $yyCorCoo?> <?php print $yyCorLI?> <a href="/admin/login.php"><?php print $yyClkHer?></a>.</p>
				</td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
}else{
$success=true;
$alreadygotadmin = getadminsettings();
if(@$_POST["updatestatus"]=="1"){
	mysql_query("UPDATE orders SET ordStatusInfo='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ordStatusInfo"]))) . "' WHERE ordID=" . @$_POST["orderid"]) or print(mysql_error());
}elseif(@$_GET["id"] != ""){
	if(@$_POST["delccdets"] != ""){
		mysql_query("UPDATE orders SET ordCNum='' WHERE ordID=" . @$_GET["id"]);
	}
	$sSQL = "SELECT cartProdId,cartProdName,cartProdPrice,cartQuantity,cartID FROM cart WHERE cartOrderID=" . $_GET["id"];
	$allorders = mysql_query($sSQL) or print(mysql_error());
}else{
	// Delete old uncompleted orders.
	if($delccafter != 0){
		$sSQL = "UPDATE orders SET ordCNum='' WHERE ordDate<'" . date("Y-m-d H:i:s", time()-($delccafter*60*60*24)) . "'";
		mysql_query($sSQL) or print(mysql_error());
	}
	if($delAfter != 0){
		$sSQL = "SELECT cartOrderID,cartID FROM cart WHERE cartCompleted=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()-($delAfter*60*60*24)) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result)>0){
			$delStr="";
			$delOptions="";
			$addcomma = "";
			while($rs = mysql_fetch_assoc($result)){
				$delStr .= $addcomma . $rs["cartOrderID"];
				$delOptions .= $addcomma . $rs["cartID"];
				$addcomma = ",";
			}
			mysql_query("DELETE FROM orders WHERE ordID IN (" . $delStr . ")") or print(mysql_error());
			mysql_query("DELETE FROM cartoptions WHERE coCartID IN (" . $delOptions . ")") or print(mysql_error());
			mysql_query("DELETE FROM cart WHERE cartID IN (" . $delOptions . ")") or print(mysql_error());
		}
		mysql_free_result($result);
	}else{
		$sSQL = "SELECT cartOrderID,cartID FROM cart WHERE cartCompleted=0 AND cartOrderID=0 AND cartDateAdded<'" . date("Y-m-d H:i:s", time()-(3*60*60*24)) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result)>0){
			$delStr="";
			$delOptions="";
			$addcomma = "";
			while($rs = mysql_fetch_assoc($result)){
				$delStr .= $addcomma . $rs["cartOrderID"];
				$delOptions .= $addcomma . $rs["cartID"];
				$addcomma = ",";
			}
			mysql_query("DELETE FROM cartoptions WHERE coCartID IN (" . $delOptions . ")") or print(mysql_error());
			mysql_query("DELETE FROM cart WHERE cartID IN (" . $delOptions . ")") or print(mysql_error());
		}
		mysql_free_result($result);
	}
	$numstatus=0;
	$sSQL = "SELECT statID,statPrivate FROM orderstatus WHERE statPrivate<>'' ORDER BY statID";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_assoc($result)){
		$allstatus[$numstatus++]=$rs;
	}
	mysql_free_result($result);
}
if(@$_POST["updatestatus"]=="1"){
?>
<script language="JavaScript" type="text/javascript">
<!--
setTimeout("history.go(-2);",1100);
// -->
</script>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyUpdSuc?></strong><br /><br /><?php print $yyNowFrd?><br /><br />
                        <?php print $yyNoAuto?> <a href="javascript:history.go(-2)"><strong><?php print $yyClkHer?></strong></a>.<br /><br />
						<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
}elseif(@$_POST["doedit"] == "true"){
	$OWSP = "";
	$sSQL = "SELECT ordSessionID FROM orders WHERE ordID='" . $_POST["orderid"] . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rs = mysql_fetch_array($result);
	$thesessionid = $rs["ordSessionID"];
	mysql_free_result($result);
	$sSQL = "UPDATE orders SET ";
	$sSQL .= "ordName='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["name"]))) . "',";
	$sSQL .= "ordAddress='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["address"]))) . "',";
	if(@$useaddressline2==TRUE) $sSQL .= "ordAddress2='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["address2"]))) . "',";
	$sSQL .= "ordCity='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["city"]))) . "',";
	$sSQL .= "ordPoApo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["APO"]))) . "',";
	$sSQL .= "ordState='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["state"]))) . "',";
	$sSQL .= "ordZip='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["zip"]))) . "',";
	$sSQL .= "ordCountry='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["country"]))) . "',";
	$sSQL .= "ordEmail='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["email"]))) . "',";
	$sSQL .= "ordPhone='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["phone"]))) . "',";
	$sSQL .= "ordShipName='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["sname"]))) . "',";
	$sSQL .= "ordShipAddress='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["saddress"]))) . "',";
	if(@$useaddressline2==TRUE) $sSQL .= "ordShipAddress2='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["saddress2"]))) . "',";
	$sSQL .= "ordShipCity='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["scity"]))) . "',";
	$sSQL .= "ordShipPoApo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ShipAPO"]))) . "',";
	$sSQL .= "ordShipState='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["sstate"]))) . "',";
	$sSQL .= "ordShipZip='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["szip"]))) . "',";
	$sSQL .= "ordShipCountry='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["scountry"]))) . "',";
	$sSQL .= "ordShipType='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["shipmethod"]))) . "',";
	$sSQL .= "ordIP='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ipaddress"]))) . "',";
	$ordComLoc = 0;
	if(trim(@$_POST["commercialloc"])=="Y") $ordComLoc = 1;
	if(trim(@$_POST["wantinsurance"])=="Y") $ordComLoc += 2;
	$sSQL .= "ordComLoc=" . $ordComLoc . ",";
	$sSQL .= "ordAffiliate='" . trim(@$_POST["PARTNER"]) . "',";
	$sSQL .= "ordAddInfo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ordAddInfo"]))) . "',";
	$sSQL .= "ordStatusInfo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ordStatusInfo"]))) . "',";
	$sSQL .= "ordSupportInfo='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["ordSupportInfo"]))) . "',";
	$sSQL .= "order_changed='yes',";
	$sSQL .= "ordDiscountText='" . mysql_real_escape_string(trim(unstripslashes(@$_POST["discounttext"]))) . "',";
	$sSQL .= "ordExtra1='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ordextra1"]))) . "',";
	$sSQL .= "ordExtra2='" . mysql_real_escape_string(unstripslashes(trim(@$_POST["ordextra2"]))) . "',";
	$sSQL .= "ordShipping='" . mysql_real_escape_string(trim(@$_POST["ordShipping"])) . "',";
	$sSQL .= "ordStateTax='" . mysql_real_escape_string(trim(@$_POST["ordStateTax"])) . "',";
	$sSQL .= "ordCountryTax='" . mysql_real_escape_string(trim(@$_POST["ordCountryTax"])) . "',";
	if(@$canadataxsystem==TRUE) $sSQL .= "ordHSTTax='" . mysql_real_escape_string(trim(@$_POST["ordHSTTax"])) . "',";
	$sSQL .= "ordDiscount='" . mysql_real_escape_string(trim(@$_POST["ordDiscount"])) . "',";
	$sSQL .= "ordHandling='" . mysql_real_escape_string(trim(@$_POST["ordHandling"])) . "',";
	$sSQL .= "ordAuthNumber='" . mysql_real_escape_string(trim(@$_POST["ordAuthNumber"])) . "',";
	$sSQL .= "ordTransID='" . mysql_real_escape_string(trim(@$_POST["ordTransID"])) . "',";
	$sSQL .= "ordTotal='" . mysql_real_escape_string(trim(@$_POST["ordtotal"])) . "',";
	$sSQL .= "ord_cert_amt='" . mysql_real_escape_string(trim(@$_POST["ord_cert_amt"])) . "'";
	$sSQL .= " WHERE ordID='" . $_POST["orderid"] . "'";
	mysql_query($sSQL) or print(mysql_error());

	foreach($_POST as $objItem => $objValue){
		//print $objItem . " : " . $objValue . "<br>";
		if(substr($objItem,0,6)=="prodid"){
			$idno = (int)substr($objItem, 6);
			$cartid = trim(@$_POST["cartid" . $idno]);
			$prodid = trim(@$_POST["prodid" . $idno]);
			$quant = trim(@$_POST["quant" . $idno]);
			$theprice = trim(@$_POST["price" . $idno]);
			$prodname = trim(@$_POST["prodname" . $idno]);
			$delitem = trim(@$_POST["del_" . $idno]);
			if($delitem=="yes"){
				mysql_query("DELETE FROM cart WHERE cartID=" . $cartid) or print(mysql_error());
				mysql_query("DELETE FROM cartoptions WHERE coCartID=" . $cartid) or print(mysql_error());
				$cartid = "";
			}elseif($cartid != ""){
				$sSQL = "UPDATE cart SET cartProdID='" . mysql_real_escape_string(trim(unstripslashes($prodid))) . "',cartProdPrice=" . $theprice . ",cartProdName='" . mysql_real_escape_string(trim(unstripslashes($prodname))) . "',cartQuantity=" . $quant . " WHERE cartID=" . $cartid;
				mysql_query($sSQL) or print(mysql_error());
				mysql_query("DELETE FROM cartoptions WHERE coCartID=" . $cartid) or print(mysql_error());
			}else{
				$sSQL = "INSERT INTO cart (cartSessionID,cartProdID,cartQuantity,cartCompleted,cartProdName,cartProdPrice,cartOrderID,cartDateAdded) VALUES (";
				$sSQL .= "'" . $thesessionid . "',";
				$sSQL .= "'" . mysql_real_escape_string(trim(unstripslashes($prodid))) . "',";
				$sSQL .= $quant . ",";
				$sSQL .= "1,";
				$sSQL .= "'" . mysql_real_escape_string(trim(unstripslashes($prodname))) . "',";
				$sSQL .= "'" . $theprice . "',";
				$sSQL .= @$_POST["orderid"] . ",";
				$sSQL .= "'" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "')";
				mysql_query($sSQL) or print(mysql_error());
				$cartid = mysql_insert_id();
			}
			if($cartid != ""){
				$optprefix = "optn" . $idno . '_';
				$prefixlen = strlen($optprefix);
				foreach($_POST as $kk => $kkval){
					if(substr($kk,0,$prefixlen)==$optprefix && trim($kkval) != ''){
						$optidarr = split('\|', $kkval);
						$optid = $optidarr[0];
						if(@$_POST["v" . $kk] == ""){
							$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)."," . $OWSP . "optPriceDiff,optWeightDiff,optType,optFlags FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($kkval) . "'";
							$result = mysql_query($sSQL) or print(mysql_error());
							if($rs = mysql_fetch_array($result)){
								if(abs($rs["optType"]) != 3){
									$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartid . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string($rs[getlangid("optName",32)]) . "',";
									$sSQL .= $optidarr[1] . ",0)";
								}else
									$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartid . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','',0,0)";
								mysql_query($sSQL) or print(mysql_error());
							}
							mysql_free_result($result);
						}else{
							$sSQL="SELECT optID,".getlangid("optGrpName",16).",".getlangid("optName",32)." FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optID='" . mysql_real_escape_string($kkval) . "'";
							$result = mysql_query($sSQL) or print(mysql_error());
							$rs = mysql_fetch_array($result);
							$sSQL = "INSERT INTO cartoptions (coCartID,coOptID,coOptGroup,coCartOption,coPriceDiff,coWeightDiff) VALUES (" . $cartid . "," . $rs["optID"] . ",'" . mysql_real_escape_string($rs[getlangid("optGrpName",16)]) . "','" . mysql_real_escape_string(unstripslashes(trim(@$_POST["v" . $kk]))) . "',0,0)";
							mysql_query($sSQL) or print(mysql_error());
							mysql_free_result($result);
						}
					}
				}
			}
		}
	}
?>
<script language="JavaScript" type="text/javascript">
<!--
setTimeout("history.go(-2);",1100);
// -->
</script>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="4" align="center"><br /><strong><?php print $yyUpdSuc?></strong><br /><br /><?php print $yyNowFrd?><br /><br />
                        <?php print $yyNoAuto?> <a href="javascript:history.go(-2)"><strong><?php print $yyClkHer?></strong></a>.<br /><br />
						<img src="/lib/images/misc/clearpixel.gif" width="300" height="3" alt="" /></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
<?php
}elseif(@$_GET["id"] != ""){
	$statetaxrate=0;
	$countrytaxrate=0;
	$hsttaxrate=0;
	$countryorder=0;
	$sSQL = "SELECT ordID,ordName,ordAddress,ordAddress2,ordCity,ordState,ordZip,ordCountry,ordEmail,ordPhone,ordShipName,ordShipAddress,ordShipAddress2,ordShipCity,ordShipState,ordShipZip,ordShipCountry,ordPayProvider,ordAuthNumber,ordTransID,ordTotal,ordDate,ordStateTax,ordCountryTax,ordHSTTax,ordShipping,ordShipType,ordIP,ordAffiliate,ordDiscount,ordHandling,ordDiscountText,ordComLoc,ordExtra1,ordExtra2,ordAddInfo,ordCNum,ordStatusInfo,ordSupportInfo,order_changed,ordStatus,ord_cert_amt,ord_cert_id,ordPoApo,ordShipPoApo,ordEID FROM orders LEFT JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordID='" . $_GET["id"] . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	$alldata = mysql_fetch_array($result);
	$alldata["ordDate"] = strtotime($alldata["ordDate"]);
	mysql_free_result($result);
	if($doedit){
		print '<form method="post" name="editform" action="/admin/orderssz.php" onsubmit="return confirmedit()"><input type="hidden" name="orderid" value="' . $_GET["id"] . '" /><input type="hidden" name="doedit" value="true" />';
		$overridecurrency=TRUE;
		$orcsymbol="";
		$orcdecplaces=2;
		$orcpreamount=TRUE;
		$orcdecimals=".";
		$orcthousands="";
	}
?>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/scriptaculous.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
var newwin="";
var plinecnt=0;
function openemailpopup(id) {
  popupWin = window.open('/admin/popupemail.php?'+id,'emailpopup','menubar=no, scrollbars=no, width=300, height=250, directories=no,location=no,resizable=yes,status=no,toolbar=no')
}
function updateoptions(id){
	prodid = document.getElementById('prodid'+id).value;
	if(prodid != ''){
		newwin = window.open('/admin/popupemail.php?prod='+prodid+'&index='+id,'updateopts','menubar=no, scrollbars=no, width=50, height=40, directories=no,location=no,resizable=yes,status=no,toolbar=no');
	}
	return(false);
}
function extraproduct(plusminus){
var productspan=document.getElementById('productspan');
if(plusminus=='+'){
productspan.innerHTML=productspan.innerHTML.replace(/<!--NEXTPRODUCTCOMMENT-->/,'<!--PLINE'+plinecnt+'--><tr><td valign="top"><input type="button" value="..." onclick="updateoptions('+(plinecnt+1000)+')">&nbsp;<input name="prodid'+(plinecnt+1000)+'" size="18" id="prodid'+(plinecnt+1000)+'"></td><td valign="top"><input type="text" id="prodname'+(plinecnt+1000)+'" name="prodname'+(plinecnt+1000)+'" size="24"></td><td><span id="optionsspan'+(plinecnt+1000)+'">-</span></td><td valign="top"><input type="text" id="quant'+(plinecnt+1000)+'" name="quant'+(plinecnt+1000)+'" size="5" value="1"></td><td valign="top"><input type="text" id="price'+(plinecnt+1000)+'" name="price'+(plinecnt+1000)+'" value="0" size="7"><br /><input type="hidden" id="optdiffspan'+(plinecnt+1000)+'" value="0"></td><td>&nbsp;</td></tr><!--PLINEEND'+plinecnt+'--><!--NEXTPRODUCTCOMMENT-->');
plinecnt++;
}else{
if(plinecnt>0){
plinecnt--;
var restr = '<!--PLINE'+plinecnt+'-->(.|\\n)+<!--PLINEEND'+plinecnt+'-->';
//alert(restr);
var re = new RegExp(restr);
productspan.innerHTML=productspan.innerHTML.replace(re,'');
}
}
}
function confirmedit(){
if(confirm('<?php print str_replace("'","\'",$yyChkRec)?>'))
	return(true);
return(false);
}


function checkAIM(frm) {
	if(frm.aim_type.selectedIndex==0) {
		alert("Please choose a transaction type.");
		frm.aim_type.focus();
		return false;
	}else if(frm.aim_txn=='') {
		alert("A transaction number is required.");
		frm.aim_txn.focus();
		return false;
	}else if(frm.aim_amt=='') {
		alert("Please enter an amount.");
		frm.aim_amt.focus();
		return false;
	}
	
	return true;
}

function toggleTXN() {
	if($('div_trans').style.display=='' || $('div_trans').style.display==undefined) {
		//$('div_trans').style.display = 'none';
		Effect.BlindUp('div_trans');
		$('btn_add_txn').value = 'Credit or Void';
	}else if($('div_trans').style.display=='none') {
		//$('div_trans').style.display = '';
		Effect.BlindDown('div_trans');
		$('btn_add_txn').value = 'Hide Credit or Void';
	}
}

function toggleCharge() {
	if($('div_charge').style.display=='' || $('div_charge').style.display==undefined) {
		//$('div_charge').style.display = 'none';
		Effect.BlindUp('div_charge');
		$('btn_charge').value = 'Charge CC';
	}else if($('div_charge').style.display=='none') {
		//$('div_charge').style.display = '';
		Effect.BlindDown('div_charge');
		$('btn_charge').value = 'Hide Charge CC';
	}
}

function togglePrcAdd() {
	if($('div_prc_add').style.display=='' || $('div_prc_add').style.display==undefined) {
		//$('div_charge').style.display = 'none';
		Effect.BlindUp('div_prc_add');
		$('btn_prc_add').value = 'Add Price Adjustment';
	}else if($('div_prc_add').style.display=='none') {
		//$('div_charge').style.display = '';
		Effect.BlindDown('div_prc_add');
		$('btn_prc_add').value = 'Hide Price Adjustment';
	}
}

//-->
</script>
<style type="text/css">
a img{
	border: 0;
}
</style>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
        <tr>
          <td width="100%">
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
<?php		if($isprinter && @$invoiceheader != ""){ ?>
			  <tr> 
                <td colspan="6"><?php print $invoiceheader?></td>
			  </tr>
<?php		} ?>
			  <tr 	<? //if(!empty($alldata["ordEID"])) echo 'bgcolor="#FA6561"'?>> 
                <td colspan="6" align="center"><strong><?php print $xxOrdNum . " " . $alldata["ordID"] . "<br /><br />" . date($dateformatstr, $alldata["ordDate"]) . " " . date("H:i", $alldata["ordDate"])?></strong></td>
			  </tr>
<?php		if($isprinter && @$invoiceaddress != ""){ ?>
			  <tr> 
                <td colspan="6"><?php print $invoiceaddress?></td>
			  </tr>
<?php		} ?>
<?php		if(trim(@$extraorderfield1)!=""){ ?>
			<tr>
			  <td width="23%" align="right"><strong><?php print $extraorderfield1 ?>:</strong></td>
			  <td align="left" colspan="5"><?php print editfunc($alldata["ordExtra1"],"ordextra1",25)?></td>
			</tr>
<?php		} ?>
			<tr>
			  <td width="23%" align="right"><strong><?php print $xxName?>:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($alldata["ordName"],"name",25)?></td>
			  <td width="25%" align="right">
			  <strong><?php print $xxEmail?>:</strong></td>
			  <td width="24%" align="left"><?php
				if($isprinter || $doedit) print editfunc($alldata["ordEmail"],"email",25); else print '<a href="mailto:' . $alldata["ordEmail"] . '">' . $alldata["ordEmail"] . '</a>';?></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxAddress?>:</strong></td>
			  <td colspan="3" align="left"<?php if(@$useaddressline2==TRUE) print ' colspan="3"'?>><?php print editfunc($alldata["ordAddress"],"address",25)?></td>
<?php	if(@$useaddressline2==TRUE){ ?>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxAddress2?>:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($alldata["ordAddress2"],"address2",25)?></td>
<?php	} ?>
			  <td align="right"><strong><?php print $xxCity?>:</strong></td>
			  <td align="left"><?php print editfunc($alldata["ordCity"],"city",25)?></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxAllSta?>:</strong></td>
			  <td align="left"><?php print editfunc($alldata["ordState"],"state",25)?></td>
			  <td align="right"><strong>APO/PO:</strong></td>
			  <td align="left"><input <?php if (!(strcmp($alldata["ordPoApo"],1))) {echo "checked=\"checked\"";} ?> <? if($doedit) echo ''; else echo 'disabled="disabled"'; ?> name="APO" type="checkbox" value="1" /></td>
			  <td align="right"><strong><?php print $xxCountry?>:</strong></td>
			  <td align="left"><?php
			if($doedit){
				$foundmatch=FALSE;
				print '<select name="country" size="1">';
				$sSQL = "SELECT countryName,countryTax,countryOrder FROM countries ORDER BY countryOrder DESC, countryName";
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs2 = mysql_fetch_array($result)){
					print '<option value="' . str_replace('"','&quot;',$rs2["countryName"]) . '"';
					if($alldata["ordCountry"]==$rs2["countryName"]){
						print ' selected';
						$foundmatch=TRUE;
						$countrytaxrate=$rs2["countryTax"];
						$countryorder=$rs2["countryOrder"];
					}
					print '>' . $rs2["countryName"] . "</option>\r\n";			}
				mysql_free_result($result);
				if(! $foundmatch) print '<option value="' . str_replace('"','&quot;',$alldata["ordCountry"]) . '" selected>' . $alldata["ordCountry"] . "</option>\r\n";
				print '</select>';
				if($countryorder==2){
					$sSQL = "SELECT stateTax FROM states WHERE stateAbbrev='" . mysql_real_escape_string($alldata["ordState"]) . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs2 = mysql_fetch_array($result))
						$statetaxrate = $rs2["stateTax"];
				}
				if($alldata["ordStateTax"]==0)$statetaxrate=0; 
			}else
				print $alldata["ordCountry"];?></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxZip?>:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($alldata["ordZip"],"zip",15)?></td>
			  <td align="right"><strong><?php print $xxPhone?>:</strong></td>
			  <td align="left"><?php print editfunc($alldata["ordPhone"],"phone",25)?></td>
			</tr>
<?php if(trim(@$extraorderfield2)!=""){ ?>
			<tr>
			  <td align="right"><strong><?php print @$extraorderfield2 ?>:</strong></td>
			  <td align="left" colspan="5"><?php print editfunc($alldata["ordExtra2"],"ordextra2",25)?></td>
			</tr>
<?php } ?>
<?php if(! $isprinter){ ?>
			<tr>
			  <td align="right"><strong>IP Address:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($alldata["ordIP"],"ipaddress",15)?></td>
			  <td align="right"><strong><?php print $yyAffili?>:</strong></td>
			  <td align="left"><?php print editfunc($alldata["ordAffiliate"],"PARTNER",15)?></td>
			</tr>
<?php }
	  if(trim($alldata["ordDiscountText"])!=""){ ?>
			<tr>
			  <td align="right" valign="top"><strong><?php print $xxAppDs?>:</strong></td>
			  <td align="left" colspan="5"><?php print editfunc($alldata["ordDiscountText"],"discounttext",25)?></td>
			</tr>
<?php }
	  if(trim($alldata["ordShipName"]) != "" || trim($alldata["ordShipAddress"]) != "" || trim($alldata["ordShipCity"]) != "" || $doedit){ ?>
			<tr>
			  <td align="center" colspan="6"><strong><?php print $xxShpDet?>.</strong></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxName?>:</strong></td>
			  <td align="left" colspan="5"><?php print editfunc($alldata["ordShipName"],"sname",25)?></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxAddress?>:</strong></td>
			  <td colspan="3" align="left"<?php if(@$useaddressline2==TRUE) print ' colspan="3"'?>><?php print editfunc($alldata["ordShipAddress"],"saddress",25)?></td>
<?php	if(@$useaddressline2==TRUE){ ?>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxAddress2?>:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($alldata["ordShipAddress2"],"saddress2",25)?></td>
<?php	} ?>
			  <td align="right"><strong><?php print $xxCity?>:</strong></td>
			  <td align="left"><?php print editfunc($alldata["ordShipCity"],"scity",25)?></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxAllSta?>:</strong></td>
			  <td align="left"><?php print editfunc($alldata["ordShipState"],"sstate",25)?></td>
			  <td align="right"><strong>APO/PO:</strong></td>
			  <td align="left"><input name="ShipAPO" type="checkbox" id="ShipAPO" value="1" <?php if (!(strcmp($alldata["ordShipPoApo"],1))) {echo "checked=\"checked\"";} ?> <? if($doedit) echo ''; else echo 'disabled="disabled"'; ?>  /></td>
			  <td align="right"><strong><?php print $xxCountry?>:</strong></td>
			  <td align="left"><?php
			if($doedit){
				if(trim($alldata["ordShipName"]) != "" || trim($alldata["ordShipAddress"]) != "") $usingshipcountry=TRUE; else $usingshipcountry=FALSE;
				$foundmatch=FALSE;
				print '<select name="scountry" size="1">';
				$sSQL = "SELECT countryName,countryTax,countryOrder FROM countries ORDER BY countryOrder DESC, countryName";
				$result = mysql_query($sSQL) or print(mysql_error());
				while($rs2 = mysql_fetch_array($result)){
					print '<option value="' . str_replace('"','&quot;',$rs2["countryName"]) . '"';
					if($alldata["ordShipCountry"]==$rs2["countryName"]){
						print ' selected';
						$foundmatch=TRUE;
						if($usingshipcountry) $countrytaxrate=$rs2["countryTax"];
						$countryorder=$rs2["countryOrder"];
					}
					print '>' . $rs2["countryName"] . "</option>\r\n";			}
				mysql_free_result($result);
				if(! $foundmatch) print '<option value="' . str_replace('"','&quot;',$alldata["ordShipCountry"]) . '" selected>' . $alldata["ordShipCountry"] . "</option>\r\n";
				print '</select>';
				if($countryorder==2 && $usingshipcountry){
					$sSQL = "SELECT stateTax FROM states WHERE stateName='" . mysql_real_escape_string($alldata["ordShipState"]) . "'";
					$result = mysql_query($sSQL) or print(mysql_error());
					if($rs2 = mysql_fetch_array($result))
						$statetaxrate = $rs2["stateTax"];
				}
			}else
				print $alldata["ordShipCountry"]?></td>
			</tr>
			<tr>
			  <td align="right"><strong><?php print $xxZip?>:</strong></td>
			  <td align="left" colspan="5"><?php print editfunc($alldata["ordShipZip"],"szip",15)?></td>
			</tr>
<?php }
	  if($alldata["ordShipType"] != "" || $alldata["ordComLoc"]>0 || $doedit){ ?>
			<tr>
			  <td align="right"><strong><?php print $xxShpMet?>:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($alldata["ordShipType"],"shipmethod",25);
									 if(! $doedit && ($alldata["ordComLoc"]&2)==2) print $xxWtIns?></td>
			  <td align="right"><strong><?php print $xxCLoc?>:</strong></td>
			  <td align="left"><?php	if($doedit){
											print '<select name="commercialloc" size="1">';
											print '<option value="N">' . $yyNo . '</option>';
											print '<option value="Y"' . (($alldata["ordComLoc"]&1)==1 ? ' selected' : '') . '>' . $yyYes . '</option>';
											print '</select>';
										}else{
											if(($alldata["ordComLoc"]&1)==1) print $yyYes; else print $yyNo;
										}?></td>
			</tr>
<?php	if($doedit){ ?>
			<tr>
			  <td align="right"><strong><?php print $xxShpIns?>:</strong></td>
			  <td align="left" colspan="5"><?php
										print '<select name="wantinsurance" size="1">';
										print '<option value="N">' . $yyNo . '</option>';
										print '<option value="Y"' . (($alldata["ordComLoc"]&2)==2 ? ' selected' : '') . '>' . $yyYes . '</option>';
										print '</select>';
			?></td>
			</tr>
<?php	}
	  }
		$ordAuthNumber = trim($alldata["ordAuthNumber"]);
		$ordTransID = trim($alldata["ordTransID"]);
		if(! $isprinter && ($ordAuthNumber != "" || $ordTransID != "" || $doedit)){ ?>
			<tr>
			  <td align="right"><strong><?php print $yyAutCod?>:</strong></td>
			  <td colspan="3" align="left"><?php print editfunc($ordAuthNumber,"ordAuthNumber",15) ?></td>
			  <td align="right"><strong><?php print $yyTranID?>:</strong></td>
			  <td align="left"><?php print editfunc($ordTransID,"ordTransID",15) ?></td>
			</tr>
<?php	}

		// BOL#s ADDED BY CHAD JUL 27,06
		$sql_bol = "SELECT * FROM bol WHERE ordID = " . $alldata["ordID"];
		$res_bol = mysql_query($sql_bol) or print(mysql_error());
		if(mysql_num_rows($res_bol) > 0) {
			$row_bol = mysql_fetch_assoc($res_bol);
?>
			<tr>
				<td align="right"><strong>Express BOL#:</strong></td>
				<td colspan="3"><?=$row_bol['exBOL']?></td>
<?php
			if(!empty($row_bol['smBOL'])) {
?>
				<td align="right"><strong>Smart Mail BOL#:</strong></td>
				<td><?=$row_bol['smBOL']?></td>
<?php
			}
			if(!empty($row_bol['gmBOL'])) {
?>
				<td align="right"><strong>Global Mail BOL#:</strong></td>
				<td><?=$row_bol['gmBOL']?></td>
<?php
			}
?>
			</tr>
<?php
		}
		// ADD ENDED

		$ordAddInfo = Trim($alldata["ordAddInfo"]);
		if($ordAddInfo != "" || $doedit){ ?>
			<tr>
			  <td align="right" valign="top"><strong><?php print $xxAddInf?>:</strong></td>
			  <td align="left" colspan="5"><?php
			if($doedit)
				print '<textarea name="ordAddInfo" cols="50" rows="4" wrap=virtual>' . $ordAddInfo . '</textarea>';
			else
				print str_replace(array("\r\n","\n"),array("<br />","<br />"),$ordAddInfo); ?></td>
			</tr>
<?php	}
if(! $isprinter){
		if(! $doedit) print '<form method="post" action="/admin/orderssz.php"><input type="hidden" name="updatestatus" value="1" /><input type="hidden" name="orderid" value="' . @$_GET["id"] . '" />';
?>			<tr>
			  <td align="right" valign="top"><strong><?php print $yyStaInf?>:</strong></td>
			  <td align="left" colspan="5"><?php print $alldata["ordStatusInfo"]?> </td>
			</tr>
			<? if($doedit) { ?>
			<tr>
			  <td align="right" valign="top"><strong><?php print $yySupInf?>:</strong></td>
			  <td align="left" colspan="5"><textarea name="ordSupportInfo" cols="50" rows="4" wrap=virtual><?php print $alldata["ordSupportInfo"]?></textarea> <?php if(! $doedit) print '<input type="submit" value="' . $yyUpdate . '" />'?></td>
			</tr>
			<? } ?>
<?php	if(($alldata["ordPayProvider"]==3 || $alldata["ordPayProvider"]==13) && $alldata["ordAuthNumber"] != ""){ ?>
			<tr>
			  <td align="center" colspan="6">&nbsp;</td>
			</tr>
<?php	}
		if(! $doedit) print '</form>';
	if((int)$alldata["ordPayProvider"]==10){ ?>
			<tr>
			  <td align="center" colspan="6"><hr width="50%">			  </td>
			</tr>
<?php	if(@$_SERVER["HTTPS"] != "on" && (@$_SERVER["SERVER_PORT"] != "443") && @$nochecksslserver != TRUE){ ?>
			<tr>
			  <td align="center" colspan="6"><strong><font color="#FF0000">You do not appear to be viewing this page on a secure (https) connection. Credit card information cannot be shown.</font></strong></td>
			</tr>
<?php	}else{
			$ordCNum = $alldata["ordCNum"];
			if($ordCNum != ""){
				$cnumarr = "";
				$encryptmethod = strtolower(@$encryptmethod);
				if($encryptmethod=="none"){
					$cnumarr = explode("&",$ordCNum);
				}elseif($encryptmethod=="mcrypt"){
					if(@$mcryptalg == "") $mcryptalg = MCRYPT_BLOWFISH;
					$td = mcrypt_module_open($mcryptalg, '', 'cbc', '');
					$thekey = @$ccencryptkey;
					$thekey = substr($thekey, 0, mcrypt_enc_get_key_size($td));
					$cnumarr = explode(" ", $ordCNum);
					$iv = @$cnumarr[0];
					$iv = @pack("H" . strlen($iv), $iv);
					$ordCNum = @pack("H" . strlen(@$cnumarr[1]), @$cnumarr[1]);
					mcrypt_generic_init($td, $thekey, $iv);
					$cnumarr = explode("&", mdecrypt_generic($td, $ordCNum));
					mcrypt_generic_deinit($td);
					mcrypt_module_close($td);
				}else{
					print '<tr><td colspan="4">WARNING: $encryptmethod is not set. Please see http://www.ecommercetemplates.com/phphelp/ecommplus/parameters.asp#encryption</td></tr>';
				}
			} ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxCCName?>:</strong></td>
			  <td align="left" colspan="2"><?php
			if(@$encryptmethod!=""){
					if(is_array(@$cnumarr)) print URLDecode(@$cnumarr[4]);
			} ?></td>
			</tr>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $yyCarNum?>:</strong></td>
			  <td align="left" colspan="2"><?php
			if($ordCNum != ""){
				if(is_array($cnumarr)) print $cnumarr[0];
			}else{
				print "(no data)";
			} ?></td>
			</tr>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $yyExpDat?>:</strong></td>
			  <td align="left" colspan="2"><?php
			if(@$encryptmethod!=""){
					if(is_array(@$cnumarr)) print @$cnumarr[1];
			} ?></td>
			</tr>
			<tr>
			  <td align="right" colspan="4"><strong>CVV Code:</strong></td>
			  <td align="left" colspan="2"><?php
			if(@$encryptmethod!=""){
					if(is_array(@$cnumarr)) print @$cnumarr[2];
			} ?></td>
			</tr>
			<tr>
			  <td align="right" colspan="4"><strong>Issue Number:</strong></td>
			  <td align="left" colspan="2"><?php
			if(@$encryptmethod!=""){
					if(is_array(@$cnumarr)) print @$cnumarr[3];
			} ?></td>
			</tr>
<?php		if($ordCNum != "" && !$doedit){ ?>
		  <form method=POST action="/admin/orderssz.php?id=<?php print $_GET["id"]?>">
			<input type="hidden" name="delccdets" value="<?php print $_GET["id"]?>" />
			<tr>
			  <td align="center" colspan="6"><input type=submit value="<?php print $yyDelCC?>" /></td>
			</tr>
		  </form>
<?php		}
		}
	}
} // isprinter ?>
			<tr>
			  <td align="center" colspan="6">&nbsp;<br /></td>
			</tr>
		  </table>
<span id="productspan">
		  <table width="100%" border="1" cellspacing="0" cellpadding="4" bordercolor="#999999" style="border-collapse: collapse">
			<tr>
			  <td><strong><?php print $xxPrId?></strong></td>
			  <td><strong><?php print $xxPrNm?></strong></td>
			  <td><strong><?php print $xxPrOpts?></strong></td>
			  <td><strong><?php print $xxQuant?></strong></td>
			  <td><strong><?php if($doedit) print $xxUnitPr; else print $xxPrice?></strong></td>
<?php	if($doedit) print '<td align="center"><strong>DEL</strong></td>' ?>
			</tr>
<?php
	$totoptpricediff = 0;
	if(mysql_num_rows($allorders)>0){
		$totoptpricediff = 0;
		$rowcounter=0;
		while($rsOrders = mysql_fetch_assoc($allorders)){
			$optpricediff = 0;
?>
			<tr>
			  <td valign="top" nowrap><?php if($doedit) print '<input type="button" value="..." onclick="updateoptions(' . $rowcounter . ')">&nbsp;<input type="hidden" name="cartid' . $rowcounter . '" value="' . str_replace('"','&quot;',$rsOrders["cartID"]) . '" />'?><strong><?php print editfunc($rsOrders["cartProdId"],'prodid' . $rowcounter,18)?></strong></td>
			  <td valign="top">
			  	<?php print editfunc($rsOrders["cartProdName"],'prodname' . $rowcounter,24)?><br />
				<? 
				$sql_cert="SELECT cert_id,cert_code FROM certificates WHERE cert_order_id=".$_GET["id"]." AND cert_prod_id='".$rsOrders["cartProdId"]."'";
				//echo $sql_cert;
				$result_cert=mysql_query($sql_cert);
				if(mysql_num_rows($result_cert)>0) {
					while($row_cert=mysql_fetch_assoc($result_cert)){?>					
					<stong>(<?=$row_cert['cert_code']?>)</strong> <a href="/admin/certs.php?mode=1&amp;sbcode=<?=$row_cert['cert_id']?>">view</a> | <a href="/admin/certs.php?mode=2&amp;sbcode=<?=$row_cert['cert_id']?>">history</a><br />
				<? } 
				}?>	
				<? 
				$sql_down="SELECT * FROM digitaldownloads WHERE orderID=".$_GET["id"]." AND type='".$rsOrders["cartProdId"]."'";
				//echo $sql_cert;
				$result_down=mysql_query($sql_down);
				if(mysql_num_rows($result_down)>0) {?>
					<ol style="margin:2px;">
					<? while($row_down=mysql_fetch_assoc($result_down)){?>					
					
					<li style="margin:1px; font-weight:bold;">License ID: <?=$row_down['licenseID']?><br />
					Password: <?=$row_down['password']?></li>
				<? 	  } ?>
					</ol>
				<? }?>				
			  </td>
			  <td valign="top"><?php
			if($doedit) print '<span id="optionsspan' . $rowcounter . '">';
			$sSQL = "SELECT coOptGroup,coCartOption,coPriceDiff,coOptID,optGroup,coExtendShipping FROM cartoptions LEFT JOIN options ON cartoptions.coOptID=options.optID WHERE coCartID=" . $rsOrders["cartID"] . " ORDER BY coID";
			$result = mysql_query($sSQL) or print(mysql_error());
			if(mysql_num_rows($result) > 0){
				if($doedit) print '<table border="0" cellspacing="0" cellpadding="1" width="100%">';
				while($rs2 = mysql_fetch_array($result)){
					if($doedit){
						print '<tr><td align="right"><strong>' . $rs2["coOptGroup"] . ':</strong></td><td>';
						if(is_null($rs2["optGroup"])){
							print 'xxxxxx';
						}else{
							$sSQL="SELECT optID," . getlangid("optName",32) . ",optPriceDiff,optType,optFlags,optStock,optPriceDiff AS optDims FROM options INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optGroup=" . $rs2["optGroup"] . ' ORDER BY optID';
							$result2 = mysql_query($sSQL) or print(mysql_error());
							if($rsl = mysql_fetch_assoc($result2)){
								if(abs($rsl["optType"])==2){
									print '<select onchange="dorecalc(true)" name="optn' . $rowcounter . '_' . $rs2["coOptID"] . '" id="optn' . $rowcounter . '_' . $rs2["coOptID"] . '" size="1">';
									do {
										print '<option value="' . $rsl["optID"] . "|" . (($rsl["optFlags"] & 1) == 1 ? ($rsOrders["cartProdPrice"]*$rsl["optPriceDiff"])/100.0 : $rsl["optPriceDiff"]) . '"';
										if($rsl["optID"]==$rs2["coOptID"]) print ' selected';
										print '>' . $rsl[getlangid("optName",32)];
										if((double)$rsl["optPriceDiff"] != 0){
											print ' ';
											if((double)$rsl["optPriceDiff"] > 0) print '+';
											if(($rsl["optFlags"] & 1) == 1)
												print number_format(($rsOrders["cartProdPrice"]*$rsl["optPriceDiff"])/100.0,2,'.','');
											else
												print number_format($rsl["optPriceDiff"],2,'.','');
										}
										print '</option>';
									} while($rsl = mysql_fetch_array($result2));
									print '</select>';
								}else{
									print "<input type='hidden' name='optn" . $rowcounter . '_' . $rs2["coOptID"] . "' value='" . $rsl["optID"] . "' /><textarea wrap='virtual' name='voptn" . $rowcounter . '_' . $rs2["coOptID"] . "' id='voptn". $rowcounter. '_' . $rs2["coOptID"] . "' cols='30' rows='3'>";
									print $rs2["coCartOption"] . '</textarea>';
								}
							}
						}
						print "</td></tr>";
					}else{						
						$extend_shipping='';
						if(!empty($rs2["coExtendShipping"])) $extend_shipping=' <span style="color:#FF0000;font-weight:bold;">(This option increases shipping time by '.$rs2["coExtendShipping"]. ' days)</span>';
						print '<strong>' . $rs2["coOptGroup"] . ':</strong> ' . str_replace(array("\r\n","\n"),array("<br />","<br />"),$rs2["coCartOption"]) .$cert_code. $extend_shipping . '<br />';
					}
					if($doedit)
						$optpricediff += $rs2["coPriceDiff"];
					else
						$rsOrders["cartProdPrice"] += $rs2["coPriceDiff"];
				}
				if($doedit) print '</table>';
			}else{
				print '-';
			}
			mysql_free_result($result);
			if($doedit) print '</span>' ?></td>
			  <td valign="top"><?php print editfunc($rsOrders["cartQuantity"],'quant' . $rowcounter . '" onchange="dorecalc(true)',5)?></td>
			  <td valign="top"><?php if($doedit) print editnumeric($rsOrders["cartProdPrice"],'price' . $rowcounter . '" onchange="dorecalc(true)',7); else print FormatEuroCurrency($rsOrders["cartProdPrice"]*$rsOrders["cartQuantity"])?>
<?php				if($doedit){
						print '<input type="hidden" id="optdiffspan' . $rowcounter . '" value="' . $optpricediff . '">';
						$totoptpricediff += ($optpricediff*$rsOrders["cartQuantity"]);
					}
			?></td>
<?php		if($doedit) print '<td align="center"><input type="checkbox" name="del_' . $rowcounter . '" id="del_' . $rowcounter . '" value="yes" /></td>' ?>
			</tr>
<?php		$rowcounter++;
		}
	}
?>
<!--NEXTPRODUCTCOMMENT-->
<?php if($doedit){ ?>
			<tr>
			  <td align="right" colspan="4">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td align="center"><?php if($doedit) print '<input style="width:30px;" type="button" value="-" onclick="extraproduct(\'-\')"> ' . $yyMoProd . ' <input style="width:30px;" type="button" value="+" onclick="extraproduct(\'+\')"> &nbsp; <input type="button" value="' . $yyRecal . '" onclick="dorecalc(false)">'?></td>
					<td align="right"><strong>Options Total:</strong></td>
				  </tr>
				</table></td>
			  <td align="left" colspan="2"><span id="optdiffspan"><?php print number_format($totoptpricediff, 2, '.', '')?></span></td>
			</tr>
<?php } ?>

			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxOrdTot?>:</strong></td>
			  <td align="left"><div id="ordTot"><?php echo sprintf("%.2f",$alldata["ordTotal"]); ?></div><input name="ordtotal" id="ordtotal" type="hidden" value="<?php echo $alldata["ordTotal"] ?>" /></td>
<?php		if($doedit) print '<td align="center">&nbsp;</td>' ?>
			</tr>
			<?php
	$runTot = $alldata["ordTotal"];
	$sql = "SELECT * FROM price_adj WHERE ordID = " . $_GET['id'] . " ORDER BY ordering";
	$res = mysql_query($sql) or print(mysql_error());
	if(mysql_num_rows($res) > 0) {
		$k=1;
		$prcTot = 0;
?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
<?php
		while($row=mysql_fetch_assoc($res)) {
			$price = 0;
			$disp_price = 0;
			$amount = '';
			
			if($row['type'] == 'credit') {
				if($row['amt_type'] == 'percentage') {
					$price = $runTot * ($row['amt'] * .01);
					$runTot -= $price;
					$prcTot -= $price;
					$disp_price = '-'.(int)$row['amt'].'%';
					$amount = '-'.money_format("%!.2n",$price);
				}else{
					$price = '$-'.money_format("%!.2n",$row['amt']);
					$runTot -= $row['amt'];
					$prcTot -= $row['amt'];
					$disp_price = $price;
					$amount = '-'.money_format("%!.2n",$row['amt']);
				}
			}else{
				if($row['amt_type'] == 'percentage') {
					$price = $runTot * ($row['amt'] * .01);
					$runTot += $price;
					$prcTot += $price;
					$disp_price = (int)$row['amt'].'%';
					$amount = money_format("%.2n",$price);
				}else{
					$price = money_format("%.2n",$row['amt']);
					$runTot += $row['amt'];
					$prcTot += $row['amt'];
					$disp_price = $price;
					$amount = money_format("%.2n",$row['amt']);
				}
			}
?>
			<tr>
				<td colspan="3" style="font-weight: bold; text-align: right">Price Adjustment <?=$k?>:</td>
				<td align="left"<?=(strstr($disp_price,"-"))?' style="color: red"':''?>><?=$disp_price?></td>
				<td align="left"<?=(strstr($disp_price,"-"))?' style="color: red"':''?>><?=$amount?></td>
			</tr>
<?php
			$k++;
		}
?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
<?php
	}
?>
<?php if((double)$alldata["ordShipping"]!=0.0 || $doedit){ ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxShippg?>:</strong></td>
			  <td align="left"><?php print editnumeric($alldata["ordShipping"],"ordShipping",7)?></td>
<?php		if($doedit) print '<td align="center">&nbsp;</td>' ?>
			</tr>
<?php }
	  if((double)$alldata["ordHandling"]!=0.0 || $doedit){ ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxHndlg?>:</strong></td>
			  <td align="left"><?php print editnumeric($alldata["ordHandling"],"ordHandling",7)?></td>
<?php		if($doedit) print '<td align="center">&nbsp;</td>' ?>
			</tr>
<?php }
	  if((double)$alldata["ordDiscount"]!=0.0 || $doedit){ ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxDscnts?>:</strong></td>
			  <td align="left"><font color="#FF0000"><?php print editnumeric($alldata["ordDiscount"],"ordDiscount",7)?></font></td>
<?php		if($doedit) print '<td align="center">&nbsp;</td>' ?>
			</tr>
<?php }
	  if((double)$alldata["ord_cert_amt"]!=0.0 || $doedit){ 
			$sqlcert="SELECT cert_code FROM certificates WHERE cert_id=".$alldata["ord_cert_id"];
			$resultcert=mysql_query($sqlcert);
			$rowcert=mysql_fetch_assoc($resultcert);
			?>
			<tr>
			  <td align="right" colspan="4"><strong><?='('.$rowcert["cert_code"].')'?> <?php print $xxGCerts?>:</strong></td>
			  <td align="left"><font color="#FF0000"><?php print editnumeric($alldata["ord_cert_amt"],"ord_cert_amt",7)?></font></td>
<?php		if($doedit) print '<td align="center">&nbsp;</td>' ?>
			</tr>
<?php }
	  if((double)$alldata["ordStateTax"]!=0.0 || $doedit){ ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxStaTax?>:</strong></td>
			  <td align="left"><?php print editnumeric($alldata["ordStateTax"],"ordStateTax",7)//$alldata["ordStateTax"]?></td>
<?php		if($doedit) print '<td align="center" nowrap><input type="text" name="staterate" id="staterate" size="1" value="' . $statetaxrate . '">%</td>' ?>
			</tr>
<?php }
	  if((double)$alldata["ordCountryTax"]!=0.0 || $doedit){ ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxCntTax?>:</strong></td>
			  <td align="left"><?php print editnumeric($alldata["ordCountryTax"],"ordCountryTax",7)?></td>
<?php		if($doedit) print '<td align="center" nowrap><input type="text" name="countryrate" id="countryrate" size="1" value="' . $countrytaxrate . '">%</td>' ?>
			</tr>
<?php }
	  if((double)$alldata["ordHSTTax"]!=0.0 || ($doedit && @$canadataxsystem)){ ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxHST?>:</strong></td>
			  <td align="left"><?php print editnumeric($alldata["ordHSTTax"],"ordHSTTax",7)?></td>
<?php		if($doedit) print '<td align="center" nowrap><input type="text" name="hstrate" id="hstrate" size="1" value="' . $hsttaxrate . '">%</td>' ?>
			</tr>
<?php } ?>
			<tr>
			  <td align="right" colspan="4"><strong><?php print $xxGndTot?>:</strong></td>
			  <td align="left"><span id="grandtotalspan"><?php print FormatEuroCurrency(($alldata["ordTotal"]+$alldata["ordStateTax"]+$alldata["ordCountryTax"]+$alldata["ordHSTTax"]+$alldata["ordShipping"]+$alldata["ordHandling"]+$prcTot)-$alldata["ordDiscount"]-$alldata["ord_cert_amt"])?></span></td>
<?php		if($doedit) print '<td align="center">&nbsp;</td>' ?>
			</tr>
	  </table>
</span>
		  </td>
		</tr>
<?php	if($isprinter && @$invoicefooter != ""){ ?>
		<tr> 
          <td width="100%"><?php print $invoicefooter?></td>
		</tr>
<?php	}elseif($doedit){ ?>
		<tr> 
          <td align="center" width="100%">&nbsp;<br /><input type="submit" value="<?php print $yyUpdate?>" /><br />&nbsp;</td>
		</tr>
<?php	} ?>
	  </table>
<?php
	if($doedit) print '</form>';
	if($doedit){
	// ADDED by Chad Jun-06-06
	// PRICE ADJUSTMENTS
?>
	
	<h2><a name="prc_adj"></a>Price Adjustments</h2>
	
<?php
	if(!empty($_GET['adj_err'])) {
?>
		<div style="margin: 5px auto; color: #FF0000; font-weight: bold; text-align: center"><?=$_GET['adj_err']?></div>
<?php
	}elseif(!empty($_GET['adj_msg'])) {
?>
		<div style="margin: 5px auto; color: #009900; font-weight: bold; text-align: center"><?=$_GET['adj_msg']?></div>
<?php
	}
?>
	<table width="95%" cellpadding="3" cellspacing="0" border="1" style="margin: 0 auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
		<tr style="background-color: #BFC9E0; color: #194C7F">
			<th width="60" style="text-align: center">Type</th>
			<th width="80" style="text-align: center">Amount Type</th>
			<th width="80" style="text-align: center">Amount</th>
			<th width="150" style="text-align: center">Date</th>
			<th>Note</th>
<?php
if(strstr($_SESSION['employee']['permissions'],"all") || $_SESSION['employee']['id']==19 || $_SESSION['employee']['id']==12 || $_SESSION['employee']['id']==2) {
?>
			<th width="28">Edit</th>
			<th width="28">Delete</th>
			<th width="28">Move Up</th>
			<th width="28">Move Down</th>
<?php
}
?>
		</tr>
<?php
	$sql = "SELECT * FROM price_adj WHERE ordID = " . $_GET["id"] . " ORDER BY ordering";
	$res = mysql_query($sql) or print(mysql_error());
	
	$num_rows = mysql_num_rows($res);
	if($num_rows > 0) {
		$j=0;
		while($row=mysql_fetch_assoc($res)) {
?>
		<tr<?=($j%0==0?'':' style="background-color: #E6E9F5"')?>>
			<td style="text-align: center"><?=$row['type']?></td>
			<td style="text-align: center"><?=$row['amt_type']?></td>
			<td style="text-align: right"><?=$row['amt']?></td>
			<td style="text-align: left"><?=date("n/j/Y g:i a",strtotime($row['date']))?></td>
			<td style="text-align: left"><?=$row['note']?></td>
<?php
if(strstr($_SESSION['employee']['permissions'],"all") || $_SESSION['employee']['id']==19 || $_SESSION['employee']['id']==12 || $_SESSION['employee']['id']==2) {
?>
			<td style="text-align: center"><a href="/admin/editprcadj.php?pa_id=<?=$row['id']?>&action=edit" onclick="window.open(this.href,'edit_txn','left=700,top=100,width=550,height=150,toolbar=0'); return false;"><img src="/lib/images/misc/edit.gif" height="24" width="24" /></a></td>
			<td style="text-align: center"><a href="/admin/editprcadj.php?pa_id=<?=$row['id']?>&action=delete" onclick="window.open(this.href,'edit_txn','left=700,top=100,width=550,height=150,toolbar=0'); return false;"><img src="/lib/images/misc/delete.gif" width="24" height="24" /></a></td>
			<td style="text-align: center"><? if($row['ordering']!=1){?><a href="/admin/mvprcadj.php?pa_id=<?=$row['id']?>&pa_ordID=<?=$row['ordID']?>&position=<?=$row['ordering']?>&action=moveup" onclick=""><img src="/lib/images/misc/arrow_up.png" width="24" height="24" /></a><? } ?></td>
			<td style="text-align: center"><? if($row['ordering']!=$num_rows){?><a href="/admin/mvprcadj.php?pa_id=<?=$row['id']?>&pa_ordID=<?=$row['ordID']?>&position=<?=$row['ordering']?>&action=movedown" onclick=""><img src="/lib/images/misc/arrow_down.png" width="24" height="24" /></a><? } ?></td>
<?php
}
?>
		</tr>
<?php
			$j++;
		}
	}else{
?>
		<tr>
			<td colspan="9" style="text-align: center; font-weight: bold">No Price Adjustments Found</td>
		</tr>
<?php
	}
?>
	</table>
	
<?php
if(strstr($_SESSION['employee']['permissions'],"all") || $_SESSION['employee']['id']==19 || $_SESSION['employee']['id']==12 || $_SESSION['employee']['id']==2) {
?>
	
	<input type="button" id="btn_prc_add" value="Add Price Adjustment" onclick="togglePrcAdd();" />
	
	<div id="div_prc_add" style="display: none">
		<form id="adj_frm" name="adj_frm" method="post" action="adminorderssz_process.php">
		<table cellpadding="3" cellspacing="0" border="1" style="margin: 0 auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
			<tr style="background-color: #BFC9E0; color: #194C7F">
				<td colspan="2" style="text-align: center; font-weight: bold; font-size: 14px">Add Price Adjustment</td>
			</tr>
			<tr>
				<td style="font-weight: bold">Type:</td>
				<td>
					<select id="adj_type" name="adj_type">
						<option value="" selected="selected">Choose...</option>
						<option value="credit">Credit</option>
						<option value="debit">Debit</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="font-weight: bold">Amt Type:</td>
				<td>
					<select id="adj_amt_type" name="adj_amt_type">
						<option value="" selected="selected">Choose...</option>
						<option value="fixed">Fixed</option>
						<option value="percentage">Percentage</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="font-weight: bold">Amount:</td>
				<td><input id="adj_amt" name="adj_amt" type="text" value="" /></td>
			</tr>
			<tr>
				<td style="font-weight: bold">Note:</td>
				<td><textarea id="adj_note" name="adj_note"></textarea></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center"><input type="submit" id="adj_submit" name="adj_submit" value="Add" /></td>
			</tr>
		</table>
		<input type="hidden" id="adj_ordID" name="adj_ordID" value="<?=$_GET["id"]?>" />
		<input type="hidden" id="adj_doedit" name="adj_doedit" value="<?=$_GET["doedit"]?>" />
		</form>
	</div>
<?php
}
	// ADD ENDED

	// ADDED by Chad Jun-05-06
	// TRANSACTIONS
	$sql = "SELECT * FROM transactions WHERE ordID = " . $_GET["id"];
	$res = mysql_query($sql) or print(mysql_error());
?>
	<h2><a name="aim"></a>Transactions</h2>
<?php
	if(!empty($_GET['aim_err'])) {
?>
		<div style="margin: 5px auto; color: #FF0000; font-weight: bold; text-align: center"><?=$_GET['aim_err']?></div>
<?php
	}elseif(!empty($_GET['aim_msg'])) {
?>
		<div style="margin: 5px auto; color: #009900; font-weight: bold; text-align: center"><?=$_GET['aim_msg']?></div>
<?php
	}
?>
	<table width="95%" cellpadding="3" cellspacing="0" border="1" style="margin: 0 auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
		<tr style="background-color: #BFC9E0; color: #194C7F">
			<th width="85" style="text-align: center">Type</th>
			<th width="60" style="text-align: right">Amount</th>
			<th width="80" style="text-align: center">TXN</th>
			<th width="120" style="text-align: left">Date</th>
			<th>Note</th>
		</tr>
<?php
	while($row=mysql_fetch_assoc($res)) {
?>
		<tr>
			<td style="text-align: center"><?=$row['type']?></td>
			<td style="text-align: center"><?=money_format("%.2n",$row['amt'])?></td>
			<td style="text-align: center"><?=$row['txn']?></td>
			<td style="text-align: center"><?=date("n/j/Y g:i a",strtotime($row['date_received']))?></td>
			<td><?=$row['note']?></td>
		</tr>
<?php	
	}
?>
	</table>
<?php
if(strstr($_SESSION['employee']['permissions'],"all") || $_SESSION['employee']['id']==19 || $_SESSION['employee']['id']==12 || $_SESSION['employee']['id']==2) {
?>
<input type="button" id="btn_add_txn" value="Credit or Void" onclick="toggleTXN();" />

<div id="div_trans" style="display: none">
	<form id="aim_frm" name="aim_frm" method="post" action="/admin/ordersprocess.php" onsubmit="return checkAIM(this);">
	<table cellpadding="3" cellspacing="0" border="1" style="margin: 5px auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
		<tr style="background-color: #BFC9E0; color: #194C7F">
			<td colspan="4" style="text-align: center; font-weight: bold; font-size: 14px">Credit or Void a Transaction</td>
		</tr>
		<tr>
			<th style="text-align: center">Type</th>
			<th style="text-align: center">TXN</th>
			<th style="text-align: center">Amount</th>
			<th>Note</th>
		</tr>
		<tr>
			<td valign="top">
				<select name="aim_type" id="aim_type">
					<option value="" selected="selected">Choose...</option>
					<option value="CREDIT">Credit</option>
					<option value="VOID">Void</option>
				</select>
			</td>
			<td valign="top"><input type="text" id="aim_txn" name="aim_txn" value="" autocomplete="off" /></td>
			<td valign="top"><input type="text" id="aim_amt" name="aim_amt" value="" autocomplete="off" /></td>
			<td valign="top"><textarea id="aim_note" name="aim_note"></textarea></td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: right"><input type="submit" id="aim_submit" name="aim_submit" value="Submit" /></td>
		</tr>
	</table>
	<input type="hidden" id="aim_inv" name="aim_inv" value="<?=$_GET["id"]?>" />
<?php
$tmp = explode(" ",$alldata["ordName"]);
?>
	<input type="hidden" id="aim_fname" name="aim_fname" value="<?=$tmp[0]?>" />
	<input type="hidden" id="aim_lname" name="aim_lname" value="<?=(!empty($tmp[1]))?$tmp[1]:''?>" />
	<input type="hidden" id="aim_doedit" name="aim_doedit" value="<?=$_GET['doedit']?>" />
	</form>
</div>

<input type="button" id="btn_charge" value="Charge CC" onclick="toggleCharge();" style="display: block; margin: 5px 5px 5px 0" />

<div id="div_charge" style="display: none">
	<? 
		if(!empty($alldata["ordEID"])){
			$sql_eid="SELECT * FROM customers WHERE custID=".$alldata["ordEID"];
			$result_eid=mysql_query($sql_eid);
			if(mysql_num_rows($result_eid)>0){
				$row_eid=mysql_fetch_assoc($result_eid);
			}
		}
	?>
	<form id="frmCharge" name="frmCharge" method="post" action="/admin/ordersprocess.php">
	<table cellpadding="3" cellspacing="0" border="1" style="margin: 5px auto 5px auto; border: 1px solid #BFC9E0; border-collapse: collapse">
		<tr style="background-color: #BFC9E0; color: #194C7F">
			<td colspan="2" style="font-size: 14px; font-weight: bold; text-align: center">Authorize &amp; Capture</td>
		</tr>
		<tr>
			<td>First Name:</td>
			<td><input type="text" id="am_fname" name="am_fname" value="<?=$row_eid['Name']?>" /></td>
		</tr>
		<tr>
			<td>Last Name:</td>
			<td><input type="text" id="am_lname" name="am_lname" value="" /></td>
		</tr>
		<tr>
			<td>Address:</td>
			<td><input type="text" id="am_address" name="am_address" value="<?=$row_eid['Address']?>" /></td>
		</tr>
		<tr>
			<td>Address 2:</td>
			<td><input type="text" id="am_address2" name="am_address2" value="<?=$row_eid['Address2']?>" /></td>
		</tr>
		<tr>
			<td>City:</td>
			<td><input type="text" id="am_city" name="am_city" value="<?=$row_eid['City']?>" /></td>
		</tr>
		<tr>
			<td>State:</td>
			<td><input type="text" id="am_state" name="am_state" value="<? if(!empty($row_eid['State2'])) echo $row_eid['State2']; else echo $row_eid['State']; ?>" /></td>
		</tr>
		<tr>
			<td>Zip:</td>
			<td><input type="text" id="am_zip" name="am_zip" value="<?=$row_eid['Zip']?>" /></td>
		</tr>
		<tr>
			<td>Amount:</td>
			<td><input type="text" id="am_amt" name="am_amt" value="" autocomplete="off" /></td>
		</tr>
		<tr>
			<td>CC#:</td>
			<td><input type="text" id="am_cc_num" name="am_cc_num" value="<?=Decrypt($row_eid['ccNum'], $cart_misc)?>" autocomplete="off" /></td>
		</tr>
		<tr>
			<td>Exp. Date:</td>
			<td> 
				<select id="am_exp_mon" name="am_exp_mon">
<?
		$ccexp=explode('/',$row_eid['ccExp']);
		for($i=1; $i<=12; $i++)
		{
			if($i<10)
				$i = '0'.$i;
			if($i == $ccexp[0])
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else if(date('n') == $i )
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else
				echo '<option value="'.$i.'">'.$i.'</option>';
		}
?>
				</select>&nbsp;/&nbsp;
				<select id="am_exp_year" name="am_exp_year">
<?
		$cur_year = date('Y');
		for($i=($cur_year); $i<=$cur_year+10; $i++)
		{
			if($i == $ccexp[1])
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			elseif(date('Y') == $i || $i==$ccexp[1])
				echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';
			else
				echo '<option value="'.$i.'">'.$i.'</option>';
		}
?>				
				</select>
			</td>
		</tr>
		<tr>
			<td>CCV:</td>
			<td><input type="text" id="am_ccv" name="am_ccv" value="<?=$row_eid['ccCCV']?>" autocomplete="off" /></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit" id="am_submit" name="am_submit" value="Submit" /></td>
		</tr>
	</table>
	<input type="hidden" id="am_inv" name="am_inv" value="<?=$_GET["id"]?>" />
	<input type="hidden" id="am_type" name="am_type" value="AUTH_CAPTURE" />
	<input type="hidden" id="am_doedit" name="am_doedit" value="<?=$_GET['doedit']?>" />
	</form>
</div>
<?php
}
} // End of User permission check

	// ADDED by Chad Apr-04-06
	// LOCATION HISTORY
	$qry = "SELECT * FROM location WHERE ordID = '".$_GET["id"]."'";
	$res = mysql_query($qry) or print(mysql_error());
?>
		<table width="300" cellpadding="3" cellspacing="0" style="margin: 10px auto; font-family: Verdana, Arial, Helvetica, sans-serif; border: 1px solid #4B1610; border-collapse: collapse">
			<tr style="border-bottom: 1px solid #4B1610">
				<td colspan="2" style="text-align: center; background-color: #4B1610; color: #FFFFFF; font-weight: bold">Location History</td>
			</tr>
<?php
	$i=0;
	if(mysql_num_rows($res) > 0 ) {
		while($row = mysql_fetch_assoc($res)) {
?>
			<tr style="background-color: #<?=($i%2==0)?'903E36':'903E36'?>; border-bottom: 1px solid #4B1610">
				<td width="50%" style="color: #FFFFFF"><?=$row['location']?></td>
				<td style="color: #FFFFFF"><?=$row['stamp']?></td>
			</tr>
<?php
			$i++;
		}
	}else{
?>
			<tr>
				<td colspan="2" style="text-align: center">No location found</td>
			</tr>
<?php
	}
?>
		</table>
<?php
	// ADD ENDED
	
	// ADDED by Chad Apr-04-06
	// FEDEX TRACKING
	$qry = "SELECT * FROM fedex WHERE ordID = '".$_GET["id"]."'";
	$res = mysql_query($qry);
	if(mysql_num_rows($res) > 0) {
		$row = mysql_fetch_assoc($res);
		$trackNum = $row['trackNum'];
		if(!empty($trackNum)) {
			include(DOCROOT.'includes/fedex/fedexdc.php');
			$fed = new FedExDC();
			$track_Ret = $fed->track(
				array(
					'1537' => $trackNum, //Tracking Number
					'1534' =>'Y' // detail_scan_indicator (Show me all the tracking data)
				)
			);
			
			$ctr = 0;
			$hasChanged = false;
			$isDelivered = false;
			for($i=1; $i<=$track_Ret[1584]; $i++) {
				// See Customer Service Page for displaying results
	?>
		<div style="margin: 10px auto; border: 2px solid #2C578A; width: 550px">
		<table align="center" width="550" border="0" cellpadding="3" cellspacing="0" style="font-size: 10px; font-family: Verdana, Arial, Helvetica, sans-serif">
			<tr>
				<th colspan="2" style="font-size: 18px; background-color: #2C578A; color: #FFF; font-weight: bold; text-align: left">Fed<span style="margin-left: -3px; color: #FF6600">Ex</span></th>
				<th colspan="3" style="font-size: 14px; text-align: right; background-color: #2C578A; color: #FFF; font-weight: bold">Tracking# <?=$trackNum?></th>
			</tr>
	<?php
			if(!empty($track_Ret['1339-'.$i]))
			{
				$tmp_date = $track_Ret['1339-'.$i];
				$est_del = substr($tmp_date,0,4).'-'.substr($tmp_date,4,2).'-'.substr($tmp_date,6,2);
	?>
			<tr>
				<td colspan="5" style="background-color: #2C578A; height: 15px">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="5" style="background-color: #DFE7FF"><strong>Estimated Delivery Date:</strong> <?=date('M j, Y',strtotime($est_del))?></td>
			</tr>
	<?php
			}
	?>
			<tr>
				<th align="left" width="150" colspan="2" style="background-color: #2C578A; color: #FFF; font-weight: bold">Date/Time</th>
				<th align="left" style="background-color: #2C578A; color: #FFF; font-weight: bold">Activity</th>
				<th align="left" style="background-color: #2C578A; color: #FFF; font-weight: bold">Location</th>
				<th align="left" style="background-color: #2C578A; color: #FFF; font-weight: bold">Details</th>
			</tr>
	<?php
				for($j=1; $j<=$track_Ret['1715-'.$i]; $j++)
				{
					$date = $track_Ret['1162-'.$i.'-'.$j];
					$year = substr($date,0,4);
					$mon = substr($date,4,2);
					$day = substr($date,6,2);
					$hrs = substr($track_Ret['1163-'.$i.'-'.$j],0,2);
					$min = substr($track_Ret['1163-'.$i.'-'.$j],2,2);
					$sec = substr($track_Ret['1163-'.$i.'-'.$j],4,2);
					
					$date = $year.'-'.$mon.'-'.$day.' '.$hrs.':'.$min.':'.$sec;
					$unixDate = strtotime($date);
					
					if(empty($track_Ret['1161-'.$i.'-'.$j]))
					{
						$state = $track_Ret['1164-'.$i.'-'.$j];
					}
					else
					{
						$state = $track_Ret['1161-'.$i.'-'.$j];
					}
										
					if(date('Ymd',$unixDate) != date('Ymd',$last_date))
					{
						$ctr++;
						$hasChanged = true;
					}
					
					if($track_Ret['1159-'.$i.'-'.$j] == 'Delivered' && empty($track_Ret['1711-'.$i.'-'.$j]))
					{
						$isDelivered = true;
					}
	?>
					<tr style="background-color: #<?=($ctr%2==0)?'FFF':'DFE7FF'?>">
			<?php
						if($hasChanged)
						{
			?>
						<td style="text-align: left" valign="top">
							<span style="font-weight: bold"><?=date('M j, Y',$unixDate)?></span>
						</td>
						<td valign="top" style="text-align: right; border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=date('g:i A',$unixDate)?></td>
			<?php
							$hasChanged = false;
						}
						else
						{
			?>
						<td valign="top" colspan="2" style="text-align: right; border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=date('g:i A',$unixDate)?></td>
			<?php
						}
			?>		
						</td>
						<td valign="top" style="border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=($isDelivered)?'<strong>':''?><?=$track_Ret['1159-'.$i.'-'.$j]?><?=($isDelivered)?'</strong>':''?></td>
						<td valign="top" style="border-right: 1px solid #<?=($ctr%2!=0)?'FFF':'DFE7FF'?>"><?=$track_Ret['1160-'.$i.'-'.$j]?>, <?=$state?></td>
						<td valign="top"><?=$track_Ret['1711-'.$i.'-'.$j]?></td>
					</tr>
	<?php
					if($isDelivered)
					{
						$isDelivered = false;
					}
					
					$last_date = $unixDate;
				}
			}
	?>
		  </table>
</div>
	<?php
		}
	}
	// ADD ENDED
	
	// ADDED by Chad Apr-03-06
	// DHL AND USPS TRACKING
	$qry = "SELECT * FROM dhl WHERE custPackID = '".$_GET["id"]."'";
	$res = mysql_query($qry) or print(mysql_error());
	if(mysql_num_rows($res) > 0) {
		$row = mysql_fetch_assoc($res);
		$trackNum = $row['DHLGMTrackNum'];
		if(!empty($trackNum)) {
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,"http://api.smartmail.com/tnt2.cfm?number=$trackNum&criteria=3&type=wddx&custid=rband&passwd=sm36732");
			//curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_HEADER,0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, "number=$trackNum&criteria=3&type=wddx&custid=rband&passwd=sm36732");
			$res = curl_exec($ch);
			curl_close($ch);
			$info = wddx_deserialize($res);
			if(empty($info['Detail'][0])) { //If no errors
?>
			<div style="height: 20px"></div>
			<table align="center" width="500" cellpadding="3" cellspacing="0" style="border: 1px solid #FFFFFF">
				<tr>
					<td colspan="2" style="color: #FFFFFF; background-color: #CC0000; text-align: center; font-weight: bold; font-size: 18px">DHL Tracking</td>
				</tr>
<?php
				if(!empty($info['TRACK_PKUP_DATE'][0])) {
?>
				<tr>
					<td style="background-color: #FFCC00; text-align: left; font-weight: bold"><?=$info['TRACK_PKUP_DATE'][0]?></td>
					<td bordercolor="#FFFFFF" style="background-color: #FFCC00">Picked Up by SmartMail</td>
				</tr>
<?php
				}
				if(!empty($info['TRACK_RECV_DATE'][0])) {
?>
				<tr>
					<td style="background-color: #FFCC00; text-align: left; font-weight: bold"><?=$info['TRACK_RECV_DATE'][0]?></td>
					<td bordercolor="#FFFFFF" style="background-color: #FFCC00">Arrived at Smart Center</td>
				</tr>
<?php
				}
				if(!empty($info['TRACK_ENCD_DATE'][0])) {
?>
				<tr>
					<td style="background-color: #FFCC00; text-align: left; font-weight: bold"><?=$info['TRACK_ENCD_DATE'][0]?></td>
					<td bordercolor="#FFFFFF" style="background-color: #FFCC00">Processed and Verified</td>
				</tr>
<?php
				}
				if(!empty($info['TRACK_DNSD_DATE'][0])) {
?>
				<tr>
					<td style="background-color: #FFCC00; text-align: left; font-weight: bold"><?=$info['TRACK_DNSD_DATE'][0]?></td>
					<td bordercolor="#FFFFFF" style="background-color: #FFCC00">Sent via <?=(!empty($info['TRACK_DNDC'][0]))?$info['TRACK_DNDC'][0]:"SmartMail"?></td>
				</tr>
<?php
				}
				if(!empty($info['TRACK_DNRC_DATE'][0])) {
?>
				<tr>
					<td style="background-color: #FFCC00; text-align: left; font-weight: bold"><?=$info['TRACK_DNRC_DATE'][0]?></td>
					<td bordercolor="#FFFFFF" style="background-color: #FFCC00">Received</td>
				</tr>
<?php
				}
				if(!empty($info['TRACK_MFST_DATE'][0])) {
?>
				<tr>
					<td style="background-color: #FFCC00; text-align: left; font-weight: bold"><?=$info['TRACK_MFST_DATE'][0]?></td>
					<td bordercolor="#FFFFFF" style="background-color: #FFCC00">Mail Delivered to Post Office</td>
				</tr>
<?php
				}
?>
			</table>
<?php
				// CHECK USPS TRACKING INFO
				if(!empty($info['TRACK_DELV_CONF'][0])) {
					$ch = curl_init();
					curl_setopt($ch,CURLOPT_URL,"http://Production.ShippingAPIs.com/ShippingAPI.dll");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch,CURLOPT_HEADER,0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'API=TrackV2&XML=<TrackFieldRequest USERID="268REMIN3619"><TrackID ID="'.$info['TRACK_DELV_CONF'][0].'"></TrackID></TrackFieldRequest>');
					$res = curl_exec($ch);
					curl_close($ch);
					
					include(APPPATH.'views/pages/admin/xml2array.php');
					
					$xmlData = new xml2array();
					$uspsData = $xmlData -> parseXMLintoarray($res);
?>
			<div style="margin: 5px auto; width: 500px">
			
				<div style="margin: 0; height: 5px; background-color: #CC0000"></div>
				<div style="margin: 0; padding: 2px; background-color: #0066CB; color: #FFFFFF; font-weight: bold; font-size: 18px; text-align: center">USPS Tracking<br /><span style="font-size: 11px">Tracking # <?=$info['TRACK_DELV_CONF'][0]?></span></div>
				<div style="margin: 0; height: 7px; background-color: #98CCFF"></div>
				<div style="margin: 2px 0 0 0; background-color: #E3F1FC; border: 1px solid #89B9E7">
<?php				
					if(is_array($uspsData['TrackResponse']['TrackInfo']['Error'])) {
						echo '<div style="margin: 0; text-align: center; font-weight: bold">Error Getting USPS Tracking Information</div>';
					}else{
						if(is_array($uspsData['TrackResponse']['TrackInfo']['TrackSummary'])) {
							$eventTime = $uspsData['TrackResponse']['TrackInfo']['TrackSummary']['EventDate'].' '.$uspsData['TrackResponse']['TrackInfo']['TrackSummary']['EventTime'];
?>
					<div style="margin: 0; padding: 3px; background-color: #89B9E7; color: #FFFFFF; font-weight: bold">Event Summary</div>
					<div style="margin: 0">
					<table width="500" border="0" cellpadding="3" cellspacing="0" style="margin: 2px;">
						<tr>
							<td valign="top" style="font-weight: bold"><?=$eventTime?></td>
							<td valign="top"><?=$uspsData['TrackResponse']['TrackInfo']['TrackSummary']['EventCity']?>, <?=$uspsData['TrackResponse']['TrackInfo']['TrackSummary']['EventState']?></td>
							<td valign="top"><?=$uspsData['TrackResponse']['TrackInfo']['TrackSummary']['Event']?></td>
						</tr>
					</table>
					</div>
<?php
						}
						if(is_array($uspsData['TrackResponse']['TrackInfo']['TrackDetail'])) {
?>
					<div style="margin: 0; padding: 3px; background-color: #89B9E7; color: #FFFFFF; font-weight: bold">Tracking Details</div>
					<table width="500" border="0" align="center" cellpadding="3" cellspacing="0" style="margin: 2px;">
<?php
							if(is_array($uspsData['TrackResponse']['TrackInfo']['TrackDetail'][0])){
								for($i=0; $i<count($uspsData['TrackResponse']['TrackInfo']['TrackDetail']); $i++) {
									$eventTime = $uspsData['TrackResponse']['TrackInfo']['TrackDetail'][$i]['EventDate'].' '.$uspsData['TrackResponse']['TrackInfo']['TrackDetail'][$i]['EventTime'];
?>
						<tr>
							<td valign="top" style="font-weight: bold"><?=$eventTime?></td>
							<td valign="top"><?=$uspsData['TrackResponse']['TrackInfo']['TrackDetail'][$i]['EventCity']?>, <?=$uspsData['TrackResponse']['TrackInfo']['TrackDetail'][$i]['EventState']?></td>
							<td valign="top"><?=$uspsData['TrackResponse']['TrackInfo']['TrackDetail'][$i]['Event']?></td>
						</tr>
<?php
								} // End of TrackDetail Loop
							}else{
								$eventTime = $uspsData['TrackResponse']['TrackInfo']['TrackDetail']['EventDate'].' '.$uspsData['TrackResponse']['TrackInfo']['TrackDetail']['EventTime'];
?>
						<tr>
							<td valign="top" style="font-weight: bold"><?=$eventTime?></td>
							<td valign="top"><?=$uspsData['TrackResponse']['TrackInfo']['TrackDetail']['EventCity']?>, <?=$uspsData['TrackResponse']['TrackInfo']['TrackDetail']['EventState']?></td>
							<td valign="top"><?=$uspsData['TrackResponse']['TrackInfo']['TrackDetail']['Event']?></td>
						</tr>
<?php
							}
?>
					</table>
<?php
						} // End of displaying all Tracking Details
					} // End of displaying all USPS tracking information
?>
				</div>
			
			</div>
<?php
				}
			}
		}
	}
	
	// ADD ENDED
	
}else{
	$sSQL = "SELECT ordID FROM orders WHERE ordStatus=1";
	if(@$_POST["act"] != "purge") $sSQL .= " AND ordStatusDate<'" . date("Y-m-d H:i:s", time()-(3*60*60*24)) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_assoc($result)){
		$theid = $rs["ordID"];
		$delOptions = "";
		$addcomma = "";
		$result2 = mysql_query("SELECT cartID FROM cart WHERE cartOrderID=" . $theid) or print(mysql_error());
		while($rs2 = mysql_fetch_assoc($result2)){
			$delOptions .= $addcomma . $rs2["cartID"];
			$addcomma = ",";
		}
		if($delOptions != ""){
			$sSQL = "DELETE FROM cartoptions WHERE coCartID IN (" . $delOptions . ")";
			mysql_query($sSQL) or print(mysql_error());
		}
		mysql_query("DELETE FROM cart WHERE cartOrderID=" . $theid) or print(mysql_error());
		mysql_query("DELETE FROM orders WHERE ordID=" . $theid) or print(mysql_error());
	}
	if(@$_POST["act"]=="authorize"){
		do_stock_management(trim($_POST["id"]));
		if(trim($_POST["authcode"]) != "")
			$sSQL = "UPDATE orders set ordAuthNumber='" . mysql_real_escape_string(trim($_POST["authcode"])) . "',ordStatus=3 WHERE ordID=" . $_POST["id"];
		else
			$sSQL = "UPDATE orders set ordAuthNumber='" . mysql_real_escape_string($yyManAut) . "',ordStatus=3 WHERE ordID=" . $_POST["id"];
		if(mysql_query($sSQL)) {
			if(!setNewLocation( 3 , $_POST["id"] )) print("Unable to record status change.");
		}else{
			print(mysql_error());
		}
		mysql_query("UPDATE cart SET cartCompleted=1 WHERE cartOrderID=" . $_POST["id"]) or print(mysql_error());
	}elseif(@$_POST["act"]=="status"){
		$maxitems=(int)($_POST["maxitems"]);
		for($index=0; $index < $maxitems; $index++){
			$iordid = trim($_POST["ordid" . $index]);
			$ordstatus = trim($_POST["ordstatus" . $index]);
			$ordauthno = "";
			$oldordstatus=999;
			$result = mysql_query("SELECT ordStatus,ordAuthNumber,ordEmail,ordDate,".getlangid("statPublic",64).",ordStatusInfo,ordName FROM orders INNER JOIN orderstatus ON orders.ordStatus=orderstatus.statID WHERE ordID=" . $iordid) or print(mysql_error());
			if($rs = mysql_fetch_assoc($result)){
				$oldordstatus=$rs["ordStatus"];
				$ordauthno=$rs["ordAuthNumber"];
				$ordemail=$rs["ordEmail"];
				$orddate=strtotime($rs["ordDate"]);
				$oldstattext=$rs[getlangid("statPublic",64)];
				$ordstatinfo=$rs["ordStatusInfo"];
				$ordername=$rs["ordName"];
			}
			if(! ($oldordstatus==999) && ($oldordstatus < 3 && $ordstatus >=3)){
				// This is to force stock management
				mysql_query("UPDATE cart SET cartCompleted=0 WHERE cartOrderID=" . $iordid) or print(mysql_error());
				do_stock_management($iordid);
				mysql_query("UPDATE cart SET cartCompleted=1 WHERE cartOrderID=" . $iordid) or print(mysql_error());
				if($ordauthno=="") mysql_query("UPDATE orders SET ordAuthNumber='". mysql_real_escape_string($yyManAut) . "' WHERE ordID=" . $iordid) or print(mysql_error());
			}
			if(! ($oldordstatus==999) && ($oldordstatus >=3 && $ordstatus < 3)) release_stock($iordid);
			if($iordid != "" && $ordstatus != ""){
				if($oldordstatus != (int)$ordstatus && @$_POST["emailstat"]=="1"){
					$result = mysql_query("SELECT ".getlangid("statPublic",64)." FROM orderstatus WHERE statID=" . $ordstatus);
					if($rs = mysql_fetch_assoc($result))
						$newstattext = $rs[getlangid("statPublic",64)];
					$emailsubject = "Order status updated";
					if(@$orderstatussubject != "") $emailsubject=$orderstatussubject;
					$ose = $orderstatusemail;
					$ose = str_replace("%orderid%", $iordid, $ose);
					$ose = str_replace("%orderdate%", date($dateformatstr, $orddate), $ose);// . " " . date("H:i", $orddate), $ose);
					$ose = str_replace("%oldstatus%", $oldstattext, $ose);
					$ose = str_replace("%newstatus%", $newstattext, $ose);
					$thetime = time() + ($dateadjust*60*60);
					$ose = str_replace("%date%", date($dateformatstr, $thetime), $ose);// . " " . date("H:i", $thetime), $ose);
					$ose = str_replace("%statusinfo%", $ordstatinfo, $ose);
					$ose = str_replace("%ordername%", $ordername, $ose);
					$ose = str_replace("%nl%", $emlNl, $ose);
					if(@$customheaders == ""){
						$customheaders = "MIME-Version: 1.0\n";
						$customheaders .= "From: %from% <%from%>\n";
						if(@$htmlemails==TRUE)
							$customheaders .= "Content-type: text/html; charset=".$emailencoding."\n";
						else
							$customheaders .= "Content-type: text/plain; charset=".$emailencoding."\n";
					}
					$headers = str_replace('%from%',$emailAddr,$customheaders);
					$headers = str_replace('%to%',$ordemail,$headers);
					if((int)$ordstatus==9) {
						$ose = $orderstatusshippedemail;
						$ose = str_replace("%orderid%", $iordid, $ose);
						$ose = str_replace("%orderdate%", date($dateformatstr, $orddate) . " " . date("H:i", $orddate), $ose);
						$emailsubject = $orderstatusshippedsubject;
					}
					mail($ordemail, $emailsubject, $ose, $headers);
				}
				if($oldordstatus != (int)$ordstatus) {
					if(mysql_query("UPDATE orders SET ordStatus=" . $ordstatus . ",ordStatusDate='" . date("Y-m-d H:i:s", time() + ($dateadjust*60*60)) . "' WHERE ordID=" . $iordid)) {
						if(!setNewLocation( $ordstatus , $iordid )) print("Unable to record status change.");
					}else{
						print(mysql_error());
					}
				}
			}
		}
	}
	if(@$_POST["sd"] != "")
		$sd = @$_POST["sd"];
	elseif(@$_GET["sd"] != "")
		$sd = @$_GET["sd"];
	else
		$sd = date($admindatestr, time() + ($dateadjust*60*60));
	if(@$_POST["ed"] != "")
		$ed = @$_POST["ed"];
	elseif(@$_GET["ed"] != "")
		$ed = @$_GET["ed"];
	else
		$ed = date($admindatestr, time() + ($dateadjust*60*60));
	$sd = parsedate($sd);
	$ed = parsedate($ed);
	if($sd > $ed) $ed = $sd;
	$fromdate = trim(@$_POST["fromdate"]);
	$todate = trim(@$_POST["todate"]);
	$ordid = trim(str_replace('"',"",str_replace("'","",@$_POST["ordid"])));
	$origsearchtext = trim(unstripslashes(@$_POST["searchtext"]));
	$searchtext = trim(mysql_real_escape_string(unstripslashes(@$_POST["searchtext"])));
	$ordstatus = "";
	if(@$_POST["powersearch"]=="1"){
		$sSQL = "SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal-ordDiscount AS ordTot,ordTransID,order_changed,ordDiscountText,ordEID FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus>=0 ";
		$addcomma = "";
		if(is_array(@$_POST["ordstatus"])){
			foreach($_POST["ordstatus"] as $objValue){
				if(is_array($objValue))$objValue=$objValue[0];
				$ordstatus .= $addcomma . $objValue;
				$addcomma = ",";
			}
		}else
			$ordstatus = trim((string)@$_POST["ordstatus"]);
		//discounts
		$ordcoupon = trim((string)@$_POST["ordcoupon"]);
		if($ordid != ""){
			if(is_numeric($ordid)){
				$sSQL .= " AND ordID=" . $ordid;
			}else{
				$success=FALSE;
				$errmsg="The order id you specified seems to be invalid - " . $ordid;
				$sSQL .= " AND ordID=0";
			}
		}else{
			if($fromdate != ""){
				if(is_numeric($fromdate))
					$thefromdate = time()-($fromdate*60*60*24);
				else
					$thefromdate = parsedate($fromdate);
				if($todate=="")
					$thetodate = $thefromdate;
				elseif(is_numeric($todate))
					$thetodate = time()-($todate*60*60*24);
				else
					$thetodate = parsedate($todate);
				if($thefromdate > $thetodate){
					$tmpdate = $thetodate;
					$thetodate = $thefromdate;
					$thefromdate = $tmpdate;
				}
				$sd = $thefromdate;
				$ed = $thetodate;
				$sSQL .= " AND ordDate BETWEEN '" . date("Y-m-d", $sd) . "' AND '" . date("Y-m-d", $ed) . " 23:59:59'";
			}
			//discount
			if($ordcoupon != "") $sSQL .= " AND ordDiscountText = '" . $ordcoupon . "'";
			if($ordstatus != "" && strpos($ordstatus,"9999")===FALSE) $sSQL .= " AND ordStatus IN (" . $ordstatus . ")";
			if($searchtext != "") $sSQL .= " AND (ordTransID LIKE '%" . $searchtext . "%' OR ordAuthNumber LIKE '%" . $searchtext . "%' OR ordName LIKE '%" . $searchtext . "%' OR ordEmail LIKE '%" . $searchtext . "%' OR ordAddress LIKE '%" . $searchtext . "%' OR ordCity LIKE '%" . $searchtext . "%' OR ordState LIKE '%" . $searchtext . "%' OR ordZip LIKE '%" . $searchtext . "%' OR ordPhone LIKE '%" . $searchtext . "%')";
			if($_POST['ordPOAPOs'] == 'shipping_APOs') {
				$sSQL .= " AND IF(ordShipAddress != '',ordShipState IN('AA','AE','AP') AND ordShipPoApo = 1,ordState IN('AA','AE','AP') AND ordPoApo = 1)";
			}elseif($_POST['ordPOAPOs'] == 'shipping_POs') {
				$sSQL .= " AND IF(ordShipAddress != '',ordShipState NOT IN('AA','AE','AP') AND ordShipPoApo = 1,ordState NOT IN('AA','AE','AP') AND ordPoApo = 1)";
			}elseif($_POST['ordPOAPOs'] == 'shipping_PO_APO') {
				$sSQL .= " AND IF(ordShipAddress != '',ordShipPoApo = 1,ordPoApo = 1)";
			}elseif($_POST['ordPOAPOs'] == 'APOs') {
				$sSQL .= " AND ((ordPoApo = 1 AND ordState IN('AA','AE','AP')) OR (ordPoApo = 1 AND ordState IN('AA','AE','AP')))";
			}elseif($_POST['ordPOAPOs'] == 'PO_APO') {
				$sSQL .= " AND (ordPoApo = 1 OR ordShipPoApo = 1)";
			}elseif($_POST['ordPOAPOs'] == 'POs') {
				$sSQL .= " AND ((ordPoApo = 1 AND ordState NOT IN('AA','AE','AP')) OR (ordPoApo = 1 AND ordState NOT IN('AA','AE','AP')))";
			}
			if($_POST['custID'] !="") $sSQL .= " AND ordEID=".$_POST['custID'];
		}
		$sSQL .= " AND ordEID=13 ORDER BY ordID";
	}else{
		$sSQL = "SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal-ordDiscount AS ordTot,ordTransID,order_changed,ordEID FROM orders LEFT JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate BETWEEN '" . date("Y-m-d", $sd) . "' AND '" . date("Y-m-d", $ed) . " 23:59:59' AND ordEID=13 ORDER BY ordID";
	}
	$alldata = mysql_query($sSQL) or print(mysql_error());
	//echo $sSQL;
	$hasdeleted=false;
	$sSQL = "SELECT COUNT(*) AS NumDeleted FROM orders WHERE ordStatus=1";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rs = mysql_fetch_assoc($result);
	if($rs["NumDeleted"] > 0) $hasdeleted=true;
	mysql_free_result($result);
?>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/popcalendar.js">
</script>
<script language="JavaScript" type="text/javascript">
<!--
function delrec(id) {
cmsg = "<?php print $yyConDel?>\n"
if (confirm(cmsg)) {
	document.mainform.id.value = id;
	document.mainform.act.value = "delete";
	document.mainform.sd.value="<?php print date($admindatestr, $sd)?>";
	document.mainform.ed.value="<?php print date($admindatestr, $ed)?>";
	document.mainform.submit();
}
}
function authrec(id) {
var aucode;
cmsg = "<?php print $yyEntAuth?>"
if ((aucode=prompt(cmsg,'<?php print $yyManAut?>'))!=null) {
	document.mainform.id.value = id;
	document.mainform.act.value = "authorize";
	document.mainform.authcode.value = aucode;
	document.mainform.sd.value="<?php print date($admindatestr, $sd)?>";
	document.mainform.ed.value="<?php print date($admindatestr, $ed)?>";
	document.mainform.submit();
}
}
function checkcontrol(tt,evt){
<?php if(strstr(@$HTTP_SERVER_VARS["HTTP_USER_AGENT"], "Gecko")){ ?>
theevnt = evt;
return;
<?php }else{ ?>
theevnt=window.event;
<?php } ?>
if(theevnt.ctrlKey){
	maxitems=document.mainform.maxitems.value;
	for(index=0;index<maxitems;index++){
		if(eval('document.mainform.ordstatus'+index+'.length') > tt.selectedIndex){
			eval('document.mainform.ordstatus'+index+'.selectedIndex='+tt.selectedIndex);
			eval('document.mainform.ordstatus'+index+'.options['+tt.selectedIndex+'].selected=true');
		}
	}
}
}
function displaysearch(){
thestyle = document.getElementById('searchspan').style;
if(thestyle.display=='none')
	thestyle.display = 'block';
else
	thestyle.display = 'none';
}
function checkprinter(tt,evt){

}
// -->
</script>
      <div style="width:19.4%; float:right; border:#030133 solid 1px;">
	  <table width="100%" border="0" cellspacing="1" cellpadding="2">
		<?  
		if(empty($_POST['packing'])) {
			$_POST['month']=date('m');
			$_POST['year']=date('Y');
			$thefromdate=$_POST['year'].'_'.$_POST['month'];
		} else $thefromdate=$_POST['year'].'_'.$_POST['month'];
		
		$file_path='order_ids/';
		$folderName=$thefromdate;
		$dir = $file_path.$folderName."/";
		?>
			<tr bgcolor="#030133">
			  <td width="33%" align="center"><strong><font color="#E7EAEF">Shieldzone Packing Slips</font></strong></td>
			</tr>
			<tr>
			  <td width="33%" align="center">
			  <form action="" method="post">			  
			  <select name="month">
			    <option value="01" <?php if (!(strcmp('01', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Jan</option>
			    <option value="02" <?php if (!(strcmp('02', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Feb</option>
			    <option value="03" <?php if (!(strcmp('03', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Mar</option>
			    <option value="04" <?php if (!(strcmp('04', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Apr</option>
			    <option value="05" <?php if (!(strcmp('05', $_POST['month']))) {echo "selected=\"selected\"";} ?>>May</option>
			    <option value="06" <?php if (!(strcmp('06', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Jun</option>
			    <option value="07" <?php if (!(strcmp('07', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Jul</option>
			    <option value="08" <?php if (!(strcmp('08', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Aug</option>
			    <option value="09" <?php if (!(strcmp('09', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Sep</option>
			    <option value="10" <?php if (!(strcmp('10', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Oct</option>
			    <option value="11" <?php if (!(strcmp('11', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Nov</option>
			    <option value="12" <?php if (!(strcmp('12', $_POST['month']))) {echo "selected=\"selected\"";} ?>>Dec</option>
			  </select>
			  <?
			  	$start_year='2006';
				$this_year=date('Y');
				
			  ?>
			  <select name="year">
			    <? for($i=$start_year;$i<=$this_year;$i++){?>
			    <option value="<?=$i?>" <?php if (!(strcmp($i, $_POST['year']))) {echo "selected=\"selected\"";} ?>><?=$i?></option>
			    <? } ?>
			  </select>
			  <input name="packing" type="submit" value="go" />
			  </form>
			  </td>
			</tr>

			<tr>
			  <?
		// Open a known directory, and proceed to read its contents
		$i=0;
		if (is_dir($dir)) {
		   if ($dh = opendir($dir)) {
			   while (($file = readdir($dh)) !== false) {
				   if($i>1 && strstr($file,'sz')) {
				   $file_info1[$i]['dir']=$dir;
				   $file_info1[$i]['file']=$file;
					}
			   $i++;
			   }
			   closedir($dh);
		   }
		}
		if(!empty($file_info1)) array_multisort($file_info1,SORT_DESC,SORT_REGULAR);
		?>
			  <? /* 
		for($i=0;$i<16 && $i<count($file_info1);$i++) {
			echo '<a href="/admin/print2.php?printer=true&path='.$folderName.'&file='.$file_info1[$i]['file'].'" target="_blank">'.(str_replace('.txt','',$file_info1[$i]['file'])).'</a><br />';
		} */
		?>
			  <td align="center">
			  <? for($i=0;$i<count($file_info1);$i++) {?>
			  <table width="100%" border="0">
			  <tr>
				<td><a href="/admin/print2.php?printer=true&path=<?=$folderName?>&file=<?=$file_info1[$i]['file']?>" target="_blank"><?=(str_replace('.txt','',$file_info1[$i]['file']))?></a></td>
				<td width="20" align="right"><a href="/admin/print2.php?printer=true&ptype=1&path=<?=$folderName?>&file=<?=$file_info1[$i]['file']?>" target="_blank">Printer</a></td>
			  </tr>
			</table>
			<? } ?>
			  </td>
			</tr>
		</table>
	  </div>
	  <div style="width:80%; border:#030133 solid 1px; " >
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="">
        <tr>
          <td width="100%" align="center">
<?php	$themask = 'yyyy-mm-dd';
		if($admindateformat==1)
			$themask='mm/dd/yyyy';
		elseif($admindateformat==2)
			$themask='dd/mm/yyyy';
		if(! $success) print "<p><font color='#FF0000'>" . $errmsg . "</font></p>"; ?>
			<span name="searchspan" id="searchspan" <?php if($usepowersearch) print 'style="display:block"'; else print 'style="display:none"'?>>
            <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="">
			  <form method="post" action="/admin/orderssz.php" name="psearchform">
			  <input type="hidden" name="powersearch" value="1" />
			  <tr bgcolor="#030133"><td colspan="6"><strong><font color="#E7EAEF">&nbsp;<?php print $yyPowSea?></font></strong></td></tr>
			  <tr bgcolor="#E7EAEF"> 
                <td align="right"><strong><?php print $yyOrdFro?>:</strong></td>
				<td align="left">&nbsp;
			    <input type="text" size="14" name="fromdate" value="<?php print $fromdate?>" /> <input type=button onclick="popUpCalendar(this, document.forms.psearchform.fromdate, '<?php print $themask?>', 0)" value='DP' /></td>
				<td align="right"><strong><?php print $yyOrdTil?>:</strong></td>
				<td align="left">&nbsp;
				    <input type="text" size="14" name="todate" value="<?php print $todate?>" />
                    <input type="button" onclick="popUpCalendar(this, document.forms.psearchform.todate, '<?php print $themask?>', -205)" value='DP' /></td>
				<td rowspan="2" align="right"><strong><?php print $yyOrdSta?>:</strong></td>
				<td rowspan="2" align="left"><select name="ordstatus" size="3" multiple="multiple" id="ordstatus">
				  <option value="9999" <?php if(strpos($ordstatus,"9999") !== FALSE) print "selected"?>><?php print $yyAllSta?></option>
				  <?php
						$ordstatus="";
						$addcomma = "";
						if(is_array(@$_REQUEST["ordstatus"])){
							foreach($_REQUEST["ordstatus"] as $objValue){
								if(is_array($objValue))$objValue=$objValue[0];
								$ordstatus .= $addcomma . $objValue;
								$addcomma = ",";
							}
						}else
							$ordstatus = trim(@$_REQUEST["ordstatus"]);
						$ordstatusarr = explode(",", $ordstatus);
						for($index=0; $index < $numstatus; $index++){
							print '<option value="' . $allstatus[$index]["statID"] . '"';
							if(is_array($ordstatusarr)){
								foreach($ordstatusarr as $objValue)
									if($objValue==$allstatus[$index]["statID"]) print " selected";
							}
							print ">" . $allstatus[$index]["statPrivate"] . "</option>";
						} ?>
				  </select>
				</td>
			  </tr>
			  <tr bgcolor="#EAECEB">
				<td align="right"><strong><?php print $yyOrdId?>:</strong></td>
				<td align="left">&nbsp;<input type="text" size="14" name="ordid" value="<?php print $ordid?>" /></td>
				<td align="right"><strong><?php print $yySeaTxt?>:</strong></td>
				<td align="left">&nbsp;
				    <input type="text" size="24" name="searchtext" value="<?php print $origsearchtext?>" /></td>
				</tr>
			  <tr bgcolor="#E7EAEF">
				<td align="right">&nbsp;</td>
				<td align="left">&nbsp;				</td>
				<td align="right">&nbsp;</td>
				<td align="left">&nbsp;</td>
				<td colspan="2" align="center"><input type="checkbox" name="startwith" value="1" <?php if($usepowersearch) print "checked"?> /> <strong><?php print $yyStaPow?></strong><br /><br />
				  <input type="submit" value="<?php print $yySearch?>" /> <input type="button" value="Stats" onclick="document.forms.psearchform.action='/admin/statssz.php';document.forms.psearchform.submit();" /></td>
			  </tr>
			  <tr>
			  	<td colspan="6">
				<?
					$sql_status =  "SELECT count( * ) as statcount , os.statPrivate , os.statID
									FROM orders o, orderstatus os
									WHERE o.ordStatus = os.statID
									AND ordEID=13
									AND o.ordStatus
									BETWEEN 0
									AND 10
									GROUP BY os.statID,os.statPrivate";
					$result_status=mysql_query($sql_status);
					while($row_status=mysql_fetch_assoc($result_status)){?>
			  		<div style="color:#000066; font-weight:bold; border:#030133 solid 1px; margin:2px; float:left; width:146px;padding:2px; text-align:center; background-color:#E7EAEF;"><?=$row_status['statID'].'-'.$row_status['statPrivate'].': '.$row_status['statcount'] ?></div>
			  	<? } ?>			  	</td>
			  </tr>
			  </form>
			</table>
			</span>
            <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <form method="post" action="/admin/orderssz.php">
			  <tr>
			    <td align="center"> <input type="button" value="<?php print $yyPowSea?>" onclick="displaysearch()" /></td><td align="center"><p><strong><?php print $yyShoFrm?>:</strong> <select name="sd" size="1"><?php
					$gotmatch=FALSE;
					$thetime = time() + ($dateadjust*60*60);
					$dayToday = date("d",$thetime);
					$monthToday = date("m",$thetime);
					$yearToday = date("Y",$thetime);
					for($index=$dayToday; $index > 0; $index--){
						$thedate = mktime(0, 0, 0, $monthToday, $index, $yearToday);
						$thedatestr = date($admindatestr, $thedate);
						print "<option value='" . $thedatestr . "'";
						if($thedate==$sd){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					for($index=1; $index<=12; $index++){
						$thedatestr = date($admindatestr, $thedate = mktime(0,0,0,date("m",$thetime)-$index,1,date("Y",$thetime)));
						if(! $gotmatch && $thedate < $sd){
							print "<option value='" . date($admindatestr, $sd) . "' selected>" . date($admindatestr, $sd) . "</option>";
							$gotmatch=TRUE;
						}
						print "<option value='" . $thedatestr . "'";
						if($thedate==$sd){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					if(!$gotmatch) print "<option value='" . date($admindatestr, $sd) . "' selected>" . date($admindatestr, $sd) . "</option>";
				?></select> <strong><?php print $yyTo?>:</strong> <select name="ed" size="1"><?php
					$gotmatch=FALSE;
					$dayToday = date("d",$thetime);
					$monthToday = date("m",$thetime);
					$yearToday = date("Y",$thetime);
					for($index=$dayToday; $index > 0; $index--){
						$thedate = mktime(0, 0, 0, $monthToday, $index, $yearToday);
						$thedatestr = date($admindatestr, $thedate);
						print "<option value='" . $thedatestr . "'";
						if($thedate==$ed){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					for($index=1; $index<=12; $index++){
						if(! $gotmatch && $thedate < $ed){
							print "<option value='" . date($admindatestr, $ed) . "' selected>" . date($admindatestr, $ed) . "</option>";
							$gotmatch=TRUE;
						}
						$thedatestr = date($admindatestr, $thedate = mktime(0,0,0,date("m",$thetime)-$index,1,date("Y",$thetime)));
						print "<option value='" . $thedatestr . "'";
						if($thedate==$ed){
							print " selected";
							$gotmatch=TRUE;
						}
						print ">" . $thedatestr . "</option>\n";
					}
					if(!$gotmatch) print "<option value='" . date($admindatestr, $sd) . "' selected>" . date($admindatestr, $sd) . "</option>";
				?></select> <input type="submit" value="Go" /></td>
			  </tr>
			  <tr><td colspan="2">&nbsp;</td></tr>
			  </form>
			</table>
			<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="">
			  <tr bgcolor="#030133"> 
                <td align="center"><strong><font color="#E7EAEF"><?php print $yyOrdId?></font></strong></td>
				<td align="center"><strong><font color="#E7EAEF"><?php print $yyName?></font></strong></td>
				<td align="center"><strong><font color="#E7EAEF"><?php print $yyMethod?></font></strong></td>
				<td align="center"><strong><font color="#E7EAEF"><?php print $yyAutCod?></font></strong></td>
				<td align="center"><strong><font color="#E7EAEF"><?php print $yyDate?></font></strong></td>
				<td align="center" bgcolor="#030133"><strong><font color="#E7EAEF"><?php print $yyStatus?></font></strong></td>
			  </tr>
			  <form method="post" name="mainform" action="/admin/orderssz.php">
			  <?php if(@$_POST["powersearch"]=="1"){ ?>
			  <input type="hidden" name="powersearch" value="1" />
			  <input type="hidden" name="fromdate" value="<?php print trim(@$_POST["fromdate"])?>" />
			  <input type="hidden" name="todate" value="<?php print trim(@$_POST["todate"])?>" />
			  <input type="hidden" name="ordid" value="<?php print trim(str_replace('"','',str_replace("'",'',@$_POST["ordid"])))?>" />
			  <input type="hidden" name="origsearchtext" value="<?php print trim(str_replace('"','&quot;',@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="searchtext" value="<?php print trim(str_replace('"',"&quot;",@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="ordstatus[]" value="<?php print $ordstatus?>" />
			  <input type="hidden" name="startwith" value="<?php if($usepowersearch) print "1"?>" />
			  <?php } ?>
			  <input type="hidden" name="act" value="xxx" />
			  <input type="hidden" name="id" value="xxx" />
			  <input type="hidden" name="authcode" value="xxx" />
			  <input type="hidden" name="ed" value="<?php print date($admindatestr, $ed)?>" />
			  <input type="hidden" name="sd" value="<?php print date($admindatestr, $sd)?>" />
<?php
	if(mysql_num_rows($alldata) > 0){
		$rowcounter=0;
		$ordTot=0;
		$i=0;
		$num_rows_order=mysql_num_rows($alldata);
		while($rs = mysql_fetch_assoc($alldata)){
			$order_id_array[$i]=$rs["ordID"];
			$i++;			
			if($rs["ordStatus"]>=3) $ordTot += $rs["ordTot"];
			if($rs["ordStatus"]>=3) $num_auth_order += 1;
			if($rs["ordAuthNumber"]=="" || is_null($rs["ordAuthNumber"])){
				$startfont="<font color='#FF0000'>";
				$endfont="</font>";
			} else{
				$startfont="";
				$endfont="";
			}
			if($rs["order_changed"]=='yes'){ 
				$startfont="<font color='#00CC00'>";
				$endfont="</font>";
			}
			if(@$bgcolor=="#E7EAEF") $bgcolor="#EAECEB"; else $bgcolor="#E7EAEF";
			//if(!empty($rs["ordEID"])) $bgcolor="#FA6561";			
?>
			  <tr bgcolor="<?php print $bgcolor?>"> 
                <td align="center"><a onclick="return(checkprinter(this,event));" href="/admin/orderssz.php?id=<?php print $rs["ordID"]?>"><?php print "<strong>" . $startfont . $rs["ordID"] . $endfont . "</strong>"?></a></td>
				<td align="center"><a onclick="return(checkprinter(this,event));" href="/admin/orderssz.php?id=<?php print $rs["ordID"]?>"><?php print $startfont . $rs["ordName"] . $endfont?></a></td>
				<td align="center"><?php print $startfont . $rs["payProvName"] . ($rs["payProvName"]=='PayPal' && trim($rs["ordTransID"]) != '' ? ' CC' : '') . $endfont?></td>
				<td align="center"><?
					if($rs["ordAuthNumber"]=="" || is_null($rs["ordAuthNumber"])){
						$isauthorized=FALSE;
						//print '<input type="button" name="auth" value="' . $yyAuthor . '" onclick="authrec(\'' . $rs["ordID"] . '\')" />';
					}else{
						print $rs["ordAuthNumber"] ;
						$isauthorized=TRUE;
					}
				?></td>
				<td align="center"><font size="1"><?php print $startfont . date($admindatestr . "\<\\b\\r\>H:i:s", strtotime($rs["ordDate"])) . $endfont?></font></td>
				<td align="center"><input type="hidden" style="background-color: " name="ordid<?php print $rowcounter?>" value="<?php print $rs["ordID"]?>" />
				<?php
						$gotitem=FALSE;
						for($index=0; $index<$numstatus; $index++){
							if(! $isauthorized && $allstatus[$index]["statID"]>2) break;
							//if(! ($rs["ordStatus"] != 2 && $allstatus[$index]["statID"]==2)){
								//print $allstatus[$index]["statID"];
								if($rs["ordStatus"]==$allstatus[$index]["statID"]){
									print $allstatus[$index]["statPrivate"];
								}
							//}
						}
					 ?>						</td>
			  </tr>
<?php		$rowcounter++;
			if($rowcounter>=1000){
				print "<tr><td colspan='6' align='center'><strong>Limit of " . $rowcounter . " orders reached. Please refine your search.</strong></td></tr>";
				break;
			}
		}
?>
			  <tr>
				<td align="center" bgcolor="#030133"><strong><font color="#E7EAEF"><?php print FormatEuroCurrency($ordTot)?></font></strong></td>
				<td align="center" bgcolor="#030133"><?php if($hasdeleted){ ?>
				    <?php } ?></td>
				<td bgcolor="#030133">				
				&nbsp;&nbsp;&nbsp;</td>
				<td bgcolor="#030133">&nbsp;</td>
				<td bgcolor="#030133">&nbsp;</td>
				<td align="center" bgcolor="#030133"><input type="hidden" name="maxitems" value="<?php print $rowcounter?>" /></td>
			  </tr>
			  </form>
			  <form method="post" action="/admin/dumporders.php" name="dumpform">
			  <?php if(@$_POST["powersearch"]=="1"){ ?>
			  <input type="hidden" name="powersearch" value="1" />
			  <input type="hidden" name="fromdate" value="<?php print trim(@$_POST["fromdate"])?>" />
			  <input type="hidden" name="todate" value="<?php print trim(@$_POST["todate"])?>" />
			  <input type="hidden" name="ordid" value="<?php print trim(str_replace('"','',str_replace("'",'',@$_POST["ordid"])))?>" />
			  <input type="hidden" name="origsearchtext" value="<?php print trim(str_replace('"','&quot;',@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="searchtext" value="<?php print trim(str_replace('"',"&quot;",@$_POST["searchtext"]))?>" />
			  <input type="hidden" name="ordstatus[]" value="<?php print $ordstatus?>" />
			  <input type="hidden" name="startwith" value="<?php if($usepowersearch) print "1"?>" />
			  <?php } ?>
			  <input type="hidden" name="sd" value="<?php print date($admindatestr, $sd)?>" />
			  <input type="hidden" name="ed" value="<?php print date($admindatestr, $ed)?>" />
			  <input type="hidden" name="details" value="false" />
			  <tr> 
                <td align="center"><?=$num_rows_order?> Orders</td>
				<td align="center"><?=$num_auth_order?> Authorized Orders</td>
				<td colspan="2" align="center"><input type="submit" value="<?php print $yyDmpOrd?>" onclick="document.dumpform.details.value='false';" /></td>
				<td colspan="2" align="center"><input type="submit" value="<?php print $yyDmpDet?>" onclick="document.dumpform.details.value='true';" /></td>
			  </tr>
			  </form>
<?php
	}else{
?>
			  <tr> 
                <td width="100%" colspan="6" align="center">
					<p><?php
					if(@$_POST["powersearch"]=="1")
						print $yyNoMat1;
					elseif($sd==$ed)
						print $yyNoMat2 . " " . date($admindatestr, $sd) . ".";
					else
						print $yyNoMat3 . " " . date($admindatestr, $sd) . " and " . date($admindatestr, $ed) . ".";
					?></p>				</td>
			  </tr>
			  <?php if($hasdeleted){ ?>
			  <tr> 
				<td colspan="6">&nbsp;</td>
			  </tr>
			  <?php } ?>
			  </form>
<?php
	} ?>
			  <tr> 
                <td width="100%" colspan="6" align="center">
				  <p><br />
					<a href="/admin/orderssz.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd)-1,date("d",$sd),date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed)-1,date("d",$ed),date("Y",$ed)))?>"><strong>- <?php print $yyMonth?></strong></a> | 
					<a href="/admin/orderssz.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)-7,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)-7,date("Y",$ed)))?>"><strong>- <?php print $yyWeek?></strong></a> | 
					<a href="/admin/orderssz.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)-1,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)-1,date("Y",$ed)))?>"><strong>- <?php print $yyDay?></strong></a> | 
					<a href="/admin/orderssz.php"><strong><?php print $yyToday?></strong></a> | 
					<a href="/admin/orderssz.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)+1,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)+1,date("Y",$ed)))?>"><strong><?php print $yyDay?> +</strong></a> | 
					<a href="/admin/orderssz.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd),date("d",$sd)+7,date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)+7,date("Y",$ed)))?>"><strong><?php print $yyWeek?> +</strong></a> | 
					<a href="/admin/orderssz.php?sd=<?php print date($admindatestr,mktime(0,0,0,date("m",$sd)+1,date("d",$sd),date("Y",$sd)))?>&ed=<?php print date($admindatestr,mktime(0,0,0,date("m",$ed),date("d",$ed)+1,date("Y",$ed)))?>"><strong><?php print $yyMonth?> +</strong></a>				  </p>				</td>
			  </tr>
			</table>
		  </td>
		</tr>
      </table>
	</div>
<?php
}
}

?>
<script language="JavaScript" type="text/javascript">
function dorecalc(onlytotal){
var thetotal=0,totoptdiff=0;
for(var i in document.forms.editform){
if(i.substr(0,5)=="quant"){
	theid = i.substr(5);
	totopts=0;
	delbutton = document.getElementById("del_"+theid);
	if(delbutton==null)
		isdeleted=false;
	else
		isdeleted=delbutton.checked;
	if(! isdeleted){
	for(var ii in document.forms.editform){
		var opttext="optn"+theid+"_";
		if(ii.substr(0,opttext.length)==opttext){
			theitem = document.getElementById(ii);
			if(document.getElementById('v'+ii)==null){
				thevalue = theitem[theitem.selectedIndex].value;
				if(thevalue.indexOf('|')>0){
					totopts += parseFloat(thevalue.substr(thevalue.indexOf('|')+1));
				}
			}
		}
	}
	thequant = parseInt(document.getElementById(i).value);
	if(isNaN(thequant)) thequant=0;
	theprice = parseFloat(document.getElementById("price"+theid).value);
	if(isNaN(theprice)) theprice=0;
	document.getElementById("optdiffspan"+theid).value=totopts;
	optdiff = parseFloat(document.getElementById("optdiffspan"+theid).value);
	if(isNaN(optdiff)) optdiff=0;
	thetotal += thequant * (theprice + optdiff);
	totoptdiff += thequant * optdiff;
	}
}
}
document.getElementById("optdiffspan").innerHTML=totoptdiff.toFixed(2);
//document.getElementById("ordtotal").value = thetotal.toFixed(2);
document.getElementById("ordTot").innerHTML = thetotal.toFixed(2);
document.getElementById("ordtotal").value = thetotal.toFixed(2);

if(onlytotal==true) return;
<? if(!empty($prcTot)){?>
thetotal+=<?=$prcTot?>;
<? } ?>
statetaxrate = parseFloat(document.getElementById("staterate").value);
if(isNaN(statetaxrate)) statetaxrate=0;
countrytaxrate = parseFloat(document.getElementById("countryrate").value);
if(isNaN(countrytaxrate)) countrytaxrate=0;
discount = parseFloat(document.getElementById("ordDiscount").value);
if(isNaN(discount)){
	discount=0;
	document.getElementById("ordDiscount").value=0;
}
statetaxtotal = (statetaxrate * (thetotal-discount)) / 100.0;
countrytaxtotal = (countrytaxrate * (thetotal-discount)) / 100.0;
shipping = parseFloat(document.getElementById("ordShipping").value);
if(isNaN(shipping)){
	//shipping=0;
	//document.getElementById("ordShipping").value=0;
}
handling = parseFloat(document.getElementById("ordHandling").value);
if(isNaN(handling)){
	handling=0;
	document.getElementById("ordHandling").value=0;
}
<?php	if(@$taxShipping==2){ ?>
statetaxtotal += (statetaxrate * shipping) / 100.0;
countrytaxtotal += (countrytaxrate * shipping) / 100.0;
<?php	}
		if(@$taxHandling==2){ ?>
statetaxtotal += (statetaxrate * handling) / 100.0;
countrytaxtotal += (countrytaxrate * handling) / 100.0;
<?php	} ?>
document.getElementById("ordStateTax").value = statetaxtotal.toFixed(2);
document.getElementById("ordCountryTax").value = countrytaxtotal.toFixed(2);
hstobj = document.getElementById("ordHSTTax");
hsttax=0;
if(! (hstobj==null)){
	hsttax = parseFloat(hstobj.value);
}
grandtotal = (thetotal + shipping + handling + statetaxtotal + countrytaxtotal + hsttax) - discount;


document.getElementById("grandtotalspan").innerHTML = grandtotal.toFixed(2);
}
</script>
