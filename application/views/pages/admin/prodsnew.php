<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$nprodoptions=0;
$nprodsections=0;
$nalloptions=0;
$nallsections=0;
$nalldropship=0;
$alreadygotadmin = getadminsettings();
$simpleOptions = (($adminTweaks & 2)==2);
$simpleSections = (($adminTweaks & 4)==4);
$dorefresh=FALSE;
if(@$maxprodsects=="") $maxprodsects=20;

if ($_POST['search']) {
	$xSearchFor = $_POST['searchfor'];
	$whereand = ' WHERE ';
	$sql = "SELECT p.*, s.sectionName, s.sectionWorkingName
			FROM multisections ms RIGHT JOIN products p ON p.pId = ms.pId LEFT OUTER JOIN sections s ON p.pSection = s.sectionID";
	if ($_POST['incategory'] != '') {
		$sectionids = getsectionids($_POST['incategory'], TRUE);
		if($sectionids != "") $sql .= $whereand . " (p.pSection IN (" . $sectionids . ") OR ms.pSection IN (" . $sectionids . ")) ";
		$whereand=' AND ';
	}
	if (!empty($_POST['price'])) {
		$sprice = $_POST['price'];
		if(strpos($sprice, '-') !== FALSE){
			$pricearr=split('-', $sprice);
			if(! is_numeric($pricearr[0])) $pricearr[0]=0;
			if(! is_numeric($pricearr[1])) $pricearr[1]=10000000;
			$sql .= $whereand . "pPrice BETWEEN " . $pricearr[0] . " AND " . $pricearr[1];
			$whereand=' AND ';
		}elseif(is_numeric($sprice)){
			$sql .= $whereand . "pPrice='" . mysql_real_escape_string($sprice) . "' ";
			$whereand=' AND ';
		}
	}
	if (!empty($_POST['searchfor'])) {
		$Xstext = mysql_real_escape_string($_POST['searchfor']);
		$aText = split(" ",$Xstext);
		$aFields[0]="p.pId";
		$aFields[1]=getlangid("pName",1);
		$aFields[2]=getlangid("pDescription",2);
		if($stype=="exact"){
			$sql .= $whereand . "(products.pId LIKE '%" . $Xstext . "%' OR ".getlangid("pName",1)." LIKE '%" . $Xstext . "%' OR ".getlangid("pDescription",2)." LIKE '%" . $Xstext . "%' OR ".getlangid("pLongDescription",4)." LIKE '%" . $Xstext . "%') ";
			$whereand=' AND ';
		}else{
			$sJoin="AND ";
			if($stype=="any") $sJoin="OR ";
			$sql .= $whereand . "(";
			$whereand=' AND ';
			for($index=0;$index<=2;$index++){
				$sql .= "(";
				$rowcounter=0;
				$arrelms=count($aText);
				foreach($aText as $theopt){
					if(is_array($theopt))$theopt=$theopt[0];
					$sql .= $aFields[$index] . " LIKE '%" . $theopt . "%' ";
					if(++$rowcounter < $arrelms) $sql .= $sJoin;
				}
				$sql .= ") ";
				if($index < 2) $sql .= "OR ";
			}
			$sql .= ") ";
		}
	}
	if(@$_REQUEST['stock']=='1') {
		$sql .= $whereand . '(pInStock<=0 AND pStockByOpts=0)';
	}
	$sql .= ' ORDER BY pName';
	
	//echo $sql;
	
	$res = mysql_query($sql) or print(mysql_error());
}
?>
<form id="mainform" name="mainform" method="post" action="/admin/prodsnew.php">
<table width="100%" cellpadding="5" cellspacing="0" border="1" bordercolor="#95A7CF" style="border-collapse: collapse;">
	<tr>
		<td bgcolor="#E6E9F5" align="right">Search For:</td>
		<td><input type="text" id="searchfor" name="searchfor" value="<?=$_POST['searchfor']?>" /></td>
		<td bgcolor="#E6E9F5" align="right">Price:</td>
		<td><input type="text" id="price" name="price" value="<?=$_POST['price']?>" /></td>
	</tr>
	<tr>
		<td bgcolor="#E6E9F5" align="right">Search Type:</td>
		<td>
			<select id="searchtype" name="searchtype">
				<option value="all">All words</option>
				<option value="any">Any word</option>
				<option value="exact">Exact phrase</option>
			</select>
		</td>
		<td bgcolor="#E6E9F5" align="right">In Category:</td>
		<td>
			<select id="incategory" name="incategory">
		<?php
		$thecat = @$_POST['incategory'];
		?>
				<option value=""<?=(empty($thecat)) ? 'selected="selected"' : '' ?>>All Categories</option>
		<?php
		if($thecat != '') $thecat = (int)$thecat;
		$sSQL = "SELECT sectionID,sectionWorkingName,topSection,rootSection FROM sections " . (@$adminonlysubcats==TRUE ? "WHERE rootSection=1 ORDER BY sectionWorkingName" : "ORDER BY sectionWorkingName");
		$allcats = mysql_query($sSQL) or print(mysql_error());
		
		$lasttsid = -1;
		while ($row = mysql_fetch_row($allcats)) {
			$allcatsa[$numcats++]=$row;
		}
		if ($numcats > 0) {
			for($index=0;$index<$numcats;$index++) {
				print '<option value="' . $allcatsa[$index][0] . '"';
				if($allcatsa[$index][0]==$thecat) print ' selected';
				print '>' . $allcatsa[$index][1] . "</option>\n";
			}
		}
		?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center" bgcolor="#E6E9F5">
			<input type="submit" id="search" name="search" value="Search" />
		</td>
	</tr>
</table>
</form>

<div>&nbsp;</div>

<?php
if ($res) {
?>
	<table cellpadding="3" cellspacing="0" border="0" width="100%">
		<tr>
			<td><strong>Product ID</strong></td>
			<td><strong>Product Name</strong></td>
			<td><strong>Sections</strong></td>
			<td><strong>Section Working Names</strong></td>
		</tr>
<?php
	if (mysql_num_rows($res) > 0) {
		// Build an array for section names
		$sql3 = "SELECT * FROM sections";
		$res3 = mysql_query($sql3) or print(mysql_error());
		$aSections = array();
		while ($row3 = mysql_fetch_assoc($res3)) {
			$aSections[$row3['sectionID']]['sectionName'] = $row3['sectionName'];
			$aSections[$row3['sectionID']]['sectionWorkingName'] = $row3['sectionWorkingName'];
		}
		
		$i = 0;
		while ($row = mysql_fetch_assoc($res)) {
			$sql2 = "SELECT p.pSection, ms.pSection AS mSection
					 FROM multisections ms
					 RIGHT JOIN products p ON p.pID = ms.pID
					 WHERE ms.pID = '" . $row['pID'] . "'";
			$res2 = mysql_query($sql2) or print(mysql_error());
	?>
		<tr<?=($i % 2 == 0) ? ' bgcolor="#E7EAEF"' : '' ?>>
			<td><?=$row['pID']?></td>
			<td><?=$row['pName']?></td>
			<td><?=$row['sectionName']?> - <em>(Root)</em></td>
			<td><?=$row['sectionWorkingName']?></td>
		</tr>
	<?php
			$j = 0;
			while ($row2 = mysql_fetch_assoc($res2)) {
	?>
		<tr<?=($i % 2 == 0) ? ' bgcolor="#E7EAEF"' : '' ?> style="">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><?=$aSections[$row2['mSection']]['sectionName']?></td>
			<td><?=$aSections[$row2['mSection']]['sectionWorkingName']?></td>
		</tr>
	<?php
				$j++;
			}
			$i++;
		}
		mysql_free_result($res);
	} else {
	?>
		<tr>
			<td colspan="4" bgcolor="#E7EAEF" style="text-align: center;"><strong>No products found.</strong></td>
		</tr>
	<?php
	}
	?>
	</table>
<?php
}
?>