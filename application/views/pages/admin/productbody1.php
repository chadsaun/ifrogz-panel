<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
$prodoptions="";
productdisplayscript(@$noproductoptions!=TRUE); ?>
		<table width="98%" align="center" border="0"  cellspacing="0" cellpadding="2" bgcolor="<?php print $innertablebg;?>">

<? if($iNumOfPages > 1 && @$pagebarattop==1){?>		  
			  <tr>
				<td colspan="3" align="center" class="pagenums"><p class="pagenums"><?php
					 print writepagebar($CurPage, $iNumOfPages) . "<br />"; ?>
				  </p></td>
			  </tr>
<? }?>
<?php
	
	while($rs = mysql_fetch_array($allprods)){
		if(trim($rs[getlangid("pLongDescription",4)])!="" || ! (trim($rs["pLargeImage"])=="" || is_null($rs["pLargeImage"]) || trim($rs["pLargeImage"])=="prodimages/")){
			if(($rs["pSell"] & 4)==4){
				$startlink='<a href="' . cleanforurl($rs["pName"]) . '.php' . (@$catid != "" && @$catid != "0" && $catid != $rs["pSection"] && @$nocatid != TRUE ? '?cat=' . $catid : "") . '">';
				$endlink="</a>";
			}elseif(@$detailslink != ""){
				$startlink=str_replace('%pid%', $rs["pId"], str_replace('%largeimage%', $rs["pLargeImage"], $detailslink));
				$endlink=@$detailsendlink;
			}else{
				$startlink='<a href="/proddetail.php?prod=' . urlencode($rs["pId"]) . (@$catid != "" && @$catid != "0" && $catid != $rs["pSection"] && @$nocatid != TRUE ? '&cat=' . $catid : "") . '">';
				$endlink="</a>";
			}
		}else{
			$startlink="";
			$endlink="";
		}
		for($cpnindex=0; $cpnindex < $adminProdsPerPage; $cpnindex++) $aDiscSection[$cpnindex][0] = "";
		if(! $isrootsection){
			$thetopts = $rs["pSection"];
			$gotdiscsection = FALSE;
			for($cpnindex=0; $cpnindex < $adminProdsPerPage; $cpnindex++){
				if($aDiscSection[$cpnindex][0]==$thetopts){
					$gotdiscsection = TRUE;
					break;
				}elseif($aDiscSection[$cpnindex][0]=="")
					break;
			}
			$aDiscSection[$cpnindex][0] = $thetopts;
			if(! $gotdiscsection){
				$topcpnids = $thetopts;
				for($index=0; $index<= 10; $index++){
					if($thetopts==0)
						break;
					else{
						$sSQL = "SELECT topSection FROM sections WHERE sectionID=" . $thetopts;
						$result2 = mysql_query($sSQL) or print(mysql_error());
						if(mysql_num_rows($result2) > 0){
							$rs2 = mysql_fetch_assoc($result2);
							$thetopts = $rs2["topSection"];
							$topcpnids .= "," . $thetopts;
						}else
							break;
					}
				}
				$aDiscSection[$cpnindex][1] = $topcpnids;
			}else
				$topcpnids = $aDiscSection[$cpnindex][1];
		}
		 
		 ?>
		 
              <tr> 
                <th colspan="2" align="left" >
<?php				if(@$showproductid==TRUE) print '<div class="prodid"><strong>' . $xxPrId . ':</strong> ' . $rs["pId"] . '</div>' ?>
<?php 				print $rs[getlangid("pName",1)] . $xxDot ?><? //if($alldiscounts != "") print ' <font color="#FF0000"><span class="discountsapply">' . $xxDsApp . '</span></font></div></strong><font size="1" color="#FF0000"><div class="proddiscounts">' . $alldiscounts . '</div></font>'; else print '</div></strong>' ?>                
				</th>
				<td width="15%" align="right"><?php
            		if($startlink != "")
                		print "<p>" . $startlink . "<strong>".$xxPrDets."</strong></a>&nbsp;</p>";
                	else
                		print "&nbsp;";
              ?></td>
			  </tr>
<?php	if(@$currencyseparator=="") $currencyseparator=" ";
		updatepricescript(@$noproductoptions!=TRUE); ?>
	<form method="post" name="tForm<?php print $Count; ?>" id="tForm<?php print $Count;?>" action="<?=$pathtossl?>/cart.php" onsubmit="return formvalidator<?php print $Count; ?>(this)">
			  <tr>
			    <td width="26%" align="center" valign="top" ><?php 
					$imgarr=explode(',',$rs["pImage"]);
					$smallcount=count($imgarr);
				
					if(trim($rs["pImage"])=="" || is_null($rs["pImage"]) || trim($rs["pImage"])=="prodimages/"){
					print '&nbsp;';
					  }else{
					print $startlink . '<img  src="' . $imgarr[0] . '" border="0" alt="' . str_replace('"','&quot;',strip_tags($rs[getlangid("pName",1)])) . '" />' . $endlink;
					  } 
				?>
				</td>
			    <td width="59%" valign="top">
			      <div class="proddescription"><?php print $rs[getlangid("pDescription",2)]; ?></div>
<?php
$optionshavestock=true;
if(is_array($prodoptions)){
	print '<div class="prodoptions"><table border="0" cellspacing="1" cellpadding="1" width="100%">';
	$rowcounter=0;
	foreach($prodoptions as $rowcounter => $theopt){
		$index=0;
		$gotSelection=FALSE;
		$cacheThis=! $useStockManagement;
		while($index < (maxprodopts-1) && ((int)($aOption[0][$index])!=0)){
			if($aOption[0][$index]==(int)($theopt["poOptionGroup"])){
				$gotSelection=TRUE;
				break;
			}
			$index++;
		}
		if(!$gotSelection){
			$aOption[2][$index]=false;
			$sSQL="SELECT optID,".getlangid("optName",32).",".getlangid("optGrpName",16)."," . $OWSP . "optPriceDiff,optType,optFlags,optStock,optPriceDiff AS optDims FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optDisplay='yes' AND optGroup=" . $theopt["poOptionGroup"] . " ORDER BY optID";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs2=mysql_fetch_array($result)){
				if(abs((int)$rs2["optType"])==3){
					$aOption[2][$index]=true;
					$fieldHeight = round(((double)($rs2["optDims"])-(int)($rs2["optDims"]))*100.0);
					$aOption[1][$index] = "<tr><td align='right' width='30%'><strong>" . $rs2[getlangid("optGrpName",16)] . ":</strong></td><td align='left'> <input type='hidden' name='optnPLACEHOLDER' value='" . $rs2["optID"] . "' />";
					if($fieldHeight != 1){
						$aOption[1][$index] .= "<textarea wrap='virtual' name='voptnPLACEHOLDER' cols='" . atb((int)($rs2["optDims"])) . "' rows='$fieldHeight'>";
						$aOption[1][$index] .= $rs2[getlangid("optName",32)] . "</textarea>";
					}else
						$aOption[1][$index] .= "<input maxlength='255' type='text' name='voptnPLACEHOLDER' size='" . atb($rs2["optDims"]) . "' value=\"" . str_replace('"',"&quot;",$rs2[getlangid("optName",32)]) . "\" />";
					$aOption[1][$index] .= "</td></tr>";
				}else{
					$aOption[1][$index] = "<tr><td align='right' width='30%'><strong>" . $rs2[getlangid("optGrpName",16)] . ':</strong></td><td align="left"> <select class="prodoption" onChange="updatepricePLACEHOLDER();" id="'.$Count.'optnPLACEHOLDER" name="optnPLACEHOLDER" size="1">';
					if((int)$rs2["optType"]>0) $aOption[1][$index] .= "<option value=''>".$xxPlsSel."</option>";
					do {
						$aOption[1][$index] .= '<option ';
						if($useStockManagement && (($rs["pSell"] & 2)==2) && $rs2["optStock"] <= 0) $aOption[1][$index] .= 'class="oostock" '; else $aOption[2][$index]=true;
						$aOption[1][$index] .= "value='" . $rs2["optID"] . "'>" . $rs2[getlangid("optName",32)];
						if(@$hideoptpricediffs != TRUE && (double)($rs2["optPriceDiff"]) != 0){
							$aOption[1][$index] .= " (";
							if((double)($rs2["optPriceDiff"]) > 0) $aOption[1][$index] .= "+";
							if(($rs2["optFlags"]&1)==1){
								$cacheThis=FALSE;
								$aOption[1][$index] .= FormatEuroCurrency(($rs["pPrice"]*$rs2["optPriceDiff"])/100.0) . ")";
							}else
								$aOption[1][$index] .= FormatEuroCurrency($rs2["optPriceDiff"]) . ")";
						}
						$aOption[1][$index] .= "</option>\n";
					} while($rs2=mysql_fetch_array($result));
					$aOption[1][$index] .= "</select></td></tr>";
				}
			}
			if($cacheThis) $aOption[0][$index] = (int)($theopt["poOptionGroup"]);
		}
		print str_replace("updatepricePLACEHOLDER", (($rs["pPrice"]==0 && @$pricezeromessage != "") || @$noprice==TRUE ?"dummyfunc":"updateprice" . $Count), str_replace("optnPLACEHOLDER","optn" . $rowcounter, $aOption[1][$index]));
		$optionshavestock = ($optionshavestock && $aOption[2][$index]);
		$rowcounter++;
	}
	print "</table></div>";
} else $rowcounter=0;
?>                </td>
			    <td width="100" align="center" valign="bottom" id="notify_<?=$rs["pId"]?>">
					<? if($showdiscountimage && $WSP==""){?><? } ?>
					
					<?php
					if(@$noprice==TRUE){
						print '&nbsp;';
					}else{
						if((double)$rs["pListPrice"]!=0.0) print '<div class="list_price"><strong>List Price:</strong> <strike>'.$rs["pListPrice"].'</strike>' . "</div>";
						if($rs["pPrice"]==0 && @$pricezeromessage != "")
							print '<div class="prodprice">' . $pricezeromessage . '</div>';
						else{
							if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs["pPricing_group"]);
							else $pPrice_adj=1;
							print '<span class="price" id="pricediv' . $Count . '" name="pricediv' . $Count . '">' . FormatEuroCurrency($rs["pPrice"]*$pPrice_adj) . '</span> ';
							if(@$showtaxinclusive && ($rs["pExemptions"] & 2)!=2) printf($ssIncTax,'<span id="pricedivti' . $Count . '" name="pricedivti' . $Count . '">' . FormatEuroCurrency($rs["pPrice"]*$pPrice_adj+($rs["pPrice"]*$pPrice_adj*$countryTax/100.0)) . '</span> ');
							//print "</div>";
							$extracurr = "";
							if($currRate1!=0 && $currSymbol1!="") $extracurr = str_replace("%s",number_format($rs["pPrice"]*$pPrice_adj*$currRate1,checkDPs($currSymbol1),".",","),$currFormat1) . $currencyseparator;
							if($currRate2!=0 && $currSymbol2!="") $extracurr .= str_replace("%s",number_format($rs["pPrice"]*$pPrice_adj*$currRate2,checkDPs($currSymbol2),".",","),$currFormat2) . $currencyseparator;
							if($currRate3!=0 && $currSymbol3!="") $extracurr .= str_replace("%s",number_format($rs["pPrice"]*$pPrice_adj*$currRate3,checkDPs($currSymbol3),".",","),$currFormat3) . "</strong>";
							if($extracurr!='') print '<div class="prodcurrency"><span class="extracurr" id="pricedivec' . $Count . '" name="pricedivec' . $Count . '">' . $extracurr . "</strong></span></div>";
						}
					} ?>
			      <?php
if(@$nobuyorcheckout == TRUE)
	print "&nbsp;";
else{
	if($useStockManagement)
		if(($rs["pSell"] & 2) == 2) $isInStock = $optionshavestock; else $isInStock = ((int)($rs["pInStock"]) > 0);
	else
		$isInStock = (((int)$rs["pSell"] & 1) != 0);
	if($isInStock){
?>
                  <input type="hidden" name="id" value="<?php print $rs["pId"]?>" />
                  <input type="hidden" name="mode" value="add" />
                  <input type="hidden" name="frompage" value="<?php print @$_SERVER['PHP_SELF'] . (trim(@$_SERVER['QUERY_STRING'])!= "" ? "?" : "") . @$_SERVER['QUERY_STRING']?>" />
                  <?php	if(@$showquantonproduct==TRUE) print '<input type="text" id="quant'.$Count.'" name="quant" size="2" maxlength="5" value="1" />';
				  $jcomma='';
				$joptlist='';
				for($k=0;$k<$rowcounter;$k++) {
					$joptlist.=$jcomma."$('".$Count."optn".$k."').value";
					$jcomma="+','+";
				}
				if($rowcounter==0) $joptlist="''";
				$strformvalidator='';
				if(!$setoptions) $strformvalidator="if(formvalidator".$Count."($('tForm".$Count."')))";
				  
				  ?>
			<div style="margin:4px;">
		<script language="JavaScript" type="text/javascript">
				document.write("<img align=\"bottom\" src=\"/lib/images/design/add_white.gif\" onclick=\"<?=$strformvalidator?> updateCart('<?=$rs["pId"]?>',<?=$joptlist?>,'',$('quant'+<?=$Count?>).value,'add','cart_added_<?=$rs["pId"]?>','')\" />");
			</script>
			<noscript>
			<? if(@$custombuybutton != "") print $custombuybutton; else print '<input align="bottom" type="image" src="/lib/images/design/add_white.gif" border="0" />';?>
			</noscript>
			</div>
<?			
	}else{
		print "<strong>".$xxOutStok."</strong>";
	}
}			  ?>
			<div style="position:relative;">
				<div id="cart_added_<?=$rs["pId"]?>" class="prod_added_body1" style="display:none;">
				<div id="cart_added_<?=$rs["pId"]?>_Working" style="display:none; position:absolute; right:140px; top:6px"><img src="/lib/images/indicator.gif" width="24" height="24" alt="" /></div>
				<div id="cart_added_<?=$rs["pId"]?>_holder">&nbsp;</div>
				</div>
			</div>			</td>
			  </tr>
			</form>
			  <tr>
				<td colspan="3" align="center">
				  <div style="border-bottom:1px solid #CCCCCC;margin:10px 0px;"></div>				</td>
			  </tr>
<?php
		$Count++;
	}
?>			  
<? if($iNumOfPages > 1 && @$nobottompagebar<>TRUE){?>
			  <tr>
				<td colspan="3" align="center" class="pagenums"><p class="pagenums"><?php
					 print writepagebar($CurPage, $iNumOfPages); ?><br />
				  <img src="/lib/images/misc/clearpixel.gif" width="300" height="1" alt="" /></p></td>
			  </tr>
<? }?>
			</table>
