<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
$prodoptions="";
productdisplayscript(@$noproductoptions!=TRUE); 
if(@$productcolumns=="") $productcolumns=1;?>		
<? if(!empty($thiscontent)) echo $thiscontent;?>
<div class="blue_header"><?= $title_header!="" ? $title_header : 'Choose a product:'?></div>


<div id="prod_body_2">
<?

			if($iNumOfPages > 1 && @$pagebarattop==1){
?>
			<div class="pagenums"><?php
				print writepagebar($CurPage, $iNumOfPages) . "<br />"; ?>
			</div>
<?php
			}
$totrows = mysql_num_rows($allprods);
if($totrows > 0) {
	while($rs = mysql_fetch_array($allprods)){
		$setoptions=false;
		if(!empty($rs["pSetOptions"])) {
			$setoptions=true;
			$setoptionsid=split(',',$rs["pSetOptions"]);
			$setcount=count($setoptionsid);
			$alloptionsDisplay=true;
			for($i=0;$i<$setcount;$i++) {
				$sql="SELECT optDisplay, optDisplay_point, optStock FROM options WHERE optID=".$setoptionsid[$i];
				$result5=mysql_query($sql);
				$row=mysql_fetch_assoc($result5);
				if($row['optDisplay']=='no' || ($row['optDisplay_point']>=$row['optStock'])) {$alloptionsDisplay=false;continue;}
			}
			if($alloptionsDisplay==false) continue;
		}
		if(trim($rs[getlangid("pLongDescription",4)])!="" || ! (trim($rs["pLargeImage"])=="" || is_null($rs["pLargeImage"]) || trim($rs["pLargeImage"])=="prodimages/")){
			if(($rs["pSell"] & 4)==4 && $rs["pURL"] != ""){
				$startlink='<a href="' . $rs["pURL"] . (@$catid != "" && @$catid != "0" && $catid != $rs["pSection"] && @$nocatid != TRUE ? '?cat=' . $catid : "") . '">';
				$endlink="</a>";
			}
			elseif(($rs["pSell"] & 4)==4){
				$startlink='<a href="' . cleanforurl($rs["pName"]) . '.php' . (@$catid != "" && @$catid != "0" && $catid != $rs["pSection"] && @$nocatid != TRUE ? '?cat=' . $catid : "") . '">';
				$endlink="</a>";
			}elseif(@$detailslink != ""){
				$startlink=str_replace('%pid%', $rs["pId"], str_replace('%largeimage%', $rs["pLargeImage"], $detailslink));
				$endlink=@$detailsendlink;
			}else{
				$startlink='<a href="/proddetail.php?prod=' . urlencode($rs["pId"]) . (@$catid != "" && @$catid != "0" && $catid != $rs["pSection"] && @$nocatid != TRUE ? '&cat=' . $catid : "") . '">';
				$endlink="</a>";
			}
		}else{
			$startlink="";
			$endlink="";
		}
		for($cpnindex=0; $cpnindex < $adminProdsPerPage; $cpnindex++) $aDiscSection[$cpnindex][0] = "";
		if(! $isrootsection){
			$thetopts = $rs["pSection"];
			$gotdiscsection = FALSE;
			for($cpnindex=0; $cpnindex < $adminProdsPerPage; $cpnindex++){
				if($aDiscSection[$cpnindex][0]==$thetopts){
					$gotdiscsection = TRUE;
					break;
				}elseif($aDiscSection[$cpnindex][0]=="")
					break;
			}
			$aDiscSection[$cpnindex][0] = $thetopts;
			if(! $gotdiscsection){
				$topcpnids = $thetopts;
				for($index=0; $index<= 10; $index++){
					if($thetopts==0)
						break;
					else{
						$sSQL = "SELECT topSection FROM sections WHERE sectionID=" . $thetopts;
						$result2 = mysql_query($sSQL) or print(mysql_error());
						if(mysql_num_rows($result2) > 0){
							$rs2 = mysql_fetch_assoc($result2);
							$thetopts = $rs2["topSection"];
							$topcpnids .= "," . $thetopts;
						}else
							break;
					}
				}
				$aDiscSection[$cpnindex][1] = $topcpnids;
			}else
				$topcpnids = $aDiscSection[$cpnindex][1];
		}
		$alldiscounts = "";
		$alldiscountsamounts="";
		if(@$noshowdiscounts != TRUE){
			$sSQL = "SELECT DISTINCT ".getlangid("cpnName",1024).", cpnDiscount,cpnType FROM coupons LEFT OUTER JOIN cpnassign ON coupons.cpnID=cpnassign.cpaCpnID WHERE (cpnSitewide=0 OR cpnSitewide=3) AND cpnNumAvail>0 AND cpnEndDate >= '" . date("Y-m-d H:i:s",time()) ."' AND cpnBeginDate <= '" . date("Y-m-d H:i:s",time()) ."' AND cpnIsCoupon=0 AND ((cpaType=2 AND cpaAssignment='" . $rs["pId"] . "')";
			if(! $isrootsection) $sSQL .= " OR (cpaType=1 AND cpaAssignment IN ('" . str_replace(",","','",$topcpnids) . "') AND NOT cpaAssignment IN ('" . str_replace(",","','",$topsectionids) . "'))";
			$sSQL .= ") ORDER BY cpnID";
			$result2 = mysql_query($sSQL) or print(mysql_error());
			while($rs2=mysql_fetch_assoc($result2)){
				$alldiscounts .= $rs2['cpnName'] . "<br />";
				if($rs2['cpnType']==2)$alldiscountsamounts .= $rs2['cpnDiscount'] . "% OFF";
			}
			mysql_free_result($result2);
		}
	 ?>
				<div id="notify_<?=$rs["pId"]?>" class="prod_layout">
					<div class="right_corner"></div>
				 <?php if($alldiscountsamounts != "") print ' <div style="position:absolute; right:20px; top:10px; font-size:16px; color:#FF0000; font-weight:bold;  ">'. $alldiscountsamounts . "</div>" ?>
				 <?php if(@$showproductid==TRUE) print '<div class="prodid">' . $xxPrId . ": " . $rs["pId"] . "</div>"; ?>
						<div class="prodimage">
						<table width="100%" height="140" cellspacing="0" cellpadding="0" >
						  <tr>
							<td valign="middle" align="center">

						<?php 
							$imgarr=explode(',',$rs["pImage"]);
							$smallcount=count($imgarr);
						
							if(trim($rs["pImage"])=="" || is_null($rs["pImage"]) || trim($rs["pImage"])=="prodimages/"){
							print '&nbsp;';
							  }else{
							print $startlink . '<img src="' . $imgarr[0] . '" valign="middle" border="0" alt="' . str_replace('"','&quot;',strip_tags($rs[getlangid("pName",1)])) . '" />' . $endlink;
							  } 
						?>
							</td>
						  </tr>
						</table>
						</div>
						<div class="prodname"><?php print $startlink . $rs[getlangid("pName",1)] . $endlink . $xxDot;?></div>

<?php	if(@$currencyseparator=="") $currencyseparator=" ";
		updatepricescript(@$noproductoptions!=TRUE);
		$thedesc = trim($rs[getlangid("pDescription",2)]); ?>

	<!--<form method="post" name="tForm<?php print $Count;?>" id="tForm<?php print $Count;?>" action="<?=$pathtossl?>/cart.php" onsubmit="return formvalidator<?php print $Count;?>(this)">
<?php	if($thedesc!="") print '<div class="proddescription">' . $thedesc . '</div>'; else print '<br />';


$optionshavestock=true;
	if(is_array($prodoptions)){
		print '<div class="prodoptions"><table border="0" cellspacing="2" cellpadding="1" width="100%" class="prodoptions">';
		$rowcounter=0;
		$this_prodid=$rs["pId"];
		$this_cat='';
		if(strstr($this_prodid,'AHX')) $this_cat='23';
		elseif(strstr($this_prodid,'BJZ')) $this_cat='18';
		elseif(strstr($this_prodid,'CCJW')) $this_cat='48';
		else $this_cat='8';			
		$str_customize='?cat='.$this_cat;
		$optid_list='';
		$optcomma='';
		foreach($prodoptions as $rowcounter => $theopt){
			$index=0;
			$gotSelection=FALSE;
			$cacheThis=! $useStockManagement;
			while($index < (maxprodopts-1) && ((int)($aOption[0][$index])!=0)){
				if($aOption[0][$index]==(int)($theopt["poOptionGroup"])){
					$gotSelection=TRUE;
					break;
				}
				$index++;
			}
			if(!$gotSelection){				
				$aOption[2][$index]=false;
				$sSQL="SELECT optID,".getlangid("optName",32).",".getlangid("optGrpName",16).",optGrpWorkingName," . $OWSP . "optPriceDiff,optType,optFlags,optStock,optPriceDiff,optColor,optStyleID,optGroup,optPriceDiff AS optDims FROM options LEFT JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE optDisplay='yes' AND optGroup=" . $theopt["poOptionGroup"] . " ORDER BY optID";
				$result = mysql_query($sSQL) or print(mysql_error());

				if($rs2=mysql_fetch_array($result)){										
					if(abs((int)$rs2["optType"])==3){
						$aOption[2][$index]=true;
						$fieldHeight = round(((double)($rs2["optDims"])-(int)($rs2["optDims"]))*100.0);
						$aOption[1][$index] = "<tr><td align='right' width='30%'><strong>" . $rs2[getlangid("optGrpName",16)] . ":</strong></td><td align='left'> <input type='hidden' name='optnPLACEHOLDER' value='" . $rs2["optID"] . "' />";
						if($fieldHeight != 1){
							$aOption[1][$index] .= "<textarea wrap='virtual' name='voptnPLACEHOLDER' cols='" . atb((int)($rs2["optDims"])) . "' rows='$fieldHeight'>";
							$aOption[1][$index] .= $rs2[getlangid("optName",32)] . "</textarea>";
						}else
							$aOption[1][$index] .= "<input maxlength='255' type='text' name='voptnPLACEHOLDER' size='" . atb($rs2["optDims"]) . "' value=\"" . str_replace('"',"&quot;",$rs2[getlangid("optName",32)]) . "\" />";
						$aOption[1][$index] .= "</td></tr>";
					}else{						
						if($setoptions){
							$aOption[1][$index] = "<tr><td align='right' nowrap='nowrap' width='35%'><strong>" . $rs2[getlangid("optGrpName",16)] . ':</strong></td><td align="left">';
							$aOption[1][$index] .= "<input id=\"".$Count."optnPLACEHOLDER\" name=\"optnPLACEHOLDER\" type=\"hidden\" value='" . $setoptionsid[$rowcounter] . "'>";
							do {								
								if($useStockManagement && (($rs["pSell"] & 2)==2) && $rs2["optStock"]+1000 <= 0) $aOption[1][$index] .= ''; else $aOption[2][$index]=true;
								if($rs2["optID"]==$setoptionsid[$rowcounter]) {
									$optid_list.=$optcomma.$rs2["optID"];
									$optcomma=',';									
									$aOption[1][$index] .= $rs2[getlangid("optName",32)];
									$str_type=explode(' ',strtolower($rs2["optGrpName"]));									
									if($str_type[0]=='screen') {
										$sql_getprod="SELECT poProdID FROM prodoptions po, products p WHERE p.pID LIKE '%-%' AND po.poProdID=p.pID AND poOptionGroup=".$rs2['optGroup'];
										$result_getprod=mysql_query($sql_getprod);
										$row_getprod=mysql_fetch_assoc($result_getprod);
										$str_customize.='&'.$str_type[0]."_overlay=".$rs2["optID"].'_'.$row_getprod['poProdID'];
									} else {
										$str_color_style='0x'.$rs2["optColor"];
										$str_customize.='&'.$str_type[0]."_overlay=".$rs2["optID"];
									}
								}
								if(@$hideoptpricediffs != TRUE && (double)($rs2["optPriceDiff"]) != 0){
									$aOption[1][$index] .= " (";
									if((double)($rs2["optPriceDiff"]) > 0) $aOption[1][$index] .= "+";
									if(($rs2["optFlags"]&1)==1){
										$cacheThis=FALSE;
										$aOption[1][$index] .= FormatEuroCurrency(($rs["pPrice"]*$rs2["optPriceDiff"])/100.0) . ")";
									}else
										$aOption[1][$index] .= FormatEuroCurrency($rs2["optPriceDiff"]) . ")";
								}
							} while($rs2=mysql_fetch_array($result));
							$aOption[1][$index] .= "</td></tr>";
						} else {
							$aOption[1][$index] = "<tr><td align='right' nowrap='nowrap' width='35%'><strong>" . $rs2[getlangid("optGrpName",16)] . ':</strong></td><td align="left"><select class="prodoption" onChange="updatepricePLACEHOLDER();" id="'.$Count.'optnPLACEHOLDER" name="optnPLACEHOLDER" size="1">';
							if((int)$rs2["optType"]>0) $aOption[1][$index] .= "<option value=''>".$xxPlsSel."</option>";
							do {
								$aOption[1][$index] .= '<option ';
								if($useStockManagement && (($rs["pSell"] & 2)==2) && $rs2["optStock"] <= 0) $aOption[1][$index] .= 'class="oostock" '; else $aOption[2][$index]=true;
								$aOption[1][$index] .= "value='" . $rs2["optID"] . "'>" . $rs2[getlangid("optName",32)];
								if(@$hideoptpricediffs != TRUE && (double)($rs2["optPriceDiff"]) != 0){
									$aOption[1][$index] .= " (";
									if((double)($rs2["optPriceDiff"]) > 0) $aOption[1][$index] .= "+";
									if(($rs2["optFlags"]&1)==1){
										$cacheThis=FALSE;
										$aOption[1][$index] .= FormatEuroCurrency(($rs["pPrice"]*$rs2["optPriceDiff"])/100.0) . ")";
									}else
										$aOption[1][$index] .= FormatEuroCurrency($rs2["optPriceDiff"]) . ")";
								}
								$aOption[1][$index] .= "</option>\n";
							} while($rs2=mysql_fetch_array($result));
							$aOption[1][$index] .= "</select></td></tr>";
						}
					}
				}
				if($cacheThis) $aOption[0][$index] = (int)($theopt["poOptionGroup"]);
			}
			print str_replace("updatepricePLACEHOLDER", (($rs["pPrice"]==0 && @$pricezeromessage != "") || @$noprice==TRUE ?"dummyfunc":"updateprice" . $Count), str_replace("optnPLACEHOLDER","optn" . $rowcounter, $aOption[1][$index]));
			$optionshavestock = ($optionshavestock && $aOption[2][$index]);
			$rowcounter++;
		}
		print "</table></div>";
	} else $rowcounter=0;
					if(@$noprice!=TRUE){
						if((double)$rs["pListPrice"]!=0.0) print str_replace("%s", FormatEuroCurrency($rs["pListPrice"]), $xxListPrice) . "<br />";
						if($rs["pPrice"]==0 && @$pricezeromessage != "")
							print '<div class="prodprice">' . $pricezeromessage . '</div>';
						else{
							if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs["pPricing_group"]);
							else $pPrice_adj=1;
							print '<div class="prodprice">' . $xxPrice . ': <span class="price" id="pricediv' . $Count . '" name="pricediv' . $Count . '">' . FormatEuroCurrency($rs["pPrice"]*$pPrice_adj) . '</span> ';
							if(@$showtaxinclusive && ($rs["pExemptions"] & 2)!=2) printf($ssIncTax,'<span id="pricedivti' . $Count . '" name="pricedivti' . $Count . '">' . FormatEuroCurrency($rs["pPrice"]*$pPrice_adj+($rs["pPrice"]*$pPrice_adj*$countryTax/100.0)) . '</span> ');
							print "</div>";
							$extracurr = "";
							if($currRate1!=0 && $currSymbol1!="") $extracurr = str_replace("%s",number_format($rs["pPrice"]*$pPrice_adj*$currRate1,checkDPs($currSymbol1),".",","),$currFormat1) . $currencyseparator;
							if($currRate2!=0 && $currSymbol2!="") $extracurr .= str_replace("%s",number_format($rs["pPrice"]*$pPrice_adj*$currRate2,checkDPs($currSymbol2),".",","),$currFormat2) . $currencyseparator;
							if($currRate3!=0 && $currSymbol3!="") $extracurr .= str_replace("%s",number_format($rs["pPrice"]*$pPrice_adj*$currRate3,checkDPs($currSymbol3),".",","),$currFormat3) . "</strong>";
							if($extracurr!='') print '<div class="prodcurrency"><span class="extracurr" id="pricedivec' . $Count . '" name="pricedivec' . $Count . '">' . $extracurr . "</strong></span></div>";
						}
					} ?>
					  
					<?php
	if(@$nobuyorcheckout != TRUE){
		if($useStockManagement)
			if(($rs["pSell"] & 2) == 2) $isInStock = $optionshavestock; else $isInStock = ((int)($rs["pInStock"]) > 0);
		else
			$isInStock = (((int)$rs["pSell"] & 1) != 0);
		if($isInStock){
	?>
	<input type="hidden" name="id" value="<?php print $rs["pId"];?>" />
	<input type="hidden" name="mode" value="add" />
	<input type="hidden" name="frompage" value="<?php print @$_SERVER['PHP_SELF'] . (trim(@$_SERVER['QUERY_STRING'])!= "" ? "?" : "") . @$_SERVER['QUERY_STRING']?>" />
	<?php	if(@$showquantonproduct==TRUE) print '<div style="margin-bottom:6px;"><strong>Quantity: </strong><input type="text" id="quant'.$Count.'" name="quant" size="2" maxlength="5" value="1" /></div>';
				$jcomma='';
				$joptlist='';
				for($k=0;$k<$rowcounter;$k++) {
					$joptlist.=$jcomma."$('".$Count."optn".$k."').value";
					$jcomma="+','+";
				}
				if($rowcounter==0) $joptlist="''";
				$strformvalidator='';
				if(!$setoptions) $strformvalidator="if(formvalidator".$Count."($('tForm".$Count."')))";
			?>
			<script language="JavaScript" type="text/javascript">
				document.write("<img src=\"/lib/images/addtocart2.gif\" onclick=\"<?=$strformvalidator?> updateCart('<?=$rs["pId"]?>',<?=$joptlist?>,'',$('quant'+<?=$Count?>).value,'add','notify_<?=$rs["pId"]?>')\" />");
			</script>
			<noscript>
			<? if(@$custombuybutton != "") print $custombuybutton; else print '<input align="middle" type="image" src="/lib/images/addtocart2.gif" border="0" />';?>
			</noscript>
			<? 
			if($setoptions) print '<br /><img src="/lib/images/new_images/orange_arrow.jpg" alt="" border="0" /> <a href="products.php'.$str_customize.'">Customize This Design</a>';
		}else{
			print "<strong>".$xxOutStok."</strong>";
		}
	}  ?>
					</form>-->
				  </div>
				<?php
		$Count++;
	} 
	
} else { ?>
				<div>There are no products that meet your filter criteria!</div>
<? }
			if($iNumOfPages > 1 && @$nobottompagebar<>TRUE) {
?>			  <div class="pagenums"><?php print writepagebar($CurPage, $iNumOfPages); ?></div>				
		<? } ?>
	</div>			