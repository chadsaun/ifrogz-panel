<?php

$numberOfOrders['today']		= 0;
$numberOfOrders['yesterday']	= 0;

$revenue['today']				= 0;
$revenue['yesterday']			= 0;

$averageOrderValue['today']		= 0;
$averageOrderValue['yesterday']	= 0;

$yesterday	= strtotime("yesterday");
$today		= mktime();
$timeNow = date("H:i:s");
$timeNowDisplay = date("g:i a");

// Calculate number of orders
$sql = "SELECT COUNT(o.ordID) AS totOrders
		FROM orders o
		WHERE o.ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $yesterday)."' AND '".date("Y-m-d", $yesterday)." $timeNow'";
$res = mysql_query($sql);
$row = mysql_fetch_assoc($res);
$numberOfOrders['yesterday'] = $row['totOrders'];

$sql = "SELECT COUNT(o.ordID) AS totOrders
		FROM orders o
		WHERE o.ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $today)."' AND '".date("Y-m-d", $today)." $timeNow'";
$res = mysql_query($sql);
$row = mysql_fetch_assoc($res);
$numberOfOrders['today'] = $row['totOrders'];

// Calculate revenue
$sql = "SELECT SUM(o.ordTotal - o.ordDiscount) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $yesterday)."' AND '".date("Y-m-d", $yesterday)." $timeNow'";
$res = mysql_query($sql);
$row = mysql_fetch_assoc($res);
$revenue['yesterday'] = $row['totRevenue'];

$sql = "SELECT SUM(o.ordTotal - o.ordDiscount) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $today)."' AND '".date("Y-m-d", $today)." $timeNow'";
$res = mysql_query($sql);
$row = mysql_fetch_assoc($res);
$revenue['today'] = $row['totRevenue'];

// Calculate average order value
$sql = "SELECT AVG(o.ordTotal - o.ordDiscount) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $yesterday)."' AND '".date("Y-m-d", $yesterday)." $timeNow'";
$res = mysql_query($sql);
$row = mysql_fetch_assoc($res);
$averageOrderValue['yesterday'] = $row['totRevenue'];

$sql = "SELECT AVG(o.ordTotal - o.ordDiscount) AS totRevenue
		FROM orders o
		WHERE ordStatus >= 3
		AND o.ordDate BETWEEN '".date("Y-m-d 00:00:00", $today)."' AND '".date("Y-m-d", $today)." $timeNow'";
$res = mysql_query($sql);
$row = mysql_fetch_assoc($res);
$averageOrderValue['today'] = $row['totRevenue'];



?>
<style type="text/css">
<!--
.label_header {font-size: 180%; font-weight: bold; color: #333333;}
.label {font-weight: bold; color: #666666;}
.yesterday_value {font-size: 120%; font-weight: bold; color: #333333;}
.today_value {font-size: 120%; font-weight: bold; color: #333333;}
.negative_comparison {color: #990000; font-weight: bold;}
.positive_comparison {color: #078C00; font-weight: bold;}
-->
</style>

		<h2>Real-time Stats</h2>
		
		<table width="600" border="0" cellspacing="0" cellpadding="10" align="center">
			<tr>
				<td align="center">
					<table border="0" cellspacing="0" cellpadding="3" align="center">
						<tr>
							<td align="center" colspan="3" style="border-bottom: 1px solid #666666;">
								<span class="label_header">Number of Orders</span>
							</td>
						</tr>
						<?php
							if ($numberOfOrders['yesterday'] != 0) {	
								$percentage = sprintf("%.2f", (($numberOfOrders['today'] - $numberOfOrders['yesterday']) / $numberOfOrders['yesterday']) * 100);
							}
							if ($percentage < 0) {
								$class = 'negative_comparison';
							} else {
								$class = 'positive_comparison';
							}
						?>
						<tr>
							<td align="right" valign="middle">
								<div><span class="label">Yesterday&nbsp;@&nbsp;<?=str_replace(" ", "&nbsp;", $timeNowDisplay)?>:</span></div>
							</td>
							<td align="right" valign="middle">
								<span class="yesterday_value"><?=$numberOfOrders['yesterday']?></span>							</td>
							<td rowspan="2" align="left" valign="middle"><span class="<?=$class?>"><?=sprintf("(%+.2f%%)", $percentage)?></span></td>
						</tr>
						<tr valign="middle">
							<td align="right">
								<div><span class="label">Today&nbsp;@&nbsp;<?=str_replace(" ", "&nbsp;", $timeNowDisplay)?>:</span></div>
							</td>
							<td align="right">
								<div><span class="today_value"><?=$numberOfOrders['today']?></span></div>
							</td>
						</tr>
					</table>
				</td>
				<td align="center">
					<table border="0" cellspacing="0" cellpadding="3" align="center">
						<tr>
							<td align="center" colspan="3" style="border-bottom: 1px solid #666666;">
								<span class="label_header">Revenue</span>
							</td>
						</tr>
						<?php
							if ($revenue['yesterday'] != 0) {	
								$percentage = sprintf("%.2f", (($revenue['today'] - $revenue['yesterday']) / $revenue['yesterday']) * 100);
							}
							if ($percentage < 0) {
								$class = 'negative_comparison';
							} else {
								$class = 'positive_comparison';
							}
						?>
						<tr>
							<td align="right" valign="middle">
								<div><span class="label">Yesterday&nbsp;@&nbsp;<?=str_replace(" ", "&nbsp;", $timeNowDisplay)?>:</span></div>
							</td>
							<td align="right" valign="middle">
									<span class="yesterday_value">$<?=number_format($revenue['yesterday'], 2)?></span>							</td>
							<td rowspan="2" align="left" valign="middle"><span class="<?=$class?>"><?=sprintf("(%+.2f%%)", $percentage)?></span></td>
						</tr>
						<tr valign="middle">
							<td align="right">
								<div><span class="label">Today&nbsp;@&nbsp;<?=str_replace(" ", "&nbsp;", $timeNowDisplay)?>:</span></div>
							</td>
							<td align="right">
								<div><span class="today_value">$<?=number_format($revenue['today'], 2)?></span></div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="3" align="center">
						<tr>
							<td align="center" colspan="3" style="border-bottom: 1px solid #666666;">
								<span class="label_header">Average Order Value</span>
							</td>
						</tr>
						<?php
							if ($averageOrderValue['yesterday'] != 0) {
								$percentage = sprintf("%.2f", (($averageOrderValue['today'] - $averageOrderValue['yesterday']) / $averageOrderValue['yesterday']) * 100);
							}
							if ($percentage < 0) {
								$class = 'negative_comparison';
							} else {
								$class = 'positive_comparison';
							}
						?>
						<tr>
							<td align="right" valign="middle">
								<div><span class="label">Yesterday&nbsp;@&nbsp;<?=str_replace(" ", "&nbsp;", $timeNowDisplay)?>:</span></div>
							</td>
							<td align="right" valign="middle">
									<span class="yesterday_value">$<?=number_format($averageOrderValue['yesterday'], 2)?></span>							</td>
							<td rowspan="2" align="left" valign="middle"><span class="<?=$class?>"><?=sprintf("(%+.2f%%)", $percentage)?></span></td>
						</tr>
						<tr valign="middle">
							<td align="right">
								<div><span class="label">Today&nbsp;@&nbsp;<?=str_replace(" ", "&nbsp;", $timeNowDisplay)?>:</span></div>
							</td>
							<td align="right">
								<div><span class="today_value">$<?=number_format($averageOrderValue['today'], 2)?></span></div>
							</td>
						</tr>
					</table>
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
