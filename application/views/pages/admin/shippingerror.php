<?php
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
?>
<h2 style="text-align: center">iFrogz Shipping</h2>

<h3 style="text-align: center">Are you sure you want to send these <span style="text-decoration: underline"><?=$_SESSION['error_status']?></span>?</h3>

<table width="300" align="center" cellpadding="2" cellspacing="0" style="border: 1px solid #345A7F">
	<tr style="background-color: #345A7F; color: #FFF">
		<th>Order ID</th>
		<th>Selected Shipping Method</th>
	</tr>
<?php
	$i = 0;
	foreach($_SESSION['error_order'] as $order) {
?>
	<tr style="background-color: <?=($i%2==0)?'#FFF':'#E6E9F5'?>">
		<td style="text-align: center"><?=$order['ordID']?></td>
		<td style="text-align: center"><?=$order['shipType']?></td>
	</tr>
<?php
		$i++;
	}
?>
</table>

<form id="frmMain" name="frmMain" method="post" action="/admin/shippingerrorprocess.php">
<table align="center" width="300" border="0" cellspacing="0" cellpadding="0" style="margin-top: 10px">
	<tr>
		<td style="text-align: center"><input type="submit" id="yes" name="yes" value="Yes" /></td>
		<td style="text-align: center"><input type="submit" id="no" name="no" value="No" /></td>
	</tr>
</table>
</form>

<p>&nbsp;</p>
