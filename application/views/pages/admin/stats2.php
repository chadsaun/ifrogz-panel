<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$admindatestr="Y-m-d";
if(@$admindateformat=="") $admindateformat=0;
if($admindateformat==1)
	$admindatestr="m/d/Y";
elseif($admindateformat==2)
	$admindatestr="d/m/Y";
$alreadygotadmin = getadminsettings();
$fromdate = trim(@$_POST["fromdate"]);
$todate = trim(@$_POST["todate"]);
$addcomma = "";
$ordstatus = "";
if(is_array(@$_POST["ordstatus"])){
	foreach($_POST["ordstatus"] as $objValue){
		if(is_array($objValue))$objValue=$objValue[0];
		$ordstatus .= $addcomma . $objValue;
		$addcomma = ",";
	}
}else
	$ordstatus = trim((string)@$_POST["ordstatus"]);
if($fromdate != ""){
	if(is_numeric($fromdate))
		$thefromdate = time()-($fromdate*60*60*24);
	else
		$thefromdate = parsedate($fromdate);
	if($todate=="")
		$thetodate = $thefromdate;
	elseif(is_numeric($todate))
		$thetodate = time()-($todate*60*60*24);
	else
		$thetodate = parsedate($todate);
	if($thefromdate > $thetodate){
		$tmpdate = $thetodate;
		$thetodate = $thefromdate;
		$thefromdate = $tmpdate;
	}
}else{
	$thefromdate = time()-(24);
	$thetodate = time();
}
$sSQL_order = "SELECT COUNT(DISTINCT ordID) AS order_count FROM cart c LEFT JOIN orders o ON o.ordID = c.cartOrderID WHERE cartCompleted=1 AND ordStatus>=3 AND ordDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59'";
if($ordstatus != "" && strpos($ordstatus,"9999")===FALSE) $sSQL_order .= " AND ordStatus IN (" . $ordstatus . ")";
//echo $sSQL_order;
$result_order=mysql_query($sSQL_order);
$row_order=mysql_fetch_assoc($result_order);
?>
      
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
       
		<tr>
          <td width="100%" align="center">
			<input type="hidden" name="posted" value="1">
			<input type="hidden" name="act" value="domodify">
            <table width="600" border="0" cellspacing="0" cellpadding="3" bgcolor="">

			  <tr> 
                <td width="80%" align="center"><h2 style="text-align: center">iFrogz Report <strong>from <?php print date($admindatestr, $thefromdate)?> to <?php print date($admindatestr, $thetodate)?> <? if(!empty($ordstatus)) echo ' and Order Status = '.$ordstatus;?> </strong></h2></td>
			    <td width="20%" align="right"><strong>(<?=$row_order['order_count']?> Orders</strong>)</td>
			  </tr>		 
			  
<?php
$sSQL = "SELECT SUM(cartQuantity) AS thecount,coOptGroup,coCartOption,coOptID FROM cart c LEFT JOIN cartoptions co ON c.cartID = co.coCartID LEFT JOIN orders o ON o.ordID = c.cartOrderID WHERE cartCompleted=1 AND ordStatus>=3 AND ordDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59' ";
if($ordstatus != "" && strpos($ordstatus,"9999")===FALSE) $sSQL .= " AND ordStatus IN (" . $ordstatus . ")";
$sSQL .= "GROUP BY coOptGroup,coCartOption,coOptID ORDER BY thecount DESC";
$result = mysql_query($sSQL) or print(mysql_error());
if(mysql_num_rows($result) > 0){
	$i=0;
	while($rs = mysql_fetch_assoc($result)){
		$sql="SELECT * FROM prodoptions po, options o, products p WHERE po.poOptionGroup=o.optGroup AND po.poProdID=p.pId AND optID='".$rs["coOptID"]."' ORDER BY pOrder";
		$result2 = mysql_query($sql) or print(mysql_error());
		$rs2 = mysql_fetch_assoc($result2);
		if(!strstr($rs2["poProdID"],'-')) $prod_style=$rs2["poProdID"] . '-' . $rs2["optStyleID"];
		else $prod_style=$rs2["poProdID"] . $rs2["optStyleID"];
		$report[$i]['prod_id']=$rs2["poProdID"];		
		$report[$i]['count']=$rs["thecount"];
		$report[$i]['product_style']=$prod_style;		
		$report[$i]['option']=$rs["coCartOption"];
		$report[$i]['pantone']=$rs2["optPantone"];
	$i++;
	}
}
function multi_sort($array, $akey)
{ 
  function compare($a, $b)
  {
     global $key;
     return strcmp($a[$key], $b[$key]);
  }
  usort($array, "compare");
  return $array;
}
function arrayColumnSort()
  {
   $n = func_num_args();
   $ar = func_get_arg($n-1);
   if(!is_array($ar))
     return false;

   for($i = 0; $i < $n-1; $i++)
     $col[$i] = func_get_arg($i);

   foreach($ar as $key => $val)
     foreach($col as $kkey => $vval)
       if(is_string($vval))
         ${"subar$kkey"}[$key] = $val[$vval];

   $arv = array();
   foreach($col as $key => $val)
     $arv[] = (is_string($val) ? ${"subar$key"} : $val);
   $arv[] = $ar;

   call_user_func_array("array_multisort", $arv);
   return $ar;
  }
if(!empty($report)) sort($report);
//$report = multi_sort($report, $key = 'product_style');
//$report = multi_sort($report, $key = 'count');
//if(!empty($report)) $report=arrayColumnSort("count", SORT_ASC, SORT_NUMERIC, "product_id", SORT_ASC, SORT_REGULAR, $report)
//array_multisort($report[0],SORT_ASC,SORT_NUMERIC);
//if(!empty($report)) sort($report);
?>		<tr>
			<td colspan="2" align="left">
			  <table width="100%"  border="0" cellspacing="0" cellpadding="2">
				  <tr>
					<th scope="col" width="25%">Quantity</th>
					<th scope="col" width="25%">Prod ID</th>
					<th scope="col" width="25%">Pantone</th>
					<th scope="col" width="25%">Option</th>
				  </tr>
				  <? for($i=0;$i<count($report);$i++) { ?>
				  <tr>
					<td style="border-top:1px solid #CCCCCC;"><?=$report[$i]['count']?></td>
					<td style="border-top:1px solid #CCCCCC;"><?=$report[$i]['product_style']?></td>
					<td style="border-top:1px solid #CCCCCC;"><?=$report[$i]['pantone']?></td>
					<td style="border-top:1px solid #CCCCCC;"><?=$report[$i]['option']?></td>
				  </tr>
				  <? } ?>
				</table>
			</td>
        </tr>
      </table>	  
