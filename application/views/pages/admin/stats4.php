<script language="JavaScript" type="text/javascript" src="/lib/js/util/popcalendar.js"></script>
<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();
$success=TRUE;
$noQuery=FALSE;
$themask='yyyy-mm-dd';
$admindatestr="Y-m-d";
if(@$admindateformat=="") $admindateformat=0;
if($admindateformat==1)
	$admindatestr="m/d/Y";
elseif($admindateformat==2)
	$admindatestr="d/m/Y";
$alreadygotadmin = getadminsettings();
$fromdate = trim(@$_POST["fromdate"]);
$todate = trim(@$_POST["todate"]);
if($fromdate != ""){
	if(is_numeric($fromdate))
		$thefromdate = time()-($fromdate*60*60*24);
	else
		$thefromdate = parsedate($fromdate);
	if($todate=="")
		$thetodate = $thefromdate;
	elseif(is_numeric($todate))
		$thetodate = time()-($todate*60*60*24);
	else
		$thetodate = parsedate($todate);
	if($thefromdate > $thetodate){
		$tmpdate = $thetodate;
		$thetodate = $thefromdate;
		$thefromdate = $tmpdate;
	}
}else{
	$noQuery=TRUE;
	$thefromdate = time()-(24);
	$thetodate = time();
}
if($ordIDFrom!='' && ordIDTo!='') $noQuery=FALSE; 
if(!$noQuery) {
	$sSQL_order = "SELECT COUNT(DISTINCT ordID) AS order_count FROM orders o,cart c WHERE o.ordID = c.cartOrderID AND cartCompleted=1"; 
	if(!empty($searchbydate)) $sSQL .= " AND ordDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59'";
	if(!empty($searchbyorderid) && $ordIDFrom!='' && ordIDTo!='') $sSQL .= " AND o.ordID BETWEEN ".$ordIDFrom." AND ".$ordIDTo;
	$result_order=mysql_query($sSQL_order);
	$row_order=mysql_fetch_assoc($result_order);
	
	if($searchby=='product_id' || empty($searchby)){
		$str_search='co.coOptGroup, co.coOptID';
		$cols='4';
	} else {
		$str_search='co.coOptGroup';
		$cols='3';
	}	
	$sSQL= "SELECT SUM( c.cartQuantity ) AS thecount, co.coCartOption,".$str_search." 
			FROM orders o, cart c, cartoptions co
			WHERE o.ordID = c.cartOrderID
			AND c.cartID = co.coCartID
			AND c.cartCompleted =1";
	if(!empty($searchbydate)) $sSQL .= " AND o.ordDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59'"; 
	if(!empty($searchbyorderid) && $ordIDFrom!='' && ordIDTo!='') $sSQL .= " AND o.ordID BETWEEN ".$ordIDFrom." AND ".$ordIDTo;
	$sSQL .= " GROUP BY co.coCartOption,".$str_search." ORDER BY co.coOptGroup,thecount DESC,c.cartProdName";
	//echo $sSQL;
	$result = mysql_query($sSQL) or print(mysql_error());
	if(mysql_num_rows($result) > 0){
		$i=0;
		while($rs = mysql_fetch_assoc($result)){
			$sql="SELECT * FROM prodoptions po, options o, products p WHERE po.poOptionGroup=o.optGroup AND po.poProdID=p.pId AND optID='".$rs["coOptID"]."' ORDER BY pOrder";
			$result2 = mysql_query($sql) or print(mysql_error());
			$rs2 = mysql_fetch_assoc($result2);
			$report[$i]['count']=$rs["thecount"];
			if(!strstr($rs2["poProdID"],'-')) $prod_style=$rs2["poProdID"] . '-' . $rs2["optStyleID"];
			else $prod_style=$rs2["poProdID"] . $rs2["optStyleID"];
			if($searchby=='product_id' || empty($searchby))	$report[$i]['product_style']=$prod_style;
			else $report[$i]['product_style']=$rs["coOptGroup"];			 
			$report[$i]['option']=$rs["coCartOption"];
			$report[$i]['pantone']=$rs2["optPantone"];
			$report[$i]['optID']=$rs2["optID"];
		$i++;
		}
	}
//if(!empty($report)) sort($report);
}
?>		
      
	  <table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="">
			  <form method="post" action="/admin/stats4.php" name="psearchform">
			  <input type="hidden" name="powersearch" value="1" />
			  <tr bgcolor="#030133"><td colspan="7"><strong><font color="#E7EAEF">&nbsp;<?php print $yyPowSea?></font></strong></td></tr>
			  <tr bgcolor="#E7EAEF">
			    <td align="right" width="11%"><strong>Search By:</strong> </td>
			    <td align="left" width="13%">
				<select name="searchby" id="searchby">
			      <option value="product_id" selected="selected" <?php if (!(strcmp("product_id", $searchby))) {echo "selected=\"selected\"";} ?>>Product ID</option>
			      <option value="opt_grp" <?php if (!(strcmp("opt_grp", $searchby))) {echo "selected=\"selected\"";} ?>>Option Group</option>
			    </select>
				</td> 
                <td align="right" width="12%"><strong><?php print $yyOrdFro?>:</strong></td>
				<td align="left" width="14%"><input type="text" size="14" name="fromdate" value="<?php print $fromdate?>" /> <input type=button onclick="popUpCalendar(this, document.forms.psearchform.fromdate, '<?php print $themask?>', 0)" value='DP' /></td>
				<td align="right" width="11%"><strong><?php print $yyOrdTil?>:</strong></td>
				<td align="left" width="20%"><input type="text" size="14" name="todate" value="<?php print $todate?>" /> <input type=button onclick="popUpCalendar(this, document.forms.psearchform.todate, '<?php print $themask?>', -205)" value='DP' /></td>
			    <td align="left" width="19%"><input name="searchbydate" type="submit" id="searchbydate" value="Search By Date" /></td>
			  </tr>
			  <tr bgcolor="#E7EAEF">
			    <td align="right">&nbsp;</td>
			    <td align="right">&nbsp;</td>
			    <td align="right"><strong>Order ID From:</strong></td>
			    <td align="left"><input name="ordIDFrom" type="text" id="ordIDFrom" size="14" /></td>
			    <td align="right"><strong>Order ID To:</strong></td>
			    <td align="left"><input name="ordIDTo" type="text" id="ordIDTo" size="14" /></td>
			    <td align="left"><input name="searchbyorderid" type="submit" id="searchbyorderid" value="Search By Order ID" /></td>
			    </tr>			  
			  </form>
			</table>
	  <table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="" align="center">
		<tr>
          <td width="100%" align="center">
			<input type="hidden" name="posted" value="1">
			<input type="hidden" name="act" value="domodify">
            <table width="100%" border="0" cellspacing="0" cellpadding="3" bgcolor="">
	<tr><td colspan="3" align="left">
	  <table width="96%"  border="0" cellspacing="1" cellpadding="2" align="center">
		  <tr>
		    <td colspan="<?=$cols?>"> <div style="font-size:14px; "><strong>Option Stats </strong></div></td>
		    <td><div align="right"><strong>(<? if(empty($row_order['order_count'])) echo '0'; else echo $row_order['order_count'];?>
  Orders)</strong></div></td>
		    </tr>
		  <tr bgcolor="#030133">
			<th  bgcolor="#030133" scope="col"><strong><font color="#E7EAEF">Option</font></strong></th>
			<th scope="col" ><strong><font color="#E7EAEF">Quantity</font></strong></th>
			<? if($searchby=='product_id' || empty($searchby)){?>
			<th scope="col" ><strong><font color="#E7EAEF">Prod ID</font></strong></th>
			<th scope="col" ><strong><font color="#E7EAEF">Opt ID</font></strong></th>
		    <th nowrap="nowrap" scope="col"><strong><font color="#E7EAEF">Pantone</font></strong></th>
		  	<? } else {?>
			<th scope="col" ><strong><font color="#E7EAEF">Option Group</font></strong></th>
			<? } ?>
		  </tr>
		  <? 
		  if(!$noQuery) {
		  	for($i=0;$i<count($report);$i++) { ?>
		  <tr style="background-color: #<?=($i%2==0)?'E7EAEF':'EAECEB'?>">
			<td><?=$report[$i]['option']?></td>
			<td><?=$report[$i]['count']?></td>			
			<td><?=$report[$i]['product_style']?></td>
			<? if($searchby=='product_id' || empty($searchby)){?>
			<td><?=$report[$i]['optID']?></td>
		    <td><?=$report[$i]['pantone']?></td>
			<? } ?>
		  </tr>
		  <? }
		  if($i==0) echo '<tr>
			<td colspan="4" align="center">There are no records returned for your search criteria.</td>
		  </tr>';
		  } else {
		  ?>
		  <tr>
			<td colspan="5" align="center">Select a date range.</td>
		  </tr>
		  <? } ?>
		</table>
	  <?  if(!$noQuery && $i>0) {?>
	  <!--<tr> 
		<td width="100%" colspan="4" align="center"><strong>Top Countries</strong><br />&nbsp;</td>
	  </tr>-->
	<?php
	//$sSQL = "SELECT COUNT(ordCountry) AS thecount,ordCountry FROM orders WHERE ordStatus>=3 AND ordDate BETWEEN '" . date("Y-m-d", $thefromdate) . "' AND '" . date("Y-m-d", $thetodate) . " 23:59:59' GROUP BY ordCountry ORDER BY thecount DESC LIMIT 100";
//	$result = mysql_query($sSQL) or print(mysql_error());
//	if(mysql_num_rows($result) > 0){
//		print '<tr><td align="left" colspan="4"><table border="0" cellspacing="0" cellpadding="0" width="96%" bgcolor="" align="center">';
//		print '<tr><td><strong>Country Name</strong></td><td align="center"><strong>Sales</strong></td></tr>';
//		while($rs = mysql_fetch_assoc($result)){
//			print '<tr><td>' . $rs["ordCountry"] . '</td><td align="center">' . $rs["thecount"] . '</td></tr>';
//		}
//		print '</table></td></tr>';
//	}
}?>
            </table>
		</td>
    </tr>
</table>
	  
	