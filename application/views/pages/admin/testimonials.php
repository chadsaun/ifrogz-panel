<?php
include('init.php');
include_once(DOCROOT.'includes/paginateit.class.php');
$PaginateIt = new PaginateIt();

if(!empty($offset)) $testimonials['offset']=$offset;
if($offset=='0') $testimonials['offset']=$offset;
if(!empty($row_count)) $testimonials['row_count']=$row_count;

function trim_text($text, $count)
{
	$text = str_replace("  ", " ", $text);
	$string = explode(" ", $text);
	for($wordCounter = 0; $wordCounter <= $count; $wordCounter++)
	{
		$trimed .= $string[$wordCounter];
		if ( $wordCounter < $count ){ $trimed .= " "; }
		else { $trimed .= "..."; }
	}
	$trimed = trim($trimed);
	return $trimed;
}
$style = '
table {border-collapse: collapse; border: 1px solid #000}
th {color: #FFF; background-color: #000066}
';
?>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
function check() {
	createIDs();

	var resp = confirm("Are you sure you want to batch process all the orders currently showing?");
	return resp;
}

function createIDs() {
	var aIDs = $A(document.getElementsByClassName('selItem'));
	var strIDs = '';
	var isComma = false;
	
	aIDs.each( function(obj,index) {
			if(isComma) {
				strIDs += ',';
			}
			strIDs += obj.value;
			isComma = true;
	});
	
	$('ids').value = strIDs;
}

function togAll(val) {
	var aBoxes = $A(document.getElementsByClassName('selItem'));
	
	aBoxes.each( function(obj,index) {
			obj.checked = val;
	});
}

function validate(frm) {
	var isBlank = true;
	var aBoxes = $A(document.getElementsByClassName('selItem'));
	
	aBoxes.each( function(obj,index) {
			if(obj.checked) {
				isBlank = false;
			}
	});
	
	if(isBlank) {
		alert("You must select at least one testimonial to update.");
		return false;
	}else{
		return true;
	}
}

function selBox(id) {
	$('box'+id).checked = true;
}
-->
</script>

	<div style="margin: 0 auto">
		<h1 style="text-align: center">Customer Testimonials</h1>
		
		<? if(!empty($message)){echo '<p align="center" style="font-weight: bold; color: green">'.$message.'</p>';} ?>
		<table border="0" cellspacing="2" cellpadding="0" align="center">
			<th colspan="5">View:</th>
			<tr>
				<td><form id="form1" method="post" action="/admin/testimonials.php">
					<input type="submit" name="submit" id="submit" value="All" />
					</form>
				</td>
				<td><form id="form2" method="post" action="/admin/testimonials.php">
					<input type="submit" name="submit" id="submit" value="Active" />
					<input type="hidden" name="field" id="field" value="status" />
					<input type="hidden" name="value" id="value" value="active" />
					</form>
				</td>
				<td><form id="form3" method="post" action="/admin/testimonials.php">
					<input type="submit" name="submit" id="submit" value="Inactive" />
					<input type="hidden" name="field" id="field" value="status" />
					<input type="hidden" name="value" id="value" value="inactive" />
					</form>
				</td>
				<td><form id="form4" method="post" action="/admin/testimonials.php">
					<input type="submit" name="submit" id="submit" value="Deleted" />
					<input type="hidden" name="field" id="field" value="status" />
					<input type="hidden" name="value" id="value" value="deleted" />
					</form>
				</td>
				<td><form id="form5" method="post" action="/admin/testimonials.php">
					<input type="submit" name="submit" id="submit" value="Featured" />
					<input type="hidden" name="field" id="field" value="featured" />
					<input type="hidden" name="value" id="value" value="yes" />
					</form>
				</td>
			</tr>
		</table>
		
		<p>&nbsp;</p>
			
		<?
		$testimonials['field']=$field;
		$testimonials['value']=$value;
		
		// Get total number of rows
		$qry = "SELECT * FROM testimonials";
		if(!empty($testimonials['field']) && !empty($testimonials['value']))
			$qry .= " WHERE ".$testimonials['field']." = '".$testimonials['value']."'";
		$rst = mysql_query($qry) or print(mysql_error().'<br />'.$qry);
		$num_rows=mysql_num_rows($rst);
		
		$PaginateIt->SetItemCount($num_rows);
		$PaginateIt->SetItemsPerPage(20);
		$PaginateIt->SetLinksToDisplay(10);
		
		// Query for page results
		$query = "SELECT * FROM testimonials";							
		if(!empty($testimonials['field']) && !empty($testimonials['value']))
			$query .= " WHERE ".$testimonials['field']." = '".$testimonials['value']."'";
		$query .= $PaginateIt->GetSqlLimit();;
		
		$result = mysql_query($query) or print(mysql_error().'<br />'.$qry);	
		
		?>
		<table width="100%" border="0" style="border-collapse: collapse">
		  <tr>
			<td align="center" height="25"><?=$PaginateIt->GetPageLinks()?></td>
		  </tr>
		</table>
		
		<form id="bp" name="bp" action="/admin/testimonialsprocess.php" method="post" onsubmit="return check();">
		<table width="100%" cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 1px solid #FFF">
			<tr style="background-color: #030133; color: #FFF">
				<th colspan="6" style="text-align: left">Batch Processing</th>
			</tr>
			<tr style="background-color: #E7EAEF">
				<td width="50"><strong>Status:</strong></td>
				<td width="75">
					<select id="allstatus" name="allstatus">
						<option value="active">active</option>
						<option value="inactive">inactive</option>
						<option value="deleted">deleted</option>
					</select>
				</td>
				<td>
					<input type="submit" id="statussubmit" name="statussubmit" value="Set All" />
				</td>
				<td width="60"><strong>Featured:</strong></td>
				<td width="50">
					<select id="allfeat" name="allfeat">
						<option value="no">no</option>
						<option value="yes">yes</option>
					</select>
				</td>
				<td>
					<input type="submit" id="featsubmit" name="featsubmit" value="Set All" />
					<input type="hidden" id="ids" name="ids" value="" />
					<input type="hidden" name="field" id="field" value="<?=$_REQUEST['field']?>" />
					<input type="hidden" name="value" id="value" value="<?=$_REQUEST['value']?>" />
				</td>
			</tr>
		</table>
		</form>
		
		<form id="mainform" name="mainform" method="post" action="/admin/testimonialsprocess.php" onsubmit="return validate(this);">
		<table id="results" width="100%" align="center" cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse; border: 1px solid #FFFFFF">
			<tr style="background-color: #030133; color: #FFFFFF">
				<th scope="col"><input type="checkbox" value="1" onclick="togAll(this.checked);" /></th>
				<th scope="col" width="100">First Name </th>
				<th scope="col">State</th>
				<th scope="col">Status</th>
				<th scope="col">Testimonial</th>
				<th scope="col">Email/Website</th>
				<th scope="col">Featured</th>
				<th scope="col" width="90">Date Posted </th>
			</tr>
	
		
		<?
		//echo $num_rows;
		if(mysql_num_rows($result) == 0)
		{
		?>
		<tr bgcolor="#E7EAEF">
			<td colspan="8" align="center" style="padding: 5px auto"><strong>None Found</strong></td>
		</tr>
		<?
		}
		
		$i=0;
		while($row=mysql_fetch_assoc($result))
		{
		?>
			<tr<?=($i%2!=0)?' bgcolor="#FFFFFF"':' bgcolor="#E7EAEF"'?>>
				<td><input type="checkbox" id="box<?=$row['id']?>" name="checked[]" value="<?=$row['id']?>" class="selItem" /></td>
				<td><?=$row['fname']?></td>
				<td style="text-align: center"><?=$row['state']?></td>
				<td><select name="status<?=$row['id']?>" onchange="selBox('<?=$row['id']?>');">
						<option value="inactive" <?=($row['status'] == 'inactive')?'selected="selected"':''?>>inactive</option>
						<option value="active" <?=($row['status'] == 'active')?'selected="selected"':''?>>active</option>
						<option value="deleted" <?=($row['status'] == 'deleted')?'selected="selected"':''?>>deleted</option>
					</select></td>
				<td><?
				if(str_word_count($row['testimonial']) > 35)
				{
					$testimony = trim_text($row['testimonial'], 35);
					$testimony .= ' <a href="/admin/testimonialspopup.php?id='.$row['id'].'&show=testimonial" onclick="window.open(this.href,\'testimony\',\'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=300, height=400, top=150, left=600\'); return false;">(all)</a>';
				}
				else
					$testimony = $row['testimonial'];
					
				echo $testimony;
				?></td>
				<td>
				<?	if(strlen($row['email']) > 15)
						$email = substr($row['email'],0,15).'...<a href="/admin/testimonialspopup.php?id='.$row['id'].'&show=email" onclick="window.open(this.href,\'testimony\',\'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=500, height=50, top=150, left=600\'); return false;">(all)</a>';
					else
						$email = $row['email'];
					echo '<a href ="mailto:'.$row['email'].'">'.$email.'</a>';	
					?><br />
				<?
				if(strlen($row['website']) > 15)
					$website = substr($row['website'],0,15).'...<a href="/admin/testimonialspopup.php?id='.$row['id'].'&show=website" onclick="window.open(this.href,\'testimony\',\'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=500, height=50, top=150, left=600\'); return false;">(all)</a>';
				else
					$website = $row['website'];
				echo $website;
					
					
					?></td>
				<td><select name="featured<?=$row['id']?>">
						<option value="no" <?=($row['featured'] == 'no')?'selected="selected"':''?>>no</option>
						<option value="yes" <?=($row['featured'] == 'yes')?'selected="selected"':''?>>yes</option>
					</select></td>
				<td style="text-align: center"><?=date("M j, Y",strtotime($row['date_posted']))?></td>
			</tr>
			
		<?
			$i++;
		}				
		?>
			<tr<?=($i%2!=0)?' bgcolor="#EAECEB"':' bgcolor="#E7EAEF"'?>>
				<td colspan="8" style="text-align: right; padding: 10px"><input type="submit" id="update" name="update" value="Update" /></td>
			</tr>
		</table>
		<input type="hidden" name="field" id="field" value="<?=$_REQUEST['field']?>" />
		<input type="hidden" name="value" id="value" value="<?=$_REQUEST['value']?>" />
		
		</form>
		
		<table width="100%" border="0">
		  <tr>
			<td align="center" height="25"><?=$PaginateIt->GetPageLinks()?></td>
		  </tr>
		</table>
	
		
	</div>
