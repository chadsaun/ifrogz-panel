<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/scriptaculous-js/1.5.1/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/popcalendar.js"></script>
<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
if(@$storesessionvalue=="") $storesessionvalue="virtualstore".time();
if($_SESSION["loggedon"] != $storesessionvalue || @$disallowlogin==TRUE) exit();

if(@$maxloginlevels=="") $maxloginlevels=5;

if($_POST['wsyear'] != "" && $_POST['wsmonth'] != ""){
	$themonth = $_POST['wsmonth'];
	$theyear = $_POST['wsyear'];
}else{	
	$themonth = date('m');
	$theyear = date('Y');
}

$sSQL = "SELECT DISTINCT pPricing_group
		FROM products";
$result=mysql_query($sSQL);
$i=0;
if(mysql_num_rows($result)>0){
	while($row=mysql_fetch_assoc($result)){
	$allpricinggroups[$i++]=$row['pPricing_group'];
	}	
}
$price_group_count=$i;
mysql_free_result($result);

$alldata=getWSNumOrders($theyear.'-'.$themonth);
//$return=getWSNumOrders($alldata[$i]['custID'],$theyear.'-'.$themonth);

//showarray($alldata);
//showarray($pstruct);

//showarray($allpricinggroups);
?>
<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="">
  <tr>
    <td width="100%" align="center"><span name="searchspan" id="searchspan">
      <table width="100%" border="0" cellspacing="1" cellpadding="1" bgcolor="">
        <form method="post" action="/admin/wsrebates.php" name="psearchform">
          
          <tr bgcolor="#030133">
            <td colspan="7"><strong><font color="#E7EAEF">&nbsp;Power WS Search</font></strong></td>
          </tr>
          <tr bgcolor="#E7EAEF">
            <td width="11%" align="right"><strong>Status:</strong></td>
            <td width="15%" align="left">
			
			<select name="wsmonth" id="wsmonth">
            <? for($i=1;$i<=12;$i++){ 
				if($i<10)$m='0'.$i;
				else $m=$i;
			?>
				<option value="<?=$m?>" <?php if($themonth==$m) {echo "SELECTED";} ?>><?= date('F',mktime(0, 0, 0, $m, 1, 1)); ?> (<?=$m?>)</option> 
			<? } ?>
			</select>			</td>
            <td width="11%" align="left"><div align="right"><strong>Year:</strong></div></td>
            <td width="14%" align="left"><div align="left">
			<select name="wsyear" id="wsyear">
            <? 
			$end_year=date('Y');
			for($i=2006;$i<=$end_year;$i++){ 
			?>
				<option value="<?=$i?>" <?php if($theyear==$i) {echo "SELECTED";} ?>><?=$i?></option> 
			<? } ?>
			</select>
            </div></td>
            <td width="9%" align="left">&nbsp;</td>
            <td width="17%" align="right">&nbsp;</td>
            <td width="23%" align="right"><!-- <strong>Products ID:</strong> -->
                <div align="center">
                  <input name="submit3" type="submit" id="submit33" value="<?php print $yySearch?>" />
                  &nbsp;
                  <!-- <input name="productid" type="text" id="productid2"> -->
                  &nbsp; </div></td>
          </tr>
          <tr bgcolor="#E7EAEF" class="noshow">
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="right">&nbsp;</td>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="left">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
        </form>
      </table>
      
      </span>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" >
          <form method="post" name="mainform" action="/admin/approvescreenz.php" onSubmit="return validateForm()">
            <input name="product_type" type="hidden" value="<?=$product_type?>">
            <input name="reporttype" type="hidden" value="<?=$reporttype?>">            
            <tr bgcolor="#030133">
              <td align="center"><strong><font color="#E7EAEF">Name</font></strong></td>
              <td align="center"><strong><font color="#E7EAEF">Login</font></strong></td>
			  <td align="center"><strong><font color="#E7EAEF">Pricing Group</font></strong></td>
			  <td align="center"><strong><font color="#E7EAEF">Next Tier</font></strong></td>
			  <td align="center"><strong><font color="#E7EAEF">Next Tier Discount</font></strong></td>			   
			   <td align="center"><strong><font color="#E7EAEF">Current Tier Level</font></strong></td>			                    
              <td align="center"><strong><font color="#E7EAEF">Current Discount</font></strong></td>
			   <td align="center"><strong><font color="#E7EAEF">Rebate Discount</font></strong></td>
			   <td align="center"><strong><font color="#E7EAEF">Amount</font></strong></td>
			  <td align="center" nowrap="nowrap"><strong><font color="#E7EAEF"># Products</font></strong></td>	
			  <td align="center"><strong><font color="#E7EAEF">Rebate</font></strong></td>
			  <td width="20" align="center"><strong><font color="#E7EAEF">&nbsp;</font></strong></td>              
            </tr>
            <?php
	if(is_array($alldata)){
		$rowcounter=0;
		$ordTot=0;
		//echo 'product_type='.$product_type;
		for($i=0;$i<count($alldata);$i++){			
			if($i%2==1) $bgcolor=""; else $bgcolor="#E7EAEF";				
			?>
            <tr bgcolor="<?php print $bgcolor?>">
              <td align="center" valign="middle"><?=$alldata[$i]['Name']?></td>
              <td align="center" valign="middle"><?=$alldata[$i]['Email']?></td>
              <td align="center">1</td>
			  <td align="center"><?=$alldata[$i]['pstruct'][0]['next_tier']?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][0]['next_tier_discount']?></td>		  
			  <td align="center"><?=$alldata[$i]['pstruct'][0]['tier_count']?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][0]['high_tier_discount']?>%</td>			  
			  <td align="center"><?=$alldata[$i]['pstruct'][0]['rebate_discount']?>%</td>
			  <td align="center"><?=money_format("$%.2n",$alldata[$i]['pstruct'][0]['amt'])?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][0]['num_prods']?></td>		  
			  <td align="center"><?=money_format("$%.2n",$alldata[$i]['pstruct'][0]['rebate'])?></td>
			  <td align="center">&nbsp;</td>
            </tr>
			<? 
			$pstruct_cnt=count($alldata[$i]['pstruct']);
			for($j=1;$j<$pstruct_cnt;$j++){?>
			<tr bgcolor="<?php print $bgcolor?>">
              <td align="center" valign="middle">&nbsp;</td>
              <td align="center" valign="middle">&nbsp;</td>
              <td align="center"><?=($j+1)?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][$j]['next_tier']?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][$j]['next_tier_discount']?></td>			  
			  <td align="center"><?=$alldata[$i]['pstruct'][$j]['tier_count']?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][$j]['high_tier_discount']?>%</td>
			  <td align="center"><?=$alldata[$i]['pstruct'][$j]['rebate_discount']?>%</td>
			   <td align="center"><?=money_format("$%.2n",$alldata[$i]['pstruct'][$j]['amt'])?></td>
			  <td align="center"><?=$alldata[$i]['pstruct'][$j]['num_prods']?></td>
			  <td align="center"><?=money_format("$%.2n",$alldata[$i]['pstruct'][$j]['rebate'])?></td>
			  <td align="center">&nbsp;</td>
            </tr>
			<? }?>
			<? if($alldata[$i]['rebate_total']>0) {?>
			<tr bgcolor="<?php print $bgcolor?>" > 
			  <td align="center" valign="middle" colspan="7">&nbsp;</td>
              <td align="center" style="border-top:solid #030133 1px;"><strong>Totals</strong></td>
			  <td align="center" style="border-top:solid #030133 1px;"><?=money_format("$%.2n",$alldata[$i]['amt_total'])?></td>
			  <td align="center" style="border-top:solid #030133 1px;"><?=$alldata[$i]['prod_total']?></td>			  
			  <td align="center" style="border-top:solid #030133 1px;"><?=money_format("$%.2n",$alldata[$i]['rebate_total'])?> </td>
			  <td align="right" style="border-top:solid #030133 1px;"> <input name="payrebate" type="button" value="Pay" /></td>
			</tr>
			<? }?>
			 <? 						
			$rowcounter++;			
		}
	}
	if($rowcounter==0) {
?>
            <tr bgcolor="<?php print $bgcolor?>">
              <td align="center" colspan="6">There are no record returnd from your search!</td>
            </tr>
            <?  } ?>            
          </form>
      </table>
    </td>
  </tr>
</table>
<?
function send($to,$from,$subject,$message,$toName='',$fromName='') {
	$customheaders = "MIME-Version: 1.0\n";
	$customheaders .= "From: $fromName <$from>\n";
	$customheaders .= "Reply-To: $from\n";
	$customheaders .= "Content-type: text/html; charset=iso-8859-1\n";
	
	mail($to,$subject,$message,$customheaders);
}

function getWSNumOrders1($thisDate,$custID=''){	
	$sSQL = "SELECT c.Name, c.Email, c.custID 
		FROM clientlogin cl, customers c
		WHERE cl.clientUser=c.Email
		AND cl.clientWholesaler>0";
	if($custID!='') $sSQL .=" AND c.custID=".$custID;
	$result=mysql_query($sSQL);
	$i=0;
	$num_rows=mysql_num_rows($result);
	if($num_rows>0){
		while($row=mysql_fetch_assoc($result)){			
			$alldata[$i]=$row;
			$custID=$row['custID'];
			$thecount='';
			$sql1="SELECT SUM(c.cartQuantity) as thecount, SUM(p.pWholesalePrice*c.cartQuantity) as theamt, p.pPricing_group FROM orders o, cart c, products p WHERE o.ordID=c.cartOrderID AND c.cartProdID=p.pID AND o.ordDate LIKE '" .$thisDate ."%' AND ordEID=".$custID." GROUP BY p.pPricing_group";
			$result1=mysql_query($sql1);
			if(mysql_num_rows($result1)>0){
				while($row1=mysql_fetch_assoc($result1)){ 
					$thecount[$row1['pPricing_group']]['cnt'] = $row1['thecount'];
					$thecount[$row1['pPricing_group']]['amt'] = $row1['theamt'];
				}	
			} else {
				$thecount[$row1['pPricing_group']]['cnt'] = '0';
				$thecount[$row1['pPricing_group']]['amt'] = '0';
			}
			$pstruct=getCustPricingStructures($custID);
			if(is_array($pstruct)) $struct_count=count($pstruct);			
			$rebate_total=0;
			$prod_total=0;
			$amt_total;
			for($j=0;$j<$struct_count;$j++){
				$num_tiers=0;
				$this_rebate=0;
				$this_num_prods=0;
				$high_tier_discount=0;
				if(is_array($pstruct[$j]['tiers'])) $struct_count2=count($pstruct[$j]['tiers']);
				$alldata[$i]['pstruct'][$j]['pricing_group']=$pstruct[$j]['pricing_group'];			
				for($k=0;$k<$struct_count2;$k++){
					if($thecount[$pstruct[$j]['pricing_group']]['cnt']+98>=$pstruct[$j]['tiers'][$k]['quantity_start']) {
						$alldata[$i]['pstruct'][$j]['tiers'][$k]['discount']=$pstruct[$j]['tiers'][$k]['discount'];
						$alldata[$i]['pstruct'][$j]['tiers'][$k]['quantity_start']=$pstruct[$j]['tiers'][$k]['quantity_start'];
						$alldata[$i]['pstruct'][$j]['tiers'][$k]['structID']=$pstruct[$j]['tiers'][$k]['pricing_struct_id'];
						$num_tiers++;				
						$rebate_discount=$pstruct[$j]['tiers'][$k]['discount']-$pstruct[$j]['tiers'][0]['discount'];
						$high_tier_discount=$pstruct[$j]['tiers'][$k]['discount'];
					}
				}
				$alldata[$i]['pstruct'][$j]['high_tier_discount']=$high_tier_discount;
				$alldata[$i]['pstruct'][$j]['tier_count']=$num_tiers;
				$alldata[$i]['pstruct'][$j]['rebate_discount']=$rebate_discount;
				$alldata[$i]['pstruct'][$j]['num_prods']=$thecount[$pstruct[$j]['pricing_group']]['cnt'];
				if(empty($alldata[$i]['pstruct'][$j]['num_prods'])) $alldata[$i]['pstruct'][$j]['num_prods']='0';
				$alldata[$i]['pstruct'][$j]['amt']=$thecount[$pstruct[$j]['pricing_group']]['amt'];
				if($num_tiers>1) $alldata[$i]['pstruct'][$j]['rebate']=($thecount[$pstruct[$j]['pricing_group']]['amt'])*(($rebate_discount)/100);
				else $alldata[$i]['pstruct'][$j]['rebate']=0;
				$rebate_total+=$alldata[$i]['pstruct'][$j]['rebate'];
				$prod_total+=$alldata[$i]['pstruct'][$j]['num_prods'];
				$amt_total+=$alldata[$i]['pstruct'][$j]['amt']; 
			}
			$alldata[$i]['rebate_total']+=$rebate_total;
			$alldata[$i]['prod_total']+=$prod_total;
			$alldata[$i]['amt_total']+=$amt_total;
			$i++;	
		}
	}
	return $alldata;
}
?>