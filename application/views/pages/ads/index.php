<script language="JavaScript" type="text/javascript" src="/lib/js/pages/ads/all.js"></script>
<!-- <script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-innerfade/08.02.14/jquery.js"></script> -->
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-innerfade/08.02.14/jquery.innerfade.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/datetimepicker.js"></script>
<style>
	.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
	.ui-timepicker-div dl{ text-align: left; }
	.ui-timepicker-div dl dt{ height: 25px; }
	.ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
	.ui-timepicker-div td { font-size: 90%; }
	.tableelement {background-color:#F7F7F7; border: #CCC dotted 1px; padding:5px; }
</style>
<? switch ($type) {
	case 'Button':
		$image_path = 'http://cloudfront.ifrogz.com/lib/images/homepagebuttons/';
	break;
	default:
		$image_path = 'http://cloudfront.ifrogz.com/lib/images/homepagebanners/';
	break;
} ?>
<div class="span-24 container">
	<div class="span-24" style="height:146px;">
		<? echo HTML::image('http://cdn.admin.ifrogz.com/lib/images/headers/hompageadmin.jpg', array('style'=>'height:146px;')); ?>
	</div>
	<div class="span-24" id="list" title="<? echo $type ?>">
		<hr class="space">
		<div class="push-1">
			<? echo HTML::anchor('/ads/', '< back', array('class'=>'procedures', 'style'=>'font-size:14px; margin:0 0 0 15px;')) ?>
		</div>
		<div class="push-1">
			<ul id="sortable">
			<? foreach ($ads as $ad) { ?>
				<div class="span-7" id="<? echo $ad->id ?>" style="cursor: move;">
					<table class="tableelement" id="table_<? echo $ad->id ?>">
						<thead>
							<tr>
								<td colspan="2">
									<span style="font:bold 14px Helvetica;">Display Order:&nbsp;<span id="order_<? echo $ad->id ?>"><? echo $ad->imgOrder ?></span></span>
								</td>
							</tr>
							<tr>
								<? if ($ad->begin_date < date("Y-m-d H:i:s") && $ad->end_date > date("Y-m-d H:i:s")) { ?>
								<td style="text-align:center;" colspan="2">
									<div style="height:130px;">
										<? echo HTML::image($image_path.$ad->image, array('style'=>'max-width:240px;')); ?>
									</div>
									<span style="font:normal 18px Helvetica; margin:20% 0 0 0;">Active</span>
								</td>
								<? } else { ?>
								<td style="text-align:center;" colspan="2">
									<div style="height:130px;">
										<? echo HTML::image($image_path.$ad->image, array('style'=>'max-width:240px; filter: alpha(opacity=50); -moz-opacity: 0.50; opacity: 0.50;')); ?>
									</div>
									<span style="font:normal 18px Helvetica; margin:20% 0 0 0; color:#ccc;">Not Active</span>
								</td>
								<? } ?>
							</tr>
							<tr>
								<td width="10">Image:</td>
								<td>
								<? if (strlen($ad->image) > 30) { ?>
									<span title="<? echo $ad->image ?>"><? echo substr($ad->image, 0, 30) . '...' ?></span>
								<? } else { ?>
									<span title="<? echo $ad->image ?>"><? echo $ad->image ?></span>
								<? } ?>
								</td>
							</tr>
							<tr>
								<td width="10">Link:</td>
								<td>
								<? if (strlen($ad->link) > 30) { ?>
									<span title="<? echo $ad->link ?>"><? echo substr($ad->link, 0, 30) . '...' ?></span>
								<? } else { ?>
									<span title="<? echo $ad->link ?>"><? echo $ad->link ?></span>
								<? } ?>
								</td>
							</tr>
							<tr>
								<td width="10">Start:</td>
								<td>
									<span><? echo $ad->begin_date ?></span>
								</td>
							</tr>
							<tr>
								<td width="10">End:</td>
								<td>
									<span><? echo $ad->end_date ?></span>
								</td>
							</tr>
							<tr >
								<td width="10"></td>
								<td height="45">
									<table style="border-width: 0px; border-style: solid; border-color: #FFFFFF;">
										<thead>
											<tr>
												<? if ($ad->isTest == 0) { ?>
													<td height="32" width="60">
														<? echo Form::checkbox('name', 'yes', FALSE, array('class'=>'staging', 'id'=>$ad->id)); ?><span>Staging</span>
													</td>
													<td height="32">
														<? echo Form::input('order', '', array('class'=>'span-1 testOrder orderbox_'.$ad->id, 'style'=>'height:18px; display:none;', 'id'=>$ad->id)); ?>
													</td>
												<? } else { ?>
													<td height="32" width="60">
														<? echo Form::checkbox('name', 'yes', TRUE, array('class'=>'staging', 'id'=>$ad->id)); ?><span>Staging</span>
													</td>
													<td height="32">
														<? echo Form::input('order', $ad->testOrder, array('class'=>'span-1 testOrder orderbox_'.$ad->id, 'style'=>'height:18px;', 'id'=>$ad->id)); ?>
													</td>
												<? } ?>
											</tr>
										</thead>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:left;">
									<a class="deletelink" style="cursor:pointer;"><? echo HTML::image('http://cdn.projects.ifrogz.com/lib/images/icons/delete.png') ?></a>
									<a id="<? echo $ad->id ?>" class="delete redbutton" style="cursor:pointer; display:none;">delete</a>
									<a class="cancel procedure" style="cursor:pointer; padding:0 0 0 5px; color:#444; display:none;">cancel</a>
									<a id="<? echo $ad->id ?>" class="edit procedure" style="cursor:pointer; padding:0 0 0 5px;">edit</a>
								</td>
							</tr>
						</thead>
					</table>
				</div>
			<? } ?>
				<? echo Form::hidden('last_id', $ad->imgOrder, array('id'=>'last_id')); ?>
				<div class="span-7" id="disable" style="padding:0 0 0 20;">
					<table class="tableelement">
						<thead>
							<tr height="380px">
								<td colspan="2" valign="middle" style="text-align:center;">
									<a id="addlink" style="font:normal 16px Helvetica; cursor:pointer;" class="procedure">Add a New Ad</a>
								</td>
							</tr>
						</thead>
					</table>
				</div>
			</ul>
		</div>
	</div>
	<div class="span-24" id="edit" style="display:none;"></div>
</div>