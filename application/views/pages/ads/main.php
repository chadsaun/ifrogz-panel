<script language="JavaScript" type="text/javascript" src="/lib/js/pages/ads/all.js"></script>
<!-- <script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-innerfade/08.02.14/jquery.js"></script> -->
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-innerfade/08.02.14/jquery.innerfade.js"></script>
<div class="span-24 container">
	<div class="span-24" style="height:146px;">
		<? echo HTML::image('http://cdn.admin.ifrogz.com/lib/images/headers/hompageadmin.jpg', array('style'=>'height:146px;')); ?>
	</div>
	<div class="push-2">
		<hr class="space">
		<? echo HTML::anchor('/ads/banners', 'Edit Banners', array('class'=>'button', 'style'=>'margin:0 15px 0 0;')) ?>
		<? echo HTML::anchor('/ads/buttons', 'Edit Buttons', array('class'=>'button', 'style'=>'margin:0 15px 0 0;')) ?>
		<a class="view_staging button" style="cursor:pointer; margin:0 15px 0 0;">Show Staging</a>
		<a class="view_live button" style="cursor:pointer; display:none; margin:0 15px 0 0;">Show Live</a>
		<? echo HTML::anchor('/home', '< back to admin', array('class'=>'procedures', 'style'=>'font-size:14px;')) ?>
		<hr class="space">
		<div id="live">
			<hr class="space">
			<div class="rotator_cuff" style="display: none;">
			<? foreach ($ads as $ad) {
				if (substr($ad['link'], 0, 1) == '/') {
					echo HTML::anchor('http://ifrogz.com'.$ad['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebanners/' . $ad['image'], array('style'=>'max-width:830px;')), array('target'=>'_blank'));
				} else {
					echo HTML::anchor($ad['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebanners/' . $ad['image']), array('target'=>'_blank'));
				}
			} ?>
			<? if (empty($ads)) {
				echo HTML::anchor('', HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebanners/default.jpg'));
			} ?>
			</div>
			<hr class="space">
			<div>		
				<? foreach ($button_ads as $button_ad) {
					echo '<div class="span-7">';
					if (substr($button_ad['link'], 0, 1) == '/') {
						echo HTML::anchor('http://ifrogz.com'.$button_ad['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebuttons/' . $button_ad['image']), array('target'=>'_blank'));
					} else {
						echo HTML::anchor($button_ad['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebuttons/' . $button_ad['image']), array('target'=>'_blank'));
					}		
					echo '</div>';
				} ?>
			</div>
			<hr class="space">
			<hr class="space">
		</div>
		<div id="staging" style="display:none; min-height:650px;">
			<hr class="space">
			<? if (empty($ads_staging)) { ?>
				<h2>No Banner ads to Display</h2>
			<? } ?>
			<div class="rotator_cuff" style="display: none;">
			<? foreach ($ads_staging as $ad_staging) {
				if (substr($ad_staging['link'], 0, 1) == '/') {
					echo HTML::anchor('http://ifrogz.com'.$ad_staging['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebanners/' . $ad_staging['image'], array('style'=>'max-width:830px;')), array('target'=>'_blank'));
				} else {
					echo HTML::anchor($ad_staging['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebanners/' . $ad_staging['image']), array('target'=>'_blank'));
				}
			} ?>
			</div>
			<hr class="space">
			<? if (empty($button_ads_staging)) { ?>
				<h2>No Button ads to Display</h2>
			<? } ?>
			<div>		
				<? foreach ($button_ads_staging as $button_ad_staging) {
					echo '<div class="span-7">';
					if (substr($button_ad['link'], 0, 1) == '/') {
						echo HTML::anchor('http://ifrogz.com'.$button_ad_staging['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebuttons/' . $button_ad_staging['image']), array('target'=>'_blank'));
					} else {
						echo HTML::anchor($button_ad_staging['link'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'homepagebuttons/' . $button_ad_staging['image']), array('target'=>'_blank'));
					}		
					echo '</div>';
				} ?>
			</div>
			<hr class="space">
			<hr class="space">
		</div>
	</div>
</div>