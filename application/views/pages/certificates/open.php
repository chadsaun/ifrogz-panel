<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#expiration').datepicker({
			showOn: 'button',
			buttonImage: '/lib/images/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd'
		});
	});
</script>

<?php echo Form::open('/certificates/open/'); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tbody>
		<tr>
			<td colspan="2" bgcolor="#030133" style="color: #E7EAEF; font-weight: bold">Power Search</td>
		</tr>
		<tr>
			<td bgcolor="#E7EAEF">
				<div align="right"><strong>Open as of:</strong>&nbsp;</div>
			</td>
			<td bgcolor="#E7EAEF">
				<?php echo Form::input('expiration', date('Y-m-d', strtotime($expiration)), array('id' => 'expiration')); ?>
			</td>
		</tr>
		<tr>
			<td bgcolor="#E7EAEF" colspan="2" align="right"><?php echo Form::submit('submit', 'Search', array('id' => 'submit')); ?></td>
		</tr>
	</tbody>
</table>
<?php echo Form::close(); ?>

<div style="margin-top: 10px;">&nbsp;</div>

<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1">
	<tr bgcolor="#030133" style="color: #E7EAEF; font-weight: bold">
		<th>Total Certificates</th>
		<th>Total Amount</th>
	</tr>
<?php if (!empty($data)): ?>
		<tr bgcolor="#E7EAEF">
			<td align="center"><?php echo $data[0]['total_certificates'] ?></td>
			<td align="center"><?php echo number_format($data[0]['total_amount']); ?></td>
		</tr>
<?php else: ?>	
		<tr bgcolor="#E7EAEF">
			<td align="center" colspan="2"><strong>No results found</strong></td>
		</tr>
<?php endif ?>
</table>