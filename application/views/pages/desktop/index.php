<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Select an image for your iFrogz desktop</title>
<style type="text/css">
body {
	background-color: #000;
	background-image:url('');
}
.text {
	font: 12px/20px Arial, Helvetica, sans-serif;
	color: #CCC;
}
a:link {
	color: #93B522;
	text-decoration: none;
}
a:visited {
	text-decoration: none;
	color: #93B522;
}
a:hover {
	text-decoration: underline;
	color: #FFF;
}
a:active {
	text-decoration: none;
	color: #CCC;
}
.border {
border-right: 1px solid #000;
margin-right: 5px;
padding-right: 4px;
}
.header {
	font: 30px Georgia, "Times New Roman", Times, serif;
	color: #CCC;
}
tbody tr:nth-child(even) td, tbody tr.even td {
background: #000;}

table {
border-color: black;
border-style: solid;
border-width: 1px;
}

</style>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="84" colspan="3" align="center" valign="middle" class="header">Select an image for your amazing desktop:</td>
        </tr>
      <tr>
        <td height="213" align="center" valign="middle"><img src="http://cdn.admin.ifrogz.com/lib/images/desktop/images/screen1.jpg" width="300" height="188" border="0" class="border" /></td>
        <td align="center" valign="middle"><img src="http://cdn.admin.ifrogz.com/lib/images/desktop/images/screen2.jpg" width="300" height="188" border="0" class="border" /></td>
        <td align="center" valign="middle"><img src="http://cdn.admin.ifrogz.com/lib/images/desktop/images/screen3.jpg" width="300" height="188" border="0" class="border" /></td>
      </tr>
      <tr>
        <td align="center" valign="middle" class="text"><p><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/2560x1440/screen1.jpg">2560 x 1400</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1920x1200/screen1.jpg">1920 x 1200</a> &nbsp;&nbsp;/&nbsp; &nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1680x1050/screen1.jpg">1680 x 1050<br />
        </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1600x1200/screen1.jpg">1600 x 1200</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1440x900/screen1.jpg">1440 x 900</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x1024/screen1.jpg">1280 x 1024<br />
        </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x800/screen1.jpg">1280 x 800</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1024x768/screen1.jpg">1024 x 768</a></p></td>
        <td align="center" valign="middle" class="text"><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/2560x1440/screen2.jpg">2560 x 1400</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1920x1200/screen2.jpg">1920 x 1200</a> &nbsp;&nbsp;/&nbsp; &nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1680x1050/screen2.jpg">1680 x 1050<br />
        </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1600x1200/screen2.jpg">1600 x 1200</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1440x900/screen2.jpg">1440 x 900</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x1024/screen2.jpg">1280 x 1024<br />
        </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x800/screen2.jpg">1280 x 800</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1024x768/screen2.jpg">1024 x 768</a></td>
        <td align="center" valign="middle" class="text"><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/2560x1440/screen3.jpg">2560 x 1400</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1920x1200/screen3.jpg">1920 x 1200</a> &nbsp;&nbsp;/&nbsp; &nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1680x1050/screen3.jpg">1680 x 1050<br />
        </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1600x1200/screen3.jpg">1600 x 1200</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1440x900/screen3.jpg">1440 x 900</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x1024/screen3.jpg">1280 x 1024<br />
        </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x800/screen3.jpg">1280 x 800</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1024x768/screen3.jpg">1024 x 768</a></td>
      </tr>
      <tr>
        <td height="218" align="center" valign="middle" class="text">&nbsp;</td>
        <td align="center" valign="middle"><p><img src="http://cdn.admin.ifrogz.com/lib/images/desktop/images/screen4.jpg" width="300" height="188" border="0" class="border" /></p></td>
        <td align="center" valign="middle" class="text">&nbsp;</td>
      </tr>
      <tr>
        <td align="center" valign="middle" class="text">&nbsp;</td>
        <td align="center" valign="middle" class="text"><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/2560x1440/screen4.jpg">2560 x 1400</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1920x1200/screen4.jpg">1920 x 1200</a> &nbsp;&nbsp;/&nbsp; &nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1680x1050/screen4.jpg">1680 x 1050<br />
          </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1600x1200/screen4.jpg">1600 x 1200</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1440x900/screen4.jpg">1440 x 900</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x1024/screen4.jpg">1280 x 1024<br />
          </a><a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1280x800/screen4.jpg">1280 x 800</a> &nbsp;&nbsp;/ &nbsp;&nbsp;<a href="http://cdn.admin.ifrogz.com/lib/images/desktop/1024x768/screen4.jpg">1024 x 768</a></td>
        <td align="center" valign="middle" class="text">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
