<script>
	$(document).ready(function() {
		$(function() {
			$( ".datepicker" ).datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'M dd, yy'
			});
		});
	});
</script>

<div class="span-24 container">
	<div class="span-24">
		<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/header.jpg'); ?>
	</div>
	<div class="push-1 span-23">
		<hr class="space">
		<span class="departmenttitle">ADD A FEATURE</span>
		<hr class="top">
	</div>
	<div class="push-1">
		<? echo Form::open('/features/save', array('enctype' => 'multipart/form-data')) ?>
		<table width='100%' border='0' cellspacing='1' cellpadding='2'>
			<tbody>
				<tr>
					<td>Name:</td>
					<td><? echo Form::input('name', '', array('class'=>'formtitle span-10')); ?></td>
				</tr>
				<tr>
					<td>Page Title:</td>
					<td><? echo Form::input('title', '', array('class'=>'formtitle span-10')); ?></td>
				</tr>
				<tr>
					<td>Page Body:</td>
					<td><? echo Form::textarea('body', '', array('class'=>'formvalues span-20')); ?></td>
				</tr>
				<tr>
					<td>Date:</td>
					<td><? echo Form::input('posted', '', array('class'=>'formtitle span-6 datepicker')); ?></td>
				</tr>
				<tr>
					<td>Website:</td>
					<td><? echo Form::input('website', '', array('class'=>'formtitle span-16')); ?></td>
				</tr>
				<tr>
					<td>Feature Type:</td>
					<td><? echo Form::select('type', array('1'=>'Entertainment', '2'=>'Athletics', '3'=>'Events/Goodwill'), '')?></td>
				</tr>
				<tr>
					<td><? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?>
					<span class="cancel">&nbsp;or&nbsp;</span>
					<? echo HTML::anchor('/features/', 'cancel'); ?></td>
				</tr>
			</tbody>
	    </table>
		<? echo Form::close(); ?>
	</div>
</div>