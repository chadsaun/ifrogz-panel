<script>
	$(document).ready(function() {
		$(function() {
			$( ".datepicker" ).datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'M dd, yy'
			});
		});
	});
</script>

<div class="span-24 container">
	<div class="span-24">
		<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/header.jpg'); ?>
	</div>
	<div class="push-1 span-23">
		<hr class="space">
		<span class="departmenttitle">EDIT: <? echo $feature['Name']?></span>
		<hr class="top">
	</div>
	<div class="push-1">
		<? echo Form::open('/features/save', array('enctype' => 'multipart/form-data')) ?>
		<table width='100%' border='0' cellspacing='1' cellpadding='2'>
			<tbody>
				<tr>
					<td>Name:</td>
					<td><? echo Form::input('name', $feature['Name'], array('class'=>'formtitle span-10')); ?></td>
				</tr>
				<tr>
					<td>Page Title:</td>
					<td><? echo Form::input('title', $feature['PageTitle'], array('class'=>'formtitle span-10')); ?></td>
				</tr>
				<tr>
					<td>Page Body:</td>
					<td><? echo Form::textarea('body', $feature['Body'], array('class'=>'formvalues span-20')); ?></td>
				</tr>
				<tr>
					<td>Date:</td>
					<td><? echo Form::input('posted', strftime("%b %d, %Y", strtotime($feature['DateCreated'])), array('class'=>'formtitle span-6 datepicker')); ?></td>
				</tr>
				<tr>
					<td>Website:</td>
					<td><? echo Form::input('website', $feature['Website'], array('class'=>'formtitle span-16')); ?></td>
				</tr>
				<? /* ?>
				<tr>
					<td>File:</td>
					<td><? echo Form::file('file'); ?></td>
				</tr>
				<? */ ?>
				<tr>
					<td>Feature Type:</td>
					<td><? echo Form::select('type', array('1'=>'Entertainment', '2'=>'Athletics', '3'=>'Events/Goodwill'), $feature['FeatureTypes_ID'])?></td>
				</tr>
				<tr>
					<td>Active:</td>
					<td>
						<? if ($feature['IsActive'] == 1) { ?>
							<? echo Form::checkbox('active', 'yes', TRUE) ?>
						<? } else { ?>
							<? echo Form::checkbox('active', 'yes', FALSE) ?>
						<? } ?>
					</td>
				</tr>
				<tr>
					<td>Sequence:</td>
					<td><? echo Form::input('sequence', $feature['Sequence'], array('class'=>'formtitle span-2')); ?></td>
				</tr>
				<? /* ?>
				<tr>
					<td>Approval:</td>
					<td><? echo $data[0]['employee'][0]['firstname'] ?> <? echo $data[0]['employee'][0]['lastname'] ?> on <? echo strftime("%b %d, %Y", strtotime($data[0]['DateModified'])) ?></td>
				</tr>
				<? */ ?>
				<tr>
					<td><? echo Form::hidden('id', $feature['ID'])?></td>
					<td><? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?>
					<span class="cancel">&nbsp;or&nbsp;</span>
					<? echo HTML::anchor('/features/view/' . $feature['URL'], 'cancel'); ?></td>
				</tr>
			</tbody>
	    </table>
		<? echo Form::close(); ?>
	</div>
</div>