<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#herospan').click(
		function() {
		$('#imageadd').toggle();
		$('#herospan').hide();
	});
});
</script>
<div class="span-24 container append-bottom">
	<div class="span-24">
		<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/header.jpg'); ?>
	</div>
	<div class="span-24">
		<hr class="space">
		<span class="departmenttitle"><? echo strtoupper($feature['Name']) ?> IMAGES</span><? echo HTML::anchor('features/view/' . $feature['URL'], '< back', array('class'=>'procedure')); ?>
		<hr class="top">
	</div>
	<div class="span-24">
		<div class="span-10">
			<a class="smallbutton" id="herospan">Add an Image</a>
			<div id="imageadd" style="display: none;">
				<? echo Form::open('/features/save_gallery', array('enctype' => 'multipart/form-data')); ?>
					<table width='100%' border='0' cellspacing='1' cellpadding='2'>
					<tbody>
						<tr>
							<td colspan="2">Thumbnail image size must be 62x62</td>
						</tr>
						<tr>
							<td>Thumbnail:</td>
							<td><? echo Form::file('file1'); ?></td>
						</tr>
						<tr>
							<td>Large:</td>
							<td><? echo Form::file('file2'); ?></td>
						</tr>
						<tr>
							<td><? echo Form::hidden('id', $feature['URL']);?></td>
							<td>
								<? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?>
								<span class="cancel">&nbsp;or&nbsp;</span>
								<? echo HTML::anchor('/features/image/' . $feature['URL'], 'cancel'); ?>
							</td>
						</tr>
					</tbody>
				    </table>
				<? echo Form::close(); ?>
			</div>
			<div class="span-24">
				<hr class="space">
				<? foreach ($image['large'] as $large) { ?>
					<div class="span-3 box" style="min-height:285px; height:auto !important;">
						<div style="min-height:275px; height:auto !important;">
							<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $feature['URL'] . '/large/' . $large['FileName'], array('width'=>'120px')); ?>
							<hr class="space">
							<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $feature['URL'] . '/thumb/' . $large['FileName']); ?>
							<hr class="space">
						</div>
						<? echo Form::open('/features/delete_image', array('enctype' => 'multipart/form-data')); ?>
						<? echo Form::hidden('id', $large['FileName']);?>
						<? echo Form::hidden('url', $feature['URL']);?>
						<? echo Form::submit('', 'delete', array('style'=>'font-size:12px;')); ?>
						<? echo Form::close(); ?>
					</div>
				<? } ?>		
			</div>
		</div>
	</div>
</div>

<? /*
<div id="slider" class="nivoSlider">
	<? echo	HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/artists/trent-hancock/gallery/large/trent_hancock_1.jpg', array('alt'=>'', 'style'=>'display: none;')) ?>
	<? echo	HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/artists/trent-hancock/gallery/large/trent_hancock_2.jpg', array('alt'=>'', 'style'=>'display: none;', 'rel'=>Kohana::$config->load('paths.s3_image_path') . 'features/artists/trent-hancock/gallery/thumb/trent_hancock_2.jpg')) ?>
	<? echo	HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/artists/trent-hancock/gallery/large/trent_hancock_3.jpg', array('alt'=>'', 'style'=>'display: none;')) ?>
	<? echo	HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/artists/trent-hancock/gallery/large/trent_hancock_4.jpg', array('alt'=>'', 'style'=>'display: none;')) ?>
	<? echo	HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/artists/trent-hancock/gallery/large/trent_hancock_5.jpg', array('alt'=>'', 'style'=>'display: none;')) ?>
</div>	<link rel="stylesheet" href="/lib/css/nivo-slider.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="/lib/css/nivo-slider-custom.css" type="text/css" media="screen" />
    <script language="JavaScript" type="text/javascript" charset="utf-8" src="http://assets.ifrogz.com/lib/packages/jquery/1.4.3/jquery.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-nivo-slider/2.6/jquery.nivo.slider.pack.js"></script>
	<script language="JavaScript" type="text/javascript">
	$(window).load(function() {
	    $('#slider3').nivoSlider({
		    effect:'fade', //Specify sets like: 'fold,fade,sliceDown'
	        animSpeed:500, //Slide transition speed
	        pauseTime:3000,
	        startSlide:0, //Set starting Slide (0 index)
	        directionNav:true, //Next & Prev
	        directionNavHide:true, //Only show on hover
	       // controlNavThumbs:false, //Use thumbnails for Control Nav
	        //controlNavThumbsFromRel:false, //Use image rel for thumbs
	        pauseOnHover:true, //Stop animation while hovering
	        manualAdvance:false, //Force manual transitions
	        captionOpacity:0.8, //Universal caption opacity
		});
		$('#slider').nivoSlider({
			effect:'fade',
			controlNavThumbs:true,
			controlNavThumbsFromRel:true
	    });    
	});
	</script>
*/ ?>