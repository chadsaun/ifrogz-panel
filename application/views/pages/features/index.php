<div class="span-24 container append-bottom">
	<div class="span-24">
		<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/header.jpg'); ?>
	</div>
	<div class="push-1 span-23">
		<hr class="space">
		<span class="departmenttitle">FEATURES </span><? echo HTML::anchor('/', '< back to admin', array('class'=>'procedure')); ?>
		<hr class="top">
		<? echo HTML::anchor('/features/add', 'add a feature', array('class'=>'smallbutton')); ?>
		<hr class="space">
	</div>
	<div class="push-2 span-21">
		<? if (! empty($inactives)) { ?>
			<span class="formtitle" style="color:#a0a0a0;">INACTIVE FEATURES</span>
			<hr>
		<? } ?>
		<? foreach ($inactives as $inactive) { ?>
			<div class="span-5" style="padding-bottom: 44px; height: 200px;">
				<? if (isset($inactive['image'][0]['FileName'])) { ?>
					<? echo HTML::anchor('/features/view/' . $inactive['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $inactive['URL'] . '/' . $inactive['image'][0]['FileName'])); ?><br />
					<? echo HTML::anchor('/features/view/' . $inactive['URL'], $inactive['Name']); ?>
				<? } else { ?>
					<? echo HTML::anchor('/features/view/' . $inactive['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/no-image.jpg')); ?><br />
					<? echo HTML::anchor('/features/view/' . $inactive['URL'], $inactive['Name']); ?>
				<? } ?>
			</div>
		<? } ?>
	</div>
	<div class="push-2 span-21">
		<span class="formtitle" style="color:#a0a0a0;">ACTIVE ARTISTS</span>
		<hr>
		<? foreach ($artists as $artist) { ?>
			<div class="span-5" style="padding-bottom: 44px; height: 200px;">
				<? if (isset($artist['image'][0]['FileName'])) { ?>
					<? echo HTML::anchor('/features/view/' . $artist['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $artist['URL'] . '/' . $artist['image'][0]['FileName'])); ?><br />
					<? echo HTML::anchor('/features/view/' . $artist['URL'], $artist['Name']); ?>
				<? } else { ?>
					<? echo HTML::anchor('/features/view/' . $artist['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/no-image.jpg')); ?><br />
					<? echo HTML::anchor('/features/view/' . $artist['URL'], $artist['Name']); ?>
				<? } ?>
			</div>
			<? } ?>
	</div>
	<div class="push-2 span-21">
		<span class="formtitle" style="color:#a0a0a0;">ACTIVE ATHLETES</span>
		<hr>
		<? foreach ($athletes as $athlete) { ?>
			<div class="span-5" style="padding-bottom: 44px; height: 200px;">
				<? if (isset($athlete['image'][0]['FileName'])) { ?>
					<? echo HTML::anchor('/features/view/' . $athlete['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $athlete['URL'] . '/' . $athlete['image'][0]['FileName'])); ?><br />
					<? echo HTML::anchor('/features/view/' . $athlete['URL'], $athlete['Name']); ?>
				<? } else { ?>
					<? echo HTML::anchor('/features/view/' . $athlete['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/no-image.jpg')); ?><br />
					<? echo HTML::anchor('/features/view/' . $athlete['URL'], $athlete['Name']); ?>
				<? } ?>
			</div>
			<? } ?>
	</div>
	<div class="push-2 span-21">
		<span class="formtitle" style="color:#a0a0a0;">ACTIVE EVENTS</span>
		<hr>
		<? foreach ($events as $event) { ?>
			<div class="span-5" style="padding-bottom: 44px; height: 200px;">
				<? if (isset($event['image'][0]['FileName'])) { ?>
					<? echo HTML::anchor('/features/view/' . $event['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $event['URL'] . '/' . $event['image'][0]['FileName'])); ?><br />
					<? echo HTML::anchor('/features/view/' . $event['URL'], $event['Name']); ?>
				<? } else { ?>
					<? echo HTML::anchor('/features/view/' . $event['URL'], HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/no-image.jpg')); ?><br />
					<? echo HTML::anchor('/features/view/' . $event['URL'], $event['Name']); ?>
				<? } ?>
			</div>
			<? } ?>
	</div>
</div>