<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#introspan').click(
		function() {
		$('#introimageadd').toggle();
		$('#introspan').hide();
	});
	$('#herospan').click(
		function() {
		$('#heroimageadd').toggle();
		$('#herospan').hide();
	});
	$('#audiospan').click(
		function() {
		$('#audioadd').toggle();
		$('#audiospan').hide();
	});
});
</script>
<div class="span-24 container append-bottom">
	<div class="span-24">
		<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/header.jpg'); ?>
	</div>
	<div class="span-24">
		<hr class="space">
		<span class="departmenttitle"><? echo strtoupper($feature['Name']) ?></span><? echo HTML::anchor('features/', '< back', array('class'=>'procedure')); ?>
		<hr class="top">
		<? echo HTML::anchor('/features/edit/' . $feature['URL'], 'edit ' . $feature['Name'], array('class'=>'smallbutton', 'style'=>'margin:0 15px 0 0;')); ?>
		<? echo HTML::anchor('/features/image/' . $feature['URL'], 'edit ' . $feature['Name'] . ' images', array('class'=>'smallbutton')); ?>
		<hr class="space">
	</div>
	<div class="span-24">
		<div class="span-18">
			<span class="formtitle"><span style="color:#a0a0a0; font-size:16px;">Page Header:</span><br /> <? echo $feature['PageTitle'] ?></span>
			<hr class="space">
			<span class="formtitle"><span style="color:#a0a0a0; font-size:16px;">Page Body:</span><br /> <? echo $feature['Body'] ?></span>
			<hr class="space">
			<? if (! empty($feature['Website'])) { ?>
				<span class="formtitle" style="color:#a0a0a0; font-size:16px;">Website:</span><br />
				<span class="formtitle"><? echo HTML::anchor($feature['Website'], $feature['Website'], array('target'=>'_blank')); ?></span>
			<? } ?>
		</div>
		<div class="push-1 span-5">	
			<span class="formtitle" style="color:#a0a0a0; font-size:16px;">Intro Image:</span><br />
			<? if ( ! empty($image['intro'])) { ?>
				<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $feature['URL'] . '/' . $image['intro'][0]['FileName'], array('width'=>'100px')); ?>
				<hr class="space">
				<a class="smallbutton" id="introspan">Edit Image</a>
			<? } else { ?>
				<hr class="space">
				<a class="smallbutton" id="introspan">Add an Image</a>
			<? } ?>
			<div id="introimageadd" style="display: none;">
				<? echo Form::open('/features/save_image', array('enctype' => 'multipart/form-data')); ?>
					<table width='100%' border='0' cellspacing='1' cellpadding='2'>
					<tbody>
						<tr>
							<td>File:</td>
							<td><? echo Form::file('file'); ?></td>
						</tr>
						<tr>
							<td>
								<? echo Form::hidden('id', $feature['URL']);?>
								<? echo Form::hidden('type', '1');?>
							</td>
							<td>
								<? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?>
								<span class="cancel">&nbsp;or&nbsp;</span>
								<? echo HTML::anchor('/features/view/' . $feature['URL'], 'cancel'); ?>
							</td>
						</tr>
					</tbody>
				    </table>
				<? echo Form::close(); ?>
			</div>			
			<hr class="space">
			<span class="formtitle" style="color:#a0a0a0; font-size:16px;">Hero Image:</span><br />			
			<? if ( ! empty($image['hero'])) { ?>
				<? echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'features/' . $feature['URL'] . '/' . $image['hero'][0]['FileName'], array('width'=>'200px')); ?>
				<hr class="space">
				<a class="smallbutton" id="herospan">Edit Image</a>
			<? } else { ?>
				<hr class="space">
				<a class="smallbutton" id="herospan">Add an Image</a>
			<? } ?>
			<hr class="space">
			<div id="heroimageadd" style="display: none;">
				<? echo Form::open('/features/save_image', array('enctype' => 'multipart/form-data')); ?>
					<table width='100%' border='0' cellspacing='1' cellpadding='2'>
					<tbody>
						<tr>
							<td>File:</td>
							<td><? echo Form::file('file'); ?></td>
						</tr>
						<tr>
							<td>
								<? echo Form::hidden('id', $feature['URL']);?>
								<? echo Form::hidden('type', '4');?>
							</td>
							<td>
								<? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?>
								<span class="cancel">&nbsp;or&nbsp;</span>
								<? echo HTML::anchor('/features/view/' . $feature['URL'], 'cancel'); ?>
							</td>
						</tr>
					</tbody>
				    </table>
				<? echo Form::close(); ?>
			</div>
			<? if ($feature['FeatureTypes_ID'] == 1) { ?>
				<hr class="space">
				<span class="formtitle" style="color:#a0a0a0; font-size:16px;">Artist MP3:</span><br />			
				<? if ( ! empty($feature['audio']['FileName'])) { ?>
					<? echo HTML::anchor(Kohana::$config->load('paths.s3_path') . 'audio/features/' . $feature['URL'] .'/'. $feature['audio']['FileName'] , 'download ' . $feature['audio']['Name'], array('class'=>'link')); ?>
					<hr class="space">
					<a class="smallbutton" id="audiospan">Edit Audio</a>
				<? } else { ?>
					<hr class="space">
					<a class="smallbutton" id="audiospan">Add a MP3</a>
				<? } ?>
				<hr class="space">
				<div id="audioadd" style="display: none;">
					<? echo Form::open('/features/save_audio', array('enctype' => 'multipart/form-data')); ?>
						<table width='100%' border='0' cellspacing='1' cellpadding='2'>
						<tbody>
							<tr>
								<td colspan="2">Audio files may take a few minutes to upload.</td>
							</tr>
							<tr>
								<td>Name:</td>
								<td><? echo Form::input('name', $feature['audio']['Name'], array('class'=>'formtitle')); ?></td>
							</tr>
							<tr>
								<td>File:</td>
								<td><? echo Form::file('file'); ?></td>
							</tr>
							<tr>
								<td><? echo Form::hidden('id', $feature['URL']);?></td>
								<td>
									<? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?>
									<span class="cancel">&nbsp;or&nbsp;</span>
									<? echo HTML::anchor('/features/view/' . $feature['URL'], 'cancel'); ?>
								</td>
							</tr>
						</tbody>
					    </table>
					<? echo Form::close(); ?>
				</div>
			<? } ?>
		</div>
	</div>
</div>