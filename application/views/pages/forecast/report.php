<?= Form::open('/forecast/report/'); ?>
<div style="margin-bottom: 10px; padding-left: 8px; width: 99%; line-height: 30px; background-color: #030133; color: #FFFFFF; font-weight: bold;">Search: Customers</div>
<div><?= Form::select('customer', $customers, $customer, array()); ?></div>
<div style="margin-top: 10px;">
    <table>
        <thead>
            <tr>
                <td>Part</td>
                    <?php for ($i = 0; $i < 6; $i++) { ?>
                        <td><?= date('M', mktime(0, 0, 0, date('m') + $i, date('d'), date('Y'))); ?></td>
                    <?php } ?>
                <td>Total</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($parts as $part): ?>
                <?php
                    $name = preg_replace('/[^a-z0-9_-]/i', '-', $part['NUM']);
                    $columns = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H');
                ?>
                <tr>
                    <td><a name="<?= $name; ?>" title="<?= $part['DESCRIPTION']; ?>"><?= $part['NUM']; ?></a></td>
                    <td><?= Form::input(str_pad('1', 2, '0', STR_PAD_LEFT), 0, array()); ?></td>
                    <td><?= Form::input(str_pad('2', 2, '0', STR_PAD_LEFT), 0, array()); ?></td>
                    <td><?= Form::input(str_pad('3', 2, '0', STR_PAD_LEFT), 0, array()); ?></td>
                    <td><?= Form::input(str_pad('4', 2, '0', STR_PAD_LEFT), 0, array()); ?></td>
                    <td><?= Form::input(str_pad('5', 2, '0', STR_PAD_LEFT), 0, array()); ?></td>
                    <td><?= Form::input(str_pad('6', 2, '0', STR_PAD_LEFT), 0, array()); ?></td>
                    <td><?= Form::input('total', 0, array()); ?></td>
                </td>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?= Form::close(); ?>