<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.button').click(function() {
			$('.loading').show();
			$('.loadingtext').show();
			$('#submitdiv').hide();
		});
		$('.message').delay(5000).fadeOut('slow');
	});
</script>

<div class="span-24 container">
	<div class="span-24">
		<? echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/productsheader.jpg', array('style'=>'height:146px;')); ?>
	</div>
	<div class="push-1 span-23">
		<hr class="space">
		<? echo Html::anchor('http://ifrogz.com/product/'. $item->Product->pID, Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/view.gif'), array('target'=>'_blank', 'title'=>'View on iFrogz.com')); ?>
		<span class="departmenttitle" style="padding:0 0 0 10px;"><? echo strtoupper($item->Product->pName) ?></span>
		<hr class="top">
	</div>
	<div class="push-1">
		<? echo Form::open('/minicart/update', array('enctype' => 'multipart/form-data')) ?>
		<table width='100%' border='0' cellspacing='1' cellpadding='2'>
			<tbody>
				<tr>
					<td>SKU:</td>
					<td><? echo Form::input('product', $item->Product->pID, array('class'=>'formtitle span-6')); ?></td>
				</tr>
				<tr>
					<td>Title:</td>
					<td><? echo Form::input('title', $item->Title, array('class'=>'formtitle span-10')); ?></td>
				</tr>
				<tr>
					<td>Description:</td>
					<td>
						<? echo Form::textarea('body', $item->Body, array('class'=>'formvalues span-20')); ?><br />
						<span>Text must be formatted in HTML.</span>
					</td>
				</tr>
				<tr>
					<td>Status:</td>
					<td><? echo Form::select('status', array('1'=>'Disabled', '2'=>'Paused', '3'=>'Active'), $item->Status->ID)?></td>
				</tr>
				<tr style="display:none;">
					<td>Image:</td>
					<td>
					<span><? echo $item->FileName; ?></span>
					<hr class="space">
					<? echo Form::file('file'); ?></td>
				</tr>
				<tr>
					<td>
						<? echo Form::hidden('id', $item->ID)?>
						<? echo Form::hidden('sequence', '0')?>
					</td>
					<td>
						<? echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/ajax.gif', array('style'=>'display:none; margin:5px 0 0 5px;', 'class'=>'loading'))?>
						<span class="cancel loadingtext" style="display: none;">Saving...</span>
						<div id="submitdiv">
							<? echo Form::submit('', 'save', array('style'=>'font-size:12px;', 'class'=>'button')); ?>
							<span class="cancel">&nbsp;or&nbsp;</span>
							<? echo Html::anchor('/minicart/', 'cancel'); ?>
						</div>
					</td>
				</tr>
			</tbody>
	    </table>
		<? echo Form::close(); ?>
	</div>
</div>