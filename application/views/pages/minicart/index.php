<? /* if (Authenticate::has_permission($auth_roles, $user_roles)) { ?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.the_span').click(
		function() {
			$(this).children('.edit').show();
			$(this).children('.display').hide();
		},
		function() {
			$(this).children('.edit').hide();
			$(this).children('.display').show();
		});
		// Click aways
		$('body').click(function(event) {
			var t=1;

			// For user menu
			if (event.target.id != '.edit') {
				if (!$(event.target).closest('#status').length) {
			        $('.edit').hide();
					$('.display').show();
			    };
			}
		});
		$('.the_price').click(
		function() {
			$(this).children('.editprice').show();
			$(this).children('.displayprice').hide();
		},
		function() {
			$(this).children('.editprice').hide();
			$(this).children('.displayprice').show();
		});
		// Click aways
		$('body').click(function(event) {
			var t=1;

			// For user menu
			if (event.target.id != '.editprice') {
				if (!$(event.target).closest('#price').length) {
			        $('.editprice').hide();
					$('.displayprice').show();
			    };
			}
		});
		$('.button').click(function() {
			$('.loading').show();
			$('.button').hide();
			$('.status_select').hide();
		});
	});
</script>
<? } */ ?>
<div class="span-24 container append-bottom">
	<div class="span-24">
		<? echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/productsheader.jpg', array('style'=>'height:146px;')); ?>
	</div>
	<div class="push-1 span-23">
		<hr class="space">
		<span class="departmenttitle">MINI-CART PRODUCTS</span>
		<? echo Html::anchor('/', '< back to admin', array('class'=>'procedure', 'style'=>'padding:0 0 0 15px;')); ?>
		<hr class="top">
		<? if (count($items) > 0) { ?>
			<? echo HTML::anchor('/minicart/add', 'add a minicart item', array('class'=>'smallbutton')); ?>
		<? } else { ?>
			<? echo HTML::anchor('/minicart/add', 'add a minicart item', array('class'=>'button')); ?>
		<? } ?>
		<hr class="space">
	</div>
	<div class="push-1 span-23">
		<? if (count($items) > 0) { ?>
		<table>
			<thead>
				<tr style="background-color:#ccc;">
					<td class="border"></td>
					<td style="font-weight:bold;" class="border">Product Name</td>
					<td style="font-weight:bold; text-align:center;" class="border">Status</td>
					<td class="border" width="15px"></td>
				</tr>
			</thead>
			<tbody>
				<? foreach ($items as $item) { ?>
					<tr height="45px">
						<td width="15px" style="text-align:center;" class="border">
								<? echo Html::anchor('http://ifrogz.com/product/'. $item->Product->pID, Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/view.gif'), array('target'=>'_blank', 'title'=>'View on iFrogz.com')); ?>
						</td>
						<td class="border"><? echo Html::anchor('/minicart/edit/'. $item->ID, $item->Product->pName, array('class'=>'procedure', 'title'=>'Edit: ' . $item->Product->pName)); ?></td>
						<td width="140px" class="border" id="status">
							<div class="the_span">
								<div class="display" style="z-index:5; font-size:14px; margin:0 0 0 15px;">
									<? switch ($item->Status->ID) {
										case '1':
											echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/disabled.png') . '&nbsp;&nbsp;<span class="status">Disabled</span>';
										break;
										case '2':
											echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/paused.gif') . '&nbsp;&nbsp;<span class="status">Paused</span>';
										break;
										case '3':
											echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/running.gif') . '&nbsp;&nbsp;<span class="status">Active</span>';
										break;									
										default:
											# code...
										break;
									} ?>
								</div>
								<? /* ?>
								<? if (Authenticate::has_permission($auth_roles, $user_roles)) { ?>
									<div class="edit" id="edit" style="display:none; z-index:300;">
										<? echo Form::open('/products/status') ?>
											<? echo Form::select('status', array('0'=>'Active', '5'=>'Staging', '127'=>'Disabled'), $section['sectionDisabled'], array('class'=>'status_select'))?>
											<? echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/ajax.gif', array('style'=>'display:none; margin:5px 0 0 5px;', 'class'=>'loading'))?>
											<? echo Form::hidden('sku', $section['sectionID'], array('class'=>'sku')); ?>
											<input type="submit" name="" value="save" class="button" style="font-size:12px; margin:5px 0 0 5px;">
										<? echo Form::close() ?>
									</div>
								<? } ?>
								<? */ ?>
							</div>	
						</td>
						<td style="text-align:center;">
							<? echo HTML::anchor('minicart/delete/' . $item->ID, Html::image('http://cdn.projects.ifrogz.com/lib/images/icons/delete.png', array('title'=>'Delete')))?>
						</td>
					</tr>
				<? } ?>
			</tbody>
		</table>
		<? } ?>
	</div>
</div>