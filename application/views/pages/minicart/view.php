<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('.message').delay(5000).fadeOut('slow');
	});
</script>

<div class="span-24 container">
	<div class="span-24">
		<? echo Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/productsheader.jpg', array('style'=>'height:146px;')); ?>
	</div>
	<div class="push-1 span-23">
		<hr class="space">
			<? echo HTML::anchor('/minicart', '< back to minicart list', array('class'=>'procedure')); ?>
		<hr class="space">
		<? echo Html::anchor('http://ifrogz.com/product/'. $item->Product->pID, Html::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/view.gif'), array('target'=>'_blank', 'title'=>'View on iFrogz.com')); ?>
		<span class="departmenttitle" style="padding:0 0 0 10px;"><? echo strtoupper($item->Product->pName) ?></span>
		<hr class="top">
		<? echo HTML::anchor('/minicart/edit/'.$item->ID, 'edit ' . $item->Product->pName, array('class'=>'smallbutton')); ?>
		<? if ($success == 'Saved') { ?>
			<div class="message">
				<hr class="space">
				<div class="span-15 success">
					<span>Item Saved.</span>
				</div>
			</div>
		<? } ?>
	</div>
	<div class="push-1">
		<hr class="space">
		<div class="span-18">
			<h1><? echo $item->Title ?></h1>
			<? echo $item->Body ?>
		</div>
	</div>
</div>