<div style="width: 100%;">
    <?= Form::open('/password/change/'); ?>
        <br />
        <table style="margin-left: auto; margin-right: auto; border-collapse: collapse;">
            <thead>
                <tr>
                    <td style="width: 450px; font-family: arial,helvetica,sans-serif; font-weight: bold; font-size: 16px;">Want to change your password?</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding-top: 15px;">To change your password, type-in your new password in both fields below.</td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">Type-In New Password&nbsp;<?= Form::password('password_1', '', array('id' => 'password_1', 'style' => 'width: 175px; height: 20px;')); ?></td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">Confirm New Password&nbsp;<?= Form::password('password_2', '', array('id' => 'password_2', 'style' => 'width: 175px; height: 20px;')); ?></td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;"><input id="update" type="button" value="Update" style="width: 65px; height: 25px; background-color: #9ECC3A; color: #FFFFFF; font-size: 12px; padding-bottom: 2px;" /></td>
                </tr>
            </tbody>
        </table>
        <br />
    <?= Form::close(); ?>
</div>