<div style="width: 100%;">
    <?= Form::open('/password/reset/'); ?>
        <br />
        <table style="margin-left: auto; margin-right: auto; border-collapse: collapse;">
            <thead>
                <tr>
                    <td style="width: 400px; font-family: arial,helvetica,sans-serif; font-weight: bold; font-size: 16px;">Forgot your password?</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding-top: 15px;">To reset your password, type in the field below your full email address.</td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">Email address&nbsp;<?= Form::input('email', '', array('id' => 'email', 'style' => 'width: 175px; height: 20px;')); ?></td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;"><input id="reset" type="button" value="Submit" style="width: 65px; height: 25px; background-color: #9ECC3A; color: #FFFFFF; font-size: 12px; padding-bottom: 2px;" /></td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;"><a href="/user/login/">Want to try logging in?</a></td>
                </tr>
            </tbody>
        </table>
        <br />
    <?= Form::close(); ?>
</div>