<script language="JavaScript" type="text/javascript" src="/lib/js/pages/perks/index.js"></script>
<div style="margin-bottom: 10px; padding-left: 8px; width: 99%; line-height: 30px; background-color: #030133; color: #FFFFFF; font-weight: bold;">History: Work Perks</div>
<div style="line-height: 30px;">
    <?= Form::open('/perks/index/', array('id' => 'search_form', 'name' => 'search_form')); ?>
        <?= Form::label('date_range', 'Date Range:'); ?>
        <?= Form::input('beg_date', $params['beg_date'], array('id' => 'beg_date', 'class' => 'datepicker', 'style' => 'width: 75px;', 'readonly' => 'readonly')); ?>
        &nbsp;&mdash;&nbsp;
        <?= Form::input('end_date', $params['end_date'], array('id' => 'end_date', 'class' => 'datepicker', 'style' => 'width: 75px;', 'readonly' => 'readonly')); ?><br />
        <input type="button" name="preview" id="preview" value="Preview" />
        <input type="button" name="export" id="export" value="Export" />
    <?= Form::close(); ?>
</div>
<div>
    <? if (count($records) > 0): ?>
        <br />
        <table>
            <thead>
                <tr>
                    <? foreach ($headers as $column): ?>
                        <td><?= $column; ?></td>
                    <? endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <? foreach ($records as $record): ?>
                    <tr>
                        <? foreach ($record as $column): ?>
                            <td><?= $column; ?></td>
                        <? endforeach; ?>
                    </tr>
                <? endforeach; ?>
            </tbody>
        </table>
    <? endif; ?>
</div>