<link rel="stylesheet" href="/lib/css/blueprint/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="/lib/css/blueprint/print.css" type="text/css" media="print">
<style type="text/css">
tr.content:hover {
	background-color: #FFFF99;
}
.scheduledates {
	font-size:20px; 
	font-family: Helvetica, Arial, sans-serif; 
	font-weight: lighter;
	color:#000000;
	background-color:#FFFFFF;
}
.formvalues {
	font-family: Helvetica, Arial, sans-serif;
	font-weight: lighter;
	color:#000000;
	font-size: 18px;
}
.formtitle {
	height:30px;
	font-size:20px; 
	font-family: Helvetica, Arial, sans-serif; 
	font-weight: lighter;
	color:#000000;
}
</style>
<script>
	$(document).ready(function() {
		$(function() {
			$( ".datepicker" ).datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'yy-mm-dd'
			});
		});
	});
</script>

<div class="span-20 container">
	<div class="span-20">
		<h1>iFrogz Press</h1>
	</div>
	<div class="span-15">
		<? echo Form::open('/press/save', array('enctype' => 'multipart/form-data')) ?>
		<table width='100%' border='0' cellspacing='1' cellpadding='2'>
		<thead>
		    <th colspan="2">Add a Press Release</th>
		</thead>
		<tbody>
			<tr>
				<td>Title:</td>
				<td><? echo Form::input('title', '', array('class'=>'formtitle span-10')); ?></td>
			</tr>
			<tr>
				<td>Release Date:</td>
				<td><? echo Form::input('posted', '', array('class'=>'formtitle span-6 datepicker')); ?></td>
			</tr>
			<tr>
				<td>File:</td>
				<td><? echo Form::file('file'); ?></td>
			</tr>
			<tr>
				<td>File Type:</td>
				<td><? echo Form::select('icon', array('1'=>'Word', '2'=>'PDF'), '')?></td>
			</tr>
			<tr>
				<td></td>
				<td><? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?> or </span>
				<? echo HTML::anchor('/press', 'cancel'); ?></td></td>
			</tr>
		</tbody>
	    </table>
		<? echo Form::close(); ?>
	</div>
</div>