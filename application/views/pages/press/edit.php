<link rel="stylesheet" href="/lib/css/blueprint/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="/lib/css/blueprint/print.css" type="text/css" media="print">
<style type="text/css">
tr.content:hover {
	background-color: #FFFF99;
}
.scheduledates {
	font-size:20px; 
	font-family: Helvetica, Arial, sans-serif; 
	font-weight: lighter;
	color:#000000;
	background-color:#FFFFFF;
}
.formvalues {
	font-family: Helvetica, Arial, sans-serif;
	font-weight: lighter;
	color:#000000;
	font-size: 18px;
}
.formtitle {
	height:30px;
	font-size:20px; 
	font-family: Helvetica, Arial, sans-serif; 
	font-weight: lighter;
	color:#000000;
}
</style>
<script>
	$(document).ready(function() {
		$(function() {
			$( ".datepicker" ).datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'M dd, yy'
			});
		});
	});
</script>

<div class="span-20 container">
	<div class="span-20">
		<h1>iFrogz Press</h1>
	</div>
	<div class="span-15">
		<? echo Form::open('/press/saveedit', array('enctype' => 'multipart/form-data')) ?>
		<table width='100%' border='0' cellspacing='1' cellpadding='2'>
		<thead>
		    <th colspan="2">Edit Press Release Details</th>
		</thead>
		<tbody>
			<tr>
				<td>Title:</td>
				<td><? echo Form::input('title', $release[0]['Title'], array('class'=>'formtitle span-10')); ?></td>
			</tr>
			<tr>
				<td>Release Date:</td>
				<td><? echo Form::input('posted', strftime("%b %d, %Y", strtotime($release[0]['DatePosted'])), array('class'=>'formtitle span-6 datepicker')); ?></td>
			</tr>
			<tr>
				<td>File:</td>
				<td><? echo Form::file('file'); ?></td>
			</tr>
			<tr>
				<td>File Type:</td>
				<td><? echo Form::select('icon', array('1'=>'Word', '2'=>'PDF'), $release[0]['IconPath'])?></td>
			</tr>
			<tr>
				<td>Active:</td>
				<td>
					<? if ($release[0]['IsActive'] == 1) { ?>
						<? echo Form::checkbox('active', 'yes', TRUE) ?>
					<? } else { ?>
						<? echo Form::checkbox('active', 'yes', FALSE) ?>
					<? } ?>
				</td>
			</tr>
			<tr>
				<td>Approval:</td>
				<td><? echo $release[0]['employee'][0]['firstname'] ?> <? echo $release[0]['employee'][0]['lastname'] ?> on <? echo strftime("%b %d, %Y", strtotime($release[0]['DateModified'])) ?></td>
			</tr>
			<tr>
				<td><? echo Form::hidden('id', $release[0]['ID'])?></td>
				<td><? echo Form::submit('', 'save', array('style'=>'font-size:12px;')); ?><span class="cancel"> or </span>
				<? echo HTML::anchor('/press', 'cancel'); ?></td>
			</tr>
		</tbody>
	    </table>
		<? echo Form::close(); ?>
	</div>
	
	
</div>