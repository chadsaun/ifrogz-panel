<link rel="stylesheet" href="/lib/css/blueprint/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="/lib/css/blueprint/print.css" type="text/css" media="print">
<style type="text/css">
tr.content:hover {
	background-color: #FFFF99;
}
</style>

<div class="span-20 container">
	<div class="span-20">
		<h1>iFrogz Press</h1>
	</div>
	<div class="span-10">
		<table>
			<thead>
				<tr>
					<th>Live Releases</th>
				</tr>
			</thead>
		<?	for ($i = 0; $i < count($active); $i++) { ?>
				<tr class="content">
					<td><? echo HTML::anchor('/press/edit/' . $active[$i]['ID'], $active[$i]['Title']); ?></td>
				</tr>
		<? } ?>
		</table>
	</div>
	
	<hr class="space">
	<? echo HTML::anchor('/press/add', 'add a release'); ?>
	<hr class="space">
	<? if (count($inactive) == 0) {
		# code...
	} else { ?>
		<div class="span-10">
			<table>
				<thead>
					<tr>
						<th>Not Live Releases</th>
					</tr>
				</thead>
			<?	for ($i = 0; $i < count($inactive); $i++) { ?>
					<tr class="content">
						<td><? echo HTML::anchor('/press/edit/' . $inactive[$i]['ID'], $inactive[$i]['Title']); ?></td>
					</tr>
			<? } ?>
			</table>
		</div>
	<? } ?>

	
	
</div>

