<h2>Returns/Exchanges Report</h2>

<div id="search">
	<?php print Form::open(NULL, array('id' => 'mainform')); ?>
	<label for="from_date">From: <?php print Form::input('from_date', $form['from_date'], array('id' => 'from_date')); ?></label>
	<label for="to_date">To: <?php print Form::input('to_date', $form['to_date'], array('id' => 'to_date')); ?></label>
	<?php print Form::submit('submit', 'Search', array('id' => 'submit')); ?>
	<?php print Form::close(); ?>
</div>

<div id="download"><a href="returns/export?from_date=<?php echo $form['from_date'] ?>&to_date=<?php echo $form['to_date'] ?>">Download Report</a></div>

<div id="grid">
	<table>
		<thead>
			<th>Order #</th>
			<th>Part #</th>
			<th>Quantity</th>
		</thead>
		<tbody>
			<?php foreach ($data as $row): ?>
			<tr>
				<td><?php echo $row[Model_ORM_iFrogz_Order::ID] ?></td>
				<td><?php echo $row['part'] ?></td>
				<td><?php echo $row['qty'] ?></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>