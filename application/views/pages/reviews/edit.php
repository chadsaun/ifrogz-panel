<style type="text/css">
tr {
	background-color: #EAECEB;
}
</style>

<table width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td bgcolor="#030133" style="color:#FFFFFF; font-weight:bold; font-size:14px">Customer Product Reviews</td>
  </tr>

</table>

<form method='post' action='/reviews/update' name='update_review'> 
	<table width='100%' border='0' cellspacing='1' cellpadding='2'>
	<tr><td>Title:</td><td><input name='title' style='font-size: 14px;' size='80' type='text' value='<?php echo $ReviewTitle ?>'></input></td></tr>
	<tr><td>Body:</td><td><textarea name='rev_body' style='font-size: 12px;' rows='10' cols='100'><?php echo $ReviewBody ?></textarea></td></tr>
	<tr><td>Score:</td><td><?php echo $ReviewScore ?></td></tr>
	<tr><td>Product:</td><td><a target='_blank' href='<?php echo $ProductURL; ?>'><?php echo $ProductName; ?></a></td></tr>
	<tr><td>Date Created:</td><td><?php echo $DateCreated ?></td></tr>
	<tr><td>Last Reviewer:</td><td><?php echo $EmployeeName ?> <?php echo $DateModified ?></td></tr>
	

	<tr><td>Approval:</td><td>
	<select name="approved">
		<?php
		if ($ReviewStatus == '1') {
			?> <option value="1" selected="selected">pending</option> <?php
		} else {
			?> <option value="1">pending</option> <?php
		}

		if ($ReviewStatus == '3') {
			?> <option value="3" selected="selected">approved</option> <?php
		} else {
			?> <option value="3">approved</option> <?php
		}
		
		if ($ReviewStatus == '2') {
			?> <option value="2" selected="selected">declined</option> <?php
		} else {
			?> <option value="2">declined</option> <?php
		}
		if ($ReviewStatus == '4') {
			?> <option value="4" selected="selected">delete</option> <?php
		} else {
			?> <option value="4">delete</option> <?php
		} ?>
	</select>
	</td></tr>
	<input name="ID" type="hidden" value="<? echo $ID ?>"></input>
	<tr><td></td><td><input type='submit' value='save'></input> <a href='/reviews'><u>cancel</u></a></td></tr>
    </table>
</form>