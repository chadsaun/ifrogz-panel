<style type="text/css">
    tr.content:hover {
        background-color: #FFFF99;
    }

    div.max_width {
        max-width: 300px;
    }

    table.table_data tr th {
        color: #E7EAEF;
        background-color: #030133;
    }
</style>
<script>
	$(document).ready(function() {
	    $(".updateStatus").change(function() {
	        var opt_vals = $(this).val().split('-');
	        var request = {
				async: true,
				type: 'POST',

				url: '/reviews/updatestatus',
				data: {
					record_id: opt_vals[0],
					status_id: opt_vals[1]
				},
				dataType: 'json',
				success: function(json) {
					if (json['response']['status'] == 'success') {
						message = json['response']['data'];
					}
					else {
						message = json['response']['errors'][0]['error']['message'];
					}
					$("#status_update").fadeIn('fast');
					$('#message').html(message);
				    $("#status_update").delay(2500).fadeOut();
					
				},
				error: function() {
					alert('Error: Failed to update status');
				}
			};
			$.ajax(request);
	    });

        $( "#first_date" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });


        $( "#last_date" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });


        $("#status_post_update").fadeIn('fast');
        $("#status_post_update").delay(2500).fadeOut();

		
	});
	

	
</script>

<form method="post" action="/reviews" class="formee">

    <fieldset>
        <legend>Search Product Reviews</legend>
        <div class="grid-4-12">
            <label>Show:</label>
            <?php echo Form::select('review_status', $review_status_options, Arr::get($_SESSION, 'review_status')); ?>
        </div>
        <div class="grid-8-12">
            <label>Score between:</label>
            <?php echo Form::select('rank1', $rank_one_options, Arr::get($_SESSION, 'rank1'), array('class' => 'formee-small')); ?>
            <?php echo Form::select('rank2', $rank_two_options, Arr::get($_SESSION, 'rank2'), array('class' => 'formee-small')); ?>
        </div>
        <div class="grid-6-12">
            <label>Date between:</label>
            <?php echo Form::input('first_date', Arr::get($_SESSION, 'first_date'), array('class' => 'formee-small')); ?><?php echo Form::input('last_date', Arr::get($_SESSION, 'last_date'), array('class' => 'formee-small')); ?>
            <?php echo Form::submit('Search', 'search') ?>
        </div>
    </fieldset>

</form>

<div style="height:55px;">
	<? if(isset($changeMessage)) {
			?> <div id="status_post_update">
					<table width="100%" cellspacing="0" cellpadding="2">
					  <tr height="50px">
					    <td bgcolor="#FFFF99" style="color:#000000;  font-size:18px; text-align:center;"><? echo $changeMessage ?></td>
					  </tr>
					  <tr height="20px"><td></td></tr>
					</table>
				</div> 
	<? } ?>
		
		<?php if (count($reviewID) == 0 ){
			?>
			<div>
				<table width='100%' cellspacing='0' cellpadding='2'>
				  <tr height='50px'>
				    <td bgcolor='#FFFF99' style='color:#000000;  font-size:24px; text-align:center;'>No Reviews<br /><span style='font-size:14px;'>Adjust filters above</span></td>
				  </tr>
				  <tr height='20px'><td></td></tr>
				</table>
			</div>
		<? } ?>

		<div id="status_update">
			<span id="message"></span>
		</div>
</div>


<table width='1000px' border='0' cellspacing='1' cellpadding='2' class="table_data">
	<tr>
        <th>Date</th>
        <th>Product</th>
        <th>Person</th>
        <th>Title</th>
        <th>Review</th>
        <th>Score</th>
        <th>Status</th>
        <th></th>
	</tr>
		
<?php

for ($i = 0; $i < count($reviewID); $i++) { ?>

<tr class='content' bgcolor='#EAECEB'>
	<td>
		<?php echo date('M j, Y', strtotime($reviewDate[$i])); ?>
	</td>
    <td><?php echo $reviewProd[$i]; ?></td>
    <td><?php echo $reviewPerson[$i]; ?></td>
	<td><div class="max_width"><?php echo $reviewTitle[$i] ?></div></td>
	<td><div class="max_width"><?php  echo $reviewBody[$i] ?></div></td>
	<td align='center'>
		<?php echo $reviewScore[$i] ?>
	</td>
	<td>
		<form>
			<select class="updateStatus">
				<?php
				if ($reviewStatus[$i] == '1') {
					?> <option value="<?php echo $reviewID[$i] . '-1'; ?>" selected="selected">pending</option> <?php
				} else {
					?> <option value="<?php echo $reviewID[$i] . '-1'; ?>">pending</option> <?php
				}

				if ($reviewStatus[$i] == '3') {
					?> <option value="<?php echo $reviewID[$i] . '-3'; ?>" selected="selected">approved</option> <?php
				} else {
					?> <option value="<?php echo $reviewID[$i] . '-3'; ?>">approved</option> <?php
				}
				
				if ($reviewStatus[$i] == '2') {
					?> <option value="<?php echo $reviewID[$i] . '-2'; ?>" selected="selected">declined</option> <?php
				} else {
					?> <option value="<?php echo $reviewID[$i] . '-2'; ?>">declined</option> <?php
				}
				if ($reviewStatus[$i] == '4') {
					?> <option value="<?php echo $reviewID[$i] . '-4'; ?>" selected="selected">delete</option> <?php
				} else {
					?> <option value="<?php echo $reviewID[$i] . '-4'; ?>">delete</option> <?php
				} ?>
			</select>	
		</form>
	</td> 
	<td  align='center'>
		<a href="/reviews/edit/<?php echo $reviewID[$i] ?>"><u>edit</u></a>
	</td>
</tr>
<?
  }
?>
	
</table>
</div>

