<div style="width: 100%;">
    <?= Form::open('/user/login/'); ?>
        <br />
        <table style="margin-left: auto; margin-right: auto; border-collapse: collapse; border: 1px solid #E5E5E5; background-color: #F5F5F5;">
            <thead>
                <tr>
                    <td style="padding: 20px 20px 5px 20px; font-family: arial,helvetica,sans-serif; font-size: 16px;">Sign in</td>
                    <td style="padding: 17px 20px 3px 20px; text-align: right;"><img style="width: 50px; height: 21px;" src="/lib/images/projects/ifrogz-logo.gif"></td>
                </tr>
            </thead>
            <tbody>
            	<tr> 
            		<td colspan="2" style="padding: 20px 20px 5px 20px; font-weight: bold; font-size: 12px; width: 260px;">Username</td>
            	</tr>
                <tr>
            		<td colspan="2" style="padding: 0px 20px 0px 20px; width: 260px;"><input type="text" name="user" style="width: 260px; height: 25px;"></td>
            	</tr>
            	<tr> 
            		<td colspan="2" style="padding: 20px 20px 5px 20px; font-weight: bold; font-size: 12px; width: 260px;">Password</td>
            	</tr>
                <tr>
            		<td colspan="2" style="padding: 0px 20px 0px 20px; width: 260px;"><input type="password" name="pass" style="width: 260px; height: 25px;"></td>
            	</tr>
            	<?php if (!empty($error_message)): ?>
            		<tr>
            			<td colspan="2" style="padding: 5px 20px 0px 20px; width: 260px; color: #FF0000;"><?= $error_message; ?></td>
            		</tr>
            	<?php endif; ?>
            	<tr> 
            		<td style="padding: 20px 0px 0px 20px; width: 75px;"><input type="submit" value="Sign in" style="width: 75px; height: 30px; background-color: #9ECC3A; color: #FFFFFF; font-size: 14px; padding-bottom: 2px;" /></td>
            		<td style="padding: 20px 20px 0px 5px; width: 175px;"><?= Form::checkbox('rememberme', 'yes', FALSE, array('id' => 'rememberme')); ?> Remember me for 1 day!</td>
            	</tr>
            	<tr>
            		<td colspan="2" style="padding: 0px 20px 20px 20px; width: 260px;"><br /><a href="/password/forgot/">Forgot your password?</a></td>
            	</tr>
        	</tbody>
        </table>
        <br />
    <?= Form::close(); ?>
</div>