<div style="width: 100%;">
    <br />
    <table style="margin-left: auto; margin-right: auto; border-collapse: collapse; border: 1px solid #E5E5E5; background-color: #F5F5F5;">
        <tr>
            <td style="padding: 20px 20px 0px 20px; width: 260px; text-align: center;">
                You are now logged out.
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 20px 20px 20px; width: 260px; text-align: center;">
                <a href="/user/login/">Log back in to iFrogz Admin</a>
            </td>
        </tr>
    </table>
    <br />
</div>