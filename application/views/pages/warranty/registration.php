<style type="text/css" media="screen">
	td.v2-header, p.v2-title    { text-align: center; font-weight: bold; }
	td.v2-header                { background-color: #030133; color: #FFFFFF; height: 25px; }
	td.v2-outter-left-column    { vertical-align: top; width: 427px; }
	td.v2-outter-right-column   { vertical-align: top; width: 427px; }
	td.v2-inner-left-column     { text-align: right; width: 125px; font-weight: bold; }
	td.v2-inner-right-column    { text-align: left;  width: 250px; }
	p.v2-back                   { text-align: center; }
</style>

<p class="v2-title"></p>
<div>
	<table>
		<thead>
		<tr>
			<td class="v2-header" colspan="2">Registration &#35;<?= $registration['ID']; ?></td>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class="v2-outter-left-column">
				<table>
					<tbody>
					<tr>
						<td class="v2-inner-left-column">First Name:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['FirstName']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Last Name:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['LastName']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Address:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Address1']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Address2']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">City:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['City']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">State:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['State']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Country:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Country']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Phone:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Phone']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Email:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Email']; ?></td>
					</tr>
					</tbody>
				</table>
			</td>
			<td class="v2-outter-right-column">
				<table>
					<tbody>
					<tr>
						<td class="v2-inner-left-column">Store:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Store']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Product:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Product']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Purchase Price:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['PurchasePrice']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Date Purchased:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['DatePurchased']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Comment:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['Comment']; ?></td>
					</tr>
					<tr>
						<td class="v2-inner-left-column">Date Registered:&nbsp;</td>
						<td class="v2-inner-right-column"><?= $registration['DateCreated']; ?></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>

<p class="v2-back"><a href="javascript:history.go(-1);">Back</a></p>