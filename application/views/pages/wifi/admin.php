<div class="span-24">
	<span class="span-24 info">
		<? $text = ( ! empty($networks)) ? 'select' : 'create'; ?>
		Please <?=$text?> a WiFi network.
	</span>
	<? echo Form::open('/wifi/edit'); ?>
		<?
		foreach ($networks as $network):
		?>
			<p>
				<? echo Form::radio('ID', $network['ID']); ?>
				<?=$network['PublicName']?> (<?=$network['PrivateName']?>): 
			</p>
		<?
		endforeach;
		?>
		<hr class="space">
		<p>
			<? if ( ! empty($network)) : ?>
				<? echo Form::submit('edit', 'Edit'); ?> or 
			<? endif; ?>
			<a href="/wifi/create"> Create a Network</a>
		</p>
	<? echo Form::close(); ?>
</div>