<div class="span-24">
	<span class="span-24 info">
		Please create a new WiFi network.
	</span>
	<? echo Form::open('/wifi/create'); ?>
		<? echo View::factory('partials/wifi/network_form')->bind('network', $network); ?>
		<p>
			<? echo Form::submit('', 'Create'); ?> or <a href="/wifi/admin"> Cancel</a>
		</p>
	<? echo Form::close(); ?>
</div>