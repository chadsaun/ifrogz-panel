<div class="span-24">
	<span class="span-24 info">
		Modify the <?=$network['PrivateName']?> network below.
	</span>
	<? echo Form::open('/wifi/edit'); ?>
		<? echo View::factory('partials/wifi/network_form')->bind('network', $network); ?>
		<p>
			<? echo Form::submit('', 'Update'); ?>
			or <a href="/wifi/admin"> Cancel</a>
			or <a href="/wifi/delete/<?=$network['hidden']['ID']?>"> Delete</a>
		</p>
	<? echo Form::close(); ?>
</div>