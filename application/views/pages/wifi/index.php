<? $total_spans = 24; ?>
<? $per_row = 6; ?>
<? if ( ! empty($networks)) : ?>
	<? $total_networks = count($networks); ?>
	<? $span_size = ($total_networks >= $per_row) ? (int) ($total_spans / $per_row) : (int) ($total_spans / $total_networks); ?>
	<div class="span-<?=$total_spans?>">
		<span class="span-<?=$total_spans?> info">
			Please note that by clicking on the I agree below and selecting a wifi network you recognize that
			your actions are being tracked by Reminderband's networking hardware and software.
		</span>
		<form method="post" accept-charset="utf-8">
			<p>
				I agree <input id="i_agree" type="checkbox" name="i_agree" value="1">
			</p>
			<? $count = 0; ?>
			<? foreach ($networks as $network) { ?>
				<? ++$count; ?>
				<? if ($count % $per_row == 1) { ?>
					<p>
				<? } ?>
				<? $last = (($count % $per_row) == 0 || $count == $total_networks) ? ' last' : ''; ?>
				<span class="span-<?=$span_size?><?=$last?>" style="text-align:center;">
					<input type="submit" name="wifi_network" value="<?=$network['PublicName']?>">
				</span>
				<? if ($count % $per_row == 0 || $count == $total_networks) { ?>
					</p>
				<? } ?>
			<? } ?>
		</form>
		<hr class="space">
		<span id="password_div" class="hide success span-<?=$total_spans?>">
			<strong id="network_tier" class="large"></strong>
			<hr class="space">
			<h1 id="password"></h1>
		</span>
	</div>
<? else: ?>
	<span class="span-<?=$total_spans?> info">
		There are no network passwords to view.
	</span>
<? endif; ?>