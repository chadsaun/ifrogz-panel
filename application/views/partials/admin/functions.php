<?php
//This code is copyright (c) Internet Business Solutions SL, all rights reserved.
//The contents of this file are protect under law as the intellectual property of Internet
//Business Solutions SL. Any use, reproduction, disclosure or copying of any kind 
//without the express and written permission of Internet Business Solutions SL is forbidden.
//Author: Vince Reid, vince@virtualred.net
$incfunctionsdefined=TRUE;
@set_magic_quotes_runtime(0);
$magicq = (get_magic_quotes_gpc()==1);
if(@$emailencoding=="") $emailencoding="iso-8859-1";
if(@$adminencoding=="") $adminencoding="iso-8859-1";
if(@$_SESSION["languageid"] != "") $languageid=$_SESSION["languageid"];
function getadminsettings(){
	global $alreadygotadmin,$splitUSZones,$adminLocale,$countryCurrency,$orcurrencyisosymbol,$useEuro,$storeurl,$stockManage,$useStockManagement,$adminProdsPerPage,$countryTax,$delAfter,$delccafter,$handling,$adminCanPostUser,$packtogether,$origZip,$shipType,$origCountry,$origCountryCode,$uspsUser,$uspsPw,$upsUser,$upsPw,$upsAccess,$adminUnits,$emailAddr,$sendEmail,$adminTweaks,$adminlanguages,$adminlangsettings,$currRate1,$currSymbol1,$currRate2,$currSymbol2,$currRate3,$currSymbol3,$currConvUser,$currConvPw,$currLastUpdate;
	if(! @$alreadygotadmin){
		$sSQL = "SELECT adminEmail,adminEmailConfirm,adminTweaks,adminProdsPerPage,adminStoreURL,adminHandling,adminPacking,adminDelUncompleted,adminDelCC,adminUSZones,adminStockManage,adminShipping,adminCanPostUser,adminZipCode,adminUnits,adminUSPSUser,adminUSPSpw,adminUPSUser,adminUPSpw,adminUPSAccess,adminlanguages,adminlangsettings,currRate1,currSymbol1,currRate2,currSymbol2,currRate3,currSymbol3,currConvUser,currConvPw,currLastUpdate,countryLCID,countryCurrency,countryName,countryCode,countryTax FROM admin LEFT JOIN countries ON admin.adminCountry=countries.countryID WHERE adminID=1";
		$result = mysql_query($sSQL) or print(mysql_error());
		$rs = mysql_fetch_array($result);
		$splitUSZones = ((int)$rs["adminUSZones"]==1);
		$adminLocale = $rs["countryLCID"];
		$countryCurrency = $rs["countryCurrency"];
		if(@$orcurrencyisosymbol != "") $countryCurrency=$orcurrencyisosymbol;
		$useEuro = ($rs["countryCurrency"]=="EUR");
		$storeurl = $rs["adminStoreURL"];
		$stockManage = (int)$rs["adminStockManage"];
		$useStockManagement = ($stockManage != 0);
		$adminProdsPerPage = $rs["adminProdsPerPage"];
		$countryTax=(double)$rs["countryTax"];
		$delAfter = (int)$rs["adminDelUncompleted"];
		$delccafter = (int)$rs["adminDelCC"];
		$handling=(double)$rs["adminHandling"];
		$adminCanPostUser=trim($rs["adminCanPostUser"]);
		$packtogether = ((int)$rs["adminPacking"]==1);
		$origZip = $rs["adminZipCode"];

		$shipType=(int)$rs["adminShipping"];
		$origCountry = $rs["countryName"];
		$origCountryCode = $rs["countryCode"];
		$uspsUser = $rs["adminUSPSUser"];
		$uspsPw = $rs["adminUSPSpw"];
		$upsUser = upsdecode($rs["adminUPSUser"], "");
		$upsPw = upsdecode($rs["adminUPSpw"], "");
		$upsAccess = $rs["adminUPSAccess"];
		if((int)$rs["adminUnits"]==0) $adminUnits="KGS"; else $adminUnits="LBS";
		$emailAddr = $rs["adminEmail"];
		$sendEmail = ((int)$rs["adminEmailConfirm"]==1);
		$adminTweaks = (int)$rs["adminTweaks"];
		$adminlanguages = (int)$rs["adminlanguages"];
		$adminlangsettings = (int)$rs["adminlangsettings"];
		$currRate1=(double)$rs["currRate1"];
		$currSymbol1=trim($rs["currSymbol1"]);
		$currRate2=(double)$rs["currRate2"];
		$currSymbol2=trim($rs["currSymbol2"]);
		$currRate3=(double)$rs["currRate3"];
		$currSymbol3=trim($rs["currSymbol3"]);
		$currConvUser=$rs["currConvUser"];
		$currConvPw=$rs["currConvPw"];
		$currLastUpdate=$rs["currLastUpdate"];
		mysql_free_result($result);
	}
	// Overrides
	global $orstoreurl,$oremailaddr;
	if(@$orstoreurl != "") $storeurl=$orstoreurl;
	if((substr(strtolower($storeurl),0,7) != "http://") && (substr(strtolower($storeurl),0,8) != "https://"))
		$storeurl = "http://" . $storeurl;
	if(substr($storeurl,-1) != "/") $storeurl .= "/";
	if(@$oremailaddr != "") $emailAddr=$oremailaddr;
	return(TRUE);
}
function cleanforurl($surl){
$surl = str_replace(' ','_',strtolower(strip_tags($surl)));
return(preg_replace('/[^a-z_0-9]/','',$surl));
}
function getlangid($col, $bfield){
	global $languageid, $adminlangsettings;
	if(@$languageid=="" || @$languageid==1){
		return($col);
	}else{
		if(($adminlangsettings & $bfield) != $bfield) return($col);
	}
	return($col . $languageid);
}
function xmlencodecharref($xmlstr){
	$xmlstr = str_replace(array('&reg;','&','<','>','®'),array('','&#x26;','&#x3c;','&#x3e;',''),$xmlstr);
	$tmp_str="";
	for($i=0; $i < strlen($xmlstr); $i++){
		$ch_code=ord(substr($xmlstr,$i,1));
		if($ch_code<=130) $tmp_str .= substr($xmlstr,$i,1);
	}
	return($tmp_str);
}
function CalcHmacSha1($data, $key){
    $blocksize = 64;
    $hashfunc = 'sha1';
    if (strlen($key) > $blocksize){
        $key = pack('H*', $hashfunc($key));
    }
    $key = str_pad($key, $blocksize, chr(0x00));
    $ipad = str_repeat(chr(0x36), $blocksize);
    $opad = str_repeat(chr(0x5c), $blocksize);
    $hmac = pack('H*', $hashfunc(($key^$opad).pack('H*', $hashfunc(($key^$ipad).$data))));
    return $hmac;
}
function parsedate($tdat){
	global $admindateformat;
	if($admindateformat==0)
		list($year, $month, $day) = sscanf($tdat, "%d-%d-%d");
	elseif($admindateformat==1)
		list($month, $day, $year) = sscanf($tdat, "%d/%d/%d");
	elseif($admindateformat==2)
		list($day, $month, $year) = sscanf($tdat, "%d/%d/%d");
	if(! is_numeric($year))
		$year = date("Y");
	elseif((int)$year < 39)
		$year = (int)$year + 2000;
	elseif((int)$year < 100)
		$year = (int)$year + 1900;
	if($year < 1970 || $year > 2038) $year = date("Y");
	if(! is_numeric($month))
		$month = date("m");
	if(! is_numeric($day))
		$day = date("d");
	return(mktime(0, 0, 0, $month, $day, $year));
}
function unstripslashes($slashedText){
	global $magicq;
	if($magicq)
		return stripslashes($slashedText);
	else
		return $slashedText;
}
function getattributes($attlist,$attid){
	$pos = strpos($attlist, $attid.'=');
	if($pos === false)
		return '';
	$pos += strlen($attid) + 1;
	$quote = $attlist[$pos];
	$pos2 = strpos($attlist, $quote, $pos + 1);
	$retstr = substr($attlist, $pos + 1, $pos2 - ($pos + 1));
	return($retstr); 
}
class vrNodeList{
	var $length;
	var $childNodes;
	var $nodeName;
	var $nodeValue;
	var $attributes;

	function createNodeList($xmlStr){
		$xLen = strlen($xmlStr);
		for($i=0; $i < $xLen; $i++){
			if(substr($xmlStr, $i, 1)=="<" && substr($xmlStr, $i+1, 1) != "/" && substr($xmlStr, $i+1, 1) != "?"){ // Got a tag
				$j = strpos($xmlStr,">",$i);
				$l = strpos($xmlStr," ",$i);
				if(is_integer($l) && $l < $j){
					$this->nodeName[$this->length]=substr($xmlStr,$i+1,$l-($i+1));
					$this->attributes[$this->length] = substr($xmlStr,$l+1,($j-$l)-1);
				}else
					$this->nodeName[$this->length]=substr($xmlStr,$i+1,$j-($i+1));
				// print "Got Node: " . $this->nodeName[$this->length] . "<br />\n";
				$k = $i+1;
				$nodeNameLen=strlen($this->nodeName[$this->length]);
				$currLev=0;
				while($k < $xLen && $currLev >= 0){
					if(substr($xmlStr, $k, 2)=="</"){
						if($currLev==0 && substr($xmlStr, $k+2, $nodeNameLen)==$this->nodeName[$this->length])
							break;
						$currLev--;
					}elseif(substr($xmlStr, $k, 1)=="<")
						$currLev++;
					elseif(substr($xmlStr, $k, 2)=="/>")
						$currLev--;
					$k++;
				}
				$this->nodeValue[$this->length]=substr($xmlStr,$j+1,$k-($j+1));
				// print "Got Value: xxx" . str_replace("<","<br />&lt;",$this->nodeValue[$this->length]) . "xxx<br />\n";
				$this->childNodes[$this->length] = new vrNodeList($this->nodeValue[$this->length]);
				$this->length++;
				$i = $k;
			}
		}
	}
	function vrNodeList($xmlStr){
		$this->length=0;
		$this->childNodes="";
		$this->createNodeList($xmlStr);
	}
	function getValueByTagName($tagname){
		for($i=0; $i < $this->length; $i++){
			//print "name: " . $this->nodeName[$i] . ", " . $this->nodeValue[$i] . "<br>";
			if($this->nodeName[$i]==$tagname){
				return($this->nodeValue[$i]);
			}else{
				if($this->childNodes!=''){
					if(($retval = $this->childNodes[$i]->getValueByTagName($tagname)) != NULL)
						return($retval);
				}
			}
		}
		return NULL;
	}
	function getAttributeByTagName($tagname, $attrib){
		for($i=0; $i < $this->length; $i++){
			if($this->nodeName[$i]==$tagname){
				return(getattributes($this->attributes[$i], $attrib));
			}else{
				if($this->childNodes!=''){
					if(($retval = $this->childNodes[$i]->getAttributeByTagName($tagname, $attrib)) != NULL)
						return($retval);
				}
			}
		}
		return NULL;
	}
}
class vrXMLDoc{
	var $tXMLStr;
	var $nodeList;

	function vrXMLDoc($xmlStr){
		$this->tXMLStr = $xmlStr;
		$this->nodeList = new vrNodeList($xmlStr);
	}

	function getElementsByTagName($tagname){
		$currlevel=0;
		$taglen = strlen($tagname);
	}
}
$netnav = TRUE;
if(strstr(@$HTTP_SERVER_VARS["HTTP_USER_AGENT"], "compatible") || strstr(@$HTTP_SERVER_VARS["HTTP_USER_AGENT"], "Gecko")) $netnav = FALSE;
function atb($size){
	global $netnav;
	if($netnav)
		return round($size / 2 + 1);
	else
		return $size;
}
$codestr="2952710692840328509902143349209039553396765";
function upsencode($thestr, $propcodestr){
	global $codestr;
	if($propcodestr=="") $localcodestr=$codestr; else $localcodestr=$propcodestr;
	$newstr="";
	for($index=0; $index < strlen($localcodestr); $index++){
		$thechar = substr($localcodestr,$index,1);
		if(! is_numeric($thechar)){
			$thechar = ord($thechar) % 10;
		}
		$newstr .= $thechar;
	}
	$localcodestr = $newstr;
	while(strlen($localcodestr) < 40)
		$localcodestr .= $localcodestr;
	$newstr="";
	for($index=0; $index < strlen($thestr); $index++){
		$thechar = substr($thestr,$index,1);
		$newstr .= chr(ord($thechar)+(int)substr($localcodestr,$index,1));
	}
	return $newstr;
}
function upsdecode($thestr, $propcodestr){
	global $codestr;
	if($propcodestr=="") $localcodestr=$codestr; else $localcodestr=$propcodestr;
	$newstr="";
	for($index=0; $index < strlen($localcodestr); $index++){
		$thechar = substr($localcodestr,$index,1);
		if(! is_numeric($thechar)){
			$thechar = ord($thechar) % 10;
		}
		$newstr .= $thechar;
	}
	$localcodestr = $newstr;
	while(strlen($localcodestr) < 40)
		$localcodestr .= $localcodestr;
	if(is_null($thestr)){
		return "";
	}else{
		$newstr="";
		for($index=0; $index < strlen($thestr); $index++){
			$thechar = substr($thestr,$index,1);
			$newstr .= chr(ord($thechar)-(int)substr($localcodestr,$index,1));
		}
		return($newstr);
	}
}
$locale_info = "";
function FormatEuroCurrency($amount){
	global $useEuro, $adminLocale, $locale_info, $overridecurrency, $orcsymbol, $orcdecplaces, $orcdecimals, $orcthousands, $orcpreamount;

	if(@$overridecurrency==TRUE){
		if($orcpreamount)
			return $orcsymbol . number_format($amount,$orcdecplaces,$orcdecimals,$orcthousands);
		else
			return number_format($amount,$orcdecplaces,$orcdecimals,$orcthousands) . $orcsymbol;
	}else{
		if(! is_array($locale_info)){
			setlocale(LC_MONETARY,$adminLocale);
			$locale_info = localeconv();
			setlocale(LC_MONETARY,"en_US");
		}
		if($useEuro)
			return number_format($amount,2,$locale_info["decimal_point"],$locale_info["thousands_sep"]) . " &euro;";
		else
			return $locale_info["currency_symbol"] . number_format($amount,2,$locale_info["decimal_point"],$locale_info["thousands_sep"]);
	}
}
function FormatEmailEuroCurrency($amount){
	global $useEuro, $adminLocale, $locale_info, $overridecurrency, $orcemailsymbol, $orcdecplaces, $orcdecimals, $orcthousands, $orcpreamount;

	if(@$overridecurrency==TRUE){
		if($orcpreamount)
			return $orcemailsymbol . number_format($amount,$orcdecplaces,$orcdecimals,$orcthousands);
		else
			return number_format($amount,$orcdecplaces,$orcdecimals,$orcthousands) . $orcemailsymbol;
	}else{
		if(! is_array($locale_info)){
			setlocale(LC_ALL,$adminLocale);
			$locale_info = localeconv();
			setlocale(LC_ALL,"en_US");
		}
		if($useEuro)
			return number_format($amount,2,$locale_info["decimal_point"],$locale_info["thousands_sep"]) . " Euro";
		else
			return $locale_info["currency_symbol"] . number_format($amount,2,$locale_info["decimal_point"],$locale_info["thousands_sep"]);
	}
}
//AFFILIATE
/*if(trim(@$_GET["PARTNER"]) != "" || trim(@$_GET["REFERER"]) != ""){
	if(@$expireaffiliate == "") $expireaffiliate=30;
	if(trim(@$_GET["PARTNER"])!="") $thereferer=trim(@$_GET["PARTNER"]); else $thereferer=trim(@$_GET["REFERER"]);
	print "<script src='/admin/savecookie.php?PARTNER=" . $thereferer . "&EXPIRES=" . $expireaffiliate . "'></script>";
}*/

//share a sale and commission junction affilates
if(trim(@$_GET["ifrogz_affiliate"]) != "" || trim(@$_GET["ifrogz_affiliate"]) != ""){
	if(@$expireaffiliate == "") $expireaffiliate=30;
	if(trim(@$_GET["ifrogz_affiliate"])!="") $thereferer=trim(@$_GET["ifrogz_affiliate"]); else $thereferer=trim(@$_GET["REFERER"]);
	print "<script src='/admin/savecookie.php?ifrogz_affiliate=" . $thereferer . "&EXPIRES=" . $expireaffiliate . "'></script>";
}

//GET COUPON CODE FROM EMAIL
/*if($_GET['utm_campaign'] == 'Free_Shipping_Dec_20_2007') {
	$_SESSION['os'] = 'freeshipping1220';
}*/
//echo '$_SESSION[os]='.$_SESSION['os'];



//end sas and cj
//$stockManage=0;
function do_stock_management($smOrdId){
	global $stockManage;
	if($stockManage != 0){
		$sSQL="SELECT cartID,cartProdID,cartQuantity,pSell, pInStock FROM cart INNER JOIN products ON cart.cartProdID=products.pID WHERE pDropship=0 AND (cartCompleted=0 OR cartCompleted=2) AND cartOrderID='" . mysql_real_escape_string(unstripslashes($smOrdId)) . "'";
		$result1 = mysql_query($sSQL) or print(mysql_error());
		while($rs1 = mysql_fetch_array($result1)){
			if(($rs1["pSell"] & 2) == 2){
				$sSQL = "SELECT coOptID, optStyleID, optStock, coOptGroup FROM cartoptions INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID WHERE (optType=2 OR optType=-2) AND coCartID=" . $rs1["cartID"];
				$result2 = mysql_query($sSQL) or print(mysql_error());
				while($rs2 = mysql_fetch_array($result2)){
					$newQty = $rs1["cartQuantity"];
					// If this option is a Custom Hype Rim, then make quantity double. (They are ordered as pairs, but inventory is kept individually)
					//  * Remember to change this in other places too ( release_stock(), getOnOrderStock(), and printpackingslips.php line 151 )
					if ($rs1['cartProdID'] == 'customhype' && strstr($rs2['coOptGroup'],'Rim')) { 
						$newQty = $rs1["cartQuantity"] * 2;
					}
					$sSQL = "UPDATE options SET optStock=optStock-" . $newQty . " WHERE optID=" . $rs2["coOptID"];
					mysql_query($sSQL) or print(mysql_error());
					
					//record change
					$empID = $_SESSION['employee']['id'];
					if (empty($empID)) {
						$empID = 81;
					}
					
					$sql="INSERT INTO inv_adjustments (iaOptID, iaProdStyle, iaAmt, iaDate, iaReason, iaEmpID, iaOldValue, iaNewValue, iaNotes) 
							VALUES ('".$rs2["coOptID"]."','".$rs1["cartProdID"]."-".$rs2["optStyleID"]."','-".$newQty."','".date('Y-m-d H:i:s')."','7','".$empID."',".$rs2["optStock"].",".($rs2["optStock"]-$rs1["cartQuantity"]).", 'ordID: $smOrdId')";
					//mail("chadsaun@gmail.com", "iFrogz Inv Test: Stock Manage", $sql);
					mysql_query($sql) or mail("chadsaun@gmail.com", "iFrogz Inv Test Error: Stock Manage", $sql);
				}
				mysql_free_result($result2);
			}else{
				$sSQL = "UPDATE products SET pInStock=pInStock-" . $rs1["cartQuantity"] . " WHERE pID='" . $rs1["cartProdID"] . "'";
				mysql_query($sSQL) or print(mysql_error());
				
				//record change
				$empID = $_SESSION['employee']['id'];
				if (empty($empID)) {
					$empID = 81;
				}
				
				$sql="INSERT INTO inv_adjustments (iaOptID,iaProdID,iaAmt,iaDate,iaReason,iaEmpID,iaOldValue,iaNewValue, iaNotes) 
						VALUES ('0','".$rs1["cartProdID"]."','-".$rs1["cartQuantity"]."','".date('Y-m-d H:i:s')."','7','".$empID."',".$rs1["pInStock"].",".($rs1["pInStock"]-$rs1["cartQuantity"]).", 'ordID: $smOrdId')";
				//mail("chadsaun@gmail.com", "iFrogz Inv Test: Stock Manage", $sql);
				mysql_query($sql) or mail("chadsaun@gmail.com", "iFrogz Inv Test Error: Stock Manage", $sql);
			}
		}
		mysql_free_result($result1);
	}
}

function release_stock($smOrdId) {
	global $stockManage;
	if($stockManage != 0){
		$sSQL="SELECT cartID,cartProdID,cartQuantity,pSell,pDownload,p_iscert, pInStock FROM cart INNER JOIN products ON cart.cartProdID=products.pID WHERE products.pDropship=0 AND cartCompleted=1 AND cartOrderID=" . $smOrdId;
		$result = mysql_query($sSQL) or print(mysql_error());
		while($rs = mysql_fetch_array($result)){
			if((($rs["pSell"] & 2) == 2)){
				$sSQL = "SELECT coOptID, coCartOption, optStyleID, optStock, coOptGroup FROM cartoptions INNER JOIN options ON cartoptions.coOptID=options.optID INNER JOIN optiongroup ON options.optGroup=optiongroup.optGrpID  WHERE (optType=2 OR optType=-2) AND coCartID=" . $rs["cartID"];
				$result2 = mysql_query($sSQL) or print(mysql_error());
				while($rs2 = mysql_fetch_array($result2)){
					$newQty = $rs["cartQuantity"];
					// If this option is a Custom Hype Rim, then make quantity double. (They are ordered as pairs, but inventory is kept individually)
					//  * Remember to change this in other places too ( do_stock_management(), getOnOrderStock(), and printpackingslips.php line 151 )
					if ($rs['cartProdID'] == 'customhype' && strstr($rs2['coOptGroup'],'Rim')) { 
						$newQty = $rs["cartQuantity"] * 2;
					}
					if (empty($newQty)) {
						$newQty = 0;
					}
					$sSQL = "UPDATE options SET optStock=optStock+" . $newQty . " WHERE optID=" . $rs2["coOptID"];
					mysql_query($sSQL) or print(mysql_error().$sSQL);	
					
					//record change
					$empID = $_SESSION['employee']['id'];
					if (empty($empID)) {
						$empID = 81;
					}
					
					$sql="INSERT INTO inv_adjustments (iaOptID, iaProdStyle, iaAmt, iaDate, iaReason, iaEmpID, iaOldValue, iaNewValue, iaNotes) 
						  VALUES ('".$rs2["coOptID"]."', '".$rs["cartProdID"]."-".$rs2["optStyleID"]."', '".$newQty."', '".date('Y-m-d H:i:s')."', '6', '".$empID."', ".$rs2["optStock"].", ".($rs2["optStock"]+$rs["cartQuantity"]).", 'ordID: $smOrdId')";
					//mail("chadsaun@gmail.com", "iFrogz Inv Test: Release Stock", $sql);
					mysql_query($sql) or mail("chadsaun@gmail.com", "iFrogz Inv Test Error: Release Stock", $sql);
				}
				mysql_free_result($result2);
			}else{
				$sSQL = "UPDATE products SET pInStock=pInStock+" . $rs["cartQuantity"] . " WHERE pID='" . $rs["cartProdID"] . "'";
				mysql_query($sSQL) or print(mysql_error());	
				
				//record change
				$empID = $_SESSION['employee']['id'];
				if (empty($empID)) {
					$empID = 81;
				}
				
				$sql="INSERT INTO inv_adjustments (iaOptID,iaProdID,iaAmt,iaDate,iaReason,iaEmpID,iaOldValue,iaNewValue, iaNotes) 
						VALUES ('0','".$rs["cartProdID"]."','".$rs["cartQuantity"]."','".date('Y-m-d H:i:s')."','6','".$empID."',".$rs["pInStock"].",".($rs["pInStock"]+$rs["cartQuantity"]).", 'ordID: $smOrdId')";
				//mail("chadsaun@gmail.com", "iFrogz Inv Test: Release Stock", $sql);
				mysql_query($sql) or mail("chadsaun@gmail.com", "iFrogz Inv Test Error: Release Stock", $sql);
			}
		}
		mysql_free_result($result);
	}
}
//END
function productdisplayscript($doaddprodoptions){
global $prodoptions, $countryTax, $xxPrdEnt, $xxPrdChs, $xxPrd255, $xxOptOOS, $useStockManagement, $prodlist, $OWSP;
global $currSymbol1,$currFormat1,$currSymbol2,$currFormat2,$currSymbol3,$currFormat3;
if($currSymbol1!="" && $currFormat1=="") $currFormat1='%s <strong>' . $currSymbol1 . '</strong>';
if($currSymbol2!="" && $currFormat2=="") $currFormat2='%s <strong>' . $currSymbol2 . '</strong>';
if($currSymbol3!="" && $currFormat3=="") $currFormat3='%s <strong>' . $currSymbol3 . '</strong>';
?>
<script language="JavaScript" type="text/javascript">
<!--
var aPC = new Array();<?php
		if($useStockManagement){ ?>
var aPS = new Array();
checkStock=function(x, i){
if(i!='' && aPS[i] > 0)return(true);
alert('<?php print str_replace("'","\'",$xxOptOOS)?>');
x.focus();return(false);
}<?php	} ?>

var isW3 = (document.getElementById&&true);
var tax=<?php print $countryTax ?>;
dummyfunc=function(){};
<?php
$prodoptions="";
if($doaddprodoptions && $prodlist != ""){
	$sSQL = "SELECT DISTINCT optID," . $OWSP . "optPriceDiff,optStock,optDisplay_point FROM options INNER JOIN prodoptions ON options.optGroup=prodoptions.poOptionGroup WHERE prodoptions.poProdID IN (" . $prodlist . ")";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rowcounter=0;
	while($row = mysql_fetch_array($result)){
		if($useStockManagement) print 'aPS[' . $row["optID"] . ']=' . ($row["optStock"]) . ';';
		print "aPC[". $row["optID"] . "]=" . $row["optPriceDiff"] . ";";
		if(($rowcounter % 10)==9) print "\r\n";
		$rowcounter++;
	}
	print "\r\n";
}
?>
pricechecker=function(i){
if(i!='')return(aPC[i]);return(0);}
enterValue=function(x){
alert('<?php print str_replace("'","\'",$xxPrdEnt)?>');
x.focus();return(false);}
chooseOption=function(x){
alert('<?php print str_replace("'","\'",$xxPrdChs)?>');
x.focus();return(false);}
dataLimit=function(x){
alert('<?php print str_replace("'","\'",$xxPrd255)?>');
x.focus();return(false);}
formatprice=function(i, currcode, currformat){
<?php
	$tempStr = FormatEuroCurrency(0);
	$tempStr2 = number_format(0,2,".",",");
	print "var pTemplate='" . $tempStr . "';\n";
	print "if(currcode!='') pTemplate=' " . $tempStr2 . "' + (currcode!=' '?'<strong>'+currcode+'<\/strong>':'');";
	if(strstr($tempStr,",") || strstr($tempStr,".")){ ?>
if(currcode==' JPY')i = Math.round(i).toString();
else if(i==Math.round(i))i=i.toString()+".00";
else if(i*10.0==Math.round(i*10.0))i=i.toString()+"0";
else if(i*100.0==Math.round(i*100.0))i=i.toString();
<?php }
	print 'if(currcode!="")pTemplate = currformat.toString().replace(/%s/,i.toString());';
	print 'else pTemplate = pTemplate.toString().replace(/\d[,.]*\d*/,i.toString());';
	if(strstr($tempStr,","))
		print "return(pTemplate.replace(/\./,','));";
	else
		print "return(pTemplate);";
?>}
openEFWindow= function(id) {
window.open('/emailfriend.php?utm_source=emailfriend&id='+id,'email_friend','menubar=no, scrollbars=no, width=400, height=460, directories=no,location=no,resizable=yes,status=no,toolbar=no')
}
//-->
</script><?php
}
function updatepricescript($doaddprodoptions){
global $prodoptions,$Count,$rs,$WSP,$noprice,$pricezeromessage,$showtaxinclusive,$currRate1,$currRate2,$currRate3,$currSymbol1,$currSymbol2,$currSymbol3,$currFormat1,$currFormat2,$currFormat3,$useStockManagement,$currencyseparator; 
   $pPrice_adj=1;
   if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs["pPricing_group"]);
?>
<script language="JavaScript" type="text/javascript">
<!--
formvalidator<?php print $Count?>= function(theForm){
<?php
$prodoptions="";
$hasonepriceoption=FALSE;
if($doaddprodoptions){
	$sSQL = "SELECT poOptionGroup,optType,optFlags FROM prodoptions LEFT JOIN optiongroup ON optiongroup.optGrpID=prodoptions.poOptionGroup WHERE poProdID='" . $rs["pId"] . "' ORDER BY poID";
	$result = mysql_query($sSQL) or print(mysql_error());
	for($rowcounter=0;$rowcounter<mysql_num_rows($result);$rowcounter++){
		$prodoptions[$rowcounter] = mysql_fetch_array($result);
	}
	if(is_array($prodoptions)){
		foreach($prodoptions as $rowcounter => $theopt){
			if($theopt["optType"]==3){
				print "if(theForm.voptn" . $rowcounter . ".value=='')return(enterValue(theForm.voptn" . $rowcounter . "));\n";
				print "if(theForm.voptn" . $rowcounter . ".value.length>255)return(dataLimit(theForm.voptn" . $rowcounter . "));\n";
			}elseif(abs($theopt["optType"])==2){
				$hasonepriceoption=TRUE;
				if($theopt["optType"]==2)
					print 'if(theForm.optn' . $rowcounter . '.selectedIndex==0)return(chooseOption(theForm.optn' . $rowcounter . "));\n";
				if($useStockManagement && (($rs["pSell"] & 2) == 2)) print 'if(!checkStock(theForm.optn' . $rowcounter . ',theForm.optn' . $rowcounter . '.options[theForm.optn' . $rowcounter . '.selectedIndex].value))return(false);' . "\r\n";
			}
		}
	}
}
if(@$customvalidator != "") print $customvalidator;
?>return (true);
}
<?php

if(@$noprice!=TRUE && ! ($rs["pPrice"]==0 && @$pricezeromessage != "") && $hasonepriceoption){
	print 'updateprice' . $Count . "= function(){\r\n";
	print 'var totAdd=' . ($rs["pPrice"]*$pPrice_adj) . ";\r\n";
	print 'if(!isW3) return;';
	foreach($prodoptions as $rowcounter => $theopt){
		if(abs($theopt["optType"])!=3){
			if(($theopt["optFlags"]&1)==1)
				print 'totAdd=totAdd+((' . ($rs["pPrice"]*$pPrice_adj) . "*pricechecker(document.forms.tForm" . $Count . ".optn" . $rowcounter . ".options[document.forms.tForm" . $Count . ".optn" . $rowcounter . ".selectedIndex].value))/100.0);\n";
			else
				print 'totAdd=totAdd+pricechecker(document.forms.tForm' . $Count . ".optn" . $rowcounter . ".options[document.forms.tForm" . $Count . ".optn" . $rowcounter . ".selectedIndex].value);\n";
		}
	}
	print "document.getElementById('pricediv" . $Count . "').innerHTML=formatprice(Math.round(totAdd*100.0)/100.0, '', '');\r\n";
	if(@$showtaxinclusive && ($rs["pExemptions"] & 2)!=2) print "document.getElementById('pricedivti" . $Count . "').innerHTML=formatprice(Math.round((totAdd+(totAdd*tax/100.0))*100.0)/100.0, '', '');\n";
	$extracurr = "";
	if($currRate1!=0 && $currSymbol1!="") $extracurr = "+formatprice(Math.round((totAdd*" . $currRate1 . ")*100.0)/100.0, ' " . $currSymbol1 . "','" . str_replace("'","\'",$currFormat1) . "')+'".str_replace("'","\'",$currencyseparator)."'\n";
	if($currRate2!=0 && $currSymbol2!="") $extracurr .= "+formatprice(Math.round((totAdd*" . $currRate2 . ")*100.0)/100.0, ' " . $currSymbol2 . "','" . str_replace("'","\'",$currFormat2) . "')+'".str_replace("'","\'",$currencyseparator)."'\n";
	if($currRate3!=0 && $currSymbol3!="") $extracurr .= "+formatprice(Math.round((totAdd*" . $currRate3 . ")*100.0)/100.0, ' " . $currSymbol3 . "','" . str_replace("'","\'",$currFormat3) . "');\n";
	if($extracurr!="") print "document.getElementById('pricedivec" . $Count . "').innerHTML=''" . $extracurr . "\r\n";
	print "}";
}
?>//-->
</script><?php
}

//////////////
//// added by blake for auto add products
function productdisplayscript2($doaddprodoptions){
global $prodoptions, $countryTax, $xxPrdEnt, $xxPrdChs, $xxPrd255, $xxOptOOS, $useStockManagement, $prodlist, $OWSP;
global $currSymbol1,$currFormat1,$currSymbol2,$currFormat2,$currSymbol3,$currFormat3;
if($currSymbol1!="" && $currFormat1=="") $currFormat1='%s <strong>' . $currSymbol1 . '</strong>';
if($currSymbol2!="" && $currFormat2=="") $currFormat2='%s <strong>' . $currSymbol2 . '</strong>';
if($currSymbol3!="" && $currFormat3=="") $currFormat3='%s <strong>' . $currSymbol3 . '</strong>';

?>
<script language="JavaScript" type="text/javascript">

var taPC = new Array();<?php
		if($useStockManagement){ ?>
var taPS = new Array();
tcheckStock=function(x, i){
if(i!='' && taPS[i] > 0)return(true);
alert('<?php print str_replace("'","\'",$xxOptOOS)?>');
x.focus();return(false);
}<?php	} ?>

 tisW3 = (document.getElementById&&true);
//alert(tisW3);
var ttax=<?php print $countryTax ?>;
tdummyfunc=function(){};
<?php
$prodoptions="";

if($doaddprodoptions && $prodlist != ""){
	$sSQL = "SELECT DISTINCT optID," . $OWSP . "optPriceDiff,optStock,optDisplay_point 
			 FROM options INNER JOIN prodoptions ON options.optGroup=prodoptions.poOptionGroup 
			 WHERE prodoptions.poProdID IN (" . $prodlist . ")";
	$result = mysql_query($sSQL) or print(mysql_error());
	$rowcounter=0;
	while($row = mysql_fetch_array($result)){
		if ($useStockManagement) {
			print 'taPS[' . $row["optID"] . ']=' . ($row["optStock"]) . ';';
		}
		print "taPC[". $row["optID"] . "]=" . $row["optPriceDiff"] . ";";
		if(($rowcounter % 10)==9) print "\r\n";
		$rowcounter++;
	}
	print "\r\n";
}
?>
tpricechecker=function(i){
if(i!='')return(taPC[i]);return(0);}
tenterValue=function(x){
alert('<?php print str_replace("'","\'",$xxPrdEnt)?>');
x.focus();return(false);}
tchooseOption=function(x){
alert('<?php print str_replace("'","\'",$xxPrdChs)?>');
x.focus();return(false);}
tdataLimit=function(x){
alert('<?php print str_replace("'","\'",$xxPrd255)?>');
x.focus();return(false);}
tformatprice=function(i, currcode, currformat){
<?php
	$tempStr = FormatEuroCurrency(0);
	$tempStr2 = number_format(0,2,".",",");
	print "var pTemplate='" . $tempStr . "';\n";
	print "if(currcode!='') pTemplate=' " . $tempStr2 . "' + (currcode!=' '?'<strong>'+currcode+'<\/strong>':'');";
	if(strstr($tempStr,",") || strstr($tempStr,".")){ ?>
if(currcode==' JPY')i = Math.round(i).toString();
else if(i==Math.round(i))i=i.toString()+".00";
else if(i*10.0==Math.round(i*10.0))i=i.toString()+"0";
else if(i*100.0==Math.round(i*100.0))i=i.toString();
<?php }
	print 'if(currcode!="")pTemplate = currformat.toString().replace(/%s/,i.toString());';
	print 'else pTemplate = pTemplate.toString().replace(/\d[,.]*\d*/,i.toString());';
	if(strstr($tempStr,","))
		print "return(pTemplate.replace(/\./,','));";
	else
		print "return(pTemplate);";
?>}
topenEFWindow= function(id) {
window.open('/emailfriend.php?utm_source=emailfriend&id='+id,'email_friend','menubar=no, scrollbars=no, width=400, height=460, directories=no,location=no,resizable=yes,status=no,toolbar=no')
}

</script><?php
}

function updatepricescript2($doaddprodoptions){
global $prodoptions,$Count,$rs,$WSP,$noprice,$pricezeromessage,$showtaxinclusive,$currRate1,$currRate2,$currRate3,$currSymbol1,$currSymbol2,$currSymbol3,$currFormat1,$currFormat2,$currFormat3,$useStockManagement,$currencyseparator; 
   $pPrice_adj=1;
   if(!empty($WSP)) $pPrice_adj=getPricingAdj($_SESSION['custID'],1,$rs["pPricing_group"]);
?>
<script language="JavaScript" type="text/javascript">

tformvalidator<?php print $Count?>= function(theForm){
<?php
$prodoptions="";
$hasonepriceoption=FALSE;
if($doaddprodoptions){
	$sSQL = "SELECT poOptionGroup,optType,optFlags FROM prodoptions LEFT JOIN optiongroup ON optiongroup.optGrpID=prodoptions.poOptionGroup WHERE poProdID='" . $rs["pId"] . "' ORDER BY poID";
	
	$result = mysql_query($sSQL) or print(mysql_error());
	for($rowcounter=0;$rowcounter<mysql_num_rows($result);$rowcounter++){
		$prodoptions[$rowcounter] = mysql_fetch_array($result);
	}
	if(is_array($prodoptions)){
		foreach($prodoptions as $rowcounter => $theopt){
			if($theopt["optType"]==3){
				print "if(theForm.voptn" . $rowcounter . ".value=='')return(tenterValue(theForm.voptn" . $rowcounter . "));\n";
				print "if(theForm.voptn" . $rowcounter . ".value.length>255)return(tdataLimit(theForm.voptn" . $rowcounter . "));\n";
			}elseif(abs($theopt["optType"])==2){
				$hasonepriceoption=TRUE;
				if($theopt["optType"]==2)
					print 'if(theForm.optn_' .$Count. '_' . $rowcounter . '.selectedIndex==0)return(tchooseOption(theForm.optn_' .$Count. '_' . $rowcounter . "));\n";
				if($useStockManagement && (($rs["pSell"] & 2) == 2)) print 'if(!tcheckStock(theForm.optn_' .$Count. '_' . $rowcounter . ',theForm.optn_' .$Count. '_' . $rowcounter . '.options[theForm.optn_' .$Count. '_' . $rowcounter . '.selectedIndex].value))return(false);' . "\r\n";
			}
		}
	}
}
if(@$customvalidator != "") print $customvalidator;
?>return (true);
}
<?php

if(@$noprice!=TRUE && ! ($rs["pPrice"]==0 && @$pricezeromessage != "") && $hasonepriceoption){
	print 'tupdateprice' . $Count . "= function(){\r\n";
	print 'var totAdd=' . ($rs["pPrice"]*$pPrice_adj) . ";\r\n";
	print 'if(!tisW3) return;';
	foreach($prodoptions as $rowcounter => $theopt){
		if(abs($theopt["optType"])!=3){
			if(($theopt["optFlags"]&1)==1)
				print 'totAdd=totAdd+((' . ($rs["pPrice"]*$pPrice_adj) . "*tpricechecker(document.forms.tForm" . $Count . ".optn_" .$Count. "_" . $rowcounter . ".options[document.forms.tForm" . $Count . ".optn_" .$Count. "_" . $rowcounter . ".selectedIndex].value))/100.0);\n";
			else
				print 'totAdd=totAdd+tpricechecker(document.forms.tForm' . $Count . ".optn_" .$Count. "_" . $rowcounter . ".options[document.forms.tForm" . $Count . ".optn_" .$Count. "_" . $rowcounter . ".selectedIndex].value);\n";
		}
	}
	print "document.getElementById('pricediv" . $Count . "').innerHTML=tformatprice(Math.round(totAdd*100.0)/100.0, '', '');\r\n";
	if(@$showtaxinclusive && ($rs["pExemptions"] & 2)!=2) print "document.getElementById('pricedivti" . $Count . "').innerHTML=tformatprice(Math.round((totAdd+(totAdd*tax/100.0))*100.0)/100.0, '', '');\n";
	$extracurr = "";
	if($currRate1!=0 && $currSymbol1!="") $extracurr = "+tformatprice(Math.round((totAdd*" . $currRate1 . ")*100.0)/100.0, ' " . $currSymbol1 . "','" . str_replace("'","\'",$currFormat1) . "')+'".str_replace("'","\'",$currencyseparator)."'\n";
	if($currRate2!=0 && $currSymbol2!="") $extracurr .= "+tformatprice(Math.round((totAdd*" . $currRate2 . ")*100.0)/100.0, ' " . $currSymbol2 . "','" . str_replace("'","\'",$currFormat2) . "')+'".str_replace("'","\'",$currencyseparator)."'\n";
	if($currRate3!=0 && $currSymbol3!="") $extracurr .= "+tformatprice(Math.round((totAdd*" . $currRate3 . ")*100.0)/100.0, ' " . $currSymbol3 . "','" . str_replace("'","\'",$currFormat3) . "');\n";
	if($extracurr!="") print "document.getElementById('pricedivec" . $Count . "').innerHTML=''" . $extracurr . "\r\n";
	print "}";
}
?>
</script><?php
}

function checkRelatedDiscounts(){
	global $WSP;
	$newprice="";
	$sSQL = "SELECT cartID,cartProdID,cartAltPrice,cartProdPrice,cartQuantity FROM cart WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "'";
	$total_in_cart=0;	
	$result = mysql_query($sSQL) or print(mysql_error());
	$i=0;
	while($rs=mysql_fetch_assoc($result)){
			$reldisc[$i]=$rs;
			$reldisc[$i][$rs['cartProdID']]['rel_disc']+=$rs['cartQuantity'];
						
		$i++;
	}
	$total_in_cart=calculateTotal();
		//echo "<br />Total=".$total_in_cart;
	$cnt=count($reldisc);
	for($j=0;$j<$cnt;$j++){
		
		$sql_mini="SELECT * 
				   FROM related_discounts rd 
				   LEFT JOIN products p ON rd.relProd=p.pID 
				   WHERE relProd='".$reldisc[$j]['cartProdID']."'
				   AND IF(relBegin != '0000-00-00 00:00:00', relBegin <= '" . date('Y-m-d H:i:s') . "', 1=1) AND IF(relEnd != '0000-00-00 00:00:00', relEnd > '" . date('Y-m-d H:i:s') . "', 1=1)";
		//$sql_mini.= " AND relCartTotal+pPrice<=".$total_in_cart;
		//echo $sql_mini;
		$result_mini=mysql_query($sql_mini);	
		$num_rows=mysql_num_rows($result_mini);
		if($num_rows>0) {
			$rs_mini=mysql_fetch_assoc($result_mini);
			//echo $rs_mini['relCartTotal'].'<='.($total_in_cart.' - '.$reldisc[$j]['cartProdPrice']);
			if ($rs_mini['relPrice']>-1) {
				if (($rs_mini['relCartTotal']<=$total_in_cart-$reldisc[$j]['cartProdPrice']) && $rs_mini['relNumber']>=($reldisc[$j][$rs_mini['relProd']]['rel_disc']+$prodcnt[$rs_mini['relProd']])){
					//echo "<br />".'Test'.$rs_mini['relNumber'].'<'.$reldisc[$j][$reldisc[$j]['cartProdID']]['rel_disc']."<br />";
					$theprice = $rs_mini['relPrice'];
					$cartAltPrice = $rs_mini['relPrice'];
					//echo '$rs_mini[relPrice]='.$rs_mini['relPrice']; 
					//echo '$prodcnt[$rs_mini[relProd]]='.$prodcnt[$reldisc[$j]['cartProdID']]."<br />";
					$prodcnt[$rs_mini['relProd']]++;
				} else {			
					$sql_p = "SELECT pPrice FROM products WHERE pID='".$reldisc[$j]['cartProdID']."'";
					$result = mysql_query($sql_p);
					$rs_p = mysql_fetch_assoc($result);
					//if($reldisc[$i]['cartProdPrice']!=$rs_p['pPrice'])$theprice=$reldisc[$j]['cartProdPrice'];
					//else 
					$theprice = $rs_p['pPrice'];				
					$cartAltPrice = -1;
				}	
				//echo "<br />".$reldisc[$j]['cartProdID'].'-'.$theprice."-".$reldisc[$j]['cartQuantity']."<br />";
				
				$sSQL = "UPDATE cart SET cartProdPrice='".$theprice."', cartAltPrice='".$cartAltPrice."' WHERE cartCompleted=0 AND cartSessionID='" . session_id() . "' AND cartID='".mysql_real_escape_string($reldisc[$j]['cartID'])."'";
				//echo $sSQL;
				mysql_query($sSQL) or print(mysql_error().$sSQL);
			}
		}	
	}
}

function calculateTotal(){
	//global $totaldiscounts;
	$sSQL = "SELECT SUM(cartProdPrice*cartQuantity) AS totalPrice FROM cart c LEFT JOIN products p ON c.cartProdID=p.pID WHERE cartCompleted=0 AND  cartSessionID='" . session_id() . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs=mysql_fetch_assoc($result)){
	 	//echo "<br />======".$rs['totalPrice'].'-'.$_SESSION["discounts"]."<br />";
		return $rs['totalPrice']-$_SESSION["discounts"];
	}
}
///// end auto add ////

function checkDPs($currcode){
	if($currcode=="JPY") return(0); else return(2);
}
function checkCurrencyRates($currConvUser,$currConvPw,$currLastUpdate,&$currRate1,$currSymbol1,&$currRate2,$currSymbol2,&$currRate3,$currSymbol3){
	global $countryCurrency,$usecurlforfsock,$pathtocurl,$curlproxy;
	$ccsuccess = true;
	if($currConvUser!="" && $currConvPw!="" && (strtotime($currLastUpdate) < time()-(60*60*24))){
		$str = "";
		if($currSymbol1!="") $str .= "&curr=" . $currSymbol1;
		if($currSymbol2!="") $str .= "&curr=" . $currSymbol2;
		if($currSymbol3!="") $str .= "&curr=" . $currSymbol3;
		if($str==""){
			mysql_query("UPDATE admin SET currLastUpdate='" . date("Y-m-d H:i:s", time()) . "'") or print(mysql_error());
			return;
		}
		$str = "?source=" . $countryCurrency . "&user=" . $currConvUser . "&pw=" . $currConvPw . $str;
		if(@$usecurlforfsock){
			if(@$pathtocurl != ""){
				exec($pathtocurl . ' --data-binary \'' . str_replace("'","\'","X") . '\' http://www.ecommercetemplates.com/currencyxml.asp' . $str, $res, $retvar);
				$sXML = implode("\n",$res);
			}else{
				if (!$ch = curl_init()) {
					$success = false;
					$errormsg = "cURL package not installed in PHP";
					$ccsuccess = FALSE;
				}else{
					curl_setopt($ch, CURLOPT_URL,'http://www.ecommercetemplates.com/currencyxml.asp' . $str); 
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_POSTFIELDS, "X");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					if(@$curlproxy!=''){
						curl_setopt($ch, CURLOPT_PROXY, $curlproxy);
					}
					$sXML = curl_exec($ch);
					if(curl_error($ch) != "") print "Error with cURL installation: " . curl_error($ch) . "<br />";
					curl_close($ch);
				}
			}
		}else{
			$header = "POST /currencyxml.asp" . $str . " HTTP/1.0\r\n";
			$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
			$header .= "Content-Length: 1\r\n\r\n";
			$fp = fsockopen ('www.ecommercetemplates.com', 80, $errno, $errstr, 30);
			if (!$fp){
				echo "$errstr ($errno)"; // HTTP error handling
				$ccsuccess = FALSE;
			}else{
				fputs ($fp, $header . "X");
				$sXML="";
				while (!feof($fp))
					$sXML .= fgets ($fp, 1024);
			}
		}
		if($ccsuccess){
			// print str_replace("<","<br />&lt;",$sXML) . "<br />\n";
			$xmlDoc = new vrXMLDoc($sXML);
			$nodeList = $xmlDoc->nodeList->childNodes[0];
			for($j = 0; $j < $nodeList->length; $j++){
				if($nodeList->nodeName[$j]=="currError"){
					print $nodeList->nodeValue[$j];
					$ccsuccess = false;
				}elseif($nodeList->nodeName[$j]=="selectedCurrency"){
					$e = $nodeList->childNodes[$j];
					$currRate = 0;
					for($i = 0; $i < $e->length; $i++){
						if($e->nodeName[$i]=="currSymbol")
							$currSymbol = $e->nodeValue[$i];
						elseif($e->nodeName[$i]=="currRate")
							$currRate = $e->nodeValue[$i];
					}
					if($currSymbol1 == $currSymbol){
						$currRate1 = $currRate;
						mysql_query("UPDATE admin SET currRate1=" . $currRate . " WHERE adminID=1") or print(mysql_error());
					}
					if($currSymbol2 == $currSymbol){
						$currRate2 = $currRate;
						mysql_query("UPDATE admin SET currRate2=" . $currRate . " WHERE adminID=1") or print(mysql_error());
					}
					if($currSymbol3 == $currSymbol){
						$currRate3 = $currRate;
						mysql_query("UPDATE admin SET currRate3=" . $currRate . " WHERE adminID=1") or print(mysql_error());
					}
				}
			}
			if($ccsuccess) mysql_query("UPDATE admin SET currLastUpdate='" . date("Y-m-d H:i:s", time()) . "'");
		}
	}
}
function getsectionids($thesecid, $delsections){
	$secid = $thesecid;
	$iterations = 0;
	$iteratemore = TRUE;
	if(@$_SESSION["clientLoginLevel"] != "") $minloglevel=$_SESSION["clientLoginLevel"]; else $minloglevel=0;
	if($delsections) $nodel = ""; else $nodel = 'sectionDisabled<=' . $minloglevel . ' AND ';
	while($iteratemore && $iterations<10){
		$sSQL2 = "SELECT DISTINCT sectionID,rootSection FROM sections WHERE " . $nodel . "(topSection IN (" . $secid . ") OR (sectionID IN (" . $secid . ") AND rootSection=1))";
		$secid = "";
		$iteratemore = FALSE;
		$result2 = mysql_query($sSQL2) or print(mysql_error());
		$addcomma = "";
		while($rs2 = mysql_fetch_assoc($result2)){
			if($rs2["rootSection"]==0) $iteratemore = TRUE;
			$secid .= $addcomma . $rs2["sectionID"];
			$addcomma = ",";
		}
		$iterations++;
	}
	if($secid=="") $secid = "0";
	return($secid);
}
function getpayprovdetails($ppid,&$ppdata1,&$ppdata2,&$ppdata3,&$ppdemo,&$ppmethod){
	$sSQL = "SELECT payProvData1,payProvData2,payProvData3,payProvDemo,payProvMethod FROM payprovider WHERE payProvEnabled=1 AND payProvID='" . mysql_real_escape_string($ppid) . "'";
	$result = mysql_query($sSQL) or print(mysql_error());
	if($rs = mysql_fetch_assoc($result)){
		$ppdata1 = trim($rs['payProvData1']);
		$ppdata2 = trim($rs['payProvData2']);
		$ppdata3 = trim($rs['payProvData3']);
		$ppdemo = ((int)$rs['payProvDemo']==1);
		$ppmethod = (int)$rs['payProvMethod'];
	}else
		return(FALSE);
	return(TRUE);
}
function callcurlfunction($cfurl, $cfxml, &$cfres, &$cfcert, &$cferrmsg, $settimeouts){
	global $curlproxy,$pathtocurl;
	$cfsuccess=TRUE;
	//print str_replace("<","<br />&lt;",str_replace("</","&lt;/",$cfxml)) . "<br />\n";
	if(@$pathtocurl != ""){
		exec($pathtocurl . ($cfcert != '' ? ' -E \'' . $cfcert . '\'' : '') . ' --data-binary \'' . str_replace("'","\'",$cfxml) . '\' ' . $cfurl, $cfres, $retvar);
		$cfres = implode("\n",$cfres);
	}else{
		if (!$ch = curl_init()) {
			$cferrmsg = "cURL package not installed in PHP. Set \$pathtocurl parameter.";
			$cfsuccess=FALSE;
		}else{
			curl_setopt($ch, CURLOPT_URL, $cfurl);
			if($cfcert != '') curl_setopt($ch, CURLOPT_SSLCERT, $cfcert); 
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $cfxml);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			if($settimeouts) curl_setopt($ch, CURLOPT_TIMEOUT, 120);
			if(@$curlproxy!=''){
				curl_setopt($ch, CURLOPT_PROXY, $curlproxy);
			}
			$cfres = curl_exec($ch);
			//print str_replace("<","<br />&lt;",str_replace("</","&lt;/",$cfres)) . "<br />\n";
			if(curl_error($ch) != ""){
				if($cfcert != '' && ! file_exists($cfcert)){
					$cferrmsg='Certificate file not found: ' . $cfcert . '<br />';
				}else
					$cferrmsg='cURL error: ' . curl_error($ch) . '<br />';
				$cfsuccess=FALSE;
			}else{
				curl_close($ch);
			}
		}
	}
	return($cfsuccess);
}
function writehiddenvar($hvname,$hvval){
print '<input type="hidden" name="' . $hvname . '" id="' . $hvname . '" value="' . str_replace('"','&quot;',$hvval) . '" />' . "\r\n";
}
function ppsoapheader($username, $password){
return '<?xml version="1.0" encoding="utf-8"?>' .
	'<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' .
	'  <soap:Header>' .
	'    <RequesterCredentials xmlns="urn:ebay:api:PayPalAPI">' .
	'      <Credentials xmlns="urn:ebay:apis:eBLBaseComponents">' .
	'        <Username>' . $username . '</Username>' .
	'        <ebl:Password xmlns:ebl="urn:ebay:apis:eBLBaseComponents">' . $password . '</ebl:Password>' .
	'      </Credentials>' .
	'    </RequesterCredentials>' .
	'  </soap:Header>';
}
if(@$enableclientlogin==TRUE){
	if(@$_SESSION["clientUser"] != ""){
	}elseif(@$_POST["checktmplogin"]=="1" && @$_POST["sessionid"] != ""){
		$sSQL = "SELECT tmploginname FROM tmplogin WHERE tmploginid='" . trim(@$_POST["sessionid"]) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$_SESSION["clientUser"]=$rs["tmploginname"];
			mysql_free_result($result);
			mysql_query("DELETE FROM tmplogin WHERE tmploginid='" . trim(@$_POST["sessionid"]) . "'") or print(mysql_error());
			$sSQL = "SELECT clientActions,clientLoginLevel,clientPercentDiscount FROM clientlogin WHERE clientUser='" . $_SESSION["clientUser"] . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result)){
				$_SESSION["clientActions"]=$rs["clientActions"];
				$_SESSION["clientLoginLevel"]=$rs["clientLoginLevel"];
				$_SESSION["clientPercentDiscount"]=(100.0-(double)$rs["clientPercentDiscount"])/100.0;
			}
		}
		mysql_free_result($result);
	}elseif(@$_COOKIE["WRITECLL"] != ""){
		$sSQL = "SELECT clientUser,clientActions,clientLoginLevel,clientPercentDiscount FROM clientlogin WHERE clientUser='" . trim($_COOKIE["WRITECLL"]) . "' AND clientPW='" . trim($_COOKIE["WRITECLP"]) . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		if($rs = mysql_fetch_array($result)){
			$_SESSION["clientUser"]=$rs["clientUser"];
			$_SESSION["clientActions"]=$rs["clientActions"];
			$_SESSION["clientLoginLevel"]=$rs["clientLoginLevel"];
			$_SESSION["clientPercentDiscount"]=(100.0-(double)$rs["clientPercentDiscount"])/100.0;
		}
		mysql_free_result($result);
	}
	if(@$requiredloginlevel != ""){
		if((int)$requiredloginlevel > @$_SESSION["clientLoginLevel"]){
			ob_end_clean();
			if(@$_SERVER["HTTPS"] == "on" || @$_SERVER["SERVER_PORT"] == "443")$prot='https://';else $prot='http://';
			header('Location: /admin/clientlogin.php?refurl=' . urlencode(@$_SERVER["PHP_SELF"] . (@$_SERVER["QUERY_STRING"] !="" ? "?" . @$_SERVER["QUERY_STRING"] : "")));
			exit();
		}
	}
}

function isPermitted($permission = 'all') {
	global $_SESSION;

	$mappings = array(
	    'accounting' => 'i_f_t',
    	'admin' => 'i_f_a',
    	'all' => 'all',
    	'customer service' => 'i_f_c',
    	'customer service admin' => 'i_f_ca', // deprecated
    	'hong kong' => 'i_f_h',
	    'inventory' => 'i_f_v',
    	'it' => 'i_f_i',
    	'management' => 'i_f_m',
    	'nadal' => 'i_f_n', // deprecated
    	'product' => 'i_f_p',
    	'quality control' => 'i_f_qc',
    	'reports_retail' => 'i_f_rr',
	    'sales' => 'i_f_sa',
    	'shieldzone' => 'i_f_z', // deprecated
    	'shipping' => 'i_f_s',
    );

	$permission = strtolower($permission);

    if (isset($mappings[$permission])) {
        $permission = $mappings[$permission];
    }

    if (isset($_SESSION['employee']['permissions'])) {
        $roles = preg_split('/,+/', $_SESSION['employee']['permissions']);
    	$length = count($roles);
    	for ($i = 0; $i < $length; $i++) {
    		$roles[$i] = trim($roles[$i]);
    		if (!empty($roles[$i]) && ($roles[$i] == $permission)) {
    			return TRUE;
    		}
    	}
    }

	return FALSE;
}

function showarray($array)
{
	echo '<ul>';
	foreach($array as $k=>$v)
	{
		if(is_array($v))
		{
			echo '<li>K:'.$k.'</li>';
			showarray($v);
		}
		else
		{
			echo '<li>'.$k.'='.$v.'</li>';
		}
	}
	echo '</ul>';
}
// END ADDED

// ADDED by Chad (Mar30,06) set new location
//  + location = INT
//  + ordID = INT
function setNewLocation($location,$ordID,$type='Automatic',$reason='') {
	global $_SESSION;
	
	if(empty($ordID)) {
		return false;
	}
	
	$qry = "SELECT statPrivate FROM orderstatus WHERE statID = $location";
	if(!$res = mysql_query($qry)) {
		echo '1';
		return false;
	}
	if(!$row = mysql_fetch_assoc($res)) {
		echo '2';
		return false;
	}
	$locName = $row['statPrivate'];
	mysql_free_result($res);
	
	$qry = "INSERT INTO location ( ordID , employeeID , stamp , statNum , location , locType , locTypeReason )
			VALUES ( $ordID , '".$_SESSION["employee"]["id"]."' , '".date("Y-m-d H:i:s")."' , '".$location."' , 
					 '$locName' , '$type' , '$reason' )";
	if(!$res = mysql_query($qry)) {
		echo '3-'.mysql_error().'<br />'.$qry;
		return false;
	}
	
	return true;
}
// END ADDED

// ADDED by Chad (Apr10,06) check for additional notes
function isAddInfo($ordID) {
	$qry = "SELECT ordAddInfo FROM orders WHERE ordID = '$ordID'";
	$res = mysql_query($qry) or print(mysql_error());
	if(mysql_num_rows($res) > 0) {
		$row = mysql_fetch_assoc($res);
	}
	if(!empty($row['ordAddInfo'])) {
		return true;
	}
	
	return false;
}
// END ADDED
// ADDED by Shane (may24,06)  for "how get" wholesale pricing
function getPricingStructure($id=1) {
	$query = 'select * from pricing_struct where id="'.$id.'"';
	$result = mysql_query($query);
	$rv = mysql_fetch_assoc($result);
	$query = 'select * from pricing_tier where pricing_struct_id="'.$id.'" order by quantity_start';
	$result = mysql_query($query);
	for($i=0;$row=mysql_fetch_assoc($result);$i++) 
		$rv[tiers][$i] = $row;
	return $rv;
}
// END ADDED
// ADDED by Chad (July 3,06)
function getCustPricingStructures($custID,$current_struct='') {
	//$qry = "SELECT * FROM pricing_cust WHERE customer_id = " . $custID;
	$next_month=date('Y-m-d',mktime(0,0,0,date('m')+1,1,date('Y')));
	$qry = "SELECT pc.id as pcid,pc.*,ps.*
			FROM pricing_cust pc, pricing_struct ps
			WHERE pc.pricing_struct_id = ps.id
			AND customer_id =$custID";		
	if($current_struct) $qry.=" AND date_start<'$next_month'";
	else $qry.=" AND date_start>='$next_month'";
	$qry.=" ORDER BY ps.pricing_group, pc.date_start";
	if($current_struct) $qry.=" DESC";
	$res = mysql_query($qry) or print(mysql_error());
	$aPrc = array();
	$i=0;
	$pricing_group='';
	while($row = mysql_fetch_assoc($res)) {
		if($row['pricing_group']!=$pricing_group){
			$aStructure = getPricingStructure($row['pricing_struct_id']);
			$aPrc[$i]=$aStructure;
			$aPrc[$i]['date_start']=$row['date_start'];
			$aPrc[$i]['custID']=$row['customer_id'];
			$aPrc[$i]['psID']=$row['pcid'];
			$aPrc[$i]['pricing_struct_id']=$row['pricing_struct_id'];
			if($current_struct) $pricing_group=$row['pricing_group'];
			$i++;
		}
	}
	return $aPrc;
}
// END ADDED
// ADDED by Shane (may24,06)  for "how get" wholesale pricing
function getPricingAdj($wsID,$qty=1,$pricing_group=1) {
	$query = '
		SELECT *
		FROM `pricing_struct` ps, pricing_tier pt, pricing_cust pc
		WHERE ps.id = pc.pricing_struct_id
		AND pc.customer_id ="'.$wsID.'"
		AND pt.pricing_struct_id = ps.id
		AND ps.pricing_group='.$pricing_group.'
		AND pt.quantity_start<='.$qty.'
		AND pc.date_start < "'.date('Y-m-d').'"
		ORDER BY pc.date_start DESC,pt.quantity_start DESC
		LIMIT 1';
	//echo $query;
	$result = mysql_query($query);
	$num_rows=mysql_num_rows($result);
	if($num_rows>0) {
		$row=mysql_fetch_assoc($result);
		return (100-$row['discount'])/100;
	} else return 1;
}
function getPricingAdjtest($wsID,$qty=1,$pricing_group=1) {
	$query = '
		SELECT *
		FROM `pricing_struct` ps, pricing_tier pt, pricing_cust pc
		WHERE ps.id = pc.pricing_struct_id
		AND pc.customer_id ="'.$wsID.'"
		AND pt.pricing_struct_id = ps.id
		AND ps.pricing_group='.$pricing_group.'
		AND pt.quantity_start<='.$qty.'
		AND pc.date_start < "'.date('Y-m-d').'"
		ORDER BY pc.date_start DESC,pt.quantity_start DESC
		LIMIT 1';
	echo $query;
	exit();
	$result = mysql_query($query);
	$num_rows=mysql_num_rows($result);
	if($num_rows>0) {
		$row=mysql_fetch_assoc($result);
		return (100-$row['discount'])/100;
	} else return 1;
}
// END ADDED

// Added by Blake Jan 5, 2006
// gets all products in pricing group
function getProductPricingGroups($pricing_group,$pDisplay=TRUE){
	$arr='';
	$sql_pg="SELECT DISTINCT pName,pWholesalePrice FROM products WHERE pPricing_group=".$pricing_group;
	if(!$pDisplay) $sql_pg.=" AND pDisplay=1";
	$sql_pg.=" ORDER BY pName";
	//echo $sql_pg;
	$result_pg=mysql_query($sql_pg);
	if(mysql_num_rows($result_pg)>0){
		$i=0;
		while($row_pg=mysql_fetch_assoc($result_pg)){
			$arr[$i++]=array($row_pg['pName'],$row_pg['pWholesalePrice']);
		}
	}
	return $arr;
}
// END
	
// ADDED by Chad (May 31,06) 
function Encrypt($string, $key)

{
	$result = '';
	for($i=1; $i<=strlen($string); $i++)
	{
	$char = substr($string, $i-1, 1);
	$keychar = substr($key, ($i % strlen($key))-1, 1);
	$char = chr(ord($char)+ord($keychar));
	$result.=$char;
	}
	return $result;
}

function Decrypt($string, $key)
{
	$result = '';
	for($i=1; $i<=strlen($string); $i++)
	{
	$char = substr($string, $i-1, 1);
	$keychar = substr($key, ($i % strlen($key))-1, 1);
	$char = chr(ord($char)-ord($keychar));
	$result.=$char;
	}
	return $result;
}
// ADD ENDED

function getCCType($ccNum) {
	if (preg_match("/^5[1-5]/", $ccNum)) {
		$type = 'Mastercard';
	}
	else if (preg_match("/^4/", $ccNum)) {
		$type = 'Visa';
	}
	else if (preg_match("/^3[47]/", $ccNum)) {
		$type = 'Amex';
	}
	else if (preg_match("/^3(0[0-5]|[68])/" ,$ccNum)) {
		$type = 'Diners Club';
	}
	else if (preg_match("/^6011/",$ccNum)) {
		$type = 'Discover';
	}
	else if (preg_match("/^(3|2131|1800)/", $ccNum)) {
		$type = 'JCB';
	}
	else {
		$type = 'Not Found';
	}
/*
	if(ereg("^5[1-5]",$ccNum)) {
		$type = 'Mastercard';
	}elseif(ereg("^4",$ccNum)) {
		$type = 'Visa';
	}elseif(ereg("^3[47]",$ccNum)) {
		$type = 'Amex';
	}elseif(ereg("^3(0[0-5]|[68])",$ccNum)) {
		$type = 'Diners Club';
	}elseif(ereg("^6011",$ccNum)) {
		$type = 'Discover';
	}elseif(ereg("^(3|2131|1800)",$ccNum)) {
		$type = 'JCB';
	}else{
		$type = 'Not Found';
	}
*/	
	return $type;
}

function aimTransaction($amt, $cnum, $expdate, $type, $transid='', $fname='', $lname='', $inv='', $note='', $address='', $address2='', $city='', $state='', $zip='', $ccv='') {
	// CHECK REQUIRED VARIABLES
	if(empty($amt) || empty($cnum) || empty($expdate) || empty($type)) {
		return false;
	}
	
	$type = strtoupper($type);
	
	// GET THE LOGIN AND TRANSID
	$sql = "SELECT payProvDemo, payProvData1, payProvData2 FROM payprovider WHERE payProvID=13";
	$res = mysql_query($sql) or print(mysql_error());
	$info = mysql_fetch_assoc($res);
	
	// IF IN DEMO MODE
	if($info['payProvDemo']) {
		return false;
	}
	
	$login = $info['payProvData1'];
	$tran_key = $info['payProvData2'];
	$strParams = '';
	
	// REQUIRED FOR ALL CC TRANSACTIONS
	$strParams .= 'x_version=3.1';
	$strParams .= '&x_delim_data=True';
	$strParams .= '&x_delim_char=|';
	$strParams .= '&x_relay_response=False';
	$strParams .= '&x_login='.$login;
	$strParams .= '&x_tran_key='.$tran_key;
	$strParams .= '&x_amount='.$amt;
	$strParams .= '&x_card_num='.$cnum;
	$strParams .= '&x_exp_date='.$expdate;
	$strParams .= '&x_type='.$type;
	if($type!='AUTH_CAPTURE') {
		$strParams .= '&x_trans_id='.$transid;
	}
	
	// OPTIONAL
	$strParams .= '&x_first_name='.$fname;
	$strParams .= '&x_last_name='.$lname;
	$strParams .= '&x_invoice_num='.$inv;
	$strParams .= '&x_description='.$note;
	if($type=='AUTH_CAPTURE') {
		$strParams .= '&x_address='.$address.' '.$address2;
		$strParams .= '&x_city='.$city;
		$strParams .= '&x_state='.$state;
		$strParams .= '&x_zip='.$zip;
		$strParams .= '&x_card_code='.$ccv;
	}
	
	$sql5 = "INSERT INTO aimtest (resp, date_created)
			VALUES ( '$strParams', '" . date('Y-m-d H:i:s') . "' )";
	$res5 = mysql_query($sql5) or print(mysql_error());
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,'https://secure.authorize.net/gateway/transact.dll'); 
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $strParams);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$resp = curl_exec($ch);
	
	$sql = "INSERT INTO aimtest (resp, date_created)
			VALUES ('$resp', '" . date('Y-m-d H:i:s') . "')";
	$res = mysql_query($sql) or print(mysql_error());
	
	if(curl_error($ch) != ""){
		return false;
	}else{
		curl_close($ch);
	}
	
	return $resp;
}
// ADDED by Blake (July 11,06) insert record into ws_sales
function addWSSales($wsid,$ordID,$cartID,$msrp,$amt_charged,$discount,$pricing_group,$pricing_struct){
	$sql = "INSERT INTO ws_sales 
			SET wsid=$wsid,
			ordID=$ordID,
			cartID=$cartID,
			msrp=$msrp,
			amt_charged=$amt_charged,
			discount=$discount,			
			pricing_struct_id=$pricing_struct,
			date_ordered='".date("Y-m-d H:i:s")."'";//
	//echo $sql;
	$result=mysql_query($sql) or print(mysql_error().$sql);
	return $result;
}
//returns wholesale pricing structure
function getWSPricing($wsID,$qty=1,$pricing_group=1) {
	$query = '
		SELECT *
		FROM `pricing_struct` ps, pricing_tier pt, pricing_cust pc
		WHERE ps.id = pc.pricing_struct_id
		AND pc.customer_id ="'.$wsID.'"
		AND pt.pricing_struct_id = ps.id
		AND ps.pricing_group='.$pricing_group.'
		AND pt.quantity_start<='.$qty.'
		AND pc.date_start < "'.date('Y-m-d').'"
		ORDER BY pc.date_start DESC,pt.quantity_start DESC
		LIMIT 1';
	$result = mysql_query($query);
	$row=mysql_fetch_assoc($result);
	if(!empty($row)) return $row;
	else return false;
}

function addWSSalesOrder($wsid,$ordID){
	$sql="SELECT * FROM cart c, products p WHERE c.cartProdID=p.pID AND c.cartOrderID=".$ordID;
	$result=mysql_query($sql) or print(mysql_error().$sql);
	while($row=mysql_fetch_assoc($result)){
		$cartID = $row['cartID'];
		$msrp = $row['pWholesalePrice'];
		$amt_charged = $row['cartProdPrice'];
		$pricing_group = $row['pPricing_group'];
		$struct = getWSPricing($wsid,1,$pricing_group);
		$discount = $struct['discount'];
		$pricing_struct = $struct['pricing_struct_id'];
		addWSSales($wsid,$ordID,$cartID,$msrp,$amt_charged,$discount,$pricing_group,$pricing_struct);
	}
}
// END ADDED
// Added June 2 2006 By Blake
// removes uploaded images for custom screenz
function deleteUploadedImages(){
	global $hrs_img_on_server,$days_delete_cust;
	$img_orig='imguploads/img_orig';
	$img_final='imguploads/img_final';
	$img_thumb='imguploads/img_thumb';
	$img_screen='imguploads/img_screen';
	$img_flash='imguploads/img_flash';	

	$delete_time=time()-(60*60*$hrs_img_on_server);
	
	$sql1 = "SELECT *
			FROM uploaded_images ui
			WHERE ui.img_status = 'pending' 
			AND ui.date_added < '".date('Y-m-d H:i:s', $delete_time)."'";			
	$result1=mysql_query($sql1);	
	if(mysql_num_rows($result1)>0) {			
		while($row1=mysql_fetch_assoc($result1)) {
			$display_image=$row1['display_image'];
			$sql = "SELECT * FROM cartoptions co, cart c
					WHERE co.coCartID = c.cartID
					AND co.coCartOption LIKE '".$row1['id']."%'";
			$result=mysql_query($sql);	
			if(mysql_num_rows($result)>0) {			
				while($row=mysql_fetch_assoc($result)) {
					//delete original and thumb images when cart is complete
					if($row['cartCompleted']==1) {
						if(file_exists($img_orig.'/'.$display_image.'.jpg')) unlink($img_orig.'/'.$display_image.'.jpg');
						if(file_exists($img_thumb.'/'.$display_image.'.jpg')) unlink($img_thumb.'/'.$display_image.'.jpg');
					} elseif($row['cartCompleted']==0 || $row['cartCompleted']=='') {			
						$error=FALSE;
						$error_reason='Cart Not Completed cartCompleted='.$row['cartCompleted'];
						if(file_exists($img_final.'/'.$display_image.'.jpg')) unlink($img_final.'/'.$display_image.'.jpg');
						else {$error=TRUE;$error_reason.=' final,';}	
						if(file_exists($img_orig.'/'.$display_image.'.jpg')) unlink($img_orig.'/'.$display_image.'.jpg');
						else {$error=TRUE;$error_reason.=' original,';}				
						if(file_exists($img_screen.'/'.$display_image.'.gif')) unlink($img_screen.'/'.$display_image.'.gif');
						else {$error=TRUE;$error_reason.=' screen,';} 				
						if(file_exists($img_thumb.'/'.$display_image.'.jpg')) unlink($img_thumb.'/'.$display_image.'.jpg');
						else {$error=TRUE;$error_reason.=' thumb';}
						$sql3="UPDATE uploaded_images SET img_status='deleted',note='incfunctions date=".date('Y-m-d H:i:s' )." reason=".$error_reason."' WHERE id=".$row1['id'];
						mysql_query($sql3);
					}
				}
			} else {
				$error=FALSE;
				$error_reason='No Join To Cart Option or Cart';
				if(file_exists($img_final.'/'.$display_image.'.jpg')) unlink($img_final.'/'.$display_image.'.jpg');
				else {$error=TRUE;$error_reason.=' final,';}	
				if(file_exists($img_orig.'/'.$display_image.'.jpg')) unlink($img_orig.'/'.$display_image.'.jpg');
				else {$error=TRUE;$error_reason.=' original,';}				
				if(file_exists($img_screen.'/'.$display_image.'.gif')) unlink($img_screen.'/'.$display_image.'.gif');
				else {$error=TRUE;$error_reason.=' screen,';} 				
				if(file_exists($img_thumb.'/'.$display_image.'.jpg')) unlink($img_thumb.'/'.$display_image.'.jpg');
				else {$error=TRUE;$error_reason.=' thumb';}
				$sql3="UPDATE uploaded_images SET img_status='deleted',note='incfunctions date=".date('Y-m-d H:i:s' )." reason=".$error_reason."' WHERE id=".$row1['id'];
				mysql_query($sql3);
			}
			//delete flash images
			if(file_exists($img_flash.'/W_screen_'.$row1['id'].'.gif')) unlink($img_flash.'/W_screen_'.$row1['id'].'.gif');
			if(file_exists($img_flash.'/Y_screen_'.$row1['id'].'.gif')) unlink($img_flash.'/Y_screen_'.$row1['id'].'.gif');
			if(file_exists($img_flash.'/X_screen_'.$row1['id'].'.gif')) unlink($img_flash.'/X_screen_'.$row1['id'].'.gif');
			if(file_exists($img_flash.'/p_screen_'.$row1['id'].'.gif')) unlink($img_flash.'/p_screen_'.$row1['id'].'.gif');
		}
	}
}
// END ADDED
// ADDED BY Blake 7-19-2006
// inserts record into uploaded img history
function setImgUpHistory($id,$type,$batch=''){
	$sql="INSERT INTO uploaded_images_history (changed_to,date_changed,batch_number) VALUES('$type','".date('Y-m-d H:i:s')."','$batch')";
	if(mysql_query($sql)) return true;
	else return false; 
}
// END ADDED
// ADDED BY Blake 8-9-2006
// gets stock amounts in cart
function getOnOrderStock($pID,$optID="") {
	global $stockManage;
	if($stockManage != 0){
		/*$sSQL = "SELECT pSell FROM products WHERE pID='" . $pID . "'";
		$result = mysql_query($sSQL) or print(mysql_error());
		$row=mysql_fetch_assoc($result);
		$pSell=$row['pSell'];
		//echo 'pSell='.$pSell;
		mysql_free_result($result);
		if($pSell == '2'){*/
		if($optID !=""){
			$sSQL = "SELECT SUM(cartQuantity) AS cartQuant FROM cart INNER JOIN cartoptions ON cart.cartID=cartoptions.coCartID JOIN orders o ON cartOrderID=o.ordID WHERE ordStatus BETWEEN 3 AND 9 AND cartCompleted=1 AND coOptID=" . $optID;
			//echo $sSQL;
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_assoc($result)) {
				if (! is_null($rs["cartQuant"])) {
					$totQuant = (int)$rs["cartQuant"];
					// See if its a custom Hype Rim.  If so double the quantity since the product doesn't come as pairs
					//  * Remember any changes need to be made elsewhere too ( do_stock_management(), release_stock(), and printpackingslips.php line 151 )
					$sql = "SELECT og.optGrpName 
							FROM optiongroup og, options o
							WHERE o.optGroup = og.optGrpID
							AND o.optID = ".$optID;
					$res = mysql_query($sql);
					$row = mysql_fetch_assoc($res);
					
					if ($pID == 'customhype' && strstr($row['optGrpName'], 'Rim')) {
						$totQuant = (int)$rs["cartQuant"] * 2;
					}
				} else { 
					$totQuant = '0';
				}
			}	
		}else{
			$sSQL = "SELECT SUM(cartQuantity) AS cartQuant FROM cart c, orders o WHERE c.cartOrderID=o.ordID AND ordStatus BETWEEN 3 AND 9 AND cartCompleted=1 AND cartProdID='" . $pID . "'";
			$result = mysql_query($sSQL) or print(mysql_error());
			if($rs = mysql_fetch_array($result)) {
				if(! is_null($rs["cartQuant"])) $totQuant = (int)$rs["cartQuant"];
				else $totQuant = '0';
			}			
		}
		mysql_free_result($result);
		return $totQuant;
	} else return '0';
}
// ADDED BY BLAKE AUG 24, 2006
// RETURNS REBATE DETAILS IN AN ARRAY
function getWSNumOrders($thisDate,$custID=''){	
	$sSQL = "SELECT c.Name, c.Email, c.custID 
		FROM clientlogin cl, customers c
		WHERE cl.clientUser=c.Email
		AND cl.clientWholesaler>0 ORDER BY c.Name";
	if($custID!='') $sSQL .=" AND c.custID=".$custID;
	$result=mysql_query($sSQL);
	$i=0;
	$num_rows=mysql_num_rows($result);
	if($num_rows>0){
		while($row=mysql_fetch_assoc($result)){			
			$alldata[$i]=$row;
			$custID=$row['custID'];
			$thecount='';
			$sql1="SELECT SUM(c.cartQuantity) as thecount, SUM(p.pWholesalePrice*c.cartQuantity) as theamt, p.pPricing_group FROM orders o, cart c, products p WHERE o.ordID=c.cartOrderID AND c.cartProdID=p.pID AND o.ordDate LIKE '" .$thisDate ."%' AND ordEID=".$custID." AND cartCompleted=1 GROUP BY p.pPricing_group";
			$result1=mysql_query($sql1);
			if(mysql_num_rows($result1)>0){
				while($row1=mysql_fetch_assoc($result1)){ 
					$thecount[$row1['pPricing_group']]['cnt'] = $row1['thecount'];
					$thecount[$row1['pPricing_group']]['amt'] = $row1['theamt'];
				}	
			} else {
				$thecount[$row1['pPricing_group']]['cnt'] = '0';
				$thecount[$row1['pPricing_group']]['amt'] = '0';
			}
			$pstruct=getCustPricingStructures($custID);
			if(is_array($pstruct)) $struct_count=count($pstruct);			
			$rebate_total=0;
			$prod_total=0;
			$amt_total;
			for($j=0;$j<$struct_count;$j++){
				$num_tiers=0;
				$this_rebate=0;
				$this_num_prods=0;
				$high_tier_discount=0;
				$next_tier=0;
				$next_tier_discount=0;
				$rebate_discount=0;
				if(is_array($pstruct[$j]['tiers'])) $struct_count2=count($pstruct[$j]['tiers']);
				$alldata[$i]['pstruct'][$j]['pricing_group']=$pstruct[$j]['pricing_group'];			
				for($k=0;$k<$struct_count2;$k++){
					if($thecount[$pstruct[$j]['pricing_group']]['cnt']>=$pstruct[$j]['tiers'][$k]['quantity_start']) {
						$alldata[$i]['pstruct'][$j]['tiers'][$k]['discount']=$pstruct[$j]['tiers'][$k]['discount'];
						$alldata[$i]['pstruct'][$j]['tiers'][$k]['quantity_start']=$pstruct[$j]['tiers'][$k]['quantity_start'];
						$alldata[$i]['pstruct'][$j]['tiers'][$k]['structID']=$pstruct[$j]['tiers'][$k]['pricing_struct_id'];
						$num_tiers++;				
						$rebate_discount=$pstruct[$j]['tiers'][$k]['discount']-$pstruct[$j]['tiers'][0]['discount'];
						$high_tier_discount=$pstruct[$j]['tiers'][$k]['discount'];
						$high_tier_qty=$pstruct[$j]['tiers'][$k]['quantity_start'];
						$next_tier=$pstruct[$j]['tiers'][$k+1]['quantity_start'];
						$next_tier_discount=$pstruct[$j]['tiers'][$k+1]['discount'];
					}  
					
				}
				if($num_tiers==0)$num_tiers=1;
				if($next_tier==0)$next_tier=$pstruct[$j]['tiers'][1]['quantity_start'];
				if($next_tier_discount==0)$next_tier_discount=$pstruct[$j]['tiers'][1]['discount'];
				if($high_tier_discount==0) $high_tier_discount=$pstruct[$j]['tiers'][0]['discount'];
				$alldata[$i]['pstruct'][$j]['next_tier_discount']=$next_tier_discount;
				$alldata[$i]['pstruct'][$j]['next_tier']=$next_tier;
				$alldata[$i]['pstruct'][$j]['high_tier_discount']=$high_tier_discount;
				$alldata[$i]['pstruct'][$j]['tier_count']=$num_tiers;
				$alldata[$i]['pstruct'][$j]['rebate_discount']=$rebate_discount;
				$alldata[$i]['pstruct'][$j]['num_prods']=$thecount[$pstruct[$j]['pricing_group']]['cnt'];
				if(empty($alldata[$i]['pstruct'][$j]['num_prods'])) $alldata[$i]['pstruct'][$j]['num_prods']='0';
				$alldata[$i]['pstruct'][$j]['amt']=$thecount[$pstruct[$j]['pricing_group']]['amt'];
				if($num_tiers>1) $alldata[$i]['pstruct'][$j]['rebate']=($thecount[$pstruct[$j]['pricing_group']]['amt'])*(($rebate_discount)/100);
				else $alldata[$i]['pstruct'][$j]['rebate']=0;
				$rebate_total+=$alldata[$i]['pstruct'][$j]['rebate'];
				$prod_total+=$alldata[$i]['pstruct'][$j]['num_prods'];
				$amt_total+=$alldata[$i]['pstruct'][$j]['amt']; 
			}
			$alldata[$i]['rebate_total']+=$rebate_total;
			$alldata[$i]['prod_total']+=$prod_total;
			$alldata[$i]['amt_total']+=$amt_total;
			$i++;	
		}
	}
	return $alldata;
}
// END
// ADDED BY BLAKE AUG 31, 2006
// RETURNS TOP PRODUCT DETAILS IN AN ARRAY
// ALL
function getAllTopProds($days,$show_start,$show_end,$prod_type=""){
	$array_wrap=getTopProds('wrap',$days,$show_start,$show_end);
	$array_band=getTopProds('band',$days,$show_start,$show_end);
	$array_screen=getTopProds('screen',$days,$show_start,$show_end,$prod_type);
	$cnt=count($array_wrap);
	for($i=0;$i<$cnt;$i++) {
		$top_prods[$i]["wrap"]=$array_wrap[$i]["wrap"];
		$top_prods[$i]["band"]=$array_band[$i]["band"];
		$top_prods[$i]["screen"]=$array_screen[$i]["screen"];	
	}
	return $top_prods;
}
function getTopProds($type,$days,$show_start,$show_end,$prod_type=""){
	$sSQL = "SELECT sum(cartQuantity) AS total, co.coCartOption, co.coOptID
			FROM cart c, cartoptions co, options o 
			WHERE c.cartID = co.coCartID
			AND co.coOptID=o.optID
			AND o.optDisplay='yes'
			AND o.optStock>o.optDisplay_point
			AND c.cartCompleted =1
			AND co.coOptGroup LIKE '%".$type."%'
			AND c.cartDateAdded >= '".date('Y-m-d',mktime(0,0,0,date('m'),date('d')-$days,date('Y')))."%'
			GROUP BY co.coCartOption
			ORDER BY total DESC 
			LIMIT ".$show_start.",".$show_end;
	$result = mysql_query($sSQL) or print(mysql_error()."<br />".$sSQL);
	//echo $sSQL;
	$i=0;
	$type2=trim($type);
	while($rs = mysql_fetch_assoc($result)){		
		$sql_prod= "SELECT p.pID,o.optStyleID,o.optID,p.pImage,o.optColor,o.optPriceDiff,og.optFlags,p.isSet,og.optGrpName,o.optDisplay 
					FROM prodoptions po, products p, options o, optiongroup og 
					WHERE o.optGroup=po.poOptionGroup 
					AND po.poProdID=p.pID 
					AND p.pID NOT LIKE '%_old%' AND p.pID NOT LIKE '%_Old%'
					AND o.optID='".$rs["coOptID"]."' 
					AND og.optGrpName LIKE '%".$type."%'";
		if(!empty($prod_type)) $sql_prod.=" AND p.pID LIKE '".$prod_type."%'"; 
		$sql_prod.="ORDER BY p.isSet, o.optGroup";
		//echo $sql_prod;
		$result_prod = mysql_query($sql_prod) or print(mysql_error());
		$rs2 = mysql_fetch_assoc($result_prod);
		$styleID=$rs2["optStyleID"];
		if(strstr('screen',$type2)){					
			$prodID=$rs2['pID'];
			$pos=strpos($prodID,'-');
			//if($pos>=0) {$prodID=substr($prodID,($pos+1));$styleID=$rs2["optStyleID"];}
			//else 
			$styleID=$rs2["optStyleID"];
		}
		$top_prods[$i][$type]['optID']=$rs2["optID"];
		$top_prods[$i][$type]['option']=$rs["coCartOption"];
		$top_prods[$i][$type]['style']=$styleID;
		$top_prods[$i][$type]['count']=$rs["total"];		
		$top_prods[$i][$type]['pID']=$rs2['pID'];
		$top_prods[$i][$type]['styleID']=$rs2["optStyleID"];
		$top_prods[$i][$type]['pImage']=$rs2["pImage"];
		$top_prods[$i][$type]['optColor']=$rs2["optColor"];
		$top_prods[$i][$type]['optGroup']=$rs2["optGrpName"];
		$price_diff_type='';
		if($rs2["optPriceDiff"]==1) $price_diff_type='%';
		$top_prods[$i][$type]['optPriceDiff']=$rs2["optPriceDiff"].$price_diff_type;
		$i++;
	}
	return $top_prods;
}
// SETS
function getTopSets($days){
	$sSQL ="SELECT c.cartID, c.cartQuantity, co.coOptGroup, co.coCartOption, co.coID, o.optColor, o.optStyleID, o.optID
			FROM cart c, cartoptions co, options o, products p
			WHERE c.cartID = co.coCartID
			AND co.coOptID=o.optID
			AND c.cartProdID = p.pID
			AND c.cartCompleted =1
			AND p.isSet = 'yes'
			AND c.cartDateAdded >= '".date('Y-m-d',mktime(0,0,0,date('m'),date('d')-$days,date('Y')))."'
			ORDER BY co.coID";
	$result = mysql_query($sSQL) or print(mysql_error());
	//echo $sSQL;
	if(mysql_num_rows($result) > 0){
		$i=0;
		$cnt=0;
		$old_cartID='';
		while($rs = mysql_fetch_assoc($result)){
			if($cnt % 3 ==0 && !empty($old_cartID)){			
				$report[$i]['option_combination']=$option_set_combination;
				$report[$i]['count']=$option_set_count;
				$option_set_combination='';
				$option_set_count='';
				$spacer='';
				$cnt=0;
				$i++;
			}
			$option_set_combination.=$spacer.$rs['coCartOption'];
			$option_set_count=$rs['cartQuantity'];		
			if($cnt==2){
				$report[$i]['set'][$cnt]['style']=$rs['optStyleID'];
				$report[$i]['set']['optID']=$rs['optID'];
			}
			else $report[$i]['set'][$cnt]['color']=$rs['optColor'];
			$report[$i]['set'][$cnt]['optName']=$rs['coCartOption'];			
			$spacer='-';
			$old_cartID=$rs['cartID'];
			$cnt++;
		}
	}
	if(!empty($report)) sort($report);
	//showarray($report);
	//exit();
	$last_combination='';
	$cnt_r=count($report);
	for($i=0,$j=-1;$i<$cnt_r;$i++) {
		if($report[$i]['option_combination']==$last_combination) {
			$report_final[$j]['count']+=$report[$i]['count'];
		} else {
			$j++;
			$report_final[$j]['count']=1;
			$report_final[$j]['option_combination']=$report[$i]['option_combination'];			
			$report_final[$j]['set']=$report[$i]['set'];
		}
		$last_combination=$report[$i]['option_combination'];
		
	}
	$report='';
	if(!empty($report_final)) array_multisort($report_final, SORT_DESC);
	//showarray($report_final);
	return $report_final;
}

function getProdPrice($pID){
	$SQL="SELECT pPrice FROM products WHERE pID='".$pID."'";
	$result=mysql_query($SQL);
	if(!$result) return '';
	else {
		$row=mysql_fetch_assoc($result);
		return $row['pPrice'];
	}
}
// END

// Blake 12/15/05
// Insert plotter_history record
function updateplotterhistory($pID,$oldamt,$newamt,$empid,$type=''){
	$update=FALSE;
	if(!empty($type)){
		$sql1="SELECT id FROM plotter_history WHERE phPID='".$pID."' AND phType='".$type."'";
		//echo $sql1.'<br />';
		$result1=mysql_query($sql1);
		if(mysql_num_rows($result1)>0)$update=TRUE;
	}
	if($update){
		$sql="UPDATE plotter_history SET phEndingAmt=".$newamt." WHERE phType='".$type."'";
		//echo $sql.'<br />';
		$result=mysql_query($sql);
	} else {
		if(empty($type))$type='Manual';
		$sql="INSERT INTO plotter_history
		(phPID,phDateAdded,phStartAmt,phEndingAmt,empID,phType)
		VALUES 
		('".$pID."','".date('Y-m-d H:i:s')."',".$oldamt.",".$newamt.",'".$empid."','".$type."')";
		//echo $sql.'<br />';
		$result=mysql_query($sql);
	}
	if($result) return TRUE;
	else return FALSE;
}
function getRelatedProducts($pID,$orderID){
	global $hasrelated;
	//find related products
	$today=date('Y-m-d H:i:s');
	$sql = "SELECT * FROM related_prods rp, related_prods_disc rpd, related_prods_disc_add rpda, related_prods_add rpa, products p 
			WHERE rp.related_prod_disc_id=rpd.id 
			AND rpda.related_prod_disc_id=rpd.id
			AND rpda.add_prod_id=rpa.id
			AND rpa.add_pID=p.pID
			AND rp.pID='".$pID."' 
			AND Startdate<'".$today."' 
			AND Enddate>'".$today."'";
	$result=mysql_query($sql);
	$num_rows=mysql_num_rows($result);
	if($num_rows>0){
		//$displaytext='';
		while($row=mysql_fetch_assoc($result)){
			//calc discount
			if(!$hasrelated[$row['add_pID']]){
				if($row['add_disc_type']==0) $add_disc=($row['pPrice']-$row['add_disc']);
				elseif($row['add_disc_type']==1) $add_disc=($row['pPrice']-($row['pPrice']*($row['add_disc']/100)));
				if($add_disc<0)$add_disc=0;
				$sql_check="SELECT * FROM cart WHERE cartSessionID='".session_id()."' AND cartProdID='".$row['add_pID']."' AND cartAddProd=1";
				$result_check=mysql_query($sql_check);
				$num_rows_check=mysql_num_rows($result_check);
				if($num_rows_check==0){
					$sql_insert = "INSERT INTO cart (cartSessionID,cartProdID,cartProdName,cartProdPrice,cartDateAdded,cartQuantity,cartOrderID,cartCompleted,cartAddProd) 
								   VALUES ('".session_id()."','".$row['add_pID']."','".$row['pName']."',".$add_disc.",'".$today."',".$row['add_qty'].",".$orderID.",0,1)";
					$result_insert=mysql_query($sql_insert);
					$hasrelated[mysql_insert_id()]=$row['add_display_text'];
					$hasrelated['disc_display_text']=$row['display_text'];
					$hasrelated[$row['add_pID']]=TRUE;
				}
			}
		}
	}
	mysql_free_result($result);
}
// gift cert move from incemail page
// Blake 12/21/06
function create_certificate($ordID){
	$certarray = array();
	$prodarray = array();
	$strsql = "SELECT cert_code, cert_amt FROM certificates WHERE cert_order_id =".$ordID;
	if(!empty($_SESSION['cert_prod'])) $strsql .=" AND cert_prod_id='".$_SESSION['cert_prod']."'"; 
	$result = mysql_query($strsql);
	if(mysql_num_rows($result) > 0) {
		while($rs=mysql_fetch_assoc($result)) {
			$certarray[] = "(\$".number_format($rs['cert_amt']).") ".$rs['cert_code'];
		}
		return implode("|",$certarray);
	}
	$strsql = "SELECT pID FROM products WHERE p_iscert > 0";
	$result = mysql_query($strsql);
	while ($rs=mysql_fetch_assoc($result)) {
		$prodarray[] = $rs['pID'];
	}
	$prodarray = "'".implode("','",$prodarray)."'";
	if($prodarray == "") $prodarray = "NOVALIDPRODIDFORTHISQUERY696986";
	$strsql = "SELECT cartID, cartProdPrice, cartQuantity, cartProdID, ordEmail FROM cart, orders  WHERE ordID =".$ordID."  AND cartOrderID =".$ordID." AND cartProdID IN (".$prodarray.")";
	$result = mysql_query($strsql);
	while ($rs=mysql_fetch_assoc($result)) {
		// CHECK OPTIONS FOR HARD CARD CERTIFICATES
		if($_SESSION['cert_prod']==$rs['cartProdID']||$_SESSION['cert_prod']==''){
			$sql_hc = "SELECT * FROM cartoptions WHERE coCartID = " . $rs['cartID'];
			$res_hc = mysql_query($sql_hc);
			$row_hc = mysql_fetch_assoc($res_hc);
			if($row_hc['coCartOption'] != 'Card via Mail') {
				$expdate = time()+(720*24*3600);
				for ($x = 0; $x < $rs['cartQuantity'];$x++) {
					do {
						$sqlrows = 1;
						$certcode = 'G'.RandomString(11);
						$strsql = "SELECT * FROM certificates WHERE cert_code='".$certcode."'";
						$sqlrows = mysql_num_rows(mysql_query($strsql));
					} while ($sqlrows > 0);
					$certarray[] = "(\$".number_format($rs['cartProdPrice']).") ".$certcode;
					$strsql = "INSERT INTO certificates (cert_code, cert_prod_id, cert_amt, cert_order_id, cert_email, cert_exp_dt, date_created)
						  VALUES ('".$certcode."','".$rs['cartProdID']."',".$rs['cartProdPrice'].",".$ordID.",'".$rs['ordEmail']."',".$expdate.",'".date('Y-m-d H:i:s')."')";
					mysql_query($strsql);
				}
			}else{
				$certarray[] = "(\$".number_format($rs['cartProdPrice']).") Card to be Mailed";
			}
		}
	}
	$_SESSION['cert_prod']='';
	return implode("|",$certarray);
}
// function to generate random strings
function RandomString($length=32) {
	$randstr='';
	srand((double)microtime()*1000000);
	//our array add all letters and numbers if you wish
	$chars = array ( 'B','C','D','F','G','H','J','R','T','X','Y','Z','2','4','6','8','3','5','7','9');
	for ($rand = 0; $rand <= $length; $rand++)
	{
	$random = rand(0, count($chars) -1);
	$randstr .= $chars[$random];
	}
	return $randstr;
}
//google
function sendmessagewithbasicauth($themessage){
	global $googledata1,$googledata2,$googledemomode,$curlproxy,$success;
	$cfurl='https://' . ($googledemomode ? 'sandbox' : 'checkout') . '.google.com' . ($googledemomode ? '/checkout' : '') . '/cws/v2/Merchant/' . $googledata1 . '/request';
	$success = TRUE;
	if(@$pathtocurl != ''){
		exec($pathtocurl . ' -H \'Authorization: Basic ' . base64_encode($googledata1 . ":" . $googledata2) . '\' -H \'Content-Type: application/xml\' -H \'Accept: application/xml\' --data-binary \'' . str_replace("'","\'", '<?xml version="1.0" encoding="UTF-8"?>' . $themessage) . '\' ' . $cfurl, $cfres, $retvar);
		$cfres = implode("\n",$cfres);
	}else{
		if (!$ch = curl_init()) {
			print "cURL package not installed in PHP. Set \$pathtocurl parameter.";
			$success=FALSE;
		}else{
			curl_setopt($ch, CURLOPT_URL, $cfurl);
			$headers = array('Authorization: Basic ' . base64_encode($googledata1 . ":" . $googledata2), 'Content-Type: application/xml', 'Accept: application/xml');
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_POSTFIELDS, '<?xml version="1.0" encoding="UTF-8"?>' . $themessage);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			if(@$curlproxy!=''){
				curl_setopt($ch, CURLOPT_PROXY, $curlproxy);
			}
			$cfres = curl_exec($ch);
			// print str_replace("<","<br />&lt;",str_replace('<'.'/','&lt;/',$cfres)) . "<br />\n";
			if(curl_error($ch) != ""){
				print 'cURL error: ' . curl_error($ch) . '<br />';
				$success=FALSE;
			}else{
				curl_close($ch);
			}
		}
	}
	return($cfres);
}

function check_email_mx($email) { 
	if( (preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) || 
		(preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/',$email)) ) { 
		$host = explode('@', $email);
		if(checkdnsrr($host[1].'.', 'MX') ) return true;
		if(checkdnsrr($host[1].'.', 'A') ) return true;
		if(checkdnsrr($host[1].'.', 'CNAME') ) return true;
	}
	return false;
}

function notifyCustShipped($ordID) {
	$customHeaders = '';
	$message = '';
	$from = 'ifrogz Customer Service';
	$fromEmail = 'support@ifrogz.com';
	$to = '';
	$subject = 'Your Order Has Been Shipped!';
	
	$customHeaders .= "From: $from <$fromEmail>\n";
	$customHeaders .= "Return-Path: <$fromEmail>\n";
	$customHeaders .= "MIME-Version: 1.0\n";
	$customHeaders .= "Content-Type: text/HTML; charset=ISO-8859-1\n";
	
	$sql = "SELECT * FROM orders WHERE ordID = '$ordID'";
	$res = mysql_query($sql) or print(mysql_error());
	$row = mysql_fetch_assoc($res);
	
	$to = $row['ordEmail'];
	
	$message .= '<p>Your order <strong>'.$ordID.'</strong> has been shipped. If you selected FedEx for shipping, your tracking information should be available within 24 hours, which will allow you can track your order every step of the way from our <a href="http://ifrogz.com/order_info.php?ordID='.$ordID.'&email='.$to.'">order status</a> page.</p>';
	$message .= '<p>Below you can find how many days until your order should arrive depending on the shipping you selected.</p>';
	$message .= '<ul>';
	$message .= '	<li><strong>Standard:</strong> 5 - 8  business days</li>';
	$message .= '	<li><strong>Priority Mail:</strong> 3 - 4 business days</li>';
	$message .= '	<li><strong>FedEx:</strong> 2 - 3 business days</li>';
	$message .= '	<li><strong>International:</strong> 10 - 12 business days</li>';
	$message .= '</ul>';
	$message .= '<p>Thank you for your order, and we hope you enjoy your new ifrogz product!</p>';

	mail($to, $subject, $message, $customHeaders);	
}

// A usort function used in printpackingslips.php to sort by bin # correctly
function cmpBins($a, $b) {
	if (strstr($a['bin'], "*") && strstr($b['bin'], "*")) { // If both follow the same format for Bins with asterisks
		$arrBin = explode("*", $a['bin']);
		$arrBin2 = explode("*", $b['bin']);
		
		if ($arrBin[1] == $arrBin2[1]) { // Same bin letter
			if ($arrBin[2] == $arrBin2[2]) {
				return 0;
			}
			if ($arrBin[2] < $arrBin2[2]) {
				return -1;
			} else {
				return 1;
			}
		} else {
			if ($arrBin[1] < $arrBin2[1]) {
				return -1;
			} else {
				return 1;
			}
		}
	} else {
		$bin1 = simplify_bin($a['bin']);
		$bin2 = simplify_bin($b['bin']);
		if ($bin1 == $bin2) {
			return 0;
		}
		if ($bin1 < $bin2) {
			return -1;
		} else {
			return 1;
		}
	}
}

function simplify_bin($bin) {
	$first_asterisk = strpos($bin, '*') != FALSE ? strpos($bin, '*')+1 : FALSE;
	$first_hyphen = strpos($bin, '-') != FALSE ? strpos($bin, '-')+1 : FALSE;
	if(strcmp($bin, '') == 0) {
		return $bin;
	} else if($first_asterisk > 0) {
		$temp_string = substr($bin, $first_asterisk);
		$second_asterisk = strpos($temp_string, '*')+$first_asterisk;
		return substr($bin, $first_asterisk, $second_asterisk - $first_asterisk);
	} else {
		$temp = substr($bin, 0, $first_hyphen);
		return $temp;
	}
}
?>