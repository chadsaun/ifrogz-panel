<?php
// ===================================================================
// For a description of these parameters and their useage, please open the following URL in your browser
// http://www.ecommercetemplates.com/phphelp/ecommplus/parameters.asp

$sortBy = 2;
if(strpos($_SERVER['HTTP_HOST'], 'admin.ifrogz.com') !== false) {
	$pathtossl = "https://admin.ifrogz.com";
}else {
	$pathtossl = "";
}
$alwaysemailstatus=FALSE;
//
//$debugmode=TRUE;
//force submit after cc charge
$forcesubmit=FALSE;
$forcesubmittimeout=5000;

//downloads
$digidownloads=TRUE;
$digidownloadsecret="dfa13sfdl435wefajpq44jwfld018";
$digidownloadwarn=TRUE;
$digidownloaddays=10;
$digidownloademail="Your products can be downloaded for the next $digidownloaddays days by going to the URL%nl%http://ifrogz.com/order_info.php%nl%. . . and enter the order ID %orderid% and order email. %nl%%nl%";
//$digidownloadmethod="filesystem";
//$digidownloadpattern="/home/myfrogz/downloads/%pid%.zip";
$noshowdigiordertext=TRUE;

//custom screenz img uploads
$hrs_img_on_server=7;
$days_delete_cust=4;
$custom_screen_msg="*(Screen will look exactly like image to the right. If there is no image or it is not correct you must delete this product and re-upload another image.)";
//login
$loginredirect = "registercontact.php";
//$shopredirect = "products.php";
$advicetag = "your message goes here";
$emailregister = TRUE;
$usecustnav = FALSE; 
$forcelogin = FALSE;
$nocatid=TRUE;

$enableclientlogin=TRUE;
$estimateshipping=TRUE;
$taxShipping=0;
$pagebarattop=1;
$productcolumns=2;
$useproductbodyformat=2;
$usesearchbodyformat=2;
$usedetailbodyformat=2;
$useemailfriend=FALSE;
$nobuyorcheckout=FALSE;
$noprice=FALSE;
$expireaffiliate=30;
$usecategoryformat=3;
$allproductsimage="";
$nogiftcertificate=FALSE;
$showtaxinclusive=FALSE;
$upspickuptype="";
$overridecurrency=FALSE;
	$orcsymbol=" AU\$";
	$orcemailsymbol=" AU\$";
	$orcdecplaces=2;
	$orcdecimals=".";
	$orcthousands=",";
	$orcpreamount=TRUE;
$encryptmethod="";
$commercialloc=FALSE;
$showcategories=TRUE;
$termsandconditions=TRUE;
$showquantonproduct=TRUE;
$showquantondetail=TRUE;
$addshippinginsurance=0;
$noshipaddress=FALSE;
$pricezeromessage="";
$showproductid=FALSE;
$currencyseparator=" ";
$noproductoptions=FALSE;
$dateformatstr = "F jS Y";
$invoiceheader='';
$invoiceaddress='<br><strong>ifrogz</strong><br>919 N 1000 W<br>Logan, Utah 84321<br>USA<br><br>';
$invoicefooter='<br><br><font size="2">Thank you for shopping with ifrogz.com!</font>';
$dumpccnumber=TRUE;
$actionaftercart=3;
$dateadjust=0;
$emailorderstatus=3;
$htmlemails=TRUE;
$emailheader='<img src="http://ifrogz.com/images/ifrogz_logo_white.jpg" alt="ifrogz.com"><br>';
$emailfooter="<h3>ifrogz.com - Custom iPod Cases</h3><br />";
$orderstatusemail="Your order number %orderid% from %orderdate% has been updated to %newstatus% on %date%.";
$orderstatussubject = "ifrogz order status";
$orderstatusshippedemail = "<p>We wanted to let you know that your order number %orderid% from %orderdate% has been shipped. You can expect your order to arrive in 3-6 business days.</p><p>If you have any questions, please email us at support@ifrogz.com.</p>";
$orderstatusshippedsubject = "Your ifrogz Order has been Shipped!";
 
$useaddressline2=TRUE;
$categorycolumns=4;
$noshowdiscounts=FALSE;
$nowholesalediscounts=TRUE;
$catseparator="<br />&nbsp;";
$willpickuptext="";
$willpickupcost=0;
$extraorderfield1="";
$extraorderfield1required=FALSE;
$extraorderfield2="";
$extraorderfield2required=FALSE;
$defaultcommission=10;
$emaillist = TRUE;

// ===================================================================
// Please do not edit anything below this line
// ===================================================================

$maintablebg="";
$innertablebg="";
$maintablewidth="924";
$innertablewidth="100%";
$maintablespacing="0";
$innertablespacing="0";
$maintablepadding="4";
$innertablepadding="4";
$headeralign="left";

error_reporting (E_ALL ^ E_NOTICE);

define("maxprodopts",100);
define("helpbaseurl","http://www.ecommercetemplates.com/phphelp/ecommplus/");
?>