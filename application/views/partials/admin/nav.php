<?php
include('init.php');
include_once(LIBPATH.'php/classes/ui/menu.php');

// Added this so Kohana would not throw exceptions when the session times out
if ( ! isset($_SESSION["employee"]["id"])) {
	$_SESSION['employee']['id'] = NULL;
	$_SESSION['employee']['permissions'] = NULL;
}
?>
<!-- Header -->
<div id="header">
    <div style="float: left; width: 200px; height: 61px; padding: 14px 0px 0px 14px;"><a href="/user/index/"><img style="width: 180px; height: 48px;" src="/lib/images/template/header/logoadmin.png" /></a></div>
    <div style="float: left; width: 110px;"><img style="width: 108px; height: 75px;" src="/lib/images/template/header/ethernet.png" /></div>
    <?php if (!is_null($_SESSION['employee']['id'])) { ?>
    <div style="float: right; width: 125px; height: 55px; padding: 20px 14px 0px 0px;"><a id="logout-button" href="/user/logout/"><img style="width: 121px; height: 35px;" src="/lib/images/template/header/logout.png" /></a></div>
    <?php } ?>
    <div style="clear: both;"></div>
</div>
<!-- Left navigation menu -->
<div style="clear: both; float: left; margin-top: 5px;">
	<div class="nav-submenu">
		<a href="/"><img src="/lib/images/template/menu/dashboard.jpg" style="width: 172px; height: 38px;" /></a>
	</div>
<?php
	$nav_menu = Menu::factory($_SESSION)
		// Sales Menu
		->add_item(Submenu::SALES, $yyLLOrds, '/admin/orders.php', array(Roles::ADMIN, Roles::CUSTOMER_SERVICE, Roles::IT, Roles::MANAGEMENT, Roles::SHIPPING, Roles::ECOMMERCE))
		// Promotions Menu
	    ->add_item(Submenu::PROMOTIONS, 'assign discounts', '/admin/asndiscounts.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT, Roles::ECOMMERCE))
		->add_item(Submenu::PROMOTIONS, $yyLLDisc, '/admin/discounts.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT, Roles::ECOMMERCE, 12))
		// Website Menu
	    ->add_item(Submenu::WEBSITE, 'features page', '/features/index/', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE, 107))
		->add_item(Submenu::WEBSITE, 'homepage ads', '/ads/index/', array(Roles::ADMIN, Roles::IT, Roles::ECOMMERCE, 107))
    	->add_item(Submenu::WEBSITE, 'mini-cart', '/minicart', array(Roles::ADMIN, Roles::IT, Roles::ECOMMERCE))
		->add_item(Submenu::WEBSITE, 'product reviews', '/reviews/index/', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE))
		->add_item(Submenu::WEBSITE, 'press page', '/press/index/', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE))
		->add_item(Submenu::WEBSITE, 'testimonials', '/admin/testimonials.php', array(Roles::ADMIN, Roles::CUSTOMER_SERVICE, Roles::IT, Roles::MANAGEMENT, Roles::ECOMMERCE, 61, 88))
		// Catalog Menu
		->add_item(Submenu::CATALOG, $yyLLCats, '/admin/cats.php', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, $yyLLProA, '/admin/prods.php', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, 'product search', '/admin/prodsnew.php', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, $yyLLProO, '/admin/prodopts.php', array(Roles::ADMIN, Roles::IT, Roles::PRODUCTS, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, 'gift certs', '/admin/certs.php?mode=1', array(Roles::ADMIN, Roles::CUSTOMER_SERVICE, Roles::IT, Roles::MANAGEMENT, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, 'hard cards', '/admin/hardcards.php', array(Roles::ADMIN, Roles::CUSTOMER_SERVICE, Roles::IT, Roles::MANAGEMENT, Roles::SALES, Roles::SHIPPING, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, 'certificates open', '/certificates/open/', array(Roles::ACCOUNTING, Roles::ADMIN, Roles::IT, Roles::MANAGEMENT, Roles::ECOMMERCE, 127))
		->add_item(Submenu::CATALOG, 'advanced inventory', '/admin/inventoryadv.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT, Roles::SHIPPING, Roles::ECOMMERCE))
		->add_item(Submenu::CATALOG, 'inventory history', '/admin/invchangehistory.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT, Roles::ECOMMERCE, 75))
		// Shipping Menu
		->add_item(Submenu::SHIPPING, $yyLLCoun, '/admin/country.php', array(Roles::ADMIN, Roles::IT, Roles::ECOMMERCE))
		->add_item(Submenu::SHIPPING, $yyLLStat, '/admin/state.php', array(Roles::ADMIN, Roles::IT, Roles::ECOMMERCE))
		->add_item(Submenu::SHIPPING, $yyLLZone, '/admin/zones.php', array(Roles::ADMIN, Roles::IT, Roles::ECOMMERCE))
		// Exchanges/Returns Menu
		->add_item(Submenu::EXCHANGES_RETURNS, 'warranties', '/warrantyregistrations/', array(Roles::ACCOUNTING, Roles::ADMIN, Roles::CUSTOMER_SERVICE, Roles::IT, Roles::MANAGEMENT, Roles::SALES))
		// Reports Menu
		->add_item(Submenu::REPORTS, 'back order report', '/admin/backorder.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT, 73))
		// Charts Menu
		->add_item(Submenu::CHARTS, 'real-time', '/admin/realtime.php', array(Roles::CHARTS, Roles::MANAGEMENT, Roles::IT, Roles::ECOMMERCE))
		->add_item(Submenu::CHARTS, 'daily', '/admin/daily.php', array(Roles::CHARTS, Roles::MANAGEMENT, Roles::IT, Roles::ECOMMERCE))
		// System Menu
        ->add_item(Submenu::SYSTEM, 'change password', '/password/change/', array(Roles::USER))
		->add_item(Submenu::SYSTEM, $yyLLMain, '/admin/main.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT))
		->add_item(Submenu::SYSTEM, 'order status', '/admin/ordstatus.php', array(Roles::ADMIN, Roles::IT, Roles::ECOMMERCE))
		->add_item(Submenu::SYSTEM, $yyLLPayP, '/admin/payprov.php', array(Roles::ADMIN, Roles::IT, Roles::MANAGEMENT))
		->render();
	echo $nav_menu;
?>
</div>