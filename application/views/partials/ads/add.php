<script>
	$(document).ready(function() {
		$('.datetimepicker').datetimepicker({ 
			changeMonth: true,
			changeYear: true,
		    dateFormat: 'yy-mm-dd',
			showSecond: true,
			timeFormat: 'hh:mm:ss'
		});
		$('#submit').click(function() {
			$('#buttonarea').hide();
			$('#loadingarea').show();
		})
	});
</script>
<hr class="space">
<div class="push-4 span-14">
	<?php echo Form::open('ads/add_save/', array('enctype' => 'multipart/form-data')) ?>
	<table style="background-color:#F7F7F7; border: #CCC solid 1px; padding:20px 5px 5px 5px;">
		<thead>
			<tr>
				<td style="text-align:center;" colspan="2">
					<span style="font:normal 30px Helvetica; margin:20% 0 0 0;">Add a New Ad</span>
					<hr class="space">
				</td>
			</tr>
			<tr>
				<td width="25">Image:</td>
				<td>
					<?php echo Form::file('image', array('class'=>'span-10', 'id'=>'image'))?>
				</td>
			</tr>
			<tr>
				<td width="25">Link:</td>
				<td>
					<?php echo Form::input('link', '', array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-10', 'id'=>'link'))?>
				</td>
			</tr>
			<?php /* ?>
			<tr>
				<td width="25">Image Alt Text:</td>
				<td>
					<?php */ ?>
					<?php echo Form::hidden('alt', 'Ad', array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-10', 'id'=>'link'))?>
					<?php /* ?>
				</td>
			</tr>
			<?php */ ?>
			<tr>
				<td width="25">Start:</td>
				<td>
					<?php echo Form::input('begin_date', '', array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-5 datetimepicker', 'id'=>'begin_date')) ?>
				</td>
			</tr>
			<tr>
				<td width="25">End:</td>
				<td>
					<?php echo Form::input('end_date', '', array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-5 datetimepicker', 'id'=>'end_date')) ?>
				</td>
			</tr>
			<tr>
				<td width="25">Staging:</td>
				<td>
					<?php echo Form::checkbox('isTest', '', TRUE, array('id' => 'isTest')) ?>
				</td>
			</tr>
			<tr id="buttonarea">
				<td></td>
				<td style="text-align:left;">
					<?php echo Form::hidden('id', $id) ?>
					<?php echo Form::hidden('type', $type) ?>
					<?php echo Form::submit('name', 'Save', array('class'=>'smallbutton', 'id'=>'submit')); ?>
					<?php // <a id="<?php echo $ad->id " class="delete" style="cursor:pointer;"> echo HTML::image('http://cdn.projects.ifrogz.com/lib/images/icons/delete.png') </a> ?>
					<a id="backtolist" class="procedure" style="cursor:pointer; padding:0 0 0 5px; color:red;">cancel</a>
				</td>
			</tr>
			<tr id="loadingarea" style="display:none;">
				<td></td>
				<td>
					<?php echo HTML::image(Kohana::$config->load('paths.s3_image_path') . 'products/admin/ajax.gif', array('style'=>'margin:5px 0 0 5px;'))?>
					<span class="cancel">Saving...</span>
				</td>
			</tr>
		</thead>
	</table>
	<?php echo Form::close(); ?>
</div>