<? switch ($type) {
	case 'Button':
		$image_path = 'http://cloudfront.ifrogz.com/lib/images/homepagebuttons/';
	break;
	default:
		$image_path = 'http://cloudfront.ifrogz.com/lib/images/homepagebanners/';
	break;
} ?>
<script>
	$(document).ready(function() {
		$('.datetimepicker').datetimepicker({ 
			changeMonth: true,
			changeYear: true,
		    dateFormat: 'yy-mm-dd',
			showSecond: true,
			timeFormat: 'hh:mm:ss'
		});
	});
</script>
<hr class="space">
<div class="push-4 span-14">
	<table style="background-color:#F7F7F7; border: #CCC solid 1px; padding:20px 5px 5px 5px;">
		<thead>
			<tr>
				<? if ($ad->begin_date < date("Y-m-d H:i:s") && $ad->end_date > date("Y-m-d H:i:s")) { ?>
				<td style="text-align:center;" colspan="2">
					<? echo HTML::image($image_path.$ad->image, array('style'=>'max-width:360px;')); ?>
					<hr class="space">
					<span style="font:normal 18px Helvetica; margin:20% 0 0 0;">Active</span>
				</td>
				<? } else { ?>
				<td style="text-align:center;" colspan="2">
					<? echo HTML::image($image_path.$ad->image, array('style'=>'max-width:360px; filter: alpha(opacity=50); -moz-opacity: 0.50; opacity: 0.50;')); ?>
					<hr class="space">
					<span style="font:normal 18px Helvetica; margin:20% 0 0 0; color:#ccc;">Not Active</span>
				</td>
				<? } ?>
			</tr>
			<? /* ?>
			<tr>
				<td width="10">Image:</td>
				<td>
					<? echo Form::input('image', $ad->image, array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-10', 'id'=>'image'))?>
				</td>
			</tr>
			<? */ ?>
			<tr>
				<td width="10">Link:</td>
				<td>
					<? echo Form::input('link', $ad->link, array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-10', 'id'=>'link'))?>
				</td>
			</tr>
			<tr>
				<td width="10">Start:</td>
				<td>
					<? echo Form::input('begin_date', $ad->begin_date, array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-5 datetimepicker', 'id'=>'begin_date'))?>
				</td>
			</tr>
			<tr>
				<td width="10">End:</td>
				<td>
					<? echo Form::input('end_date', $ad->end_date, array('style'=>'height:30px; font:normal 16px Helvetica;', 'class'=>'span-5 datetimepicker', 'id'=>'end_date'))?>
				</td>
			</tr>
			<tr>
				<td></td>
				<td style="text-align:left;">
					<a id="save" class="smallbutton" style="cursor:pointer;">Save</a>
					<? echo Form::hidden('id', $ad->id, array('id'=>'ad_id'))?>
					<? // <a id="<? echo $ad->id " class="delete" style="cursor:pointer;"> echo HTML::image('http://cdn.projects.ifrogz.com/lib/images/icons/delete.png') </a> ?>
					<a id="backtolist" class="procedure" style="cursor:pointer; padding:0 0 0 5px; color:red;">cancel</a>
				</td>
			</tr>
		</thead>
	</table>
</div>