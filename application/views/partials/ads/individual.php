<? switch ($type) {
	case 'Button':
		$image_path = 'http://cloudfront.ifrogz.com/lib/images/homepagebuttons/';
	break;
	default:
		$image_path = 'http://cloudfront.ifrogz.com/lib/images/homepagebanners/';
	break;
} ?>
<li style="list-style-type: none; cursor: move;" id="<? echo $ad->id ?>">
	<table style="background-color:#F7F7F7; border: #CCC dotted 1px; padding:5px;">
		<thead>
			<tr>
				<td colspan="2">
					<span style="font:bold 14px Helvetica;">Display Order:&nbsp;<span id="order_<? echo $ad->id ?>"><? echo $ad->imgOrder ?></span></span>
				</td>
			</tr>
			<tr>
				<? if ($ad->begin_date < date("Y-m-d H:i:s") && $ad->end_date > date("Y-m-d H:i:s")) { ?>
				<td style="text-align:center;" colspan="2">
					<? echo HTML::image($image_path.$ad->image, array('style'=>'max-width:240px;')); ?>
					<span style="font:normal 18px Helvetica; margin:20% 0 0 0;">Active</span>
				</td>
				<? } else { ?>
				<td style="text-align:center;" colspan="2">
					<? echo HTML::image($image_path.$ad->image, array('style'=>'max-width:240px; filter: alpha(opacity=50); -moz-opacity: 0.50; opacity: 0.50;')); ?>
					<span style="font:normal 18px Helvetica; margin:20% 0 0 0; color:#ccc;">Not Active</span>
				</td>
				<? } ?>
			</tr>
			<tr>
				<td width="10">Image:</td>
				<td>
				<? if (strlen($ad->image) > 30) { ?>
					<span title="<? echo $ad->image ?>"><? echo substr($ad->image, 0, 30) . '...' ?></span>
				<? } else { ?>
					<span title="<? echo $ad->image ?>"><? echo $ad->image ?></span>
				<? } ?>
				</td>
			</tr>
			<tr>
				<td width="10">Link:</td>
				<td>
				<? if (strlen($ad->link) > 30) { ?>
					<span title="<? echo $ad->link ?>"><? echo substr($ad->link, 0, 30) . '...' ?></span>
				<? } else { ?>
					<span title="<? echo $ad->link ?>"><? echo $ad->link ?></span>
				<? } ?>
				</td>
			</tr>
			<tr>
				<td width="10">Start:</td>
				<td>
					<span><? echo $ad->begin_date ?></span>
				</td>
			</tr>
			<tr>
				<td width="10">End:</td>
				<td>
					<span><? echo $ad->end_date ?></span>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:left;">
					<a id="<? echo $ad->id ?>" class="delete" style="cursor:pointer;"><? echo HTML::image('http://cdn.projects.ifrogz.com/lib/images/icons/delete.png') ?></a>
					<a id="<? echo $ad->id ?>" class="edit" style="cursor:pointer; padding:0 0 0 5px;">edit</a>
				</td>
			</tr>
		</thead>
	</table>
</li>