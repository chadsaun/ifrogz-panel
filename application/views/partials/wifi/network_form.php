<? foreach ($network['hidden'] as $key => $value): ?>
	<? echo Form::hidden($key, $value); ?>
<? endforeach; ?>
<? unset($network['hidden']); ?>
<? foreach ($network as $key => $value): ?>
	<? if (strcmp($key, 'SCPasswords_ID') == 0): ?>
		<? $key = 'Hash'; ?>
	<? endif; ?>
	<? $text = preg_replace('/([a-z]|[A-Z])([A-Z][a-z])/', '$1 $2', $key); // Remove camel case column names. ?>
	<p>
		<? echo Form::label($key, $text)?>
		<? if (strcmp($key, 'IsViewable') == 0 || strcmp($key, 'IsActive') == 0 || strcmp($key, 'IsNice') == 0): ?>
			<? $checked = ($value) ? TRUE : FALSE; ?>
			<? echo Form::checkbox($key, '1', $checked); ?>
		<? elseif (strcmp($key, 'Channel') == 0): ?>
			<br /><? echo Form::select($key, array(
				'0' => 'Dynamic',
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'5' => '5',
				'6' => '6',
				'7' => '7',
				'8' => '8',
				'9' => '9',
				'10' => '10',
				'11' => '11',
				'12' => '12',
				'13' => '13',
				'14' => '14',
			), $value); ?>
		<? else: ?>
			<br /><? echo Form::input($key, $value, array('class' => 'title')); ?>
		<? endif; ?>
	</p>
<? endforeach; ?>