<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $title; ?></title>

<script language="JavaScript" type="text/javascript" charset="utf-8"  src="http://assets.ifrogz.com/lib/packages/jquery/1.5.1/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8"  src="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8.13/jquery-ui.custom.min.js"></script>
<!-- <script language="JavaScript" type="text/javascript" charset="utf-8"  src="http://assets.ifrogz.com/lib/packages/jquery-form-defaults/00.00.00/jquery.form-defaults.js"></script>
<script language="JavaScript" type="text/javascript" charset="utf-8"  src="http://assets.ifrogz.com/lib/packages/jquery-form/2.43/jquery.form.js"></script> -->
<script language="JavaScript" type="text/javascript" charset="utf-8"  src="http://assets.ifrogz.com/lib/packages/framework/log.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="/lib/css/adminstyle.css"> -->
<link rel="stylesheet" type="text/css" media="screen, projection" href="/lib/css/blueprint/screen.css">
<link rel="stylesheet" type="text/css" media="print" href="/lib/css/blueprint/print.css">
<link rel="stylesheet" type="text/css" href="/lib/css/procedures.css" />
<link rel="stylesheet" type="text/css" href="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8.13/ui-lightness/jquery-ui.custom.css" />
<!--[if lt IE 8]>
  <link rel="stylesheet" href="/lib/css/blueprint/ie.css" type="text/css" media="screen, projection">
<![endif]-->
<style>
body {
	background-color: white;
	<?php if ($header == 'black'): ?>
		background-image: url('http://cdn.admin.ifrogz.com/lib/images/headers/repeatingblack.jpg');
	<?php else: ?>
		background-image: url('http://cdn.admin.ifrogz.com/lib/images/headers/repeatingyellow.jpg');
	<?php endif; ?>
	background-repeat: repeat-x;
}
</style>
</head>
<body>
<!-- Layout -->
<!-- Main Content -->
<div class="container">
	<?= $content; ?>
</div>

</body>
</html>