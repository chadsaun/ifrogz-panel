<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $title; ?></title>
<link rel="stylesheet" type="text/css" href="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8.6/ui-lightness/jquery-ui.custom.css" />
<script language="JavaScript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-ui/1.8.6/jquery-ui.custom.min.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-form-defaults/00.00.00/jquery.form-defaults.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/jquery-form/2.43/jquery.form.js"></script>
<script language="JavaScript" type="text/javascript" src="http://assets.ifrogz.com/lib/packages/framework/log.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="/lib/css/adminstyle.css"> -->
<link rel="stylesheet" href="/lib/css/blueprint/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="/lib/css/blueprint/print.css" type="text/css" media="print">
<link type="text/css" href="/lib/css/procedures.css" rel="stylesheet" />
<!--[if lt IE 8]>
  <link rel="stylesheet" href="/lib/css/blueprint/ie.css" type="text/css" media="screen, projection">
<![endif]--></code></pre>
<style>
body {
	background-color: white;
	background-image: url(http://admin.ifrogz.com/lib/images/procedures/background.jpg);
	background-repeat: repeat-x;
}
</style>

<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$(".able_default").DefaultValue("your available date");
		$(".listtitle").DefaultValue("Name your To-Do list");
		$(".listitem").DefaultValue("Add a To-Do (optional)");
		$(".addtodo").DefaultValue("Add a To-Do");	
		$(".startdate").DefaultValue("Start Date");
		$(".enddate").DefaultValue("End Date");		
		
		$(".datepicker").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd'
		});	
		
		$(".sidenavi").hover(function() { 
			$(this).find("span").show(); 
		} , function() {

			$(this).find("span").hide();
		});
		
	});
		
</script>
</head>
<body>
<?php
    include(APPPATH.'views/partials/admin/language.php');
    function isPermitted($permission = 'all') {
    	global $_SESSION;

    	$mappings = array(
    	    'accounting' => 'i_f_t',
        	'admin' => 'i_f_a',
        	'all' => 'all',
        	'customer service' => 'i_f_c',
        	'customer service admin' => 'i_f_ca', // deprecated
        	'hong kong' => 'i_f_h',
    	    'inventory' => 'i_f_v',
        	'it' => 'i_f_i',
        	'management' => 'i_f_m',
        	'nadal' => 'i_f_n', // deprecated
        	'product' => 'i_f_p',
        	'quality control' => 'i_f_qc',
        	'reports_retail' => 'i_f_rr',
    	    'sales' => 'i_f_sa',
        	'shieldzone' => 'i_f_z', // deprecated
        	'shipping' => 'i_f_s',
        );

    	$permission = strtolower($permission);

        if (isset($mappings[$permission])) {
            $permission = $mappings[$permission];
        }

        if (isset($_SESSION['employee']['permissions'])) {
            $roles = preg_split('/,+/', $_SESSION['employee']['permissions']);
        	$length = count($roles);
        	for ($i = 0; $i < $length; $i++) {
        		$roles[$i] = trim($roles[$i]);
        		if (!empty($roles[$i]) && ($roles[$i] == $permission)) {
        			return TRUE;
        		}
        	}
        }

    	return FALSE;
    }
    // include(APPPATH.'views/partials/admin/nav.php');
?>
<div class="container">
	<?= $content; ?>
</div>
</body>
</html>