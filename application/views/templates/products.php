<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?= $title; ?></title>

<script language="JavaScript" type="text/javascript" src="/lib/js/util/jquery-1.5.1.min.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/jquery-ui-1.8.13.custom.min.js"></script>
<!-- <script type="text/javascript" src="/lib/js/util/jquery.form-defaults.js"></script>
<script language="JavaScript" type="text/javascript" src="/lib/js/util/jquery.form.js"></script> -->
<script language="JavaScript" type="text/javascript" src="/lib/js/util/log.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="/adminstyle.css"> -->
<link rel="stylesheet" href="/lib/css/blueprint/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="/lib/css/blueprint/print.css" type="text/css" media="print">
<link type="text/css" href="/lib/css/procedures.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="/lib/css/jqueryui/ui-lightness-1.8.13/jquery-ui-1.8.13.custom.css" />
<!--[if lt IE 8]>
  <link rel="stylesheet" href="/lib/css/blueprint/ie.css" type="text/css" media="screen, projection">
<![endif]--></code></pre>
<style>
body {
	background-color: white;
	background-image: url(http://cloudfront.ifrogz.com/lib/images/products/admin/background.jpg);
	background-repeat: repeat-x;
}
</style>
</head>
<body>
<!-- Layout -->
<?php
    function isPermitted($permission='all') {
    	$permission = strtolower($permission);

    	if ($permission == 'admin') $permission = 'i_f_a';
    	if ($permission == 'it') $permission = 'i_f_i';
    	if ($permission == 'hong kong') $permission = 'i_f_h';
    	if ($permission == 'management') $permission = 'i_f_m';
    	if ($permission == 'customer service') $permission = 'i_f_c';
    	if ($permission == 'customer service admin') $permission = 'i_f_ca';
    	if ($permission == 'shipping') $permission = 'i_f_s';
    	if ($permission == 'shieldzone') $permission = 'i_f_z';
    	if ($permission == 'nadal') $permission = 'i_f_n';
    	if ($permission == 'product') $permission = 'i_f_p';
    	if ($permission == 'all') $permission = 'all';
    	if ($permission == 'reports_retail') $permission = 'i_f_rr';

    	if (strstr($_SESSION['employee']['permissions'],"," . $permission)) {
    		return true;
    	} elseif (strstr($_SESSION['employee']['permissions'],$permission . ",")) {
    		return true;
    	} elseif (strstr($_SESSION['employee']['permissions'],"," . $permission . ",")) {
    		return true;
    	}
    	return false;
    }
   // include(APPPATH . '../inc/int_nav.php');
?>
<!-- Main Content -->
<div class="container">
	<?= $content; ?>
</div>

</body>
</html>