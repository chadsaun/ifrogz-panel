<?php
if (!defined('KOHANA_EXTERNAL_MODE')) {
    define('KOHANA_EXTERNAL_MODE', TRUE);
}
//include_once('/home/www2ifr/public_html/index.php');
include_once('/home/ifrogz/public_html/index.php');

// Date definitions.
define('YEAR', 0);
define('MONTH', 1);
define('DAY', 2);

// CSV file conversion definitions.
define('ID', 0);
define('USERNAME', 3);

$source = new DB_DataSource('default_fishbowl');

$connection_string = $source->host;
if (!preg_match('/^localhost$/i', $connection_string)) {
	$port = $source->port;
	if (!empty($port)) {
	    $connection_string .= '/' . $source->port;
	}
}
$connection_string .= ':' . $source->database;

if ( ! ibase_connect($connection_string, $source->username, $source->password)) {
	die('Could not connect: ' . ibase_errmsg());
}

/*
 * There are 4 things that each sales representative must have:
 * -First name
 * -Last name
 * -Username (This is the username of this individual in FishBowl. It can also be an array.)
 * -E-mail
 */
$sales_representatives = array(
	// Jeff Morgan
	array(
		'first_name' => 'Jeff', 
		'last_name' => 'Morgan', 
		'username' => 'jeffmorgan', 
		'email' => 'jeff@ifrogz.com'
	),
	// Shawn Rapier
	array(
		'first_name' => 'Shawn', 
		'last_name' => 'Rapier', 
		'username' => 'shawnrapier', 
		'email' => 'shawnr@ifrogz.com'
	),
	// Tuck Wong
	array(
		'first_name' => 'Tuck', 
		'last_name' => 'Wong', 
		'username' => 'tuck', 
		'email' => 'tuck@ifrogz.com'
	)
	/*
	// Michelle Burkinshaw (she gets the report where a customer is not one of the above sales representatives).
	array(
		'first_name' => 'Michelle', 
		'last_name' => 'Burkinshaw', 
		'username' => array('tuck', 'shawnrapier', 'jeffmorgan'),
		'email' => 'michelle@ifrogz.com'
	)
	*/
);

// Customizable options
$columns = array('COST' => FALSE, 'PRICE' => TRUE, 'QTY' => TRUE);

// Yesterday's date
$yesterday = explode('-', date('Y-m-d', strtotime('-1 day')));
$today = explode('-', date('Y-m-d', strtotime('now')));

// The character that will delimit a column.
$column_delimiter = ',';

// The character that is used to include data. For example, "Some Data".
$column_wrapper = '"';

// The part number to search for.
$part_number = '%';

// The starting date.
$start = "{$yesterday[YEAR]}-01-01 00:00:00";

// The ending date.
$end = "{$today[YEAR]}-{$today[MONTH]}-{$today[DAY]} 00:00:00";

$csv_string = ''; // The message we are returning.
$row_delimiter = chr(10); // The character that will delimit a row.

$start_month = '01'; // Always want to start with January.
$end_month = $today[MONTH]; // Today's month.
$start_year = $yesterday[YEAR];
$end_year = $today[YEAR];

$total = 1;
// This is used to determine how many months should be on the report.
if (($start_month != $end_month) || ($start_year != $end_year)) {
	$total = 0;
	$month = $start_month;
	while ($start_year < $end_year) {
		// The start year is less than the end year. In this case we will 12 months minus the start month.
		// For example, if the start month is June then we only add 7 months.
		$total += 12 - $month + 1; // The + 1 is because January is considered month 1 not month 0.
		$month = 1;
		$start_year++;
	}
	$total += $end_month - $month + 1; // The + 1 is because January is considered month 1 not month 0.
}

// Now set up the heading row for the csv file.
$column_array = array();

foreach ($columns as $column_name => $is_visible) {
	if ($is_visible) {
		$column_array[] = $column_name;
	}
}

$heading_string .= "{$column_wrapper}PART{$column_wrapper}{$column_delimiter}"; // Part Heading
$heading_string .= "{$column_wrapper}CUSTOMER{$column_wrapper}{$column_delimiter}"; // Customer Heading

$start_month = date('n', strtotime($start));
$start_year = date('Y', strtotime($start));

for ($i = 0; $i < $total; $i++) {
	if ($start_month == 13) {
		$start_month = 1;
		$start_year++;
	}
	for ($j = 0; $j < count($column_array); $j++) {
		// Year-Month Type Heading. For Example, 2010-07 Price
		$heading_string .= "{$column_wrapper}{$start_year}-{$start_month} {$column_array[$j]}{$column_wrapper}{$column_delimiter}";
	}
	$start_month++;
}
$heading_string .= "{$column_wrapper}SALES REP{$column_wrapper}{$column_delimiter}";
$heading_string .= $row_delimiter;

/*
 * The conversion table is used as a look up reference. You send the table a month and year seperated by a dash
 * and it will tell you want order that month is in the sequence. For example if we had the months of:
 * May 2010, June 2010, July 2010, August 2010. Then when you look up July like so:
 * $conversion_array['2010-07'] it will return 2.
 */
$conversion_array = get_conversion_array($start, $total);

/*
 * This will associate an account with a sales representative.
 * 
 * This array looks like:
 * array(
 * 	[0] => array(
 * 		'ID' => 660,
 * 		'USERNAME' => 'jeffmorgan'
 * 	),
 * 	[1] => array(
 * 		'ID' => 600,
 * 		'USERNAME' => 'tuck'
 * 	)
 * )
 */
$sales_representatives_and_accounts = get_sales_representatives_and_accounts_array();

/*
 * This loop will get all the data for each sales representative and then send an e-mail containing a csv to them.
 */
foreach ($sales_representatives as $representative) {
	// Add the heading to the return string.
	$csv_string = $heading_string;
	
	// This is the data straight from the database.
	$data_array = get_raw_data_array($part_number, $start, $end, $representative['username'], $sales_representatives_and_accounts);
	
	// In case there is no data for this sales representative we want to break the skip this representative.
	if (count($data_array) < 1) {
		echo "Warning: {$representative['first_name']} has no results<br />";
		continue;
	}
	
	// This is the data in the array within an array format that we need.
	$data_array = get_data_array($data_array, $conversion_array);

	 // Now print out all the data for this sales representative.
	$iterations = count($data_array);
	for ($i = 0; $i < $iterations; $i++) {
		// The part number and customer name will be the same for each item in the array. Therefore, the first one is grabbed.
		// The foreach loop must be used because of how the logic is set.
		foreach ($data_array[$i] as $data) {
			$csv_string .= "{$column_wrapper}{$data['PARTNUM']}{$column_wrapper}{$column_delimiter}";
			$csv_string .= "{$column_wrapper}{$data['CUSTOMERNAME']}{$column_wrapper}{$column_delimiter}";
			break; // Only 1 part number and customer name is needed so we break out here.
		}
		
		//$total was declared and defined earlier.
		for ($j = 0; $j < $total; $j++) {
			if ( ! isset($data_array[$i][$j]) || $data_array[$i][$j]['QTY'] < 1) {
				// No data was found so we just print out zeros.
				$column_array_size = count($column_array);
				for ($k = 0; $k < $column_array_size; $k++) {
					$csv_string .= "{$column_wrapper}0{$column_wrapper}{$column_delimiter}";
				}
			} else {
				// We have data so now we will print it out.
				if ($columns['COST']) {
					$average_cost = $data_array[$i][$j]['COST']/$data_array[$i][$j]['QTY']; // The average cost.
				}
				
				if ($columns['PRICE']) {
					$average_price = $data_array[$i][$j]['PRICE']/$data_array[$i][$j]['QTY']; // The average price.
				}
				
				if ($columns['COST']) {
					$csv_string .= $column_wrapper.sprintf('%0.2f', $average_cost).$column_wrapper.$column_delimiter;
				}
				
				if ($columns['PRICE']) {
					$csv_string .= $column_wrapper.sprintf('%0.2f', $average_price).$column_wrapper.$column_delimiter;
				}
				$csv_string .= $column_wrapper.$data_array[$i][$j]['QTY'].$column_wrapper.$column_delimiter;
			}
		}
		$csv_string .= "{$column_wrapper}{$data['EMPLOYEE']}{$column_wrapper}{$column_delimiter}";
		$csv_string .= $row_delimiter;
	}
	
	// All the data is created. It needs to be sent to the representative.
	send_email($csv_string, $representative['email'], $representative['first_name'].' '.$representative['last_name']);
}

function get_month_index($month, $year, $conversion_array) {
	$index = $year.'-'.$month;
	return $conversion_array[$index];
}

function get_conversion_array($start_date, $total) {
	$start_month = date('n', strtotime($start_date));
	$start_year = date('Y', strtotime($start_date));

	$conversion_array = array();
	for ($i = 0; $i < $total; $i++) {
		if ($start_month == 13) {
			$start_month = 1;
			$start_year++;
		}
		$temp = ($start_month < 10) ? '0'.$start_month : $start_month;
		$index = $start_year.'-'.$temp;
		$conversion_array[$index] = $i;

		$start_month++;
	}
	
	return $conversion_array;
}

function get_raw_data_array($part_number, $start, $end, $representative, $sales_representatives_and_accounts) {
	$company_select = get_companies($representative, $sales_representatives_and_accounts);
	if (empty($company_select)) {
		echo "We didn't have anyone for: {$representative}";
		return array();
	}
	
	$select_query = "SELECT 
		(CASE 
		WHEN EXTRACT(MONTH FROM SO.DATEISSUED) < 10 
	    	THEN '0'||EXTRACT(MONTH FROM SO.DATEISSUED) 
		WHEN EXTRACT(MONTH FROM SO.DATEISSUED) > 9 
	    	THEN EXTRACT(MONTH FROM SO.DATEISSUED) 
		END) AS THEMONTH, 
		EXTRACT(YEAR FROM SO.DATEISSUED) AS THEYEAR, 
		PART.ID AS PARTID, 
		PART.NUM AS PARTNUM, 
		CUSTOMER.ID AS CUSTOMERID, 
		CUSTOMER.NAME AS CUSTOMERNAME,
		SYSUSER.USERNAME AS EMPLOYEE,
		SUM(SOITEM.TOTALCOST) AS COST, 
		SUM(SOITEM.TOTALPRICE) AS PRICE, 
		CAST(SUM(SOITEM.QTYTOFULFILL) AS INTEGER) AS QTY
	FROM 
		SO 
		LEFT JOIN SOITEM ON SOITEM.SOID=SO.ID 
		LEFT JOIN PRODUCT ON PRODUCT.ID=SOITEM.PRODUCTID 
		LEFT JOIN PART ON PART.ID=PRODUCT.PARTID 
		LEFT JOIN CUSTOMER ON CUSTOMER.ID=SO.CUSTOMERID
		LEFT JOIN SYSUSER ON SYSUSER.ID=CUSTOMER.DEFAULTSALESMANID
	WHERE
		PART.NUM LIKE '{$part_number}'
		AND SO.DATEISSUED BETWEEN '{$start}' AND '{$end}'
		AND CUSTOMER.ID IN ({$company_select})
	GROUP BY 
		PARTID, CUSTOMERID, PARTNUM, CUSTOMERNAME, THEYEAR, THEMONTH, EMPLOYEE
	ORDER BY 
		CUSTOMERID, PARTID";

	$results = ibase_query($select_query) OR die(ibase_errmsg());

	// Now we set up the array.
	$array = array();
	while ($row = ibase_fetch_assoc($results)) {
		$array[] = $row;
	}
	
	return $array;
}

function get_data_array($array, $conversion_array) {
	$data_array = array();
	$customer_id = '';
	$part_id = '';
	$current_index = 0;

	// Now put the data in an easy to iterate array
	$iterations = count($array);
	for ($i = 0; $i < $iterations; $i++) {
		$index = get_month_index($array[$i]["THEMONTH"], $array[$i]["THEYEAR"], $conversion_array);
		if (empty($customer_id) && empty($part_id)) {
			$customer_id = $array[$i]['CUSTOMERID'];
			$part_id = $array[$i]['PARTID'];
			$data_array[$current_index][$index] = $array[$i];
		} else if ((strcmp($customer_id, $array[$i]['CUSTOMERID']) == 0) && (strcmp($part_id, $array[$i]['PARTID']) == 0)) {
			$data_array[$current_index][$index] = $array[$i];
		} else {
			$customer_id = $array[$i]['CUSTOMERID'];
			$part_id = $array[$i]['PARTID'];
			$data_array[++$current_index][$index] = $array[$i];
		}
	}
	return $data_array;
}

function send_email($csv_string, $recipent_email, $recipent_name) {
	$csv_file = '/home/ifrogz/public_html/lib/csv/item_data_warehouse.csv';
	
	$file_pointer = fopen($csv_file, 'w');
	if (!$file_pointer) {
		echo "We were on: {$recipent_name}\n";
		echo "We have a file pointer problem here.";
		exit();
	}
	fwrite($file_pointer, $csv_string);
	fclose($file_pointer);
	
	// Send the Email
    $Mailer = new Mailer('GMAIL');
	$Mailer->add_mailing_list('ifrogz_item_data_warehouse');
	
	// Recepient
	$Mailer->add_recipient(new EmailAddress($recipent_email, $recipent_name));
	
	// Subject
	$Mailer->set_subject('Sales Representative Report - CONFIDENTIAL');

	// Message for debugging
	$Mailer->set_content_type('multipart/mixed');
    $Mailer->set_message('This report is intended only for the recipient and is not to be shared with anyone in the company or outside of it.');
    $Mailer->set_alt_message('To view the message, please use an HTML compatible email viewer!');
	
	// The CSV file for the sales representative.
	$Mailer->add_attachment(new Attachment(DataSource::FILE_CONTENTS, $csv_file, 'item_data_warehouse.csv'));

	if (!$Mailer->send()) { 
		$error = $Mailer->get_error();
		echo $error['message'];
	} else {
		echo 'Message was sent!';
	}
}

function get_sales_representatives_and_accounts_array() {
	$row = 1;
	if (($file_pointer = fopen('/home/ifrogz/public_html/lib/csv/representative_accounts.csv', 'r')) !== FALSE) {
		$data_array = array();
		while (($data = fgetcsv($file_pointer, 1000, ",")) !== FALSE) {
			$temp_array = array('ID' => $data[ID], 'USERNAME' => $data[USERNAME]);
			$data_array[] = $temp_array;
		}
		fclose($file_pointer);
		return $data_array;
	}
	return FALSE;
}

function get_companies($representative, $sales_representatives_and_accounts) {
	$return_string = '';
	$iterations = count($sales_representatives_and_accounts);
	for ($i = 0; $i < $iterations; $i++) {
		if (strcmp($sales_representatives_and_accounts[$i]['USERNAME'], $representative) == 0) {
			$return_string .= $sales_representatives_and_accounts[$i]['ID'].',';
		}
	}
	
	$return_string = substr($return_string, 0, -1);
	return $return_string;
}
?>