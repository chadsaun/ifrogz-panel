<?php

require_once '/home/ifrogz/public_html/lib/php/misc/db_conn_open.php';

/**
 * Get last Twitter ID
 */
$since_id_mentions = getVariable('last_twitter_mentions_id');
$since_id_from = getVariable('last_twitter_from_id');
$since_id_keyword = getVariable('last_twitter_keyword_id');

$orig_id_mentions = $since_id_mentions;
$orig_id_from = $since_id_from;
$orig_id_keyword = $since_id_keyword;

/**
 * Get Twitter Data
 */
$url_mentions = 'http://search.twitter.com/search.json?q=%40ifrogz';
if ($since_id_mentions > 0) {
	$url_mentions .= '&since_id=' . $since_id_mentions;
}

$url_from = 'http://search.twitter.com/search.json?q=from%3Aifrogz';
if ($since_id_from > 0) {
	$url_from .= '&since_id=' . $since_id_from;
}

$url_keyword = 'http://search.twitter.com/search.json?q=ifrogz';
if ($since_id_keyword > 0) {
	$url_keyword .= '&since_id=' . $since_id_keyword;
}


$twitter_mentions_json = file_get_contents($url_mentions);
$twitter_mentions_data = json_decode($twitter_mentions_json);

$twitter_from_json = file_get_contents($url_from);
$twitter_from_data = json_decode($twitter_from_json);

$twitter_keyword_json = file_get_contents($url_keyword);
$twitter_keyword_data = json_decode($twitter_keyword_json);

//debug($twitter_mentions_data); exit;

$last_mentions_id = 0;
$last_from_id = 0;
$last_keyword_id = 0;

/**
 * Send to SiteCatalyst
 */
$namespace = "ifrogz";
$domain    = "122.2o7.net";
$host      = $namespace.".".$domain;
$rsid      = "ifgtwitter";
$ip        = '74.200.224.242';
$timestamp = date('c');
$evar1 = 'iFrogz';

$i = 0;
foreach ($twitter_mentions_data->results as $result) {
	// Skip the original
	if ($orig_id_mentions == $result->id) {
		continue;
	}
	if ($i == 0) {
		$last_mentions_id = $result->id;
	}
	$evar2 = $result->from_user;
	$evar3 = $result->to_user;
	$evar4 = $result->text;
	
	// create opening XML tags
	$xml  = "<?xml version=1.0 encoding=UTF-8?>\n";
	
	$xml .= "<request>\n";
	$xml .= " <scXmlVer>1.0</scXmlVer>\n";
	
	// add tags for required elements
	$xml .= $rsid ? " <reportSuiteID>$rsid</reportSuiteID>\n":""; 

	//The timestamp line of code can only be used when an Omniture representitive has enabled timestamp support for your organization.
	$xml .= $timestamp ? " <timestamp>$timestamp</timestamp>\n":"";
	
	$xml .= $evar1 ? " <eVar1>$evar1</eVar1>\n":"";
	$xml .= $evar2 ? " <eVar2>$evar2</eVar2>\n":"";
	$xml .= $evar3 ? " <eVar3>$evar3</eVar3>\n":"";
	$xml .= $evar4 ? " <eVar4>$evar4</eVar4>\n":"";
	$xml .= " <events>event1</events>\n";
	$xml .= $ip ? " <ipaddress>$ip</ipaddress>\n":""; 
	$xml .= " <pageURL>http://ifrogz.com</pageURL>\n";
	$xml .= " <pageName>Tweeter Mention</pageName>\n"; 
	
	// close the XML request
	$xml .= "</request>\n";
	
	//echo $xml; exit;
	
	if (sendxml($xml)) {
		setVariable('last_twitter_mentions_id', $last_mentions_id);
		echo "UPDATED last_twitter_mentions_id TO $last_mentions_id";
	}
	
	$i++;
}

$i = 0;
foreach ($twitter_from_data->results as $result) {
	// Skip the original
	if ($orig_id_from == $result->id) {
		continue;
	}
	if ($i == 0) 
	{
		$last_from_id = $result->id;
	}
	$evar2 = $result->from_user;
	$evar3 = $result->to_user;
	$evar4 = $result->text;
	
	// create opening XML tags
	$xml  = "<?xml version=1.0 encoding=UTF-8?>\n";
	
	$xml .= "<request>\n";
	$xml .= " <scXmlVer>1.0</scXmlVer>\n";
	
	// add tags for required elements
	$xml .= $rsid ? " <reportSuiteID>$rsid</reportSuiteID>\n":"";

	//The timestamp line of code can only be used when an Omniture representitive has enabled timestamp support for your organization.
	$xml .= $timestamp ? " <timestamp>$timestamp</timestamp>\n":"";
	
	$xml .= $evar1 ? " <eVar1>$evar1</eVar1>\n":"";
	$xml .= $evar2 ? " <eVar2>$evar2</eVar2>\n":"";
	$xml .= $evar3 ? " <eVar3>$evar3</eVar3>\n":"";
	$xml .= $evar4 ? " <eVar4>$evar4</eVar4>\n":"";
	$xml .= " <events>event2</events>\n";
	$xml .= $ip ? " <ipaddress>$ip</ipaddress>\n":"";
	$xml .= " <pageURL>http://ifrogz.com</pageURL>\n";
	$xml .= " <pageName>Tweeter From</pageName>\n"; 
	
	// close the XML request
	$xml .= "</request>\n";
	
	if (sendxml($xml)) {
		setVariable('last_twitter_from_id', $last_from_id);
		echo "UPDATED last_twitter_from_id TO $last_from_id";
	}
	
	$i++;
}

$i = 0;
foreach ($twitter_keyword_data->results as $result) {
	// Skip the original
	if ($orig_id_keyword == $result->id) {
		continue;
	}
	if ($i == 0) 
	{
		$last_keyword_id = $result->id;
	}
	$evar2 = $result->from_user;
	$evar3 = $result->to_user;
	$evar4 = $result->text;
	
	// create opening XML tags
	$xml  = "<?xml version=1.0 encoding=UTF-8?>\n";
	
	$xml .= "<request>\n";
	$xml .= " <scXmlVer>1.0</scXmlVer>\n";
	
	// add tags for required elements
	$xml .= $rsid ? " <reportSuiteID>$rsid</reportSuiteID>\n":""; 

	//The timestamp line of code can only be used when an Omniture representitive has enabled timestamp support for your organization.
	$xml .= $timestamp ? " <timestamp>$timestamp</timestamp>\n":"";
	
	$xml .= $evar1 ? " <eVar1>$evar1</eVar1>\n":"";
	$xml .= $evar2 ? " <eVar2>$evar2</eVar2>\n":"";
	$xml .= $evar3 ? " <eVar3>$evar3</eVar3>\n":"";
	$xml .= $evar4 ? " <eVar4>$evar4</eVar4>\n":"";
	$xml .= " <events>event3</events>\n";
	$xml .= $ip ? " <ipaddress>$ip</ipaddress>\n":"";
	$xml .= " <pageURL>http://ifrogz.com</pageURL>\n";
	$xml .= " <pageName>Keyword Mention</pageName>\n"; 
	
	// close the XML request
	$xml .= "</request>\n";
	
	if (sendxml($xml)) {
		setVariable('last_twitter_keyword_id', $last_keyword_id);
		echo "UPDATED last_twitter_keyword_id TO $last_keyword_id";
	}
	
	$i++;
}

//echo $xml; exit;




function sendxml($xml) {
	global $host;
	// Create POST, Host and Content-Length headers
	$head = "POST /b/ss//6 HTTP/1.0\n";
	$head .= "Host: $host\n";
	$head .= "Content-Length: ".(string)strlen($xml)."\n\n"; 

	// combine the head and XML
	$request = $head.$xml; 

	//$fp=fsockopen($host,80,$errno,$errstr,30); 
	// Use this function in place of the call above if you have PHP 4.3.0 or
	//   higher and have compiled OpenSSL into the build.
	//
	$fp = pfsockopen("ssl://".$host, 443, $errno, $errstr);
	// 
	if( $fp ) { 

		// send data
		fwrite($fp,$request); 

		// get response
		$response="";
		while( !feof($fp) ){
			$response .= fgets($fp,1028);
		}
		fclose($fp); 

		// display results
		
		echo "RESULTS:\n";
		print_r($response);
		echo "\n";
		

		// check for errors
		if( preg_match("/status\>FAILURE\<\/status/im",$response) ) {
			/*
			* TODO:
			* write $request and $response to log file for investigation
			* and retries
			*/
			echo "<h1>Failure</h1>\n";
			echo "<p>Note the reason tag in the response, fix and try again.</p>\n";
		} else {
			// SUCCESS
			return TRUE;
		}
	} else {
		echo "<H1>Couldn't open port to SiteCatalyst servers</H1>\n";

		echo "<p>$errstr ($errno)</p>\n";
		/*
		* TODO:
		* write $request and $errstr to log file for investigation
		* and retries
		*/
	}	
}

function getVariable($name) {
	$sql = "SELECT * FROM globals WHERE name = '$name'";
	$res = mysql_query($sql);
	if ($res) {
		if (mysql_num_rows($res) > 0) {
			$row = mysql_fetch_assoc($res);
			return $row['value'];
		}
	}
	return FALSE;
}

function setVariable($name, $val) {
	$sql = "UPDATE globals SET value = '$val' WHERE name = '$name'";
	$res = mysql_query($sql);
	if ($res) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function debug($var) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

/**
 * Example mentions JSON
 */

/*
stdClass Object
(
    [results] => Array
        (
            [0] => stdClass Object
                (
                    [profile_image_url] => http://a3.twimg.com/profile_images/77662337/MIBranchInsignia_normal.jpg
                    [created_at] => Fri, 14 May 2010 17:24:48 +0000
                    [from_user] => SJU87
                    [metadata] => stdClass Object
                        (
                            [result_type] => recent
                        )

                    [to_user_id] => 13061326
                    [text] => @ifrogz my luxe case broke, the thin plastic connections on the side cracked - also need to fix the back circle latch, phone won't lie flat
                    [id] => 13987579913
                    [from_user_id] => 4751142
                    [to_user] => ifrogz
                    [geo] => 
                    [iso_language_code] => en
                    [source] => <a href="http://twitter.com/">web</a>
                )

            [1] => stdClass Object
                (
                    [profile_image_url] => http://a3.twimg.com/profile_images/737053457/small_cat_normal.jpg
                    [created_at] => Fri, 14 May 2010 16:33:45 +0000
                    [from_user] => mitch1066
                    [metadata] => stdClass Object
                        (
                            [result_type] => recent
                        )

                    [to_user_id] => 13061326
                    [text] => @iFrogz is giving away a set of CS40s Ear Phones @ #NetworkingWitches http://tinyurl.com/22qf6hc
                    [id] => 13985158634
                    [from_user_id] => 22626279
                    [to_user] => ifrogz
                    [geo] => 
                    [iso_language_code] => en
                    [source] => <a href="http://www.tweetdeck.com" rel="nofollow">TweetDeck</a>
                )

            [2] => stdClass Object
                (
                    [profile_image_url] => http://a1.twimg.com/profile_images/198705208/rent_logo_normal.jpg
                    [created_at] => Fri, 14 May 2010 15:52:18 +0000
                    [from_user] => broadway_fan
                    [metadata] => stdClass Object
                        (
                            [result_type] => recent
                        )

                    [to_user_id] => 13061326
                    [text] => @ifrogz when will the iPad cases be available?
                    [id] => 13983193131
                    [from_user_id] => 15343093
                    [to_user] => ifrogz
                    [geo] => 
                    [iso_language_code] => en
                    [source] => <a href="http://www.atebits.com/" rel="nofollow">Tweetie</a>
                )

        )

    [max_id] => 13987579913
    [since_id] => 13971648803
    [refresh_url] => ?since_id=13987579913&q=%40ifrogz
    [results_per_page] => 15
    [page] => 1
    [completed_in] => 0.024789
    [query] => %40ifrogz
)*/


/**
 * Example From JSON
 */

/*
stdClass Object
(
    [results] => Array
        (
            [0] => stdClass Object
                (
                    [profile_image_url] => http://a3.twimg.com/profile_images/395326069/ifrogzbutton2_normal.jpg
                    [created_at] => Fri, 14 May 2010 16:06:24 +0000
                    [from_user] => ifrogz
                    [metadata] => stdClass Object
                        (
                            [result_type] => recent
                        )

                    [to_user_id] => 15343093
                    [text] => @broadway_fan We're anticipating early June, I'll be sure to let everyone know!
                    [id] => 13983860817
                    [from_user_id] => 13061326
                    [to_user] => broadway_fan
                    [geo] => 
                    [iso_language_code] => en
                    [source] => <a href="http://www.tweetdeck.com" rel="nofollow">TweetDeck</a>
                )

            [1] => stdClass Object
                (
                    [profile_image_url] => http://a3.twimg.com/profile_images/395326069/ifrogzbutton2_normal.jpg
                    [created_at] => Thu, 13 May 2010 20:42:26 +0000
                    [from_user] => ifrogz
                    [metadata] => stdClass Object
                        (
                            [result_type] => recent
                        )

                    [to_user_id] => 
                    [text] => Great visual of BlackBerry vs. iPhone from GigaOM: http://bit.ly/9YVTmz
                    [id] => 13933553063
                    [from_user_id] => 13061326
                    [geo] => 
                    [iso_language_code] => en
                    [source] => <a href="http://www.tweetdeck.com" rel="nofollow">TweetDeck</a>
                )

            [2] => stdClass Object
                (
                    [profile_image_url] => http://a3.twimg.com/profile_images/395326069/ifrogzbutton2_normal.jpg
                    [created_at] => Thu, 13 May 2010 16:13:16 +0000
                    [from_user] => ifrogz
                    [metadata] => stdClass Object
                        (
                            [result_type] => recent
                        )

                    [to_user_id] => 
                    [text] => Review of the custom MyFrogz Luxe case from andPOP - have a look: http://bit.ly/aiyVW1
                    [id] => 13921836679
                    [from_user_id] => 13061326
                    [geo] => 
                    [iso_language_code] => en
                    [source] => <a href="http://www.tweetdeck.com" rel="nofollow">TweetDeck</a>
                )
        )

    [max_id] => 13986400056
    [since_id] => 0
    [refresh_url] => ?since_id=13986400056&q=from%3Aifrogz
    [results_per_page] => 15
    [page] => 1
    [completed_in] => 0.050998
    [query] => from%3Aifrogz
)
*/