<?php
/***********************************************************************
| Froogle developed by 68 Designs, LLC. 
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2004, 68 Designs LLC 
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
|
| File: froogle.php
|
| Purpose: The froogle page, exports your inventory into froogle's format.
|
| Developers involved with this file: 
|		Eric Barnes  http://www.68designs.com/contact-support.php [EB]
|
| ______________________________________________________________________
|	http://www.68designs.com/
***********************************************************************/
error_reporting (E_ALL ^ E_NOTICE);
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');

//get the values//
$sSQL = "SELECT * FROM froogle";
$result = mysql_query($sSQL) or die(mysql_error());
$sql=mysql_fetch_array($result); 

	$ftp_server = $sql['ftp_server'];
	$ftp_user_name = $sql['ftp_user_name'];
	$ftp_user_pass = $sql['ftp_user_pass'];
	$email = $sql['email'];
	$fullurl = $sql['fullurl'];
	$sitename = $sql['sitename'];
	$destination = $sql['destination'];
	$pDescription = $sql['description'];
	$time = $sql['time'];

//debug	
		if (@$_GET['debug'] == "TRUE") {
		echo $ftp_server . "<br";
		echo "FTP Username ". $ftp_user_name ."<br>";
		echo "FTP Password ". $ftp_user_pass ."<br>";
		echo "email ". $email ."<br>";
		echo "Site Name ". $sitename ."<br>";
		echo "Destination ". $destination ."<br>";
		echo "Time ". $time ."<br>";
		echo "Description ". $pDescription ."<br>";
		echo "Full URL ". $fullurl ."<br>";	
	}
	$output = "product_url\t name\t description\t price\t image_url\t category\n";

 
		 $sSQL = ("SELECT pID, pName, pSection, pDescription, pLongdescription, pLargeimage, pPrice, pDisplay FROM products WHERE pDisplay = 1 AND pPrice > 0");
		 $result = mysql_query($sSQL) or die(mysql_error());
		 $rows = mysql_num_rows($result);

			while ($row_froogle = mysql_fetch_array($result)) {
					$link = $row_froogle['pID'];
					$name = trim($row_froogle['pName']);
					$name = strip_tags(nl2br($name));
					$name = str_replace("\n", "", $name);
					$name = str_replace("\r", "", $name);
										
					$description = trim($row_froogle['pDescription']);
					$description = strip_tags(nl2br($description));
					$description = str_replace("\n", "", $description);
					$description = str_replace("\r", " ", $description);
					$description = str_replace("  ", "", $description);
					$description = str_replace("   ", "", $description);
					$description = str_replace("\t", "", $description);
					
					$longdescription = trim($row_froogle['pLongdescription']);
					$longdescription = strip_tags(nl2br($longdescription));
					$longdescription = str_replace("\n", "", $longdescription); 
					$longdescription = str_replace("\r", " ", $longdescription); 
					$longdescription = str_replace("  ", "", $longdescription);
					$longdescription = str_replace("   ", "", $longdescription);
					$longdescription = str_replace("\t", "", $longdescription);
					
					$price = $row_froogle['pPrice'];
					$price = number_format($price,2,'.',',');
					$image = $row_froogle['pLargeimage'];
					$image1 = $row_froogle['pImage'];
					$sectionid = $row_froogle['pSection'];
		$output .= "$fullurl/proddetail.php?prod=$link\t$name\t";
			if ($pDescription == "pLongdescription") 
				{
				$output .= "$longdescription";
				}
			else 
				{
				$output .= "$description";
				}
		$output .= "\t$price\t";

		//image //
			if ($image <> "" && $image <> "prodimages/") {
				$output .=  $fullurl ."/". $image;             // print filename
			}elseif($image1 <> "" && $image1 <> "prodimages/") {
				$output .=  $fullurl ."/". $image1;             // print filename
			}
			else {
				$output .= "";
			}
//finally the category//
$tslist = "";
$thetopts = $sectionid;
$topsectionids = $sectionid;
$isrootsection=FALSE;
for($index=0; $index <= 10; $index++){
	if($thetopts==0){
		$tslist = 'Home' . $tslist;
		break;
	}elseif($index==10){
		$tslist = "<b>Loop</b>" . $tslist;
	}else{
		$sSQL = "SELECT sectionID,topSection,sectionName,rootSection FROM sections WHERE sectionID=" . $thetopts;
		$result2 = mysql_query($sSQL) or print(mysql_error());
		if(mysql_num_rows($result2) > 0){
			$rs2 = mysql_fetch_assoc($result2);
			if($rs2["sectionID"]==(int)$catid) $isrootsection = ($rs2["rootSection"]==1);
			if($rs2["rootSection"]==1){
				if($isrootsection)
					$tslist = ' > ' . $rs2["sectionName"] . $tslist;
				else
					$tslist = ' >' . $rs2["sectionName"]  . $tslist;
			}else
				$tslist = ' > ' . $rs2["sectionName"] . $tslist;
			$thetopts = $rs2["topSection"];
			$topsectionids .= "," . $thetopts;
		}else{
			$tslist = "Top Section Deleted" . $tslist;
			break;
		}
		mysql_free_result($result2);
	}
}
		$output .= "\t$tslist\n";

}//end while

//now we write to the file.
$ext = '.txt';
$filename = "/ectmods/froogle/froogle.txt";
if (is_writable($filename)) {
    if (!$handle = fopen($filename, 'w+')) {
         $error .= "Cannot open file ($filename)\n\r";
         exit();
    }
    if (!fwrite($handle, $output)) {
       $error .= "Cannot write to file ($filename)\r\n";
        exit();
    }
 //  print "Success, wrote to file ($filename)<br /> <a href=\"ftp.php\">Click here to send it</a>";
 //  print "<br /><a href=\"froogle.txt\">Or click here if you want to see the file and make it sure it looks right.</a>";
    //now the ftp portion
					$ext = '.txt';
					$filename = $destination .$ext;
					$destination_file = "$filename";
					$source_file = "ectmods/froogle/froogle.txt";
					if(@$_GET['debug'] == TRUE) {
						$email="eric@68designs.com";
					}
					// set up basic connection
					$conn_id = ftp_connect($ftp_server); 
					
					// login with username and password
					$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 
					
					// turn passive mode on
					ftp_pasv($conn_id, true);
					
					// check connection
					if ((!$conn_id) || (!$login_result)) { 
							$error .= "FTP connection has failed!";
							$error .= "Attempted to connect to $ftp_server for user $ftp_user_name"; 
							exit(); 
						} else {
							//echo "Connected to $ftp_server, for user $ftp_user_name";
						}
					
					// upload the file
					$upload = @ftp_put($conn_id, $destination_file, $source_file, FTP_BINARY); 
					
					// check upload status
					if (!$upload) { 
							//echo "FTP upload has failed!";
					$comment = "I am sorry there was an error when uploading your inventory.  Normally this will happen if you have submitted a feed and Froogle hasn't processed it yet.";
					$message = $comment ." ". $error;
					mail("$email", "Error with Froogle Upload - $sitename", $message,
						 "From: webmaster@{$_SERVER['SERVER_NAME']}\r\n"
						."Reply-To: webmaster@{$_SERVER['SERVER_NAME']}\r\n"
						."X-Mailer: PHP/" . phpversion());
					
						} else {
						   // echo "Uploaded $source_file to $ftp_server as $destination_file";
					$message = "Your inventory has been uploaded. Thanks for using the Froogle Uploader from ECT Mods\n\r";
					$message .= $rows ." products was successfully sent."; 
					mail("$email", "Froogle Upload", $message,
						 "From: noreply@ectmods.com\r\n"
						."Reply-To: noreply@ectmods.com\r\n"
						."X-Mailer: PHP/" . phpversion());	
					}
					// close the FTP stream 
					ftp_close($conn_id); 
					if(@$_GET['debug'] == TRUE) {
						echo "Email: ". $email ."<br>";
						echo "Message: ". $message ."<br>";
						echo "Error: ". $error ."<br>";
					}

    fclose($handle);
} else {
    $error .= "The file $filename is not writable. Please chmod it 666\n\r";
}
if(@$_GET['debug'] == TRUE) {
	echo "Error: ". $error ."<br>";
}
?>