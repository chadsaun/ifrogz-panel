<?php
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if (!$login==TRUE){
if (@$storesessionvalue=="") $storesessionvalue="virtualstore";
if (@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])=="") {
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Froogle</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>
    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		     <h1>Froogle Mod</h1>
		     <p align="center"></p>
		     <?php
#check if install is still uploaded
$filename = 'setup.php';
if(file_exists("setup.php"))
			{
				die("<center><a href=\"setup.php\">Click here to start the installation</a><br><b>Warning: Please delete the setup file after you install.!</b></center>");
			}
if (@$_GET['debug'] == TRUE) {
	phpinfo();
}else {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
			$theValue = ($theValue != "") ? "'" . date("Y-m-d",strtotime($theValue)) . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $HTTP_SERVER_VARS['PHP_SELF'];
if (isset($HTTP_SERVER_VARS['QUERY_STRING'])) {
  $editFormAction .= "?" . $HTTP_SERVER_VARS['QUERY_STRING'];
}

if ((isset($HTTP_POST_VARS["MM_update"])) && ($HTTP_POST_VARS["MM_update"] == "account_edit")) {
  $updateSQL = sprintf("UPDATE froogle SET ftp_server=%s, ftp_user_name=%s, ftp_user_pass=%s, email=%s, sitename=%s, destination=%s, fullurl=%s, description=%s, `time`=%s WHERE id=%s",
                       GetSQLValueString($HTTP_POST_VARS['ftp_server'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['ftp_user_name'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['ftp_user_pass'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['email'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['sitename'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['destination'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['fullurl'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['description'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['time'], "text"),
                       GetSQLValueString($HTTP_POST_VARS['id'], "int"));

  mysql_select_db($db_name, $dbh);
  $Result1 = mysql_query($updateSQL, $dbh) or die(mysql_error());
}

mysql_select_db($db_name, $dbh);
$query_froogle = "SELECT id, `sitekey`, ftp_server, ftp_user_name, ftp_user_pass, email, sitename, destination, fullurl, description, `time` FROM froogle";
$froogle = mysql_query($query_froogle, $dbh) or die(mysql_error());
$row_froogle = mysql_fetch_assoc($froogle);
$totalRows_froogle = mysql_num_rows($froogle);
?>
<?php
	if ($_POST['MM_update']) {
		echo "Your changes have been accepted<br> You will be returned to the main page in 10 seconds";
		echo '<meta http-equiv="refresh" content="8;url=index.php"><br>';
		echo 'or <a href="index.php">Click Here</a> if you do not want to wait.';
	} else {
?>
<form method="POST" action="<?php echo $editFormAction; ?>" name="account_edit">
<table border="0" width="90%" align="center">
	<tr><td>
      <p align="center"><b>Setup<br>
&nbsp;</b></td></tr>
  <tr>
    <td align="center">Enter the full URL to your shopping cart directory excluding
      the final slash.<br>
    <font size="2"><?php
						$guessURL = "http://" . @$_SERVER["SERVER_NAME"] . @$_SERVER["REQUEST_URI"];
						$guessURL = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
						$wherevs = strpos(strtolower($guessURL),"/vsadmin");
						if($wherevs > 0)
							$guessURL = substr($guessURL, 0, $wherevs);
						else
							$guessURL = "http://www.myurl.com/mystore/";
						print $guessURL;
						?></font></td>
  </tr>
  <tr>
    <td align="center">      <label><input name="id" type="hidden" value="<?php echo $row_froogle['id']; ?>">
      <b>Shopping Cart URL: </b>
      <input name="fullurl" type="text" id="fullurl" value="<?php echo $row_froogle['fullurl']; ?>" size="30">
    </label>
      <hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center">Enter the FTP Server<span class="small"> Froogle 
    has assigned</span> </td>
  </tr>
  <tr>
    <td align="center"><b>FTP Server: </b><input name="ftp_server" type="text" id="ftp_server" value="<?php echo $row_froogle['ftp_server']; ?>" size="30">    
    <hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center"><span class="small">Enter your Froogle 
    UserName</span> </td>
  </tr>
  <tr>
    <td align="center"><b>FTP User Name: </b><input name="ftp_user_name" type="text" id="ftp_user_name" value="<?php echo $row_froogle['ftp_user_name']; ?>" size="30">
    <hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center"><span class="small">Enter your Froogle Password</span> </td>
  </tr>
  <tr>
    <td align="center"><b>Password:</b> <input name="ftp_user_pass" id="ftp_user_pass" value="<?php echo $row_froogle['ftp_user_pass']; ?>" size="30"><hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center"><span class="small">Enter your Email address for feed 
    upload confirmation</span></td>
  </tr>
  <tr>
    <td align="center"><b>Email:</b> <input name="email" type="text" id="email" value="<?php echo $row_froogle['email']; ?>" size="30">
    <hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center">&nbsp;<span class="small">Your site's name </span></td>
  </tr>
  <tr>
    <td align="center"><b>Site Name:</b> <input name="sitename" type="text" id="sitename" value="<?php echo $row_froogle['sitename']; ?>" size="30"><hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center">File name for the Froogle feed file.<br>
    <font size="2">(This is usually your FTP Username. Do not use a file extension!)</font></td>
  </tr>
  <tr>
    <td align="center"><b>Feed File Name: </b><input name="destination" type="text" id="destination" value="<?php echo $row_froogle['destination']; ?>" size="30">
      <hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center">Product description field. You may select the short or 
    long or description field for Froogle to use. If you select the long
        description and do not have any text in that field, the short 
    description will be used.</td>
  </tr>
  <tr>
      <td align="center"><p>
        <label>
        <input <?php if (!(strcmp($row_froogle['description'],"pLongdescription"))) {echo "CHECKED";} ?> type="radio" name="description" value="pLongdescription">
        Long Description</label>
        <br>
        <label>
        <input <?php if (!(strcmp($row_froogle['description'],"pDescription"))) {echo "CHECKED";} ?> type="radio" name="description" value="pDescription">
        Short Description</label><hr color="#000000" width="70%"></td>
  </tr>
  <tr>
    <td align="center">You may set to update Froogle automatically by selecting 
    from the options below. Froogle requires that feeds be updated at least once 
    per month and not more than once per day.</td>
  </tr>
  <tr>
    <td align="center"><p>
        <label>
        <input <?php if (!(strcmp($row_froogle['time'],"day"))) {echo "CHECKED";} ?> type="radio" name="time" value="day">
        Daily</label>
        <br>
        <label>
        <input <?php if (!(strcmp($row_froogle['time'],"week"))) {echo "CHECKED";} ?> type="radio" name="time" value="week">
        Weekly</label>
        <br>
        <label>
        <input <?php if (!(strcmp($row_froogle['time'],"month"))) {echo "CHECKED";} ?> type="radio" name="time" value="month">
        Monthly</label>
        (every 30 days) <br>
        <br>
      </p>
      </td>
  </tr>
  <tr>
    <td><div align="center">
      <input type="submit" name="Submit" value="Submit">
    </div></td>
    </tr>
</table>
<input type="hidden" name="MM_update" value="account_edit">
</form>
<?php }} ?>
        <!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
