<?php
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if (!$login==TRUE){
if (@$storesessionvalue=="") $storesessionvalue="virtualstore";
if (@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])=="") {
	header('Location: /user/login/');
	exit();
}
}
$row_count = 0; 
$color1="row1";
$color2="row2";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>
<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>
    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav">
         <?php include("navigation.php"); ?>
    </div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		     <h1>Welcome to ECT Mods Control Panel </h1>
		     <table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
			 	<tr>
					<th>Modification</th><th>Version</th>
				</tr>
			 <?php 
			 $sSQL="SELECT * FROM ect_mods ORDER BY eName";
			 $result=mysql_query($sSQL) or print(mysql_error()); 
			 while($rs=mysql_fetch_array($result)){
			 $row_color = ($row_count % 2) ? $color2 : $color1;
			 ?>
                  <tr>
                       <td class="<?php echo $row_color; ?>"><a href="<?php echo $rs['eURL']; ?>"><?php echo $rs['eName']; ?></a></td>
                       <td class="<?php echo $row_color; ?>"><?php echo $rs['eVersion']; ?></td>
                  </tr>
				  <?php 
				  	$row_count++;
				  } 
				  ?>
                 <tr> 
				<td width="100%" colspan="2" align="left">&nbsp;&nbsp;<a href="/admin/logout.php"><strong><?php print $yyLOut?></strong></a> </td>
			  </tr>
             </table>
		     <p>&nbsp;</p>
		     
		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
