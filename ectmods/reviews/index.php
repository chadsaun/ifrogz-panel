<?php
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Product Reviews</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>
    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		     <h1>Product Reviews </h1>
<?php
#check if install is still uploaded
$filename = 'install.php';
if(file_exists("install.php"))
			{
				die("<center><a href=\"install.php\">Click here to start the installation</a><br><b>Warning: Please delete the setup file after you install.!</b></center>");
			}
if(isset($_POST['action']) && $_POST['action'] == "GO") {

	$review = unstripslashes(@$_POST['revReview']);
	$sSQL = "UPDATE reviews SET 
				revPerson='" . mysql_real_escape_string(@$_POST["revPerson"]) . "',
				revProd='" . mysql_real_escape_string(@$_POST["revProd"]) . "',
				revTitle='". mysql_real_escape_string(@$_POST["revTitle"]) . "',
				revEmail='". mysql_real_escape_string(@$_POST["revEmail"]) . "',
				revURL='". mysql_real_escape_string(@$_POST["revURL"]) . "',
				revScore='". mysql_real_escape_string(@$_POST["revScore"]) . "',
				revReview='". mysql_real_escape_string($review) . "',
				revApproved='". mysql_real_escape_string(@$_POST["revApproved"]) . "'
				WHERE revID=".$_POST['revID']." LIMIT 1";
	mysql_query($sSQL) or print(mysql_error());
	print '<meta http-equiv="refresh" content="1; url=index.php">';	
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><br><b><?php print $yyUpdSuc?></b><br><br><?php print $yyNowFrd?><br><br>
                        <?php print $yyNoAuto?> <A href="index.php"><b><?php print $yyClkHer?></b></a>.<br>
                        <br>
                </td>
			  </tr>
			</table>
	<?php
}elseif(isset($_POST['act']) && $_POST['act']=="delete"){
	if(isset($_POST['id'])) $id=$_POST['id'];
	$sSQL="DELETE FROM reviews WHERE revID = ".$id." LIMIT 1";
	$result=mysql_query($sSQL) or print(mysql_error());
	print '<meta http-equiv="refresh" content="1; url=index.php">';	
	?>
	<table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="">
			  <tr> 
                <td width="100%" colspan="2" align="center"><br><b><?php print $yyUpdSuc?></b><br><br><?php print $yyNowFrd?><br><br>
                        <?php print $yyNoAuto?> <A href="index.php"><b><?php print $yyClkHer?></b></a>.<br>
                        <br>
                </td>
			  </tr>
			</table>
<?php 
}elseif(isset($_POST['act']) && $_POST['act']=="modify") { 
		if(isset($_POST['id'])) $id=$_POST['id'];
		$sSQL="SELECT * FROM reviews WHERE revID = ".$id;
		$result=mysql_query($sSQL) or print(mysql_error());
		$rs=mysql_fetch_array($result);
		$review = $rs['revReview'];
		?>

		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<table width="500" border="0" align="center" cellpadding="5" cellspacing="0" style="border:1px solid #000; ">
		<tr>
		  <td colspan="2"><div align="center"><strong>Modify A Review</strong></div></td>
	  </tr>
		<tr>
		  <td>Name:</td>
          <td><input name="revPerson" type="text" id="revPerson" value="<?php echo $rs['revPerson']; ?>" size="25"></td>
  </tr>
		<tr>
		  <td>Title:</td>
		  <td><input name="revTitle" type="text" id="revTitle" value="<?php echo $rs['revTitle']; ?>" size="25"></td>
  </tr>
		<tr>
		  <td>Email:</td>
		  <td><input name="revEmail" type="text" id="revEmail" value="<?php echo $rs['revEmail']; ?>" size="25"></td>
  </tr>
		<tr>
		  <td>URL:</td>
		  <td><input name="revURL" type="text" id="revURL" value="<?php echo $rs['revURL']; ?>" size="25"></td>
  </tr>
		<tr>
		  <td>Score:</td>
		  <td><input name="revScore" type="text" id="revScore" value="<?php echo $rs['revScore']; ?>" size="2"></td>
  </tr>
		<tr>
		  <td>Approved:</td>
		  <td>
		    <select name="revApproved" id="revApproved">
		         <option value="Y"<?php if($rs['revApproved']=="Y") echo " SELECTED"; ?>>Yes</option>
		         <option value="N"<?php if($rs['revApproved']=="N") echo " SELECTED"; ?>>No</option>
		         </select>
		   </td>
  </tr>
		<tr>
		  <td>Product:</td>
		  <td><input name="revProd" type="text" id="revProd" value="<?php echo $rs['revProd']; ?>" size="25"></td>
  </tr>
		<tr>
		  <td>Review:</td>
		  <td><textarea name="revReview" cols="40" rows="10" id="revReview"><?php echo $review; ?></textarea></td>
  </tr>
		<tr>
		  <td colspan="2"><div align="center">
	        <input type="submit" name="Submit" value="Submit">
	        <input name="revID" type="hidden" id="revID" value="<?php echo $rs['revID']; ?>">
			<input type="hidden" name="action" value="GO">
	      </div></td>
    </tr>
	</table>
</form>
<?php
		mysql_free_result($result);
		}else{
		?>
<?php
$sSQL="SELECT * FROM reviews";
$result=mysql_query($sSQL) or print(mysql_error());
$rows=mysql_num_rows($result);
mysql_free_result($result);

$sSQL="SELECT * FROM reviews WHERE revApproved='N'";
$result=mysql_query($sSQL) or print(mysql_error());
$unapproved=mysql_num_rows($result);
mysql_free_result($result);

?>
	<table width="400" border="0" align="center" cellpadding="5" cellspacing="0" style="border:1px solid #000; ">
		<tr>
		  <td colspan="2"><div align="center"><strong>Quick Stats </strong></div></td>
	  </tr>
		<tr><td width="50%"><div align="right"><strong>Total reviews: </strong></div></td>
	    <td><?php echo $rows; ?></td></tr>
		<tr>
		  <td><div align="right"><strong>Unapproved Reviews: </strong></div></td>
		  <td><?php echo $unapproved; ?> </td>
	  </tr>
	</table>
    <p>&nbsp;</p>
	
<script language="JavaScript" type="text/javascript">
<!--
function modify(id) {
	document.mainform.id.value = id;
	document.mainform.act.value = "modify";
	document.mainform.submit();
}
function remove(id) {
cmsg = "Sure you want to delete this record?\n"
if (confirm(cmsg)) {
	document.mainform.id.value = id;
	document.mainform.act.value = "delete";
	document.mainform.submit();
}
}
// -->
</script>
<form method="post" action="index.php" name="mainform">
	<input type="hidden" name="posted" value="1" />
	<input type="hidden" name="act" value="xxxxx" />
	<input type="hidden" name="id" value="xxxxx" />
		<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">

		<tr>
		  <th colspan="7"><div align="center"><strong>Latest Reviews </strong></div></th>
	  </tr>
		<tr><td><strong>Name</strong></td>
	    <td><strong>Email</strong></td>
		<td><strong>Product</strong></td>
		<td><strong>Score</strong></td>
		<td><strong>Approved</strong></td>
		<td><strong>Modify</strong></td>
		<td><strong>Delete</strong></td>
		</tr>
		<?php
		$row_count = 0; 
		$color1="row1";
		$color2="row2";
		$sSQL="SELECT * FROM reviews ORDER BY revDate DESC";
		$result=mysql_query($sSQL) or print(mysql_error());
		while($rs=mysql_fetch_array($result)) {
		$row_color = ($row_count % 2) ? $color2 : $color1;
		?>
		<tr>
		  <td class="<?php echo $row_color; ?>"><?php echo $rs['revPerson']; ?></td>
		  <td class="<?php echo $row_color; ?>"><?php echo $rs['revEmail']; ?></td>
	      <td class="<?php echo $row_color; ?>"><?php echo $rs['revProd']; ?></td>
	      <td class="<?php echo $row_color; ?>"><?php echo $rs['revScore']; ?></td>
	      <td class="<?php echo $row_color; ?>"><?php echo $rs['revApproved']; ?></td>
	      <td class="<?php echo $row_color; ?>"><input type="button" value="Modify" onclick="modify('<?php echo $rs['revID']; ?>')" /></td>
		  <td class="<?php echo $row_color; ?>"><input type="button" value="Delete" onclick="remove('<?php echo $rs['revID']; ?>')" /></td>
		</tr>
		<?php
		$row_count++;
		}
		mysql_free_result($result);
		?>
	</table>
	</form>
        <p>&nbsp;</p>
<?php } ?>				   
        <!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
