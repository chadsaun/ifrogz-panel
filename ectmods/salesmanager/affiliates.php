<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.2 $
| Updated: $Date: 2005/10/10 13:48:27 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>
<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>

    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		     <h1>Affiliate Sales</h1>
<?php
	$sSQL="SELECT affilID, affilName FROM affiliates ORDER BY affilName";
	$result = mysql_query($sSQL) or die(mysql_error());
?>
<script src="../javascript/sortable.js"></script>
<table  class="sortable" id="results" width="100%"  border="0" cellpadding="2" cellspacing="3"> 
  <tr>
    <th width="40%">Affiliate</th>
    <th>Amount Sold </th>
  </tr>
<?php 
$row_count = 0; 
$color1="row1";
$color2="row2";
while($rs = mysql_fetch_array($result)){
		$id 		= 	$rs['affilID'];
		$name 		= 	$rs['affilName'];
	$row_color = ($row_count % 2) ? $color2 : $color1;
	?>
  <tr>
    <td class="<?php echo $row_color; ?>"><a href="productinfo.php?affiliate=<?php echo $id; ?>"><?php echo $name; ?></a></td>
    <td class="<?php echo $row_color; ?>">
			<?php
			$sSQL2 = "SELECT SUM(ordTotal-ordDiscount) FROM affiliates LEFT JOIN orders ON affiliates.affilID=orders.ordAffiliate WHERE affilID='" . $id . "' AND ordStatus>=3";
			$alldata2 = mysql_query($sSQL2) or print(mysql_error());
			$rs2=mysql_fetch_array($alldata2);
			if(! is_numeric($rs2[0])){
				print "-";
				$thistotal=0.0;
			}else{
				print FormatEuroCurrency($rs2[0]);
				$thistotal=(double)$rs2[0];
			}
			$grandtotal+=$thistotal;
			 ?>
	</td>
  </tr>
<?php $row_count++; } ?>
</table>
<div align="center" style="font-weight: bold">Grand Total: <?php echo FormatEuroCurrency($grandtotal); ?></div>
		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
