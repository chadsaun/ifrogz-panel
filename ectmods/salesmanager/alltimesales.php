<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.2 $
| Updated: $Date: 2005/10/10 13:48:28 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>
<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>

    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		     <h1>All Time Sales</h1>
<div align="center">
<form name="form" action="alltimesales.php" method="get">
  <select name="view">
  <option value="5" <? if (@$_GET['view'] == '5') echo "selected"; ?>>View Top 5</option>
    <option value="10" <? if (@$_GET['view'] == '10') echo "selected"; ?>>View Top 10</option>
    <option value="20"<? if (@$_GET['view'] == '20') echo "selected"; ?>>View Top 20</option>
    <option value="30"<? if (@$_GET['view'] == '30') echo "selected"; ?>>View Top 30</option>
    <option value="40"<? if (@$_GET['view'] == '40') echo "selected"; ?>>View Top 40</option>
  </select>
  &nbsp;
  <select name="date">
  <option value="7" <? if (@$_GET['date'] == '7') echo "selected"; ?>>Last 7 Days</option>
    <option value="14" <? if (@$_GET['date'] == '14') echo "selected"; ?>>Last 14 Days</option>
	  <option value="30" <? if (@$_GET['date'] == '30') echo "selected"; ?>>Last 30 Days</option>
	    <option value="90" <? if (@$_GET['date'] == '90') echo "selected"; ?>>Last 90 Days</option>
		  <option value="365" <? if (@$_GET['date'] == '365') echo "selected"; ?>>Last Year</option>
  </select> &nbsp;
  <input type="submit" name="Submit" value="Go">
</form>
</div>
<?php
	$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, SUM(cartQuantity) AS quantity FROM cart WHERE cartCompleted = '1'";
	if (@$_GET['date'] <> "") {
		$sSQL.= " AND TO_DAYS('" . date('Y-m-d H:i:s') . "') - TO_DAYS(cartDateAdded) <= ".@$_GET['date'];
	}
	else {
		$sSQL.= " AND TO_DAYS('" . date('Y-m-d H:i:s') . "') - TO_DAYS(cartDateAdded) <= 7";
	}
	$sSQL.=" GROUP BY cartProdName ORDER BY quantity DESC";
		if (@$_GET['view'] == "") {
		$sSQL.= " LIMIT 5";
		}
	else {
		$sSQL.= " LIMIT ".@$_GET['view'];
	}
	$result = mysql_query($sSQL) or die(mysql_error());
?>
<script src="../javascript/sortable.js"></script>
<table  class="sortable" id="results" width="100%"  border="0" cellpadding="2" cellspacing="3"> 
  <tr>
    <th width="40%">Product</th>
    <th width="40%">Amount Sold </th>
    <th>Total</th>
  </tr>
<?php 
$row_count = 0; 
$color1="row1";
$color2="row2";
if(isset($_GET['date'])){
	$date=$_GET['date'];
}else{
	$date=7;
}
while($rs = mysql_fetch_array($result)){
		$id 		= 	$rs['cartProdID'];
		$name 		= 	$rs['cartProdName'];
		$price 		= 	$rs['cartProdPrice'];
		$cartQuantity = $rs['quantity'];
		$quantity	=	$cartQuantity * $rs['quantity'];
		$total		=	$price * $cartQuantity;
		$grandtotal += $total;
	$row_color = ($row_count % 2) ? $color2 : $color1;
	?>
	
  <tr>
    <td class="<?php echo $row_color; ?>"><a href="productinfo.php?prodid=<?php echo $id; ?>&date=<?php echo $date; ?>"><?php echo $name; ?></a></td>
    <td class="<?php echo $row_color; ?>"><?php echo $cartQuantity; ?></td>
    <td class="<?php echo $row_color; ?>"><?php echo FormatEuroCurrency($total); ?></td>
  </tr>
<?php $row_count++; } ?>
</table>
<div align="right" style="font-weight: bold">Grand Total: <?php echo FormatEuroCurrency($grandtotal); ?></div>
		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
