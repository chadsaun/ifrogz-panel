<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.4 $
| Updated: $Date: 2005/10/10 13:48:28 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>

    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?> <!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		    <?php 
$aHandling=FALSE;
if(isset($_GET['year'])){
	$year=$_GET['year'];
}else{
	$year=date("Y");
}
$sSQL = "SELECT adminEmail,adminStoreURL,adminShipping,adminZipCode,adminUnits,adminPacking,adminHandling FROM admin WHERE adminID=1";
$result = mysql_query($sSQL) or print(mysql_error());
$rsAdmin = mysql_fetch_assoc($result);
	if($rsAdmin['adminHandling']>0){
		$aHandling=TRUE;
	}
mysql_free_result($result);
?>
<div align="center">
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" name="year">
<select name="year">
     <option value="2003"<?php if($year=="2003") echo " SELECTED"; ?>>2003</option>
     <option value="2004"<?php if($year=="2004") echo " SELECTED"; ?>>2004</option>
     <option value="2005"<?php if($year=="2005") echo " SELECTED"; ?>>2005</option>
     <option value="2006"<?php if($year=="2006") echo " SELECTED"; ?>>2006</option>
</select>&nbsp;
 <input type="submit" name="Submit" value="Refresh">
</form>
</div>
<script src="../javascript/sortable.js"></script>
<table  class="sortable" id="results" width="100%"  border="0" cellpadding="2" cellspacing="3"> 
  <tr>
    <th>Month</th>
    <th>Sales</th>
    <th>AVG Sale</th>
	<th>Revenue</th>
	<th>Discounts</th>
    <th>Taxes</th>
	<?php if($aHandling){ ?>
	<th>Handling</th>
	<?php } ?>
    <th>Total</th>
  </tr>
<?php 
$grandTotalSales=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-01-01' AND ordDate<'".$year."-01-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$janTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}	
?>

  <tr>
    <td>January</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($janTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-02-01' AND ordDate<'".$year."-02-28 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$febTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr class="row1">
    <td>February</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($febTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-03-01' AND ordDate<'".$year."-03-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());

$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$marTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
		$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr>
    <td>March</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($marTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-04-01' AND ordDate<'".$year."-04-30 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$aprTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr class="row1">
    <td>April</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($aprTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-05-01' AND ordDate<'".$year."-05-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$mayTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr>
    <td>May</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($mayTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-06-01' AND ordDate<'".$year."-06-30 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$junTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr class="row1">
    <td>June</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($junTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-07-01' AND ordDate<'".$year."-07-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$julTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr>
    <td>July</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($julTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-08-01' AND ordDate<'".$year."-08-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$augTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr class="row1">
    <td>August</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($augTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-09-01' AND ordDate<'".$year."-09-30 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$sepTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr>
    <td>September</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($sepTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-10-01' AND ordDate<'".$year."-10-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$octTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr class="row1">
    <td>October</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($octTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-11-01' AND ordDate<'".$year."-11-30 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$novTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	$totTax+=$tax;
}
?>

  <tr>
    <td>November</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($novTotal); ?></td>
  </tr>
<?php 
$sales=0;
$avgSale=0;
$totalSales=0;
$discount=0;
$tax=0;
$handling=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordHandling,ordStateTax,ordCountryTax,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus<>1 AND ordDate>'".$year."-12-01' AND ordDate<'".$year."-12-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$decTotal+=$rs['ordTot'];
		}
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
	
}
?>

  <tr class="row1">
    <td>December</td>
    <td><?php echo $sales; ?></td>
	<td><?php echo FormatEuroCurrency($avgSale); ?></td>
    <td><?php echo FormatEuroCurrency($totalSales); ?></td>
	<td><?php echo FormatEuroCurrency($discount); ?></td>
	<td><?php echo FormatEuroCurrency($tax); ?></td>
	<?php if($aHandling){ ?>
	<td><?php echo FormatEuroCurrency($handling); ?></td>
	<?php } ?>
	<td><?php echo FormatEuroCurrency($decTotal); ?></td>
  </tr>
</table>
<table width="100%">
	 <tr>
    <td><div align="right" style="font-weight: bold">Total Sales: </div></td>
    <td width="5%"><?php echo $grandTotalSales; ?></td>
  </tr>
    <tr>
       <td><div align="right" style="font-weight: bold">Total Taxes: </div></td>
       <td width="5%"><?php echo FormatEuroCurrency($totTax); ?></td>
  </tr>
<?php $revenue=$janTotal+$febTotal+$marTotal+$aprTotal+$mayTotal+$junTotal+$julTotal+$augTotal+$sepTotal+$octTotal+$novTotal+$decTotal; ?>

  <tr>
    <td><div align="right" style="font-weight: bold">Total Revenue:</div></td>
    <td width="5%"><?php echo FormatEuroCurrency($revenue); ?></td>
  </tr>
</table>


<?php 
//build chart
$data="bar_data[January]=".round($janTotal)."&bar_data[Febuary]=".round($febTotal)."&bar_data[March]=".round($marTotal)."&bar_data[April]=".round($aprTotal)."&bar_data[May]=".round($mayTotal)."&bar_data[June]=".round($junTotal);
$data.="&bar_data[July]=".round($julTotal)."&bar_data[August]=".round($augTotal)."&bar_data[September]=".round($sepTotal)."&bar_data[October]=".round($octTotal)."&bar_data[November]=".round($novTotal)."&bar_data[December]=".round($decTotal);
?>
<p align="center"><img src="charts/adepti_bar.php?<?php echo $data; ?>">&nbsp;</p>

		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
