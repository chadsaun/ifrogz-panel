<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.2 $
| Updated: $Date: 2005/10/10 13:48:29 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>

    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
<?php if(isset($_GET['action']) && $_GET['action']=="find"){ ?>
<?php 
	$sSQL="SELECT DISTINCT ordEmail,ordName FROM orders WHERE 1=1";
		$city=@trim($_REQUEST['city']);
		$state=@($_REQUEST['state']);
		$country=@($_REQUEST['country']);
		$zip=@trim($_REQUEST['zipcode']);
		$search=@$_REQUEST['which_search'];
		
		switch ($search) {
			case "shipping":
			   	$sSQL.=" AND (ordShipCity LIKE '%".$city."%' AND ordShipState LIKE '%".$state."%' AND ordShipCountry LIKE '%".$country."%' AND ordShipZip LIKE '%".$zip."%')";
			   break;
			case "billing":
			   	$sSQL.=" AND (ordCity LIKE '%".$city."%' AND ordState LIKE '%".$state."%' AND ordCountry LIKE '%".$country."%' AND ordZip LIKE '%".$zip."%')";
			   break;
			default:
				$sSQL.=" AND (ordCity LIKE '%".$city."%' AND ordState LIKE '%".$state."%' AND ordCountry LIKE '%".$country."%' AND ordZip LIKE '%".$zip."%')";
				$sSQL.=" OR (ordShipCity LIKE '%".$city."%' AND ordShipState LIKE '%".$state."%' AND ordShipCountry LIKE '%".$country."%' AND ordShipZip LIKE '%".$zip."%')";
			   break;
		}
		//echo $sSQL."<br>";
		$result=mysql_query($sSQL) or die(mysql_error());
		$total=mysql_num_rows($result);
?>
<h1><?php echo $total; ?> Customers found.</h1>
<?php if($total>0){ ?>
<script src="../javascript/sortable.js"></script>
<table  class="sortable" id="results" width="100%"  border="0" cellpadding="2" cellspacing="3"> 
   <tr>
    <th>Name</th>
    <th>Email</th>
  </tr>
<?php 
$row_count = 0; 
$color1="row1";
$color2="row2";
		while ($rs=mysql_fetch_array($result)){
		$row_color = ($row_count % 2) ? $color2 : $color1;
			echo "<tr><td class='".$row_color."'>".$rs['ordName'] ."</td><td class='".$row_color."'>". $rs['ordEmail']."</td></tr>";
		$row_count++;
		}
?>
</table>
<?php } ?>
<?php 
}else{
?>
<p>This search is used to find customers based on their location, you can then
     export the customers emails/details or send them highly targeted emails
     based on where they live. Excellent fo sending out special offers to everyone
     who lives in a specific city or country.</p>
<p>*TIP: you can enter just one search criteria, eg partial post code like 280
     will find all customers in the 280* area.</p>
<form name="form1" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                <div align="center"> </div>
                <p align="center"><strong>Find customers who live in the following 
                  areas: </strong></p>
			    <center>
                <table width="346" border="0" align="center" cellpadding="2" cellspacing="0" class="tableData">

                  <tr> 
                    <td>Search</td>

                    <td> 
                      <select name="which_search">
                        <option value="both">Billing &amp; Shipping Addresses</option>
                      	<option value="shipping">Shipping Address Only</option>
                      	<option value="billing">Billing Address Only</option>
                      </select>
                      </td>

                  </tr>
                  
                  <tr> 
                    <td>City</td>
                    <td> 
                      <input name="city" type="text" id="city" size="30">
                      </td>
                  </tr>
                  <tr> 
                    <td width="116">State/County</td>

                    <td width="220"> 
                      <input name="state" type="text" id="state" size="30">
                      </td>
                  </tr>
                  <tr> 
                    <td>Country</td>
                    <td> 
                     <select name="country" size="1">
<?php
function show_countries($tcountry){
	global $numhomecountries;
	$numhomecountries = 0;
	$sSQL = "SELECT countryName,countryOrder FROM countries WHERE countryEnabled=1 ORDER BY countryOrder DESC, countryName";
	$result = mysql_query($sSQL) or print(mysql_error());
	while($rs = mysql_fetch_array($result)){
		print '<option value="' . str_replace('"','&quot;',$rs["countryName"]) . '"';
		if($rs["countryOrder"]==2) $numhomecountries++;
		if($tcountry==$rs["countryName"])
			print " selected";
		print '>' . $rs["countryName"] . "</option>\n";
	}
}
show_countries(@$ordCountry)
?>
					</select>
                      </td>
                  </tr>
                  <tr> 
                    <td>*Zip/postal Code</td>

                    <td> 
                      <input name="zipcode" type="text" id="zipcode" size="15">
                      </td>
                  </tr>

                </table>
                <div align="center"><br>
                          <input type="hidden" name="action" value="find">
                          <input name="Submit" type="submit" value="Find Customers">
                         </p>
                </div>
</form>
<?php } ?>

		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
