<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.2 $
| Updated: $Date: 2005/10/10 13:48:29 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>
    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
		     <h1>Sales Manager </h1>
		     <table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
			 	<tr>
					<th colspan="2">Store Reports</th>
					</tr>
                  <tr>
                       <td width="50%" class="row1"><a href="annualreport.php">Annual Report</a></td>
                       <td class="row1"><p class="small">Display your store
                              annual revenue report for any given year.</p></td>
                  </tr>
                 <tr> 
				<td width="50%" align="left" class="row2"><a href="monthlysummary.php">Monthly Summary</a></td>
			    <td width="50%" align="left" class="row2"><p class="small">Displays a monthly summary.</p></td>
                 </tr>
                 <tr>
                      <td align="left" class="row1"><a href="top5sales.php">Monthly Top 5 Sellers</a></td>
                      <td align="left" class="row1"><p class="small">Display which products where
                              the top 5 sellers for each month. </p></td>
                 </tr>
                 <tr>
                      <td align="left" class="row2"><a href="alltimesales.php">Best
                              selling products</a></td>
                      <td align="left" class="row2"><p class="small">This report
                                displays the top selling products</p></td>
                 </tr>
                 <tr>
                      <td align="left" class="row1"><a href="producthits.php">Product Hits</a></td>
                      <td align="left" class="row1"><p class="small">This shows a list of your
                              products hits. </p></td>
                 </tr>
                 <tr>
                      <td align="left" class="row2"><a href="statesSales.php">State Sales </a></td>
                      <td align="left" class="row2"><p class="small">This report shows sales by
                              state. </p></td>
                 </tr>
                 <tr>
                      <td align="left" class="row1"><a href="countrySales.php">Country Sales </a></td>
                      <td align="left" class="row1"><p class="small">This report shows sales by
                              country </p></td>
                 </tr>
                 <tr>
                      <td align="left" class="row2"><a href="payprovider.php">Payment Provider </a></td>
                      <td align="left" class="row2">This reports displays sales
                           broken down by payment provider. </td>
                 </tr>
                 <tr>
                      <td align="left" class="row1"><a href="affiliates.php">Affiliates</a></td>
                      <td align="left" class="row1">This shows your affiliates
                           and displays what orders are associated with each
                           one. </td>
                 </tr>

             </table>       
			 <p>&nbsp;</p>
			 <table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
                  <tr>
                       <th colspan="2">Customer Reports</th>
                  </tr>
                  <tr>
                       <td width="50%" class="row1"><a href="customerfilter.php">Location
                                 Report </a></td>
                       <td class="row1"><p class="small">This allows you to find
                                 customers that match certain criteria.</p></td>
                  </tr>
                  <tr>
                       <td width="50%" align="left" class="row2"><a href="bigspenders.php">Big Spenders </a></td>
                       <td width="50%" align="left" class="row2"><p class="small">Displays
                                 a report of your big spenders.</p></td>
                  </tr>
                  <tr>
                       <td align="left" class="row1"><a href="top5sales.php">Monthly
                                 Top 5 Sellers</a></td>
                       <td align="left" class="row1"><p class="small">Display
                                 which products where the top 5 sellers for each
                                 month. </p></td>
                  </tr>
                  <tr>
                       <td align="left" class="row2"><a href="loyal.php">Find Loyal Customers</a></td>
                       <td align="left" class="row2"><p class="small">This report
                                 displays customers who have purchased more than
                                 one time.</p></td>
                  </tr>
                  <tr>
                       <td align="left" class="row1"><a href="coupon.php">Coupon Users</a></td>
                       <td align="left" class="row1"><p class="small">See which customers used
                              a coupon when purchasing</p></td>
                  </tr>
             </table>
		     <p>&nbsp;</p>
		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
