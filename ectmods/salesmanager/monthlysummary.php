<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.4 $
| Updated: $Date: 2005/10/10 13:48:30 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/ectmain.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<TITLE>Ecommerce Templates Mods</TITLE>
<!-- InstanceEndEditable -->
<link href="../style.css" rel="stylesheet" type="text/css">
<META name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<META name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<META name="Robots" content="index, follow">
<META name="Author" content="68 Designs, LLC">
<META name="Copyright" content="2005 68 Designs, LLC">
<META name="Distribution" content="Global">
<META name="Rating" content="General">
<META name="Language" content="en">
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>

    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
<?php 
if(isset($_GET['year'])){
	$year=$_GET['year'];
}else{
	$year=date("Y");
}
if(isset($_GET['month'])){
	$month=$_GET['month'];
}else{
	$month=date("m");
}
?>
<div align="center">
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" name="year">

	<select name=month>
	<option value=1<?php if($month==1) echo " SELECTED"; ?>>January</option>
	<option value=2<?php if($month==2) echo " SELECTED"; ?>>February</option>
	<option value=3<?php if($month==3) echo " SELECTED"; ?>>March</option>
	<option value=4<?php if($month==4) echo " SELECTED"; ?>>April</option>	
	<option value=5<?php if($month==5) echo " SELECTED"; ?>>May</option>
	<option value=6<?php if($month==6) echo " SELECTED"; ?>>June</option>
	<option value=7<?php if($month==7) echo " SELECTED"; ?>>July</option>
	<option value=8<?php if($month==8) echo " SELECTED"; ?>>August</option>
	<option value=9<?php if($month==9) echo " SELECTED"; ?>>September</option>
	<option value=10<?php if($month==10) echo " SELECTED"; ?>>October</option>
	<option value=11<?php if($month==11) echo " SELECTED"; ?>>November</option>
	<option value=12<?php if($month==12) echo " SELECTED"; ?>>December</option>			
	</select>
	
	&nbsp;&nbsp;

	<select name=year>
    	<option value="2002"<?php if($year==2002) echo " SELECTED"; ?>>2002</option>
		<option value="2003"<?php if($year==2003) echo " SELECTED"; ?>>2003</option>
		<option value="2004"<?php if($year==2004) echo " SELECTED"; ?>>2004</option>
		<option value="2005"<?php if($year==2005) echo " SELECTED"; ?>>2005</option>
		<option value="2006"<?php if($year==2006) echo " SELECTED"; ?>>2006</option>
    </select>
		&nbsp;&nbsp;
    <input type="submit" name="Submit" value="Submit">	
	</form>
</div>

<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Summary</th>
    <th>Amount</th>
  </tr>
<?php 
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordHandling,ordStateTax,ordCountryTax,ordTotal,ordShipping,ordDiscount,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus>=3 AND ordDate>'".$year."-".$month."-01' AND ordDate<'".$year."-".$month."-31 23:59:59'";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
$grandTotalSales+=$sales;
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$price=$rs['cartProdPrice'];
		$quantity=$rs['cartQuantity'];
		$products+=$quantity;
		$totSales=$price*$quantity;
		$tTax=$rs['ordStateTax']+$rs['ordCountryTax'];
		$tax+=$tTax;
		$handling+=$rs['ordHandling'];
		if($rs["ordStatus"]>=3) {
			$totalSales += $rs["ordTotal"];
			$janTotal+=$rs['ordTot'];
		}
		$shipping += $rs['ordShipping'];
		$discount += $rs['ordDiscount'];
	}
	$avgSale=$totalSales/$sales;
}	
?>
  <tr class="row1">
    <td>Sales</td>
    <td><?php echo $sales; ?></td>
  </tr>
  <tr class="row2">
       <td>Revenue </td>
       <td><?php echo FormatEuroCurrency($totalSales); ?></td>
  </tr>
  <tr class="row1">
       <td> Shipping Costs </td>
       <td><?php echo FormatEuroCurrency($shipping); ?></td>
  </tr>
  <tr class="row2">
       <td>Taxes</td>
       <td><?php echo FormatEuroCurrency($tax); ?></td>
  </tr>
  <tr class="row1">
       <td>Handling</td>
       <td><?php echo FormatEuroCurrency($handling); ?></td>
  </tr>
  <tr class="row2">
       <td>Discounts</td>
       <td><?php echo FormatEuroCurrency($discount); ?></td>
  </tr>
  <tr class="row1">
       <td>AVG Sale </td>
       <td><?php echo FormatEuroCurrency($avgSale); ?></td>
  </tr>
</table>

		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
