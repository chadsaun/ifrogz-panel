<?php
/***********************************************************************
| ectmods developed by 68 Designs, LLC.
|-----------------------------------------------------------------------
| All source code & content (c) Copyright 2005, 68 Designs LLC
|   unless specifically noted otherwise.
|
|The contents of this file are protect under law as the intellectual property
| of 68 Designs, LLC. Any use, reproduction, disclosure or copying
| of any kind without the express and written permission of 68 Designs, LLC is forbidden.
|
| Author $Author: Eric $ (68 Designs)
| Version $Revision: 1.2 $
| Updated: $Date: 2005/10/10 13:48:30 $
| ______________________________________________________________________
|	http://www.ectmods.com	  http://www.68designs.com/
***********************************************************************/
session_cache_limiter('none');
session_start();
include('init.php');
include(APPPATH.'views/partials/admin/dbconnection.php');
include(APPPATH.'views/partials/admin/includes.php');
include(APPPATH.'views/partials/admin/language.php');
include(APPPATH.'views/partials/admin/functions.php');
if(!$login==TRUE){
if(@$storesessionvalue=="") $storesessionvalue="virtualstore";
if(@$_SESSION["loggedon"] != $storesessionvalue && trim(@$_COOKIE["WRITECKL"])==""){
	header('Location: /user/login/');
	exit();
}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>E-commerce Templates Mods</title>
<link href="../style.css" rel="stylesheet" type="text/css">
<meta name="Keywords" content="ecommerce mods, php mods, asp mods, dreamweaver ecommerce mods, frontpage ecommerce mods">
<meta name="Description" content="ECT Mods - Your source for Ecommerce Templates Mods! ">
<meta name="Robots" content="index, follow">
<meta name="Author" content="68 Designs, LLC">
<meta name="Copyright" content="2005 68 Designs, LLC">
<meta name="Distribution" content="Global">
<meta name="Rating" content="General">
<meta name="Language" content="en">
</head>

<body>
<table width="700" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF" class="border">
  <tr>
    <td colspan="2"><div class="greybar">&nbsp;</div></td>
  </tr>
  <tr>
    <td><img src="/ectmods/images/logo.gif" alt="Ecommerce Templates Mods" width="446" height="79" border="0"></td>
    <td width="200">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" id="submenu"><div class="nav"><?php include("navigation.php"); ?></div></td>
  </tr>
  <tr>
       <td colspan="2" valign="top" class="padding">
	   <!-- InstanceBeginEditable name="nav" --><?php include(DOCROOT.'includes/navigation.php'); ?><!-- InstanceEndEditable -->
	   </td>
  </tr>
  <tr>
    <td colspan="2" valign="top" class="padding">
		<div class="main"><!-- InstanceBeginEditable name="body" -->
<?php 
if(isset($_GET['year'])){
	$year=$_GET['year'];
}else{
	$year=date("Y");
}
?>
<div align="center">
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" name="year">
<select name="year">
     <option value="2003"<?php if($year=="2003") echo " SELECTED"; ?>>2003</option>
     <option value="2004"<?php if($year=="2004") echo " SELECTED"; ?>>2004</option>
     <option value="2005"<?php if($year=="2005") echo " SELECTED"; ?>>2005</option>
     <option value="2006"<?php if($year=="2006") echo " SELECTED"; ?>>2006</option>
</select>&nbsp;
 <input type="submit" name="Submit" value="Refresh">
</form>
</div>
<h1 align="center">January</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php 
if(isset($_GET['year'])){
	$year=$_GET['year'];
}else{
	$year=date("Y");
}
$month=01;
$grandTotalSales=0;
$sSQL="SELECT ordID,ordName,payProvName,ordAuthNumber,ordDate,ordStatus,ordTotal,ordDiscount,ordTotal-ordDiscount AS ordTot FROM orders INNER JOIN payprovider ON payprovider.payProvID=orders.ordPayProvider WHERE ordStatus>=0 AND ordDate>'".$year."-01-01' AND ordDate<'".$year."-01-31'";
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">February</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$grandTotalSales=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">March</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>

    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">April</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">May</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">June</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">July</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">August</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">September</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">October</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">November</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>

<h1 align="center">December</h1>
<table width="100%"  border="0" cellpadding="2" cellspacing="3" class="border">
  <tr>
    <th width="80%">Product</th>
    <th>Amount Sold</th>
  </tr>
<?php
$month++;
$quantity=0;
$sSQL="SELECT cartProdID, cartProdName, cartProdPrice, cartQuantity, count(*) AS quantity FROM cart WHERE cartCompleted=1 AND cartDateAdded>'".$year."-".$month."-01' AND cartDateAdded<'".$year."-".$month."-31' GROUP BY cartProdName ORDER BY quantity DESC LIMIT 5";
$result=mysql_query($sSQL) or print(mysql_error());
$sales=mysql_num_rows($result);
if($sales>0){
	while ($rs=mysql_fetch_array($result)){
		$name=$rs['cartProdName'];
		$quantity=$rs['quantity'];
?>
  <tr>
    <td><?php echo $name; ?></td>
    <td><?php echo $quantity; ?></td>
  </tr>
<?php
	}
}	
?>  
</table>
		<!-- InstanceEndEditable --></div>
	    
    </tr>
  <tr>
    <td colspan="2"><!-- Footer Navigation -->
		  <table width="100%">
		       <tr><td width="115">
		       <div align="center"><a href="http://www.68designs.com"><img src="/ectmods/images/greylogo.gif" alt="Ecommerce Web Design" width="110" height="34" border="0" /></a></div></td>
		  <td>		       <div style="margin-top:2px;" class="menu">&copy; <?php echo date("Y"); ?> ECT-Mods - A division of 68 Designs, LLC
					 &#8226; <a href="../customers/" class="menu">Customers</a>
					</div>
		  </td></tr></table>
		  <!-- End Footer Navigation --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
