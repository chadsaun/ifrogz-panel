<?php
// $Id:$
/*
    Example address validation request.
*/
include('fedexdc.php');

// create new FedExDC object
$fed = new FedExDC(null,null);


$aFedData = array(
    11=>'Vermonster LLC',
    13=>'312 Stewart St', // Spelled wrong to test.  Should be "Stuart St"
    14=>'',
    15=>'Boston',
    16=>'MA',
    17=>'02116',
    1600=>'',   // Urbanization Code?  Used for PR
    1601=>3     // How many retuned items
);


$aVData = $fed->address_validate($aFedData);
//echo "BUFF ". $fed->sBuf;
if ($error = $fed->getError()) {
    die("ERROR: ". $error);
}

for ($i=1; $i<=$aVData['1604']; $i++) {
    foreach (array(13,14,15,16,17) as $key){
        if (isset($aVData[$key.'-'.$i]))
        echo $aFedData[$key]." => ".$aVData[$key.'-'.$i]."<BR>\n";
    }
}

?>