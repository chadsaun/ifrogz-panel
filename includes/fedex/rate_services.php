<?php
// $Id$
// rate_services.php by panreach
/*
    Example rate_services
*/

include('fedexdc.php');

// create new FedExDC object
$fed = new FedExDC('xxxxxxxxx','xxxxxxx');

// rate services example
// You can either pass the FedEx tag value or the field name in the
// $FE_RE array
$rate_Ret = $fed->services_rate (
    array(
        'weight_units' =>'LBS'
        ,8=>'KS '// Sender State
        ,9=>'67212'//Sender Postal Code
        ,117=>	'US'// Sender Country Code
        ,16=>   'MA'// Recipient State
        ,17=>   '02116'//Recipient Postal Code
        ,50=>   'US'//Recipient Country Code
        ,57=>	'12'//Dim Height
        ,58=>	'24'//Dim Width
        ,59=>	'10'//Dim Length
        ,1116=>	'IN'// Dim Units
        ,1401=>	'14.0'//Total Package Weight/ Shipment total weight
        ,1333=>	'1'//Drop Off Type
    )
);


echo "<PRE>";
if ($error = $fed->getError()) {
    echo "ERROR :". $error;
} else {
    echo $fed->debug_str. "\n<BR>";
    print_r($rate_Ret);
    echo "\n";
    echo "ZONE: ".$rate_Ret[1092]."\n\n";

    for ($i=1; $i<=$rate_Ret[1133]; $i++) {
        echo "SERVICE : ".$fed->service_type($rate_Ret['1274-'.$i])."\n";
        echo "SURCHARGE : ".$rate_Ret['1417-'.$i]."\n";
        echo "DISCOUNT : ".$rate_Ret['1418-'.$i]."\n";
        echo "NET CHARGE : ".$rate_Ret['1419-'.$i]."\n";
        echo "DELIVERY DAY : ".$rate_Ret['194-'.$i]."\n";
        echo "DELIVERY DATE : ".$rate_Ret['409-'.$i]."\n\n";
    }
}
echo "</PRE>";
?>