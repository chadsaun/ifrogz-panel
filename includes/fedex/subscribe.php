<?php
// $Id$
/*
    Example subscription request.
*/

include('fedexdc.php');

$fed = new FedExDC('#########');

$aRet = $fed->subscribe(
    array(
        1 => 'unique12345', // Don't really need this but can be used for ref
        4003 => 'John Doe',
        4008 => '123 Main St',
        4011 => 'Boston',
        4012 => 'MA',
        4013 => '02116',
        4014 => 'US',
        4015 => '6175551111',
    )
);

// The return info will have the services available to you (ground, express ...).
print_r($aRet);
?>