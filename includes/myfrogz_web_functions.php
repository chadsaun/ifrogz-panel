<?php
session_start();
$base_url = "http://ifrogz.com/rest/index.php";

$_POST['category'] = "iapps";
$_POST['service'] = "CaseDesignerWebService";
$_POST['version'] = "0.3";
$_POST['api-key'] = "qwertyuiopasdfghjklzxcvbnm";

//variables for posting your design.
$design_message = "'has just designed a customized MyFrogz case.'";
$design_caption = 'Check out this MyFrogz case.';
$design_action_link_text = 'Get your own MyFrogz case';
$design_ifrogz_url = 'http://ifrogz.com';
$design_myfrogz_url = 'http://myfrogz.com';


if(isset($_POST['get_products'])) {
	get_products($base_url, $_POST);
} else if(isset($_POST['process_order'])){
	$results = process_order($base_url, $_POST);
	$results = json_decode($results);
	$status = $results->response->status;
	if(strcmp($status, "success") == 0) {
		$order_number = $results->response->data->order->number;
		$_POST['order_number'] = $order_number;
		$_SESSION['form_vars'] = $_POST;
		header("Location: http://m.ifrogz.com/myfrogz_thank_you.php");
	} else {
		//We had an error and we need to send them back to the form with the error
		$errors = array();
		$array = $results->response->errors;
		for($i = 0; $i < count($array); $i++) {
			$errors[] = $array[$i]->error->message." ";
		}
		$_POST['error_messages'] = $errors;
		$_SESSION['form_vars'] = $_POST;
		unset($_POST);
		header("Location: http://m.ifrogz.com/myfrogz/form.php");
	}
} else if(isset($_POST['get_legalese'])) {
	echo get_legalese($base_url, $_POST);
} else if(isset($_POST['get_post_design_content'])) {
	if(isset($_POST['sku_1']) && isset($_POST['sku_2']))
		echo get_post_design_content($_POST['sku_1'], $_POST['sku_2']);
} else if(isset($_POST['get_regional_data'])) {
	echo get_regional_data($base_url, $_POST);
}

function get_products($base_url, $_POST) {
	$_POST['operation'] = "getProducts";
	$_POST['reply'] = "text/x-json";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $base_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);

	$results = curl_exec($ch);
	curl_close($ch);
	
	$results = json_decode($results);
	$status = $results->response->status;
	
	if(strcmp($status, "success") == 0){
		$tops = array();
		foreach($results->response->data->top as $component) {
			$file = $component->component->file;
			$path = "lib/images/case_designer/parts/".$file;
			$color = $component->component->color;
			$sku = $component->component->sku;
			$tops[] = "<img alt='".$color."' class='".$sku."' src='".$path."' width='160' height='237'/>";
		}
		$bottoms = array();
		foreach($results->response->data->bottom as $component) {
			$file = $component->component->file;
			$path = "lib/images/case_designer/parts/".$file;
			$color = $component->component->color;
			$sku = $component->component->sku;
			$bottoms[] = "<img alt='".$color."' class='".$sku."' src='".$path."' width='160' height='117'/>";
		}
		$array = array($tops, $bottoms);
		$array = json_encode($array);
		echo $array;
	}
}

function process_order($base_url, $_POST) {
	$_POST['operation'] = 'processOrder';
	
	if(isset($_POST['opt_in']))
		$_POST['opt_in'] = 'yes';
	else
		$_POST['opt_in'] = 'no';
		
	$_POST['reply'] = 'text/x-json';
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $base_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);

	$results = curl_exec($ch);
	curl_close($ch);
	
	return $results;
}

function get_legalese($base_url, $_POST) {
	$_POST['operation'] = "getLegalese";
	$_POST['reply'] = "text/x-json";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $base_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
	
	$results = curl_exec($ch);
	curl_close($ch);
	
	$results = json_decode($results);
	$status = $results->response->status;
	if(strcmp($status, "success") == 0) {
		$string = $results->response->data[0]->text;
		
		$string = str_replace("\n", "<br />", $string);
		
		$results = array('status'=>$status, 'text'=>$string);
		$results = json_encode($results);
		
		return $results;
	} else {
		$results = array('status'=>'failed');
		$results = json_encode($results);
		
		return $results;
	}
}

function get_post_design_content($top, $bottom) {
	$facebook = get_facebook_submit($top, $bottom);
	$myspace = get_myspace_submit($top, $bottom);
	if((strcmp($facebook['success'], 'true') == 0) && (strcmp($myspace['success'], 'true') == 0))
		$array = array('success' => 'true', 'facebook' => $facebook, 'myspace' => $myspace);
	else
		$array = array('success' => 'false');
	return json_encode($array);
}

function get_facebook_submit($top, $bottom) {
	global $design_message;
	global $design_caption;
	global $design_action_link_text;
	global $design_ifrogz_url;
	global $design_myfrogz_url;
	
	$results = get_thumbnail_url($top, $bottom);
	
	$results = json_decode($results);
	
	$thumbnail = $results->response->data->thumbnail->uri;
	
	$array = array();
	if(isset($thumbnail))
		$array['success'] = 'true';
	else
		$array['success'] = 'false';
	$array['message'] = $design_message;
	
	$attachments = array(
		'caption' => $design_caption,
		'media' => array(
			array(
				'type' => 'image',
				'src' => $thumbnail,
				'href' => $design_ifrogz_url
			)
		)
	);
	
	$array['attachments'] = $attachments;
	
	$array['action_links'] = array(
	array(	
		"text" => $design_action_link_text,
		"href" => $design_myfrogz_url
	));
	
	
	return $array;
}

function get_myspace_submit($top, $bottom) {
	global $design_message;
	global $design_caption;
	global $design_action_link_text;
	global $design_ifrogz_url;
	global $design_myfrogz_url;
	
	$results = get_thumbnail_url($top, $bottom);
	
	$results = json_decode($results);
	
	$thumbnail = $results->response->data->thumbnail->uri;
	
	$design_message = "I have ".substr($design_message, 5, -1);
	
	$array = array();
	if(isset($thumbnail))
		$array['success'] = 'true';
	else
		$array['success'] = 'false';
	$array['message'] = $design_message;
	
	$attachments = "<img src='{$thumbnail}' width='70' height='120'>{$design_caption}";
	
	$array['attachments'] = $attachments;
	
	$array['link'] = $design_myfrogz_url;
	
	return $array;
}

function get_thumbnail_url($top, $bottom) {
	$_POST['operation'] = 'getThumbnail';
	$_POST['reply'] = 'text/x-json';
	
	$base_url = "http://ifrogz.com/rest/index.php";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $base_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);

	$results = curl_exec($ch);
	curl_close($ch);
	
	return $results;
}

function get_regional_data($base_url, $_POST) {
	$_POST['operation'] = "getRegionalData";
	$_POST['reply'] = "text/x-json";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $base_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
	
	$results = curl_exec($ch);
	curl_close($ch);
	
	return $results;
}

?>