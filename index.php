<?php

/**
 * The purpose of this check is to prevent CRON job from failing.
 *
 */
if ( ! isset($_SERVER['HTTP_HOST'])) {
	$_SERVER['HTTP_HOST'] = '';
}

/**
 * The directory in which your assets are located.  The lib directory
 * contains the Web site's assets.
 *
 */
$assets = 'lib';

/**
 * The directory in which your application specific resources are located.
 * The application directory must contain the bootstrap.php file.
 *
 * @see  http://kohanaframework.org/guide/about.install#application
 */
$application = 'application';

/**
 * The directory in which your modules are located.
 *
 * @see  http://kohanaframework.org/guide/about.install#modules
 */
$modules = 'modules';

/**
 * The directory in which the Kohana resources are located. The system
 * directory must contain the classes/kohana.php file.
 *
 * @see  http://kohanaframework.org/guide/about.install#system
 */
$system = 'system';

/**
 * This modified the directory paths to appropriate locations.
 *
 */
$_SERVER['KOHANA_ENV'] = (isset($_SERVER['KOHANA_ENV'])) ? strtoupper($_SERVER['KOHANA_ENV']) : 'UNDEFINED';
if (($_SERVER['KOHANA_ENV'] == 'DEVELOPMENT') || (strstr($_SERVER['HTTP_HOST'], ':8888') && !preg_match('/^admin\.ifrogz\.com$/i', $_SERVER['HTTP_HOST']))) { // i.e. localhost
	$assets = '' . $assets;
    $application = '' . $application;
	$modules = '' . $modules;
	$system = '' . $system;
} else if (($_SERVER['KOHANA_ENV'] == 'STAGING') || preg_match('/^dev\.admin\.ifrogz\.com$/i', $_SERVER['HTTP_HOST'])) { // i.e. dev site
	$assets = '/home/ifrogzad/public_html/dev.admin.ifrogz.com/' . $assets;
    $application = '' . $application;
	$modules = '/home/ifrogz/dev.root.ifrogz/kohana-3.2/' . $modules;
	$system = '/home/ifrogz/dev.root.ifrogz/kohana-3.2/' . $system;
} else if (($_SERVER['KOHANA_ENV'] == 'STAGING') || preg_match('/^ifrgoz\.audrey4\.com$/i', $_SERVER['HTTP_HOST'])) { // i.e. dev site
	$_SERVER['KOHANA_ENV'] = 'STAGING';
	$application = '' . $application;
	$modules = '/home/ifrogz/root/kohana-3.2/' . $modules;
	$system = '/home/ifrogz/root/kohana-3.2/' . $system;
} else { // i.e. live site
	$assets = '/home/ifrogzad/public_html/' . $assets;
	$application = '' . $application;
	$modules = '/home/ifrogz/kohana-3.2/' . $modules;
	$system = '/home/ifrogz/kohana-3.2/' . $system;
}

/**
 * The default extension of resource files. If you change this, all resources
 * must be renamed to use the new extension.
 *
 * @see  http://kohanaframework.org/guide/about.install#ext
 */
define('EXT', '.php');

/**
 * Set the PHP error reporting level. If you set this in php.ini, you remove this.
 * @see  http://php.net/error_reporting
 *
 * When developing your application, it is highly recommended to enable notices
 * and strict warnings. Enable them by using: E_ALL | E_STRICT
 *
 * In a production environment, it is safe to ignore notices and strict warnings.
 * Disable them by using: E_ALL ^ E_NOTICE
 *
 * When using a legacy application with PHP >= 5.3, it is recommended to disable
 * deprecated notices. Disable with: E_ALL & ~E_DEPRECATED
 */
ini_set('display_errors', 1);
if ($_SERVER['KOHANA_ENV'] == 'DEVELOPMENT') {
	error_reporting(E_ALL | E_STRICT);
} else {
	error_reporting(E_ALL ^ E_NOTICE);
}

/**
 * End of standard configuration! Changing any of the code below should only be
 * attempted by those with a working knowledge of Kohana internals.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 */

// Set the full path to the docroot
if (!defined('DOCROOT')) { define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR); }

// Make the application relative to the docroot
if ( ! is_dir($assets) AND is_dir(DOCROOT.$assets)) {
	$assets = DOCROOT.$assets;
}
// Make the application relative to the docroot, for symlink'd index.php
if ( ! is_dir($application) AND is_dir(DOCROOT.$application))
	$application = DOCROOT.$application;

// Make the modules relative to the docroot, for symlink'd index.php
if ( ! is_dir($modules) AND is_dir(DOCROOT.$modules))
	$modules = DOCROOT.$modules;

// Make the system relative to the docroot, for symlink'd index.php
if ( ! is_dir($system) AND is_dir(DOCROOT.$system))
	$system = DOCROOT.$system;

// Define the absolute paths for configured directories
if (!defined('LIBPATH')) { define('LIBPATH', realpath($assets).DIRECTORY_SEPARATOR); }
if (!defined('APPPATH')) { define('APPPATH', realpath($application).DIRECTORY_SEPARATOR); }
if (!defined('MODPATH')) { define('MODPATH', realpath($modules).DIRECTORY_SEPARATOR); }
if (!defined('SYSPATH')) { define('SYSPATH', realpath($system).DIRECTORY_SEPARATOR); }

// Clean up the configuration vars
unset($assets, $application, $modules, $system);

if (file_exists('install'.EXT))
{
	// Load the installation check
	return include 'install'.EXT;
}

/**
 * Define the start time of the application, used for profiling.
 */
if ( ! defined('KOHANA_START_TIME'))
{
	define('KOHANA_START_TIME', microtime(TRUE));
}

/**
 * Define the memory usage at the start of the application, used for profiling.
 */
if ( ! defined('KOHANA_START_MEMORY'))
{
	define('KOHANA_START_MEMORY', memory_get_usage());
}

// Bootstrap the application
require APPPATH.'bootstrap'.EXT;

/**
 * Cron Job Setup - If this is a CLI call then we need to manually set $_SERVER['PATH_INFO'] with the first argument passed
 * 		Example Call: php "/Users/username/Sites/ifrogz/index.php" cron/update_inventory/
 * 		Example of $_SERVER['argv']:
 * 			"argv" => (
 *       		0 => "/Users/username/Sites/ifrogz/index.php"
 *       		1 => "cron/update_inventory/"
 *			)
 *
 */
if (Kohana::$is_cli) {
	$_SERVER['DOCUMENT_ROOT'] = dirname($_SERVER['PHP_SELF']);
	if (isset($_SERVER['argv'][1])) {
		$_SERVER['PATH_INFO'] = $_SERVER['argv'][1];
	} else {
		throw new Kohana_Exception('No parameter passed for CLI call.');
	}
}

/**
* Execute the main request using $_SERVER['PATH_INFO']. If no URI source is specified,
* the URI will be automatically detected.
*/
if (!defined('SUPPRESS_REQUEST') && !defined('KOHANA_EXTERNAL_MODE')) {
    try {
        $path_info = TRUE;
        if (isset($_SERVER['PATH_INFO'])) {
            $path_info = $_SERVER['PATH_INFO'];
        }
	    echo Request::factory($path_info)
	        ->execute()
	        ->send_headers()
	        ->body();
    }
    catch (Exception $ex) {
        Kohana_System_Exception::handler($ex);
    }
}
?>