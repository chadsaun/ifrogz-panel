<?php

/**
* The purpose of this check is to prevent CRON job from failing.
*
*/
if ( ! isset($_SERVER['HTTP_HOST'])) {
	$_SERVER['HTTP_HOST'] = '';
}

/**
 * The directory in which your assets are located.  The lib directory
 * contains the Web site's assets.
 *
 */
$assets = 'lib';

/**
 * The directory in which your application specific resources are located.
 * The application directory must contain the bootstrap.php file.
 *
 * @see  http://kohanaframework.org/guide/about.install#application
 */
$application = 'application';

/**
 * The directory in which your modules are located.
 *
 * @see  http://kohanaframework.org/guide/about.install#modules
 */
$modules = 'modules';

/**
 * The directory in which the Kohana resources are located. The system
 * directory must contain the classes/kohana.php file.
 *
 * @see  http://kohanaframework.org/guide/about.install#system
 */
$system = 'system';

/**
 * This modified the directory paths to appropriate locations.
 *
 */
$_SERVER['KOHANA_ENV'] = (isset($_SERVER['KOHANA_ENV'])) ? strtoupper($_SERVER['KOHANA_ENV']) : 'UNDEFINED';
if (($_SERVER['KOHANA_ENV'] == 'DEVELOPMENT') || (strstr($_SERVER['HTTP_HOST'], ':8888') && !preg_match('/^admin\.ifrogz\.com$/i', $_SERVER['HTTP_HOST']))) { // i.e. localhost
	$assets = '' . $assets;
    $application = '' . $application;
	$modules = '' . $modules;
	$system = '' . $system;
} else if (($_SERVER['KOHANA_ENV'] == 'STAGING') || preg_match('/^dev\.admin\.ifrogz\.com$/i', $_SERVER['HTTP_HOST'])) { // i.e. dev site
	$assets = '/home/ifrogzad/public_html/dev.admin.ifrogz.com/' . $assets;
    $application = '' . $application;
	$modules = '/home/ifrogz/dev.root.ifrogz/kohana-3.2/' . $modules;
	$system = '/home/ifrogz/dev.root.ifrogz/kohana-3.2/' . $system;
} else if (($_SERVER['KOHANA_ENV'] == 'STAGING') || preg_match('/^panel\.ifrogz\.com$/i', $_SERVER['HTTP_HOST'])) { // i.e. dev site
	$_SERVER['KOHANA_ENV'] = 'STAGING';
	$application = '' . $application;
	$modules = '/home/ifrogz/kohana-3.2/' . $modules;
	$system = '/home/ifrogz/kohana-3.2/' . $system;
} else { // i.e. live site
	$assets = '/home/ifrogzad/public_html/' . $assets;
	$application = '' . $application;
	$modules = '/home/ifrogz/kohana-3.2/' . $modules;
	$system = '/home/ifrogz/kohana-3.2/' . $system;
}

/**
 * End of standard configuration! Changing any of the code below should only be
 * attempted by those with a working knowledge of Kohana internals.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 */

// Set the full path to the docroot
if (!defined('DOCROOT')) { define('DOCROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR); }

// Make the application relative to the docroot
if ( ! is_dir($assets) AND is_dir(DOCROOT.$assets)) {
	$assets = DOCROOT.$assets;
}
// Make the application relative to the docroot, for symlink'd index.php
if ( ! is_dir($application) AND is_dir(DOCROOT.$application))
	$application = DOCROOT.$application;

// Make the modules relative to the docroot, for symlink'd index.php
if ( ! is_dir($modules) AND is_dir(DOCROOT.$modules))
	$modules = DOCROOT.$modules;

// Make the system relative to the docroot, for symlink'd index.php
if ( ! is_dir($system) AND is_dir(DOCROOT.$system))
	$system = DOCROOT.$system;

// Define the absolute paths for configured directories
if (!defined('LIBPATH')) { define('LIBPATH', realpath($assets).DIRECTORY_SEPARATOR); }
if (!defined('APPPATH')) { define('APPPATH', realpath($application).DIRECTORY_SEPARATOR); }
if (!defined('MODPATH')) { define('MODPATH', realpath($modules).DIRECTORY_SEPARATOR); }
if (!defined('SYSPATH')) { define('SYSPATH', realpath($system).DIRECTORY_SEPARATOR); }
if (!defined('IFZROOT')) { define('IFZROOT', '/home/ifrogz/'); }

// Clean up the configuration vars
unset($assets, $application, $modules, $system);

?>