$(document).ready(function() {
	$("#sortable").sortable({ 
		cursor: 'move',
		update: function (event, ui) {
			var list_order = $(this).sortable('toArray');
			// console.log(list_order);
			$.ajax({
		 		type: "POST",  
	            url: "/ads/sort/",
	            data: {
					list_order: list_order
				},
				dataType: 'json',
	            success: function(json){
					var status = json['response']['status'];
					var order = json['response']['data']['order']
					if (json['response']['status'] == 'success') {
						// alert('you made it');
						// console.log(order);
						for (var i=0; i < order.length; i++) {
						 	//console.log(order[i]['position']);
							$('#order_'+order[i]['id']).html(order[i]['position']);
						};
					};
	            }
	        });
		}
	});
	$("#sortable").disableSelection();
	$('.deletelink').live('click', function() {
		$(this).hide();
		$(this).siblings('.delete').show();
		$(this).siblings('.cancel').show();
		$(this).siblings('.edit').hide();
	});
	$('.cancel').live('click', function() {
		$(this).hide();
		$(this).siblings('.deletelink').show();
		$(this).siblings('.edit').show();
		$(this).siblings('.delete').hide();
	});
	$('.staging').live('click', function() {
		var id = $(this).attr('id');
		var ischecked = $(this).attr('checked');	
		$.ajax({
	 		type: "POST",  
            url: "/ads/staging/",
            data: {
				id: id,
				ischecked: ischecked,
			},
			dataType: 'json',
            success: function(json){
				var status = json['response']['status'];
            }
        });
		if ($(this).attr('checked')) {
			$('.orderbox_'+id).show();
		} else {
			$('.orderbox_'+id).hide();
		};	
	});
	$('.testOrder').live('keyup', function() {
		var id = $(this).attr('id');
		var order = $(this).val();
		$.ajax({
	 		type: "POST",  
            url: "/ads/staging_order/",
            data: {
				id: id,
				order: order,
			},
			dataType: 'json',
            success: function(json){
				var status = json['response']['status'];
            }
        });		
	});
	$('.view_staging').click(function() {
		$('#live').hide();
		$('#staging').show();
		$(this).hide();
		$('.view_live').show();
	});
	$('.view_live').click(function() {
		$('#live').show();
		$('#staging').hide();
		$(this).hide();
		$('.view_staging').show();
	});
	$('.delete').live('click', function() {
		var id = $(this).attr('id');
		$.ajax({
	 		type: "POST",  
            url: "/ads/delete/",
            data: {
				ID: id
			},
			dataType: 'json',
            success: function(json){
				var status = json['response']['status'];
				var id = json['response']['data']['id']
				if (json['response']['status'] == 'success') {
					$('#'+id).hide();
					$('#edit').hide();
					$('#list').show();
				};
            }
        });
	});
	
	$('.edit').live('click', function() {
		var id = $(this).attr('id');
		var type = $('#list').attr('title');

		$.ajax({
	 		type: "POST",
            url: "/ads/edit/",
            data: {
				id: id,
				type: type
			},
			dataType: 'json',
            success: function(json){
				var status = json['response']['status'];
				var view = json['response']['data']['view']
				if (json['response']['status'] == 'success') {
					$('#edit').html(view);
					$('#edit').toggle();
					$('#list').toggle();
				};
            }
        });
	});
	
	$('#addlink').live('click', function() {
		var id = $('#last_id').val();
		var type = $('#list').attr('title');

		$.ajax({
	 		type: "POST",  
            url: "/ads/add/",
            data: {
				id: id,
				type: type
			},
			dataType: 'json',
            success: function(json){
				var status = json['response']['status'];
				var view = json['response']['data']['view']
				if (json['response']['status'] == 'success') {
					$('#edit').html(view);
					$('#edit').toggle();
					$('#list').toggle();
				};
            }
        });
	});
	$('#backtolist').live('click', function() {
		$('#edit').hide();
		$('#list').show();
	});
	$('#save').live('click', function() {
		var image = $('#image').val();
		var link = $('#link').val();
		var begin_date = $('#begin_date').val();
		var end_date = $('#end_date').val();
		var id = $('#ad_id').val();
		var type = $('#list').attr('title');
		
		$.ajax({
	 		type: "POST",  
            url: "/ads/edit_save/",
            data: {
				id: id,
				image: image,
				link: link,
				begin_date: begin_date,
				end_date: end_date,
				type: type
			},
			dataType: 'json',
            success: function(json){
				var status = json['response']['status'];
				var view = json['response']['data']['view'];
				if (json['response']['status'] == 'success') {
					$('#edit').hide();
					$('#'+id).html(view);
					$('#list').show();
					$(window).scrollTop($('#'+id).position().top)
				};
            }
        });
	});
	// Initialize the banner fades
	$('div.rotator_cuff').innerfade({ 
		animationtype: 'fade', 
		speed: 750, 
		timeout: 6000, 
		type: 'sequence', 
		containerheight: '466px'
	});
	
	$('div.rotator_cuff').fadeIn("slow");
});