var ItemCount = 0;
var IsIntlShippment = false;

$('document').ready(function() {

	/**
	* This sets focus to the scan field.
	*/
	$('#scan_field').focus();

	/**
	* This function uses an independent thread to look for scanned in data
	* and will process the data once it is scanned.
	*/
	setInterval(function() {
		var scanned_data = $("#scan_field").val();
		if (scanned_data == undefined) {
			scanned_data = '';
		}
		var index = scanned_data.indexOf("\n");
		if (index != -1) {
			$("#scan_field").val("");
			if ((ItemCount % 2) == 1) {
				var regex1 = /^R[AC-Z][0-9]{9}HK\n$/;
				var regex2 = /^RB[0-9]{9}HK\n$/;
				if ((!IsIntlShippment && regex1.test(scanned_data)) || (IsIntlShippment && regex2.test(scanned_data))) {
					scanned_data = jQuery.trim(scanned_data);
					if ($('#tracking_nums option[value=' + scanned_data + ']').length == 0) {
						$.ajax({
							async: false,
							type: 'POST',
							url: '/batchshipping/validate/',
							data: 'scanned_data=' + scanned_data,
							dataType: 'json',
							success: function(json) {
								if (json['response']['data'] == 'valid') {
									ItemCount++;
									$('#counter').text(ItemCount / 2);
									$('#tracking_nums').append($('<option></option>').attr('value', scanned_data).text(scanned_data));
									var order_id = document.getElementById('order_nums').options[(ItemCount / 2) - 1].value;
									var child_window = window.open('/addresslabel/fetch/' + order_id + '/', 'address_label', 'width=400,height=250');
									child_window.focus();
									//child_window.print();
									//child_window.close();
								}
								else if (json['response']['data'] == 'matched') {
									alert('Error: Tracking number has already been used....');
								}
								else {
									alert('Error: Expecting an order number....');
								}
							},
							error: function() {
								alert('Error: Unable to establish database connection....');
							},
							complete: function() {
								$('#scan_field').focus();
							}
						});
					}
					else {
						alert('Error: Tracking number is already in queue....');
					}
				}
				else {
					alert('Error: Expecting a tracking number....');
				}
			}
			else {
				var regex = /^(0|[1-9][0-9]*)\n$/;
				if (regex.test(scanned_data)) {
					scanned_data = jQuery.trim(scanned_data);
					if ($('#order_nums option[value=' + scanned_data + ']').length == 0) {
						$.ajax({
							async: false,
							type: 'POST',
							url: '/batchshipping/validate/',
							data: {
								scanned_data: scanned_data
							},
							dataType: 'json',
							success: function(json) {
								if (json['response']['data'] == 'valid') {
									IsIntlShippment = true;
									ItemCount++;
									$('#order_nums').append($('<option></option>').attr('value', scanned_data).text(scanned_data));
								}
								else if (json['response']['data'] == 'valid - HK') {
									IsIntlShippment = false;
									ItemCount++;
									$('#order_nums').append($('<option></option>').attr('value', scanned_data).text(scanned_data + ' - HK'));
								}
								else if (json['response']['data'] == 'bad') {
									alert('Error: Order does not ship via this method....');
								}
								else if (json['response']['data'] == 'cancelled') {
									alert('Error: Order was cancelled....');
								}
								else if (json['response']['data'] == 'deleted') {
									alert('Error: Order was deleted....');
								}
								else if (json['response']['data'] == 'matched') {
									alert('Error: Order has already been shipped....');
								}
								else {
									alert('Error: Expecting an order number....');
								}
							},
							error: function() {
								alert('Error: Unable to establish database connection....');
							},
							complete: function() {
								$('#scan_field').focus();
							}
						});
					}
					else {
						alert('Error: Order number is already in queue....');
					}
				}
				else {
					alert('Error: Expecting an order number....');
				}
			}
		}
	}, 250);

	/**
	* This function will delete the selected order number and its
	* corresponding tracking number.
	*/
	$('#delete_button').click(function() {
		if (ItemCount > 0) {
			var menu1 = document.getElementById('order_nums');
			var menu2 = document.getElementById('tracking_nums');
			var index = menu1.selectedIndex;
			if (index >= 0) {
				menu1.remove(index);
				if (menu2.length > index) {
					menu2.remove(index);
				}
				ItemCount -= ((index != menu1.length) || ((ItemCount % 2) == 0)) ? 2 : 1;
				$('#counter').text(ItemCount / 2);
			}
			else {
				alert('Error: No order is selected for deletion....');
			}
		}
		else {
			alert('Error: No orders are in the queue....');
		}
	});

	/**
	* This function will reprint the order with the selected tracking number.
	*/
	$('#reprint_button').click(function() {
		if (ItemCount > 0) {
			var menu1 = document.getElementById('order_nums');
			var menu2 = document.getElementById('tracking_nums');
			var index = menu2.selectedIndex;
			if (index >= 0) {
				var order_id = menu1.options[index].value;
				var child_window = window.open('/addresslabel/fetch/' + order_id + '/', 'address_label', 'width=400,height=250');
				child_window.focus();
				//child_window.print();
				//child_window.close();
			}
			else {
				alert('Error: No tracking number is selected....');
			}
		}
		else {
			alert('Error: No orders are in the queue....');
		}
	});

	/**
	* This function will select the corresponding tracking number
	* when the user selects an order number.
	*/
	$('#order_nums').change(function() {
		var index = $("#order_nums").attr("selectedIndex");
		var menu2 = document.getElementById('tracking_nums');
		if (menu2.length > index) {
			menu2.selectedIndex = index;
		}
	});

	/**
	* This function will select the corresponding order number when
	* the user selects a tracking number.
	*/
	$('#tracking_nums').change(function() {
		var index = $("#tracking_nums").attr("selectedIndex");
		$("#order_nums")[0].selectedIndex = index;
	});

	//$('#shipping_form').submit(function() {
	$('#submit_button').click(function() {
		if (ItemCount > 0) {
			if ((ItemCount % 2) == 0) {
				$('#order_nums, #tracking_nums').attr('multiple', 'multiple');
				$('#order_nums').each(function() {
					$('#order_nums option').attr('selected', 'selected');
				});
				$('#tracking_nums').each(function() {
					$('#tracking_nums option').attr('selected', 'selected');
				});
				/*
				var query_string = decodeURIComponent($.param($(this).serializeArray()));
				var options = {
					//async: false,
					type: 'POST',
					url: '/batchshipping/process/',
					data: query_string,
					dataType: 'application/pdf',
					success: function() {	// like "try"
						$('#order_nums, #tracking_nums').children().remove();
						ItemCount = 0;
					},
					error: function() {		// like "catch"
						alert('Error: Unable to process batch....');
					},
					complete: function() {	// like "finally"
						$('#order_nums, #tracking_nums').removeAttr('multiple');
					}
				};
				//$.ajax(options);
				$(this).ajaxSubmit(options);
				*/
				$('#shipping_form').submit();
				$('#order_nums, #tracking_nums').children().remove();
				ItemCount = 0;
				$('#order_nums, #tracking_nums').removeAttr('multiple');
			}
			else {
				alert('Error: Missing a tracking number....');
			}
		}
		else {
			alert('Error: No orders are in the queue....');
		}
		//return false;
	});

});