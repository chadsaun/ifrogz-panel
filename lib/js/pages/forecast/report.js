/**
 * This script is for processing HK shipments.
 *
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2012-01-26
 * @copyright (c) 2011-2012, iFrogz (a subsidary of Zagg, Inc.)
 * @package JavaScript
 * @category Forecast Report
 */
$('document').ready(function() {

	// Load initial records on page load
	$.ajax({
		type: 'POST',
		url: '/forecast/data/',
		data: {
			customer: $('#customer').val()
		},
		dataType: 'json',
		success: function(json) {
			if (json['response']['status'] == 'success') {
				alert('Success: Carrier has been changed!');
			} else {
				alert('Error (1): Unable to process request.');
			}
		},
		error: function (xmlhttp, textStatus, errorThrown) {
			alert('Error (2): Unable to process request.');
		}
	});

});