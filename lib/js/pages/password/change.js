/**
 * This script is for submitting a request for a password change.
 *
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2012-01-16
 * @copyright (c) 2011, iFrogz (a subsidary of Zagg, Inc.)
 * @package JavaScript
 * @category Login
 */
$('document').ready(function() {

	$('#update').click(function (event) {
		$.ajax({
			type: 'POST',
			url: '/password/update/',
			data: {
				password_1: $('#password_1').val(),
				password_2: $('#password_2').val()
			},
			dataType: 'json',
			success: function(json) {
				if (json['response']['status'] == 'success') {
					alert(json['response']['data'][0]);
				} else {
					alert(json['response']['errors'][0]['error']['message']);
				}
			},
			error: function (xmlhttp, textStatus, errorThrown) {
				alert('Failed to update password.');
			}
		});
	});

});