/**
 * This script is for submitting a request for a password change.
 *
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2011-12-20
 * @copyright (c) 2011, iFrogz (a subsidary of Zagg, Inc.)
 * @package JavaScript
 * @category Login
 */
$('document').ready(function() {

	$('#reset').click(function (event) {
		$.ajax({
			type: 'POST',
			url: '/password/reset/',
			data: {
				email: $('#email').val()
			},
			dataType: 'json',
			success: function(json) {
				if (json['response']['status'] == 'success') {
					alert(json['response']['data'][0]);
				} else {
					alert(json['response']['errors'][0]['error']['message']);
				}
			},
			error: function (xmlhttp, textStatus, errorThrown) {
				alert('Failed to email new password.');
			}
		});
	});

});