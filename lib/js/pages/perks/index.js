$('document').ready(function() {

	$('#preview').click(function() {
		$('#search_form').attr('action', '/perks/index');
		$('#search_form').submit();
	});

	$('#export').click(function() {
		$('#search_form').attr('action', '/perks/export');
		$('#search_form').submit();
	});

});