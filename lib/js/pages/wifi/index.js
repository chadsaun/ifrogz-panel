$(document).ready(function() {
	var timer = null;
	$('input[name|="wifi_network"]').click(function() {
		var network_tier_public_name = $(this).val();
		if ($('[name|="i_agree"]:checked').val() != 1) {
			alert('Please agree to the terms above and click the I agree checkbox.');
		} else {
			$.ajax({
				type: 'post',
				url: '/wifi/password/',
				data: {
					network_tier_public_name: network_tier_public_name
				},
				dataType: 'json',
				success: function (json) {
					if (json['response']['status'] == 'success') {
						var password = json['response']['data']['password'];
						$('#password').html(password);
						$('#network_tier').html(network_tier_public_name);
						$('#password_div').show('fast');
						clearTimeout(timer);
						timer = setTimeout(function() {
						    $('#password_div').fadeOut('slow');
						}, 300000);
						$('input[name=|"i_agree"]').attr('checked', false);
					} else {
						alert(json['response']['errors']['error'][0]['code']
							+' - '
							+json['response']['errors']['error'][0]['message']);
					}
				},
				error: function() {
					alert('Error: Try reloading this page. If the problem continues please see IT.');
				}
			});
		}
		return false;
	});
});