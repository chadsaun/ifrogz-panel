<?php

/**
* This class acts as enumerated class of error codes.
*
* @final
*/
final class Code {
	
	/*
	* When adding more error codes to this file, it is highly suggested that these error code are in accordance
	* with MSDN's System Error Codes.  These codes can be found on the Internet at http://msdn.microsoft.com/
	* en-us/library/ms681381%28VS.85%29.aspx.
	*/
	const ERROR = -1;									// Error of undetermine type
	
	const ERROR_HTTP_BAD_REQUEST = 400;                 // Occurs when no file is found.

	//const ERROR_MISSING_HEADER_DATA = 20000;			// Header information 

	//const ERROR_INVALID_SERVICENAME = 1213;			// The format of the specified service name is invalid.
	//const ERROR_SERVICE_NOT_FOUND = 1243;				// The specified service does not exist.

	const ERROR_CLASS_DOES_NOT_EXIST = 1411;			// Class does not exist.
	const ERROR_INVALID_FUNCTION = 1;					// Incorrect function.
	//const ERROR_FUNCTION_FAILED = 1627;				// Function failed during execution.

	const ERROR_BAD_CREDENTIALS = 1244;					// The operation being requested was not performed because the user has not been authenticated.

	const ERROR_INVALID_COMMAND = 20001;				// Unable to process command.

	//const ERROR_INVALID_PARAMETER = 87;				// The parameter is incorrect.
	const ERROR_DATATYPE_MISMATCH = 1629;				// Data supplied is of wrong type.
	const ERROR_INVALID_DATA = 13;						// The data is invalid.
	const ERROR_NO_DATA_FOUND = 21168;					// Data could not be found.
	//const ERROR_NO_MATCH = 1169;						// There was no match for the specified key in the index.
	const ERROR_NOT_MAPPED = 20002;						// Unable to map value.
    const ERROR_UNRELATED_DATA = 20006;                 // Data is not related

	//const ERROR_OPEN_FAILED = 110;					// The system cannot open the device or file specified.

	//const ERROR_BAD_DRIVER = 2001;					// The specified driver is invalid.
	//const ERROR_DRIVER_DATABASE_ERROR = 652;			// There was error [%2] processing the driver database

	const ERROR_CONNECTION_FAILURE = 21229;				// Unable to connection.
	const ERROR_NO_CONNECTION_ESTABLISHED = 21230;		// No connection is currently established

	const ERROR_DATABASE_DOES_NOT_EXIST = 1065;			// The database specified does not exist.
	//const ERROR_DATABASE_FAILURE = 4313;				// Unable to read from or write to the database.

	const ERROR_SQL_STATEMENT_FAILED = 1615;			// SQL query syntax invalid or unsupported.

	//const ERROR_FILE_NOT_FOUND = 2;					// The system cannot find the file specified.
	//const ERROR_CANT_ACCESS_FILE = 1920;				// The file cannot be accessed by the system.

    const ERROR_EMAIL_SERVICE_UNAVAILABLE = 20005;      // Unable to find mail service
	const ERROR_EMAIL_FAILURE = 20004;					// Failed to deliever email.

	const UNKNOWN_PROTOCOL = 30000;						// Unable to determine protocol

	/**
	* This function does nothing.
	*
	* @access private
	*/
	private function __construct() { }

}
?>