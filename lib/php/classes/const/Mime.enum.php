<?php
/**
* This class enumerates mime type.
*
* @author Matthew A. Mattson <matt.mattson@ifrogz.com>
* @version 0.0
* @copyright Copyright (c) 2009, Reminderband, Inc.
*
* @final
*/
final class Mime {

    const HTML = 'text/html';
	const JSON = 'text/x-json';
    const TEXT = 'text/plain';
	const XML =  'text/xml';

    const REGEX = '/^text\/(html|x-json|plain|xml)$/';

	/**
	* This function does nothing.
	*
	* @access private
	*/
	private function __construct() { }

}
?>
