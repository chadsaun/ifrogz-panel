<?php

/**
* This class acts as enumerated class of redemption codes.
*
* @final
*/
final class Redemption {

    /**
    * This constant is used by the script to avoid processing an order.
    */
    const MYFROGZ_BYPASS_CODE = 'BYPASS1234';

    /**
    * This constant acts as a flag when processing orders so that the script know if an
    * order is an exchange.
    */
    const MYFROGZ_EXCHANGE_CODE = 'YV9G2K7B12MJ49L0P3SKJFZ8X6'; // This must also be changed in the main web service class.

    /**
    * This constant acts as a flag when processing orders so that the script know when an
    * order is a test order.
    */
    const MYFROGZ_TEST_CODE = 'ITSCEM2009';

	/**
	* This function does nothing.
	*
	* @access private
	*/
	private function __construct() { }

}
?>