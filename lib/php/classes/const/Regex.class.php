<?php
include_once('Code.enum.php');
include_once('Mime.enum.php');
include_once('Redemption.enum.php');

/**
* This class serves as a look-up table for common regular expressions.
*
* @author Matthew A. Mattson <matt.mattson@ifrogz.com>
* @version 2009.09.28.01
* @copyright Copyright (c) 2009, Reminderband, Inc.
*/
class Regex {

	/**
	* This function attempts to look-up a regular expression using a string value as a key.
	*
	* @access public
	* @static
	* @param string $value					the string value used to find the regular expression
	* @return string						the regular expression associated with the passed string value
	* @throws Exception						if unable to map string value to a regular expression
	*/
	public static function lookup($value) {
		$value = strtolower($value);
		switch ($value) {
			case 'name':
				return '/^[a-z](\.[a-z]\.|[a-z]*\'[a-z][a-z]+|[a-z]+)([ -][a-z](\.[a-z]\.|[a-z]*\'[a-z][a-z]+|[a-z]+))*$/i';
			case 'street':
				return '/^[^"@!%=;<>\+\$\^\|\*\(\)\[\]\{\}]+$/';
			case 'city':
				return '/^([1-9][0-9]*[\s-_])?[a-z]+((([\.][\s])|[\s-_\'])?[a-z])+$/i';
			case 'state':
				return '/^(AL|AK|AZ|AR|CA|CO|CT|DE|DC|FL|GA|GU|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|PR|RI|SC|SD|TN|TX|UT|VT|VI|VA|WA|WV|WI|WY|AE|AA|AP)$/i';
    		case 'postal_code':
    			return '/^(\d{5})([ -\._]?\d{4})?$/';
			case 'country':
				return '/^[ a-z\'-]+$/i';
			case 'postal_code':
				return '/^(\d{5})([ -\._]?\d{4})?$/';
			case 'phone': // http://www.sitepoint.com/forums/showthread.php?t=304186
			    //return '/^((\+\d{1,3}[_-\s.]?\(?\d\)?[_-\s.]?\d{1,3})|(\(?\d{2,3}\)?))[_-\s.]?(\d{3,4})[_-\s.]?(\d{4})(( x| ext)\d{1,5}){0,1}$/';
				return '/^\(?\d{3}\)?[_-\s.]?\d{3}[_-\s.]?\d{4}$/';
			case 'email':
				return '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i';
			case 'timestamp':
			    return '/^[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}(\s[0-9]{2}:[0-9]{2}:[0-9]{2})?$/';
			case 'date':
			    return '/^[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}$/';
			case 'integer':
				return '/^[+-]?(0|[1-9][0-9]*)$/';
			case 'real':
			case 'float':
			case 'double':
			    return '/^[+-]?(0|[1-9][0-9]*)(\.[0-9]*)?$/';
			case 'mime':
			    return Mime::REGEX;
			case 'identifier':
    		    return '/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/';
			default:
				throw new Exception('Unable to map to a regular expression', Code::ERROR_NOT_MAPPED);
		}
	}

    public static function get_regex_for_myfrogz_order_code() {
        $expr = '/^((a[a-hjkmnp-z2-9]{14})|(a[a-hjkmnp-z]{2}[1-9]{7})|(' . Redemption::MYFROGZ_EXCHANGE_CODE . ')|(' . Redemption::MYFROGZ_TEST_CODE . ')|(' . Redemption::MYFROGZ_BYPASS_CODE . '))$/i';
        return $expr;
    }

	/**
	* This function does nothing.
	*
	* @access private
	*/
	private function __construct() { }

}
?>