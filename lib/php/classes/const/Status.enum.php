<?php

/**
* This class enumerates status messages.
*
* @author Matthew A. Mattson <matt.mattson@ifrogz.com>
* @version 0.0
* @copyright Copyright (c) 2009, Reminderband, Inc.
*
* @final
*/
final class Status {
	
	const SUCCESS = 'success';
	const FAILED = 'failed';

	/**
	* This function does nothing.
	*
	* @access private
	*/
	private function __construct() { }

}
?>
