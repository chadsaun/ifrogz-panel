<?php defined('SYSPATH') OR die('No direct access allowed.');

final class LocationGroup {

	const ALL = 'All Locations';
	const CHINA = 'China'; // 5
	const HONG_KONG = 'Hong Kong'; // 2
	const INTEGRACORE = 'IntegraCore'; // 7
	const LOGAN = 'Logan'; // 1
	const LOGAN_EAST = 'East Logan'; // 6
	const THREEPL = '3PL'; // 4
	const TRANSIT = 'In-Transit'; // 3

	private function __construct() { }

}
?>