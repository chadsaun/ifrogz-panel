<?php
include('init.php');
include_once(LIBPATH.'php/classes/ui/submenu.php');
include_once(MODPATH.'system-ext/classes/kohana/object.php');
include_once(MODPATH.'security/classes/base/roles.php');
include_once(APPPATH.'classes/roles.php');

/**
 * This class handles the building of the Admin's navigation menu.
 *
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2011-11-20
 * @copyright (c) 2011, iFrogz (a subsidiary of ZAGG, Inc.)
 * @package UI
 * @category Menu
 */
class Menu {

	/**
	 * This variable stores the user's roles.
	 *
	 * @access protected
	 * @var array
	 */
    protected $user_roles = NULL;

	/**
	 * This variable stores the submenus and associated items.
	 *
	 * @access protected
	 * @var array
	 */
    protected $submenus = NULL;

	/**
	 * This constructor creates an instance of this class.
	 *
	 * @access public
	 * @param string $session					the user's session
	 */
    public function __construct($session) {
		$user_roles = (isset($session['employee']['permissions'])) ? Menu::roles($session['employee']['permissions']) : array();
		if (isset($session['employee']['id'])) {
			$user_roles[intval($session['employee']['id'])] = NULL;
			$user_roles['permitted'] = NULL;
		}
		$user_roles['always'] = NULL;
		$this->user_roles = array_keys($user_roles);
    	$this->submenus = array();
    }

	/**
	 * This function adds an item to the specified submenu.
	 *
	 * @access public
	 * @param string $submenu					the submenu to which the item will be
	 * 											added
	 * @param string $name						the name of associated with the item
	 * @param string $link						the link associated with the item
	 * @param array $roles						the roles associated with the item
	 * @return Menu								a current instance this class
	 */
    public function add_item($submenu, $name, $link, $roles = array('always')) {
        $this->submenus[$submenu][$link] = array(
            'name' => $name,
            'roles' => Menu::roles($roles),
        );
        return $this;
    }

	/**
	 * This function generates an HTML string representing the menu.
	 *
	 * @access public
	 * @param string $submenu					the submenu that will be rendered
	 * @return string							the HTML string representing the menu
	 */
    public function render($submenu = NULL) {
        $menu = '<div>';
        foreach ($this->submenus as $image => $links) {
            if (is_null($submenu) || ($submenu == $image)) {
                $i = 0;
				$buffer  = '<div class="nav-submenu">';
                $buffer .= '<img src="' . $image . '" style="width: 172px; height: 38px;" /><br />';
                foreach ($links as $link => $attributes) {
					foreach ($this->user_roles as $user_role) {
						if (array_key_exists($user_role, $attributes['roles'])) {
                        	$buffer .= '&nbsp;&nbsp;&middot;&nbsp;&nbsp;<a class="topbar" href="' . $link . '">' . $attributes['name'] . '</a><br />';
							$i++;
							break;
                    	}
					}
                }
                $buffer .= '</div>';
				if ($i > 0) {
					$menu .= $buffer;
				}
            }
        }
        $menu .= '</div>';
        return $menu;
    }

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * This variable stores the mappings for legacy defined roles.
	 *
	 * @access protected
	 * @static
	 * @var array
	 */
	protected static $mappings = array(
	    'accounting' => 'i_f_t',
    	'admin' => 'i_f_a',
    	'all' => 'all', // deprecated
    	'charts' => 'all', // TODO needs to be fixed in DB first before changed
    	'customer service' => 'i_f_c',
    	'customer service admin' => 'i_f_ca', // deprecated
    	'hong kong' => 'i_f_h',
	    'inventory' => 'i_f_v',
    	'it' => 'i_f_i',
    	'management' => 'i_f_m',
    	'nadal' => 'i_f_n', // deprecated
    	'product' => 'i_f_p',
    	'quality control' => 'i_f_qc',
    	'reports_retail' => 'i_f_rr',
	    'sales' => 'i_f_sa',
    	'shieldzone' => 'i_f_z', // deprecated
    	'shipping' => 'i_f_s',
    );

	/**
	 * This function generates an instance of this class.
	 *
	 * @access public
	 * @static
	 * @param array $session					the user's session
	 * @return Menu								an instance of this class
	 */
    public static function factory($session) {
        return new Menu($session);
    }

	/**
	 * This function processes the roles and formats them correctly.
	 *
	 * @access protected
	 * @static
	 * @param mixed $roles						a list of roles
	 * @return array							a set of roles
	 */
    protected static function roles($roles) {
        $buffer = array();
        if (!is_null($roles)) {
            if (is_string($roles)) {
                $roles = preg_split('/,+/', $roles);
				$length = count($roles);
				for ($i = 0; $i < $length; $i++) {
					$roles[$i] = trim($roles[$i]);
					if (empty($roles[$i])) {
						unset($roles[$i]);
					}
				}
            }
            foreach ($roles as $role) {
				$key = (isset(Menu::$mappings[$role])) ? Menu::$mappings[$role] : $role;
                $buffer[$key] = NULL;
            }
        }
        return $buffer;
    }

}
?>