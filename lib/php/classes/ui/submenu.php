<?php

/**
 * This class enumerates the Admin's submenu using the header image's uri.
 *
 * @author iFrogz Developers <developers@ifrogz.com>
 * @version 2011-09-18
 * @copyright (c) 2011, iFrogz (a subsidiary of ZAGG, Inc.)
 * @package UI
 * @category Menu
 */
class Submenu {

    const ADMIN_INTRANET = '/lib/images/template/menu/adminintranet.jpg';
	const CATALOG = '/lib/images/template/menu/catalog.jpg';
    const CHARTS = '/lib/images/template/menu/charts.jpg';
    const COMPANY = '/lib/images/template/menu/company.jpg';
    const DOWNLOADS = '/lib/images/template/menu/downloads.jpg';
    const EXCHANGES_RETURNS = '/lib/images/template/menu/exchangesreturns.jpg';
    const EXTRAS = '/lib/images/template/menu/extras.jpg';
    const ORDERS = '/lib/images/template/menu/orders.jpg';
    const PARTS_PRODUCTS = '/lib/images/template/menu/partsproducts.jpg';
	const PROMOTIONS = '/lib/images/template/menu/promotions.jpg';
    const QUALITY_CONTROL = '/lib/images/template/menu/qualitycontrol.jpg';
    const REPORTS = '/lib/images/template/menu/reports.jpg';
	const SALES = '/lib/images/template/menu/sales.jpg';
    const SHIPPING  = '/lib/images/template/menu/shipping.jpg';
	const SYSTEM = '/lib/images/template/menu/system.jpg';
    const WEBSITE = '/lib/images/template/menu/website.jpg';

}
?>